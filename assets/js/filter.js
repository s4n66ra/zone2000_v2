function inventorytype_name_selected(){
	var id = window.content_table;
	var data_source = $(id).attr('data-source');
    var temp_id = $(id).attr('id');

	load_inventory_table();
    
}

function load_inventory_table(id, page, callback, keyword) {
    //console.log("EVENT : LOAD TABLE");
    if(id === undefined){
        id = window.content_table;
    }

    if(page === undefined)
        page = 1;

    if(keyword === undefined){
        keyword = ''
    }
    
    var data_source = $(id).attr('data-source');
    // jika tidak ada datasource pada content_table / data grid nya maka tidak perlu dilakukan refresh table
    if(data_source !== undefined && data_source != ''){
        var content = '&nbsp;';
        var data = {};
        var temp_id = $(id).attr('id');
        var filter = $(id).attr('data-filter');
        var pagination = $(id).attr('pagination');
        var st_paging = 'true';
        //var keyword = $("#filter").val();

        var option = $("#list-typename option:selected").text();


        if(filter === undefined)
            filter = window.datafilter;

        if (typeof filter !== 'undefined') {
            data = $(filter).serialize();
            tools_filter(filter, id, callback);
        }

        if (typeof pagination !== 'undefined') {
            st_paging = pagination;
        }
        
        // Loading The Table
        Metronic.blockUI({
            target: id,
            boxed :true,
            message : 'Loading Data...',
            loadingColor : 'blue',
            //overlayColor : 'blue',
            //animate:true,
        });

        $.post(
            data_source + '/' + page + '/' + option, 
            data, 
            function(res) {
                console.log('---------------------');
                console.log(res);
                console.log('---------------------');
                content = res.table;
                content += '<div id="pp-' + temp_id + '"></div>';
                
                // Close Loading Message
                Metronic.unblockUI(id);

                $(id).html(content);
                if (st_paging === 'true') {
                    console.log(page);
                    $('#pp-' + temp_id).pagination({
                        pageNumber: page,
                        total: res.total,
                        pageSize: res.limit,
                        onSelectPage: function(pageNumber) {
                            load_inventory_table(id, pageNumber, callback, keyword);
                        },
                        //sembunyikan pagelist pagintion easyui
                        showPageList: false
                    });
                }

                if (callback !== undefined) {
                    callback();
                }

            },'json'
        );    
    }
    
}

$(document).ready(

	function(){

	}
);
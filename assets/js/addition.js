var selected_payout_id = 4;
var selected_store_id = 4;

function gotoPage(url){
	window.location.replace(url);
}



function loadContent(element, url, callback){

	if(url==undefined)
		url = $(element).attr('data-source');

	unblockUI(element);
	$.post(
		url,
		'',
		function(data){
			$(element).html(data);

			if(callback!==undefined)
				callback();
		}
	).always(function(){
		unblockUI(element);
	});
}

function btnLoadContent(button){
	var target = $(button).attr('target');
	var url   = $(button).attr('data-source');
	loadContent(target, url);
}

function loatList(){
	$.ajax({
		dataType: "json",
            url: './supplier/getNamaItem/'+ id,
            success: function(data){



            },
            error: function(data,res){
                console.log("AJAX ERROR");
            }
	});
}

function getItemName(id){
	$.ajax({
		dataType: "json",
            url: './supplier/getNamaItem/'+ id,
            success: function(data){



            },
            error: function(data,res){
                console.log("AJAX ERROR");
            }
	});
}

function btnLoadContentSupplier(button){
	var target = $(button).attr('target');
	var url   = $(button).attr('data-source');
	var def_id   = $(button).attr('def-id');

	loadContent(target, url);
	$.ajax({
		dataType: "json",
        url: './supplier/getNamaItem/'+def_id,
        success: function(temp){
			$.ajax({
	            dataType: "json",
	            url: './supplier/getAllItem/',
	            success: function(data,res){
	                $("#supplier_list_item_id").select2({
	                    allowClear:true,
	                    minimumInputLength:5,
	                    data : data,
	                    placeholder: "Select Item",
	                    initSelection: function(element, callback) {
	                    	var id = def_id;
	                    	data = {id:id,text:temp};
	                    	callback(data);
	                    }
	                })

	            },
	            error: function(data,res){
	                console.log("AJAX ERROR");
	            }

	        });
		}
	});


/*$('#supplier_list_item_id').select2({
            placeholder: 'Search Item',
            ajax: {
                url: './supplier/getAllItem/',
                dataType: 'json',
                quietMillis: 100,
                data: function (term, page) {
                    return {
                        term: term, //search term
                        page_limit: 10 // page size
                    };
                },
                results: function (data, page) {
                    return {results: data.results};
                }

            },
            initSelection: function(element, callback) {

            var id = $(element).val();
            if(id !== "") {
                $.ajax("./supplier/getAllItem/", {
                    data: {id: id},
                    dataType: "json"
                }).done(function(data) {
                    callback(data.result);
                });
            }
        }
    });*/
}

function blockUI(id, message){
	if(message===undefined)
		message = 'Loading Page...';
	Metronic.blockUI({
        target: id,
        boxed :true,
        message : message,
        loadingColor : 'blue',
        overlayColor : 'blue'
    });
}

function unblockUI(id){
	Metronic.unblockUI(id);
}

function reloadPage(id){
	if(id===undefined){
		$('.page:visible').each(function(index, element){
			reloadPage('#' + $(element).attr('id'));
		});
		return;
	}

	var ini = $(id);
	ini.load(ini.attr('data-source'));
	refreshCustomTable();
}

function reloadPageLoading(id){
	if(id===undefined){
		$('.page:visible').each(function(index, element){
			reloadPageLoading('#' + $(element).attr('id'));
		});
		return;
	}

	var ini = $(id);
	blockUI(id, 'Reloading Page ...');
	ini.load(
		ini.attr('data-source'),
		function(){
			Metronic.unblockUI(id);
			refreshCustomTable();
		}
	);
}

function loadNextPage(page, url, callback){
	var page = page;
	var pageNext = page*1 + 1;
	loadPage(page, pageNext, url, callback);
}

function loadPage(from, to, url, callback){
	var page_current 	= '#page-' + from;
	var page_next 		= '#page-' + to;
	
	$(page_next).attr('data-source', url);
	blockUI(page_current);
	$(page_next).load( url, function(){
        Metronic.unblockUI(page_current);
        $(page_current).hide();
        $(page_next).hide();
        $(page_next).fadeIn('fast');
		
		refreshCustomTable();

		if(callback!=undefined){
			eval(callback);
		}
    });

}

function btnLoadNextPage(button){
	var page = $(button).closest('.page').attr('page');
	var callback = $(button).attr('callback');

	loadNextPage(page, $(button).attr('data-source'), callback);
}

function loadModal(source, param, options){
	var content_id = '#form-content-2';
	if(options!= undefined) {
		if(options.full!==undefined && options.full==1)
			$(content_id).addClass('container');
		else
			$(content_id).removeClass('container');
	}

	if (param==undefined)
		param = {};

    $(content_id).load(source, param, function() {    
        $(content_id).modal({
            "backdrop": "static",
            "keyboard": true,
            "show": true,
        });

        $('.bs-select, .select2').trigger('change');
    });
}

function btnLoadModal(button) {
	//TAMBAHAN YOSO
	$("#form-content").html('');
	//----
	var source = $(button).attr('data-source');
	var full = $(button).attr('full-width');
	loadModal(source, {}, {full:full});
}

function pageBack(button){
	//var button = $('#'+buttonId);
	var page_current = $(button).closest('.page').attr('page');
	var page_prev = page_current - 1;
	$('#page-'+page_current).html('').hide();
	$('#page-'+page_prev).fadeIn('fast');

	// RELOAD OLD CUSTOM TABLE IF EXISTS
	refreshCustomTable();

	// RELOAD ALL DATATABLE-AJAX IF EXIST
	refreshDataTable();
}

function pageSaveProcess(url, param, options){
	//var thisPage = '#'+$(this).closest('.page').attr('id');

	var thisPage = '';
	var pageVisible = $('.page:visible');
	pageVisible.each(function(index, element){
		thisPage = '#' + $(element).attr('id');
	});

	console.log(param);
    blockUI(thisPage, 'Saving...');
    $.post(
        url,
        param,
        function(data){
            console.log(data);
            if(data.success){
                toastr['success'](data.message, 'Success');        
                if(data.callback){
                    eval(data.callback);
                }
            }
            else
                toastr['warning'](data.message, 'Error');
        },'json'
    ).error(function(xhr, textStatus, error){
    	console.log('---------- ERROR -----------');
    	console.log(xhr.responseText);
    	//console.log(textStatus);
        toastr['error'](xhr.responseText, 'Error');
    }).always(function(){
        unblockUI(thisPage);
    });
}

function pageSave(url, param, options){
	if(options==undefined)
		options = {};

	if(options.confirm !== undefined){
		componentConfirm(options.confirm, function(result){
			if(result==true){
				pageSaveProcess(url, param, options);
			}
			return;
		});
        // bootbox.confirm(options.confirm_message, function(result) {
       	// 	if(result==false){
       	// 		return;
        //    	}else{
        //    		pageSaveProcess(url, param, options);
        //    	}
        // });   
   	}else{
		pageSaveProcess(url, param, options);
	}

}

function btnPageSave(button){
	var options = {};
	var url = $(button).attr('data-source');
	var confirm_title = $(button).attr('confirm-title');
	var confirm_body = $(button).attr('confirm-body');
	if(confirm_body){
		options.confirm = {
			title : confirm_title,
			body: confirm_body,
		}
	}
	pageSave(url, {}, options);
}


function formSave(divForm){
	if(!divForm) return;
	var form = divForm.find('form');

	console.log("form : "+form);

	$('.format-number', form).each(function (index, element){
		//$(element).val($(element).unmask());
		//$(element).val($(element)) = $(element).val($(element)).split('.').join('') ;
		$(element).val($(element).val().split('.').join('').replace(/\,/g, '.'));
	});

/*	$('.format-number2', form).each(function (index, element){
		$(element).val($(element).unmask());
		
	});*/



	blockUI(divForm, 'Saving Data..');

	$(form).ajaxSubmit({
        beforeSubmit: function(a, f, o) {
            o.dataType = 'json';
            console.log('data o : '+o);
        },
        success: function(data) {
        	if(data.success){
        		// success message
        		toastr['success'](data.message, 'Success');

        		if(data.callback){
        			eval(data.callback);
        		}
        		
				// refresh table
        		refreshCustomTable();
        		refreshDataTable();
        		refreshNewCustomTable();

        		if(divForm.attr('close-modal')) 
        			divForm.closest('.modal').modal('hide');

        	}else{
        		toastr['warning'](data.message, 'Error');
        	}
        	unblockUI(divForm);
        },
        error: function(xhr, textStatus, error){
            toastr['error']('Error : \n'+error+'\n'+textStatus, 'Error');
            //disabled_html(disabled_list, false);
            unblockUI(divForm);
        }

    });

}

function formSaveOptions(divForm, options){
	if(options==undefined)
		options = {};

	if(options.confirm !== undefined){
		componentConfirm(options.confirm, function(result){
			if(result==true){
				formSave(divForm);
			}
			return;
		});
	}else{
		formSave(divForm);
	}

}

function btnFormSave(button){
	var divForm = $(button).closest('.component-form');
	formSave(divForm);
}

function refreshTable(){
	refreshCustomTable();
	refreshDataTable();
	refreshNewCustomTable();
}

// refresh custom table in specified page
function refreshCustomTable(){
	var table = $('.grid-table:visible');
	table.each(function(index, element){
		var id = $(element).attr('id');
		load_table('#'+id);
	});
}

function refreshNewCustomTable(id){

	var table = $('.new-custom-table:visible');
	table.each(function(index, element){
		var id = $(element).attr('id');
		console.log(id);
		loadNewCustomTable('#'+id);
	});

}

function refreshDataTable(all){
	var all = !all;
	var all = false;
	var tables = $.fn.dataTable.fnTables(all);
	if ( tables.length > 0 ) {
		$(tables).each(function(index,element){
			$(element).DataTable().ajax.reload(null, false);
		});
	}
}

function loadDataTable(id){
	$(id).DataTable().ajax.reload(null, false);
}

function loadNewCustomTable(id){
	var div = $(id);
	var data_source = div.attr('data-source');

	console.log(data_source);

	if(data_source==undefined)
		return;

	blockUI(id, 'Loading Table ...');
	$.post(
		data_source,
		{},
		function(data){
			//DATA BALIKAN harus di-echo
			//console.log('data : '+data);
			div.html(data);
		}
	).error(function(){
		toastr['error'](error.message, 'Loading Table Fail');
	}).always(function(){
		unblockUI(id);
	});
}

function customTableDrilldown(button){
	var btn = $(button);
	var url = btn.attr('data-source');
	var target = '#drildown_row_'+ btn.closest('table').attr('id') + '_' + btn.attr('primary');
	
	if(!$(target).hasClass('open'))
		loadContent(target + ' > td > .div-drilldown', url, function(){
			$(target).show(function(){
				$(target).find('.div-drilldown').slideDown();
			});
		});
	else
		$(target).hide();
	
	$(target).toggleClass('open');
}

function hideDrilldown(button){
	var btn = $(button); 
	btn.closest('.div-drilldown').slideUp('', function(){
		btn.closest('tr').toggleClass('open').hide();
	});
	
}

function refreshList(list , target, param){
	if(list==undefined)
		return;

	if(target==undefined)
		target 	= $(list).attr('target');
	
	var url 	= $(list).attr('data-source');
	var target_type = $(list).attr('target-type');
	if(target==undefined || url==undefined)
		return;

	blockUI(target, 'Loading');
	
/*    $.ajax({
        dataType: "text",
        param: param,
        url: uri+'/'+$(list).val(),
        success: function(data,res){
			if (target_type=='text') {
				$(target).val(data);
			}else{
				$(target).html(data);
				if($(target).hasClass('bs-select')){
					$(target).selectpicker('refresh');
				}
				if($(target).hasClass('select2')){
					$(target).select2('val','');
				}
			}

        },
        error: function(data,res){
        }

    });
*/
	$.post(
		url+'/'+$(list).val(),
		param,
		function(data){
			if (target_type=='text') {
				$(target).val(data);
			}else{
				$(target).html(data);
				if($(target).hasClass('bs-select')){
					$(target).selectpicker('refresh');
				}
				if($(target).hasClass('select2')){
					$(target).select2('val','');
				}
			}
		}
	).error(function(){
		
	}).always(function(){
		unblockUI(target);
	});


}

function refreshListPayout(list , target, param){
	if(list==undefined)
		return;

	if(target==undefined)
		target 	= $(list).attr('target');
	
	var url 	= $(list).attr('data-source');
	var target_type = $(list).attr('target-type');
	if(target==undefined || url==undefined)
		return;

	blockUI(target, 'Loading');
	$.post(
		url+'/'+$(list).val(),
		param,
		function(data){
			$(target).val(data);
		}
	).error(function(){
		
	}).always(function(){
		unblockUI(target);
	});


}


$(document).ready(function(){
	
	toastr.options = {
		"closeButton": true,
		"debug": true,
		"positionClass": "toast-top-center",
		"onclick": null,
		"showDuration": "1000",
		"hideDuration": "1000",
		"timeOut": "3000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	}

	$('.btn-page-back').live('click', function(){
		pageBack($(this));
	});	

	$('.btn-page-save').live('click', function(){
		pageSave($(this));
	});

	$('.component-form-save').live('click', function(){
		var divForm = $(this).closest('.component-form');
		console.log('divForm : '+divForm);
		formSave(divForm);
	});

	$('.btn-delete-confirm-yes').live('click', function(){
		var popover = $(this).closest('.popover');
		var btn = popover.parent().find('.btn-delete-confirm');
		var url = btn.attr('data-source');
		var param = {id: btn.attr('delete-id')};
		pageSaveProcess(url, param);
		popover.popover('hide');
	});

	$('.btn-delete-confirm-no').live('click', function(){
		var btn = $(this).closest('.popover').popover('hide');
	});

	// INISIATE TAB ON URL
	var url = document.location.toString();
	if (url.match('#')) {
		var target = '.nav-tabs a[href=#'+url.split('#')[1]+']';
	    //$(target).tab('show') ;
	    $(target).click();
	} 
	$('.nav-tabs a').on('shown.bs.tab', function (e) {
	    window.location.hash = e.target.hash;
	})

	// CLOSE POPOVER WHER CLICK OUTSIDE
	$('html').live('click', function(e) {
		var target = e.target;
		var selector = '.btn-delete-confirm';
		var selectorClass = 'btn-delete-confirm';
		var btn;
		if($(target).parents().is('.popover.in')){
		}else if($(target).hasClass(selectorClass)) {
			btn = $(target);
		}else if($(target).parents().is(selector)){
			btn = $(target).closest(selector);
		}else{
			$(selector).popover('hide');	
		}

		if(btn){
			var current_id = btn.parent().find('.popover').attr('id');			
			$('.popover.in').each(function(key,element){
				var button = $(element).prevAll( selector+":first" );
				var id = $(element).attr('id');
				if(id!=current_id)
					button.popover('hide');
			});
		}
	});

	

	// INISIALISASI
	refreshCustomTable();
	formatNumber();
	initSelect2();
	initDatePicker();
	initBootstrapSelect();
	initPopoverDelete();

});

$(document).ajaxComplete(function() {
	formatNumber();
	initSelect2();
	initDatePicker();
	initBootstrapSelect();
	initPopoverDelete();
});

function initPopoverDelete(element){

	if(element == undefined){
		initPopoverDelete('.btn-delete-confirm');
		return;
	}

	var message = '';
	message+= '<div class="row"><div class="col-md-12">';
	message+= '<a class="btn red btn-delete-confirm-yes">Delete</a>';
	message+= '<a class="btn grey btn-delete-confirm-no">Cancel</a>';
	message+= '</div></div>';

	$(element).popover({
		placement : 'top',
		title : 'Are you sure Delete?',
		animation:true, content:message, html:true
	});
	
}

function formatNumber(element){
	if(element==undefined)
		element = $('.format-number');

	element.formatNumber();
}

function unmaskNumber(){
	$('.format-number', form).each(function (index, element){
		//$(element).val($(element).unmask());
		$(element).val($(element).val().split('.').join('').replace(/\,/g, '.'));

		//$(element).val($(element).val().split('.').join('').replace(/\,/g, '.'));
	});
}

function initSelect2(element){
	if(element==undefined)
		element = $('.select2');

	if ($().select2) {
		element.each(function(key, ex){
			if(!$(ex).data('select2'))
				$(ex).select2({
		            allowClear: true
		        });
		});
    }	
}

function initBootstrapSelect(element){
	if(element==undefined)
		element = $('.bs-select');

	element.selectpicker({
        iconBase: 'fa',
        tickIcon: 'fa-check'
    });
}

function initDatePicker(element){
	if(element==undefined)
		element = $('.date-picker');

	if ($().datepicker) {
        $('.date-picker').datepicker({
            rtl: Metronic.isRTL(),
            orientation: "left",
            autoclose: true
        });
    }
}

function initDatePickerMonth(element){
	if ($().datepicker) {
        $(".date-picker-month").datepicker( {
        	rtl: Metronic.isRTL(),
            orientation: "left",
            autoclose: true,
		    format: "mm-yyyy",
		    viewMode: "months", 
		    minViewMode: "months"
		});
    }
}

function saveUpdateBundle(element){
	var historyprbundle_id = element.id;
	var id = 'tab_po_detail_2';
	//$("#tab_po_detail_2").html('');

	Metronic.blockUI({
        target: '#tab_po_detail_2',
        boxed :true,
        message : "Loading page...",
        loadingColor : 'blue',
        overlayColor : 'blue'
    });


	$.ajax({
	  dataType: "json",
	  url: element.name,
	  success: function(data,res){
       		toastr['success'](data.message, 'Success');
       		var ds = $("#div-table-history").attr("data-source");       
			$("#div-table-history").load(ds);
			$(".save-space").html('');
			Metronic.unblockUI('#tab_po_detail_2');
		}

    });

}

function storetypeaction(){
	var opsi = $('#storetype').val();
	$('.row input').each(function(){
		if(opsi==1){

			if($(this).attr('url-action')=='data[service_charge]'){
				$(this).parent().parent().parent().attr('hidden','true');
				$(this).attr('name','');
			}

			if($(this).attr('url-action')=='data[rent_charge]'){
				$(this).parent().parent().parent().attr('hidden','true');
				$(this).attr('name','');
			}

			if($(this).attr('url-action')=='data[rent_date_start]'){
				$(this).parent().parent().parent().attr('hidden','true');
				$(this).attr('name','');
			}			

			if($(this).attr('url-action')=='data[rent_date_end]'){
				$(this).parent().parent().parent().attr('hidden','true');
				$(this).attr('name','');
			}			

			if($(this).attr('url-action')=='data[percentage_ps]'){
				$(this).parent().parent().parent().removeAttr('hidden');
				$(this).attr('name','data[percentage_ps]');
			}			

		}else if(opsi==0){

			if($(this).attr('url-action')=='data[service_charge]'){
				$(this).parent().parent().parent().removeAttr('hidden');
				$(this).attr('name','data[service_charge]');
			}

			if($(this).attr('url-action')=='data[rent_charge]'){
				$(this).parent().parent().parent().removeAttr('hidden');
				$(this).attr('name','data[rent_charge]');				
			}

			if($(this).attr('url-action')=='data[rent_date_start]'){
				$(this).parent().parent().parent().removeAttr('hidden');
				$(this).attr('name','data[rent_date_start]');				
			}			

			if($(this).attr('url-action')=='data[rent_date_end]'){
				$(this).parent().parent().parent().removeAttr('hidden');
				$(this).attr('name','data[rent_date_end]');				
			}			

			if($(this).attr('url-action')=='data[percentage_ps]'){
				$(this).parent().parent().parent().attr('hidden','true');
				$(this).attr('name','');
			}			


		}


	});

}


function cancel_purchase_order(){
	console.log($("#po_id").val());
	var po_id = $("#po_id").val();

	Metronic.blockUI({
        target: '#table-po',
        boxed :true,
        message : "Loading page...",
        loadingColor : 'blue',
        overlayColor : 'blue'
    });


	$.ajax({
	  dataType: "json",
	  url: './purchase_order/cancel_purchase_order/'+$("#po_id").val(),
	  success: function(data,res){
       		toastr['success'](data.message, 'Success');
       		var ds = preuri+'po/';       
       		$("#tab_po_3").load(ds);
			$("#form-content").modal('hide');
		}

    });

}


function setpoid(po_id){
	console.log(po_id);
	$("#form-content").on('shown',function(){
		$("#form-content #po_id").val(po_id);

	})
}

function printBarcode(){

//    var dataUrl = document.getElementById("barcode_modal").toDataURL(); //attempt to save base64 string to server using this var  
    var dataUrl = document.getElementById("barcode_modal").innerHTML; //attempt to save base64 string to server using this var  

    var windowContent = '<!DOCTYPE html>';
    windowContent += '<html>'
    windowContent += '<head><title></title></head>';
    windowContent += '<body>'
    //windowContent += '<img src="' + dataUrl + '">';
    windowContent += '<div style="margin-left:10px;width:200px;height:100px;">'+dataUrl+'</div>';
    windowContent += '<center style="font-size:10px;">'+$("#nama_hadiah").val()+'</center>';
    windowContent += '</body>';
    windowContent += '</html>';
    var printWin = window.open('','','width=700,height=500');
    printWin.document.open();
    printWin.document.write(windowContent);
    printWin.document.close();
    printWin.focus();
    printWin.print();
    printWin.close();


}


function printZPL(content){
	var ank = $("#item_key").val();
	var nm = $("#nama_hadiah").val();
	var nc = $("#numb_copies").val();
	$.ajax({
		type:"POST",
	  	dataType: "html",
	  	data:{angka : ank, nama : nm, nc:nc,},
	  	url: './hadiah/initPrinting/',
	  	success: function(data,res){
	  		//alert(data);
	  		$("#list_printer").html(data);
	  		//javascript:jsWebClientPrint.getPrinters();
	    
		
			javascript:doClientPrint();
		},
	    error:function(){
	    	console.log("JS PRINTING ERROR");
	    }


    });
}


function printZPLAllHadiah(content){
	var ank = 3;
	var nm = "dora";
	var nc = 4;
	$.ajax({
		type:"POST",
	  	dataType: "html",
	  	data:{angka : ank, nama : nm, nc:nc,},
	  	url: './hadiah/printAllMerBarcode/',
	  	success: function(data,res){
	  		//alert(data);
	  		$("#list_printer").html(data);
	  		//javascript:jsWebClientPrint.getPrinters();
	    
		
			javascript:doClientPrint();
		},
	    error:function(){
	    	console.log("JS PRINTING ERROR");
	    }


    });
}


function printZPLKoli(content){

	var koli_id = $("#koli_id").val();
	var koli_code = $("#koli_code").val();
	var qty = $("#qty").val();
	var note = $("#note").val();
	var koli_status = $("#koli_status").val();
	var store_id = $("#store_id").val();
	var supplier_id = $("#supplier_id").val();
	var sp_code = $("#sp_code").val();
	var dc_qty_received = $("#dc_qty_received").val();
	var kd_cabang = $("#kd_cabang").val();

	var nc = $("#numb_copies").val();

	$.ajax({
		type:"POST",
	  	dataType: "html",
	  	data:{nc:nc, koli_id:koli_id, koli_code:koli_code, qty:qty, note:note, koli_status:koli_status, store_id:store_id, supplier_id:supplier_id, sp_code:sp_code, dc_qty_received:dc_qty_received, kd_cabang:kd_cabang},
	  	url: './receiving_warehouse/initPrinting/',
	  	success: function(data,res){
	  		$("#list_printer").html(data);
	  		//javascript:jsWebClientPrint.getPrinters();
	    
		
			javascript:doClientPrint();
		},
	    error:function(){
	    	console.log("JS PRINTING ERROR");
	    }


    });
}

function printZPLSparepart(content){

	var kd_sparepart = $("#kd_sparepart").val();
	var nama_sparepart = $("#nama_sparepart").val();

	var nc = $("#numb_copies").val();

	$.ajax({
		type:"POST",
	  	dataType: "html",
	  	data:{nc:nc, nama_sparepart:nama_sparepart, kd_sparepart:kd_sparepart},
	  	url: './sparepart/initPrinting/',
	  	success: function(data,res){
	  		$("#list_printer").html(data);
	  		//javascript:jsWebClientPrint.getPrinters();
	    
		
			javascript:doClientPrint();
		},
	    error:function(){
	    	console.log("JS PRINTING ERROR");
	    }


    });
}

function printZPLAllSparepart(){
	var kd_sparepart = $("#kd_sparepart").val();
	var nama_sparepart = $("#nama_sparepart").val();

	var nc = $("#numb_copies").val();

	$.ajax({
		type:"POST",
	  	dataType: "html",
	  	data:{nc:nc, nama_sparepart:nama_sparepart, kd_sparepart:kd_sparepart},
	  	url: './sparepart/printAllBarcode/',
	  	success: function(data,res){
	  		$("#list_printer").html(data);
	  		//javascript:jsWebClientPrint.getPrinters();
	    
		
			javascript:doClientPrint();
		},
	    error:function(){
	    	console.log("JS PRINTING ERROR");
	    }


    });	
}

function printZPLMesin(content){

	var kd_mesin = $("#kd_mesin").val();
	var serial_number = $("#serial_number").val();
	var nama_jenis_mesin = $("#nama_jenis_mesin").val();

	var nc = $("#numb_copies").val();

	$.ajax({
		type:"POST",
	  	dataType: "html",
	  	data:{nc:nc, kd_mesin:kd_mesin, serial_number:serial_number,nama_jenis_mesin:nama_jenis_mesin,},
	  	url: './mesin/initPrinting/',
	  	success: function(data,res){
	  		$("#list_printer").html(data);
	  		//javascript:jsWebClientPrint.getPrinters();
	    
		
			javascript:doClientPrint();
		},
	    error:function(){
	    	console.log("JS PRINTING ERROR");
	    }


    });
}

function printZPLAllMesin(content){

	var kd_mesin = $("#kd_mesin").val();
	var serial_number = $("#serial_number").val();

	var nc = $("#numb_copies").val();

	$.ajax({
		type:"POST",
	  	dataType: "html",
	  	data:{nc:nc, kd_mesin:kd_mesin, serial_number:serial_number,},
	  	url: './mesin/printAllBarcode/',
	  	success: function(data,res){
	  		$("#list_printer").html(data);
	  		//javascript:jsWebClientPrint.getPrinters();
	    
		
			javascript:doClientPrint();
		},
	    error:function(){
	    	console.log("JS PRINTING ERROR");
	    }


    });
}



function printZPLStokSC(content){
	var item_key = $("#item_key").val();
	var item_name = $("#item_name").val();

	var nc = $("#numb_copies").val();

	$.ajax({
		type:"POST",
	  	dataType: "html",
	  	data:{nc:nc, item_key:item_key, item_name:item_name,},
	  	url: './stok_sc/initPrinting/',
	  	success: function(data,res){
	  		$("#list_printer").html(data);
	  		//javascript:jsWebClientPrint.getPrinters();
	    
		
			javascript:doClientPrint();
		},
	    error:function(){
	    	console.log("JS PRINTING ERROR");
	    }


    });	
}

function printZPLStok(content){
	var item_key = $("#item_key").val();
	var item_name = $("#item_name").val();

	var nc = $("#numb_copies").val();

	$.ajax({
		type:"POST",
	  	dataType: "html",
	  	data:{nc:nc, item_key:item_key, item_name:item_name,},
	  	url: './stok/initPrinting/',
	  	success: function(data,res){
	  		$("#list_printer").html(data);
	  		//javascript:jsWebClientPrint.getPrinters();
	    
		
			javascript:doClientPrint();
		},
	    error:function(){
	    	console.log("JS PRINTING ERROR");
	    }


    });	
}

function printZPLAllStok(){
	var item_key = $("#item_key").val();
	var item_name = $("#item_name").val();

	var nc = $("#numb_copies").val();

	$.ajax({
		type:"POST",
	  	dataType: "html",
	  	data:{nc:nc, item_key:item_key, item_name:item_name,},
	  	url: './stok/initPrintingAll/',
	  	success: function(data,res){
	  		$("#list_printer").html(data);
	  		//javascript:jsWebClientPrint.getPrinters();
	    
		
			javascript:doClientPrint();
		},
	    error:function(){
	    	console.log("JS PRINTING ERROR");
	    }


    });	
	
}


function printZPLService(content){
	var doc_number = $("#doc_number").val();
	var nama_jenis_mesin = $("#nama_jenis_mesin").val();

	var nc = $("#numb_copies").val();

	$.ajax({
		type:"POST",
	  	dataType: "html",
	  	data:{nc:nc, doc_number:doc_number, nama_jenis_mesin:nama_jenis_mesin,},
	  	url: './service/initPrinting/',
	  	success: function(data,res){
	  		$("#list_printer").html(data);		
			javascript:doClientPrint();
		},
	    error:function(){
	    	console.log("JS PRINTING ERROR");
	    }


    });	

}


function convertCanvasToImage(canvas) {
	var image = new Image();
	image.src = canvas.toDataURL("image/png");
	return image;
}

function changeGlobeDate(input){
	var d1 = new Date().getTime();
	var d2 = new Date($("#glob_date_outlet").val()).getTime();
	var target = $("#sistem_date").attr('target');
	$.ajax({
		url:target+"/"+input.value,
		dataType:"JSON",
		success:function(data){
			if(data==1){
				$("#glob_date_outlet").css("color","red");
			}else{
				$("#glob_date_outlet").css("color","black");
			}
			console.log(data);
		}
	});
}


function read_notif(notif_user_id){
    $.ajax({
        dataType:"html",
        url:"./notification/read_notif/"+notif_user_id,
        success:function(data){
            var res = JSON.parse(data);
            if(res.message=="success"){
            	updateNotification();
                refreshDataTable();
                toastr['success']("Notification Added", 'Success');                                           
            }else{
                toastr['error']("Fail to Add", 'Failed');                                                    
            }                                             


        },
        error:function(){
            toastr['error']("Fail to Add", 'Failed');          
            console.log("kirim notif gagal");
        }
    });

}


function read_notif_pr(notif_id){
    $.ajax({
        dataType:"html",
        url:"./notification/read_notif_pr/"+notif_id,
        success:function(data){
            var res = JSON.parse(data);
            if(res.message=="success"){
            	updateNotificationPR();
                refreshDataTable();
                toastr['success']("Notification Added", 'Success');                                           
            }else{
                toastr['error']("Fail to Add", 'Failed');                                                    
            }                                             


        },
        error:function(){
            toastr['error']("Fail to Add", 'Failed');          
            console.log("kirim notif gagal");
        }
    });

}

function gotopo(){
	window.location = './purchase_order';
}


function updateNotification(){
    $.ajax({
        dataType:"html",
        url:"./notification/get_jml_notif/",
        success:function(data){
            var res = JSON.parse(data);
            $("#jml_notif1").html(res["jml_notif"]);
            $("#jml_notif2").html('<i class="icon-bell"></i> Notification '+res["jml_notif"]);
        },
        error:function(){
        }
    });

}

function updateNotificationPR(){
    $.ajax({
        dataType:"html",
        url:"./notification/get_jml_notif/",
        success:function(data){
            var res = JSON.parse(data);
            $("#jml_notif1").html(res["jml_notif"]);
            $("#jml_notif2").html('<i class="icon-bell"></i> Notification '+res["jml_notif"]);
        },
        error:function(){
        }
    });

}

//FILTER SERVICE DENGAN START DATE AND END DATE
function refreshTableByDate(){
	var startdate = $("#rent_date_start").val();
	var enddate = $("#rent_date_end").val();

	$.ajax({
		url: './service/update_table_main/' +startdate+ '/' + enddate + '/',
		type: 'POST',
		dataType : 'html',
		success:function(data){
			var dataparse = JSON.parse(data).data;
			
			var row='';
			if(dataparse.length>0){
				if(dataparse[0].nama_cabang != ''){
					var j=0;
					for(var i=0; i<dataparse.length; i++){
						j=j+1;
						row = row +'<tr role="row" class="odd">'+'<td>'+j+'</td>'
						+'<td>'+dataparse[i].serial_number+'</td>'
						+'<td>'+dataparse[i].doc_number+'</td>'
						+'<td>'+dataparse[i].nama_jenis_mesin+'</td>'
						+'<td>'+dataparse[i].nama_cabang+'</td>'
						+'<td>'+dataparse[i].rec_created+'</td>'
						+'<td>'+dataparse[i].due_date+'</td>'
						+'<td>'+dataparse[i].loc_name+'</td>'
						+'<td>'+dataparse[i].nama+'</td>'
						+'<td>'+dataparse[i].issue_description+'</td>'
						+'<td>'+dataparse[i].service_id+'</td>'

						+'</tr>';
					}
					$(".table-scrollable tbody").html(row);
				}else{
					row = row + '<tr class="odd"><td valign="top" colspan="11" class="dataTables_empty">No matching records found</td></tr>';
					$(".table-scrollable tbody").html(row);
				}

			}else{
				row = row + '<tr class="odd"><td valign="top" colspan="11" class="dataTables_empty">No matching records found</td></tr>';
				$(".table-scrollable tbody").html(row);

			}
		}
	});
}

function refreshByStore(list , target, param){
	if(list==undefined)
		return;

	if(target==undefined)
		target 	= $(list).attr('target');
	
	var url 	= $(list).attr('data-source');
	var target_type = $(list).attr('target-type');
	if(target==undefined || url==undefined)
		return;

	blockUI(target, 'Loading');
	
	selected_payout_id	= $(list).val();	
	selected_store_id	= $(list).val();	

	$.post(
		url+'/'+$(list).val(),
		param,
		function(data){
			blockUI("table-laporan-payout", "Loading...");
			$.ajax({
				datatype:'html',
				success:function(){
					//$(".table-scrollable tbody").html('');
					var dataparse = JSON.parse(data).data;
					//alert(dataparse[0].nama_cabang);
					var row='';
					if(dataparse.length>0){
						if(dataparse[0].nama_cabang != ''){
							var j=0;
							for(var i=0; i<dataparse.length; i++){
								j=j+1;
								row = row +'<tr role="row" class="odd">'+'<td>'+j+'</td>'
								+'<td>'+dataparse[i].no+'</td>'
								+'<td>'+dataparse[i].register+'</td>'
								+'<td>'+dataparse[i].nama_cabang+'</td>'
								+'<td>'+dataparse[i].tiket+'</td>'
								+'<td>'+dataparse[i].value+'</td>'
								+'<td>'+dataparse[i].omzet+'</td>'
								+'<td>'+dataparse[i].payout+'</td>'

								+'</tr>';
							}
							$(".table-scrollable tbody").html(row);
						}else{
							row = row + '<tr class="odd"><td valign="top" colspan="9" class="dataTables_empty">No matching records found</td></tr>';
							$(".table-scrollable tbody").html(row);
						}

					}
				}

			});

		}

	).error(function(){
		
	}).always(function(){
		unblockUI(target);
	});


}


function refreshPenjualanByStore(list , target, param){
	if(list==undefined)
		return;

	if(target==undefined)
		target 	= $(list).attr('target');
	
	var url 	= $(list).attr('data-source');
	var target_type = $(list).attr('target-type');
	if(target==undefined || url==undefined)
		return;

	blockUI(target, 'Loading');
	
	selected_payout_id	= $(list).val();	
	selected_store_id	= $(list).val();	

	$.post(
		url+'/'+$(list).val(),
		param,
		function(data){
			blockUI("table-laporan-payout", "Loading...");
			$.ajax({
				datatype:'html',
				success:function(){
					//$(".table-scrollable tbody").html('');
					var dataparse = JSON.parse(data).data;
					//alert(dataparse[0].nama_cabang);
					var row='';
					if(dataparse.length>1){
						var j=0;
						for(var i=0; i<dataparse.length; i++){
							j=j+1;
							row = row +'<tr role="row" class="odd">'+'<td>'+j+'</td>'
							+'<td>'+dataparse[i].ticket_count+'</td>'
							+'<td>'+dataparse[i].item_key+'</td>'
							+'<td>'+dataparse[i].item_name+'</td>'
							+'<td>'+dataparse[i].stok_total+'</td>'
							+'<td>'+dataparse[i].v1+'</td>'
							+'<td>'+dataparse[i].t1+'</td>'
							+'<td>'+dataparse[i].v2+'</td>'
							+'<td>'+dataparse[i].t2+'</td>'
							+'<td>'+dataparse[i].v3+'</td>'
							+'<td>'+dataparse[i].t3+'</td>'
							+'<td>'+dataparse[i].v4+'</td>'
							+'<td>'+dataparse[i].t4+'</td>'
							+'<td>'+dataparse[i].v5+'</td>'
							+'<td>'+dataparse[i].t5+'</td>'
							+'<td>'+dataparse[i].v6+'</td>'
							+'<td>'+dataparse[i].t6+'</td>'
							+'<td>'+dataparse[i].v7+'</td>'
							+'<td>'+dataparse[i].t7+'</td>'
							+'<td>'+dataparse[i].v8+'</td>'
							+'<td>'+dataparse[i].t8+'</td>'
							+'<td>'+dataparse[i].v9+'</td>'
							+'<td>'+dataparse[i].t9+'</td>'
							+'<td>'+dataparse[i].v10+'</td>'
							+'<td>'+dataparse[i].t10+'</td>'
							+'<td>'+dataparse[i].v11+'</td>'
							+'<td>'+dataparse[i].t11+'</td>'
							+'<td>'+dataparse[i].v12+'</td>'
							+'<td>'+dataparse[i].t12+'</td>'
							+'<td>'+dataparse[i].v13+'</td>'
							+'<td>'+dataparse[i].t13+'</td>'
							+'<td>'+dataparse[i].v14+'</td>'
							+'<td>'+dataparse[i].t14+'</td>'
							+'<td>'+dataparse[i].v15+'</td>'
							+'<td>'+dataparse[i].t15+'</td>'
							+'<td>'+dataparse[i].v16+'</td>'
							+'<td>'+dataparse[i].t16+'</td>'
							+'<td>'+dataparse[i].v17+'</td>'
							+'<td>'+dataparse[i].t17+'</td>'
							+'<td>'+dataparse[i].v18+'</td>'
							+'<td>'+dataparse[i].t18+'</td>'
							+'<td>'+dataparse[i].v19+'</td>'
							+'<td>'+dataparse[i].t19+'</td>'
							+'<td>'+dataparse[i].v20+'</td>'
							+'<td>'+dataparse[i].t20+'</td>'
							+'<td>'+dataparse[i].v21+'</td>'
							+'<td>'+dataparse[i].t21+'</td>'
							+'<td>'+dataparse[i].v22+'</td>'
							+'<td>'+dataparse[i].t22+'</td>'
							+'<td>'+dataparse[i].v23+'</td>'
							+'<td>'+dataparse[i].t23+'</td>'
							+'<td>'+dataparse[i].v24+'</td>'
							+'<td>'+dataparse[i].t24+'</td>'
							+'<td>'+dataparse[i].v25+'</td>'
							+'<td>'+dataparse[i].t25+'</td>'
							+'<td>'+dataparse[i].v26+'</td>'
							+'<td>'+dataparse[i].t26+'</td>'
							+'<td>'+dataparse[i].v27+'</td>'
							+'<td>'+dataparse[i].t27+'</td>'
							+'<td>'+dataparse[i].v28+'</td>'
							+'<td>'+dataparse[i].t28+'</td>'
							+'<td>'+dataparse[i].v29+'</td>'
							+'<td>'+dataparse[i].t29+'</td>'
							+'<td>'+dataparse[i].v30+'</td>'
							+'<td>'+dataparse[i].t30+'</td>'
							+'<td>'+dataparse[i].v31+'</td>'
							+'<td>'+dataparse[i].t31+'</td>'
							+'<td>'+dataparse[i].total+'</td>'
							+'<td>'+dataparse[i].total_ticket+'</td>'

							+'</tr>';
						}
						$(".table-scrollable tbody").html(row);

					}else{
						row = row + '<tr class="odd"><td valign="top" colspan="32" class="dataTables_empty">No matching records found</td></tr>';
						$(".table-scrollable tbody").html(row);
					}
				}

			});

		}

	).error(function(){
		
	}).always(function(){
		unblockUI(target);
	});


}


function printExcelLaporanPayout(){
	window.location='./laporan_payout/excel/'+selected_payout_id;
}

function printExcelLaporanPenjualan(){
	window.location='./laporan_penjualan/excel/'+selected_store_id;
}

function printExcelLapPegawaiPusat(){
	window.location='./lap_pegawai/excel/'+selected_store_id;
}

function printExcelLap(url){
	window.location=url;
}

function printExcelJs(url){
	arr_nama_cabang = [];
	arr_doc_number = [];
	arr_nama_jenis_mesin = [];
	arr_nama = [];
	arr_issue_description = [];
	arr_loc_name = [];

	$(".filter-value-container .filter-value-row-doc_number .label-info").each(
		function(){
			$(".filter-value-container .filter-value-row-doc_number .label-info span").each(function(){$(this).remove();});
			arr_doc_number.push($(this).html());
			$(".filter-value-container .filter-value-row-doc_number .label-info").append('<span data-role="remove"></span>');
		}
	);

	$(".filter-value-container .filter-value-row-nama_cabang .label-info").each(
		function(){
			$(".filter-value-container .filter-value-row-nama_cabang .label-info span").each(function(){$(this).remove();});
			arr_nama_cabang.push($(this).html());
			$(".filter-value-container .filter-value-row-nama_cabang .label-info").append('<span data-role="remove"></span>');
		}
	);


	$(".filter-value-container .filter-value-row-nama_jenis_mesin .label-info").each(
		function(){
			$(".filter-value-container .filter-value-row-nama_jenis_mesin .label-info span").each(function(){$(this).remove();});
			arr_nama_jenis_mesin.push($(this).html());
			$(".filter-value-container .filter-value-row-nama_jenis_mesin .label-info").append('<span data-role="remove"></span>');
		}
	);

	$(".filter-value-container .filter-value-row-nama .label-info").each(
		function(){
			$(".filter-value-container .filter-value-row-nama .label-info span").each(function(){$(this).remove();});
			arr_nama.push($(this).html());
			$(".filter-value-container .filter-value-row-nama .label-info").append('<span data-role="remove"></span>');
		}
	);

	$(".filter-value-container .filter-value-row-issue_description .label-info").each(
		function(){
			$(".filter-value-container .filter-value-row-issue_description .label-info span").each(function(){$(this).remove();});
			arr_issue_description.push($(this).html());
			$(".filter-value-container .filter-value-row-issue_description .label-info").append('<span data-role="remove"></span>');
		}
	);

	$(".filter-value-container .filter-value-row-loc_name .label-info").each(
		function(){
			$(".filter-value-container .filter-value-row-loc_name .label-info span").each(function(){$(this).remove();});
			arr_loc_name.push($(this).html());
			$(".filter-value-container .filter-value-row-loc_name .label-info").append('<span data-role="remove"></span>');
		}
	);

	//alert(arr_nama_cabang);

	//window.location='./laporan_payout/excel/'+selected_payout_id;
	//POST ke EXCEL
	$.ajax({
		url : './'+url,
		type : 'POST',
		dataType : 'html',
		data : {arr_nama_cabang : arr_nama_cabang, arr_doc_number : arr_doc_number, arr_nama_jenis_mesin : arr_nama_jenis_mesin, arr_nama : arr_nama, arr_issue_description : arr_issue_description, arr_loc_name: arr_loc_name},
		success:function(data){
	       	var blob = new Blob([data], {type: "text/plain;charset=utf-8"});
			saveAs(blob, "Laporan Service "+Date("Y-m-d H:i:s")+".xls");
		}
	});	

}
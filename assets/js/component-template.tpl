<div class="portlet light modal-body" id="component-confirm" tabindex="-1">				
	<div class="portlet-title">
		<div class="caption font-purple-plum">
			<i class="icon-speech font-purple-plum"></i>
			<span class="caption-subject bold uppercase">
				{{title}}
			</span>
		</div>
		<div class="actions">
			<a class="btn btn-circle btn-icon-only btn-default remove" data-dismiss="modal" title="Fullscreen" href="#">
				<i class="icon-close"></i>
			</a>
		</div>
	</div>

	<div class="portlet-body">
		<div class="row">
						
		</div>

		<div class="row" style="margin-top:20px;">
			<div class="col-md-12" style="text-align:center;">
				<div class="btn-group " style="text-align:center;">
					<a href="javascript://" id="button-back" class="btn btn-default" data-dismiss="modal">
						<i class="fa fa-arrow-left"></i> CANCEL
					</a>
					<a href="javascript://" id="button-save" class="btn yellow-casablanca">
						<i class="fa fa-check"></i> CONFIRM
					</a>
				</div>
			</div>
		</div>

	</div>
</div>
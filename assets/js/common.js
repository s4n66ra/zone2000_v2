
jQuery(document).ready(function() { 
    /*$('.btn-fosrm-modal').click(function(){

        var $modal = $('#form-contents');
        var url = $(this).attr('data-source');
        // create the backdrop and wait for next modal to be triggered
        $('body').modalmanager('loading');
        setTimeout(function(){
          $modal.load('http://facebook.com', '', function(){
          $modal.modal();
        });
        }, 10000);
    });

*/

    $("#finput").submit(
        function(){
            load_table();
            console.log("---DEBUG 1---");
            return false;
        }
    );
    
    load_table();

});

window.status_modal = false;

window.form_content_main = '#index-content';
window.drildown_key = 'drildown_key_';

// content_table adalah id dimana data grid (table) ditempatkan
window.content_table = '#content_table';
// form-content adalah div untuk menempatkan form untuk CRUD yang dimunculkan secara modal
window.form_content_modal = '#form-content';
// ffilter adalah id dari div filter / search 
window.data_filter = '#ffilter';
window.page_content= '#page-content';

function refresh_filter(){ 
    //alert('test refresh');
    load_table();
    console.log("---DEBUG 2---");
}

function load_page(id, url, param, callback){
    // id : id div yang akan diganti , url : url ajax
    if(id === undefined)
        id = window.page_content;
    if(url === undefined || url=='' || url==null){
        url = $(id).attr('data-source');
    }
    if(url === undefined) return 0;
    blockUI(id);
    $.post(
        url,
        param,
        function(data){
            console.log('Load Page : '+url);
            console.log('===================');
            console.log(data);
            $(id).hide();
            $(id).html(data);
            $(id).fadeIn('fast');
            Metronic.unblockUI(id);
            load_table();
            console.log("---DEBUG 3---");
            if(callback !== undefined){
                callback();                
                console.log("---DEBUG 4---");
            }
            

        }
    );
}

function refresh_dropdown_options(sourceElement, targetElementId, url){
	id = window.page_content;
	value = $(sourceElement).val();
	targetUrl = url + "/" + value;
	blockUI(id);
	$.post(
			targetUrl,
			function(data){
				console.log("Load page: "+ targetUrl);
				$(targetElementId).hide();
	            $(targetElementId).html(data);
	            $(targetElementId).fadeIn('fast');
	            Metronic.unblockUI(id);
			}
	);
}

function test(domElement){
	alert($(domElement).val());
}

function refresh_options (id, url, param, callback){
    // id : id div yang akan diganti , url : url ajax
    if(id === undefined)
        id = window.page_content;
    if(url === undefined || url=='' || url==null){
        url = $(id).attr('data-source');
    }
    if(url === undefined) return 0;
    blockUI(id);
    $.post(
        url,
        param,
        function(data){
            console.log('Load Page : '+url);
            console.log('===================');
            console.log(data);
            $(id).hide();
            $(id).html(data);
            $(id).fadeIn('fast');
            Metronic.unblockUI(id);
            load_table();
            if(callback !== undefined)
            	callback();
        }
    );

}

function load_table(id, page, callback, keyword) {
    //console.log("EVENT : LOAD TABLE");
    if(id === undefined){
        id = window.content_table;
    }

    if(page === undefined)
        page = 1;

    if(keyword === undefined){
        keyword = ''
    }
    
    var data_source = $(id).attr('data-source');
    // jika tidak ada datasource pada content_table / data grid nya maka tidak perlu dilakukan refresh table
    if(data_source !== undefined && data_source != ''){
        var content = '&nbsp;';
        var data = {};
        var temp_id = $(id).attr('id');
        var filter = $(id).attr('data-filter');
        var pagination = $(id).attr('pagination');
        var st_paging = 'true';
        var keyword = $("#filter").val();

        var island = $("#list-island option:selected").text();

        if(filter === undefined)
            filter = window.datafilter;

        if (typeof filter !== 'undefined') {
            data = $(filter).serialize();
            tools_filter(filter, id, callback);
        }

        if (typeof pagination !== 'undefined') {
            st_paging = pagination;
        }
        
        // Loading The Table
        Metronic.blockUI({
            target: id,
            boxed :true,
            message : 'Loading Data...',
            loadingColor : 'blue',
            //overlayColor : 'blue',
            //animate:true,
        });

        $.post(
            data_source + '/' + page + '/' + keyword, 
            data, 
            function(res) {
                console.log('---------------------');
                console.log(res);
                console.log('---------------------');
                content = res.table;
                content += '<div id="pp-' + temp_id + '"></div>';
                
                // Close Loading Message
                Metronic.unblockUI(id);

                $(id).html(content);
                if (st_paging === 'true') {
                    console.log(page);
                    $('#pp-' + temp_id).pagination({
                        pageNumber: page,
                        total: res.total,
                        pageSize: res.limit,
                        onSelectPage: function(pageNumber) {
                            load_table(id, pageNumber, callback, keyword);
                        },
                        //sembunyikan pagelist pagintion easyui
                        showPageList: false
                    });
                }

                if (callback !== undefined) {
                    callback();
                }

            },'json'
        );    
    }
    
}

function load_form_modal_2(button, filter,content) {
    var form_filter = $(filter);
    window.status_modal = true;
    var content_id = window.form_content_modal;
    if (typeof content !== 'undefined') {
        content_id = content;
        //YOSO COMMENT
        //console.log("IYA :"+content_id);
    }

    //TAMBAHAN YOSO
    $("#form-content-2").html('');
    //

    //YOSO COMMENT
    //console.log("content_id :"+content_id);

    //alert(content);

    var id = '#' + button;
    if (typeof $(id).attr('disabled') === 'undefined') {

        var source      = $(id).attr('data-source');
        var full_width  = $(id).attr('full-width');

        // implementasi opsi ke modal yg didapat dari attribut tombol
        // contoh : full-width
        if(full_width !== undefined && full_width==1){
            $(content_id).addClass('container');
        }else{
            $(content_id).removeClass('container');
        }
        
        disabled_html([id], true);
        $(content_id).html('');
        
        $(content_id).load(source, function() {
            // LOGGING
            console.log('xxxxxxxxxxxxxxxxx');
            console.log(source);
            console.log('xxxxxxxxxxxxxxxxx');

            disabled_html([id], false);            
            $(content_id).modal({
                "backdrop": "static",
                "keyboard": true,
                "show": true,
            });
        });
    }
}


function load_form_modal(button, content) {
    window.status_modal = true;

    var content_id = window.form_content_modal;
    if (typeof content !== 'undefined') {
        content_id = content;
    }

    //alert(content);
    var id = '#' + button;
    if (typeof $(id).attr('disabled') === 'undefined') {

        var source = $(id).attr('data-source');
        disabled_html([id], true);
        //$(content_id).addClass('loading-progress');
        $(content_id).modal({
            "backdrop": "static",
            "keyboard": true,
            "show": true
        }).load(source, function() {
            //$(content_id).removeClass('loading-progress');            
            disabled_html([id], false);
        });
    }
}


function load_form_modal_3(button, filter,content,po_id) {
    var form_filter = $(filter);
    window.status_modal = true;
    var content_id = window.form_content_modal;
    if (typeof content !== 'undefined') {
        content_id = content;
        //YOSO COMMENT
        //console.log("IYA :"+content_id);
    }

    //TAMBAHAN YOSO
    $("#form-content-2").html('');
    //

    //YOSO COMMENT
    //console.log("content_id :"+content_id);

    //alert(content);

    var id = '#' + button;
    if (typeof $(id).attr('disabled') === 'undefined') {

        var source      = $(id).attr('data-source');
        var full_width  = $(id).attr('full-width');

        // implementasi opsi ke modal yg didapat dari attribut tombol
        // contoh : full-width
        if(full_width !== undefined && full_width==1){
            $(content_id).addClass('container');
        }else{
            $(content_id).removeClass('container');
        }
        
        disabled_html([id], true);
        $(content_id).html('');
        
        $(content_id).load(source, function() {
            // LOGGING
            console.log('xxxxxxxxxxxxxxxxx');
            console.log(source);
            console.log('xxxxxxxxxxxxxxxxx');

            disabled_html([id], false);            
            $(content_id).modal({
                "backdrop": "static",
                "keyboard": true,
                "show": true,
            });
        });
    }
}


function simpan_data(button, form, disable, content, callback){
    /*
        button : tombol yang memanggil fungsi ini. / button save-nya (id-nya saja)
                ex <a href="#" id="button">
        form : form yang akan disubmit ke url action dari form ini
                form disini sudah ada # (kres nya)
                ex : #form-input

    */
    if (typeof $('#' + button).attr('disabled') === 'undefined') {
        var confirm_message = $('#'+button).attr('data-confirm');

        if(form === undefined){
            // jika form tidak didefinisikan. maka form diambil dari tag <form> yang paling dekat dengan tombol
            form = '#' + $('#' + button).closest('form').attr('id');
            
        }
        
        // kondisi saat butuh konfirmasi : contohnya pas delete
        if(confirm_message !== undefined){
            bootbox.confirm(confirm_message, function(result) {
               if(result==true){
                    // proses selanjutnya
                    // ex : proses delete
                    simpan_data_proses(button,form,disable,content, callback);
               }
            });    
        }else{
            // langsung proses karena tidak ada konfirmasi
            // ex : proses input
            simpan_data_proses(button,form,disable,content, callback);
        }
    }                
}

function simpan_data_proses(button, form, disable, content, callback){
    var conf_tinymce    = $(form).attr('data-tinymce');
    
    if(disable !== undefined){
        var disabled_list = disable.split('|');
        disabled_list.push('#' + button);
        disabled_html(disabled_list, true);
    }

    //$(form).addClass('loading-progress');
    if (typeof conf_tinymce !== 'undefined' && conf_tinymce === 'true') {
        tinyMCE.triggerSave();
    }
    
    $(form).ajaxSubmit({
        beforeSubmit: function(a, f, o) {
            o.dataType = 'json';
        },
        success: function(res) {

            console.log('------------');
            console.log(res);
            console.log('------------');

            var message = '';
            var icon = 'icon-remove-sign';
            var color = '#ac193d;';
            var content_id = res[3];

            if (res[0]) {
                icon = 'icon-ok-sign';
                color = '#0072c6;';
            }

            message += '<div class="box-title" style="color:' + color + '"><i class="' + icon + '"></i> ' + res[1] + '</div>';
            message += res[2];
            
            disabled_html(disabled_list, false);
            if (res[0]==false) {
                bootbox.alert(message, function() {
                    if (isValidURL(content_id)) {
                        window.location = content_id;
                    } else {
                        if (typeof content_id !== 'undefined' && content_id !== '') {
                            var patt = /^#/;

                            if (patt.test(content_id)) {

                                if (window.status_modal) {
                                    var form_content_id = window.form_content_modal;

                                    if (typeof content !== 'undefined')
                                        form_content_id = content;

                                    close_form_modal('', form_content_id);
                                } else {
                                    close_form();
                                }

                                load_table(content_id, 1);
                            } else {

                                if (window.status_modal) {
                                    var form_content_id = window.form_content_modal;

                                    if (typeof content !== 'undefined')
                                        form_content_id = content;

                                    close_form_modal('', form_content_id);
                                } else {
                                    close_form();
                                }
                                console.log("eval : "+content_id);
                                eval('(' + content_id + ')');
                            }
                        }
                    }
                });
            } else {
                // div modal yang akan di-hide setelah transaksi berhasil
                var div_modal = '#'+ $('#'+button).closest('div.modal').attr('id');
                $(div_modal).modal('hide');
                eval(content_id);
                if (isValidURL(content_id)) {
                    //window.location = content_id;
                } else {

                    if (typeof content_id !== 'undefined' && content_id !== '') {
                        var patt = /^#/;
                        if (patt.test(content_id)) {
                            if (window.status_modal) {
                                var form_content_id = window.form_content_modal;

                                if (typeof content !== 'undefined')
                                    form_content_id = content;

                                close_form_modal('', form_content_id);
                            } else {
                                close_form();
                            }

                            load_table(content_id, 1);
                        } else {
                            /*if (window.status_modal) {
                                var form_content_id = window.form_content_modal;

                                if (typeof content !== 'undefined')
                                    form_content_id = content;
                                
                                close_form_modal('', form_content_id);
                            } else {
                                close_form();
                            }*/
                            
                        }
                    } else {
                        
                        // jika tidak ada callback

                    }
                }

                if(!res[1])
                    res[1] = 'Success';
                if(!res[2])
                    res[2] = 'Save Process Success';
                toastr['success'](res[2],res[1]);

                // Call callback function
                if(callback!==undefined)
                    callback();
            }

        },
        error: function(){
            toastr['error']('Server Error', 'Error');
            disabled_html(disabled_list, false);
        },
        done: function(){
        }

    });
}

/*
    Yang digunakan dalam Aplikasi zone2000 ini masih sebatas ini
    

*/




function save_purchase_order(id, page, callback) {
    var content = '&nbsp;';
    var link = $(id).attr('data-source');
    var data = {};
    var temp_id = $(id).attr('id');
    var filter = $(id).attr('data-filter');
    var pagination = $(id).attr('pagination');
    var st_paging = 'true';


    if (typeof filter !== 'undefined') {
        data = $(filter).serialize();
        tools_filter(filter, id, callback);
    }


    if (typeof pagination !== 'undefined') {
        st_paging = pagination;
    }

    $(id).html(content).addClass('loading-progress');
    $.post(link + '/' + page, data, function(res) {
        content = res.table;
        content += '<div id="pp-' + temp_id + '"></div>';
        $(id).removeClass('loading-progress').html(content);

        if (st_paging === 'true') {
            $('#pp-' + temp_id).pagination({
                pageNumber: page,
                total: res.total,
                pageSize: res.limit,
                onSelectPage: function(pageNumber) {
                    load_table(id, pageNumber, callback, keyword);
                },
                //sembunyikan pagelist pagintion easyui
                showPageList: false
            });
        }

        if (typeof callback !== 'undefined') {
            callback();
        }

    }, 'json');
}

function edit_purchase_order(id,link2, page, callback) {
    var content = '&nbsp;';
    var button = '.'+link2;
    var link = 'referensi_purchase_order/edit_detail_purchase_order';
    var data = {};
    var temp_id = $(id).attr('id');
    var filter = $(id).attr('data-filter');
    var pagination = $(id).attr('pagination');
    var st_paging = 'true';


    if (typeof filter !== 'undefined') {
        data = $(filter).serialize();
        tools_filter(filter, id, callback);
    }


    if (typeof pagination !== 'undefined') {
        st_paging = pagination;
    }

    $(id).html(content).addClass('loading-progress');
    $.post(link + '/' + page, data, function(res) {
        content = res.table;
        content += '<div id="pp-' + temp_id + '"></div>';
        $(id).removeClass('loading-progress').html(content);

        if (st_paging === 'true') {
            $('#pp-' + temp_id).pagination({
                pageNumber: page,
                total: res.total,
                pageSize: res.limit,
                onSelectPage: function(pageNumber) {
                    load_table(id, pageNumber, callback, keyword);
                },
                //sembunyikan pagelist pagintion easyui
                showPageList: false
            });
        }

        if (typeof callback !== 'undefined') {
            callback();
        }

    }, 'json');
}


/*
 * Class yang digunakan class="modal fade modal-xlarge"
 */
function _do_print(id, filter) {
    var form_filter = $(filter);
    var source = $('#' + id).attr('data-source');

    form_filter.attr('action', source);
    form_filter.attr('target', '_blank');
    form_filter.submit();
    form_filter.attr('action', '');
}



/*
 * Class yang digunakan class="well-content"
 */
function load_form(button, content) {
    window.status_modal = false;
    var id = '#' + button;
    if (typeof $(id).attr('disabled') === 'undefined') {
        var source = $(id).attr('data-source');
        disabled_html([id], true);

        var content_id = window.form_content_modal;
        if (typeof content !== 'undefined') {
            content_id = content;
        }

        $('#index-content').hide();
        $(content_id).show().load(source, function() {
            disabled_html([id], false);
        });
    }
}

function close_form(button, content, main, confirm) {
    if (typeof $('#' + button).attr('disabled') === 'undefined') {

        var id_content = window.form_content_modal;
        var id_main = window.form_content_main;

        if (typeof content !== 'undefined')
            id_content = content;
        if (typeof main !== 'undefined')
            id_main = main;
        if (typeof confirm !== 'undefined') {

            bootbox.setBtnClasses({
                CANCEL: '',
                CONFIRM: 'blue'
            });

            bootbox.confirm("Anda yakin akan menutup halaman ini?", "Tidak", "Ya", function(e) {
                if (e) {
                    $(id_main).show();
                    $(id_content).hide().html('&nbsp;');
                }
            });

        }
        else {
            $(id_main).show();
            $(id_content).hide().html('&nbsp;');
        }

    }
}

function close_form_modal(button, content) {
    if (typeof $('#' + button).attr('disabled') === 'undefined') {
        var content_id = window.form_content_modal;
        if (typeof content !== 'undefined') {
            content_id = content;
        }
        $(content_id).modal('hide');
        window.status_modal = false;
    }
}

/*
 * Fungsi untuk mendisabled tag HTML
 * Parameter :
 *      1. id, diisi dengan id tag HTML
 *         Contoh : #idtaghtml
 *      2. status, (boolean)
 *         Contoh : true/false (Jika true maka tag HTML akan didisabeld)
 */

function disabled_html(id, status) {
    $(id).each(function(idx, val) {
        if (status) {
            $(val).attr('disabled', 'disabled');
        } else {
            $(val).removeAttr('disabled');
        }
    });
}

/*
 * Funngsi untuk menghapus data row
 * Parameter :
 *      1. id, diisi dengan id anchor/button
 *         Contoh : karena fungsi ini disimpan pada action onclick anchor/button, jadi untuk menggunakan
 *                  funsi ini yaitu delete_row(this.id)
 * Deskripsi :
 *      1. link, diambil dari attribut data-source anchor/button
 *         Contoh : <a href="javascript:void(0)" id="idtarget" data-source="http://localhost/namacontroler/fungsidelete"> .... </a>
 */
function delete_row(id) {
    var temp_id = '#' + id;
    var link = $(temp_id).attr('data-source');
    bootbox.setBtnClasses({
        CANCEL: '',
        CONFIRM: 'red'
    });
    bootbox.confirm('Anda yakin akan menghapus data?', "Tidak", "Ya", function(e) {
        if (e) {
            disabled_html([temp_id], true);
            bootbox.modal('<div class="loading-progress"></div>');
            $.post(link, function(res) {
                var message = '';
                var icon = 'icon-remove-sign';
                var color = '#ac193d;';
                var content_id = res[3];

                if (res[0]) {
                    icon = 'icon-ok-sign';
                    color = '#0072c6;';
                }

                message += '<div class="box-title" style="color:' + color + '"><i class="' + icon + '"></i> ' + res[1] + '</div>';
                message += res[2];

                $(".bootbox").modal("hide");
                disabled_html(temp_id, false);
                bootbox.alert(message, function() {
                    if (isValidURL(content_id)) {
                        window.location = content_id;
                    } else {
                        if (typeof content_id !== 'undefined' && content_id !== '') {
                            var patt = /^#/;

                            if (patt.test(content_id)) {

                                if (window.status_modal) {
                                    var form_content_id = window.form_content_modal;

                                    if (typeof content !== 'undefined')
                                        form_content_id = content;

                                    close_form_modal('', form_content_id);
                                } else {
                                    close_form();
                                }

                                load_table(content_id, 1);
                            } else {

                                if (window.status_modal) {
                                    var form_content_id = window.form_content_modal;

                                    if (typeof content !== 'undefined')
                                        form_content_id = content;

                                    close_form_modal('', form_content_id);
                                } else {
                                    close_form();
                                }

                                eval('(' + content_id + ')');
                            }
                        }
                    }
                });
            }, 'json');
        }
    });
}

function confirm_dialog_ajax(id, message_conf) {
    var temp_id = '#' + id;
    var link = $(temp_id).attr('data-source');
    var msg = $(temp_id).attr('data-message');
    
    if (typeof message_conf === 'undefined') {
        message_conf = msg;
    }
    
    bootbox.setBtnClasses({
        CANCEL: '',
        CONFIRM: 'red'
    });
    bootbox.confirm(message_conf, "Tidak", "Ya", function(e) {
        if (e) {
            disabled_html([temp_id], true);
            bootbox.modal('<div class="loading-progress"></div>');
            $.post(link, function(res) {
                var message = '';
                var icon = 'icon-remove-sign';
                var color = '#ac193d;';
                var content_id = res[3];

                if (res[0]) {
                    icon = 'icon-ok-sign';
                    color = '#0072c6;';
                }

                message += '<div class="box-title" style="color:' + color + '"><i class="' + icon + '"></i> ' + res[1] + '</div>';
                message += res[2];

                $(".bootbox").modal("hide");
                disabled_html(temp_id, false);
                bootbox.alert(message, function() {
                    if (isValidURL(content_id)) {
                        window.location = content_id;
                    } else {
                        if (typeof content_id !== 'undefined' && content_id !== '') {
                            var patt = /^#/;

                            if (patt.test(content_id)) {

                                if (window.status_modal) {
                                    var form_content_id = window.form_content_modal;

                                    if (typeof content !== 'undefined')
                                        form_content_id = content;

                                    close_form_modal('', form_content_id);
                                } else {
                                    close_form();
                                }

                                load_table(content_id, 1);
                            } else {

                                if (window.status_modal) {
                                    var form_content_id = window.form_content_modal;

                                    if (typeof content !== 'undefined')
                                        form_content_id = content;

                                    close_form_modal('', form_content_id);
                                } else {
                                    close_form();
                                }

                                eval('(' + content_id + ')');
                            }
                        }
                    }
                });
            }, 'json');
        }
    });
}

/* added by Nisa
 * Funngsi untuk mengapprove data row
 * Parameter :
 *      1. id, diisi dengan id anchor/button
 *         Contoh : karena fungsi ini disimpan pada action onclick anchor/button, jadi untuk menggunakan
 *                  funsi ini yaitu delete_row(this.id)
 * Deskripsi :
 *      1. link, diambil dari attribut data-source anchor/button
 *         Contoh : <a href="javascript:void(0)" id="idtarget" data-source="http://localhost/namacontroler/fungsidelete"> .... </a>
 */
function approve_row(id) {
    var temp_id = '#' + id;
    var link = $(temp_id).attr('data-source');
    bootbox.setBtnClasses({
        CANCEL: '',
        CONFIRM: 'red'
    });
    bootbox.confirm('Anda yakin akan menyetujui data ini?', "Tidak", "Ya", function(e) {
        if (e) {
            disabled_html([temp_id], true);
            bootbox.modal('<div class="loading-progress"></div>');
            $.post(link, function(res) {
                var message = '';
                var icon = 'icon-check';
                var color = '#ac193d;';
                var content_id = res[3];

                if (res[0]) {
                    icon = 'icon-ok-sign';
                    color = '#0072c6;';
                }

                message += '<div class="box-title" style="color:' + color + '"><i class="' + icon + '"></i> ' + res[1] + '</div>';
                message += res[2];

                $(".bootbox").modal("hide");
                disabled_html(temp_id, false);
                bootbox.alert(message, function() {
                    if (typeof content_id !== 'undefined' && content_id !== '') {
                        load_table(content_id, 1);
                    }
                });
            }, 'json');
        }
    });
}

/* added by Nisa
 * Funngsi untuk me-reject data row
 * Parameter :
 *      1. id, diisi dengan id anchor/button
 *         Contoh : karena fungsi ini disimpan pada action onclick anchor/button, jadi untuk menggunakan
 *                  funsi ini yaitu delete_row(this.id)
 * Deskripsi :
 *      1. link, diambil dari attribut data-source anchor/button
 *         Contoh : <a href="javascript:void(0)" id="idtarget" data-source="http://localhost/namacontroler/fungsidelete"> .... </a>
 */
function reject_row(id) {
    var temp_id = '#' + id;
    var link = $(temp_id).attr('data-source');
    bootbox.setBtnClasses({
        CANCEL: '',
        CONFIRM: 'red'
    });
    bootbox.confirm('Anda yakin tidak akan menyetujui data ini?', "Tidak", "Ya", function(e) {
        if (e) {
            disabled_html([temp_id], true);
            bootbox.modal('<div class="loading-progress"></div>');
            $.post(link, function(res) {
                var message = '';
                var icon = 'icon-check';
                var color = '#ac193d;';
                var content_id = res[3];

                if (res[0]) {
                    icon = 'icon-ok-sign';
                    color = '#0072c6;';
                }

                message += '<div class="box-title" style="color:' + color + '"><i class="' + icon + '"></i> ' + res[1] + '</div>';
                message += res[2];

                $(".bootbox").modal("hide");
                disabled_html(temp_id, false);
                bootbox.alert(message, function() {
                    if (typeof content_id !== 'undefined' && content_id !== '') {
                        load_table(content_id, 1);
                    }
                });
            }, 'json');
        }
    });
}


/*
 * Fungsi untuk mengambil data option dengan ajax 
 * Parameter :
 *      1. target, diisi dengan ID select 
 *         Contoh : #idtaget
 *      2. data, diisi dengan data filter yang berupa object
 *         Contoh : {kode : '1', selected : '12'}
 *      3. chosen, diisi dengan status bootsrap chosen (boolean)
 *         Contoh : true/false (Jika true berarti select mengganan bootsrap chosen)
 * Deskripsi :
 *      1. link, diambil dari attribut data-source target
 *         Contoh : <select id="idtarget" data-source="http://localhost/namacontroler/get_data_option"> .... </select>
 */
function get_options(target, data, chosen, type) {
    var st_chosen = false;

    if (typeof chosen !== 'undefined')
        st_chosen = chosen;

    var link = $(target).attr('data-source');
    $.post(link, data, function(res) {
        if (typeof type === 'undefined' || type === 'options') {
            $(target).html(res);
            if (st_chosen)
                $(target).trigger('liszt:updated');
        } else {
            $(target).val(res);
        }
    }, 'json');
}


function drildown(key, refresh, table) {
    if (key === '')
        alert('Primary key tidak terdefinisi');
    else {
        var id;
        var source;
        var $drildown;

        if (typeof refresh !== '' && refresh === true) {
            id = $('#' + window.drildown_key + table + '_' + key).attr('rel');
            source = $('#' + window.drildown_key + table + '_' + key).attr('data-source');

            $drildown = $('#drildown_row_' + table + '_' + id);

            $drildown.attr('status', 'open');
            $drildown.children('td').html('<div id="drildown_content_' + table + '_' + id + '"><div class="loading-progress"></div></div>');
            $('#drildown_content_' + table + '_' + id).load(source, {id: id});
        } else {
            id = $('#' + key).attr('rel');
            source = $('#' + key).attr('data-source');
            table = $('#' + key).attr('parent');

            $drildown = $('#drildown_row_' + table + '_' + id);
            var st_toggle = $drildown.attr('status');
            $drildown.toggle();
            if (st_toggle === 'close') {
                $drildown.attr('status', 'open');
                $drildown.children('td').html('<div id="drildown_content_' + table + '_' + id + '"><div class="loading-progress"></div></div>');
                $('#drildown_content_' + table + '_' + id).load(source, {id: id});
            } else {
                $drildown.attr('status', 'close');
            }
        }
    }
}

function select_all(id) {
    var target = $('#' + id).attr('target-selected');
    if ($('#' + id).is(':checked')) {
        $('.' + target).prop('checked', true);
    } else {
        $('.' + target).prop('checked', false);
    }
}

function isValidURL(url) {
    var RegExp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;

    if (RegExp.test(url)) {
        return true;
    } else {
        return false;
    }
}

function config_tabs_ajax(id, init) {
    var default_tab = $(id).children('li.active').children('a');
    var default_content = default_tab.attr('href');
    var default_url = default_tab.attr('data-source');

    $(default_content).html('<div class="loading-progress"></div>').load(default_url, function() {
        $(id).tab();
    });

    if (typeof init === 'undefined' || init === true) {
        $(id).bind('show', function(e) {
            var pattern = /#.+/gi
            var contentID = e.target.toString().match(pattern)[0];
            var url = $('a[href=' + contentID + ']').attr('data-source');

            $(contentID).html('<div class="loading-progress"></div>').load(url, function() {
                $(id).tab();
            });
        });
    }
}

/* Org Cart */
function Setup(selector) {
    var config = GetOrgDiagramConfig();
//    config.templates = [getCursorTemplate()];
    config.cursorItem = 0;
    config.templates = [getContactTemplate()];
    config.onItemRender = onTemplateRender;
    config.defaultTemplateName = "customControlTemplate";

    return selector.orgDiagram(config);
}

function Update(selector, updateMode) {
    selector.orgDiagram("option", GetOrgDiagramConfig());
    selector.orgDiagram("update", updateMode);
}

function GetOrgDiagramConfig() {

    return {
        normalLevelShift: 20,
        dotLevelShift: 10,
        lineLevelShift: 10,
        normalItemsInterval: 20,
        dotItemsInterval: 15,
        lineItemsInterval: 5,
        hasSelectorCheckbox: primitives.common.Enabled.False,
        leavesPlacementType: primitives.common.ChildrenPlacementType.Matrix,
        hasButtons: primitives.common.Enabled.False,
        itemTitleFirstFontColor: primitives.common.Colors.White,
        itemTitleSecondFontColor: primitives.common.Colors.White,
        labelSize: new primitives.common.Size(10, 20),
        labelPlacement: primitives.common.PlacementType.Top,
        labelOffset: 3
    };
}

function getCursorTemplate() {
    var result = new primitives.orgdiagram.TemplateConfig();
    result.name = "CursorTemplate";

    result.itemSize = new primitives.common.Size(120, 100);
    result.minimizedItemSize = new primitives.common.Size(3, 3);
    result.highlightPadding = new primitives.common.Thickness(2, 2, 2, 2);
    result.cursorPadding = new primitives.common.Thickness(3, 3, 50, 8);

    var cursorTemplate = jQuery("<div></div>")
            .css({
                position: "absolute",
                overflow: "hidden",
                width: (result.itemSize.width + result.cursorPadding.left + result.cursorPadding.right) + "px",
                height: (result.itemSize.height + result.cursorPadding.top + result.cursorPadding.bottom) + "px"
            });

    var cursorBorder = jQuery("<div></div>")
            .css({
                width: (result.itemSize.width + result.cursorPadding.left + 1) + "px",
                height: (result.itemSize.height + result.cursorPadding.top + 1) + "px"
            }).addClass("bp-item bp-corner-all bp-cursor-frame");
    cursorTemplate.append(cursorBorder);

    var bootStrapVerticalButtonsGroup = jQuery("<div></div>")
            .css({
                position: "absolute",
                overflow: "hidden",
                top: result.cursorPadding.top + "px",
                left: (result.itemSize.width + result.cursorPadding.left + 10) + "px",
                width: "35px",
                height: (result.itemSize.height + 1) + "px"
            }).addClass("btn-group btn-group-vertical");

    bootStrapVerticalButtonsGroup.append('<button class="btn btn-small" data-buttonname="info" type="button"><i class="icon-info-sign"></i></button>');
    bootStrapVerticalButtonsGroup.append('<button class="btn btn-small" data-buttonname="edit" type="button"><i class="icon-edit"></i></button>');
    bootStrapVerticalButtonsGroup.append('<button class="btn btn-small" data-buttonname="remove" type="button"><i class="icon-remove"></i></button>');
    bootStrapVerticalButtonsGroup.append('<button class="btn btn-small" data-buttonname="user" type="button"><i class="icon-user"></i></button>');

    cursorTemplate.append(bootStrapVerticalButtonsGroup);

    result.cursorTemplate = cursorTemplate.wrap('<div>').parent().html();

    return result;
}

function LoadItems(selector) {

    var items = [];
    var link = selector.attr('data-source');
    $.get(link, {}, function(out) {
        $.each(out, function(idx, val) {
            items.push(new primitives.orgdiagram.ItemConfig(val));
        });

        selector.orgDiagram("option", {
            items: items,
            cursorItem: 0
        });
        selector.orgDiagram("update");
    }, 'json');


}

function getContactTemplate() {
    var result = new primitives.orgdiagram.TemplateConfig();
    result.name = "customControlTemplate";

    result.itemSize = new primitives.common.Size(174, 83);
    result.minimizedItemSize = new primitives.common.Size(3, 3);
    result.highlightPadding = new primitives.common.Thickness(2, 2, 2, 2);


    var itemTemplate = jQuery(
            '<div class="bp-item">'
            + '<div name="title" class="bp-item" style="top: 3px; left: 6px; width: 162px; height: 55px; text-align:center;  line-height:14px; font-weight:bold;border-bottom:1px dotted #000;"></div>'
            + '<div name="description" class="bp-item" style="top: 33px; left: 6px; width: 162px; height: 35px; font-size: 10px; line-height:12px; text-align:center;" ></div>'
            + '<div name="kuota" class="bp-item" style="top: 65px; left: 6px; width: 162px; height: 35px; font-size: 12px; line-height:12px; text-align:left;" ></div>'
            + '<div name="total" class="bp-item" style="top: 65px; left: 6px; width: 162px; height: 35px; font-size: 12px; line-height:12px; text-align:right;" ></div>'
            + '</div>'
            ).css({
        width: result.itemSize.width + "px",
        height: result.itemSize.height + "px"
    }).addClass("bp-item bp-corner-all bt-item-frame");
    result.itemTemplate = itemTemplate.wrap('<div>').parent().html();

    return result;
}

function onTemplateRender(event, data) {
    var itemConfig = data.context;

    var fields = ["title", "description", "kuota", "total"];
    for (var index = 0; index < fields.length; index++) {
        var field = fields[index];

        var element = data.element.find("[name=" + field + "]");
        if (element.text() !== itemConfig[field]) {
            element.html(itemConfig[field]);
        }
    }
}

function do_print(id, filter) {
    var form_filter = $(filter);
    var source = $('#' + id).attr('data-source');

    form_filter.attr('action', source);
    form_filter.attr('target', '_blank');
    form_filter.submit();
    form_filter.attr('action', '');
}

function _s2_image(e) {
    return e.id ? "<img style='padding-right:10px;' src='" + e.icon.toLowerCase() + "'/>" + e.text : e.text
}

function select2_image(id) {
    $("#" + id).select2({searchable: false, formatResult: _s2_image, formatSelection: _s2_image, escapeMarkup: function(e) {
            return e;
        }});
}

function _s2_icon(e) {
    return "<i class='" + e.text + "'></i> " + e.text
}

function select2_icon(id) {
    $("#" + id).select2({searchable: false, formatResult: _s2_icon, formatSelection: _s2_icon, escapeMarkup: function(e) {
            return e;
        }});
}

function tooltip() {
    var tooltips = function() {
        $('a.btn-tooltip').mouseenter(function() {
            $(this).tooltip('show');
        }).mouseleave(function() {
            $(this).tooltip('hide');
        });
    };
    return tooltips;
}

function redirect(url) {
    if (typeof url === 'undefined')
        url = '';
    window.location = url;
}

function show_modal(url) {
    $.post(url, {}, function(out) {
        bootbox.setBtnClasses({
            CANCEL: '',
            CONFIRM: 'blue'
        });

        if (out[0]) {
            bootbox.confirm('Password anda sudah kadaluarsa, silahkan klik tombol <b>YA</b> untuk mengganti password!', "Tidak", "Ya", function(e) {
                //bootbox.alert('Password anda sudah kadaluarsa, silahkan klik tombol <b>YA</b> jika Anda ingin mengganti passwod sekarang', function() {
                if (e) {
                    redirect(out[1]);
                }
            });
        }
    }, 'json');
}

function reset_filter(id, st_table) {
    var btn_id = '#' + id;
    var frm = $(btn_id).attr('data-filter');
    var table = $(btn_id).attr('data-table');
    var cb = $(btn_id).attr('data-callback');

    $('#' + frm)[0].reset();
    $("select").trigger("liszt:updated");

    var callback = function() {
    };
    if (cb !== '') {
        eval('(callback = window.' + cb + ')');
    }
    if (st_table)
        load_table(table, 1, callback, keyword);
    else {
        callback();
    }

//		alert(cb);
}

function tools_filter(id, table, callback) {
    var frm_id = id.replace(/#/g, "");

    var cb = '';
    if (typeof callback !== 'undefined') {
        cb = 'callback_' + frm_id;
        eval('(window.' + cb + ' = callback)');
    }
    var st_table = true;
    if (table === '')
        st_table = false;

    var t = '';
    t += '<div id="box-message-' + frm_id + '" class="box-message-filter" style="display: none;">';
    t += '  Untuk menampilkan filter data silahkan klik tombol <strong>Show Filter</strong> !';
    t += '</div>';
    t += '<div id="button-' + frm_id + '" class="box-filter-button">';
    t += '  <a href="javascript:void(0);" id="button-reset-' + frm_id + '" class="freset" data-filter="' + frm_id + '" data-table="' + table + '" data-callback="' + cb + '" onclick="reset_filter(this.id,' + st_table + ')"><i class="icon-undo" ></i> Reset Filter</a>';
    t += '  <a href="javascript:void(0);" id="button-showhide-' + frm_id + '" class="fhide" data-filter="' + frm_id + '" onclick="fshowhide(this.id)"><i class="icon-circle-arrow-up"></i> Hide Filter</a>';
    t += '</div>';

    var tools_status = $('#' + frm_id).attr('tools-filter');
    if (typeof tools_status === 'undefined' || tools_status === 'true') {
        $('#box-message-' + frm_id).remove();
        $('#button-' + frm_id).remove();
        $('#' + frm_id).after(t);
    }
}

function fshowhide(id) {
    var btn_id = '#' + id;
    var cls = $(btn_id).attr('class');
    var frm = $(btn_id).attr('data-filter');
    var box_message = 'box-message-' + frm;
    var btn_reset = 'button-reset-' + frm;
    if (cls === 'fhide') {
        $(btn_id).attr('class', 'fshow');
        $(btn_id).html('<i class="icon-circle-arrow-down"></i> Show Filter');
        $('#' + frm).hide();
        $('#' + btn_reset).hide();
        $('#' + box_message).show();
    } else {
        $(btn_id).attr('class', 'fhide');
        $(btn_id).html('<i class="icon-circle-arrow-up"></i> Hide Filter');
        $('#' + frm).show();
        $('#' + btn_reset).show();
        $('#' + box_message).hide();
    }
}
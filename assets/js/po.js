function refreshTotalPo(table){  
        
    if (table==undefined){
        $('.table-po-detail').each(function(i,e){

            refreshTotalPo(e);
        });
        return;
    }

    table = $(table);

    var total = 0;
    var sub_total = 0;
    var sub_total_temp = 0;
    $('.qty-approved-pax', table).each(function(i, el){
        var price = $(el).closest('tr').find('.sup-price-pax').unmask();
        var qty = $(el).closest('tr').find('.qty-approved-pax').unmask();
        var reg_diskon = $(el).closest('tr').find('.regular-diskon').unmask();
        var additional_diskon = $(el).closest('tr').find('.additional-diskon').unmask();
        //var qty   = $(el).unmask();
        //sub_total = ((price-reg_diskon-additional_diskon)*qty);
        sub_total_temp = (price*qty)/100;
        sub_total1 = sub_total_temp - (sub_total_temp * reg_diskon / 10000);
        sub_total2 = sub_total1 - (sub_total1 * additional_diskon / 10000);
        total = total + sub_total2;
        var subTotal = $(el).closest('tr').find('.sub-total');
        subTotal.html(sub_total).formatNumber();
    });

    console.log(' : ' +total);
    var spanTotal = $('.table-footer-total', table).find('.format-number');
    spanTotal.html(total).formatNumber();

    refreshGrandTotalPo();
}



function refreshGrandTotalPo(){
    var total = 0;
    $('.table-footer-total', $('.table-po-detail')).each(function(i,e){
        total+= Number($(e).find('.format-number').unmask());
    });
    $('.grand-total').html(total).formatNumber();
}

//==================po sc=======================
function refreshTotalPoSc(table){  
        
    if (table==undefined){
        $('.table-po-detail').each(function(i,e){

            refreshTotalPoSc(e);
        });
        return;
    }

    table = $(table);

    var total = 0;
    var sub_total = 0;
    var total_pp = 1;
    var index = 0;
    var new_qty = 0;

    //total_pp[0] = 0;
    $('.qty-approved-sc', table).each(function(i, el){
        var price = $(el).closest('tr').find('.po-price').unmask();
        var reg_diskon = $(el).closest('tr').find('.reg-diskon').unmask();
        var additional_diskon = $(el).closest('tr').find('.additional-diskon-sc').unmask();

        //$(element).val($(element).unmask());
        //$(element).val($(element)) = $(element).val($(element)).split('.').join('') ;
       // $(element).val($(element).val().split('.').join('').replace(/\,/g, '.'));
        //$(element).val($(element).val().split('.').join('').replace(/\,/g, '.'));

        var qty = $(el).closest('tr').find('.qty-approved-sc').unmask();
        //sub_total = (price*qty);
        sub_total_temp = (price*qty)/100;
        sub_total1 = sub_total_temp - (sub_total_temp * reg_diskon / 10000);
        sub_total2 = sub_total1 - (sub_total1 * additional_diskon / 10000);
        total = total + sub_total2;
        var subTotal = $(el).closest('tr').find('.sub-total');
        subTotal.html(sub_total).formatNumber();

        var item_id = $(this).attr('pp-item');
        var temp_pp = {item_id:item_id,qty:qty};
        if(index != item_id){
            //total_pp[i] = {item_id:item_id,qty:qty};
            index = item_id;
            new_qty = qty;
            //total_pp[0] = temp_pp;
            console.log(i + '/' + qty + 1);
            $('#total_pp_'+item_id).html(qty);
        }else{
            new_qty = parseInt(new_qty,10) + parseInt(qty,10);
            //total_pp[i] = {item_id:item_id,qty:new_qty}
            console.log(i + '/' + new_qty + 3);
            $('#total_pp_'+item_id).html(new_qty);

        }
    });
    
    console.log(' : ' +total);
    var spanTotal = $('.table-footer-total', table).find('.format-number');
    spanTotal.html(total).formatNumber();
   // $('#total_pp_11759').html(1212);

    refreshGrandTotalPoSc();
}

function refreshGrandTotalPoSc(){
    var total = 0;
    $('.table-footer-total', $('.table-po-detail')).each(function(i,e){
        total+= Number($(e).find('.format-number').unmask());
    });
    $('.grand-total').html(total).formatNumber();
}

$(document).ready(function(){

    $('.btn-draft-save').live('click', function(){
        var rows = [];
        $('.qty-approved').each(function() {
            rows.push({
                prdetail_id : $(this).attr('prdetail-id'),
                qty_approved : $(this).val(),
            });
        });
        $('.qty-approved-pax').each(function() {
            rows.push({
                prdetail_id : $(this).attr('prdetail-id'),
                qty_approved_pax : $(this).val(),
            });
        });
        $('.qty-for-po').each(function() {
            rows.push({
                prdetail_id  : $(this).attr('prdetail-id'),
                qty_for_po   : $(this).val(),
            });
        });
        $('.additional-diskon').each(function() {
            rows.push({
                prdetail_id : $(this).attr('prdetail-id'),
                additional_diskon : $(this).val(),
            });
        });
        $('.sup-price-pax').each(function() {
            rows.push({
                prdetail_id : $(this).attr('prdetail-id'),
                sup_price_pax : $(this).val(),
            });
        });
        $('.reg-diskon').each(function() {
            rows.push({
                prdetail_id : $(this).attr('prdetail-id'),
                reg_diskon : $(this).val(),
            });
        });
        var act = $(this).attr('act');
        var param = {
            data : rows,
            act : act,
        }
        pageSave($(this).attr('url-action'), param);
    });

	$('.btn-supplier-save').live('click', function(){
		var rows = [];
		$('.po-price').each(function() {
			var item_id = $(this).attr('item-id');
			var supplier_id = $(this).attr('supplier-id');
			console.log("supid: "+supplier_id);
        	rows.push({
        		price : $(this).val(),
        		item_id : item_id,
        		qty : $(this).attr('qty'),
        		supplier_id : $('#po-supplier-'+supplier_id).val(),
        	});
        });
		
		console.log(rows);

		var act = $(this).attr('act');
        var param = {
            data : rows,
            act : act,
        }
        pageSave($(this).attr('url-action'), param);
	});

    $('.btn-detail-save').live('click', function(){
        var rows = [];
        $('.po-code').each(function() {
            rows.push({
                po_id       : $(this).attr('po-id'),
                po_code     : $(this).val(),
            });
        });

        var rowsQty = [];
        $('.qty-approved').each(function() {
            rowsQty.push({
                prdetail_id  : $(this).attr('prdetail-id'),
                podetail_id  : $(this).attr('podetail-id'),
                qty_approved : $(this).val(),
            });
        });
        $('.qty-approved-sc').each(function() {
            rowsQty.push({
                prdetail_id  : $(this).attr('prdetail-id'),
                podetail_id  : $(this).attr('podetail-id'),
                qty_approved : $(this).val(),
            });
        });
        $('.qty-for-po').each(function() {
            rowsQty.push({
                prdetail_id  : $(this).attr('prdetail-id'),
                podetail_id  : $(this).attr('podetail-id'),
                qty_for_po   : $(this).val(),
            });
        });
        $('.qty-approved-pax').each(function() {
            rowsQty.push({
                prdetail_id  : $(this).attr('prdetail-id'),
                podetail_id  : $(this).attr('podetail-id'),
                qty_approved_pax : $(this).val(),    
            });
        });
        $('.additional-diskon').each(function() {
            rowsQty.push({
                prdetail_id  : $(this).attr('prdetail-id'),
                podetail_id  : $(this).attr('podetail-id'),
                additional_diskon : $(this).val(),
            });
        });
        $('.additional-diskon-sc').each(function() {
            rowsQty.push({
                prdetail_id  : $(this).attr('prdetail-id'),
                podetail_id  : $(this).attr('podetail-id'),
                additional_diskon : $(this).val(),
            });
        });
        $('.sup-price-pax').each(function() {
            rowsQty.push({
                prdetail_id : $(this).attr('prdetail-id'),
                podetail_id  : $(this).attr('podetail-id'),
                sup_price_pax : $(this).val(),
            });
        });
        $('.reg-diskon').each(function() {
            rowsQty.push({
                prdetail_id : $(this).attr('prdetail-id'),
                podetail_id  : $(this).attr('podetail-id'),
                reg_diskon : $(this).val(),
            });
        });

        var param = {
            po_code : rows,
            qty  : rowsQty,
        }
        console.log(param);
        pageSave($(this).attr('url-action'), param);
    });

    $('.list-supplier').live('change', function(){
        var param = {
            podetail_id     : $(this).attr('podetail-id'),
            supplier_id     : $(this).val()
        }
        pageSave($(this).attr('data-source'), param);
    });

    $('.qty-approved-pax').live('keyup', function(){
        var table = $(this).closest('.new-custom-table');
        refreshTotalPo(table);
    });

    $('.additional-diskon').live('keyup', function(){
        var table = $(this).closest('.new-custom-table');
        refreshTotalPo(table);
    });

    $('.additional-diskon-sc').live('keyup', function(){
        var table = $(this).closest('.new-custom-table');
        refreshTotalPoSc(table);
    });

    $('.qty-approved-sc').live('keyup', function(){
        var table = $(this).closest('.new-custom-table');
        refreshTotalPoSc(table);
    });

});

function initMenuReordering(){

    $('#table-menu tbody').sortable({
        items : 'tr:not(.unsortable)',
        placeholder: "item-menu-highlight",
        cursor: "move",
        start:function(event, ui){
            startPosition = ui.item.prevAll().length + 1;
            //event.preventDefault();
        },
        update: function(event, ui) {
            endPosition = ui.item.prevAll().length + 1;

            // SEND URUTAN TO SERVER
            var current = $(ui.item[0]);
            var parent  = current.parent();
            var head    = current.prevAll( ".group-head:first" );
            var parent  = current.prevAll( ".parent:last" );
            var before  = current.prev();
            var after   = current.next();
            var group_id = head.attr('group-id');
            var parent_id;

            var param = {};

            if( (before.hasClass('child')&&after.hasClass('child')) || (before.hasClass('parent')&&after.hasClass('child')) ){
                // masukan ke parent itu
                param.parent_id = parent_id = after.attr('parent-id');
                param.group_id  = group_id  = after.attr('group-id');
                group           = after.attr('group');
                
                current.removeClass(current.attr('group')).addClass(group);
                current.attr('group', group);

            } else {

                param.group_id = group_id  = head.attr('group-id');
                group     = 'group-'+group_id;

                current.removeClass(current.attr('group')).addClass(group);
                current.attr('group', group);
            }
            
            var data   = [];
            $('.'+group).each(function(index, element){
                data.push($(element).attr('menu-id'));
            });

            recursiveMove(current, after.attr('depth'));

            // ikut mindah child nya
            var kd_menu = current.attr('menu-id');
            var placeInsert = current;
            $('.menu-child-'+kd_menu).each(function(i, e){
                $(e).insertAfter(placeInsert);
                placeInsert = $(e);
            });

            //pageSaveProcess(url_action_reordering, param);

        }
    });
} 

initMenuReordering();

$('.parent, .group-head').live('click', function(){
    menu_id = $(this).attr('menu-id');
    group   = $(this).attr('group');
    
    if ($(this).hasClass('group-head')) {
        // group-head
        target = $('.menu-child-group-'+menu_id);
    } else {
        // parent
        target = $('.menu-child-'+menu_id);
    }

    if($(this).hasClass('closed')){
        target.show();
        // harusnya yg dibuka yang sebelumnya kebuka
    }else{
        target.hide();
    }

    $(this).toggleClass('closed');
    
    $(this).find('.icon-slide').toggleClass('fa fa-angle-up');
    $(this).find('.icon-slide').toggleClass('fa fa-angle-down');

});

function recursiveMove(current, depth){
    group = 'group-'+current.attr('menu-id');

    indent  = (current.attr('indent')*parseInt(depth)+ parseInt(current.attr('indent-offset')))+'px';
    alert(indent);
    $('.first-column', current).css('padding-left', indent);
    current.attr('depth', depth);

    // $('[group="'+group+'"]').each(function(index, element){
    //     // if($(element).hasClass('parent')){

    //     // }else{
    //         indent  = (current.attr('indent')*depth)+'px';
    //         $('.first-column', current).css('padding-left', indent);
    //         current.attr('depth', depth);
    //     // }
    // });

}

function recursiveMenu(menu){
    menu_id = menu.attr('menu-id');
    depth   = menu.attr('depth');
    group_id= menu.attr('group-id');
    group   = 'group-'+menu_id;
    next    = menu.nextAll('.'+group);
    
    var param = {};
    if (next.length>0){
        param.id = menu_id;
        param.child = [];
        //menu.css('background-color','green');
        next.each(function(index, element){
            param.child.push(recursiveMenu($(element)));
        });    
        return param;
    }else{
        //$(menu).css('background-color', 'red');
        param.id = menu_id;
        return param;
    }
}

$('.btn-save-order').live('click', function(){
    var param = [];
    
    $('.group-head').each(function(key, element){
        //$(element).css('background-color','green');
        param.push(recursiveMenu($(element)));
    });

    param = {
        data : JSON.stringify(param),
    };

    console.log(param);
    pageSaveProcess(url_action_reordering, param);    
});

$(document).ajaxComplete(function() {
    initMenuReordering();
});

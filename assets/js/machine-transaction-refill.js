$('.btn-table-add').live('click', function(){
	var container = $(this).closest('.component-table-data');

	var row 		= $(this).closest('.row-data');
	var list_item 	= $('.list-item', row);
	var record_id 	= list_item.attr('value');


	console.log("RECORD ID "+record_id);
	// GET QTY VALUE
	var qty = [];
	$('.data-input', row).each(function(i, el){
		qty.push($(el).val());
	});

	// CHECK JIKA DATA-RECORD SUDAH ADA record_id diganti record_id
	var row_exist = $('.data-record-'+record_id, container);
	if(row_exist.length){
		//row_exist.effect("highlight", {}, 500);
		//return;
	}

	// EDIT NEW ROW
	var new_row = row.clone();
	new_row.insertBefore(row);
	new_row.addClass('data-record');
	new_row.attr('id','data-record-'+record_id);
	new_row.addClass('data-record-'+record_id);
	new_row.attr('data-id', record_id);

	// SET EDITABLE
	$('.data-qty', new_row).each(function(i, el){
		var dataQty = $(el);
		dataQty.on('shown', function(e, editable) {
		    editable.input.$input.formatNumber();
		});
		
		dataQty.show().html(qty[i]).attr('id','data-qty-'+record_id);
		dataQty.editable({
			'pk' : record_id,
			'inputclass' : 'form-control format-number',
			'mode' : 'inline',
		});
	});

	// DISABLE DROPDOWN
	$('div.bootstrap-select', new_row).remove();
	$('select.list-item', new_row).val(record_id).prop('disabled', true).selectpicker('refresh');

	// BUTTON
	$('.disposable', new_row).hide();
	$('.btn-table-add', new_row).remove();
	$('.btn-table-delete', new_row).show();

	// RESET INPUT ROW
	$('.data-info', row).empty();
	$('.data-input', row).val(0);
});

$('.btn-table-delete').live('click', function(){
	var row = $(this).closest('.row-data').remove();
});

$('.table-data-save').live('click', function(){
	var container = $(this).closest('.component-table-data');
	var param 	= {};
	param.bundle_id	= $('.list-bundle', container).val();
	param.data 		= [];

	// GET ALL PARAMETER FROM TABLE-DATA
	$('.data-param', container).each(function(i, e){
		param[$(e).attr('param-name')] = $(e).val();
	});

	// GET ALL INPUT QTY
	$('.data-record', container).each(function(index, el){
		var tmp= {};
		tmp.item_id = $(el).attr('data-id');

		$('.data-qty', $(el)).each(function(i,e){
			var nm = 'qty'+(i+1);
			tmp[nm]= $(e).unmask();
		});
		
		param.data.push(tmp);
	});

	//REFRESH ALL PAGE
	//location.reload();
	$('.data-record').remove();
	
	console.log(param);
	pageSave($(this).attr('data-source'), param);
	
});

function refreshListTransaction(list) {
	var container = $(list).closest('.component-table-data');
	
	target = $('.data-info', $(list).closest('.row-data'));
	param = {
		item_id : $(list).val(),
		type_id : container.attr('type-id')
	};
	
	refreshList(list, target, param);
}

function refreshListTransactionPayout(list) {
	var container = $(list).closest('.component-table-data');
	
	target = $('#name', $(list).closest('.row-data'));
	param = {
		item_id : $(list).val(),
		type_id : container.attr('type-id')
	};
	refreshListPayout(list, target, param);
}

$(document).ready(function(){
	$('select.list-item').trigger('change');	
})



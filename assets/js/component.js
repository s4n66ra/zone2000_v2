function getTemplate(options){
	if(options == undefined)
		options = {};

	var template = '
	<div class="modal fade" id="component-confirm" tabindex="-1" aria-hidden="true">				
	<div class="portlet light">
		<div class="portlet-title">
			<div class="caption font-purple-plum">
				<i class="icon-speech font-purple-plum"></i>
				<span class="caption-subject bold uppercase">
					'+options.title+'
				</span>
			</div>
			<div class="actions">
				<a class="btn btn-circle btn-icon-only btn-default remove" data-dismiss="modal" title="Fullscreen" href="#">
					<i class="icon-close"></i>
				</a>
			</div>
		</div>

		<div class="portlet-body">
			<div class="row text-center">
				<h3>'+options.body+'</h3>
			</div>

			<div class="row" style="margin-top:20px;">
				<div class="col-md-12" style="text-align:center;">
					<div class="btn-group " style="text-align:center;">
						<a href="javascript://" id="button-back" class="btn btn-default btn-cancel" data-dismiss="modal">
							<i class="fa fa-arrow-left"></i> CANCEL
						</a>
						<a href="javascript://" id="button-save" class="btn green-haze btn-ok" data-dismiss="modal">
							<i class="fa fa-check"></i> CONFIRM
						</a>
					</div>
				</div>
			</div>

		</div>
	</div>
	</div>
	';

	return template;
}

function componentConfirm(options, callback){
	var target = '#component-confirm';

	if(!options.title)
		options.title = 'CONFIRMATION';
	if(!options.body)
		options.body = 'Are You Sure ?';

	$(target).remove();
	var template = getTemplate(options);
	$('body').append(template);

	$(target).modal({
        "backdrop": "static",
        "keyboard": true,
        "show": true,
    });

	$(target).on('click', '.btn-ok', function(){
		callback(true);
	});


}
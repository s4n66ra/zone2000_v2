var subtotal=0;
var arrsubtotal=[];
var urutan = 0;
var total =0;
var hargatiket = 0;
var totkurang = 0;
var urutkurang = 0;


function convertToInteger(str){
	return parseFloat(str.replace(/\./g, ''));
}

$('.btn-table-add').live('click', function(){
	var container = $(this).closest('.component-table-data');

	var row 		= $(this).closest('.row-data');
	var list_item 	= $('.list-item', row);
	var record_id 	= list_item.attr('value');

	var record_id_baru = $('.component-table-data #redemp_list_item_id').val();

	console.log("RECORD ID "+record_id_baru);

	var col3val = 0;
	var col2val = 0;

	col2val = $('.wrapper .format-number', row).html();
	if(col2val !== undefined){
		console.log("COL2 : "+ col2val);
		var col2asli = 0;
		var str = col2val;
		if(col2val.length>4){
			col2asli = convertToInteger(col2val);
			console.log("COL 2 ASLI :"+col2asli);
		}else{
			col2asli = col2val;
		}
		
	}


	$("#subtotal").removeAttr("hidden");



	// GET QTY VALUE
	var qty = [];
	$('.data-input', row).each(function(i, el){
		qty.push($(el).val());
	});


	// CHECK JIKA DATA-RECORD SUDAH ADA record_id diganti record_id_baru
	var col3asli = 0;
	var row_exist = $('.data-record-'+record_id_baru, container);
	if(row_exist.length){
		row_exist.effect("highlight", {}, 500);
		return;
	}else{
		//KALO BELUM ADA DI ROW
		$('input',row).each(function(){
			if($(this).attr("name")=="name"){
				console.log("COL3 : "+$(this).val());

				col3val = $(this).val();
				var tempcol3 = col3val;
				console.log("TEMP COL 3 length :"+tempcol3.length);

				if(tempcol3.length>4){
					var str = ""+tempcol3+"";
					col3asli = convertToInteger(str);
					console.log("TEMP COL 3 BILANGAN : "+Number(col3asli));
				}else{
					col3asli = col3val;
				}

			}
		});
		subtotal = (col2asli*col3asli);
		console.log("SUBTOTAL : "+ subtotal);
		arrsubtotal.push(subtotal);

		total = 0;
		for(i=0;i<arrsubtotal.length;i++){
			total = total + arrsubtotal[i];
		}

		$("#angkatotal").html(total);


		hargatiket = $("#hargatiket").val()
		if(hargatiket){
			hargatiket = convertToInteger(hargatiket);
		}
		$("#angkasisa").html(hargatiket-total);

		var finsisa = $("#angkasisa").html();
		if(Number(finsisa)<0){
			$("#angkasisa").css('background-color','pink');
		}else{
			$("#angkasisa").css('background-color','white');
		}

	}

	//SELALU REFRESH SETELAH ADD
    refreshListTransaction($("#redemp_list_item_id", row));

	// EDIT NEW ROW
	var new_row = row.clone();
	new_row.insertBefore(row);
	new_row.addClass('data-record');
	new_row.attr('id','data-record-'+record_id_baru);
	new_row.addClass('data-record-'+record_id_baru);
	new_row.attr('data-id', record_id_baru);

	new_row.attr('urutan',urutan);
	console.log("URUTAN : "+urutan);
	urutan++;

	// SET EDITABLE
	$('.data-qty', new_row).each(function(i, el){
		var dataQty = $(el);
		dataQty.on('shown', function(e, editable) {
		    editable.input.$input.formatNumber();
		});
		
		dataQty.show().html(qty[i]).attr('id','data-qty-'+record_id_baru);
		dataQty.editable({
			'pk' : record_id_baru,
			'inputclass' : 'form-control format-number',
			'mode' : 'inline',
		});
	});

	// DISABLE DROPDOWN
	$('div.bootstrap-select', new_row).remove();
	$('select.list-item', new_row).val(record_id_baru).prop('disabled', true).selectpicker('refresh');

	// BUTTON
	$('.disposable', new_row).hide();
	$('.btn-table-add', new_row).remove();
	$('.btn-table-delete', new_row).show();

	// RESET INPUT ROW
	$('.data-info', row).empty();
	$('.data-input', row).val(0);
});

$('.btn-table-delete').live('click', function(){
	var row = $(this).closest('.row-data').remove();
	console.log(row.attr('urutan'));

	//HITUNG SUBTOTAL
	urutkurang = row.attr('urutan');
	arrsubtotal[urutkurang]=0;

	totkurang =0;
	for(i=0;i<arrsubtotal.length;i++){
		totkurang = totkurang + arrsubtotal[i];
	}
	console.log("TOT KURANG :"+ totkurang);
	$("#angkatotal").html(totkurang);

	//HITUNG SISA
	hargatiket = $("#hargatiket").val();
	hargatiket = convertToInteger(hargatiket);
	$("#angkasisa").html(hargatiket-totkurang);

	var finsisa = $("#angkasisa").html();
	if(Number(finsisa)<0){
		$("#angkasisa").css('background-color','pink');
	}else{
		$("#angkasisa").css('background-color','white');
	}



});

$('.table-data-save').live('click', function(){
	//RESET ANGKA
	subtotal=0;
	arrsubtotal=[];
	urutan = 0;
	total =0;
	hargatiket = 0;
	totkurang = 0;
	urutkurang = 0;
	$("#angkasisa").html('0');
	$("#hargatiket").html('0');
	$("#angkatotal").html('0');
	//END RESET ANGKA


	var container = $(this).closest('.component-table-data');
	var param 	= {};
	param.bundle_id	= $('.list-bundle', container).val();
	param.data 		= [];

	// GET ALL PARAMETER FROM TABLE-DATA
	$('.data-param', container).each(function(i, e){
		param[$(e).attr('param-name')] = $(e).val();
	});

	// GET ALL INPUT QTY
	$('.data-record', container).each(function(index, el){
		var tmp= {};
		tmp.item_id = $(el).attr('data-id');

		$('.data-qty', $(el)).each(function(i,e){
			var nm = 'qty'+(i+1);
			tmp[nm]= $(e).unmask();
		});
		
		param.data.push(tmp);
	});

	//REFRESH ALL PAGE
	//location.reload();
	$('.data-record').remove();
	
	console.log(param);
	pageSave($(this).attr('data-source'), param);
	
});

function refreshListTransaction(list) {
	var container = $(list).closest('.component-table-data');
	
	target = $('.data-info', $(list).closest('.row-data'));
	param = {
		item_id : $(list).val(),
		type_id : container.attr('type-id')
	};
	
	refreshList(list, target, param);
}

$(document).ready(function(){
	$('select.list-item').trigger('change');

	//HITUNG SISA
	$(document).on('input','#hargatiket',function(){
		var hargatiket = $("#hargatiket").val();
		var angtot = $("#angkatotal").html();
		hargatiket = convertToInteger(hargatiket);
		$("#angkasisa").html(hargatiket-angtot);

		var finsisa = $("#angkasisa").html();
		if(Number(finsisa)<0){
			$("#angkasisa").css('background-color','pink');
		}else{
			$("#angkasisa").css('background-color','white');
		}


	});


})



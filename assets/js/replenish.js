$(document).ready(function(){

    $('.btn-receive-request').live('click', function(){
        
        var rows = [];
        $('.qty-received').each(function() {
            rows.push({
                prdetail_id : $(this).attr('prdetail-id'),
                qty_received : $(this).val(),
            });
        });
        
        var param = {
            data : rows,
        };

        var options = {
            confirm_message : 'Confirmation..'
        };

        pageSave($(this).attr('url-action'), param, options);
    });

    $('.btn-receiving').live('click', function(){
        var url = $(this).attr('data-source');
        var options = {
            body:'Process Receiving Goods ?'
        };
        pageSave(url,{},{confirm:options});
    });

});

/*jQuery.fn.extend({
   formatNumber: function() {
      if(this.val() == 0 ){
         return null;
      }else{
      this.priceFormat({
         prefix: '',
         centsSeparator: ',',
         thousandsSeparator: '.',
         centsLimit : 2
      });
      return this;
   }
   },
});*/

jQuery.fn.extend({
   formatNumber: function() {
      this.priceFormat({
         prefix: '',
         centsSeparator: ',',
         thousandsSeparator: '.',
         centsLimit : 0
      });
      return this;
   },
});
var TableAjax = function () {
    var grid; // object Datatable()
    var test;

    var initPickers = function () {
        //init date pickers
        $('.date-picker').datepicker({
            rtl: Metronic.isRTL(),
            autoclose: true
        });
    }

    return {

        //main function to initiate the module
        init: function (config) {
            initPickers();
            if(config.pagination)
                recordCount = 10;
            else
                recordCount = -1;

            //config.pagination = 1;
            grid = new Datatable(config);
            //grid = new Datatabletabdua(config);

            var options = { // here you can define a typical datatable settings from http://datatables.net/usage/options 

                    // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                    // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js). 
                    // So when dropdowns used the scrollable div should be removed. 
                    //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                    
                    "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.
                    "lengthMenu": [
                        [2, 10, 20, 50, 100, 150, -1],
                        [2, 10, 20, 50, 100, 150, "All"] // change per page values here
                    ],
                    "pageLength": 10, // default record count per page
                    "ajax": {
                        "url": config.url // ajax source
                    },
                    "order": [
                        [1, "asc"]
                    ]// set first column as a default sort by asc
                };

            if(config.numbering==true){
                options.columnDefs = [ {
                        "searchable": false,
                        "orderable": false,
                        "targets": 0
                    }];
            }

            grid.init({
                src: $("#"+config.id),
                onSuccess: function (grid) {
                    // execute some code after table records loaded
                },
                onError: function (grid) {
                    // execute some code on network or other general error  
                },
                onDataLoad: function(grid) {
                    // execute some code on ajax data load
                }, 
                loadingMessage: 'Loading...',
                dataTable: options,
            });

            // handle group actionsubmit button click
            grid.getTableWrapper().on('click', '.table-group-action-submit', function (e) {
                e.preventDefault();
                var action = $(".table-group-action-input", grid.getTableWrapper());
                if (action.val() != "" && grid.getSelectedRowsCount() > 0) {
                    grid.setAjaxParam("customActionType", "group_action");
                    grid.setAjaxParam("customActionName", action.val());
                    grid.setAjaxParam("id", grid.getSelectedRows());

                    // CALLBACK
                    config.callback_group_submit(this, grid);

                } else if (action.val() == "") {
                    Metronic.alert({
                        type: 'danger',
                        icon: 'warning',
                        message: 'Please select an action',
                        container: grid.getTableWrapper(),
                        place: 'prepend'
                    });
                } else if (grid.getSelectedRowsCount() === 0) {
                    Metronic.alert({
                        type: 'danger',
                        icon: 'warning',
                        message: 'No record selected',
                        container: grid.getTableWrapper(),
                        place: 'prepend'
                    });
                }
            });
            
            var griddatatable = grid.getDataTable();

            // handle action refresh
            grid.getTableWrapper().on('click', '.table-action-refresh', function (e) {
                e.preventDefault();
                grid.getDataTable().ajax.reload();
            });


            $('a').each(
                function(){
                    var vhref = $(this).attr('href');
                    if(vhref=='#hist-coin-out'){
                        $(this).click(
                            function(){
                                grid.getDataTable().ajax.reload();
                            }
                        );
                    }

                    if(vhref=='#hist-cutter'){
                        $(this).click(
                            function(){
                                grid.getDataTable().ajax.reload();
                            }
                        );
                    }

                    if(vhref=='#hist-mc-refill'){
                        $(this).click(
                            function(){
                                grid.getDataTable().ajax.reload();
                            }
                        );
                    }

                    if(vhref=='#hist-mc-redemp'){
                        $(this).click(
                            function(){
                                grid.getDataTable().ajax.reload();
                            }
                        );
                    }

                }
            );


        },

        getGrid : function(){
            return grid;
        },

    };

};
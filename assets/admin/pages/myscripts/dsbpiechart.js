var pchart;
var plegend;


$.ajax({
    url:'./dashboard/getMostItemRedemp/',
    dataType:"JSON",
    success:function(data){
        // PIE CHART
        var pchart = new AmCharts.AmPieChart();
        pchart.dataProvider = data;
        pchart.titleField = "item_name";
        pchart.valueField = "jumlah";
        pchart.outlineColor = "#FFFFFF";
        pchart.outlineAlpha = 0.8;
        pchart.outlineThickness = 2;
        // this makes the chart 3D
        pchart.depth3D = 15;
        pchart.angle = 30;

        // WRITE
        pchart.write("pie_laku");
    }
});


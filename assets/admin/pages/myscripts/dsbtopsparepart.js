/*
- Barang paling laku
- Grafik Penukaran Tiket Harian
- Grafik Omset Toko Harian
- Mesin Paling Laku
- Member Paling Aktif

*/

var chart;



function dsbtop10sparepart(kategory, det_kategory, start, end, mode){
    var url='';
    Metronic.blockUI({
        target: "#top10sparepart",
        boxed :true,
        message : "Loading Dashboard...",
        loadingColor : 'blue',
        overlayColor : 'blue'
    });
    if(det_kategory==undefined || start==undefined || end==undefined){
        url = './dashboard/getTop10Sparepart/'+kategory;
    }else{
        url = './dashboard/getTop10Sparepart/'+kategory+'/'+det_kategory+'/'+start+'/'+end;
    }
    
    $.ajax({
    url:url,
    dataType:"JSON",
    success:function(data){
        Metronic.unblockUI("#top10sparepart");

        // SERIAL CHART
        chart = new AmCharts.AmSerialChart();

        chart.dataProvider = data;
        chart.categoryField = "item_name";
        chart.marginRight = 0;
        chart.marginTop = 0;    
        chart.autoMarginOffset = 0;
        // the following two lines makes chart 3D
/*        chart.depth3D = 20;
        chart.angle = 30;
*/        
        chart.rotate=true;

        // AXES
        // category
        var categoryAxis = chart.categoryAxis;
        categoryAxis.labelRotation = 90;
        categoryAxis.dashLength = 5;
        categoryAxis.gridPosition = "start";
        
        categoryAxis.gridCount = data.length;
        categoryAxis.autoGridCount = false;

        // value
        var valueAxis = new AmCharts.ValueAxis();
        valueAxis.title = "Sum Of Value";
        valueAxis.dashLength = 5;
        chart.addValueAxis(valueAxis);

        // GRAPH            
        for(var i=0;i<1;i++){
            var graph = new AmCharts.AmGraph();
            graph.valueField = "jumlah";
            graph.colorField = "#DC5525";
            graph.balloonText = "[[category]]: [[value]]";
            graph.type = "column";
            graph.lineAlpha = 0;
            graph.fillAlphas = 1;

            chart.addGraph(graph);
        }

        // CHECK EMPTY DATA
        AmCharts.checkEmptyData = function (chart) {
            if ( 0 == chart.dataProvider.length ) {
                // set min/max on the value axis
                chart.valueAxes[0].minimum = 0;
                chart.valueAxes[0].maximum = 100;
                
                // add dummy data point
                var dataPoint = {
                    dummyValue: 0
                };
                dataPoint[chart.categoryField] = '';
                chart.dataProvider = [dataPoint];
                
                // add label
                chart.addLabel(0, '50%', 'The Chart contains no data', 'center');
                
                // set opacity of the chart div
                chart.chartDiv.style.opacity = 0.5;
                
                // redraw it
                chart.validateNow();
            }
        }        

        // END CHECK EMPTY DATA
        AmCharts.checkEmptyData(chart);

        // WRITE
        chart.write("top10sparepart");
        $("#top10sparepart a").hide();
    },
    complete:function(){
        if(det_kategory !=''){
            //dsbstatusservice(kategory, det_kategory, start, end, mode);
        }else{
            //dsbstatusservice(kategory);        
        }        
    }
    });


}


var lchart;

var average = 90.4;
function dsbomzet(kategory, det_kategory, start, end, mode){
    var url='';
    Metronic.blockUI({
        target: "#dsbomzet",
        boxed :true,
        message : "Loading Dashboard...",
        loadingColor : 'blue',
        overlayColor : 'blue'
    });


    if(det_kategory==undefined || start==undefined || end==undefined){
        url = './dashboard/getOmzet/'+kategory;
    }else{
        url = './dashboard/getOmzet/'+kategory+'/'+det_kategory+'/'+start+'/'+end+'/'+mode;
    }

    $.ajax({
        url:url,
        dataType:"JSON",
        success:function(data){
            Metronic.unblockUI("#dsbomzet");
            // SERIAL CHART    
            lchart = new AmCharts.AmSerialChart();
            lchart.pathToImages = "http://www.amcharts.com/lib/images/";
            lchart.autoMarginOffset = 5;
            lchart.marginTop = 0;
            lchart.marginRight = 10;    
            lchart.zoomOutButton = {
                backgroundColor: '#000000',
                backgroundAlpha: 0.15
            };
            lchart.dataProvider = data;
            lchart.categoryField = "hari";
            lchart.theme="light";

            // AXES
            // category
            var lcategoryAxis = lchart.categoryAxis;
            lcategoryAxis.parseDates = true; // as our data is date-based, we set parseDates to true
            lcategoryAxis.minPeriod = "DD"; // our data is daily, so we set minPeriod to DD
            lcategoryAxis.dashLength = 1;
            lcategoryAxis.gridAlpha = 0.15;
            lcategoryAxis.axisColor = "#DADADA";

            // value                
            var lvalueAxis = new AmCharts.ValueAxis();
            lvalueAxis.axisColor = "#DADADA";
            lvalueAxis.dashLength = 0;
            lvalueAxis.labelsEnabled = true;
            lvalueAxis.title = "Omzet Value";
            lchart.addValueAxis(lvalueAxis);

            // GUIDE for 100
            var lguide = new AmCharts.Guide();
            lguide.value = 100;
            lguide.lineColor = "#CC0000";
            //lguide.label = "hello";
            lguide.inside = false;
            lguide.lineAlpha = 0;
            lvalueAxis.addGuide(lguide);
          
            // GUIDE for 120
            var lguide = new AmCharts.Guide();
            lguide.value = 120;
            lguide.lineColor = "#CC0000";
            //lguide.label = "Sum";
            //lguide.inside = false;
            lguide.lineAlpha = 0;
            lvalueAxis.addGuide(lguide);


            // GRAPH
            var lgraph = new AmCharts.AmGraph();
            lgraph.fillColorsField="#2498d2";
            lgraph.bullet = "square";
            lgraph.bulletColor = "#000000";
            lgraph.bulletBorderColor = "#00BBCC";
            lgraph.bulletBorderThickness = 2;
            lgraph.bulletSize = 7;
            lgraph.title = "Sum";
            lgraph.valueField = "jumlah";
            lgraph.lineThickness = 2;
            lgraph.lineColor = "#00BBCC";
            lgraph.fillAlphas = 0.3;
            lchart.addGraph(lgraph);

            // CHECK EMPTY DATA
            AmCharts.checkEmptyData = function (chart) {
                if ( 0 == chart.dataProvider.length ) {
                    // set min/max on the value axis
                    chart.valueAxes[0].minimum = 0;
                    chart.valueAxes[0].maximum = 100;
                    
                    // add dummy data point
                    var dataPoint = {
                        dummyValue: 0
                    };
                    dataPoint[chart.categoryField] = '';
                    chart.dataProvider = [dataPoint];
                    
                    // add label
                    chart.addLabel(0, '50%', 'The Chart contains no data', 'center');
                    
                    // set opacity of the chart div
                    chart.chartDiv.style.opacity = 0.5;
                    
                    // redraw it
                    chart.validateNow();
                }
            }        

            // END CHECK EMPTY DATA
            AmCharts.checkEmptyData(lchart);

            // CURSOR
            var lchartCursor = new AmCharts.ChartCursor();
            lchartCursor.cursorPosition = "mouse";
            lchart.addChartCursor(lchartCursor);

            // SCROLLBAR
            var lchartScrollbar = new AmCharts.ChartScrollbar();
            lchart.addChartScrollbar(lchartScrollbar);




            // WRITE
            lchart.write("dsbomzet");
            $("#dsbomzet a").hide();
        },
        complete:function(){
            if(det_kategory !=''){
                dsbsummerchandise(kategory, det_kategory, start, end, mode);
            }else{
                dsbsummerchandise(kategory);        
            }        
        }
    });
    
}

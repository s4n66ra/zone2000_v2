var lchart;
var chart;
var chartData=[];

var average = 90.4;
function dsbstatusservice(kategory, det_kategory, start, end, mode){
    var url='';
    Metronic.blockUI({
        target: "#trendservice",
        boxed :true,
        message : "Loading Dashboard...",
        loadingColor : 'blue',
        overlayColor : 'blue'
    });


    if(det_kategory==undefined || start==undefined || end==undefined){
        url = './dashboard/getStatusService/'+kategory;
    }else{
        url = './dashboard/getStatusService/'+kategory+'/'+det_kategory+'/'+start+'/'+end+'/'+mode;
    }

    $.ajax({
        url:url,
        dataType:"JSON",
        success:function(oridata){
            var data = oridata.data;
            // generate some random data first
            //generateChartData();

            var firstDate = new Date();
            firstDate.setDate(firstDate.getDate() - 50);

            //MENGAMBIL DATE YANG UNIQUE
            var datecollection = [];
            
            if(data.length>0){
                var tempdate = data[0].hari;
                datecollection[0] = data[0].hari;                
            }
            var j = 1;
            for(var k=1; k<data.length;k++){
                if(tempdate != data[k].hari){
                    datecollection[j] = data[k].hari;
                    tempdate = data[k].hari;
                    j++;
                }
            }

            //UNTUK SEMENTARA STATUS BERJUMLAH 13 BUAH, JIKA LEBIH MAKA G HARUS DITAMBAH JUMLAHNYA
            //KOLEKSI SETIAP G1, G2, .. G13
            var finaldata = [];
            var namecollection = [];

            //MENGOLEKSI NAMA DARI STATUS YANG TELAH DIBUAT
            for(var nc=0;nc<oridata.statusidname.length;nc++){
                namecollection[nc] = oridata.statusidname[nc].nama;
            }

            for(var l=0; l<datecollection.length; l++){
                finaldata.push({
                    date:datecollection[l],
                    g1:"",
                    g2:"",
                    g3:"",
                    g4:"",
                    g5:"",
                    g6:"",
                    g7:"",
                    g8:"",
                    g9:"",
                    g10:"",
                    g11:"",
                    g12:"",
                    g13:"",
                });
            }


            for(var m=0; m<data.length; m++){
                for(n=0; n<finaldata.length; n++){
                    if(finaldata[n].date==data[m].hari){
                        //ANGKA YANG DIGUNAKAN DI DALAM CASE SESUAI DENGAN ID STATUS SERVICE DI TABEL M_STATUS_SERVICE_NEW
                        //KALO ADA PERUBAHAN DATA HARUS UBAH ININYA
                        switch(data[m].status_id){
                            case '2' : finaldata[n].g1++;
                            break;
                            case '3' : finaldata[n].g2++;
                            break;
                            case '4' : finaldata[n].g3++;
                            break;
                            case '5' : finaldata[n].g4++;
                            break;
                            case '6' : finaldata[n].g5++;
                            break;
                            case '7' : finaldata[n].g6++;
                            break;
                            case '8' : finaldata[n].g7++;
                            break;
                            case '9' : finaldata[n].g8++;
                            break;
                            case '10' : finaldata[n].g9++;
                            break;
                            case '11' : finaldata[n].g10++;
                            break;
                            case '12' : finaldata[n].g11++;
                            break;
                            case '13' : finaldata[n].g12++;
                            break;
                            case '14' : finaldata[n].g13++;
                            break;
                        }
                    }
                }
            }
            console.log(finaldata);

            chartData=finaldata;
/*            chartData.push({
                date:"2016-01-15",
                g1: 5,
                g2: 9,
            });
*/


            // SERIAL CHART    
            chart = new AmCharts.AmSerialChart();
            chart.marginTop = 0;
            chart.autoMarginOffset = 5;
            chart.pathToImages = "http://www.amcharts.com/lib/images/";
            chart.zoomOutButton = {
                backgroundColor: '#000000',
                backgroundAlpha: 0.15
            };
            chart.dataProvider = chartData;
            chart.categoryField = "date";

            // AXES
            // category                
            var categoryAxis = chart.categoryAxis;
            categoryAxis.parseDates = true; // as our data is date-based, we set parseDates to true
            categoryAxis.minPeriod = "DD"; // our data is daily, so we set minPeriod to DD
            categoryAxis.dashLength = 2;
            categoryAxis.gridAlpha = 0.15;
            categoryAxis.axisColor = "#DADADA";

            // value axis
            var valueAxis = new AmCharts.ValueAxis();
            valueAxis.axisColor = "#FF6600";
            valueAxis.axisThickness = 2;
            valueAxis.gridAlpha = 0;
            chart.addValueAxis(valueAxis);

            // GRAPHS
            // first graph
            for(var grph=0;grph<namecollection.length;grph++){
                var graph1 = new AmCharts.AmGraph();
                graph1.title = namecollection[grph].substr(0,20);
                graph1.valueField = "g"+(grph+1);
                graph1.bullet = "round";
                graph1.hideBulletsCount = 30;
                chart.addGraph(graph1);

            }



            // CURSOR
            var chartCursor = new AmCharts.ChartCursor();
            chartCursor.cursorPosition = "mouse";
            chart.addChartCursor(chartCursor);

            // SCROLLBAR
            var chartScrollbar = new AmCharts.ChartScrollbar();
            chart.addChartScrollbar(chartScrollbar);

            // LEGEND
            var legend = new AmCharts.AmLegend();
            legend.marginLeft = 110;
            chart.addLegend(legend);

            // WRITE
            chart.write("trendservice");
            $("#trendservice a").hide();



        },
        complete:function(){
            if(det_kategory !=''){
                dsbtop10sparepart(kategory, det_kategory, start, end, mode);
            }else{
                dsbtop10sparepart(kategory);        
            }        
        }
    });

}



// generate some random data
function generateChartData() {

    $.ajax({
        url : './dashboard/getStatusService/store',
        dataType: 'JSON',
        success:function(data){


        }
    });

}

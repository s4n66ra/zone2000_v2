$(document).ready(function(){
    $('a').each(
        function(){
            var vhref = $(this).attr('href');

            if(vhref=='#tab_po_1'){
                $(this).click(
                    function(){
                        var navandtab = $(this).parent().parent().parent().parent();
                        var tabonly = $('#tab_po_1',navandtab);


                        tabonly.load('./purchase_order/review/',function(){

                        });
                        
                        Metronic.unblockUI('#tab_po_1');
                    }
                );
            }


            if(vhref=='#tab_po_2'){
                $(this).click(
                    function(){
                        var navandtab = $(this).parent().parent().parent().parent();
                        var tabonly = $('#tab_po_2',navandtab);
                        tabonly.load('./purchase_order/bundle/');
                        
                    }
                );
            }

            if(vhref=='#tab_po_3'){
                $(this).click(
                    function(){
                        var navandtab = $(this).parent().parent().parent().parent();
                        var tabonly = $('#tab_po_3',navandtab);
                        console.log(tabonly);
                        tabonly.load('./purchase_order/po/');
                        
                    }
                );
            }

            if(vhref=='#tab_po_sc_1'){
                $(this).click(
                    function(){
                        var navandtab = $(this).parent().parent().parent().parent();
                        var tabonly = $('#tab_po_sc_1',navandtab);


                        tabonly.load('./purchase_order_sc/review/',function(){

                        });
                        
                        Metronic.unblockUI('#tab_po_sc_1');
                    }
                );
            }


            if(vhref=='#tab_po_sc_2'){
                $(this).click(
                    function(){
                        var navandtab = $(this).parent().parent().parent().parent();
                        var tabonly = $('#tab_po_sc_2',navandtab);
                        tabonly.load('./purchase_order_sc/bundle/');
                        
                    }
                );
            }

            if(vhref=='#tab_po_sc_3'){
                $(this).click(
                    function(){
                        var navandtab = $(this).parent().parent().parent().parent();
                        var tabonly = $('#tab_po_sc_3',navandtab);
                        //console.log(tabonly);
                        tabonly.load('./purchase_order_sc/po/');
                        
                    }
                );
            }

            if(vhref=='#tab_po_tk_1'){
                $(this).click(
                    function(){
                        var navandtab = $(this).parent().parent().parent().parent();
                        var tabonly = $('#tab_po_tk_1',navandtab);


                        tabonly.load('./purchase_order_sc/review/',function(){

                        });
                        
                        Metronic.unblockUI('#tab_po_tk_1');
                    }
                );
            }


            if(vhref=='#tab_po_tk_2'){
                $(this).click(
                    function(){
                        var navandtab = $(this).parent().parent().parent().parent();
                        var tabonly = $('#tab_po_tk_2',navandtab);
                        tabonly.load('./purchase_order_ticket/bundle/');
                        
                    }
                );
            }

            if(vhref=='#tab_po_tk_3'){
                $(this).click(
                    function(){
                        var navandtab = $(this).parent().parent().parent().parent();
                        var tabonly = $('#tab_po_tk_3',navandtab);
                        //console.log(tabonly);
                        tabonly.load('./purchase_order_ticket/po/');
                        
                    }
                );
            }

        }
    );
    
    //ACTION KLIK START DAY IN SERVICE
    $(document).on('click', '.day', function(){
        if(window.location.href.indexOf("service") > -1){
            refreshTableByDate();
        }
    });

    //ACTION PO COPY
    $(document).on('click','.btn-copy-save', function(){
        $('input').each(function(){
            
            if($(this).val().length > 15){
                var po_code = $(this).val();
                console.log(po_code);

                $.ajax({
                  dataType: "json",
                  url: './purchase_order/collectPoFromPoCode/'+po_code,
                  success: function(data,res){
                        console.log(res);
                        toastr['success']("sukses", 'Success');
                    }
                  ,error:function(){
                        console.log("GAGAL");
                  },

                });
              
            }
        });
    });
    //END ACTION PO COPY


    //ITEM DI PURCHASE REQUEST
    $("#form-content").on('shown',function(){
        var pr_id = $("#pr_id").val();
        $.ajax({
            dataType: "json",
            url: './purchase_request/getItemsByIdAndKeywordJSON/'+pr_id,
            success: function(data,res){
                $("#form-content #pr_list_item_id").select2({
                    allowClear:true,
                    minimumInputLength:5,
                    data : data,
                    placeholder: "Select Item",
                });

                $("#form-content #pr_list_item_id").select2('open');
                $("#form-content #pr_list_item_id").attr('name','data[item_id]');


            },
            error: function(data,res){
                console.log("ERROR");
            }

        });

    });
    //ITEM DI PURCHASE REQUEST


    //ITEM DI SELLING ADD
/*    Dipakai juga di Mutasi, Likuidasi, Return Outlet

*/
    $("#form-content-2").on('shown',function(){
        var pr_id = $("#pr_id").val();

        $.ajax({
            dataType: "json",
            url: './replenish/getItemsByIdAndKeywordJSON/',
            success: function(data,res){
                $("#form-content-2 #sell_list_item_id").select2({
                    allowClear:true,
                    minimumInputLength:5,
                    data : data,
                    placeholder: "Select Item",
                });

                $("#form-content-2 #sell_list_item_id").attr('name','data[item_id]');



            },
            error: function(data,res){
                console.log("ERROR");
            }

        });

    });
    //END ITEM DI SELLING ADD

     //ITEM DI Stok ofname sc
/*    

*/
    $("#form-content-sc").on('shown',function(){
        var pr_id = $("#pr_id").val();

        $.ajax({
            dataType: "json",
            url: './replenish/getItemsByIdAndKeywordJSON/3/',
            success: function(data,res){
                $("#form-content-2 #sell_list_item_id").select2({
                    allowClear:true,
                    minimumInputLength:5,
                    data : data,
                    placeholder: "Select Item",
                });

                $("#form-content-2 #sell_list_item_id").attr('name','data[item_id]');



            },
            error: function(data,res){
                console.log("ERROR");
            }

        });

    });
    //END ITEM DI SELLING ADD

    //ITEM DI SELLING EDIT
    $("#form-content").on('shown',function(){
        var pr_id = $("#pr_id").val();
        console.log($("#form-content #sell_list_item_id").val());

        $.ajax({
            dataType: "json",
            url: './replenish/getItemsByIdAndKeywordJSON/2/',
            success: function(data,res){
                $("#form-content #sell_list_item_id").select2({
                    allowClear:true,
                    minimumInputLength:5,
                    data : data,
                    placeholder: "Select Item to Sell",
                });

                $("#form-content-2 #sell_list_item_id").attr('name','data[item_id]');



            },
            error: function(data,res){
                console.log("ERROR");
            }

        });

    });
    //END ITEM DI SELLING EDIT
    
    /*
        ITEM DI REDEMP table_data.php
        Dipakai juga untuk mc-refill / Vending

    */    
    if($("#mc-redemp").attr('class') == 'tab-pane active' || $("#mc-refill").attr('class')=='tab-pane active'){
        console.log($(".component-table-data").attr('type-id'));


        $("#hargatiket").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                 // Allow: Ctrl+A, Command+A
                (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
                 // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                     // let it happen, don't do anything
                     return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });

        $("#hargatiket").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                 // Allow: Ctrl+A, Command+A
                (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
                 // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                     // let it happen, don't do anything
                     return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
        if((window.location.href.indexOf("buying_sc") > -1)||(window.location.href.indexOf("mutasi_sc_in") > -1)){
            $.ajax({
                dataType: "json",
                url: './buying_sc/history/true/',
                success: function(data,res){
                    $(".component-table-data #redemp_list_item_id").select2({
                        allowClear:true,
                        minimumInputLength:5,
                        data : data,
                        placeholder: "Select Item",
                    }).select2('val','83').on('select2-selected',function(e){
                        $('.component-table-data #redemp_list_item_id').val(e.val);
                    });



                    refreshListTransaction($(".component-table-data #redemp_list_item_id"));


                },
                error: function(data,res){
                    console.log("AJAX ERROR");
                }

            });
        }else{
            $.ajax({
                dataType: "json",
                url: './redeem/history/true/',
                success: function(data,res){
                    $(".component-table-data #redemp_list_item_id").select2({
                        allowClear:true,
                        minimumInputLength:5,
                        data : data,
                        placeholder: "Select Item",
                    }).select2('val','83').on('select2-selected',function(e){
                        $('.component-table-data #redemp_list_item_id').val(e.val);
                    });



                    refreshListTransaction($(".component-table-data #redemp_list_item_id"));


                },
                error: function(data,res){
                    console.log("AJAX ERROR");
                }

            });
        }


    }
    //END ITEM DI REDEMP dan REFILL / VENDING


    /*
        ITEM DI REPLENISH replenish.php

    */    
    if($("#rep-mc-refill").attr('class')=='tab-pane active'){
        console.log($(".component-table-data").attr('type-id'));
        $.ajax({
            dataType: "json",
            url: './replenish/getItemsByIdAndKeywordJSON/2/',
            success: function(data,res){
                $(".component-table-data #redemp_list_item_id").select2({
                    allowClear:true,
                    minimumInputLength:5,
                    data : data,
                    placeholder: "Select Item",
                }).select2('val','83').on('select2-selected',function(e){
                    $('.component-table-data #redemp_list_item_id').val(e.val);
                });



                refreshListTransaction($(".component-table-data #redemp_list_item_id"));


            },
            error: function(data,res){
                console.log("AJAX ERROR");
            }

        });


    }
    //END ITEM DI REPLENISH



    //SUPPLIER KETIKA SUPPLY
    $(document).on('click','.btn-supply',function(e,f,g){
        $('#table-supplier-detail_wrapper').on('shown',function(){
            console.log('muncul');
        });
        $.ajax({
            dataType: "json",
            url: './supplier/getAllItem/',
            success: function(data,res){
                $("#supplier_list_item_id").select2({
                    allowClear:true,
                    minimumInputLength:5,
                    data : data,
                    placeholder: "Select Item",
                }).select2('val','').on('select2-selected',function(e){
                    $('#supplier_list_item_id').val(e.val);
                    console.log($('#supplier_list_item_id').val());
                });


            },
            error: function(data,res){
                console.log("AJAX ERROR");
            }

        });
            
    });
/*     $('.edit-detail-supplier').each(function () {
            var $this = $(this);
            $this.on("click", function () {
                alert($(this).attr('def-id'));
            });
        });*/
    
    $(".edit-detail-supplier").click(function() {
       var myClass = $(this).attr("def-id");
       alert(myClass);
    });
    $('.edit-detail-supplier').each(function () {
            var $this = $(this);
            $this.on("click", function () {
                alert($(this).data('def_id'));
            });
        });

    $(document).on('click','.edit-detail-supplier--',function(e,f,g){
    //$(".edit-detail-supplier").click(function(e,f,g) {
/*        $('#table-supplier-detail_wrapper').on('shown',function(){
            console.log('muncul');
        });*/
/*       $('.showEdt').each(function () {
            var $this = $(this);
            $this.on("click", function () {
                alert($(this).data('evalz'));
            });
        });*/
        
       /* var def_id = g.attr('def_id');
        console.log(def_id);*/
        $.ajax({
            dataType: "json",
            url: './supplier/getAllItem/',
            success: function(data,res){
                $("#supplier_list_item_id").select2({
                    allowClear:true,
                    minimumInputLength:5,
                    data : data,
                    placeholder: "Select Item",
                }).select2('val','').on('select2-selected',function(e){
                    $('#supplier_list_item_id').val(e.val);
                    console.log($('#supplier_list_item_id').val());
                });


            },
            error: function(data,res){
                console.log("AJAX ERROR");
            }

        });
            
    });

    if(window.location.href.indexOf("stok_sc") > -1){
        $("#form-content").on('shown',function(){
            var value = {code:value, rect: true};
            var settings = {
                            output:"css",
                            bgColor: "white",
                            color: "black",
                            barWidth: 2,
                            barHeight: 50,
                            moduleSize: 0,
                            posX: 0,
                            posY: 0,
                            addQuietZone: false,
                            fontSize:12,
                        };

            //$("#barcode_modal").show().barcode($("#item_key").val(), "code128", settings);            
            $("#barcode_modal").html("").show().barcode($("#item_key").val(), "code128", settings);
            //new EAN13(document.getElementById("barcode_modal"),"5901234123457777777777");
        });
    }

    if(window.location.href.indexOf("stok") > -1){
        $("#form-content").on('shown',function(){
            var value = {code:value, rect: true};
            var settings = {
                            output:"css",
                            bgColor: "white",
                            color: "black",
                            barWidth: 2,
                            barHeight: 50,
                            moduleSize: 0,
                            posX: 0,
                            posY: 0,
                            addQuietZone: false,
                            fontSize:12,
                        };

            //$("#barcode_modal").show().barcode($("#item_key").val(), "code128", settings);            
            $("#barcode_modal").html("").show().barcode($("#item_key").val(), "code128", settings);
            //new EAN13(document.getElementById("barcode_modal"),"5901234123457777777777");
        });
    }

    if(window.location.href.indexOf("service") > -1){
        $("#form-content").on('shown',function(){
            var value = {code:value, rect: true};
            var settings = {
                            output:"css",
                            bgColor: "white",
                            color: "black",
                            barWidth: 2,
                            barHeight: 50,
                            moduleSize: 0,
                            posX: 0,
                            posY: 0,
                            addQuietZone: false,
                            fontSize:12,
                        };

            //$("#barcode_modal").show().barcode($("#item_key").val(), "code128", settings);            
            $("#barcode_modal").html("").show().barcode($("#doc_number").val(), "code128", settings);
            //new EAN13(document.getElementById("barcode_modal"),"5901234123457777777777");
        });
    }


    if(window.location.href.indexOf("hadiah") > -1){
        $("#form-content").on('shown',function(){
            $("#barcode_modal").css("z-index","200000");
            var value = {code:value, rect: true};
            var settings = {
                            output:"css",
                            bgColor: "white",
                            color: "black",
                            barWidth: 2,
                            barHeight: 50,
                            moduleSize: 0,
                            posX: 0,
                            posY: 0,
                            addQuietZone: false,
                            fontSize:12,
                        };

            //$("#barcode_modal").show().barcode($("#item_key").val(), "code128", settings);            
            $("#barcode_modal").html("").show().barcode($("#item_key").val(), "code128", settings);
            //new EAN13(document.getElementById("barcode_modal"),"5901234123457777777777");
        });
    }else{
        if(window.location.href.indexOf("receiving_warehouse") > -1){
            $("#form-content").on('shown',function(){
                $("#barcode_modal").css("z-index","200000");
                var value = {code:value, rect: true};
                var settings = {
                                output:"css",
                                bgColor: "white",
                                color: "black",
                                barWidth: 1,
                                barHeight: 50,
                                moduleSize: 0,
                                posX: 0,
                                posY: 0,
                                addQuietZone: false,
                                fontSize:12,
                            };

                //$("#barcode_modal").show().barcode($("#item_key").val(), "code128", settings);
                $("#barcode_modal").html("").show().barcode($("#koli_code").val(), "code128", settings);
                console.log("Koli Code : "+ $("#koli_code").val());
                //new EAN13(document.getElementById("barcode_modal"),"5901234123457777777777");
            });
            
        }else{
            if(window.location.href.indexOf("sparepart") > -1){
                $("#form-content").on('shown',function(){
                    $("#barcode_modal").css("z-index","200000");
                    var value = {code:value, rect: true};
                    var settings = {
                                    output:"css",
                                    bgColor: "white",
                                    color: "black",
                                    barWidth: 1,
                                    barHeight: 50,
                                    moduleSize: 0,
                                    posX: 0,
                                    posY: 0,
                                    addQuietZone: false,
                                    fontSize:12,
                                };

                    //$("#barcode_modal").show().barcode($("#item_key").val(), "code128", settings);
                    $("#barcode_modal").html("").show().barcode($("#kd_sparepart").val(), "code128", settings);
                    console.log("Kode Sparepart : "+ $("#kd_sparepart").val());
                    //new EAN13(document.getElementById("barcode_modal"),"5901234123457777777777");
                });

            }else{
                if(window.location.href.indexOf("mesin") > -1){
                    $("#form-content").on('shown',function(){
                        $("#barcode_modal").css("z-index","200000");
                        var value = {code:value, rect: true};
                        var settings = {
                                        output:"css",
                                        bgColor: "white",
                                        color: "black",
                                        barWidth: 1,
                                        barHeight: 50,
                                        moduleSize: 0,
                                        posX: 0,
                                        posY: 0,
                                        addQuietZone: false,
                                        fontSize:12,
                                    };
                        var compact_km = removeSpace($("#kd_mesin").val());
                        var compact_sn = removeSpace($("#serial_number").val());

                        $("#serial_number").val(compact_sn);
                        $("#kd_mesin").val(compact_km);
                        //$("#barcode_modal").show().barcode($("#item_key").val(), "code128", settings);
                        $("#barcode_modal").html("").show().barcode(compact_sn, "code128", settings);
                        console.log("Kode Mesin : "+ compact_km);
                        //new EAN13(document.getElementById("barcode_modal"),"5901234123457777777777");
                    });

                }else{
                    if(window.location.href.indexOf("notification_sender") > -1){
                        var objselect2="";
                        var objselect2selected="";
                        var objvalueselected = "";
                        var arr_user=[];
                        $("#form-content-2").on("shown", function(){
                            var list_form_user = $(".portlet-body form .col-md-12",$("#form-content-2"));
    
                            $.ajax({
                                dataType: "json",
                                url: './notification_sender/getAllStores/',
                                success: function(data,res){
                                    objselect2 = $("#list_user").select2({
                                        allowClear:true,
                                        minimumInputLength:1,
                                        data : data,
                                        placeholder: "Select Store",
                                    });

                                    objselect2.select2('val','').on('select2-selected',function(e){
                                        console.log("item id : "+ e.val);
                                        objselect2selected = e.val;
                                        objvalueselected = e.choice.text;
                                    });


                                    $("#tambah_user").click(function(){
                                        //ACTION DITARUH DI SINI
                                        if(objselect2selected!="" && objselect2selected !=null && objselect2selected!=undefined){
                                            $(".save_notification").removeAttr("disabled");
                                            var rowbaru = '<div class="form-group row"><label class="control-label col-md-4"></label><div class="col-md-6"><input type="text" data-required="1" class="form-control" id="selected_user'+objselect2selected+'" value="'+objvalueselected+'" nomor="'+objselect2selected+'" name="dest[]" disabled/><!-- {{ list.user|raw }} --></div><div class="btn red-pink btn-table-delete" id="red'+objselect2selected+'"><i class="fa fa-trash"></i></div></div>';

                                            //ceknomor bernilai TRUE jika tidak ada yang sama
                                            var ceknomor = true;
                                            $("input", list_form_user).each(
                                                function(){
                                                    if(objselect2selected==$(this).attr("nomor")){
                                                        ceknomor = false;
                                                    }
                                                }
                                            );

                                            console.log("ceknomor : "+ ceknomor);
                                            if(ceknomor==true){
                                                list_form_user.append(rowbaru);
                                                arr_user.push(objselect2selected);
                                                
                                                for(var i=0;i<arr_user.length;i++){
                                                    console.log("ISI ARRAY : "+arr_user[i]);
                                                }
                                            }


                                        }
                                        //END ACTION
                                        //SET objselected ke undefined lagi
                                        objselect2selected = null;
                                    });

                                    $(document).on('click','.btn-table-delete',function(){
                                        var input = $("input", $(this).parent());
                                        var foundnomor =  input.attr("nomor");
                                        console.log("YG DIHAPUS : "+ foundnomor);
                                        for(var j=0; j<arr_user.length;j++){
                                            if(arr_user[j]==foundnomor){
                                                arr_user.splice(arr_user.indexOf(foundnomor), 1);
                                            }
                                        }

                                        for(var k=0;k<arr_user.length;k++){
                                            console.log("ISI ARRAY AFTER HAPUS: "+arr_user[k]);
                                        }


                                        $(this).parent().remove();

                                    });

                                    //SAVE NOTIFICATION TOMBOL SAVE CLICK
                                    $("#button-save-notification").click(function(){
                                        var custom_message = $("#custom_message").val();
                                        $.ajax({
                                            dataType:"html",
                                            type:"POST",
                                            url:"./notification_sender/savenotif/",
                                            data:{custom_message:custom_message, destination:arr_user,},
                                            success:function(data){
                                                var res = JSON.parse(data);
                                                if(res.message=="success"){
                                                    refreshDataTable();
                                                    toastr['success']("Notification Added", 'Success');                                           
                                                }else{
                                                    toastr['error']("Fail to Add", 'Failed');                                                    
                                                }
                                                arr_user = [];                                             

        
                                            },
                                            error:function(){
                                                toastr['error']("Fail to Add", 'Failed');          
                                                console.log("kirim notif gagal");
                                            }
                                        });
                                    });

                                },
                                error: function(data,res){
                                    console.log("ERROR");
                                }

                            });
                        });
                    }
                }
            }
        }        
    }

    if(window.location.href.indexOf("shipping_sc") > -1){

        $("#form-content-2").on("shown", function(){
            var content_table = $("#content_table");
            var btn = $("button", content_table);
            var row = $(".no-footer tbody", content_table);
            var count = 0;
            var koli_id_val = "";
            var koli_qty = "";
            var list_form = $(".portlet-body form .col-md-12",$("#form-content-2"));

            //DISABLE SAVE BUTTON
            var bs = $("#button-save",this);
            //bs.attr("disabled", true);

            //FIELD SCAN HERE CHANGE
            $("#scan_here").change(function(){
                var res_convert = convertToClearString($(this).val());
                $("#"+res_convert).attr("checked", true);

                //GET INPUT CHECKBOX SAJA
                truestat = true;
                var cbxs = $("#form-content-2 input");
                cbxs.each(function(){
                    var tp = $(this).attr("type");
                    if(tp == "checkbox"){
                        truestat = truestat && $(this)[0].checked;
                    }
                });

                if(truestat){
                    bs.removeAttr("disabled");
                }

                $(this).val('');
            });

            $('span', $(row)).each(function(){
                if($(this).attr("class")=="checked"){                    
                    pr_id = $("input",$(this)).val();
                    var itemQty = 0;
                    $.ajax({
                        dataType: "json",
                        url: './shipping_sc/getItemQty/'+pr_id+'/',
                        success: function(data,res){
                            itemQty = data;
                            console.log('qty:'+itemQty);
                                $.ajax({
                                dataType: "json",
                                url: './shipping_sc/getItemIdFromPP/'+pr_id+'/',
                                success: function(data,res){
                                    var at = 0;
                                    var i = itemQty;
                                    for(var m=0; m<i; m++){
                                        check_id = data[m].item_key;
                                        check_id = convertToClearString(check_id);
                                        if(count==0){
                                            list_form.append('<div class="form-group row"><label class="control-label col-md-4">LIST ITEM</label><div class="col-md-6"><input type="checkbox" name="item_id[]" value = "'+data[m].item_id+'" data-required="1" id="'+check_id+'" />'+data[m].item_key+'/'+data[m].item_name+'/'+data[m].qty_approved+'</div></div>');  
                                            count++;                        
                                        }else{
                                            list_form.append('<div class="form-group row"><label class="control-label col-md-4"></label><div class="col-md-6"><input type="checkbox" name="item_id[]" value = "'+data[m].item_id+'" data-required="1" id="'+check_id+'" />'+data[m].item_key+'/'+data[m].item_name+'/'+data[m].qty_approved+'</div></div>');                                                         
                                        }
                                    }
                                }
                            });
                        }
                    });





                }
            });

        });
    }

    if(window.location.href.indexOf("shipping_warehouse") > -1){

        $("#form-content-2").on("shown", function(){
            var content_table = $("#content_table");
            var btn = $("button", content_table);
            var row = $(".no-footer tbody", content_table);
            var count = 0;
            var koli_id_val = "";
            var koli_qty = "";
            var list_form = $(".portlet-body form .col-md-12",$("#form-content-2"));

            //DISABLE SAVE BUTTON
            var bs = $("#button-save",this);
            bs.attr("disabled", true);

            //FIELD SCAN HERE CHANGE
            $("#scan_here").change(function(){
                var res_convert = convertToClearString($(this).val());
                $("#"+res_convert).attr("checked", true);

                //GET INPUT CHECKBOX SAJA
                truestat = true;
                var cbxs = $("#form-content-2 input");
                cbxs.each(function(){
                    var tp = $(this).attr("type");
                    if(tp == "checkbox"){
                        truestat = truestat && $(this)[0].checked;
                    }
                });

                if(truestat){
                    bs.removeAttr("disabled");
                }

                $(this).val('');
            });

            $('span', $(row)).each(function(){
                if($(this).attr("class")=="checked"){                    
                    koli_id_val = $("input",$(this)).val();

                    $.ajax({
                        dataType: "json",
                        url: './shipping_warehouse/getKoliCodeFromKoliId/'+koli_id_val+'/',
                        success: function(data,res){
                            var at = 0;
                            var i = data[0].qty;
                            for(var m=0; m<i; m++){
                                check_id = data[0].koli_code+'-'+Number(at+1)+'-'+i;
                                check_id = convertToClearString(check_id);
                                if(count==0){
                                    list_form.append('<div class="form-group row"><label class="control-label col-md-4">LIST KOLI</label><div class="col-md-6"><input type="checkbox" name="data[truck_number]" value = "" data-required="1" id="'+check_id+'" disabled/>'+data[0].koli_code+'/'+Number(at+1)+'/'+data[0].qty+'</div></div>');                         
                                }else{
                                    list_form.append('<div class="form-group row"><label class="control-label col-md-4"></label><div class="col-md-6"><input type="checkbox" name="data[truck_number]" value = "" data-required="1" id="'+check_id+'" disabled/>'+data[0].koli_code+'/'+Number(at+1)+'/'+data[0].qty+'</div></div>');                                                         
                                }
                                at++;
                                count++; 
                            }

                        }
                    });



                }
            });

        });
    }

    if(window.location.href.indexOf("stok_ofname") > -1){
        $(document).on("keydown","#form-content-2 #stok_gudang",function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                 // Allow: Ctrl+A, Command+A
                (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
                 // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                     // let it happen, don't do anything
                     return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });

        $(document).on("keydown","#form-content-2 #stok_store",function (e){
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                 // Allow: Ctrl+A, Command+A
                (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
                 // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                     // let it happen, don't do anything
                     return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });

        $(document).on("keydown","#form-content-2 #stok_mesin",function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                 // Allow: Ctrl+A, Command+A
                (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
                 // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                     // let it happen, don't do anything
                     return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });

        $(document).on('input','#stok_gudang',function(){
            $("#stok_total").val(Number($("#stok_gudang").val())+Number($("#stok_store").val())+Number($("#stok_mesin").val()));
        });

        $(document).on('input','#stok_mesin',function(){
            $("#stok_total").val(Number($("#stok_gudang").val())+Number($("#stok_store").val())+Number($("#stok_mesin").val()));
        });

        $(document).on('input','#stok_store',function(){
            $("#stok_total").val(Number($("#stok_gudang").val())+Number($("#stok_store").val())+Number($("#stok_mesin").val()));
        });

        $(document).on('shown', '#form-content-2',function(){
            $("#stok_total").val(0);
            $("#stok_gudang").val(0);
            $("#stok_mesin").val(0);
            $("#stok_store").val(0);

            $.ajax({
                dataType: "json",
                url: './replenish/getItemsByIdAndKeywordJSON/2/',
                success: function(data,res){
                    $("#sell_list_item_id").select2({
                        allowClear:true,
                        minimumInputLength:5,
                        data : data,
                        placeholder: "Select Item",
                    }).select2('val','').on('select2-selected',function(e){
                        console.log("item id : "+ e.val);
                        console.log("id cabang : "+ $("#store_id").val());

                        $.ajax({
                            dataType:"json",
                            url: './stok_ofname/getDataByItemIdAndStoreId/'+e.val+'/'+$("#store_id").val(),
                            success:function(data){
                                if(data==""){
                                    $("#old_stok_total").val(0);
                                    $("#old_stok_gudang").val(0);
                                    $("#old_stok_mesin").val(0);
                                    $("#old_stok_store").val(0);
                                }else{
                                    $("#old_stok_total").val(data.stok_total);
                                    $("#old_stok_gudang").val(data.stok_gudang);
                                    $("#old_stok_mesin").val(data.stok_mesin);
                                    $("#old_stok_store").val(data.stok_store);
                                }

                            }
                        });
                   });

                    $("#sell_list_item_id").attr('name','data[item_id]');



                },
                error: function(data,res){
                    console.log("ERROR");
                }

            });

        });

    }

    if(window.location.href.indexOf("stok_ofname_sc") > -1){
        $(document).on("keydown","#form-content-sc #stok_gudang",function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                 // Allow: Ctrl+A, Command+A
                (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
                 // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                     // let it happen, don't do anything
                     return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });

        $(document).on("keydown","#form-content-sc #stok_store",function (e){
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                 // Allow: Ctrl+A, Command+A
                (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
                 // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                     // let it happen, don't do anything
                     return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });

        $(document).on("keydown","#form-content-sc #stok_mesin",function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                 // Allow: Ctrl+A, Command+A
                (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
                 // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                     // let it happen, don't do anything
                     return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });

        $(document).on('input','#stok_gudang',function(){
            $("#stok_total").val(Number($("#stok_gudang").val())+Number($("#stok_store").val())+Number($("#stok_mesin").val()));
        });

        $(document).on('input','#stok_mesin',function(){
            $("#stok_total").val(Number($("#stok_gudang").val())+Number($("#stok_store").val())+Number($("#stok_mesin").val()));
        });

        $(document).on('input','#stok_store',function(){
            $("#stok_total").val(Number($("#stok_gudang").val())+Number($("#stok_store").val())+Number($("#stok_mesin").val()));
        });

        $("#form-content-2").on('shown',function(){
            var pr_id = $("#pr_id").val();

            $.ajax({
                dataType: "json",
                url: './replenish/getItemsByIdAndKeywordJSON/3/',
                success: function(data,res){
                    $("#form-content-2 #sell_list_item_id_sc").select2({
                        allowClear:true,
                        minimumInputLength:5,
                        data : data,
                        placeholder: "Select Item",
                    });

                    $("#form-content-2 #sell_list_item_id_sc").attr('name','data[item_id]');



                },
                error: function(data,res){
                    console.log("ERROR");
                }

            });

        });

        $(document).on('shown', '#form-content-2',function(){
            $("#stok_total").val(0);
            $("#stok_gudang").val(0);
            $("#stok_mesin").val(0);
            $("#stok_store").val(0);
            //alert('te');
            $.ajax({
                dataType: "json",
                url: './replenish/getItemsByIdAndKeywordJSON/3/',
                success: function(data,res){
                    $("#sell_list_item_id_sc").select2({
                        allowClear:true,
                        minimumInputLength:5,
                        data : data,
                        placeholder: "Select Item",
                    }).select2('val','').on('select2-selected',function(e){
                        console.log("item id : "+ e.val);
                        console.log("id cabang : "+ $("#store_id").val());

                        $.ajax({
                            dataType:"json",
                            url: './stok_ofname_sc/getDataByItemIdAndStoreId/'+e.val+'/'+$("#store_id").val(),
                            success:function(data){
                                if(data==""){
                                    $("#old_stok_total").val(0);
                                    $("#old_stok_gudang").val(0);
                                    $("#old_stok_mesin").val(0);
                                    $("#old_stok_store").val(0);
                                    $("#old_stok_gudang_mutasi").val(0);
                                }else{
                                    $("#old_stok_total").val(data.stok_total);
                                    $("#old_stok_gudang").val(data.stok_gudang);
                                    $("#old_stok_mesin").val(data.stok_mesin);
                                    $("#old_stok_store").val(data.stok_store);
                                    $("#old_stok_gudang_mutasi").val(data.stok_gudang_mutasi);
                                }

                            }
                        });
                   });

                    $("#sell_list_item_id_sc").attr('name','data[item_id]');



                },
                error: function(data,res){
                    console.log("ERROR");
                }

            });

        });

    }

    if((window.location.href.indexOf("buying_sc") > -1)||(window.location.href.indexOf("mutasi_sc_in") > -1)){


        $(document).on("keydown","#form-content-sc #stok_gudang",function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                 // Allow: Ctrl+A, Command+A
                (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
                 // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                     // let it happen, don't do anything
                     return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });

        $(document).on("keydown","#form-content-sc #stok_store",function (e){
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                 // Allow: Ctrl+A, Command+A
                (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
                 // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                     // let it happen, don't do anything
                     return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });

        $(document).on("keydown","#form-content-sc #stok_mesin",function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                 // Allow: Ctrl+A, Command+A
                (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
                 // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                     // let it happen, don't do anything
                     return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });

        $(document).on('input','#stok_gudang',function(){
            $("#stok_total").val(Number($("#stok_gudang").val())+Number($("#stok_store").val())+Number($("#stok_mesin").val()));
        });

        $(document).on('input','#stok_mesin',function(){
            $("#stok_total").val(Number($("#stok_gudang").val())+Number($("#stok_store").val())+Number($("#stok_mesin").val()));
        });

        $(document).on('input','#stok_store',function(){
            $("#stok_total").val(Number($("#stok_gudang").val())+Number($("#stok_store").val())+Number($("#stok_mesin").val()));
        });

        $("#form-content-2").on('shown',function(){
            var pr_id = $("#pr_id").val();

            $.ajax({
                dataType: "json",
                url: './replenish/getItemsByIdAndKeywordJSON/3/',
                success: function(data,res){
                    $("#form-content-2 #sell_list_item_id_sc").select2({
                        allowClear:true,
                        minimumInputLength:5,
                        data : data,
                        placeholder: "Select Item",
                    });

                    $("#form-content-2 #sell_list_item_id_sc").attr('name','data[item_id]');



                },
                error: function(data,res){
                    console.log("ERROR");
                }

            });

        });

        $(document).on('shown', '#form-content-2',function(){
            $("#stok_total").val(0);
            $("#stok_gudang").val(0);
            $("#stok_mesin").val(0);
            $("#stok_store").val(0);

            $.ajax({
                dataType: "json",
                url: './replenish/getItemsByIdAndKeywordJSON/3/',
                success: function(data,res){
                    $("#sell_list_item_id").select2({
                        allowClear:true,
                        minimumInputLength:5,
                        data : data,
                        placeholder: "Select Item",
                    }).select2('val','').on('select2-selected',function(e){
                        console.log("item id : "+ e.val);
                        console.log("id cabang : "+ $("#store_id").val());

                        $.ajax({
                            dataType:"json",
                            url: './buying_sc/getDataByItemIdAndStoreId/'+e.val+'/'+$("#store_id").val(),
                            success:function(data){
                                if(data==""){
                                    $("#old_stok_total").val(0);
                                    $("#old_stok_gudang").val(0);
                                    $("#old_stok_mesin").val(0);
                                    $("#old_stok_store").val(0);
                                }else{
                                    $("#old_stok_total").val(data.stok_total);
                                    $("#old_stok_gudang").val(data.stok_gudang);
                                    $("#old_stok_mesin").val(data.stok_mesin);
                                    $("#old_stok_store").val(data.stok_store);
                                }

                            }
                        });
                   });

                    $("#sell_list_item_id_sc").attr('name','data[item_id]');



                },
                error: function(data,res){
                    console.log("ERROR");
                }

            });

        });

    }

    //MENGHAPUS HIDDEN DI SUB TOTAL
    if(window.location.href.indexOf("redeem") > -1){
        $("#table-kalkulator").removeAttr("hidden");
        $("#sub_total").removeAttr("hidden");
        $("#sisa").removeAttr("hidden");
        $("#hargatiket").removeAttr("hidden");
    }

    /*    
        INI BAGIAN DASHBOARD
        START DASHBOARD
    */    
    if(window.location.href.indexOf("dashboard") > -1){
        //AWAL DASHBOARD MUNCUL
        $.ajax({
            url:'./dashboard/store_empty/',
            dataType:'JSON',
            success:function(data){

                $("#det_kategory_id").select2({
                    data:data,
                    allowClear:true,
                    placeholder:'--Select Store--',
                });                        

            }
        });


        //MEMANGGIL SELURUH DASHBOARD SEBELUM FILTER
        var dsb_f_kategory_id = $("#kategory_id");
        dsbtopten(dsb_f_kategory_id.val());
/*
        
        dsbsummerchandise(dsb_f_kategory_id.val());
        dsbtop5mesin(dsb_f_kategory_id.val());
        dsbomzet(dsb_f_kategory_id.val());
*/




        //ACTION KATEGORY CHANGE
        var dsb_f_kategory_id = $("#kategory_id");
        dsb_f_kategory_id.change(function(){
            var kat_id = dsb_f_kategory_id.val();
            switch(kat_id){
                case 'store':
                    $.ajax({
                        url:'./dashboard/store_empty/',
                        dataType:'JSON',
                        success:function(data){

                            $("#det_kategory_id").select2({
                                data:data,
                                allowClear:true,
                                placeholder:'--Select Store--',
                            });                        

                        }
                    });
                    break;
                case 'area':
                    $.ajax({
                        url:'./dashboard/area_empty/',
                        dataType:'JSON',
                        success:function(data){

                            $("#det_kategory_id").select2({
                                data:data,
                                allowClear:true,
                                placeholder:'--Select Area--',
                            });                        

                        }
                    });
                    break;
                case 'province':
                    $.ajax({
                        url:'./dashboard/province_empty/',
                        dataType:'JSON',
                        success:function(data){

                            $("#det_kategory_id").select2({
                                data:data,
                                allowClear:true,
                                placeholder:'--Select Province--',
                            });                        

                        }
                    });
                    break;
            }
        });


        

        //ACTION APPLY BUTTON DI DASHBOARD FILTER
        $("#btn_dsb_filter_apply").click(function(){
            var dsb_filt_content = $(this).closest('.portlet-body');
            var dsb_f_kategory_id = $("#kategory_id", dsb_filt_content);

            var dsb_f_store_id = '';
            var dsb_f_area_id = '';
            var dsb_f_province_id = '';

            switch(dsb_f_kategory_id.val()){
                case 'store':
                    var kat_id_val = dsb_f_kategory_id.val();
                    var det_kategory_id = $("#det_kategory_id", dsb_filt_content).val();
                    var rent_date_start = $("#rent_date_start", dsb_filt_content).val();
                    var rent_date_end = $("#rent_date_end", dsb_filt_content).val();
                    var mode = $("#mode", dsb_filt_content).val();

                    /*SEBENARNYA dsbtopten TIDAK PERLU mode TAPI mode 
                    DIBUTUHKAN UNTUK FUNGSI YANG LAINNYA
                    */
                    
                    dsbtopten(kat_id_val, det_kategory_id, rent_date_start, rent_date_end, mode);
/*                    dsbsummerchandise(kat_id_val, det_kategory_id, rent_date_start, rent_date_end, mode);
                    dsbsumticket(kat_id_val, det_kategory_id, rent_date_start, rent_date_end, mode);
                    dsbsumvalue(kat_id_val, det_kategory_id, rent_date_start, rent_date_end, mode);
                    dsbtop5mesin(kat_id_val, det_kategory_id, rent_date_start, rent_date_end, mode);
                    dsbomzet(kat_id_val, det_kategory_id, rent_date_start, rent_date_end, mode);
                    dsbstatusservice(kat_id_val, det_kategory_id, rent_date_start, rent_date_end, mode);
*/                    
                    createNoteFilter(dsb_f_kategory_id.val(), dsb_filt_content);

                    console.log(kat_id_val);
                    console.log(det_kategory_id);
                    console.log(rent_date_start);
                    console.log(rent_date_end);
                    break;
                case 'area':
                    var kat_id_val = dsb_f_kategory_id.val();
                    var det_kategory_id = $("#det_kategory_id", dsb_filt_content).val();
                    var rent_date_start = $("#rent_date_start", dsb_filt_content).val();
                    var rent_date_end = $("#rent_date_end", dsb_filt_content).val();
                    var mode = $("#mode", dsb_filt_content).val();

                    dsbsummerchandise(kat_id_val, det_kategory_id, rent_date_start, rent_date_end, mode);
                    dsbomzet(kat_id_val, det_kategory_id, rent_date_start, rent_date_end, mode);
                    dsbtopten(kat_id_val, det_kategory_id, rent_date_start, rent_date_end);

                    dsbsumticket(kat_id_val, det_kategory_id, rent_date_start, rent_date_end, mode);
                    dsbsumvalue(kat_id_val, det_kategory_id, rent_date_start, rent_date_end, mode);
                    dsbtop5mesin(kat_id_val, det_kategory_id, rent_date_start, rent_date_end, mode);
                    
                    createNoteFilter(dsb_f_kategory_id.val(), dsb_filt_content);

                    console.log(kat_id_val);
                    console.log(det_kategory_id);
                    console.log(rent_date_start);
                    console.log(rent_date_end);
                    break;

                case 'province':
                    var kat_id_val = dsb_f_kategory_id.val();
                    var det_kategory_id = $("#det_kategory_id", dsb_filt_content).val();
                    var rent_date_start = $("#rent_date_start", dsb_filt_content).val();
                    var rent_date_end = $("#rent_date_end", dsb_filt_content).val();
                    var mode = $("#mode", dsb_filt_content).val();

                    dsbsummerchandise(kat_id_val, det_kategory_id, rent_date_start, rent_date_end, mode);
                    dsbomzet(kat_id_val, det_kategory_id, rent_date_start, rent_date_end, mode);
                    dsbtopten(kat_id_val, det_kategory_id, rent_date_start, rent_date_end);

                    dsbsumticket(kat_id_val, det_kategory_id, rent_date_start, rent_date_end, mode);
                    dsbsumvalue(kat_id_val, det_kategory_id, rent_date_start, rent_date_end, mode);
                    dsbtop5mesin(kat_id_val, det_kategory_id, rent_date_start, rent_date_end, mode);
                    
                    createNoteFilter(dsb_f_kategory_id.val(), dsb_filt_content);

                    console.log(kat_id_val);
                    console.log(det_kategory_id);
                    console.log(rent_date_start);
                    console.log(rent_date_end);
                    break;

            }

        });
        

        //MENGHILANGKAN TULISAN LICENSE
        $(document).on('mouseenter',function(){
            $("#lineomsetbarang a").hide();            
            $("#horbar a").hide();
            $("#mer_value a").hide();
            $("#lineomsetticket a").hide();
            $("#dsbomzet a").hide();
            $("#top5machine a").hide();
        });

        $(document).on('mouseleave',function(){
            $("#lineomsetbarang a").hide();
            $("#horbar a").hide();
            $("#mer_value a").hide();
            $("#lineomsetticket a").hide();
            $("#dsbomzet a").hide();
            $("#top5machine a").hide();
        });

        $(document).on('mouseover',function(){
            $("#lineomsetbarang a").hide();
            $("#horbar a").hide();
            $("#mer_value a").hide();
            $("#lineomsetticket a").hide();
            $("#dsbomzet a").hide();
            $("#top5machine a").hide();
        });


    }


    function createNoteFilter(kat,clsportletbody){
        var msg = "By ";
        switch(kat){
            case 'store':
                var from    = $("#rent_date_start",clsportletbody).val();
                var until   = $("#rent_date_end", clsportletbody).val();
                var det_kat_val     = $("#det_kategory_id", clsportletbody).val();
                var det_kat = $("#s2id_det_kategory_id .select2-chosen", clsportletbody).html();
                var mode    = $("#s2id_mode .select2-chosen",clsportletbody).html();

                if(from != undefined && until!=undefined && det_kat_val!=''){
                    msg = msg + "Store "+det_kat+" "+from+" - "+until+" "+mode;
                    $(".note", clsportletbody).html(msg);                    
                }else{
                    $(".table-toolbar",clsportletbody).effect("highlight", {}, 500);
                    $(".note", clsportletbody).html("No Filter");
                }

                break;
            case 'area':
                var from    = $("#rent_date_start",clsportletbody).val();
                var until   = $("#rent_date_end", clsportletbody).val();
                var det_kat_val     = $("#det_kategory_id", clsportletbody).val();
                var det_kat = $("#s2id_det_kategory_id .select2-chosen", clsportletbody).html();
                var mode    = $("#s2id_mode .select2-chosen",clsportletbody).html();

                if(from != undefined && until!=undefined && det_kat_val!=''){
                    msg = msg + "Area "+det_kat+" "+from+" - "+until+" "+mode;
                    $(".note", clsportletbody).html(msg);                    
                }else{
                    $(".table-toolbar",clsportletbody).effect("highlight", {}, 500);
                    $(".note", clsportletbody).html("No Filter");
                }

                break;
            case 'province':
                var from    = $("#rent_date_start",clsportletbody).val();
                var until   = $("#rent_date_end", clsportletbody).val();
                var det_kat_val     = $("#det_kategory_id", clsportletbody).val();
                var det_kat = $("#s2id_det_kategory_id .select2-chosen", clsportletbody).html();
                var mode    = $("#s2id_mode .select2-chosen",clsportletbody).html();

                if(from != undefined && until!=undefined && det_kat_val!=''){
                    msg = msg + "Province "+det_kat+" "+from+" - "+until+" "+mode;
                    $(".note", clsportletbody).html(msg);                    
                }else{
                    $(".table-toolbar",clsportletbody).effect("highlight", {}, 500);
                    $(".note", clsportletbody).html("No Filter");
                }
                break;
        }

    }
    /*    
        INI BAGIAN DASHBOARD
        END DASHBOARD
    */

    /* GLOBAL DATE */
    //var d3 = new Date();
    //var value = @Request.RequestContext.HttpContext.Session["sistem_date"];
    var value = $("#hid_date").val();
    //console.log(value);

    $("#glob_date_outlet").val(value);
    //$("#glob_date_outlet").val(d3.getFullYear().toString()+"-"+(d3.getMonth()+1).toString()+"-"+d3.getDate().toString());
/*    if(window.location.href.indexOf("replenish") > -1){
        $("#glob_date_outlet").show();
        var d3 = new Date();
        $("#glob_date_outlet").val(d3.getFullYear().toString()+"-"+(d3.getMonth()+1).toString()+"-"+d3.getDate().toString());
    }else{
        $("#glob_date_outlet").hide();        
    }
    */

function convertToClearString(str){
    var temp = str.replace(/\./g, '');
    temp = temp.replace(/\//g, '');
    return temp.replace(/\-/g, '');
}

function removeSpace(str){
    var temp = str.replace(/\ /g, '');
    return temp;

}



});
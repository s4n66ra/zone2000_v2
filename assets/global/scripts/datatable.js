/***
Wrapper/Helper Class for datagrid based on jQuery Datatable Plugin
***/
var Datatable = function(conf) {

    var tableOptions; // main options
    var dataTable; // datatable object
    var table; // actual table jquery object
    var tableContainer; // actual table container object
    var tableWrapper; // actual table wrapper jquery object
    var tableInitialized = false;
    var ajaxParams = {}; // set filter mode
    var the;

    // untuk menyimpan filter
    var filter;

    // rofid config
    var config = conf;

    var countSelectedRecords = function() {
        var selected = $('tbody > tr > td:nth-child(1) input[type="checkbox"]:checked', table).size();
        var text = tableOptions.dataTable.language.metronicGroupActions;
        // if (selected > 0) {
        //     $('.table-group-actions', tableWrapper).show();
        //     $('.table-group-actions > span', tableWrapper).text(text.replace("_TOTAL_", selected));
        // } else {            
        //     $('.table-group-actions > span', tableWrapper).text("");
        //     $('.table-group-actions', tableWrapper).hide();
        // }
    };

    return {

        //main function to initiate the module
        init: function(options) {            
            if (!$().dataTable) {
                return;
            }
            the = this;
            if(config.pagination){
                part_pagination = "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>";
            }
            else
                part_pagination = "";

            // default settings
            options = $.extend(true, {
                src: "", // actual table  
                filterApplyAction: "filter",
                filterCancelAction: "filter_cancel",
                resetGroupActionInputOnSuccess: true,
                loadingMessage: 'Loading...',
                dataTable: {
                    "dom": "<'row'<'col-md-4 col-sm-12'<'table-actions'>><'col-md-4 col-sm-12'><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r><'table-scrollable't>"+part_pagination, // datatable layout
                    "pageLength": 2, // default records per page
                    "language": { // language settings
                        // metronic spesific
                        "metronicGroupActions": "_TOTAL_ records selected:  ",
                        "metronicAjaxRequestGeneralError": "Could not complete request. Please check your internet connection",

                        // data tables spesific
                        "lengthMenu": "<span class='seperator'> | </span> view _MENU_ records",
                        "info": "<span class='seperator'> | </span> found total _TOTAL_ records",
                        "infoEmpty": "No records found to show",
                        "emptyTable": "No data available in table",
                        "zeroRecords": "No matching records found",
                        "paginate": {
                            "previous": "Prev",
                            "next": "Next",
                            "last": "Last",
                            "first": "First",
                            "page": "Page",
                            "pageOf": "of"
                        }
                    },

                    "orderCellsTop": true,
                    "columnDefs": [{ // define columns sorting options(by default all columns are sortable extept the first checkbox column)
                        'orderable': false,
                        'targets': [0]
                    }],

                    "pagingType": "bootstrap_extended", // pagination type(bootstrap, bootstrap_full_number or bootstrap_extended)
                    "autoWidth": false, // disable fixed width and enable fluid table
                    "processing": false, // enable/disable display message box on record load
                    "serverSide": true, // enable/disable server side ajax loading

                    "ajax": { // define ajax settings
                        "url": "", // ajax URL
                        "type": "POST", // request type
                        "timeout": 20000,
                        "data": function(data) { // add request parameters before submit
                            $.each(ajaxParams, function(key, value) {
                                data[key] = value;
                            });

                            Metronic.blockUI({
                                message: tableOptions.loadingMessage,
                                target: tableContainer,
                                overlayColor: 'none',
                                cenrerY: true,
                                boxed: true
                            });
                        },
                        "dataSrc": function(res) { // Manipulate the data returned from the server
                            if (res.customActionMessage) {
                                Metronic.alert({
                                    type: (res.customActionStatus == 'OK' ? 'success' : 'danger'),
                                    icon: (res.customActionStatus == 'OK' ? 'check' : 'warning'),
                                    message: res.customActionMessage,
                                    container: tableWrapper,
                                    place: 'prepend'
                                });
                            }

                            if (res.customActionStatus) {
                                if (tableOptions.resetGroupActionInputOnSuccess) {
                                    $('.table-group-action-input', tableWrapper).val("");
                                }
                            }

                            if ($('.group-checkable', table).size() === 1) {
                                $('.group-checkable', table).attr("checked", false);
                                $.uniform.update($('.group-checkable', table));
                            }

                            if (tableOptions.onSuccess) {
                                tableOptions.onSuccess.call(undefined, the);

                            }

                            Metronic.unblockUI(tableContainer);

                            return res.data;
                        },
                        "error": function() { // handle general connection errors
                            if (tableOptions.onError) {
                                tableOptions.onError.call(undefined, the);
                            }

                            Metronic.alert({
                                type: 'danger',
                                icon: 'warning',
                                message: tableOptions.dataTable.language.metronicAjaxRequestGeneralError,
                                container: tableWrapper,
                                place: 'prepend'
                            });

                            Metronic.unblockUI(tableContainer);
                        },
                    },

                    "drawCallback": function(oSettings) { // run some code on table redraw
                        if (tableInitialized === false) { // check if table has been initialized
                            tableInitialized = true; // set table initialized
                            table.show(); // display table
                        }
                        Metronic.initUniform($('input[type="checkbox"]', table)); // reinitialize uniform checkboxes on each table reload
                        countSelectedRecords(); // reset selected records indicator

                        // callback for ajax data load
                        if (tableOptions.onDataLoad) {
                            tableOptions.onDataLoad.call(undefined, the);
                        }



                        
                    }
                }
            }, options);
                  
            options.onDataLoad = function(a, b){
                the.initNumbering();
            }
            

            tableOptions = options;

            // create table's jquery object
            // table = $(options.src); before
            table = options.src;
            tableContainer = table.parents(".table-container");

            // apply the special class that used to restyle the default datatable
            var tmp = $.fn.dataTableExt.oStdClasses;

            $.fn.dataTableExt.oStdClasses.sWrapper = $.fn.dataTableExt.oStdClasses.sWrapper + " dataTables_extended_wrapper";
            $.fn.dataTableExt.oStdClasses.sFilterInput = "form-control input-small input-sm input-inline";
            $.fn.dataTableExt.oStdClasses.sLengthSelect = "form-control input-xsmall input-sm input-inline";

            // initialize a datatable
            if(config.rowReordering==true){
                dataTable = table.DataTable(options.dataTable);
                $('#'+config.id+' tbody').sortable({
                    cursor: "move",
                    /*start:function(event, ui){
                        startPosition = ui.item.prevAll().length + 1;
                    },*/
                    update: function(event, ui) {
                        endPosition = ui.item.prevAll().length + 1;

                        // SEND URUTAN TO SERVER
                        var parent = $(ui.item[0]).parent();
                        var param = [];
                        $('.contain-primary', parent).each(function(index, element){
                            param.push($(element).attr('primary'));
                        });
                        
                        pageSaveProcess(config.url_action_reordering, {data : param});

                        
                        the.initNumbering();
                     }
                });
                
            }
            else
                dataTable = table.DataTable(options.dataTable);

            // revert back to default
            $.fn.dataTableExt.oStdClasses.sWrapper = tmp.sWrapper;
            $.fn.dataTableExt.oStdClasses.sFilterInput = tmp.sFilterInput;
            $.fn.dataTableExt.oStdClasses.sLengthSelect = tmp.sLengthSelect;

            // get table wrapper
            tableWrapper = table.parents('.dataTables_wrapper');

            // build table group actions panel
            if ($('.table-group-actions-wrapper', tableContainer).size() === 1) {
                $('.table-group-actions', tableWrapper).html($('.table-group-actions-wrapper', tableContainer).html()); // place the panel inside the wrapper
                $('.table-group-actions-wrapper', tableContainer).remove(); // remove the template container
            }

            // build table actions
            if ($('.table-actions-wrapper', tableContainer).size() === 1) {
                $('.table-actions', tableWrapper).html($('.table-actions-wrapper', tableContainer).html()); // place the panel inside the wrapper
                $('.table-actions-wrapper', tableContainer).remove(); // remove the template container
            }

            // handle group checkboxes check/uncheck
            $('.group-checkable', table).change(function() {
                var set = $('tbody > tr > td:nth-child(1) input[type="checkbox"]', table);
                var checked = $(this).is(":checked");
                $(set).each(function() {
                    $(this).attr("checked", checked);
                });
                $.uniform.update(set);
                countSelectedRecords();
            });

            // handle numbering on table
            table.on( 'order.dt search.dt page.dt', function () {                
                
            });

            /*----------------------------------
            ======================================
                    CUSTOM FILTER
            ======================================
            -----------------------------------*/
            datatableContainer = tableContainer.parents('.datatable-container');
            filterContainer = $('.filter-container', datatableContainer);
            filterInnerContainer = $('.filter-inner', tableContainer);
            ajaxParams.filter = {};
            $('.filter-value', filterContainer).tagsinput({
                itemValue: 'value',
                itemText: 'text',
                width: 'auto',
                maxTags : 5,
                allowDuplicates: false,
                trimValue : true,
                // typeaheadjs: {
                //     name: 'cities',
                //     displayKey: 'text',
                // }
            });
            $('.filter-value', filterContainer).on('itemAdded', function(event) {
                // event.item: contains the item
                var column = $(this).attr('column');
                $('.filter-value-row-'+column, filterContainer).show();
            });
            $('.filter-value', filterContainer).on('itemRemoved', function(event) {
                // event.item: contains the item
                var column = $(this).attr('column');
                var size = $(this).tagsinput('items').length;
                if(size<=0)
                    $('.filter-value-row-'+column, filterContainer).hide();
            });
            $('.filter-value', filterContainer).on('beforeItemAdd', function(event) {
                // event.item: contains the item
                // event.cancel: set to true to prevent the item getting added
                var column = $(this).attr('column');
                var val = event.item.value;
                var text= event.item.text;
                if (typeof val == 'string' && val.trim()=='')
                    event.cancel = true;

                if (typeof val == 'object'){
                    if (val[0]=='' && val[1]=='')
                        event.cancel = true; 
                    else
                        event.cancel = the.checkFilterParam(column, text);

                }
            });

            // filter-input on ENTER
            $('.filter-input', filterContainer).keypress(function(e){
                if(e.which == 13){
                    $('.filter-add', filterContainer).click();
                }
            });

            // init filter type varible
            ajaxParams.filterType = {};
            $('.filter-options', filterContainer).each(function(key, element){
                var column  = $(element).attr('value');
                var type    = $(element).attr('type');

                ajaxParams.filterType[column] = type;
            }); 

            // hide filter value-input. agar list tags tidak ada input fieldnya
            $('.filter-value').each(function(key, element){
                $(element).parent().find('.bootstrap-tagsinput').find('input').attr('readonly','readonly');
            });

            // handle filter change
            // saat memilih jenis filer. ex : item_name / item_key / item_code
            filterContainer.on('change', '.filter-select', function(){
                var select      = $(this);
                var selected    = $(this).val();
                var type        = $('.filter-option-'+selected, select).attr('type');

                $('.filter-input', filterContainer).val('');
                $('.filter-input-wrapper', filterContainer).hide();
                $('.filter-input-wrapper-'+selected, filterContainer).show();
                
            });

            // handle add filter
            filterContainer.on('click', '.filter-add', function(){
                var filterSelect= $('.filter-select', filterContainer);
                var column= filterSelect.val();

                var filterInput = $('.filter-input-'+column, filterContainer);
                var filterOption= $('.filter-option-'+column, filterContainer);
                
                var type  = filterOption.attr('type');

                if (type=='range-date' || type=='range-numeric'){
                    var valFrom = filterInput.find('.value-from').val();
                    var valTo   = filterInput.find('.value-to').val();
                    $('.filter-value-'+column, filterContainer).tagsinput('add', { 'value': [valFrom, valTo], 'text' : valFrom+' - '+valTo});
                } else if (type=='list') {
                    var filterInput = $('select.filter-input-'+column, filterContainer);
                    var value = filterInput.attr('value');
                    var text  = $('.filter-list-value-'+column+'-'+value).val();
                    $('.filter-value-'+column, filterContainer).tagsinput('add', { 'value': value, 'text' : text});
                } else {
                    var value = filterInput.val();
                    $('.filter-value-'+column, filterContainer).tagsinput('add', { 'value': value, 'text' :value});
                    filterInput.val('');
                }

                filterInput.focus();

            });

            // handle submit custom filter
            filterContainer.on('click', '.filter-submit', function(e){
                e.preventDefault();
                // set all filter param from tagsinput
                the.submitFilter();
            });

            // handle submit inner-filter
            $('.filter-input', filterInnerContainer).live('change', function(){
                the.submitFilter();
            });

/*          
            //TADINYA PAKE YANG INI  
            $('.table-action-show-filter', tableContainer).live('click', function(){
                filterContainer.slideToggle();
                if ($(this).html().trim()=='Show Filter'){
                    $(this).html('Hide Filter');
                }
                else {
                    $(this).html('Show Filter');
                }
            });
*/
            
            $('.table-action-show-filter').click(function(){
                $('.filter-container').slideToggle();
                if ($(this).html().trim()=='Show Filter'){
                    $(this).html('Hide Filter');
                }
                else {
                    $(this).html('Show Filter');
                }
            });
            /*----------------------------------
            ======================================
                    END CUSTOM FILTER
            ======================================
            -----------------------------------*/

            // handle row's checkbox click
            table.on('change', 'tbody > tr > td:nth-child(1) input[type="checkbox"]', function() {
                countSelectedRecords();
            });

            // handle filter submit button click
            table.on('click', '.filter-submit', function(e) {
                e.preventDefault();
                the.submitFilter();
            });

            // handle filter cancel button click
            table.on('click', '.filter-cancel', function(e) {
                e.preventDefault();
                the.resetFilter();
            });
        },

        initNumbering : function(){
            if(config.numbering)
                dataTable.column(0 ,{search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                    var start = dataTable.page.info().start;
                    cell.innerHTML = i + 1 + start;
                });
        },

        submitFilter: function() {
            the.clearAjaxParamsFilter();
            the.setAjaxParam("action", tableOptions.filterApplyAction);

            // SUBMIT FROM CUSTOM FILTER
            // set all filter param from filter-container
            console.log('satu');
            // console.log(tmpFilter);
            $('.filter-value', filterContainer).each(function(key, element){
                var param = $(element).tagsinput('items');
                var column= $(element).attr('column');
                var type  = ajaxParams.filterType[column];
                $.each(param, function(k,e){
                    the.addFilterParam(column, e);
                });
                
            }); 

            // set all filter param from filter-inner
            $('.filter-inner .filter-input', tableContainer).each(function(i, el){
                // nama field
                var type = $(el).attr('column-type');
                var column = $(el).attr('column');
                var filterInput = $(el);
                if(!column) 
                    return true;

                if (type=='range-date' || type=='range-numeric'){
                    var valFrom = filterInput.find('.value-from').val();
                    var valTo   = filterInput.find('.value-to').val();
                    // skip iteration jika value ga ada
                    if(!valFrom && !valTo) return true;
                    the.addFilterParam(column, {'value':[valFrom, valTo], 'text': valFrom+' - '+valTo});
                    
                } else if (type=='list') {
                    var value = filterInput.attr('value');
                    var text  = $(el).find('option:selected').text();
                    // skip iteration jika value ga ada
                    if(!value) return true;
                    the.addFilterParam(column, {'value':value, 'text': text});
                    
                } else {
                    var value = filterInput.val();
                    // skip iteration jika value ga ada
                    if(!value) return true;
                    the.addFilterParam(column, {'value':value, 'text': value});
                    
                }
            });

            // get all typeable inputs
            $('textarea.form-filter, select.form-filter, input.form-filter:not([type="radio"],[type="checkbox"])', table).each(function() {
                the.setAjaxParam($(this).attr("name"), $(this).val());
            });

            // get all checkboxes
            $('input.form-filter[type="checkbox"]:checked', table).each(function() {
                the.addAjaxParam($(this).attr("name"), $(this).val());
            });

            // get all radio buttons
            $('input.form-filter[type="radio"]:checked', table).each(function() {
                the.setAjaxParam($(this).attr("name"), $(this).val());
            });
            console.log(ajaxParams.filter);
            dataTable.ajax.reload();
        },

        resetFilter: function() {
            $('textarea.form-filter, select.form-filter, input.form-filter', table).each(function() {
                $(this).val("");
            });
            $('input.form-filter[type="checkbox"]', table).each(function() {
                $(this).attr("checked", false);
            });
            the.clearAjaxParams();
            the.addAjaxParam("action", tableOptions.filterCancelAction);
            dataTable.ajax.reload();
        },

        getSelectedRowsCount: function() {
            return $('tbody > tr > td:nth-child(1) input[type="checkbox"]:checked', table).size();
        },

        getSelectedRows: function() {
            var rows = [];
            $('tbody > tr > td:nth-child(1) input[type="checkbox"]:checked', table).each(function() {
                rows.push($(this).val());
            });

            return rows;
        },

        setAjaxParam: function(name, value) {
            ajaxParams[name] = value;
        },

        addAjaxParam: function(name, value) {
            if (!ajaxParams[name]) {
                ajaxParams[name] = [];
            }

            skip = false;
            for (var i = 0; i < (ajaxParams[name]).length; i++) { // check for duplicates
                if (ajaxParams[name][i] === value) {
                    skip = true;
                }
            }

            if (skip === false) {
                ajaxParams[name].push(value);
            }
        },

        clearAjaxParams: function(name, value) {
            ajaxParams = {};
        },

        clearAjaxParamsFilter : function() {
            $.each(ajaxParams.filter,function(i,e){
                ajaxParams.filter[i] = [];
            });
            ajaxParams.filter = {};
        },

        addFilterParam : function(name, value){

            if (ajaxParams.filter[name]==undefined){
                ajaxParams.filter[name] = [];
            }
            //ajaxParams.filter[name][size] = value;
            ajaxParams.filter[name].push(value);
        },

        checkFilterParam : function(name, value){
            var items = $('.filter-value-'+name, filterContainer).tagsinput('items');
            var ret = false;

            $(items).each(function(key, val){
                if(val['text']==value){
                    ret = true; // berarti ditemukan di list
                }
            });
            return ret;
            
        },

        getDataTable: function() {
            return dataTable;
        },

        getTableWrapper: function() {
            return tableWrapper;
        },

        gettableContainer: function() {
            return tableContainer;
        },

        getTable: function() {
            return table;
        }

    };

};
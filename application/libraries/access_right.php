<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class access_right {

    public $exception = array('source','dashboard_empty','repair_db', 'git', 'api', 'machine_transaction#coin_out');

    public function __construct() {
        
    }

    private $role;

    public function getMenu(){
        $CI = &get_instance();
        $menu = $CI->uri->segment(1);
        
        if($CI->uri->segment(2))
            $menu.='/'.$CI->uri->segment(2);
        //echo $menu;
        return $menu;

    }

    private function countMenu($menu){
        if($menu['child']) {
            $cnt = 0;
            foreach($menu['child'] as $child)
                $cnt+= $this->countMenu($child);
            return $cnt;
        } else {
            return 1;
        }
    }

    public function menu() {
        $ci =& get_instance();
        $ci->load->model(array('roles_model','menu_group_model','menu_model','user_model','config_model'));

        $currentMenu = $this->getMenu();

        $userId = $ci->session->userdata('user_id');
        $user = $ci->user_model->getById($userId)->row();
        $groupId = $user->grup_id;

        $menuTemp = array();
        $menuTemp = $ci->roles_model
                    ->with(array('menu','group_menu'))
                    ->select('kd_menu, menu.kd_grup_menu, nama_menu, url, menu.icon, group_menu.icon as group_icon, nama_grup_menu')
                    ->where('this.grup_id', $groupId)
                    ->where('menu.kd_menu IS NOT NULL')
                    ->order_by('group_menu.urutan')
                    ->order_by('menu_urutan')
                    ->get()->result();

        // pengelompokan berdasarkan group menu
        $groups = array();
        $menus  = array();
        $flagActive = true;
        foreach ($menuTemp as $key => $menu) {
            if ($flagActive && $menu->url==$currentMenu){
                $groups[$menu->kd_grup_menu]['active']  = 'active';    
                $flagActive = false;
            }

            $groups[$menu->kd_grup_menu]['icon']    = $menu->group_icon;
            $groups[$menu->kd_grup_menu]['label']   = $menu->nama_grup_menu;
            $groups[$menu->kd_grup_menu]['menu'][]  = $menu;

            $menus[$menu->kd_menu] = $menu;
        }
        
        $tmp = $ci->config_model->get()->row()->menu;
        $layout = json_decode($tmp, true);

        // generate menu
        $con = '
                <div class="hor-menu ">
                    <ul class="nav navbar-nav">
                        <li class="'.($currentMenu=='dashboard' ? 'active' : '') .'">
                            <a href="'.base_url().'">
                                <i class="icon-home"></i> Dashboard
                            </a>
                        </li>
                ';
        //vdebug($menus);
        foreach ($layout as $head) {
            $group = $groups[$head['id']];
            //$group['menu'] = $head['child'];
            if(!$group) continue;

            $numMenu = sizeof($group['menu']);
            $numCols = ceil($numMenu/10);
            $colWidth= 12/$numCols;
            //vdebug($numCols);
            $menuChunk = array_chunk($group['menu'], 10);

            $con.= '<li class="menu-dropdown mega-menu-dropdown mega-menu-full '.$group['active'].'">';
            $con.= '<a data-toggle="dropdown" href="javascript:;" class="dropdown-toggle">
                        <i class="'.$group['icon'].'"></i> '.$group['label'].' <i class="fa fa-angle-down"></i>
                    </a>';
            $con.= '<ul class="dropdown-menu" style="min-width: 210px">';
            $con.= '    
                        <li>
                            <div class="mega-menu-content">
                                <div class="row">
                    ';
            // foreach ($menuChunk as $j => $menus) {
            //     $con.= '<div class="col-md-'.$colWidth.'">
            //                 <ul class="mega-menu-submenu">';
            //     foreach ($menus as $menu) {
            //         $con.= '<li>
            //                     <a href="'.site_url($menu->url).'" class="iconify">
            //                     <i class="'.$menu->icon.'"></i>
            //                     '.$menu->nama_menu.' </a>
            //                 </li>';    
            //     }

            //     $con.=      '</ul>
            //             </div>';
            // }

            $listMenu = array();
            
            foreach ((array)$head['child'] as $child1) {
                $menu = $menus[$child1['id']];

                if(!$menu) continue;

                if($child1['child']){
                    $listMenu[] = '<li><h3>'.$menu->nama_menu.'</h3></li>';

                    foreach ($child1['child'] as $child2) {
                        $menu = $menus[$child2['id']];
                        if(!$menu) continue;
                        $listMenu[] = '<li>
                                <a href="'.site_url($menu->url).'" class="iconify">
                                <i class="'.$menu->icon.'"></i>
                                '.$menu->nama_menu.' </a>
                            </li>';    
                    }

                }else{
                    $listMenu[] = '<li>
                                <a href="'.site_url($menu->url).'" class="iconify">
                                <i class="'.$menu->icon.'"></i>
                                '.$menu->nama_menu.' </a>
                            </li>';
                }
            }

            $menuChunk = array_chunk($listMenu, 11);
            foreach ($menuChunk as $key => $value) {
                $con.= '<div class="col-md-4"><ul class="mega-menu-submenu">';
                foreach ($value as $val)
                    $con.= $val;
                $con.= '</ul></div>';
            }

            $con.=              '</div>';
            $con.=          '</div>';   
            $con.=      '</li>';
            $con.= '</ul>';
            $con.= '</li>';
        }
        
        $con.= '</ul>';
        $con.= '</div>';
        
        return $con;

    }

    public function menuOld() {
        $CI = &get_instance();
        $CI->load->model('roles_model');
        $CI->load->model('menu_group_model');

        $kd_roles = $CI->session->userdata('kd_roles');
        $url = $this->getMenu();
        $url = $CI->uri->segment(1);
        $active_group = '';

        $temp_menu = array();
        $roles = $CI->roles_model->get_data_roles(array('a.grup_id' => $kd_roles));
        foreach ($roles->result() as $value) {
            $pos = '';
            if ($url == $value->url) {
                $active_group = $value->kd_grup_menu;
                $pos = 'border-color:#0072c6';
            }

            $parent = !empty($value->kd_parent) ? $value->kd_parent : 0;
            $temp_menu[$value->kd_grup_menu][$parent][] = array(
                'kd_menu' => $value->kd_menu,
                'kd_parent' => $value->kd_parent,
                'kd_grup_menu' => $value->kd_grup_menu,
                'nama_menu' => $value->nama_menu,
                'url' => $value->url,
                'pos' => $pos
            );
        }
        $menu = "<li class='start active '>";
        $menu .= '<a href="index.html">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                    </a>
                </li>';
        $menu_group = $CI->menu_group_model->get_data();
        foreach ($menu_group->result() as $group) {
            if (isset($temp_menu[$group->kd_grup_menu])) {

                $expand = '';
                $actived = '';
                if ($active_group == $group->kd_grup_menu) {
                    $expand = ' class="expand" id="current" ';
                    $actived = 'class="active navAct"';
                }
                $menu .= '<li>
                    <a href="javascript:;">
                    <i class="'.($group->icon ? $group->icon : 'icon-basket').'"></i>
                    <span class="title">'.$group->nama_grup_menu.'</span>
                    <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">';
                if (isset($temp_menu[$group->kd_grup_menu][0])) {
                    foreach ($temp_menu[$group->kd_grup_menu][0] as $level1) {
                        $kd_menu1 = $level1['kd_menu'];
                        if (isset($temp_menu[$group->kd_grup_menu][$level1['kd_menu']])) {
                            $menu .= '<li>
                                        <a href="javascript:;">
                                        <i class="'.($level1['icon'] ? $level1['icon'] : 'icon-settings').'"></i> '. $level1['nama_menu'] .' <span class="arrow"></span>
                                        </a>
                                        <ul class="sub-menu">';
                            foreach ($temp_menu[$group->kd_grup_menu][$kd_menu1] as $level2) {
                                $kd_menu2 = $level2['kd_menu'];
                                if (isset($temp_menu[$group->kd_grup_menu][$kd_menu2])) {
                                    $menu .= '<li>
                                                <a href="javascript:;">
                                                <i class="icon-user"></i>
                                                '.$level2['nama_menu'].'<span class="arrow"></span>
                                                </a>
                                                <ul class="sub-menu">';
                                    foreach ($temp_menu[$group->kd_grup_menu][$kd_menu2] as $level3) {
                                        $menu .= '<li>
                                            <a href="' . $this->set_url($level3['url']) . '"><i class="icon-power"></i> '.$level3['nama_menu'].'</a>
                                            </li>';
                                    }
                                    $menu .= '</ul>
                                        </li>';
                                } else {
                                    $menu .= '<li>
                                                <a href="' . $this->set_url($level2['url']) . '">
                                                <i class="icon-user"></i>
                                                '.$level2['nama_menu'].'</a> </li>';
                                    //$menu .= '<li style="display:none;" class="gm_' . $kd_menu1 . '"><a href="' . $this->set_url($level2['url']) . '" style="padding-left:18px;font-weight:bold;' . $level2['pos'] . '"><i class="icon-menu"></i> ' . $level2['nama_menu'] . ' </a></li>';
                                }
                            
                            }
                            $menu .= '</ul>
                                </li>';
                        } else {
                            $menu .= '<li>
                                        <a href="' . $this->set_url($level1['url']) . '">
                                        <i class="'.($level1['icon'] ? $level1['icon'] : 'icon-settings').'"></i> '. $level1['nama_menu'] .'</a>
                                        </li>';
                            //$menu .= '<li><a href="' . $this->set_url($level1['url']) . '" style="font-weight:bold;color:#0072c6;' . $level1['pos'] . '"><i class="icon-menu"></i> ' . $level1['nama_menu'] . '</a></li>';
                        }
                    }
                    $menu .= '</ul></li>';
                }

                //$menu .= '  </ul>';
                //$menu .= '</li>';
            }
        }
        //$menu .= '</ul>';

        return $menu;
    }

    private function set_url($url) {

        switch ($url) {
            case 'moodle':
                $full_url = 'http://' . $_SERVER['HTTP_HOST'] . '/' . $url;
                break;
            default:
                $full_url = base_url($url);
                break;
        }
        return $full_url;
    }

    public function check($list_modul = array()) {
        $CI = &get_instance();

        // exception
        if(in_array($CI->uri->segment(1), $this->exception))
            return true;

        $CI->load->model('roles_model');
        $CI->load->model('menu_group_model');

        $user_id = $CI->session->userdata('kd_roles');

        if (count($list_modul) == 0) {

            $menu = $this->getMenu();
            
            $url = $CI->uri->segment(1);

            $otoritas = $CI->roles_model->with('menu')
                        ->where('grup_id', $user_id)
                        ->where('lower(url)', strtolower($url))
                        ->get()->row();
            // vdebug($user_id);
            // vdebug(strtolower($url));
            // vdebug($otoritas);
            // die;
            if(!$otoritas){
                // misal. menu yg diakses setting/main
                // search di db roles = setting
                // jika tidak ketemu maka mencoba cari di db roles = setting/main
                $otoritas = $CI->roles_model->with('menu')
                        ->where('grup_id', $user_id)
                        ->where('lower(url)', strtolower($menu))
                        ->get()->row();                
            }

            //$roles = $CI->roles_model->get_data_roles(array('a.grup_id' => $user_id, "lower(b.url) = '" . strtolower($url) . "'" => null));
            if ($otoritas) {
                $this->role[$url] = array(
                    'view' => $otoritas->is_view,
                    'add' => $otoritas->is_add,
                    'edit' => $otoritas->is_edit,
                    'delete' => $otoritas->is_delete,
                    'approve' => $otoritas->is_approve,
                    'import' => $otoritas->is_import,
                    'print' => $otoritas->is_print
                );
            } else {
                
                redirect('dashboard_empty');
                exit;
            }
        } else {
            foreach ($list_modul as $list) {
                $url = $list;

                $roles = $CI->roles_model->get_data_roles(array('a.grup_id' => $user_id, 'b.url' => $url));
                if ($roles->num_rows() > 0) {
                    $otoritas = $roles->row();
                    $this->role[$url] = array(
                        'view' => $otoritas->is_view,
                        'add' => $otoritas->is_add,
                        'edit' => $otoritas->is_edit,
                        'delete' => $otoritas->is_delete,
                        'approve' => $otoritas->is_approve,
                        'import' => $otoritas->is_import,
                        'print' => $otoritas->is_print
                    );
                }
            }
        }
    }

    public function real_otoritas($id = '', $redirect = false, $modul = '') {
        $CI = &get_instance();

        if (!empty($id)) {
            $url = $CI->uri->segment(1);
            $user_id = $CI->session->userdata('kd_roles');
            $otoritas = $CI->roles_model->with('menu')
                        ->where('grup_id', $user_id)
                        ->where('lower(url)', strtolower($url))
                        ->get()->row();
            if($otoritas){
                return TRUE;
            }else
            {
                return FALSE;

            }
        }
    }

    public function otoritas($id = '', $redirect = false, $modul = '') {
        $CI = &get_instance();

        // Exception  
        if (empty($modul)) {  
            $modul = $CI->uri->segment(1);    
            if(in_array($modul, $this->exception))
                return true;
        }

        if (!empty($id)) {

            if (empty($modul)) {
                $url = $this->getMenu();
                $url = $CI->uri->segment(1);
            } else {
                $url = $modul;
            }

            if (isset($this->role[$url][$id]) && $this->role[$url][$id] == 't') {
                $otoritas = true;
            } else {
                $otoritas = false;
            }            
        } else {
            $otoritas = false;
        }

        return $otoritas;
    }

    public function redirect() {
        redirect('denied');
    }

    public function activity_logs($activity, $description) {
        $CI = &get_instance();

        if (!isset($CI->log_activity_model)) {
            $CI->load->model('log_activity_model');
        }

        $message = '';
        switch ($activity) {
            case 'add':
                $message = 'Menambahkan data ' . $description;
                break;
            case 'view':
                $message = 'Menampilkan halaman ' . $description;
                break;
            case 'edit':
                $message = 'Mengubah data ' . $description;
                break;
            case 'delete':
                $message = 'Menghapus data ' . $description;
                break;
            case 'approve':
                $message = 'Menyetujui proses ' . $description;
                break;
            case 'not_approve':
                $message = 'Tidak menyetujui proses ' . $description;
                break;
            default:
                $message = $description;
                break;
        }

        $nik = $CI->session->userdata('nik_session');
        $username = $CI->session->userdata('username');
        $log = array(
            'nik' => $nik,
            'username' => $username,
            'activity' => $message
        );
        $CI->log_activity_model->create($log);
    }

}

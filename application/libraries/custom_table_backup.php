<?php

/* Created by : Muhammad Rizky Bonito
 * Upgrade by : Warman Suganda
 * Don't copy or modify source code or the whole of file without permission from the creator */

class custom_table {

    private $CI;
    private $id;
    private $data;
    private $kolom;
    private $style;
    private $link;
    private $header;
    private $title;
    private $limit;
    private $offset;
    private $page;
    private $numbering;
    private $width_colom;
    private $align;
    private $valign;
    private $message;
    private $cellspacing;
    private $cellpadding;
    private $border;
    private $info_cell;

    public function __construct() {
        $this->CI = &get_instance();
    }

    public function makeTable($dataTable) {
        if (!isset($dataTable->data))
            $dataTable->data = array();
        if (!isset($dataTable->kolom))
            $dataTable->kolom = 20;
        if (!isset($dataTable->style))
            $dataTable->style = "";
        if (!isset($dataTable->link))
            $dataTable->link = array();
        if (!isset($dataTable->header))
            $dataTable->header = array();
        if (!isset($dataTable->title))
            $dataTable->title = array();
        if (!isset($dataTable->limit))
            $dataTable->limit = 10;
        if (!isset($dataTable->offset))
            $dataTable->offset = 0;

        $this->config($dataTable);

        $nomor = $this->makeNumber($dataTable->limit, $dataTable->offset);
        $stringData = $this->createHeader();
        $stringData .= $this->createContentData($dataTable->data, $dataTable->link, $dataTable->title, $dataTable->status, $dataTable->kolom, $nomor);
        return $stringData;
    }

    function createHeader() {
        $row = $col = 0;
        $isFirst = $this->numbering;
        $rowHeader = count($this->header);
        $style = '';
        if (!empty($this->style))
            $style = "class = '$this->style'";
        $id = '';
        if (!empty($this->id))
            $id = "id='$this->id'";
        $stringData = "<table $id $style border='$this->border' cellspacing='$this->cellspacing' cellpadding='$this->cellpadding'><thead>";
        if ($rowHeader > 0) {
            foreach ($this->header as $dataHeader) {
                $col = count($dataHeader);
                $stringData .= "<tr>";
                if ($isFirst) {
                    $stringData .= "<th class='cell-first' rowspan='$rowHeader'>No.</th>";
                    $isFirst = false;
                }
                $colspan = 1;
                $rowspan = 2;
                for ($index = 0; $index < $col; $index+=3) {

                    $cell_first = '';
                    if ($index == 0 && !$this->numbering)
                        $cell_first = "class='cell-first'";

                    //$stringData .= "<th $cell_first colspan='$dataHeader[$colspan]' rowspan='$dataHeader[$rowspan]'>";
                    $stringData .= "<th $cell_first >";
                    $stringData .= "$dataHeader[$index]";
                    $stringData .= "</th>";
                    $colspan += 3;
                    $rowspan += 3;
                }
                $stringData .= "</tr>";
            }
        }
        $stringData .= "</thead>";
        return $stringData;
    }

    function createContentData($data, $link = array(), $title = array(), $total_status = array(), $kolom = 20, $nomor = 0) {
        $nomor += 1;
        $stringData = "";
        $row = count($data);
        $tempData = array();
        $indexBaris = $colspan = 0;
        $totalData = array();
        if (!empty($total_status)) {
            for ($i = 0; $i < $kolom; $i++) {
                $totalData[$i] = 0;
            }
        }
        if ($row != 0) {
            foreach ($data as $dataTable) {

                $pk = '';
                if (isset($dataTable['PRIMARY_KEY'])) {
                    $pk = $dataTable['PRIMARY_KEY'];
                    unset($dataTable['PRIMARY_KEY']);
                }

                $col = count($dataTable);
                $stringData .= "<tr><td>$nomor.</td>";
                $index = 0;
                $tempData[$indexBaris] = $dataTable;
                $colspan = 1;
                foreach ($dataTable as $dataKolom) {
                    $td = "<td>";
                    if (!empty($total_status)) {
                        if (!is_numeric($dataKolom)) {
                            $colspan++;
                        } else if ($total_status[$index]) {
                            $totalData[$index]+= $dataKolom;
                            $td = "<td align='right'>";
                        }
                    }
                    $stringData .= $td;
                    $c = '"';
                    if (!empty($link[$index])) {
                        $stringData .= "<a href='javascript:void(0);' onclick='drildown(" . $c . '#drildown_' . $nomor . $c . "," . $c . $link[$index] . $c . "," . $c . $pk . $c . ")'>" . "$dataKolom" . "</a>";
                    } else {
                        $stringData .= "$dataKolom";
                    }
                    $stringData .= "</td>";
                    $index++;
                }
                $stringData .= "</tr>";

                if (!empty($pk)) {
                    $clsp = $kolom + 1;
                    $stringData .= "<tr id='drildown_$nomor' status='close' style='display:none;'><td colspan='$clsp'>sss</td></tr>";
                }

                $indexBaris++;
                $nomor++;
            }

            if (!empty($total_status)) {
                $stringData .= $this->createTotal($total_status, $totalData, $colspan, $kolom);
            }
        }
        else
            $stringData .= "<tr><td colspan='$kolom' align='center'>Data tidak ditemukan</td></tr>";
        $stringData .= "</table>";
        return $stringData;
    }

    function createTotal($total_status, $totalData, $colspan, $kolom) {
        $stringData = "<tr>";
        $awal = true;
        $space = false;
        for ($i = 0; $i < $kolom; $i++) {
            if ($awal) {
                $stringData .= "<td colspan='$colspan' align='center'><b>Total</b></td>";
                $awal = false;
            } else if ($totalData[$i] != 0) {
                $stringData .= "<td align='right'><b>" . $totalData[$i] . "</b></td>";
                $space = true;
            } else if ($space) {
                $stringData .= "<td align='center'>-</td>";
            }
        }
        $stringData .= "</tr>";
        return $stringData;
    }

    function makeNumber($limit, $offset) {
        return ($limit * $offset);
    }

    private function config($dataTable) {
        if (!isset($dataTable->id))
            $dataTable->id = '';
        if (!isset($dataTable->data))
            $dataTable->data = array();
        if (!isset($dataTable->kolom))
            $dataTable->kolom = 20;
        if (!isset($dataTable->style))
            $dataTable->style = "";
        if (!isset($dataTable->link))
            $dataTable->link = array();
        if (!isset($dataTable->header))
            $dataTable->header = array();
        if (!isset($dataTable->title))
            $dataTable->title = array();
        if (!isset($dataTable->limit))
            $dataTable->limit = 10;
        if (!isset($dataTable->page))
            $dataTable->page = 0;
        if (!isset($dataTable->model))
            $dataTable->model = '';
        if (!isset($dataTable->numbering))
            $dataTable->numbering = true;
        if (!isset($dataTable->width_colom))
            $dataTable->width_colom = array();
        if (!isset($dataTable->align))
            $dataTable->align = array();
        if (!isset($dataTable->valign))
            $dataTable->valign = array();
        if (!isset($dataTable->message))
            $dataTable->message = 'Data tidak ditemukan';
        if (!isset($dataTable->cellspacing))
            $dataTable->cellspacing = 0;
        if (!isset($dataTable->cellpadding))
            $dataTable->cellpadding = 0;
        if (!isset($dataTable->border))
            $dataTable->border = 0;
        if (!isset($dataTable->info_cell))
            $dataTable->info_cell = true;

        $this->id = $dataTable->id;
        $this->data = $dataTable->data;
        $this->kolom = $dataTable->kolom;
        $this->style = $dataTable->style;
        $this->link = $dataTable->link;
        $this->header = $dataTable->header;
        $this->title = $dataTable->title;
        $this->limit = $dataTable->limit;
        $this->page = $dataTable->page;
        $this->offset = ($dataTable->page * $dataTable->limit) - $dataTable->limit;
        $this->model = $dataTable->model;
        $this->numbering = $dataTable->numbering;
        $this->width_colom = $dataTable->width_colom;
        $this->align = $dataTable->align;
        $this->valign = $dataTable->valign;
        $this->message = $dataTable->message;
        $this->cellspacing = $dataTable->cellspacing;
        $this->cellpadding = $dataTable->cellpadding;
        $this->border = $dataTable->border;
        $this->info_cell = $dataTable->info_cell;
    }

    public function generate($dataTable) {
        $this->config($dataTable);

        $total = 0;
        $data = $this->get_data();
        if ($data) {
            $total = $data['total'];
            $this->data = $data['rows'];
        }

        $table = $this->createHeader();
        $table .= $this->createContent();

        //return array('table' => $table, 'total' => $total, 'limit' => $this->limit);
        return $table;
    }

    public function get_data() {
        $model = $this->model;
        if (!empty($model)) {
            list($model_name, $function_name) = explode('->', $model);
            $m = $this->CI->{$model_name};

            $m->limit = $this->limit;
            $m->offset = $this->offset;
            return $m->{$function_name}();
        } else {
            return false;
        }
    }

    function createContent() {
        $content = '';
        $kolom = $this->kolom;
        $nomor = ($this->page - 1) * $this->limit;
        $data = $this->data;
        $content.='<tbody>';

        if (count($data) > 0) {

            foreach ($data as $row) {
                $nomor++;

                $pk = '';
                if (isset($row['primary_key'])) {
                    $pk = $row['primary_key'];
                    unset($row['primary_key']);
                }

                if (!empty($pk)) {
                    $content .= '<tr id="content_row_' . $pk . '">';
                } else {
                    $content .= '<tr>';
                }

                $jml_colom = 0;

                // Jika akan menampilkan penomoran
                if ($this->numbering) {
                    $jml_colom++;
                    $content .= '<td class="cell-first" style="text-align:center;">' . $nomor . '</td>';
                }
                
                $index = 0;
                foreach ($row as $key => $value) {
                    $jml_colom++;

                    $align = (isset($this->align[$key])) ? 'align="' . $this->align[$key] . '"' : '';
                    $valign = (isset($this->valign[$key])) ? 'valign="' . $this->valign[$key] . '"' : '';
                    $width = (isset($this->width_colom[$key])) ? 'width="' . $this->width_colom[$key] . '"' : '';

                    $cell_first = '';
                    if ($index == 0 && !$this->numbering)
                        $cell_first = 'class="cell-first"';
                    
                    $cell = '';
                    if ($this->info_cell) {
                        $cell = 'cell="' . $key . '" ';
                    }

                    $content .= '<td ' . $cell_first . ' ' . $cell . ' ' . $align . ' ' . $valign . ' ' . $width . '>';

                    if (isset($this->link[$key])) {
                        $content .= '<a href="javascript:void(0)" id="drildown_key_' . $this->id . '_' . $pk . '" rel="' . $pk . '" data-source="' . $this->link[$key] . '" parent="' . $this->id . '" onclick="drildown(this.id)">' . $value . '</a>';
                    } else {
                        $content .= $value;
                    }

                    $content .= '</td>';
                    $index++;
                }

                $content .= '</tr>';

                if (!empty($pk))
                    $content .= '<tr id="drildown_row_' . $this->id . '_' . $pk . '" status="close" style="display:none;"><td style="background: #fff;" colspan="' . $jml_colom . '">&nbsp;</td></tr>';
            }
        } else {
            $content .= '<tr><td class="cell-first" align="center" colspan="' . $kolom . '">' . $this->message . '</td></tr>';
        }
        $content.='</tbody>';

        $content .= '</table>';

        return $content;
    }

}

/* End of file custom_table.php */
/* Location: ./application/libraries/custom_table.php */
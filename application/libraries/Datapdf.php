<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
  /**
  * Ignited Datatables
  *
  * This is a wrapper class/library based on the native Datatables server-side implementation by Allan Jardine
  * found at http://datatables.net/examples/data_sources/server_side.html for CodeIgniter
  *
  * @package    CodeIgniter
  * @subpackage libraries
  * @category   library
  * @version    2.0 <beta>
  * @author     Vincent Bambico <metal.conspiracy@gmail.com>
  *             Yusuf Ozdemir <yusuf@ozdemir.be>
  * @link       http://ellislab.com/forums/viewthread/160896/
  */
require('pagegroup.php');
require_once("connect.php");


class Datapdf extends PDF_PageGroup{
	private $header;
	public $title;
	private $data;


	public function getData(){
		return $this->LoadData('newfile.txt');
	}

	public function generatePDF($header, $data, $id_user, $bulanke, $tahunke){
        // Column widths
        $w = array(30,16,25,30,30,15);
        // Header
        for($i=0;$i<count($header);$i++)
            $this->Cell($w[$i],10,$header[$i],1,0,'C');
        $this->Ln();
        // Data
        $x = $this->GetX();
        $y = $this->GetY();

        $i=0;
        foreach($data as $row)
        {
        	$x=$this->GetX();
        	foreach ($header as $key => $value) {

	            $this->setXY($x,$y);
	            $this->MultiCell($w[$key],10,$row[$key],0,L,FALSE);        
				$x = $x + $w[$key];
        	}

            $y = $y + 40;
            $i++;

            if($i % 5 != 0){
                continue;
            }else{
                //break;
                $x=10;
                $y=65;
                $pdf = new Datapdf();
                $pdf->StartPageGroup();
                $this->AddPage();
                $this->setAttribute($id_user,$bulanke, $tahunke);
                
                for($j=0;$j<count($header);$j++)
                    $this->Cell($w[$j],10,$header[$j],1,0,'C');
                $this->Ln();

                $this->setXY($x,$y);
            }
            
        }

	}

	public function setKop($arr = array()){
        $this->SetFont('Arial','',10);
        //$this->MultiCell(20,10,'Nama : '.$id_user,0,1,'C');
        
        $nama = "";
        $alamat_pelanggan = "";
        $no_telp = "";
        $email = "";


        $yattr = $this->GetY();
        $xattr = $this->GetX();

        foreach ($arr as $key => $value) {

	        $this->MultiCell(50,5,$key.'       ',0,L,FALSE);
	        $this->setXY($xattr+50,$yattr+$temp);
	        $temp = $temp + 5;
	        $this->MultiCell(40,5,': '.$nama,0,L,FALSE);

	        $this->setXY($xattr,$yattr+$temp);
        }


	}

    function setAttribute($id_user, $bulanke, $tahunke){

        $this->SetFont('Arial','',10);
        //$this->MultiCell(20,10,'Nama : '.$id_user,0,1,'C');
        $x = $this->GetX();
        $y = $this->GetY();
        $query_alamat = "select * from user where username='".$id_user."'";
        $eksekusi_query_alamat = mysql_query($query_alamat);
        
        $nama = "";
        $alamat_pelanggan = "";
        $no_telp = "";
        $email = "";

        while($rows=mysql_fetch_array($eksekusi_query_alamat)){
            $alamat_pelanggan = $rows["alamat"];
            $nama = $rows["nama"];
            $no_telp = $rows["no"];
            $email = $rows["email"];
        }

        $yattr = $this->GetY();
        $xattr = $this->GetX();

        $this->MultiCell(30,5,'Nama Pelanggan       ',0,L,FALSE);
        $this->setXY($xattr+30,$yattr);
        $this->MultiCell(40,5,': '.$nama,0,L,FALSE);

        $this->setXY($xattr,$yattr+5);
        $this->MultiCell(30,5,'Nomor Hp       ',0,L,FALSE);
        
        $this->setXY($xattr+30,$yattr+5);
        $this->MultiCell(40,5,': '.$no_telp,0,L,FALSE);

        $this->setXY($xattr,$yattr+10);
        $this->MultiCell(30,5,'Email       ',0,L,FALSE);
        
        $this->setXY($xattr+30,$yattr+10);
        $this->MultiCell(40,5,': '.$email,0,L,FALSE);

        $this->setXY($xattr,$yattr+15);
        $this->MultiCell(30,5,'No HP Butik      ',0,L,FALSE);
        
        $this->setXY($xattr+30,$yattr+15);
        $this->MultiCell(40,5,': 087822844910',0,L,FALSE);

        $this->setXY($xattr,$yattr+20);
        $this->MultiCell(30,5,'Untuk      ',0,L,FALSE);
        
        $this->setXY($xattr+30,$yattr+20);
        $this->MultiCell(40,5,': Bulan '.$bulanke.' Tahun '.$tahunke.'',0,L,FALSE);

    }

    public function Header()
    {
        // Logo
        //$this->Image('images/domba1.jpg',10,6,30);
        // Arial bold 15
        $this->SetFont('Arial','B',15);
        // Move to the right
        $this->Cell(80);

        // Title
        $this->Cell(30,10,$this->title,0,1,'C');
        // Line break
        $this->Ln(10);

    }


    function Footer()
    {
        $query="select id_transaksi from transaksi";
        $panjangquery = mysql_num_rows(mysql_query($query));
        // Position at 1.5 cm from bottom
        $this->SetY(-15);
        // Arial italic 8
        $this->SetFont('Arial','I',8);
        // Page number
        $this->Cell(0,10,'Page '.$this->PageNo().'/'.round($panjangquery / 5),0,0,'C');
    }

    // Load data
    function LoadData($file)
    {
        // Read file lines
        $lines = file($file);
        $data = array();
        foreach($lines as $line)
            $data[] = explode(';',trim($line));
        return $data;
    }


    // Better table
    function ImprovedTable($header, $data, $id_user, $bulanke, $tahunke)
    {
        // Column widths
        $w = array(40,16,25,30,30,15, 15);
        // Header
        for($i=0;$i<count($header);$i++)
            $this->Cell($w[$i],10,$header[$i],1,0,'C');
        $this->Ln();
        // Data
        $x = $this->GetX();
        $y = $this->GetY();

        $i=0;
        foreach($data as $row)
        {
            $this->setXY($x,$y);
            $this->MultiCell($w[0],10,$row[0],0,L,FALSE);        
            $this->setXY($x+40,$y);
            $this->MultiCell($w[1],10,$row[1],0,L,FALSE);
            $this->setXY($x+56,$y);
            $this->MultiCell($w[2],10,$row[2],0,L,FALSE);
            $this->setXY($x+81,$y);
            $this->MultiCell($w[3],10,$row[3],0,L,FALSE);
            $this->setXY($x+111,$y);
            $this->MultiCell($w[4],10,$row[4],0,R,FALSE);
            $this->setXY($x+141,$y);
            $this->MultiCell($w[5],10,$row[5],0,R,FALSE);
            $this->setXY($x+156,$y);
            $this->MultiCell($w[6],10,$row[6],0,R,FALSE);
            $this->setXY($x+171,$y);
            $this->MultiCell($w[7],10,$row[7],0,R,FALSE);
            $y = $y + 40;
            $i++;

            if($i % 5 != 0){
                continue;
            }else{
                //break;
                $x=10;
                $y=65;
                $pdf = new Datapdf();
                $pdf->StartPageGroup();
                $this->AddPage();
                $this->setAttribute($id_user,$bulanke, $tahunke);
                
                for($j=0;$j<count($header);$j++)
                    $this->Cell($w[$j],10,$header[$j],1,0,'C');
                $this->Ln();

                $this->setXY($x,$y);
            }
            
        }

        //Closing line
        //$this->Cell(array_sum($w),0,'','T');
    }



    function createFile($id_user, $bulanke, $tahunke){
        include "connect.php";
        $query = "select t.id_transaksi, b.merkbaju, p.nomor_resi, p.status, p.alamat_tujuan, t.jumlah, t.harga, t.total from transaksi t INNER JOIN pengiriman p ON(p.id_transaksi=t.id_transaksi) INNER JOIN barang b ON(t.id_barang=b.id_baju) where t.id_user ='".$id_user."' AND MONTH(t.id_transaksi)='".$bulanke."' AND YEAR(t.id_transaksi)='".$tahunke."' ORDER BY id_transaksi ASC";
        $eksekusiquery = mysql_query($query);
        $myfile = fopen("newfile.txt", "w") or die("Unable to open file!");
        
        while($rows=mysql_fetch_array($eksekusiquery)){
            $txt = $rows["id_transaksi"].";".$rows["merkbaju"].";".$rows["nomor_resi"].";".$rows["status"].";".$rows["alamat_tujuan"].";".$rows["jumlah"].";".$rows["harga"].";".$rows["total"]."\n";
            fwrite($myfile, $txt);                
        }
        fclose($myfile);    
    }

}
/* End of file Datatables.php */
/* Location: ./application/libraries/Datatables.php */

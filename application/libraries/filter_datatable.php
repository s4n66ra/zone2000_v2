<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class filter_datatable
{
	protected $ci;

	public $filter = array();

	public function __construct()
	{
        $this->ci =& get_instance();
	}


	public function generate(){

		// 0:column. 1:label. 2:tipe : (text, numeric, date, list, range-date, range-numeric)
		$filter 	= '<select class="filter-column form-control bs-select">';
		$filterval 	= '<div class="">';
		foreach ($this->filter as $val) {
			// combobox filter
			$filter.= '<option value="'.$val[0].'" type="'.$val[2].'">'.$val[1].'</option>';

			// filter value container
			$filterval.= '
						<div class="col-md-4">
							'.$val[1].'
						</div>

						<div class="col-md-8">
						</div>

						';


		}
		$filter.= '</select>';
		$filterval.= '<div>';

		// container filter value
		

		

		return '
			<div class="filter-datatable">
				<div class="row">
					<div class="col-md-4">
					'.$filter.'
					</div>
					<div class="col-md-4">
						<input type="text" class="form-control">
					</div>
					<div class="col-md-4">
						<a class="btn yellow col-md-12"> Add Filter </a>
					</div>
				</div>

				<div class="row">
					'.$filterval.'
				</div>
			</div>
		';
	}

}

/* End of file filter_datatable.php */
/* Location: ./application/libraries/filter_datatable.php */

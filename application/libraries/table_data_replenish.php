<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Table_data_replenish
{
	protected $ci;
	public $table;
	public $button;
	public $param;
	public $type_id;
	public $num_input = 1;
	public $list_item;
	public $list_bundle;

	public function __construct()
	{
        $this->ci =& get_instance();
        $this->ci->load->library('custom_table');
        $this->table = new custom_table();
	}

	public function generateTable(){
		$this->ci->load->model('mesin_model');
		$mesinModel = $this->ci->mesin_model;
		if(!$this->list_item)
			$this->list_item = $mesinModel->options2();

		$temp = array();
		if(!$this->table->data){
/*			$temp[] = form_dropdown('machine', $this->list_item, '', 
                    	'class="bs-select list-item col-md-12"  data-live-search="true" onchange="refreshListTransaction(this)" data-source="'.site_url('api/machine/detail/last-transaction').'"'
                	);
*/			//$temp[] = '<div class="data-info"></div>';

           $temp[] = '<input type="text" id="redemp_list_item_id" name="data[item_id]" data-live-search="true" onchange="refreshListTransaction(this)" class="form-control list-item col-md-12" data-source="'.site_url('api/machine/detail/last-transaction').'"/>';
			
			for($i=0;$i<$this->num_input;$i++) {
				$temp[] =  form_input('name', '0', 'class="format-number data-input disposable form-control col-md-12" data-type="text"')
            		    	.'<a href="javascript:;" id="username" data-type="text" data-pk="1" data-original-title="Enter Ticket Qty" class="format-number data-qty" hidden> 0 </a>';
    		}

            $temp[] = view::button_add_table()
            		  .'<a class="btn red-pink btn-table-delete" style="display:none;"><i class="fa fa-trash"></i></a>';

            $this->table->data = array($temp);
		}	

		return $this->table->generateWithWrapper();
	}


	public function generateHeader(){
		$ci =& get_instance();

		if($this->list_bundle)
			$dropdown = form_dropdown('machine', $this->list_bundle, $ci->session->userdata('store_id'), 
                    	'class="bs-select list-bundle col-md-12"  data-live-search="true" disabled'
                	);
		$content = '
			<div class="row" style="padding:15px 0 15px 0;">
				<div class="col-md-6">'
				.$dropdown.
				'</div>
				<div class="col-md-6">
					<div class="btn-group pull-right">
					'.view::button_save_transaction().'
					</div>
				</div>
			</div>
		';
		return $content;
	}

	public function generateOptions(){
		if($this->list_bundle)
			$dropdown = form_dropdown('machine', $this->list_bundle, '', 
                    	'class="bs-select list-bundle col-md-12"  data-live-search="true"'
                	);

		$content = '
			<div class="row" style="padding:15px 0 15px 0;">
				<div class="col-md-6">'
				.$dropdown.
				'</div>
			</div>
		';
		return $content;
	}

	public function generateParameter(){
		$content = '';
		foreach ((array) $this->param as $key => $value) {
			$content.= '<input type="hidden" class="data-param" param-name="'.$key.'" value="'.$value.'">';
		}
		return $content;
	}

	public function generate(){
		$content = '';
		$content.= '<div class="component-table-data" type-id="'.$this->param['type_id'].'">';
		$content.= $this->generateParameter();
		$content.= $this->generateHeader();
		$content.= $this->generateTable();
		$content.= '</div>';
		return $content;
	}

	

}

/* End of file table_data.php */
/* Location: ./application/libraries/table_transaction.php */

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Custom_tab
{
	protected $ci;
	public $tab = array();

	public function __construct()
	{
        $this->ci =& get_instance();
	}

	public function generate(){

		$count = 0;
		foreach ((array)$this->tab as $key => $value) {
			$title.= '
					<li class="'.($count==0 ? 'active' : '').'">
						<a href="#tab-'.$key.'" data-toggle="tab">
						'.$value['title'].'</a>
					</li>
				';

			$body.= '
				<div class="tab-pane '.($count==0 ? 'active' : '').'" id="tab-'.$key.'">
					'.$value['body'].'
				</div>
			';

			$count++;
		}
		$content.= '
			<div class="tabbable-custom show-overflow">
				<ul class="nav nav-tabs ">
					'.$title.'
				</ul>

				<div class="tab-content">
					'.$body.'
				</div>

			</div>
			';

		return $content;

	}
	

}

/* End of file custom_tab.php */
/* Location: ./application/libraries/custom_tab.php */

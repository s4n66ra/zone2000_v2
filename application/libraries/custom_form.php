<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class custom_form{
	public $ci;
	public $url_form;
	public $id;
	public $data = array();
	public $param = array();
	public $portlet = false;
	public $button_back = false;
	public $button_save = true;
	
	public $action_fullscreen = true;

	public $buttonInForm = false;
	public $close_modal  = true;
	public $only_form	 = false;
	public $readonly 	 = false;
	public $type 		 = NULL;

	public $button = array();


	public function __construct(){
		$this->ci =& get_instance();
	}

	public function produceParam(){
		$content 	= '';
		$param 		= $this->param;

		$param['id'] = $this->id;

		if($this->close_modal)
			$param['close_modal'] 	= $this->close_modal;

		if($this->data)
			$param['data']		= $this->data;

		$param['form'] 			= $this->url_form;

		// jika true. maka hanya akan generate form saja tanpa wrapper
		$param['only_form']		= $this->only_form;
		// form_open berarti tag form ada di page_form bukan di form nya
		$param['form_open']		= $this->form_open;

		if ($this->portlet){
			$param['portlet']		= true;
			$param['no_portlet'] 	= false;				
		} else {
			$param['portlet']		= false;
			$param['no_portlet'] 	= true;
		}

		if ($this->action_fullscreen)
			$param['action_fullscreen'] = $this->action_fullscreen;

		switch ($this->type) {
			case 'delete':
				$this->readonly = true;
				$button_group[] = view::button_back();
				$button_group[] = view::button_delete_confirm_form();
				break;
			
			default:
				if ($this->button_back)
					$button_group[] 	= view::button_back();

				if ($this->button)
					foreach ($this->button as $btn)
						$button_group[] = $btn;

				if ($this->button_save)
					$button_group[] 	= view::button_save_form();
				break;
		}

		if ($this->buttonInForm)
			// menempatkan button di dalam form. bukan di page_form
			$param['button_group_inform'] 	= view::render_button_group($button_group);
		else
			$param['button_group'] 	= view::render_button_group($button_group);

		return $param;
	}

	public function generateJavascript(){
		$content = '';
		if ($this->readonly)
			$content.= "
				<script>
					$('#div-".$this->id." input[type=text]').attr('disabled','disabled');
					$('#div-".$this->id." select').attr('disabled','disabled');
					$('#div-".$this->id." textarea').attr('disabled','disabled');
				</script>
			";
		return $content;
	}

	public function generate(){
		$content = $this->ci->twig->render('base/page_form.tpl', $this->produceParam());
		$content.= $this->generateJavascript();
		return $content;
	}

	public function generateOnlyForm(){
		$this->only_form = true;
		return $this->generate();
	}
}


<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
  /**
  * Ignited Datatables
  *
  * This is a wrapper class/library based on the native Datatables server-side implementation by Allan Jardine
  * found at http://datatables.net/examples/data_sources/server_side.html for CodeIgniter
  *
  * @package    CodeIgniter
  * @subpackage libraries
  * @category   library
  * @version    2.0 <beta>
  * @author     Vincent Bambico <metal.conspiracy@gmail.com>
  *             Yusuf Ozdemir <yusuf@ozdemir.be>
  * @link       http://ellislab.com/forums/viewthread/160896/
  */
class Datatable extends MY_Model {
   /**
   * Global container variables for chained argument results
   *
   */
   public $ci;
   public $table;
   public $distinct;
   public $group_by       = array();
   public $select         = array();
   public $joins          = array();
   public $columns        = array();
   public $where          = array();
   public $or_where       = array();
   public $where_in       = array();
   public $like           = array();
   public $filter         = array();
   public $add_columns    = array();
   public $edit_columns   = array();
   public $unset_columns  = array();
   public $with           = array();
   public $align = array();
   public $headerAlign = array();

   public $style          = 'table-hover';
   public $showfilter_id  = '';

   //UNTUK MENAMPILKAN MENU DI ATAS DATATABLE
   public $dropdowncabang = false;
   public $dropdowncabangpenjualan = false;
   public $startdate = false;
   public $enddate = false;

   public $submit_filter_cabang = false;
   // ROFID

   public $header = array();
   public $source;
   public $id;
   public $add_javascript;
   
   // datatable bisa didrag and drop untuk ordering
   public $rowReordering = false;
   public $url_action_reordering; 

   public $btn_refresh = true;

   public $pagination = true;

   public $checklist = false;
   public $url_action_group;
   public $callback_group_submit;
   public $caption_btn_group = 'Submit';

   public $numbering   = false;
   public $selectLater = false;
   public $selectFromIndex = false;

   public $isDataSet = false;
   public $isScrollable = true;
   public $model;
   public $data = array(); // data yang didapatkan setelah eksekusi query;

   // filter form untuk datatable ini. 
   public $dataFilter = array();
   // filter yang di dalam table
   public $innerFilter = array();
   private $separatorFilter = '-';


    public function reset(){
      $this->header = array();
      $this->source = NULL;
      $this->id = NULL;
      $this->checklist = false;
      $this->url_action_group = NULL;
      $this->callback_group_submit = NULL;
      $this->caption_btn_group = 'Submit';
      $this->numbering = false;
    }

    /**
    * Copies an instance of CI
    */
    public function __construct()
    {
      $this->ci =& get_instance();
    }

    /**
    * If you establish multiple databases in config/database.php this will allow you to
    * set the database (other than $active_group) - more info: http://ellislab.com/forums/viewthread/145901/#712942
    */
    public function set_database($db_name)
    {
      $db_data = $this->ci->load->database($db_name, TRUE);
      $this->ci->db = $db_data;
    }

    /**
    * Generates the SELECT portion of the query
    *
    * @param string $columns
    * @param bool $backtick_protect
    * @return mixed
    */
    public function select($columns, $backtick_protect = TRUE)
    {

      if($this->numbering) $this->columns[] = 'no';

      $firstColumn;
      foreach($this->explode(',', $columns) as $val)
      {
        if(!$firstColumn) $firstColumn = $val;
        $column = trim(preg_replace('/(.*)\s+as\s+(\w*)/i', '$2', $val));
        $column = preg_replace('/.*\.(.*)/i', '$1', $column); // get name after `.`
        $this->columns[] =  $column;
        $this->select[$column] =  trim(preg_replace('/(.*)\s+as\s+(\w*)/i', '$1', $val));
      }

      if($this->numbering && $firstColumn!='*') {
        $columns = $firstColumn.' as no ,'.$columns;
      }

      if (!($this->selectLater))
        parent::select($columns, $backtick_protect);

      return $this;
    }

    /**
    * Generates the DISTINCT portion of the query
    *
    * @param string $column
    * @return mixed
    */
    public function distinct($column)
    {
      $this->distinct = $column;
      parent::distinct($column);
      return $this;
    }

    /**
    * Generates a custom GROUP BY portion of the query
    *
    * @param string $val
    * @return mixed
    */
    public function group_by($val)
    {
      $this->group_by[] = $val;
      parent::group_by($val);
      return $this;
    }

    /**
    * Generates the JOIN portion of the query
    *
    * @param string $table
    * @param string $fk
    * @param string $type
    * @return mixed
    */
    public function join($table, $fk, $type = NULL)
    {
      $this->joins[] = array($table, $fk, $type);
      parent::join($table, $fk, $type);
      return $this;
    }

    /**
    * Generates the WHERE portion of the query
    *
    * @param mixed $key_condition
    * @param string $val
    * @param bool $backtick_protect
    * @return mixed
    */
    public function where($key_condition, $val = NULL, $backtick_protect = TRUE)
    {
      $this->where[] = array($key_condition, $val, $backtick_protect);
      parent::where($key_condition, $val, $backtick_protect);
      return $this;
    }

    /**
    * Generates the WHERE portion of the query
    *
    * @param mixed $key_condition
    * @param string $val
    * @param bool $backtick_protect
    * @return mixed
    */
    public function or_where($key_condition, $val = NULL, $backtick_protect = TRUE)
    {
      $this->or_where[] = array($key_condition, $val, $backtick_protect);
      parent::or_where($key_condition, $val, $backtick_protect);
      return $this;
    }
    
    /**
    * Generates the WHERE IN portion of the query
    *
    * @param mixed $key_condition
    * @param string $val
    * @param bool $backtick_protect
    * @return mixed
    */
    public function where_in($key_condition, $val = NULL, $backtick_protect = TRUE)
    {
      $this->where_in[] = array($key_condition, $val, $backtick_protect);
      parent::where_in($key_condition, $val, $backtick_protect);
      return $this;
    }

    public function with($with){
      if (is_array($with))
        $this->with = array_merge($this->with, $with);
      else
        $this->with[] = $with;
      parent::with($with);

      return $this;
    }

    /**
    * Generates the WHERE portion of the query
    *
    * @param mixed $key_condition
    * @param string $val
    * @param bool $backtick_protect
    * @return mixed
    */
    public function filter($key_condition, $val = NULL, $backtick_protect = TRUE)
    {
      $this->filter[] = array($key_condition, $val, $backtick_protect);
      return $this;
    }

    /**
    * Generates a %LIKE% portion of the query
    *
    * @param mixed $key_condition
    * @param string $val
    * @param bool $backtick_protect
    * @return mixed
    */
    public function like($key_condition, $val = NULL, $backtick_protect = TRUE)
    {
      $this->like[] = array($key_condition, $val, $backtick_protect);
      parent::like($key_condition, $val, $backtick_protect);
      return $this;
    }

    /**
    * Sets additional column variables for adding custom columns
    *
    * @param string $column
    * @param string $content
    * @param string $match_replacement
    * @return mixed
    */
    public function add_column($column, $content, $match_replacement = NULL)
    {
      $this->add_columns[$column] = array('content' => $content, 'replacement' => $this->explode(',', $match_replacement));
      return $this;
    }

    /**
    * Sets additional column variables for editing columns
    *
    * @param string $column
    * @param string $content
    * @param string $match_replacement
    * @return mixed
    */
    public function edit_column($column, $content, $match_replacement)
    {
      $this->edit_columns[$column][] = array('content' => $content, 'replacement' => $this->explode(',', $match_replacement));
      return $this;
    }

    /**
    * Unset column
    *
    * @param string $column
    * @return mixed
    */
    public function unset_column($column)
    {
      $column=explode(',',$column);
      $this->unset_columns=array_merge($this->unset_columns,$column);
      return $this;
    }

    /**
    * Builds all the necessary query segments and performs the main query based on results set from chained statements
    *
    * @param string $output
    * @param string $charset
    * @return string
    */
    public function generate($output = 'json', $charset = 'UTF-8')
    {
      if(strtolower($output) == 'json')
        $this->get_paging();

      $this->get_ordering();

      // sebelum filtering. copy object 
      $this->get_filtering();

      return $this->produce_output(strtolower($output), strtolower($charset));
    }

    /**
    * Generates the LIMIT portion of the query
    *
    * @return mixed
    */
    private function get_paging()
    {
      $iStart = $this->ci->input->post('start');
      $iLength = $this->ci->input->post('length');

      if($iLength != '' && $iLength != '-1')
        parent::limit($iLength, ($iStart)? $iStart : 0);
    }

    /**
    * Generates the ORDER BY portion of the query
    *
    * @return mixed
    */
    private function get_ordering()
    {

      $Data = $this->ci->input->post('columns');

      if ($this->ci->input->post('order'))
        foreach ($this->ci->input->post('order') as $key)
          if($this->check_cType())
            parent::order_by($Data[$key['column']]['data'], $key['dir']);
          else
            parent::order_by($this->columns[$key['column']] , $key['dir']);

    }

   private function get_custom_filtering(){

   }

    /**
    * Generates a %LIKE% portion  of the query
    *
    * @return mixed
    */
    private function get_filtering()
    {

      $mColArray = $this->ci->input->post('columns');

      $sWhere = '';
      $search = $this->ci->input->post('search');
      $sSearch = parent::escape_like_str(trim($search['value']));
      $columns = array_values(array_diff($this->columns, $this->unset_columns));

      if($sSearch != '')
         for($i = 0; $i < count($mColArray); $i++)
            if($mColArray[$i]['searchable'] == 'true' )
               if($this->check_cType())
                  $sWhere .= $this->select[$mColArray[$i]['data']] . " LIKE '%" . $sSearch . "%' OR ";
               else
                  $sWhere .= $this->select[$this->columns[$i]] . " LIKE '%" . $sSearch . "%' OR ";


      $sWhere = substr_replace($sWhere, '', -3);

      if($sWhere != '')
         parent::where('(' . $sWhere . ')');

      // TODO : sRangeSeparator
      //file_put_contents('holahola.txt', json_encode($this->columns));
      foreach ($columns as $key => $value) {
         $temp = $this->ci->input->post($value);
         if($temp){
            parent::where($value, $temp);  
         }
      }

      foreach($this->filter as $val)
        parent::where($val[0], $val[1], $val[2]);

      // custom filter
      $postFilter = $this->input->post('filter');
      $filterType = $this->input->post('filterType');
      if($postFilter)
         foreach ($postFilter as $key => $value) {
            // key = nama_field : nama_cabang, store_id, dll
            // value = array of (text, value)
            // val = (text, value)
            if (!$value) continue;
            $type = $filterType[$key];

            // replace separator - dengan .
            // this-field jadi this.field
            $key = str_replace($this->separatorFilter, '.', $key);
            //file_put_contents('holahola.txt', json_encode($key));

            switch ($type) {
               case 'text':
                  $regex = '';
                  foreach ($value as $j => $val){
                     if ($j<=0)
                        $regex.= $val['value'];
                     else
                        $regex.= '|'.$val['value'];
                  }
                  parent::where($key." REGEXP '".$regex."'");
                  break;
               
               case 'numeric':
                  foreach ($value as $j => $val) {
                     $value[$j] = str_replace('.', '', $val['value']);
                  }
                  parent::where_in($key, $value);
                  break;

               case 'date':
                  parent::where_in("DATE(".$key.")", array_values_index($value,'value'));
                  break;

               case 'list':

                  parent::where_in($key, array_values_index($value, 'value'));
                  break;

               case $type=='range-date' || $type=='range-numeric' :
                  $temp = '';
                  foreach ($value as $j => $val) {
                     $from = $val['value'][0];
                     $to   = $val['value'][1];
                     $and = '';
                     if ($from!='' && $to!='')
                        $and = ' AND ';

                     if ($type=='range-date'){
                        $valFrom = "'".$from."'";
                        $valTo   = "'".$to."'";
                     } else {
                        $valFrom = $from;
                        $valTo   = $to;
                     }

                     $temp.= ($j==0 ? '' : 'OR').'('. ($from!='' ? " $key >= ".$valFrom : "").$and. ($to!='' ? " $key <= ".$valTo : "").')';
                  }
                  $swer = '('.$temp.')';
                  //file_put_contents('holahola.txt', json_encode($swer));
                  parent::where($swer);
                  break;

               default:
                  # code...
                  break;
            }

         }
      
   }

    /**
    * Compiles the select statement based on the other functions called and runs the query
    *
    * @return mixed
    */
    private function get_display_result()
    {      
      return $this->get();
    }

    /**
    * Builds an encoded string data. Returns JSON by default, and an array of aaData if output is set to raw.
    *
    * @param string $output
    * @param string $charset
    * @return mixed
    */
    private function produce_output($output, $charset)
    {

      $aaData = array();
      
      if (!$this->isDataSet){
        $this->data = $this->get_display_result()->result_array();
      }

      if($output == 'json')
      {
        $iTotal = $this->get_total_results();
        $iFilteredTotal = $this->get_total_results(TRUE);
        //file_put_contents('holahola.txt', json_encode($this->db->last_query()));
      }

      $no = 0;
      foreach($this->data as $row_key => $row_val)
      {
        // nomer urut
        $no++;

        //$primId = $row_val['kd_grup_menu'];
        $aaData[$row_key] =  ($this->check_cType())? $row_val : array_values($row_val);

        foreach($this->add_columns as $field => $val)
         if($this->check_cType())
            $aaData[$row_key][$field] = $this->exec_replace($val, $aaData[$row_key]);
          else
            $aaData[$row_key][] = $this->exec_replace($val, $aaData[$row_key]);


        foreach($this->edit_columns as $modkey => $modval)
          foreach($modval as $val)
            $aaData[$row_key][($this->check_cType())? $modkey : array_search($modkey, $this->columns)] = $this->exec_replace($val, $aaData[$row_key]);

        $aaData[$row_key] = array_diff_key($aaData[$row_key], ($this->check_cType())? $this->unset_columns : array_intersect($this->columns, $this->unset_columns));
        //$this->numbering = true;
        //$aaData[$row_key] = array_merge(array('no' => $no),$aaData[$row_key]);
        //print_r($aaData[$row_key]);die;
        //$aaData[$row_key] = array_merge(array('_DT_RowIndex' => $primId),$aaData[$row_key]);
        

        if(!$this->check_cType())
          $aaData[$row_key] = array_values($aaData[$row_key]);

      }

      if($output == 'json')
      {
        $sOutput = array
        (
          'draw'                => intval($this->ci->input->post('draw')),
          'recordsTotal'        => $iTotal,
          'recordsFiltered'     => $iFilteredTotal,
          'data'                => $aaData
        );
        
        if($charset == 'utf-8'){
          return json_encode($sOutput);
        }
        else
          return $this->jsonify($sOutput);
      }
      else
        return array('aaData' => $aaData);
    }

    /**
    * Get result count
    *
    * @return integer
    */
    private function get_total_results($filtering = FALSE)
    {
      if($filtering)
        $this->get_filtering();

      parent::with($this->with);

      foreach($this->where as $val)
        parent::where($val[0], $val[1], $val[2]);

      foreach($this->or_where as $val)
        parent::or_where($val[0], $val[1], $val[2]);
        
      foreach($this->where_in as $val)
        parent::where_in($val[0], $val[1], $val[2]);

      foreach($this->group_by as $val)
        parent::group_by($val);

      foreach($this->like as $val)
        parent::like($val[0], $val[1], $val[2]);

      if(strlen($this->distinct) > 0)
      {
        parent::distinct($this->distinct);
        parent::select($this->columns);
      }

      //return $this->count_all_results();
      return $this->get()->num_rows();
    }

    /**
    * Runs callback functions and makes replacements
    *
    * @param mixed $custom_val
    * @param mixed $row_data
    * @return string $custom_val['content']
    */
    private function exec_replace($custom_val, $row_data)
    {
      $replace_string = '';

      if(isset($custom_val['replacement']) && is_array($custom_val['replacement']))
      {
        //Added this line because when the replacement has over 10 elements replaced the variable "$1" first by the "$10"
        $custom_val['replacement'] = array_reverse($custom_val['replacement'], true);
        foreach($custom_val['replacement'] as $key => $val)
        {
          $sval = preg_replace("/(?<!\w)([\'\"])(.*)\\1(?!\w)/i", '$2', trim($val));

      if(preg_match('/(\w+::\w+|\w+)\((.*)\)/i', $val, $matches) && is_callable($matches[1]))
          {
            $func = $matches[1];
            $args = preg_split("/[\s,]*\\\"([^\\\"]+)\\\"[\s,]*|" . "[\s,]*'([^']+)'[\s,]*|" . "[,]+/", $matches[2], 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);

            foreach($args as $args_key => $args_val)
            {
              $args_val = preg_replace("/(?<!\w)([\'\"])(.*)\\1(?!\w)/i", '$2', trim($args_val));
              $args[$args_key] = (in_array($args_val, $this->columns))? ($row_data[($this->check_cType())? $args_val : array_search($args_val, $this->columns)]) : $args_val;
            }

            $replace_string = call_user_func_array($func, $args);
          }
          elseif(in_array($sval, $this->columns))
            $replace_string = $row_data[($this->check_cType())? $sval : array_search($sval, $this->columns)];
          else
            $replace_string = $sval;

          $custom_val['content'] = str_ireplace('$' . ($key + 1), $replace_string, $custom_val['content']);
        }
      }

      return $custom_val['content'];
    }

    /**
    * Check column type -numeric or column name
    *
    * @return bool
    */
    private function check_cType()
    {
      $column = $this->ci->input->post('columns');
      if(is_numeric($column[0]['data']))
        return FALSE;
      else
        return TRUE;
    }


    /**
    * Return the difference of open and close characters
    *
    * @param string $str
    * @param string $open
    * @param string $close
    * @return string $retval
    */
    private function balanceChars($str, $open, $close)
    {
      $openCount = substr_count($str, $open);
      $closeCount = substr_count($str, $close);
      $retval = $openCount - $closeCount;
      return $retval;
    }

    /**
    * Explode, but ignore delimiter until closing characters are found
    *
    * @param string $delimiter
    * @param string $str
    * @param string $open
    * @param string $close
    * @return mixed $retval
    */
    private function explode($delimiter, $str, $open = '(', $close=')')
    {
      $retval = array();
      $hold = array();
      $balance = 0;
      $parts = explode($delimiter, $str);

      foreach($parts as $part)
      {
        $hold[] = $part;
        $balance += $this->balanceChars($part, $open, $close);

        if($balance < 1)
        {
          $retval[] = implode($delimiter, $hold);
          $hold = array();
          $balance = 0;
        }
      }

      if(count($hold) > 0)
        $retval[] = implode($delimiter, $hold);

      return $retval;
    }

    /**
    * Workaround for json_encode's UTF-8 encoding if a different charset needs to be used
    *
    * @param mixed $result
    * @return string
    */
    private function jsonify($result = FALSE)
    {
      if(is_null($result))
        return 'null';

      if($result === FALSE)
        return 'false';

      if($result === TRUE)
        return 'true';

      if(is_scalar($result))
      {
        if(is_float($result))
          return floatval(str_replace(',', '.', strval($result)));

        if(is_string($result))
        {
          static $jsonReplaces = array(array('\\', '/', '\n', '\t', '\r', '\b', '\f', '"'), array('\\\\', '\\/', '\\n', '\\t', '\\r', '\\b', '\\f', '\"'));
          return '"' . str_replace($jsonReplaces[0], $jsonReplaces[1], $result) . '"';
        }
        else
          return $result;
      }

      $isList = TRUE;

      for($i = 0, reset($result); $i < count($result); $i++, next($result))
      {
        if(key($result) !== $i)
        {
          $isList = FALSE;
          break;
        }
      }

      $json = array();

      if($isList)
      {
        foreach($result as $value)
          $json[] = $this->jsonify($value);

        return '[' . join(',', $json) . ']';
      }
      else
      {
        foreach($result as $key => $value)
          $json[] = $this->jsonify($key) . ':' . $this->jsonify($value);

        return '{' . join(',', $json) . '}';
      }
    }
  
   /**
     * returns the sql statement of the last query run
     * @return type
     */
    public function last_query()
    {
      return  $this->ci->db->last_query();
    }

    /*--------------------------------------
      TAMBAHAN ROFID
    ---------------------------------------*/
   public function getColumnCount(){
      $count = sizeof($this->header);
      if($this->numbering)
         $count++;
      return $count;
   }

    public function setScrollable($param = true){
      $this->isScrollable = $param;
      return $this;
    }

    public function setNumbering($param = true){
      $this->numbering = true;
      return $this;
    }

    public function setModel($model){
      
      // copy semua attribute model ke datatable
      $attribute = get_object_vars($model);
      foreach ($attribute as $key => $value) {
        $this->{$key} = $value;
      }

      return $this;
    }

    public function getData(){
      $this->data = $this->get_display_result()->result_array();
      return $this->data;
    }

    public function setData($data){
      if(!$data) return $this;

      $this->isDataSet = true;

      if ($this->selectFromIndex) {
        // inisialisasi
        $this->columns = array();
        $this->select  = array();

        // cek setnumbering
        if($this->numbering) {
          $this->column[] = 'no';
          foreach ($data as $key => $value) {
            $data[$key] = array_merge(array('no' => $key+1) , $data[$key]);
          }
        }

        // mendapatkan nama column berdasarkan index pada data
        foreach ($data[0] as $key => $value) {
          $this->columns[]    =  $key;
          $this->select[$key] =  $key;    
        }  
      }
      $this->data = $data;

      return $this;
    }

    public function selectLater($bool = TRUE){
      $this->selectLater = $bool;
      return $this;
    }

    public function selectFromIndex($bool = TRUE) {
      $this->selectFromIndex = $bool;
      return $this;
    }


   public function generateCss(){
      $content = '<style>';
      $content.= file_get_contents('Datatable/datatable.css', true);
      if(!$this->isScrollable){
        $content.= '#'.$this->id.'_wrapper .table-scrollable {
                        overflow-x:visible;
                        overflow-y:visible;
                      }';
      }
      $content.= '</style>';
      return $content;
   }

   public function getFilterInput($val){
      $filterInput = '';
      switch ($val[2]) {
         case 'text':
            $filterInput.= '<input column="'.$val[0].'" type="text" style="" class="form-control filter-input filter-input-'.$val[0].'" column-type="text">';
            break;
         case 'numeric':
            $filterInput.= '<input column="'.$val[0].'" type="text" style="" class="form-control filter-input filter-input-'.$val[0].' format-number"  column-type="numeric">';
            break;

         case 'date':
            $filterInput.= '<input column="'.$val[0].'" type="text" style="" class="form-control filter-input filter-input-'.$val[0].' date-picker" data-date-format="yyyy-mm-dd"  column-type="date">';
            break;

         case 'range-numeric' :
            $filterInput.= '
               <div class="input-group filter-input filter-input-'.$val[0].'" style=""  column="'.$val[0].'"  column-type="range-numeric">
                  <input type="text" class="form-control format-number value-from">
                  <span class="input-group-addon"> to </span>
                  <input type="text" class="form-control format-number value-to">
               </div>
               ';
            break;

         case 'range-date' :
            $filterInput.= '
               <div class="filter-input col-md-12 filter-input-'.$val[0].' input-group date-picker input-daterange" data-date-format="yyyy-mm-dd" style=""  column="'.$val[0].'"  column-type="range-date">
                  <input type="text" class="form-control value-from" >
                  <span class="input-group-addon"> to </span>
                  <input type="text" class="form-control value-to" >
               </div>
               <!--<span class="help-block">Select date range</span>-->
               ';
            break;
         
         case 'list' :
            $list = $val[3];
            if(is_array($list)){
               $htmlOptions = 'class="bs-select filter-input form-control col-md-12 filter-input-'.$val[0].'" data-live-search="true"  column="'.$val[0].'" column-type="list"';
               $filterInput.= form_dropdown('name', $list, 'default', $htmlOptions);
            }
            foreach ($list as $klist => $li) {
               $filterInput.= '<input type="hidden" value="'.$li.'" class="filter-list-value-'.$val[0].' filter-list-value-'.$val[0].'-'.$klist.'">';
            }
            break;

         default:
            # code...
            break;
      }

      return $filterInput;
   }

   public function generateFilter(){

      // 0:column. 1:label. 2:tipe : (text, numeric, date, list, range-date, range-numeric)
      $filter        = '<select class="filter-select form-control bs-select">';
      $filterval     = '';
      $filterInput   = '';
      foreach ($this->dataFilter as $key => $val) {
         // mengatasi agar bisa pake this.field
         // . direplace dengan separator lain
         $val[0] = str_replace('.', $this->separatorFilter, $val[0]);

         // combobox filter
         $filter.= '<option class="filter-option-'.$val[0].' filter-options " value="'.$val[0].'" type="'.$val[2].'">'.$val[1].'</option>';

         // filter value container
         $filterval.= '
                     <div class="filter-value-row filter-value-row-'.$val[0].' row" style="display:none">
                     <div class="col-md-3">
                      '.$val[1].'
                     </div>

                     <div class="col-md-9">
                        <input id="filter-value-'.$val[0].'" class="filter-value filter-value-'.$val[0].'" column="'.$val[0].'">
                     </div>
                     </div>
                     ';
         
         // filter input
         $isVisible = ($key==0 ? '' : 'display:none');
         $filterInput.= '<div class="filter-input-wrapper filter-input-wrapper-'.$val[0].'" style="'.$isVisible.'">';
         $filterInput.= $this->getFilterInput($val);
         $filterInput.= '</div>';



      }
      
      $filter.= '</select>';

      return '
      <div class="portlet light filter-container" id="fc-'.$this->showfilter_id.'" style="display:none;">
         <div class="portlet-title">
            <div class="caption font-purple-plum">
               <i class=""></i>
               <span class="caption-subject bold uppercase"> Table Filter </span>
            </div>
            <div class="tools">
               <a href="" class="collapse"></a>
            </div>
         </div>
         <div class="portlet-body">
            <div class="row" style="margin-top:15px;">
               <div class="col-md-4 filter-select-container">
                  '.$filter.'
               </div>
               <div class="col-md-7 filter-input-container">
                  '.$filterInput.'
               </div>
               <div class="col-md-1">
                  <a class="btn btn-default col-md-12 filter-add">
                     <i class="icon-plus"></i>
                  </a>
               </div>
            </div>

            <div class="filter-value-container">
               
                  '.$filterval.'
            </div>

            <div class="row filter-action-container" style="margin-top:15px;">
               <div class="col-md-12 text-center">
                  <a class="btn btn-default filter-submit">
                     <i class="icon-magnifier"></i> Submit Filter
                  </a>
               </div>
            </div>

         </div>
      </div>
      ';
   }

   public function generateInnerFilter(){
      $numFilter= sizeof($this->innerFilter);

      if($numFilter<=0) return;

      $numCount = $this->getColumnCount();
      if($numFilter==0) $numFilter = 1;
      $col = 12/$numFilter;
      $filterInput = '<div class="row filter-inner form-horizontal">';
      foreach ($this->innerFilter as $key => $val) {
         $filterInput.= '<div class="form-group col-md-'.$col.' filter-input-wrapper filter-input-wrapper-'.$val[0].'">';
         $filterInput.= '<label class="control-label col-md-3">'.$val[1].'</label>';
         $filterInput.= '<div class="col-md-9">'.$this->getFilterInput($val).'</div>';
         $filterInput.= '</div>';

      }
      $filterInput.= '</div>';
      
      $content = '
         <tr role="row" class="filter">
            <td colspan="'.$numCount.'" class="text-center filter-inner-wrapper">
               '.$filterInput.'
            </td>
         </tr>
      ';
      return $content; 
   }

   public function generateWrapper(){
      $content = '';

      // generate CSS
      $content.= $this->generateCss();

      $content.= '<div class="portlet-body datatable-container">';
      
      // filter
      if ($this->dataFilter) {
         $content.= $this->generateFilter();
      }

      $content.= '<div class="table-container">';

      $this->ci->load->model(array('store_model'));
      $htmlOptions            =  'id="list-cabang" target="#list-city" data-source="'.site_url($this->class_name.'/update_table_main/').'" onchange="refreshByStore(this)" class="form-control select2" data-placeholder="Select Store"';
      $data['list']['cabang']  = form_dropdown('cabang', $this->ci->store_model->options_empty(), $row->id_cabang, $htmlOptions);

      $htmlOptions            =  'id="list-cabang" target="#list-city" data-source="'.site_url($this->class_name.'/update_table_main/').'" onchange="refreshPenjualanByStore(this)" class="form-control select2" data-placeholder="Select Store"';
      $data['list']['cabang_penjualan']  = form_dropdown('cabang', $this->ci->store_model->options_empty(), $row->id_cabang, $htmlOptions);
      
      $data['list']['startdate'] = '<div class="input-group date-picker input-daterange input-daterange col-md-8" data-date="10/11/2012" data-date-format="yyyy-mm-dd">
          <input type="text" class="form-control" name="rent_date_start" id="rent_date_start" url-action="rent_date_start" value="'.date("Y-m-d").'">
          <span class="input-group-addon">
          to </span>
          <input type="text" class="form-control" name="rent_date_end" id="rent_date_end" url-action="rent_date_end" value="'.date("Y-m-d").'">
        </div>
        ';

      // TOMBOL-TOMBOL
      $content.= '<div class="table-actions-wrapper">'
                      .($this->btn_refresh ? '<a class="btn btn-default btn-sm table-action-refresh"><i class="icon-refresh"></i></a>' : '')
                      .($this->dataFilter ? '<a class="btn btn-default btn-sm table-action-show-filter" id="'.$this->showfilter_id.'"> Show Filter</a>' : '')
                      .($this->dropdowncabang ? $data['list']['cabang'] : '')
                      .($this->dropdowncabangpenjualan ? $data['list']['cabang_penjualan'] : '')
                      .($this->startdate ? $data['list']['startdate'] : '')
                  .'</div>';

      // IF ACTION
      if($this->checklist)
         $content.='
            <div class="table-group-actions-wrapper">'.
               ($this->url_action_group ? 
               '<button url-action="'.$this->url_action_group.'" class="btn yellow-saffron table-group-action-submit datatable-ajax"><i class="fa fa-check"></i> '.$this->caption_btn_group. '</button>' : '' ).
            '</div>';
      

      
      
      $content.='  
                      <table class="table '.$this->style.'" id="'.$this->id.'">
                        <thead>   
                          <tr role="row" class="heading yellow">';

      // CHECKLIST AWAL
      if($this->checklist)
        $content.= '<th width="2%">
                      <input type="checkbox" class="group-checkable" />
                    </th>';

      if($this->numbering)
        $content.= '<th width="2%">#</th>';

      // HEADER
      $iteration = 0;
      $class = '';
      foreach ($this->header as $key => $value) {
      //  if ($this->headerAlign[$iteration])
      //  $class  = ' text-'.$this->headerAlign[$iteration];
        $content.=          '<th width="5%" class="'.$class.'">'
                              .$value.
                            '</th>';
        $iteration++;
      }

      $content.=          '</tr>';
      $content.= $this->generateInnerFilter();   
      $content.= '  </thead>

                        <tbody>

                        </tbody>

                      </table>
                    </div>
                  </div>';


      // JAVASCRIPT
      $kar = "BLA";
      
      if(!empty($this->add_javascript)){
      $content.=
        "<script>
          jQuery(document).ready(function() { 
              ".$this->add_javascript."
          });
        </script>";
      }
      $content.=
        "<script>
          jQuery(document).ready(function() { 
              var tes = new TableAjax();
              tes.init({
                pagination : '$this->pagination',
                rowReordering : '$this->rowReordering',
                url_action_reordering : '$this->url_action_reordering',
                url : '$this->source',
                id : '$this->id',".($this->numbering ? "numbering : true," : "")
                ."callback_group_submit : function(button, grid){
                  ".$this->callback_group_submit."
                },
              });
          });
        </script>";

      return $content;
   }





  }
/* End of file Datatables.php */
/* Location: ./application/libraries/Datatables.php */

<?php

/* Created by : Muhammad Rizky Bonito
 * Upgrade by : Warman Suganda
 * Don't copy or modify source code or the whole of file without permission from the creator */

class custom_table {

    public $CI;
    public $id;
    public $data;
    public $kolom;
    public $style;
    public $link;
    public $header;
    public $title;
    public $limit;
    public $offset;
    public $page;
    public $keyword;
    public $numbering;
    public $width_colom;
    public $align;
    public $valign;
    public $message;
    public $cellspacing;
    public $cellpadding;
    public $border;
    public $info_cell;

    public $test;

    public $body;
    public $headerAlign = array();

    public $columns = array();
    public $columnWidth = array();
    public $wrapperClass;

    public function __construct() {
        $this->CI = &get_instance();

        $this->config();
    }

    public function makeTable($dataTable) {
        if (!isset($dataTable->data))
            $dataTable->data = array();
        if (!isset($dataTable->kolom))
            $dataTable->kolom = 20;
        if (!isset($dataTable->style))
            $dataTable->style = "";
        if (!isset($dataTable->link))
            $dataTable->link = array();
        if (!isset($dataTable->header))
            $dataTable->header = array();
        if (!isset($dataTable->title))
            $dataTable->title = array();
        if (!isset($dataTable->limit))
            $dataTable->limit = 10;
        if (!isset($dataTable->offset))
            $dataTable->offset = 0;

        $this->config($dataTable);

        $nomor = $this->makeNumber($dataTable->limit, $dataTable->offset);
        $stringData = $this->createHeader();
        $stringData .= $this->createContentData($dataTable->data, $dataTable->link, $dataTable->title, $dataTable->status, $dataTable->kolom, $nomor);
        return $stringData;
    }

    function createHeader() {

        // generate header dari column
        if ($this->columns){
            $tmp = array();
            foreach ($this->columns as $v) {
                $tmp[] = $v;
                $tmp[] = 1;
                $tmp[] = 1;
            }
            $this->header[0] = $tmp;
        }

        $row = $col = 0;
        $isFirst = $this->numbering;
        $rowHeader = count($this->header);
        $style = ($this->style ? $this->style : "table table-striped table-hover table-bordered" );
        $id = ($this->id ? $this->id : 'sample_editable_1');

        $stringData.= '<table class="'.$style.'" id="'.$id.'">';
        
        // column width
        $stringData.='<colgroup>';
        if($this->numbering)
            $stringData.= '<col />';
        $numHeader = sizeof($this->header[0])/3;
        for($i=0;$i<$numHeader;$i++){
            $colgroup = $this->columnWidth[$i];  
            if($colgroup)
                $stringData.= '<col width="'.$colgroup.'%" />';
            else
                $stringData.= '<col />';
        }
        $stringData.='</colgroup>';

        $stringData.= '<thead>';
        if ($rowHeader > 0) {
            foreach ($this->header as $dataHeader) {
                $col = count($dataHeader);
                $stringData .= "<tr>";
                if ($isFirst) {
                    $stringData .= "<th class='cell-first' rowspan='$rowHeader'>#</th>";
                    $isFirst = false;
                }
                $colspan = 1;
                $rowspan = 2;
                $iteration = 0;
                
                for ($index = 0; $index < $col; $index+=3) {

                    $class = '';
                    if ($index == 0 && !$this->numbering)
                        $class = 'cell-first';
                    
                    if ($this->headerAlign[$iteration])
                        $class.= ' text-'.$this->headerAlign[$iteration];
                    //else
                    //    $class.= ' text-center';

                    //$stringData .= "<th $cell_first colspan='$dataHeader[$colspan]' rowspan='$dataHeader[$rowspan]'>";
                    $stringData .= '<th class="'.$class.'">';
                    $stringData .= "$dataHeader[$index]";
                    $stringData .= "</th>";
                    $colspan += 3;
                    $rowspan += 3;

                    $iteration++;
                }
                $stringData .= "</tr>";
                
            }
        }
        $stringData .= "</thead>";
        return $stringData;
    }

    function createContentData($data, $link = array(), $title = array(), $total_status = array(), $kolom = 20, $nomor = 0) {
        $nomor = 0;
        $nomor += 1;
        $stringData = "";
        $row = count($data);
        $tempData = array();
        $indexBaris = $colspan = 0;
        $totalData = array();
        if (!empty($total_status)) {
            for ($i = 0; $i < $kolom; $i++) {
                $totalData[$i] = 0;
            }
        }
        if ($row != 0) {
            foreach ($data as $dataTable) {

                $pk = '';
                if (isset($dataTable['PRIMARY_KEY'])) {
                    $pk = $dataTable['PRIMARY_KEY'];
                    unset($dataTable['PRIMARY_KEY']);
                }

                $col = count($dataTable);
                $stringData .= "<tr><td>$nomor.</td>";
                $index = 0;
                $tempData[$indexBaris] = $dataTable;
                $colspan = 1;
                foreach ($dataTable as $dataKolom) {
                    $td = "<td>";
                    if (!empty($total_status)) {
                        if (!is_numeric($dataKolom)) {
                            $colspan++;
                        } else if ($total_status[$index]) {
                            $totalData[$index]+= $dataKolom;
                            $td = "<td align='right'>";
                        }
                    }
                    $stringData .= $td;
                    $c = '"';
                    if (!empty($link[$index])) {
                        $stringData .= "<a href='javascript:void(0);' onclick='drildown(" . $c . '#drildown_' . $nomor . $c . "," . $c . $link[$index] . $c . "," . $c . $pk . $c . ")'>" . "$dataKolom" . "</a>";
                    } else {
                        $stringData .= "$dataKolom";
                    }
                    $stringData .= "</td>";
                    $index++;
                }
                $stringData .= "</tr>";

                if (!empty($pk)) {
                    $clsp = $kolom + 1;
                    $stringData .= "<tr id='drildown_$nomor' status='close' style='display:none;'><td colspan='$clsp'>sss</td></tr>";
                }

                $indexBaris++;
                $nomor++;
            }

            if (!empty($total_status)) {
                $stringData .= $this->createTotal($total_status, $totalData, $colspan, $kolom);
            }
        }
        else
            $stringData .= "<tr><td colspan='$kolom' align='center'>Data tidak ditemukan</td></tr>";
        $stringData .= "</table>";
        return $stringData;
    }

    function createTotal($total_status, $totalData, $colspan, $kolom) {
        $stringData = "<tr>";
        $awal = true;
        $space = false;
        for ($i = 0; $i < $kolom; $i++) {
            if ($awal) {
                $stringData .= "<td colspan='$colspan' align='center'><b>Total</b></td>";
                $awal = false;
            } else if ($totalData[$i] != 0) {
                $stringData .= "<td align='right'><b>" . $totalData[$i] . "</b></td>";
                $space = true;
            } else if ($space) {
                $stringData .= "<td align='center'>-</td>";
            }
        }
        $stringData .= "</tr>";
        return $stringData;
    }

    function makeNumber($limit, $offset) {
        return ($limit * $offset);
    }

    private function config($dataTable = NULL) {
        if(!$dataTable)
            $dataTable = new stdClass();

        if (!isset($dataTable->id))
            $dataTable->id = '';
        if (!isset($dataTable->data))
            $dataTable->data = array();
        if (!isset($dataTable->kolom))
            $dataTable->kolom = 20;
        if (!isset($dataTable->style))
            $dataTable->style = 'table table-advance table-hover';
        if (!isset($dataTable->link))
            $dataTable->link = array();
        if (!isset($dataTable->header))
            $dataTable->header = array();
        if (!isset($dataTable->title))
            $dataTable->title = array();
        if (!isset($dataTable->limit))
            $dataTable->limit = 10;
        if (!isset($dataTable->page))
            $dataTable->page = 0;
        if (!isset($dataTable->keyword))
            $dataTable->keyword = '';
        if (!isset($dataTable->model))
            $dataTable->model = '';
        if (!isset($dataTable->numbering))
            $dataTable->numbering = true;
        if (!isset($dataTable->width_colom))
            $dataTable->width_colom = array();
        if (!isset($dataTable->align))
            $dataTable->align = array();
        if (!isset($dataTable->valign))
            $dataTable->valign = array();
        if (!isset($dataTable->message))
            $dataTable->message = 'Data tidak ditemukan';
        if (!isset($dataTable->cellspacing))
            $dataTable->cellspacing = 0;
        if (!isset($dataTable->cellpadding))
            $dataTable->cellpadding = 0;
        if (!isset($dataTable->border))
            $dataTable->border = 0;
        if (!isset($dataTable->info_cell))
            $dataTable->info_cell = true;

        $this->id = $dataTable->id;
        $this->data = $dataTable->data;
        $this->kolom = $dataTable->kolom;
        $this->style = $dataTable->style;
        $this->link = $dataTable->link;
        $this->header = $dataTable->header;
        $this->title = $dataTable->title;
        $this->limit = $dataTable->limit;
        $this->page = $dataTable->page;
        $this->keyword = $dataTable->keyword;
        $this->offset = ($dataTable->page * $dataTable->limit) - $dataTable->limit;
        $this->model = $dataTable->model;
        $this->numbering = $dataTable->numbering;
        $this->width_colom = $dataTable->width_colom;
        $this->align = $dataTable->align;
        $this->valign = $dataTable->valign;
        $this->message = $dataTable->message;
        $this->cellspacing = $dataTable->cellspacing;
        $this->cellpadding = $dataTable->cellpadding;
        $this->border = $dataTable->border;
        $this->info_cell = $dataTable->info_cell;

        //if($this->offset<0) $this->offset = 0;
    }

    public function produceTableContent($dataTable = NULL) {
        
        if($dataTable) $this->config($dataTable);

        $total = 0;
        if(!$this->body){
            if(!$this->data){
                $data = $this->get_data();
                if ($data) {
                    $total = $data['total'];
                    $this->data = $data['rows'];
                }    
            }
        }
        
        $table= $this->createHeader();
        $table.= $this->createContent();
        $table.= $this->createFooter();
        $table .= $this->createJavascript();
        //return array('table' => $table, 'total' => $total, 'limit' => $this->limit);
        return $table;
    }

    public function generate_ajax($dataTable) {

        return array('table' => $this->produceTableContent($dataTable), 'total' => $total, 'limit' => $this->limit, 'offset'=>$this->offset);
    }

    public function get_data() {
        $ci =& get_instance();
        $model = $this->model;
        if (!empty($model)) {
            list($model_name, $function_name) = explode('->', $model);
            
            // LOAD MODEL IF NOT LOADED
            if(!$ci->{$model_name})
                $ci->load->model($model_name);

            // breakdown parameter
            list($function_name, $parameter) = explode('/', $function_name);

            //echo ' model name '.$model_name;
            $m = $this->CI->{$model_name};
            $m->limit = $this->limit;
            $m->offset = $this->offset;
            $m->keyword = $this->keyword;

            return $m->{$function_name}($parameter);
        } else {
            return false;
        }
    }

    function createContent() {
        // body diset manual
        if($this->body)
            return $this->body;

        $content = '';
        $kolom = $this->kolom;
        if($this->offset<0){
            $nomor = 0;
        }else{
            $nomor = ($this->page - 1) * $this->limit;
        }
        
        //

        $data = $this->data;
        //$content.='<body>';

        if (count($data) > 0) {

            foreach ($data as $row) {
                $nomor++;

                $pk = '';
                
                if ($row['primary_key']) {
                    $pk = $row['primary_key'];
                    unset($row['primary_key']);
                }

                if (!empty($pk)) {
                    $rowId = "content_row_".$pk;
                }

                $content .= '<tr id="'.$rowId.'" class="row-data" valign="middle">';

                // Jika akan menampilkan penomoran
                if ($this->numbering) {
                    $jml_colom++;
                    $content .= '<td class="cell-first" style="text-align:center;">'. $nomor . '</td>';
                }
                
                $index = 0;
                $column = 0;
                $jml_colom+= sizeof($row);
                foreach ($row as $key => $value) {

                    $align = (isset($this->align[$column])) ? 'align="' . $this->align[$column] . '"' : '';
                    $valign = (isset($this->valign[$key])) ? 'valign="' . $this->valign[$key] . '"' : 'valign="middle"';
                    $width = (isset($this->width_colom[$key])) ? 'width="' . $this->width_colom[$key] . '"' : '';

                    $cell_first = '';
                    if ($index == 0 && !$this->numbering)
                        $cell_first = 'class="cell-first"';
                    
                    $cell = '';
                    if ($this->info_cell) {
                        $cell = 'cell="' . $key . '" ';
                    }

                    $tdClass = 'column-'.$key;

                    $content .= '<td '.
                                $cell_first .' '.
                                $cell . ' '.
                                $align . ' ' .
                                $valign . ' ' .
                                $width . ' '.
                                $tdClass .'>'
                        ;

                    if (isset($this->link[$key])) {
                        $content .= '<a href="javascript:void(0)" id="drildown_key_' . $this->id . '_' . $pk . '" rel="' . $pk . '" data-source="' . $this->link[$key] . '" parent="' . $this->id . '" onclick="drildown(this.id)">' . $value . '</a>';
                    } else {
                        $content .= $value;
                    }

                    $content .= '</td>';
                    $index++;
                    $column++;
                }

                $content .= '</tr>';

                if (!empty($pk))
                    $content .= '<tr id="drildown_row_' . $this->id . '_' . $pk . '" status="close" style="display:none;"><td style="background: #fff;" colspan="' . $jml_colom . '"><div class="div-drilldown" style="display:none;"></div></td></tr>';
            }
        } else {
            $content .= '<tr class=""><td class="cell-first" align="center" colspan="' . $kolom . '">' . $this->message . '</td></tr>';
        }
        //$content.='</body>';

        return $content;
        //return "";
    }

    function createFooter(){
        return '</table>';
    }

    function createJavascript(){
        return '';
    }

    public function setData($data){
        $this->data = $data;
    }

    public function generateFooter(){
        return $this->footer;
    }

    public function generate(){
        $content = $this->produceTableContent();
        $content.= $this->generateFooter();
        return $content;
    }

    public function generateWithWrapper(){
        $content = $this->generateCss();
        $content.= '<div class="new-custom-table '.$this->wrapperClass.'" id="div-'.$this->id.'" data-source="'.$this->data_source.'">';
        $content.= $this->generate();
        $content.= '</div>';
        $content.= $this->generateJavascript();
        return $content;
    }

    public function generateWrapper(){
        $content = '<div class="new-custom-table" id="div-'.$this->id.'" data-source="'.$this->data_source.'">';
        $content.= '</div>';
        $content.= $this->generateJavascript();
        return $content;
    }

    public function generateCss(){
        $con = '<style>';
        $con.= '#div-'.$this->id.' .table-scrollable {
                    overflow-x:visible,
                    overflow-y:visible,
                }';
        $con.= '</style>';
        return $con;
    }

    public function generateJavascript(){
        $con = '<script>';
        $con.= '
                $("#div-'.$this->id.'").parent().css("overflow-x","visible");
                $("#div-'.$this->id.'").parent().css("overflow-y","visible");
                ';
        $con.= '</script>';
        return $con;
    }

}

/* End of file custom_table.php */
/* Location: ./application/libraries/custom_table.php */
<?php
class purchase_order_model extends MY_Model {

    public $transaction_id;

    public function __construct() {
        parent::__construct();
        $this->flex = true;
        $this->table    = 'zn_purchase_order';
        $this->primary  = 'po_id';
    }

    public function relation(){
        return array(
            'detail' => array('zn_purchase_order_detail','po_id', 'po_id'),
            'item' => array('m_item','item_id', 'item_id', 'detail'),
            'supplier' => array('m_supplier', 'id_supplier', 'supplier_id'),
            'document' => array('zn_transaction_document','transaction_id', 'transaction_id'),
            'store' => array('m_cabang', 'id_cabang', 'store_id'),
            'transaction' => array('zn_transaction', 'transaction_id', 'transaction_id'),
            'repostore_koli' => array('zn_receiving_postore_koli','postore_id','postore_id','detail'),
            'koli' => array('zn_koli','koli_id','koli_id','repostore_koli'),
            'shipping_koli' => array('zn_shipping_koli','koli_id','koli_id','koli'),
            'shipping' => array('zn_shipping','shipping_id','shipping_id','shipping_koli'),
            'divisi' => array('m_divisi_md','id_divisi_md','id_divisi_md'),
        );
    }

    

    public function create($data){
        $user_id    = $this->session->userdata('user_id');
        $store_id   = $this->session->userdata('store_id');
        $now        = date::now_sql();

        $this->load->model(array('transaction_model','transaction_store_model'));
        // insert to transaction
        $trans['transactiontype_id']    = transactiontype::PO;
        $trans['rec_user']              = $user_id;
        $trans['rec_created']           = $now;
        $this->transaction_model->create($trans);
        $transaction_id = $this->db->insert_id();

        // insert to transaction store
        $trans_store['transaction_id']  = $transaction_id;
        $trans_store['store_id']        = $store_id;
        $this->transaction_store_model->create($trans_store);

        $this->transaction_id = $transaction_id;

        $data['transaction_id'] = $transaction_id;
        $data['user_id']        = $user_id;
        parent::create($data);
    }
}

?>
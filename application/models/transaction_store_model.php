<?php

class transaction_store_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'zn_transaction_store';
        $this->primary = 'transactionstore_id';
    }

}
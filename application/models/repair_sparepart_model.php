<?php 

class repair_sparepart_model extends MY_Model {

    public $limitStore = true;
    
    public function __construct() {
        parent::__construct();
        $this->table = 'zn_repair_sparepart';
        $this->primary = 'repairsparepart_id';
        $this->flex = true;
    }

    public function relation(){
        return array(
            'repair' => array('zn_transaction_repair', 'transactionrepair_id', 'transactionrepair_id'),
            'service'=> array('zn_transaction_service', 'transactionservice_id', 'transactionservice_id'),
            'item' => array('m_item', 'item_id', 'item_id'),
        );
    }
}
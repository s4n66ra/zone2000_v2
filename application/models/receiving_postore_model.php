<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class receiving_postore_model extends MY_Model {

	public $table 	= 'zn_receiving_postore';
    public $primary = 'receivingpostore_id';
    public $flex 	= true;

    public function __construct() {
        parent::__construct();
    }

    public function relation(){
        return array(
            'receiving_po_koli' => array('zn_receiving_postore_koli','postore_id','postore_id'),
            'po_detail' => array('zn_purchase_order_detail','postore_id','postore_id'),
            'item' => array('m_item','item_id','item_id','po_detail'),
            'koli' => array('zn_koli', 'koli_id','koli_id','receiving_po_koli'),
            'shipping_koli' => array('zn_shipping_koli','koli_id','koli_id','koli'),
            'shipping' => array('zn_shipping','shipping_id','shipping_id','shipping_koli'),
            'store' => array('m_cabang','id_cabang','store_id','po_detail'),
            'pr' => array('m_status_pr', 'status_id', 'status_id'),
        );
    }

}
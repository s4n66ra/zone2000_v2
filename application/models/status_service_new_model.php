<?php 

class status_service_new_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->flex = true;
        $this->table = 'm_status_service_new';
        $this->primary = 'status_id';
    }

    public function relation(){
        return array(
            'service_detail' => array("zn_service_detail", "id_status", "status_id"),
        );
    }

}
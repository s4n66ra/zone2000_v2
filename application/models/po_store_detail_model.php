<?php
class po_store_detail_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->flex = true;
        $this->table    = 'zn_po_store_detail';
        $this->primary  = 'postoredetail_id';
    }

    public function relation(){
        return array(
            'prdetail' => array('zn_purchase_request_detail', 'prdetail_id', 'prdetail_id'),
            'postore' => array('zn_po_store', 'postore_id', 'postore_id'),
            'item' => array('m_item', 'item_id', 'item_id', 'prdetail'),
            'store' => array('m_cabang','id_cabang', 'store_id', 'postore'),
            'supplier' => array('m_supplier', 'id_supplier', 'supplier_id', 'postore'),
            'po' => array('zn_purchase_order', 'po_id', 'po_id', 'postore'),
        );
    }
}

?>
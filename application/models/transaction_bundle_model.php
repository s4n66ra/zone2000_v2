<?php

class transaction_bundle_model extends MY_Model {
	public $table = 'zn_transaction_bundle';
   	public $primary = 'transactionbundle_id';
   	public $flex = true;

    public function __construct() {
        parent::__construct();
        
    }

    public function relation(){
        return array(
            'transaction' => array('zn_transaction', 'transaction_id', 'transaction_id'),
            'document' => array('zn_transaction_document','transaction_id', 'transaction_id'),
            'pr' => array('zn_purchase_request','transactionbundle_id','transactionbundle_id'),
            'po' => array('zn_purchase_order','bundle_id','transactionbundle_id'), //==== khusus untuk ticket====
            'po_store' => array('zn_po_store','po_id','po_id','po'),//==== khusus untuk ticket====
            'supplier' => array('m_supplier','id_supplier','supplier_id','po_store'),//==== khusus untuk ticket====
            'po_detail' => array('zn_purchase_order_detail','po_id','po_id','po'),//==== khusus untuk ticket====
            'item' => array('m_item','item_id','item_id','po_detail'),//==== khusus untuk ticket====
        );
    }

    

}
<?php
class purchase_request_detail_model extends MY_Model {

    public $total;

    public function __construct() {
        parent::__construct();
        $this->flex = true;
        $this->table    = 'zn_purchase_request_detail';
        $this->primary  = 'prdetail_id';
    }

    public function relation(){
        return array(
            'pr' => array('zn_purchase_request','pr_id', 'pr_id'),
            'item' => array('m_item', 'item_id', 'item_id'),
            'po' => array('zn_purchase_order', 'po_id', 'po_id'),
            'postore' => array('zn_po_store', 'postore_id', 'postore_id'),
            'supplier' => array('m_supplier', 'id_supplier', 'supplier_id'),
            'stok' => array('m_item_stok', 'item_id','item_id'),

            'podetail'=> array('zn_purchase_order_detail','prdetail_id','prdetail_id'),
            'postore' => array('zn_po_store', 'postore_id', 'postore_id'),
            'postore_koli' => array('zn_receiving_postore_koli', 'postore_id', 'postore_id', 'postore'),
            'koli' => array('zn_koli','koli_id','koli_id','postore_koli'),
        );
    }

   

    public function getDataCustomTable($pr_id = 0) {
        $this->load->model(array('purchase_request_model','item_stok_model'));
        $pr   = $this->purchase_request_model->getById($pr_id)->row();
        $data = $this
                //->select("*, this.price as harga")
                ->with(array('item','supplier'))
                ->where('pr_id', $pr_id)
                //->limit($this->limit, $this->offset)
                ->get()->result_array();
        
        $rows = array();
        $this->total = 0;
        foreach ($data as $key => $value) {
            
            $id = $value[$this->primary];
            $id_enc = url_base64_encode($id);            

            $action = array();
            $temp = array();

            // get stok item di store tertentu
            $stok = $this->item_stok_model
                ->where('item_id', $value['item_id'])
                ->where('store_id', $pr->store_id)
                ->get()->row_array();

            // membuat item_stok di toko jika tidak ditemukan record
            if(!$stok) {
                $this->item_stok_model->create(array('item_id'=>$value['item_id'], 'store_id'=>$pr->store_id));
            }

            $temp['primary_key']    = $value['prdetail_id'];
            $temp['item_key']       = $value['item_key'];
            $temp['item_name']      = $value['item_name'];
            $temp['nama_supplier']  = $value['nama_supplier'];
            //$temp['last_stok']      = view::format_number($stok['stok_store']);
            $temp['qty_request']    = view::format_number_plaint($value['qty_request']);
            $temp['qty_approved']   = view::format_number_plaint($value['qty_approved']);
            $temp['price']          = view::format_number2($value['price']);
            $temp['total']          = view::format_number2($value['qty_temp'] * $value['price']);
            
            $this->total+= $value['qty_temp'] * $value['price'];

            // aktifasi tombol jika status pr adalah draft
            if($pr->pr_status==prstatus::DRAFT || $pr->pr_status==prstatus::REVISI) {
                if ($this->access_right->otoritas('edit')) {
                    $action[] = view::button_edit_detail($id_enc);
                }

                if ($this->access_right->otoritas('delete')) {
                    $action[] = view::button_delete_detail($id);
                }

                if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                    $temp['aksi'] = view::render_button_group($action);
                }
            }

            $rows[] = $temp;
        }

        $this->total = view::format_number($this->total);

        return $rows;
    }

}
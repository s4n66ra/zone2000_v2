<?php
class pegawai_cabang_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'm_pegawai_cabang';
        $this->primary = 'id_pegawai_cabang';
    }

    public $limit;
    public $offset;


    public function relation(){
        return array(
            'cabang' => array('m_cabang','id_cabang','id_cabang', 'm_pegawai_cabang'),
            'pegawai' => array('m_pegawai', 'id_pegawai', 'id_pegawai', 'm_pegawai_cabang')
        );
    }

    private function data($condition = array()) {
        if($condition==null){
            $zn_transaction = $this->pegawai_cabang_model
                    ->with_flex(array("cabang", "pegawai"))
                    ->order_by("pegawai.nama_pegawai", "ASC")
                    ->group_by("cabang.nama_cabang")
                    ->group_by("pegawai.nama_pegawai")
                    ->db();
        }else{
            $zn_transaction = $this->pegawai_cabang_model
                    ->with_flex(array("cabang", "pegawai"))
                    ->order_by("pegawai.nama_pegawai", "ASC")
                    ->group_by("cabang.nama_cabang")
                    ->group_by("pegawai.nama_pegawai")
                    ->where("id_pegawai_cabang",$condition["id_pegawai_cabang"])
                    ->db();
        }
        return $zn_transaction;
    }

    private function data_detail($condition = array()) {
        // =============Filtering===============
        //$condition = array();
        $kd_bbm= $this->input->post("kd_bbm");
        $id_supplier= $this->input->post("id_supplier");
        $id_purchase_order = $this->input->post('id_purchase_order');

        $id_bbm = $this->input->post('id_bbm');
        if(!empty($kd_bbm)){
            $condition["a.kd_bbm like '%$kd_bbm%'"]=null;
        }
        if(!empty($id_supplier)){
            $condition["a.id_supplier"]=$id_supplier;
        }
        if(!empty($id_purchase_order)){
            $condition["a.id_purchase_order"]=$id_purchase_order;
        }
        if(!empty($id_bbm)){
            $condition["a.id_bbm"]=$id_bbm;
        }

        $tanggal_awal = hgenerator::switch_tanggal($this->input->post('tanggal_awal'));
        $tanggal_akhir = hgenerator::switch_tanggal($this->input->post('tanggal_akhir'));
        //--------end filtering-----------------------------------


        $this->db->select('*,b.id as id_bbm_barang');
        $this->db->from($this->table  . ' a');
        $this->db->join($this->table4 . ' b',' a.id_bbm = b.id_bbm');
        $this->db->join($this->table5 . ' c',' b.id_barang = c.id_barang','left');
        $this->db->join($this->table6 . ' d',' c.id_satuan = d.id_satuan','left');
        $this->db->join($this->table7 . ' e',' e.id_kategori = c.id_kategori','left');
        $this->db->order_by('c.kd_barang ASC');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_by_id($id) {
        $condition['id_pegawai_cabang'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_detail_by_id($id) {
        $condition['b.id'] = $id;
        $this->data_detail($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function data_table() {
        // Total Record
        $total = $this->data()->count_all_results();

        // List Data
        //$this->db->limit($this->limit, $this->offset);
        $data = $this->data($cond)->get()->result();
        $rows = array();

        foreach ($data as $key => $value) {
            $id = $value->id_pegawai_cabang;

            $id_enc = url_base64_encode($id);            
            $action = '';
            $action .= '';

            if ($this->access_right->otoritas('edit')) {
                $action .= view::button_edit($id_enc, array('full-width'=>0));
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= view::button_delete($id_enc, array('full-width'=>0));
            }

            $temp = array(
                'primary_key' =>$value->id_pegawai_cabang,
                'id_pegawai' => $value->nama_pegawai,
                'id_cabang' => $value->nama_cabang
            );

            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $temp['aksi'] = view::render_button_group_raw($action);
            }

            $rows[] = $temp;
        }

        return array('rows' => $rows, 'total' => $total);
    }   


    public function create($data) { 
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->table, $data, array('id_pegawai_cabang' => $id));
    }

    public function create_detail($data) { 
        return $this->db->insert($this->table4, $data);
    }

    public function update_detail($data, $id) {
        return $this->db->update($this->table4, $data, array('id' => $id));
    }


    public function delete($id) {
        //return $this->db->delete($this->table, array('id_pegawai_cabang' => $id));
        $cek = true;

        $data = $this->get_by_id($id)->result();
        $id_pegawai = '';
        $id_cabang = '';

        foreach ($data as $key => $value) {
            $id_pegawai = $value->id_pegawai;
            $id_cabang = $value->id_cabang;
        }

        return $this->db->delete($this->table, array('id_pegawai' => $id_pegawai, 'id_cabang' => $id_cabang));

    }
    
    
    public function options($default = '--Pilih pegawai_cabang--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_pegawai_cabang] = $row->nama_pegawai_cabang ;
        }
        return $options;
    }

    public function options_empty($field, $value) {
        $condition = array();
        if(is_array($field)){
            $condition = $field;
        }else{
            $condition[$field] = $value;
        }
        $data = $this->pegawai_cabang_model->with('jenis_pegawai_cabang')->db()
                ->where('id_cabang',$this->session->userdata('id_cabang'))
                ->get()->result();
        $options = array();
        $options['']  = '';
        foreach ($data as $row) {
            $options[$row->id_pegawai_cabang] = $row->kd_pegawai_cabang.' - '.$row->nama_jenis_pegawai_cabang ;
        }
        return $options;
    }

    public function data_table_detail_excel() {
        // Total Record
        $total = $this->data_detail()->count_all_results();

        // List Data
        $data_detail = $this->data_detail()->get();
        $rows = array();

        foreach ($data_detail->result() as $value) {
            $id = $value->id_bbm_barang;
            $action = '';
            $rows[] = array(
                'primary_key' => $id,
                'kd_barang'=>$value->kd_barang,
                'nama_barang' => $value->nama_barang,
                'jumlah' => $value->jumlah_barang,
                'satuan' => $value->nama_satuan,
                'kategori' => $value->nama_kategori,
            );
        }

        return array('rows' => $rows, 'total' => $total);
    }    

    
}

?>
<?php 

class transaction_meter_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'zn_transaction_meter';
        $this->primary = 'transactionmeter_id';
    }

}

<?php 

class transaction_machine_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'zn_transaction_machine';
        $this->primary = 'transactionmachine_id';
    }

}

<?php
class history_prbundle_detail_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->flex = true;
        $this->table    = 'zn_history_prbundle_detail';
        $this->primary  = 'historyprbundledetail_id';
    }

    public function relation(){
        return array(
            'prbundle' => array('zn_history_prbundle', 'historyprbundle_id', 'historyprbundle_id'),
            'item' => array('m_item', 'item_id', 'item_id'),
            'store' => array('m_cabang','id_cabang', 'store_id'),
            'supplier' => array('m_supplier', 'id_supplier', 'supplier_id'),
            'po' => array('zn_purchase_order', 'bundle_id','historyprbundle_id'),
        );
    }
}

?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class sparepart_dua_model extends MY_Model {

    public $table   = 'm_sparepart';
    public $primary = 'id_sparepart';
    public $flex    = true;
    public $limitStore = false;

    public function __construct() {
        parent::__construct();
    }

    public function relation(){
        return array(
            'store' => array('m_cabang','id_cabang', 'store_id'),
        );
    }


}
<?php
class cabang_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $cabang  = 'm_cabang';
    private $area = 'm_area';
    private $pegawai = 'm_pegawai';
    private $table2 = 'supplier';
    private $table3 = 'purchase_order';
    private $table4 = 'bbm_barang';
    private $table5 = 'barang';
    private $table6 = 'satuan';
    private $table7 = 'kategori';

    private function data($condition = array()) {
        $this->db->select("m_cabang.id_cabang, m_area.id_area, m_pegawai.status_pegawai, m_cabang.nama_cabang, m_cabang.alamat, m_cabang.kd_cabang, m_area.nama_area, m_cabang.alamat, m_cabang.telp, m_pegawai.nama_pegawai, m_pegawai.nama_pegawai, m_cabang.luas, m_cabang.pajak_toko, m_cabang.id_supervisor as id_supervisor, m_cabang.id_kepcabang as id_kepcabang");        
        $this->db->from($this->cabang);
        $this->db->join($this->area,$this->area.'.id_area='.$this->cabang.'.id_area','left');
        $this->db->join($this->pegawai, $this->pegawai.'.status_pegawai='.$this->cabang.'.id_supervisor', 'left');
        $this->db->where_condition($condition);
        return $this->db;
    }

    private function data_detail($condition = array()) {
        // =============Filtering===============
        //$condition = array();
        $kd_bbm= $this->input->post("kd_bbm");
        $id_supplier= $this->input->post("id_supplier");
        $id_purchase_order = $this->input->post('id_purchase_order');

        $id_bbm = $this->input->post('id_bbm');
        if(!empty($kd_bbm)){
            $condition["a.kd_bbm like '%$kd_bbm%'"]=null;
        }
        if(!empty($id_supplier)){
            $condition["a.id_supplier"]=$id_supplier;
        }
        if(!empty($id_purchase_order)){
            $condition["a.id_purchase_order"]=$id_purchase_order;
        }
        if(!empty($id_bbm)){
            $condition["a.id_bbm"]=$id_bbm;
        }

        $tanggal_awal = hgenerator::switch_tanggal($this->input->post('tanggal_awal'));
        $tanggal_akhir = hgenerator::switch_tanggal($this->input->post('tanggal_akhir'));
        //--------end filtering-----------------------------------


        $this->db->select('*,b.id as id_bbm_barang');
        $this->db->from($this->table  . ' a');
        $this->db->join($this->table4 . ' b',' a.id_bbm = b.id_bbm');
        $this->db->join($this->table5 . ' c',' b.id_barang = c.id_barang','left');
        $this->db->join($this->table6 . ' d',' c.id_satuan = d.id_satuan','left');
        $this->db->join($this->table7 . ' e',' e.id_kategori = c.id_kategori','left');
        $this->db->order_by('c.kd_barang ASC');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_by_id($id) {
        $condition['id_cabang'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_detail_by_id($id) {
        $condition['b.id'] = $id;
        $this->data_detail($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function data_table() {
        // Total Record
        $total = $this->data()->count_all_results();

        // List Data
        //$this->db->limit($this->limit, $this->offset);
        $data = $this->data()->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $id = $value->id_cabang;
            $id_enc = $id_enc = url_base64_encode($id);            
            $action = '';
            $action .= '';

            if ($this->access_right->otoritas('edit')) {
                $action .= '<a href="'.base_url("cabang/edit/$id_enc").'" class="btn default btn-xs purple"><i class="fa fa-edit"></i> Edit </a>';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= '<a href="'.base_url("cabang/delete/$id_enc").'" class="btn default btn-xs black"><i class="fa fa-edit"></i> Delete </a>';
            }

            $db1 = $this->load->database('default', TRUE);
            $query = $db1->query("select nama_pegawai from m_pegawai where status_pegawai='".$value->id_kepcabang."'");
            foreach ($query->result() as $baris) {
                if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                    $rows[] = array(
                        'primary_key' =>$value->id_cabang,
                        'kd_cabang' => $value->kd_cabang,
                        'nama_cabang' => $value->nama_cabang,
                        'nama_area' => $value->nama_area,
                        'alamat' => $value->alamat,
                        'telp' => $value->telp,
                        'nama_pegawai' => $value->nama_pegawai,
                        'nama_pegawai2' => $baris->nama_pegawai,
                        'luas' => $value->luas,
                        'pajak_toko' => $value->pajak_toko,
                        'aksi' => $action
                    );
                }else{
                    $rows[] = array(
                        'primary_key' =>$value->id_cabang,
                        'kd_cabang' => $value->kd_cabang,
                        'nama_cabang' => $value->nama_cabang,
                        'nama_area' => $value->nama_area,
                        'alamat' => $value->alamat,
                        'telp' => $value->telp,
                        'nama_pegawai' => $value->nama_pegawai,
                        'nama_pegawai2' => $baris->nama_pegawai,
                        'luas' => $value->luas,
                        'pajak_toko' => $value->pajak_toko
                    );
                }            
            }
            



        }

        return array('rows' => $rows, 'total' => $total);
    }   

    public function data_table_excel() {
        //=============Tahun Aktif====================
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif))
        {
            $tahun_aktif = date('Y');
        }
        $tahun_aktif_awal = $tahun_aktif.'-01-01';
        $tahun_aktif_akhir = $tahun_aktif.'-12-31';
        $condition["a.tanggal_bbm >= '$tahun_aktif_awal'"] = null ;
        $condition["a.tanggal_bbm <= '$tahun_aktif_akhir'"] = null;
        //-----------end tahunaktif-------------------
        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $rows[] = array(
                'tanggal_bbm' => hgenerator::switch_tanggal($value->tanggal_bbm),
                'kd_bbm' => $value->kd_bbm,
                'kd_po' => $value->kd_po,
                'supplier' => $value->nama_supplier,
            );
        }

        return array('rows' => $rows, 'total' => $total);
    }     




    public function create($data) {	
        return $this->db->insert($this->cabang, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->cabang, $data, array('id_cabang' => $id));
    }

    public function create_detail($data) { 
        return $this->db->insert($this->table4, $data);
    }

    public function update_detail($data, $id) {
        return $this->db->update($this->table4, $data, array('id' => $id));
    }

    public function delete($id) {
        return $this->db->delete($this->cabang, array('id_cabang' => $id));
    }

    public function delete_detail($id) {
        return $this->db->delete($this->table4, array('id' => $id));
    }
    
    
    public function options($default = '--Select Branch--', $key = '') {
        //$data = $this->data()->get();
        $data = $this->db->select('id_cabang,nama_cabang')->from('m_cabang')->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_cabang] = $row->nama_cabang ;
        }
        return $options;
    }

    public function options_empty() {
    	$data = $this->data()->get();
    	$options = array();
    	$options['']  = '';
    	foreach ($data->result() as $row) {
    		$options[$row->id_cabang] = $row->nama_cabang ;
    	}
    	return $options;
    }

    public function data_table_detail_excel() {
        // Total Record
        $total = $this->data_detail()->count_all_results();

        // List Data
        $data_detail = $this->data_detail()->get();
        $rows = array();

        foreach ($data_detail->result() as $value) {
            $id = $value->id_bbm_barang;
            $action = '';
            $rows[] = array(
                'primary_key' => $id,
                'kd_barang'=>$value->kd_barang,
                'nama_barang' => $value->nama_barang,
                'jumlah' => $value->jumlah_barang,
                'satuan' => $value->nama_satuan,
                'kategori' => $value->nama_kategori,
            );
        }

        return array('rows' => $rows, 'total' => $total);
    }    

    
}

?>
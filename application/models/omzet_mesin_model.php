<?php
class omzet_mesin_model extends MY_Model {

    public $id_trans_teknogame;

    public function __construct() {
        parent::__construct();
        $this->flex = true;
        $this->table    = 'v_omzet_mesin_harian';
        //$this->primary  = 'id_trans_teknogame';
    }

    public function relation(){
        return array(
            'mesin_teknogame' => array('m_mesin_teknogame','kd_mesin', 'kode_mesin'),
            'cabang' => array('m_cabang', 'id_cabang', 'store_id'),
            'area' => array('m_area', 'id_area', 'id_area', 'cabang'),
            'kota' => array('m_kota', 'id_kota', 'id_kota', 'cabang'),
            'province' => array('m_provinsi', 'id_provinsi', 'id_provinsi', 'kota'),
        );
    }

    
}

?>
<?php
class jenis_mesin_model extends MY_Model {

    private $kategori_mesin    = 'm_kategori_mesin';
    private $kelas              = 'm_kelas';

    public function __construct() {
        parent::__construct();
        $this->table = 'm_jenis_mesin';
        $this->primary = 'id_jenis_mesin';
    }

    public function relation(){
        return array(
            'category' => array('m_kategori_mesin','id_kategori_mesin', 'id_kategori_mesin'),
            'class' => array('m_kelas', 'id_kelas', 'id_kelas'),
            'coin' => array('m_koin', 'id_koin', 'jenis_koin'),
        );
    }
    
    private function base_join(){
        $this->db->from($this->table);
        $this->db->join($this->kategori_mesin. ' km','km.id_kategori_mesin = '.$this->table.'.id_kategori_mesin','left');
        $this->db->join($this->kelas. ' k','k.id_kelas = '.$this->table.'.id_kelas','left');
        return $this->db;
    }

    private function data($condition = array()) {
        $this->db->select($this->table.".".$this->primary.", foto, kd_jenis_mesin, nama_jenis_mesin, km.nama_kategori_mesin, jenis_koin, k.nama_kelas, deskripsi");       
        $this->base_join()->where_condition($condition);
        return $this->db;
    }

    

    public function get_by_id($id) {
        $condition[$this->table.'.'.$this->primary] = $id;
        $this->base_join()->where_condition($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function data_table() {

        // Total Record
        $total = $this->data()->count_all_results();

        // List Data
        //$this->db->limit($this->limit, $this->offset);
        $data = $this->withAll()->get()->result_array();

        $rows = array();

        foreach ($data as $key => $value) {
            $id = $value[$this->primary];
            $id_enc = url_base64_encode($id);            
            $action = '';
            $action .= '';

            $temp = array();

            if ($this->access_right->otoritas('edit')) {
                $action .= view::button_edit($id_enc, array('full-width'=>1));
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= view::button_delete($id_enc, array('full-width'=>1));
            }

            $temp['primary_key']       = $value['id_jenis_mesin'];
            $temp['kd_jenis_mesin']       = $value['kd_jenis_mesin'];
            $temp['nama_jenis_mesin']  = $value['nama_jenis_mesin'];
            $temp['nama_kategori_mesin']      = $value['nama_kategori_mesin'];
//             $temp['type_in']        = transaksi::getItemType($value['type_in']);
//             $temp['type_out']       = transaksi::getItemType($value['type_out']);
            $temp['deskripsi']      = $value['deskripsi'];


            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $temp['aksi'] = view::render_button_group_raw($action);
            }

            $rows[] = $temp;
        }

        return array('rows' => $rows, 'total' => $total);
    }   


    public function create($data) {    

        $this->load->model('item_model');

        // insert to m_item
        $item['itemtype_id'] = itemtype::ITEM_MACHINETYPE;
        $item['item_key']    = $data['item_key'];
        $item['item_name']   = $data['nama_jenis_mesin'];
//         $item['price']       = $data['price']; June 29th
        $this->item_model->create($item);
        $item_id = $this->db->insert_id();

        // insert to jenis_mesin
        $data['item_id'] = $item_id;
        $return = $this->db->insert($this->table, $data);
        return $return;
    }

    public function update($data, $id) {

        /*
            data = array (
                kd_jenis_mesin, nama_jenis_mesin, deskripsi, etc   
            )
        */
        $this->load->model('item_model');

        $mesin = $this->getById($id)->row();
        if($mesin->kd_jenis_mesin != $data['kd_jenis_mesin']){
            $this->load->model('kategori_mesin_model');
            $this->kategori_mesin_model->raiseSequence($data['id_kategori_mesin']);
        }
        // edit jenis mesin
        $this->db->where($this->primary, $id)->update($this->table, $data);

        // edit item 
        $item['item_key'] = $data['item_key'];
        $item['item_name']= $data['nama_jenis_mesin'];
//         $item['price']    = $data['price']; June 29th
        $return = $this->item_model->update($item, $mesin->item_id);

        return $return;
    }

    public function delete($id) {

        return $this->db->delete($this->table, array($this->primary => $id));

    }
    
    public function options() {
        $data = $this->data()->get();
        $options = array('' => '');

        foreach ($data->result() as $row) {
            $options[$row->{$this->primary}] = $row->nama_jenis_mesin ;
        }
        return $options;
    }

    public function options_kd() {
        $data = $this->data()->get();
        $options = array('' => '');

        foreach ($data->result() as $row) {
            $options[$row->kd_jenis_mesin] = $row->kd_jenis_mesin.' - '.$row->nama_jenis_mesin ;
        }
        return $options;
    }

    
}

?>
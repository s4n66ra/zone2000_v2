<?php
class transactiontype_model extends MY_Model {

    public $table  = 'zn_transaction_type';
    public $primary_key = 'transactiontype_id';
    public $flex = true;

    public function __construct() {
        parent::__construct();
    }

    public function options_empty() {
        $data = $this->where("transactiontype_name <> ''")->get()->result();
        $options = array();
        $options['']  = '';
        foreach ($data as $row) {
            $options[$row->transactiontype_id] = $row->transactiontype_name ;
        }
        return $options;
    }

    public function options_empty_name() {
        $data = $this->where("transactiontype_name <> ''")->get()->result();
        $options = array();
        $options['']  = '';
        foreach ($data as $row) {
            $options[$row->transactiontype_name] = $row->transactiontype_name ;
        }
        return $options;
    }
    
}

?>
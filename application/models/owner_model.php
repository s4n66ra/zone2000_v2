<?php
class owner_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table    = 'm_owner';
        $this->primary  = 'id_owner';
    }

    private function data($condition = array()) {        
        $this->db->from($this->table);
        $this->db->where_condition($condition);
        return $this->db;
    }
    public function data_table() {
        // Total Record
        $total = $this->data()->count_all_results();

        // List Data
        //$this->db->limit($this->limit, $this->offset);
        $data = $this->data()->get()->result();
        $rows = array();

        foreach ($data as $key => $value) {
            $id = $value->id_owner;
            $id_enc = url_base64_encode($id);
            $action = '';
            $action .= '';

            if ($this->access_right->otoritas('edit')) {
                $action .= view::button_edit($id_enc, array('full-width'=>1));
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= view::button_delete($id_enc, array('full-width'=>1));
            }

            $temp = array(
                    'primary_key' =>$value->id_owner,
                    'kd_owner' => $value->kd_owner,
                    'nama_owner' => $value->nama_owner,
                    'aksi' => $action
            );

            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $temp['aksi'] = view::render_button_group_raw($action);
            }

            $rows[] = $temp;

        }

        return array('rows' => $rows, 'total' => $total);
    }   

    public function get_by_id($id) {
        $condition[$this->primary] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function create($data) {	
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->table, $data, array($this->primary => $id));
    }

    public function delete($id) {
        return $this->db->delete($this->table, array($this->primary => $id));
    }
    
    public function options($default = '--Pilih owner--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->{$this->primary}] = $row->nama_owner ;
        }
        return $options;
    }

    public function options_empty() {
        $data = $this->data()->get();
        $options = array();
        $options['']  = '';
        foreach ($data->result() as $row) {
            $options[$row->{$this->primary}] = $row->nama_owner ;
        }
        return $options;
    }
    
}

?>
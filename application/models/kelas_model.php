<?php
class kelas_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'm_kelas';
        $this->primary = 'id_kelas';
    }

    private function data($condition = array()) {
        $this->db->select("m_kelas.id_kelas, m_kelas.kd_kelas, m_kelas.nama_kelas, m_kelas.min_value, m_kelas.max_value");        
        
        $this->db->from($this->table);
        $this->db->where_condition($condition);
        return $this->db;
    }

    public function get_by_id($id) {
        $condition[$this->table.'.'.$this->primary] = $id;

        $this->db->from($this->table);
        $this->db->where_condition($condition);

        return $this->db->get();
    }


    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function data_table() {
        // Total Record
        $total = $this->data()->count_all_results();

        // List Data
        $this->db->limit($this->limit, $this->offset);
        $data = $this->data()->get()->result_array();
        $rows = array();

        foreach ($data as $key => $value) {
            $id = $value[$this->primary];
            $id_enc = url_base64_encode($id);            
            $action = '';
            $action .= '';

            $temp = array();

            if ($this->access_right->otoritas('edit')) {
                $action .= view::button_edit($id_enc);
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= view::button_delete($id_enc);
            }

            // mengganti index id_cabang menjadi primary_key
            $temp['primary_key']  = $value[$this->primary];
            unset($value[$this->primary]);
            $temp = hgenerator::merge_array($temp, $value);

            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $temp['aksi'] = view::render_button_group_raw($action);
            }

            $rows[] = $temp;
        }

        return array('rows' => $rows, 'total' => $total);
    }   

    

    public function create($data) {
        $this->db->trans_start();

        // insert to kelas
        $return = $this->db->insert($this->table, $data);

        $this->db->trans_complete();
        return $return;
    }

    public function update($data,$id) {
        $this->db->trans_start();

        // edit store
        $return = $this->db->update($this->table, $data, array($this->primary => $id));

        $this->db->trans_complete();
        return $return;
    }


    public function delete($id) {

        $this->db->trans_start();
       
        // delete class
        $return = $this->db->delete($this->table,  array('id_kelas' => $id));

        $this->db->trans_complete();
        return $return;
    }
    
    
    public function options($default = '--Select Branch--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_cabang] = $row->nama_cabang ;
        }
        return $options;
    }  

    public function options_empty() {
        $data = $this->data()->get();
        $options = array();
        $options['']  = '';
        foreach ($data->result() as $row) {
            $options[$row->id_kelas] = $row->nama_kelas ;
        }
        return $options;
    }

}

?>
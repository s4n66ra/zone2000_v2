<?php
class pr_sparepart_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table    = 'zn_pr_sparepart';
        $this->primary  = 'id_pr';
        $this->detail   = 'zn_pr_sparepart_detail';
        $this->history  = 'zn_pr_sparepart_history';
        $this->history_detail   = 'zn_pr_sparepart_history_detail';
        $this->detail_primary   = 'id_pr_detail';
        $this->detail_join      = 'm_sparepart';
        $this->detail_join_primary = 'id_sparepart';
        $this->cabang   = 'm_cabang';
        $this->cabang_primary = 'id_pengirim';
    }
    
    private function base_join(){
        $this->db->from($this->table);
        $this->db->join('m_cabang','m_cabang.id_cabang = zn_pr_sparepart.id_pengirim');
        //$this->db->join($this->kategori_mesin. ' km','km.id_kategori_mesin = '.$this->table.'.id_kategori_mesin','left');        
        return $this->db;
    }

    private function data($condition = array()) {      
        $this->base_join()->where_condition($condition);
        return $this->db;
    }

    public function get_by_id($id) {
        $condition[$this->table.'.'.$this->primary] = $id;
        $this->base_join()->where_condition($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function data_table() {
        // Total Record
        $total = $this->data()->count_all_results();

        // List Data
        //$this->db->limit($this->limit, $this->offset);
        $data = $this->data()->get()->result_array();
        $rows = array();

        foreach ($data as $key => $value) {
            $id = $value[$this->primary];
            $id_enc = url_base64_encode($id);            
            $action = '';
            $action .= '';

            $temp = array();

            if ($this->access_right->otoritas('edit')) {
                $action .= view::button_detail($id);
                $action .= view::button_edit($id_enc,array('full-width'=>1));
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= view::button_delete($id_enc,array('full-width'=>1));
            }

            $conf_status = $this->config->item('status_pr');

            $temp['primary_key']    = $value[$this->primary];
            $temp['kd_pr']          = $value['kd_pr'];
            $temp['tgl_dibuat']     = $value['tgl_dibuat'];
            $temp['status_pr']      = $conf_status[$value['status_pr']];
            $temp['keterangan']     = $value['keterangan'];

            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $temp['aksi'] = view::render_button_group_raw($action);
            }

            $rows[] = $temp;
        }
        
        return array('rows' => $rows, 'total' => $total);
    }   


    public function create($data) {
        /*
            data = array (
                kd_pr_sparepart, nama_pr_sparepart, deskripsi, etc   
            )
        */
    
        $this->db->trans_start();

        // insert to tabel m_mesin
        $return = $this->db->insert($this->table, $data);

        $this->db->trans_complete();
        return $return;
    }

    public function update($data,$id) {
        /*
            data = array (
                kd_pr_sparepart, nama_pr_sparepart, deskripsi, etc   
            )
        */
    
        $this->db->trans_start();
        $return = $this->db->update($this->table, $data, array($this->primary => $id));
        $this->db->trans_complete();
        return $return;
    }

    public function delete($id) {

        $this->db->trans_start();
       
        // edit setting
        $return = $this->db->delete($this->table, array($this->primary => $id));

        $this->db->trans_complete();
        return $return;
    }

    /* -------------------------------------
        DETAIL PR
    ---------------------------------------*/
    private function base_join_detail(){
        $detail                 = $this->detail;
        $detail_join            = $this->detail_join;
        $detail_join_primary    = $this->detail_join_primary;

        $this->db->from($detail);
        $this->db->join($detail_join, "$detail.$detail_join_primary = $detail_join.$detail_join_primary" ,'left');
        //$this->db->join($this->kategori_mesin. ' km','km.id_kategori_mesin = '.$this->table.'.id_kategori_mesin','left');        
        return $this->db;
    }

    private function data_detail($condition = array()) {
        //$select = "id_pr_detail, kd_sparepart, nama_sparepart, jumlah, harga, kd_sparepart, keterangan";
        //$this->db->select($select);       
        $this->base_join_detail()->where_condition($condition);
        return $this->db;
    }

    public function get_detail_by_id($id) {
        $condition[$this->detail.'.'.$this->detail_primary] = $id;
        $this->base_join_detail()->where_condition($condition);
        return $this->db->get();
    }

    public function data_table_detail($id_pr) {

        // List Data
        $this->db->limit($this->limit, $this->offset);

        $data = $this->data_detail(array('id_pr'=>$id_pr))->get()->result_array();
        $pr   = $this->get_by_id($id_pr)->row();
        // size of records

        $total= sizeof($data);
        $rows = array();

        foreach ($data as $key => $value) {
            
            $id = $value[$this->detail_primary];
            $id_enc = url_base64_encode($id);            
            $action = '';
            $action .= '';

            $temp = array();

            $temp['primary_key']    = $value['id_pr_detail'];
            $temp['kd_sparepart']      = $value['kd_sparepart'];
            $temp['nama_sparepart']    = $value['nama_sparepart'];
            $temp['jumlah']         = $value['jumlah'];
            // $temp['jml_disetujui']  = intval($value['jml_disetujui']);
            // $temp['jml_diterima']   = intval($value['jml_diterima']);
            $temp['harga']          = $value['harga'];
            $temp['total']          = $value['jumlah'] * $value['harga'];
            //$temp['keterangan']     = $value['keterangan'];

            // aktifasi tombol jika status pr adalah draft
            // if($pr->status_pr==0) {
                if ($this->access_right->otoritas('edit')) {
                    $action .= view::button_edit_detail($id_enc);
                }

                if ($this->access_right->otoritas('delete')) {
                    $action .= view::button_delete_detail($id_enc);
                }

                if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                    $temp['aksi'] = view::render_button_group_raw($action);
                }
            //}

            $rows[] = $temp;
        }
        
        return array('rows' => $rows, 'total' => $total);
    }

    public function create_detail($data) {
        $return = $this->db->insert($this->detail, $data);
        return $return;
    }

    public function update_detail($data, $id){
        return $this->db->update($this->detail, $data, array($this->detail_primary => $id));
    }

    public function delete_detail($id) {
        $return = $this->db->delete($this->detail, array($this->detail_primary => $id));
        return $return;
    }

    public function create_history_detail(){

    }

    public function proceed($id, $status){
        /*
            0 => 'Draft',
            1 => 'Waiting Aproval',
            2 => 'Revision',
            3 => 'Rejected',
            4 => 'Aproved',
            5 => 'Shiping',
            6 => 'Delivered',
            7 => 'Canceled',
        */

        $return = true;
        $write_history_detail = true;
        $this->db->trans_start();

        $pr = $this->get_by_id($id)->row_array();
        if($pr && $pr['status_pr']!= $status && $status>=0 && $status<=7){
            $data   = array();
            $detail = $this->db->from($this->detail)->where($this->primary, $id)->get()->result_array();
            $history= array(
                        'id_pr' => $id,
                        'tanggal' => date::now_sql(),
                        'id_pegawai' => $this->session->userdata('id_pegawai')
                    );
            $history_detail = array();
            switch ($status) {
                case 1:
                    // waiting approval
                    $data['status_pr']  = 1;
                    $data['tgl_diajukan'] = date::now_sql();
                    $history['status']  = 1;  
                    break;

                case 2:
                    // revisi
                    $data['count_revisi']   = $pr->count_revisi + 1;
                    $data['status_pr']  = 2;
                    $data['tgl_revisi'] = date::now_sql();
                    $history['status']  = 2;  
                    break;
                
                case 3:
                    // rejected
                    $data['status_pr']      = 3;
                    $data['tgl_ditolak']    = date::now_sql();
                    $history['status']      = 3;
                    $write_history_detail   = false;  
                    break;

                case 4:
                    // approved
                    $data['status_pr']      = 4;
                    $data['tgl_disetujui']  = date::now_sql();
                    $history['status']      = 4;  
                    //$write_history_detail   = false;

                    // UPDATE JUMLAH DIKIRIM
                    foreach ($detail as $key => $value) {   
                        $this->update_detail(array('jml_disetujui' => $value['jumlah']),$value[$this->detail_primary]);
                    }
                    break;

                case 5:
                    // shipping
                    $data['status_pr']      = 5;
                    $data['tgl_dikirim']  = date::now_sql();
                    $history['status']      = 5;  
                    $write_history_detail   = false;

                    // UPDATE JUMLAH DIKIRIM
                    foreach ($detail as $key => $value) {   
                        $this->update_detail(array('jml_dikirim' => $value['jumlah']),$value[$this->detail_primary]);
                    }

                    break;

                case 6:
                    // delivered
                
                    $data['status_pr']      = 6;
                    $data['tgl_diterima']  = date::now_sql();
                    $history['status']      = 6;  
                    //$write_history_detail   = false;

                    // jika pr sparepart ini berasal dari laporan kerusakan mesin
                    // update status di kerusakan mesin jika sparepart sudah datang
                    $this->load->model('mesin_kerusakan_model');
                    $data['status'] = 4;
                    $this->mesin_kerusakan_model->update($data ,$pr->id_mesin_rusak);

                    $this->load->model('stok_sparepart_model');
                    foreach ($detail as $key => $value) {
                        // UPDATE JUMLAH DITERIMA
                       $this->update_detail(array('jml_diterima' => $value['jumlah']),$value[$this->detail_primary]);

                        // INSERT TO STOK sparepart
                        $temp = array();
                        $condition['id_cabang'] = $this->session->userdata('id_cabang');
                        $condition['id_sparepart'] = $value['id_sparepart'];
                        $stok = $this->stok_sparepart_model->table()->where($condition)->get()->row_array();
                        if($stok){
                            // sudah ada
                            $temp['stok_total']+= $value['jumlah'];
                            $temp['stok_gudang']+= $value['jumlah'];
                            $this->stok_sparepart_model->update($temp,$stok['id_stok_sparepart']);
                        }else{
                            // create new
                            $temp['id_cabang'] = $this->session->userdata('id_cabang');
                            $temp['id_sparepart'] = $value['id_sparepart'];
                            $temp['stok_total'] =  $value['jumlah'];
                            $temp['stok_gudang']= $value['jumlah'];
                            $this->stok_sparepart_model->create($temp);
                        }

                    }

                    break;

                default:
                    # code...
                    break;
            }

            // update pr_sparepart
            $return = $this->db->update($this->table, $data, array($this->primary => $id));

            // insert history
            if($write_history_detail){                
                $this->db->insert($this->history,$history);
                $id_history = $this->db->insert_id();
                if($id_history){
                    // insert history detail
                    foreach ($detail as $key => $value) {
                        $history_detail[] = array(
                                'id_pr_history' => $id_history,
                                'id_sparepart'     => $value['id_sparepart'],
                                'jumlah'        => $value['jumlah'],
                                'keterangan'    => $value['keterangan'],
                            );
                    }
                    
                    $return = $this->db->insert_batch($this->history_detail,$history_detail);
                }else{
                    $return = false;
                }
            }

        } else
            $return = false;

        $this->db->trans_complete();
        return $return;
    }


    public function options_mesin_empty() {
        $this->load->model('mesin_model');
        $data = $this->mesin_model->with('jenis_mesin')->db()
                ->where('id_cabang',$this->session->userdata('id_cabang'))
                ->get()->result();
        $options = array();
        $options['']  = '';
        foreach ($data as $row) {
            $options[$row->id_mesin] = $row->kd_mesin.' - '.$row->nama_jenis_mesin ;
        }
        return $options;
    }
    
}

?>
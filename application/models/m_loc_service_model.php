<?php
class m_loc_service_model extends My_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'm_location_service';
        $this->primary='location_id';
        $this->flex = true;
    }

    public $limit=20;
    public $offset;
    private $hadiah  = 'm_hadiah';
    private $table2 = 'supplier';
    private $table3 = 'purchase_order';
    private $table4 = 'bbm_barang';
    private $table5 = 'barang';
    private $table6 = 'satuan';
    private $table7 = 'kategori';

    public function relation(){
        return array(
            'item' => array('m_item','item_id','item_id'),
            'type' => array('m_hadiah_type', 'hadiahtype_id', 'hadiahtype_id'),
        );
    }

    private function data($condition = array()) {        
        $this->db->from($this->table);
        $this->db->where_condition($condition);
        return $this->db;
    }

    public function get_by_id($id) {
        $condition['status_id'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_detail_by_id($id) {
        $condition['b.id'] = $id;
        $this->data_detail($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function data_table() {
        // Total Record
        $total = $this->data()->count_all_results();

        // List Data
        //$this->db->limit($this->limit, $this->offset);
        $data = $this->data()->get()->result();
        $rows = array();

        foreach ($data as $key => $value) {
            $id = $value->id_hadiah;
            $id_enc = url_base64_encode($id);
            $action = '';
            $action .= '';

            if ($this->access_right->otoritas('edit')) {
                $action .= view::button_edit($id_enc, array('full-width'=>0));
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= view::button_delete($id_enc, array('full-width'=>0));
            }

            $temp = array(
                'primary_key' =>$value->id_hadiah,
                'kd_hadiah' => $value->kd_hadiah,
                'nama_hadiah' => $value->nama_hadiah,
                'harga' => $value->harga,
                'jumlah_tiket' => $value->jumlah_tiket,
                'aksi' => $action
            );

            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $temp['aksi'] = view::render_button_group_raw($action);
            }
            $rows[] = $temp;
            
        }

        return array('rows' => $rows, 'total' => $total);
    }   
 
/*
    public function create($data) {
        $return = false;
        $this->db->trans_start();
        
        // INSERT TO ITEM FIRST
        $this->load->model('item_model');
        $item['item_name'] = $data['nama_hadiah'];
        $item['item_key']  = $data['item_key'];
        $item['harga']     = $data['harga'];
        $item['itemtype_id'] = 2;
        $this->item_model->create($item);
        $item_id = $this->db->insert_id();

        // GET ITEM_ID THEN INSERT TO HADIAH
        $data['item_id'] = $item_id;
        $return = $this->db->insert($this->hadiah, $data);
        $this->db->trans_complete();

        return $return;
    }

    public function update($data, $id) {
        $this->db->trans_start();

        // update hadiah
        $data['harga']     = hgenerator::switch_number($data['harga']);
        $return = $this->db->update($this->hadiah, $data, array('id_hadiah' => $id));


        $this->load->model('item_model');
        $item['item_name'] = $data['nama_hadiah'];
        $item['item_key']  = $data['item_key'];
        $item['harga']     = $data['harga'];
        $item['item_barcode'] = $data['item_barcode'];

        $itemObject = $this->item_model->with('hadiah')->where('id_hadiah', $id)->get()->row();
        if($itemObject->item_id){
            //update item            
            $this->item_model->update($item, $itemObject->item_id);
        }else{
            // insert new
            $item['itemtype_id'] = 2;
            $this->item_model->create($item);
        }
        
        $this->db->trans_complete();
        return $return;
    }

   

    public function delete($id) {
        return $this->db->delete($this->hadiah, array('id_hadiah' => $id));
    }*/

    
    
    public function options($default = '--Pilih Lokasi Service--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->location_id] = $row->loc_code.' - '.$row->loc_name ;
        }
        return $options;
    }

    public function options_empty() {
        $data = $this->data()->get();
        $options = array();
        $options['']  = '';
        foreach ($data->result() as $row) {
            $options[$row->id_hadiah] = $row->nama_hadiah ;
        }
        return $options;
    }
    

    public function data_table_detail_excel() {
        // Total Record
        $total = $this->data_detail()->count_all_results();

        // List Data
        $data_detail = $this->data_detail()->get();
        $rows = array();

        foreach ($data_detail->result() as $value) {
            $id = $value->id_bbm_barang;
            $action = '';
            $rows[] = array(
                'primary_key' => $id,
                'kd_barang'=>$value->kd_barang,
                'nama_barang' => $value->nama_barang,
                'jumlah' => $value->jumlah_barang,
                'satuan' => $value->nama_satuan,
                'kategori' => $value->nama_kategori,
            );
        }

        return array('rows' => $rows, 'total' => $total);
    }    

    
}

?>
<?php

/**
 * Description of menu_group_model
 *
 * @author Warman Suganda
 */
class menu_group_model extends MY_Model {

    public $limit;
    public $offset;
    public $table = 'm_grup_menu';
    public $primary = 'kd_grup_menu';
    public $flex = true;
    public $relation = array(
                'menu' => array('m_menu', 'kd_grup_menu', 'kd_grup_menu'),
            );

    public function __construct() {
        parent::__construct();
    }

    private function data($condition = array()) {
        $this->db->from($this->table);
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_by_id($id) {
        $condition['kd_grup_menu'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        $this->db->order_by('urutan');
        return $this->db->get();
    }

    public function data_table() {
        // Filtering
        $condition = array();

        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        $this->db->order_by('urutan');
        $data = $this->data($condition)->get();
        $rows = array();

        $idx_group = 0;
        foreach ($data->result() as $value) {
            $idx_group++;
            $id = $value->kd_grup_menu;
            $id_enc = url_base64_encode($id);
            $action = '';
            if ($this->access_right->otoritas('edit')) {
                $action .= view::button_edit($id_enc);
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= view::button_delete($id_enc);
            }

            /*$group_up = $group_down = '';
            if ($idx_group == 1) {
                $group_up = 'disabled="disabled"';
            } else if ($idx_group == $total) {
                $group_down = 'disabled="disabled"';
            }*/

            //$urutan = anchor(null, '<i class="icon-arrow-up"></i>', 'class="btn transparant" ' . $group_up);
            //$urutan .= anchor(null, '<i class="icon-arrow-down"></i>', 'class="btn transparant" style="margin-left:5px;" ' . $group_down);

            $rows[] = array(
                'icon' => '<i class="' . $value->icon . '"></i>',
                'nik' => $value->nama_grup_menu,
                'action' => !empty($action) ? $action : '<i class="icon-lock" title="Acces Denied" style="color:#999;"></i>'
            );
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function create($data) {
        $group = $this->select('kd_grup_menu')->order_by('kd_grup_menu','desc')->get()->row();
        $newId = $group->kd_grup_menu+1;
        $data['kd_grup_menu'] = $newId;
        return parent::create($data);
    }

    public function delete($id){
        $this->load->model('menu_model');
        $this->menu_model->set('kd_grup_menu', NULL)->where('kd_grup_menu',$id)->update();
        parent::delete($id);
    }

    public function options($default = '--Pilih Group Menu--', $key = '') {
        $data = $this->get_data();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->kd_grup_menu] = $row->nama_grup_menu;
        }
        return $options;
    }

}

/* End of file menu_group_model.php */
/* Location: ./application/models/menu_group_model.php */


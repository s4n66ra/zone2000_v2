<?php
class inventory_summary_model extends MY_Model {

    public $limit;
    public $offset;
    public $keyword;

    public function __construct() {
        parent::__construct();
        $this->table = 'm_item';
        $this->primary = 'item_id';
    }

    public function relation(){
        return array(
            'item_stok' => array('m_item_stok','item_id','item_id'),
            'item_type' => array('m_item_type','itemtype_id','itemtype_id'),
            'cabang' => array('m_cabang','id_cabang','store_id', 'm_item_stok')

        );
    }



    private function data($condition = array(), $like= array()) {
        
        $zn_transaction = $this->inventory_summary_model
                ->with(array("item","item_stok","item_type", "cabang"))
                ->db();
        return $zn_transaction;

    }

    public function get_by_id($id) {
        $condition['item_id'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_detail_by_id($id) {
        $condition['b.id'] = $id;
        $this->data_detail($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function data_table_by_keyword($keyword = ''){
        $like['type_name'] = $keyword;
        $this->data(array(),$like);
        return $this->db;
    }

    public function data_table() {

        if ($this->access_right->otoritas('edit') && $this->access_right->otoritas('delete')) {
            // Limit dan Offset Data
            $this->db->limit($this->limit, $this->offset);
            $this->db->like(array('type_name' => $this->keyword));
            $data = $this->data()->get()->result();


            // Hitung Total row menggunakan Keyword
            $total = $this->data_table_by_keyword($this->keyword)->count_all_results();
            $rows = array();
            
            foreach ($data as $key => $value) {
                $id = $value->item_id;
                $id_enc = url_base64_encode($id);  
                $action = '';
                $action .= '';


                $temp = array(
                    'item_key' => $value->item_key,
                    'item_name' => $value->type_name,
                    'itemtype_id' => $value->itemtype_id,
                    'nama_cabang' => $value->nama_cabang,
                    'num_stok' => $value->num_stok
                );


                $rows[] = $temp;
            }

        }


        return array('rows' => $rows, 'total' => $total);
    }   


    public function create($data) { 
        return $this->db->insert($this->mesin, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->mesin, $data, array('id_mesin' => $id));
    }

    public function create_detail($data) { 
        return $this->db->insert($this->table4, $data);
    }

    public function update_detail($data, $id) {
        return $this->db->update($this->table4, $data, array('id' => $id));
    }

    public function delete($id) {
        return $this->db->delete($this->mesin, array('id_mesin' => $id));
    }

    public function delete_detail($id) {
        return $this->db->delete($this->table4, array('id' => $id));
    }
    
    
    public function options($default = '--Pilih mesin--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_mesin] = $row->nama_mesin ;
        }
        return $options;
    }

    public function options_empty($field, $value) {
        $condition = array();
        if(is_array($field)){
            $condition = $field;
        }else{
            $condition[$field] = $value;
        }

        $data = $this->mesin_model->with('jenis_mesin')->db()
                ->where('id_cabang',$this->session->userdata('id_cabang'))
                ->get()->result();

        $options = array();

        $options['']  = '';
        foreach ($data as $row) {
            $options[$row->id_mesin] = $row->kd_mesin.' - '.$row->nama_jenis_mesin ;
        }
        return $options;
    }

    public function getInventoryTypes(){
        $this->db->select("type_name");
        $this->db->from('m_item_type');
        $hasil = $this->db->get()->result();
        $fixresult = array();
        $fixresult['']='';
        foreach ($hasil as $key => $value) {
            array_push($fixresult, $value->type_name);
        }
        return $fixresult;
    }

    
}

?>
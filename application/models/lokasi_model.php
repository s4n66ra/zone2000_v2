<?php
class lokasi_model extends MY_Model {

    public $relation = array(
            'kota' => array('m_kota', 'id_kota', 'id_kota')
        );

    public function __construct() {
        parent::__construct();
        $this->table = 'm_lokasi';
        $this->primary = 'id_lokasi';
    }

    public function options_empty() {
        $data = $this->get();
        $options = array();
        $options['']  = '';
        foreach ($data->result() as $row) {
            $options[$row->id_lokasi] = $row->nama_lokasi ;
        }
        return $options;
    }

}
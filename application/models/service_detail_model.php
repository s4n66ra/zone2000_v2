<?php 

class service_detail_model extends MY_Model {

    public $limitStore = true;
    
    public function __construct() {
        parent::__construct();
        $this->table = 'zn_service_detail';
        $this->primary = 'id_trans_service_detail';
        $this->flex = true;
    }

    public function relation(){
        return array(
            'service' => array('zn_service','transactionservice_id','id_trans_service'),
            'status_service_new' => array('m_status_service_new','status_id','id_status'),
        );
    }


}
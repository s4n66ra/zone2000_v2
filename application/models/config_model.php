<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class config_model extends MY_Model {

	public $table = 'm_config';
	public $primary = 'config_id';
	public $flex = true;

	public function __construct()
	{
		parent::__construct();
	}
	

}

/* End of file config_model.php */
/* Location: ./application/models/config_model.php */
<?php 

class status_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'm_status';
        $this->primary = 'status_id';
        $this->flex = true;
    }

    public function relation(){
        return array(
            'type' => array('zn_transaction_type', 'transactiontype_id', 'status_type'),
            'pr' => array('m_status_pr', 'status_id', 'status_id'),
            'po' => array('m_status_po', 'status_id', 'status_id'),
            'service' => array('m_status_service', 'status_id', 'status_id'),
            'repair' => array('m_status_repair', 'status_id', 'status_id'),
        );
    }

}
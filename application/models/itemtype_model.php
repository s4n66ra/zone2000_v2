<?php 

class itemtype_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'm_item_type';
        $this->primary = 'itemtype_id';
    }

    public function options(){
    	$data = $this->get()->result();
    	$options = array();
        foreach ($data as $row) {
            $options[$row->itemtype_id] = $row->type_name ;
        }
        return $options;
    }

    public function options_filter(){
        $data = $this->get()->result();
        $options = array();
        $options[''] = 'All';
        foreach ($data as $row) {
            $options[$row->itemtype_id] = $row->type_name ;
        }
        return $options;
    }

}
<?php
class history_prbundle_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->flex = true;
        $this->table    = 'zn_history_prbundle';
        $this->primary  = 'historyprbundle_id';
    }

    public function relation(){
        return array(
            'status_bundle' => array('m_status_bundle','statusbundle_id', 'bundle_status'),
            'status' =>array('m_status','status_id','status_id','status_bundle'),
            'history' => array('zn_history', 'history_id', 'history_id'),
            'detail' => array('zn_history_prbundle_detail', 'historyprbundle_id', 'historyprbundle_id'),
            'pr' => array('zn_purchase_request', 'transactionbundle_id','bundle_id'),
            'pr_detail' => array('zn_purchase_request_detail','pr_id','pr_id','zn_purchase_request'),
            'item' => array('m_item', 'item_id', 'item_id', 'detail'),

            'po' => array('zn_purchase_order','bundle_id','bundle_id'),
            'po_detail'=>array('zn_purchase_order_detail','po_id','po_id','po'),
            'item_detail' => array('m_item', 'item_id', 'item_id', 'po_detail'),
            'supplier' => array('m_supplier','id_supplier','supplier_id','po'),
            'cabang' => array('m_cabang','id_cabang','store_id','po_detail'),

        );
    }

    public function createHistory($postore, $postoredetail){
        /*  
            $postore : po_store
            $postoredetail : array of po_store_detail
        */

        

    }   
}

?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shipping_model extends MY_Model {

    public $table   = 'zn_shipping';
    public $primary = 'shipping_id';
    public $flex    = true;
    public $limitStore = true;

    public function __construct() {
        parent::__construct();
    }

    public function relation(){
        return array(
            'store' => array('m_cabang', 'id_cabang', 'store_id'),
            'employee' => array('m_pegawai','user_id','user_id'),
            'shipping_koli' => array('zn_shipping_koli','shipping_id','shipping_id'),
            'koli' => array('zn_koli','koli_id','koli_id','shipping_koli'),
            'receiving' => array('zn_receiving_postore_koli','koli_id','koli_id','koli'),
            'purchase_order' => array('zn_purchase_order_detail','postore_id','postore_id','receiving'),
            'purchase_request' => array('zn_purchase_request_detail','prdetail_id','prdetail_id','purchase_order'),
            'item' => array('m_item','item_id','item_id','purchase_order'),
            'bagian_md' =>array('m_bagian_md','id_bagian_md','id_bagian_md','item'),
            'supplier' =>array('m_supplier','id_supplier','supplier_id','koli'),
            'item_supplier' => array('m_item_supplier','item_id','id_supplier','item.item_id and item_supplier.supplier_id = supplier'),

            //---------shipping sparepart-------------
            'purchase_request_sparepart' => array('zn_purchase_request','pr_id','pr_id'),
            'pr_detail_sparepart' => array('zn_purchase_request_detail','pr_id','pr_id'),
            'item_sparepart' => array('m_item','item_id','item_id','pr_detail_sparepart'),
            'store_sparepart' => array('m_cabang','id_cabang','store_id','purchase_request_sparepart'),
        );
    }

    public function create($data = array()){
        $this->load->model('transaction_model');
        $data['user_id']    = $this->session->userdata('user_id');
        $data['fstore_id']  = $this->session->userdata('store_id');
        $data['date_created'] = date::now_sql();
        $this->transaction_model->transactiontype_id = transactiontype::SHIPPING;
        $transaction_id = $this->transaction_model->createTransaction();
        
        $data['transaction_id'] = $transaction_id;
        parent::create($data);
    }

}
<?php

/**
 * Description of user_model
 *
 * @author Warman Suganda
 */
class user_model extends MY_Model {

    public $limit;
    public $offset;
    
    private $table2 = 'm_pegawai';
    private $table6 = 'm_grup';
    private $table7 = 'm_user_akses';
    private $table8 = 'm_menu';

    public function __construct() {
        parent::__construct();
        $this->table = 'm_user';
        $this->primary = 'user_id';
        $this->flex = true;
    }

    public function relation(){
        return array(
            'pegawai' => array('m_pegawai', 'user_id', 'user_id'),
            'grup' => array('m_grup','grup_id', 'grup_id'),
        );
    }

    private function data($condition = array()) {
        //$this->db->select('a.*, b.nama, c.nama_cabang, d.nama_jabatan, e.nama_unit_kerja, f.grup_nama');
        $this->db->select('a.*, b.nama_pegawai as nama, f.grup_nama');
        $this->db->from($this->table . ' a');
        $this->db->join($this->table2 . ' b', 'a.user_id = b.user_id','left');
        //$this->db->join($this->table3 . ' c', 'b.kd_cabang = c.kd_cabang');
        //$this->db->join($this->table4 . ' d', 'b.kd_jabatan = d.kd_jabatan');
        //$this->db->join($this->table5 . ' e', 'b.kd_unit_kerja = e.kd_unit_kerja');
        $this->db->join($this->table6 . ' f', 'b.grup_id = f.grup_id','left');

        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_by_id($id) {
        $condition['a.user_id'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function data_table() {
        // Filtering
        $condition = array();
        $kata_kunci = $this->input->post('kata_kunci');
        $cabang = $this->input->post('lokasi');
        $unit = $this->input->post('unit');
        $jabatan = $this->input->post('jabatan');

        if (!empty($kata_kunci))
            $condition["LOWER(a.nik) LIKE " . " '%" . strtolower($kata_kunci) . "%' OR LOWER(b.nama) LIKE " . " '%" . strtolower($kata_kunci) . "%'"] = null;
        if (!empty($cabang))
            $condition['b.kd_cabang'] = $cabang;
        if (!empty($unit))
            $condition['b.kd_unit_kerja'] = $unit;
        if (!empty($jabatan))
            $condition['b.kd_jabatan'] = $jabatan;

        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        $this->db->order_by('nik');
        //$this->db->limit($this->limit, $this->offset);
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {

            $id = $value->user_id;

            $action = '';

            if ($this->access_right->otoritas('edit')) {
                $action .= anchor(null, '<i class="icon-edit"></i>', array('id' => 'button-edit-' . $id, 'class' => 'btn', 'onclick' => 'load_form(this.id)', 'data-source' => base_url('user/edit/' . $id))) . ' ';
                $action .= anchor(null, '<i class="icon-undo"></i>', array('id' => 'button-reset-' . $id, 'class' => 'btn', 'onclick' => 'confirm_dialog_ajax(this.id)', 'data-message' => 'Apakah anda yakin akan mereset password?', 'data-source' => base_url('user/reset_password/' . $id)));
            }

            $rows[] = array(
                'nik' => $value->nik,
                'nama' => $value->nama,
                'username' => $value->user_name,
                'ket_password' => md5($value->user_name) == $value->user_password ? '<b style="color:red;">Default</b>' : '<b style="color:green;">Sudah Diganti</b>',
                'status_password' => hgenerator::datetime_diff('d', date('Y-m-d H:i:s'), $value->last_update . ' ' . date('H:i:s')) >= 30 ? '<b style="color:red;">Expired</b>' : '<b style="color:green;">Aktif</b>',
                'grup_nama' => $value->grup_nama,
                'status' => $value->is_active == 't' ? 'Aktif' : 'Tidak Aktif',
                'action' => !empty($action) ? $action : '<i class="icon-lock denied-color" title="Acces Denied"></i>'
            );
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function data_table_report() {
        // Filtering
        $condition = array();
        $kata_kunci = $this->input->post('kata_kunci');
        $cabang = $this->input->post('lokasi');
        $unit = $this->input->post('unit');
        $jabatan = $this->input->post('jabatan');

        if (!empty($kata_kunci))
            $condition["LOWER(a.nik) LIKE " . " '%" . strtolower($kata_kunci) . "%' OR LOWER(b.nama) LIKE " . " '%" . strtolower($kata_kunci) . "%'"] = null;
        if (!empty($cabang))
            $condition['b.kd_cabang'] = $cabang;
        if (!empty($unit))
            $condition['b.kd_unit_kerja'] = $unit;
        if (!empty($jabatan))
            $condition['b.kd_jabatan'] = $jabatan;

        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        $this->db->order_by('a.nik');
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $rows[] = array(
                'nik' => $value->nik,
                'nama' => $value->nama,
                'username' => $value->user_name,
                'ket_password' => md5($value->user_name) == $value->user_password ? 'Default' : 'Sudah Diganti',
                'status_password' => hgenerator::datetime_diff('d', date('Y-m-d H:i:s'), $value->last_update . ' ' . date('H:i:s')) >= 30 ? 'Expired' : 'Aktif',
                'grup_nama' => $value->grup_nama,
                'status' => $value->is_active == 't' ? 'Aktif' : 'Tidak Aktif'
            );
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function create($data) {
        $this->db->trans_begin();

        // Insert data grup roles
        $this->db->insert($this->table, array('grup_id' => $data['grup_id'], 'nik' => $data['nik'], 'user_name' => $data['user_name'], 'is_active' => $data['status']));
        $user_id = $this->db->insert_id();

        // Save akses
        // $this->save_akses($data, $user_id, $data['grup_id']);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function update($data, $id) {
        $this->db->trans_begin();
        $data['is_active'] = $data['status'];
        unset($data['status']);
        $this->update($data, $id);
        // Insert data grup roles
        //$this->db->update($this->table, array('grup_id' => $data['grup_id'], 'nik' => $data['nik'], 'user_name' => $data['user_name'], 'is_active' => $data['status']), array('user_id' => $id));

        // Save akses
        // $this->save_akses($data, $id, $data['grup_id']);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function save_akses($data, $userid, $grup_id) {

        // Delete Roles
        $this->db->delete($this->table7, array('userid' => $userid));

        // Parsing roles
        $roles = array();

        if (isset($data['otoritas_menu_view']) && is_array($data['otoritas_menu_view'])) {
            foreach ($data['otoritas_menu_view'] as $value) {
                $roles[$value]['is_view'] = 't';
            }
        }

        if (isset($data['otoritas_menu_add']) && is_array($data['otoritas_menu_add'])) {
            foreach ($data['otoritas_menu_add'] as $value) {
                $roles[$value]['is_add'] = 't';
            }
        }

        if (isset($data['otoritas_menu_edit']) && is_array($data['otoritas_menu_edit'])) {
            foreach ($data['otoritas_menu_edit'] as $value) {
                $roles[$value]['is_edit'] = 't';
            }
        }

        if (isset($data['otoritas_menu_delete']) && is_array($data['otoritas_menu_delete'])) {
            foreach ($data['otoritas_menu_delete'] as $value) {
                $roles[$value]['is_delete'] = 't';
            }
        }

        if (isset($data['otoritas_menu_approve']) && is_array($data['otoritas_menu_approve'])) {
            foreach ($data['otoritas_menu_approve'] as $value) {
                $roles[$value]['is_approve'] = 't';
            }
        }

        if (isset($data['otoritas_menu_export']) && is_array($data['otoritas_menu_export'])) {
            foreach ($data['otoritas_menu_export'] as $value) {
                $roles[$value]['is_import'] = 't';
            }
        }

        if (isset($data['otoritas_menu_print']) && is_array($data['otoritas_menu_print'])) {
            foreach ($data['otoritas_menu_print'] as $value) {
                $roles[$value]['is_print'] = 't';
            }
        }

        foreach ($roles as $menu_id => $value) {
            $data_roles = array(
                'kd_menu' => $menu_id,
                'userid' => $userid,
                'kd_grup' => $grup_id,
                'is_view' => isset($value['is_view']) ? $value['is_view'] : 'f',
                'is_add' => isset($value['is_add']) ? $value['is_add'] : 'f',
                'is_edit' => isset($value['is_edit']) ? $value['is_edit'] : 'f',
                'is_del' => isset($value['is_delete']) ? $value['is_delete'] : 'f',
                'is_apr' => isset($value['is_approve']) ? $value['is_approve'] : 'f',
                'is_import' => isset($value['is_import']) ? $value['is_import'] : 'f',
                'is_print' => isset($value['is_print']) ? $value['is_print'] : 'f'
            );

            $this->db->insert($this->table7, $data_roles);
        }
    }

    public function delete($id) {
        return $this->db->delete($this->table, array('permohonan_sakit_id' => $id));
    }

    private function data_akses($condition = array()) {
        $this->db->select('a.*, b.nama_menu, b.url, b.kd_parent, b.kd_grup_menu');
        $this->db->from($this->table7 . ' a');
        $this->db->join($this->table8 . ' b', 'a.kd_menu = b.kd_menu');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_data_akses($condition = array()) {
        $this->data_akses($condition);
        $this->db->order_by('b.menu_urutan');
        return $this->db->get();
    }

    public function parsing_akses($condition = array()) {
        $list = $this->get_data_akses($condition);
        $data = array(
            'is_view' => array(), 'is_add' => array(), 'is_edit' => array(), 'is_delete' => array(), 'is_approve' => array(), 'is_import' => array(), 'is_print' => array()
        );
        foreach ($list->result() as $value) {
            $key = $value->kd_menu;
            if ($value->is_view == 't')
                $data['is_view'][] = $key;
            if ($value->is_add == 't')
                $data['is_add'][] = $key;
            if ($value->is_edit == 't')
                $data['is_edit'][] = $key;
            if ($value->is_del == 't')
                $data['is_delete'][] = $key;
            if ($value->is_apr == 't')
                $data['is_approve'][] = $key;
            if ($value->is_import == 't')
                $data['is_import'][] = $key;
            if ($value->is_print == 't')
                $data['is_print'][] = $key;
        }
        return $data;
    }

    public function reset_password($id) {
        $this->db->trans_begin();

        $data_user = $this->get_by_id($id);
        if ($data_user->num_rows() > 0) {
            $user = $data_user->row();
            $data = array(
                'user_password' => md5($user->user_name)
            );
            $this->db->update($this->table, $data, array('user_id' => $id));
			$this->reset_salah_pass($id);
        }
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    /* heubeul */

    function get_all_user() {
        $nik = $this->input->post('txtnik');
        $nama = $this->input->post('select_nama');
        $filter = "";
        if ($nik != '') {
            $filter = " and m_user.nik ilike '%$nik%'";
        }
        if ($nama != '') {
            $filter = $filter . " and hr_pegawai.nama ilike '%$nama%'";
        }
        $query = "select m_user.user_id, m_user.nik, m_user.grup_id, m_grup.grup_nama, hr_pegawai.nama
                FROM m_user
                LEFT JOIN hr_pegawai ON m_user.nik=hr_pegawai.nik
                LEFT JOIN m_grup ON m_user.grup_id=m_grup.grup_id
                WHERE hr_pegawai.status_aktif='Aktif' $filter
                limit $this->limit offset $this->offset
                ";
        $q = $this->db->query($query);
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    function getComboJabatan() {
        $this->db->select('nama_jabatan');
        $this->db->from('hr_ref_jabatan');
        $query = $this->db->get();
        $hasil = "";
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil .= "<option>$data->nama_jabatan</option>";
            }
        }
        return $hasil;
    }

    function count_user() {
        $nik = $this->input->post('txtnik');
        $nama = $this->input->post('select_nama');
        $filter = "";
        if ($nik != '') {
            $filter = " and m_user.nik like '%$nik%'";
        }
        if ($nama != '') {
            $filter = $filter . " and hr_pegawai.nama like '%$nama%'";
        }
        $query = "select count(1) as jumlah
                FROM m_user
                LEFT JOIN hr_pegawai ON m_user.nik=hr_pegawai.nik 
                LEFT JOIN m_grup ON m_user.grup_id=m_grup.grup_id
                WHERE hr_pegawai.status_aktif='Aktif' $filter
                ";
        $q = $this->db->query($query);
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $data) {
                $hasil = $data->jumlah;
            }
            return $hasil;
        }
    }

    function retrieve_grup_roles() {
        $query = "select m_grup.grup_id, m_grup.grup_nama, m_grup.grup_deskripsi FROM m_grup ";
        $q = $this->db->query($query);
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    function retrieve_grup_roles_direktur() {
        $query = " select m_grup.grup_id, m_grup.grup_nama, m_grup.grup_deskripsi
            FROM m_grup WHERE grup_nama like '%Direktur%' or  grup_nama like '%Komisaris%'";
        $q = $this->db->query($query);
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    function retrieve_nik() {
        $query = " select hr_pegawai.nik, hr_pegawai.nama FROM hr_pegawai  WHERE hr_pegawai.status_aktif='Aktif'";
        $q = $this->db->query($query);
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    function add_data_user_mdl() {
        $nik = $this->input->post('txtnik');
        $username = $this->input->post('username');
        $password = $this->input->post('pswrd');
        $roles_id = $this->input->post('grup_roles');
        $data_user = array
            (
            'nik' => $nik,
            'user_name' => $username,
            'user_password' => md5($password),
            'grup_id' => $roles_id
        );
        $this->db->insert('m_user', $data_user);
        $q0 = $this->db->query("select user_id FROM m_user WHERE nik='$nik'");
        foreach ($q0->result_array() as $row) {
            $user_id = $row['user_id'];
        }
        $query = $this->db->query("select menu_id,grup_id FROM m_roles WHERE grup_id='$roles_id'");
        foreach ($query->result_array() as $row) {
            $id_menu = $row['menu_id'];
            $this->db->query("insert into m_user_akses (kd_menu,kd_grup,userid,is_view,is_add,is_edit,is_del,is_import,is_print,is_apr) values ('$id_menu','$roles_id','$user_id','y','y','y','y','y','y','y')");
        }
    }

    function data_user_mdl($noid) {
        $query = "  select m_user.nik, m_user.user_name, m_user.grup_id, m_grup.grup_nama,
                    hr_pegawai.nama
                    FROM m_user    
                    LEFT JOIN m_grup ON m_user.grup_id=m_grup.grup_id
                    LEFT JOIN hr_pegawai ON m_user.nik = hr_pegawai.nik
                    WHERE m_user.user_id='$noid'";
        $q = $this->db->query($query);
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    function editUserAkses($noid) {
        $query = "select * FROM m_grup_menu ORDER BY kd_grup_menu ASC";
        $q = $this->db->query($query);
        $hasil = array();
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $data) {
                $hasil[] = $data;
            }
        }
        return $hasil;
    }

    function update_data_user_mdl() {
        $user_id = $this->input->post('noid');
        $nik = $this->input->post('txtnik');
        $kd_menu = "";
        $kd_menu = "";
        $is_view = "";
        $is_add = "";
        $is_edit = "";
        $is_del = "";
        $is_import = "";
        $is_print = "";
        $is_apr = "";
        $user_id = "";
        $q0 = $this->db->query("select user_id,grup_id FROM m_user WHERE nik='$nik'");
        foreach ($q0->result_array() as $row) {
            $user_id = $row['user_id'];
            $grup_id = $row['grup_id'];
        }

        $query = $this->db->query("select m_user_akses.noid, m_user_akses.userid, m_user_akses.kd_menu,
                m_user_akses.kd_grup, m_user_akses.is_view, m_user_akses.is_add, m_user_akses.is_del,
                m_user_akses.is_edit, m_user_akses.is_import, m_user_akses.is_print, m_user_akses.is_apr,
                m_menu.nama_menu, m_grup_menu.nama_grup_menu, m_grup_menu.kd_grup_menu 
                FROM m_user_akses    
                LEFT JOIN m_menu ON m_user_akses.kd_menu=m_menu.kd_menu   
                LEFT JOIN m_grup_menu ON m_menu.kd_grup_menu=m_grup_menu.kd_grup_menu   
                WHERE m_user_akses.userid='$user_id'    order by m_grup_menu.kd_grup_menu ASC");
        foreach ($query->result_array() as $row) {
            $kd_menu = $row['kd_menu'];
            $is_view = $this->input->post('is_view' . $kd_menu);
            $is_add = $this->input->post('is_add' . $kd_menu);
            $is_edit = $this->input->post('is_edit' . $kd_menu);
            $is_del = $this->input->post('is_del' . $kd_menu);
            $is_import = $this->input->post('is_import' . $kd_menu);
            $is_print = $this->input->post('is_print' . $kd_menu);
            $is_apr = $this->input->post('is_apr' . $kd_menu);
            if ($is_view == 'on') {
                $is_view = 'y';
            }

            if ($is_add == 'on') {
                $is_add = 'y';
            }

            if ($is_edit == 'on') {
                $is_edit = 'y';
            }

            if ($is_del == 'on') {
                $is_del = 'y';
            }

            if ($is_import == 'on') {
                $is_import = 'y';
            }

            if ($is_print == 'on') {
                $is_print = 'y';
            }

            if ($is_apr == 'on') {
                $is_apr = 'y';
            }

            if ($is_view == '') {
                $is_view = 'f';
            }

            if ($is_add == '') {
                $is_add = 'f';
            }

            if ($is_edit == '') {
                $is_edit = 'f';
            }

            if ($is_del == '') {
                $is_del = 'f';
            }

            if ($is_import == '') {
                $is_import = 'f';
            }

            if ($is_print == '') {
                $is_print = 'f';
            }

            if ($is_apr == '') {
                $is_apr = 'f';
            }

            $query = $this->db->query("update  m_user_akses set is_view='$is_view', is_add='$is_add',
                is_edit='$is_edit', is_del='$is_del', is_import='$is_import', is_print='$is_print', is_apr='$is_apr'
                WHERE userid='$user_id' and kd_menu='$kd_menu'  
                ");
        }
    }

    function updateRoles() {
        $user_id = $this->input->post('noid');
        $nik = $this->input->post('txtnik');
        $roles_baru = $this->input->post('grup_roles');
        $q0 = $this->db->query("select user_id,grup_id FROM m_user WHERE nik='$nik'");
        foreach ($q0->result_array() as $row) {
            $user_id = $row['user_id'];
            $grup_id = $row['grup_id'];
        }
        if ($grup_id != $roles_baru) {
            $this->db->query("delete FROM m_user_akses WHERE userid='$user_id'");
            $this->db->query("update m_user set grup_id='$roles_baru' WHERE user_id='$user_id'");
            $q_m_roles = $this->db->query("select * FROM m_roles WHERE grup_id='$roles_baru'");
            foreach ($q_m_roles->result_array() as $row) {
                $id_menu = $row['menu_id'];
                $id_grup = $row['grup_id'];
                $is_view = $row['is_view'];
                $is_add = $row['is_add'];
                $is_edit = $row['is_edit'];
                $is_delete = $row['is_delete'];
                $is_import = $row['is_import'];
                $is_print = $row['is_print'];
                $is_approve = $row['is_approve'];
                if ($is_view == 't') {
                    $is_view = 'y';
                } else {
                    $is_view = 'd';
                }
                if ($is_add == 't') {
                    $is_add = 'y';
                } else {
                    $is_add = 'd';
                }
                if ($is_edit == 't') {
                    $is_edit = 'y';
                } else {
                    $is_edit = 'd';
                }
                if ($is_delete == 't') {
                    $is_delete = 'y';
                } else {
                    $is_delete = 'd';
                }
                if ($is_import == 't') {
                    $is_import = 'y';
                } else {
                    $is_import = 'd';
                }
                if ($is_print == 't') {
                    $is_print = 'y';
                } else {
                    $is_print = 'd';
                }
                if ($is_approve == 't') {
                    $is_approve = 'y';
                } else {
                    $is_approve = 'd';
                }
                $this->db->query("insert into m_user_akses (kd_menu,kd_grup,userid,is_view,is_add,is_edit,is_del,is_import,is_print,is_apr)  values ('$id_menu','$roles_baru','$user_id','$is_view','$is_add','$is_edit','$is_delete','$is_import','$is_print','$is_approve')");
            }
        }
    }

    function update_user() {
        $user_id = $this->input->post('noid');
        $nik = $this->input->post('txtnik');
        $username_lama = $this->input->post('txtulama');
        $password_lama = $this->input->post('txtplama');
        $username_baru = $this->input->post('txtubaru');
        $password_baru = md5($this->input->post('txtpbaru'));
        $date = date("Y-m-d");
        $this->db->query("update m_user set user_password ='$password_baru'  ,last_update='$date', user_name = '$username_baru'  WHERE nik='$nik'");
    }

    function delete_user_mdl() {
        $noid = $this->input->post('id');
        $this->db->trans_start();
        $this->db->query("delete FROM m_user WHERE user_id='$noid'");
        $this->db->query("delete FROM m_user_akses WHERE userid='$noid'");
        $st = $this->db->trans_status();
        if ($st == 1) {
            echo"SUKSES HAPUS DATA";
        } else {
            echo"GAGAL UPDATE DATA";
        }
        $this->db->trans_complete();
    }

    function reset_pass_mdl($id) {
        $query = $this->db->query("select nik FROM m_user WHERE user_id='$id'");
        foreach ($query->result_array() as $row) {
            $nik = $row['nik'];
        }
        $password = md5($nik);

        $this->db->trans_start();
        $this->db->query("update m_user set user_password ='$password' ,user_name='$nik'   WHERE user_id='$id'");
        $st = $this->db->trans_status();
        if ($st == 1) {
            echo"SUKSES UPDATE DATA";
        } else {
            echo"GAGAL UPDATE DATA";
        }
        $this->db->trans_complete();
    }

    public function get_by_nik($nik) {
        $condition['a.nik'] = $nik;
        $this->data($condition);
        return $this->db->get()->result_array();
    }
	/* EDIRTED BY AGNI */
	function check_old_pass(){
		 $username_lama = $this->input->post('txtulama');
         $password_lama = md5($this->input->post('txtplama'));
		 $q = $this->db->query("select count(1) as jumlah FROM  m_user WHERE user_name='$username_lama'  and user_password='$password_lama'");
		  if ($q->num_rows() > 0) {
            foreach ($q->result() as $data) {
				return $jumlah=$data->jumlah;
			}
		}	
	}
	function reset_salah_pass($id=''){
		$q=$this->db->query("select user_name from m_user where user_id='$id'");
		if ($q->num_rows() > 0) {
			  foreach ($q->result() as $data) {
				$username=$data->user_name;
			}
			$q=$this->db->query("delete from hr_salah_password where username='$username'");
		}
	}
	/*-----------------*/
    
	public function options_empty() {
		$data = $this->data()->get();
		$options = array();
		$options[''] = '';
		foreach ($data->result() as $row) {
			$options[$row->user_id] = $row->user_name ;
		}
		return $options;
	}
	
	public function get_jomblo_user(){
		$this->db->from($this->table);
		$this->db->join('m_pegawai', 'm_pegawai.user_id=m_user.user_id', 'left');
		$this->db->where('m_pegawai.user_id is null');
		$this->db->select('m_user.user_id, m_user.user_name');
		$data = $this->db->get();
		
		$options = array();
		$options[''] = '';
		foreach ($data->result() as $row) {
			$options[$row->user_id] = $row->user_name ;
		}
		return $options;
	}
}

/* End of file user_model.php */
/* Location: ./application/models/user_model.php */
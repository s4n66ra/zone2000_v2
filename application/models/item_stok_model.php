<?php 

class item_stok_model extends MY_Model {

    /*public function __construct() {
        parent::__construct();
        $this->table = 'm_item_stok';
        $this->primary = 'itemstok_id';
        $this->limitStore = true;
    }

    public function relation(){
        return array(
            'store' => array("m_cabang", "id_cabang", "store_id"),
        	'item' => array("m_item", "item_id", "item_id"),
            
        );
    }*/

    public function __construct() {
        parent::__construct();
        $this->table = 'zn_item_stok_tekno';
        $this->primary = 'itemstok_id';
        $this->limitStore = true;
    }

    public function relation(){
        return array(
            'store' => array("m_cabang", "id_cabang", "store_id"),
            'item' => array("m_item", "item_id", "item_id"),
            
        );
    }


}
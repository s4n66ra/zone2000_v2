<?php
class purchase_order_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table    = 'zn_purchase_request';
        $this->primary  = 'pr_id';
        $this->detail   = 'zn_purchase_request_detail';
        $this->history  = 'zn_purchase_request_history';
        $this->history_detail   = 'zn_purchase_request_history_detail';
        $this->detail_primary   = 'pr_id_detail';
        $this->detail_join      = 'm_hadiah';
        $this->detail_join_primary = 'id_hadiah';
        $this->cabang   = 'm_cabang';
        $this->cabang_primary = 'id_pengirim';
    }

    public function relation(){
        return array(
            'document' => array('zn_transaction_document','transaction_id', 'transaction_id'),
            'store' => array('m_cabang', 'id_cabang', 'store_id'),
        );
    }
    
    public function data_table() {
        $total = $this->where('store_id', $this->session->userdata('store_id'))->count_all_results();

        $data = $this
                ->where('store_id', $this->session->userdata('store_id'))
                ->with('document')
                ->limit($this->limit, $this->offset)
                ->get()
                ->result_array();
        

        $rows = array();
        
        foreach ($data as $key => $value) {
            $id = $value[$this->primary];
            $id_enc = url_base64_encode($id);            
            $action = '';
            $action .= '';

            $temp = array();

            if ($this->access_right->otoritas('edit')) {
                $action .= view::button_detail($id);
                $action .= view::button_edit($id_enc);
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= view::button_delete($id_enc);
            }

            $temp['primary_key']    = $value[$this->primary];
            $temp['doc_number']     = $value['doc_number'];
            $temp['date_created']   = $value['date_created'];
            $temp['pr_type']        = transaksi::getPrType($value['pr_type']);
            $temp['pr_status']      = transaksi::getPrStatus($value['pr_status']);
            $temp['pr_note']     = $value['pr_note'];

            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $temp['aksi'] = view::render_button_group_raw($action);
            }

            $rows[] = $temp;
        }
        
        return array('rows' => $rows, 'total' => $total);
    }   

    public function delete($id) {

        $this->db->trans_start();
       
        // edit setting
        $return = $this->db->delete($this->table, array($this->primary => $id));

        $this->db->trans_complete();
        return $return;
    }

    public function proceed($id, $status){

        /*
        $id : purchase_request_id
        $status = 
            0 => 'Draft',
            1 => 'Waiting Aproval',
            2 => 'Revision',
            3 => 'Rejected',
            4 => 'Aproved',
            5 => 'Shiping',
            6 => 'Delivered',
            7 => 'Canceled',
        */

        $this->load->model('purchase_request_detail_model');

        $return = true;
        $write_history_detail = true;
        $this->db->trans_start();

        $pr = $this->getById($id)->row_array();
        //return array('success'=>false, 'message'=>$pr);        
        if($pr && $pr['pr_status']!=$status && $status>=0 && $status<=7){
            $data   = array();
            $detail = $this->purchase_request_detail_model->where('pr_id',$id)->get()->result_array();
            $history= array(
                        'pr_id' => $id,
                        'tanggal' => date::now_sql(),
                        'id_pegawai' => $this->session->userdata('id_pegawai')
                    );
            $history_detail = array();

            switch ($status) {
                case 1:
                    // waiting approval
                    $data['pr_status']  = 1;
                    $data['date_request'] = date::now_sql();
                    $history['status']  = 1;  
                    break;

                case 2:
                    // revisi
                    $data['count_revisi']   = $pr->count_revisi + 1;
                    $data['pr_status']  = 2;
                    $data['date_revisi'] = date::now_sql();
                    $history['status']  = 2;  
                    break;
                
                case 3:
                    // rejected
                    $data['pr_status']      = 3;
                    $data['date_confirmed']    = date::now_sql();
                    $history['status']      = 3;
                    $write_history_detail   = false;  
                    break;

                case 4:
                    // approved
                    $data['pr_status']      = 4;
                    $data['date_confirmed'] = date::now_sql();
                    $history['status']      = 4;  
                    //$write_history_detail   = false;

                    // UPDATE JUMLAH DIKIRIM
                    foreach ($detail as $key => $value) {   
                        $this->update_detail(array('jml_disetujui' => $value['jumlah']),$value[$this->detail_primary]);
                    }
                    break;

                case 5:
                    // shipping
                    $data['pr_status']      = 5;
                    $data['tgl_dikirim']  = date::now_sql();
                    $history['status']      = 5;  
                    $write_history_detail   = false;

                    // UPDATE JUMLAH DIKIRIM
                    foreach ($detail as $key => $value) {   
                        $this->update_detail(array('jml_dikirim' => $value['jumlah']),$value[$this->detail_primary]);
                    }

                    break;

                case 6:
                    // delivered
                
                    $data['pr_status']      = 6;
                    $data['tgl_diterima']  = date::now_sql();
                    $history['status']      = 6;  
                    //$write_history_detail   = false;

                    $this->load->model('stok_hadiah_model');
                    foreach ($detail as $key => $value) {
                        // UPDATE JUMLAH DITERIMA
                        $this->update_detail(array('jml_diterima' => $value['jumlah']),$value[$this->detail_primary]);

                        // INSERT TO STOK HADIAH
                        $temp = array();
                        $condition['id_cabang'] = $this->session->userdata('id_cabang');
                        $condition['id_hadiah'] = $value['id_hadiah'];
                        $stok = $this->stok_hadiah_model->table()->where($condition)->get()->row_array();
                        if($stok){
                            // sudah ada
                            $temp['stok_total']+= $value['jumlah'];
                            $temp['stok_gudang']+= $value['jumlah'];
                            $this->stok_hadiah_model->update($temp,$stok['id_stok_hadiah']);
                        }else{
                            // create new
                            $temp['id_cabang'] = $this->session->userdata('id_cabang');
                            $temp['id_hadiah'] = $value['id_hadiah'];
                            $temp['stok_total'] =  $value['jumlah'];
                            $temp['stok_gudang']= $value['jumlah'];
                            $this->stok_hadiah_model->create($temp);
                        }

                    }

                    break;

                default:
                    # code...
                    break;
            }

            // update purchase_request
            $this->db->update($this->table, $data, array($this->primary => $id));

        } else {
            return array('success'=>false);
        }

        if($this->db->trans_status() === TRUE){
            $this->db->trans_complete();
            return array('success'=>true);
        }else{
            return array('success'=>false, 'message'=>$this->db->_error_message());
        }
    }
    
}

?>
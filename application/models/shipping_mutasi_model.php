<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class shipping_mutasi_model extends MY_Model {

    public $table   = 'zn_shipping_mutasi';
    public $primary = 'shipping_id';
    public $flex    = true;
    public $limitStore = true;

    public function __construct() {
        parent::__construct();
    }

    public function relation(){
        return array(
            'mutasi' => array('zn_mutasi','shipping_mutasi_id','shipping_id'),
            'item' => array('m_item','item_id','item_id','mutasi'),
            'outlet' => array('m_cabang','id_cabang','store_id','mutasi'),
            'outlet_destinasi' => array('m_cabang','id_cabang','store_destination_id','mutasi'),


            'store' => array('m_cabang', 'id_cabang', 'store_id'),
            'store_from' => array('m_cabang', 'id_cabang', 'fstore_id'),
            'employee' => array('m_pegawai','user_id','user_id'),
            'shipping_koli' => array('zn_shipping_koli','shipping_id','shipping_id'),
            'koli' => array('zn_koli','koli_id','koli_id','shipping_koli'),
            'receiving' => array('zn_receiving_postore_koli','koli_id','koli_id','koli'),
            'purchase_order' => array('zn_purchase_order_detail','postore_id','postore_id','receiving'),
            'purchase_request' => array('zn_purchase_request_detail','prdetail_id','prdetail_id','purchase_order'),
            //'item' => array('m_item','item_id','item_id','purchase_order')
        );
    }

    public function create($data = array()){
        $this->load->model('transaction_model');
        $data['user_id']    = $this->session->userdata('user_id');
        //$data['fstore_id']  = $this->session->userdata('store_id');
        $data['date_created'] = date::now_sql();
        $this->transaction_model->transactiontype_id = transactiontype::SHIPPING;
        $transaction_id = $this->transaction_model->createTransaction();
        
        $data['transaction_id'] = $transaction_id;
        parent::create($data);
    }

}
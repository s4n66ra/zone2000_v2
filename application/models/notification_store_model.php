<?php
class notification_store_model extends MY_Model {

    public $id;

    public function __construct() {
        parent::__construct();
        $this->flex = true;
        $this->table    = 'zn_notification_store';
        $this->primary  = 'id';
    }

    public function relation(){
        return array(
            'notification' => array('zn_notification','notif_id', 'notif_id'),
            'store' => array('m_cabang','id_cabang','store_id'),
            'pegawai' => array('m_pegawai', 'default_store_id', 'id_cabang', 'store'),     
            'user' => array('m_user','user_id','user_id'),
            'user_detail' => array('m_pegawai','user_id','user_id','user'),
        );
    }

    
}

?>
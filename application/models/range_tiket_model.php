<?php
class range_tiket_model extends My_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'm_range_tiket';
        $this->primary='id_range';
        $this->flex = true;
    }

    public $limit;
    public $offset;
    private $range  = 'm_range_tiket';


/*    public function relation(){
        return array(
            'item' => array('m_item','item_id','item_id'),
            'type' => array('m_hadiah_type', 'hadiahtype_id', 'hadiahtype_id'),
        );
    }*/

    function get_tiket($harga=1,$type=2,$st_member = 1){
       //$this->load->model('range_tiket_model');
        if($st_member == 1){  //------------- untuk member
            if($type == 2){
                $harga = $harga / 35; //--------------------- untuk non elektronik
            }else{
                $harga = $harga / 20;
            }
        }else{ //----------------untuk non-member
            if($type == 2){
                $harga = $harga / 30; // ----------- untuk non-elektronik
            }else{
                $harga = $harga / 15;
        }
        }
        $data = $this->db
            ->select('min,max,kelipatan')
            ->from('m_range_tiket')
            ->where('min <',$harga)
            ->where('max >=',$harga)
            ->get()->row();
        $tiket = !empty($data->min) ? $data->min :0;
        $kelipatan = $data->kelipatan;
        while($harga > $tiket){
            $tiket += $kelipatan;
        }
        return $tiket;
    }

    function get_tiket_by_id($item_id,$st_member = 1){
        if(!empty($item_id)){
            $this->load->model('item_model');
            $dataItem = $this->item_model->getById($item_id)->row();
            $data = $this->get_tiket($dataItem->harga,$dataItem->m_type,$st_member);
            $this->load->model('range_tiket_model');
            $dataTemp = $this->range_tiket_model
                ->getById($item_id)->row();
            return $data;
        }
    }

    private function data($condition = array()) {        
        $this->db->from($this->range);
        $this->db->where_condition($condition);
        return $this->db;
    }

   

    public function get_by_id($id) {
        $condition['id_hadiah'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_detail_by_id($id) {
        $condition['b.id'] = $id;
        $this->data_detail($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function data_table() {
        // Total Record
        $total = $this->data()->count_all_results();

        // List Data
        //$this->db->limit($this->limit, $this->offset);
        $data = $this->data()->get()->result();
        $rows = array();

        foreach ($data as $key => $value) {
            $id = $value->id_hadiah;
            $id_enc = url_base64_encode($id);
            $action = '';
            $action .= '';

            if ($this->access_right->otoritas('edit')) {
                $action .= view::button_edit($id_enc, array('full-width'=>0));
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= view::button_delete($id_enc, array('full-width'=>0));
            }

            $temp = array(
                'primary_key' =>$value->id_range,
                'min' => $value->min,
                'max' => $value->max,
                'kelipatan' => $value->kelipatan,
                'aksi' => $action
            );

            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $temp['aksi'] = view::render_button_group_raw($action);
            }
            $rows[] = $temp;
            
        }

        return array('rows' => $rows, 'total' => $total);
    }   
 

    public function create($data) {
        $return = false;
        $this->db->trans_start();
        $return = $this->db->insert($this->range, $data);
        $this->db->trans_complete();

        return $return;
    }

    public function update($data, $id) {
        $this->db->trans_start();
        $return = $this->db->update($this->range, $data, array('id_range' => $id));        
        $this->db->trans_complete();
        return $return;
    }

   

    public function delete($id) {
        return $this->db->delete($this->range, array('id_range' => $id));
    }    

    public function data_table_detail_excel() {
        // Total Record
        $total = $this->data_detail()->count_all_results();

        // List Data
        $data_detail = $this->data_detail()->get();
        $rows = array();

        foreach ($data_detail->result() as $value) {
            $id = $value->id_bbm_barang;
            $action = '';
            $rows[] = array(
                'primary_key' => $id,
                'kd_barang'=>$value->kd_barang,
                'nama_barang' => $value->nama_barang,
                'jumlah' => $value->jumlah_barang,
                'satuan' => $value->nama_satuan,
                'kategori' => $value->nama_kategori,
            );
        }

        return array('rows' => $rows, 'total' => $total);
    }    

    
}

?>
<?php
// by dina


class list_pegawai_login_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table = 'hr_pegawai';
    private $table2 = 'm_user';
	private $table3 = 'hr_ref_cabang';
	private $table4 = 'hr_ref_unit_kerja';
	private $table5 = 'hr_ref_jabatan';

	private function data($condition = array()) {
		//$this->db->select("to_char(b.last_login, 'dd-mm-yyyy') as last_login");
		//$cabang_user = $this->session->userdata('cabang_user');
		//$cu = strval($cabang_user);
        $this->db->from($this->table . ' a');
        $this->db->join($this->table2 . ' b', 'a.nik = b.nik', 'left');
		$this->db->join($this->table3 . ' c', 'a.kd_cabang = c.kd_cabang', 'left');
		$this->db->join($this->table4 . ' d', 'a.kd_unit_kerja = d.kd_unit_kerja', 'left');
		$this->db->join($this->table5 . ' e', 'a.kd_jabatan = e.kd_jabatan', 'left');
		
		//$this->db->where("a.kd_cabang = convert_int_to_string( ".$cabang_user.")");
        $this->db->where_condition() ;
        return $this->db;
    }

    public function get_by_id($id) {
        $condition['a.nik'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }
	
    public function data_table() {
		
        // Filtering
        $condition = array();
		$cabang_user = $this->session->userdata('cabang_user');
        $last_login = $this->input->post('last_login');
		
		// buat pencarian dan where query
		//if ($this->is_sdm) {
        if (!empty($last_login)) {
            //$condition['OR']['LOWER(a.tgl_akses) LIKE' . " '%" . strtolower($tgl_akses) . "%'"] = null;
			$condition['AND']['b.last_login >= '] = hgenerator::switch_tanggal($last_login); 
			$condition['AND']['a.kd_cabang'] = $cabang_user;
			$condition['AND']['c.kd_cabang'] = $cabang_user;
			$condition['AND']['d.kd_cabang'] = $cabang_user;
           // $condition['OR']['LOWER(b.nama_propinsi) LIKE' . " '%" . strtolower($kata_kunci) . "%'"] = null;
        }
		//}
		//else {
		
		//}
        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        $this->db->order_by('b.last_login', 'asc');
        $this->db->limit($this->limit, $this->offset);
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {

            $id = $value->nik;  // ??
			// buat button edit n hapus dan pake otoritas juga
			$action = '';
			// jangan lupa pake hak akses buat di gambar edit / hapus
			// if ($this->access_right->otoritas('edit')) {
				//$action .= anchor(null, '<i class="icon-edit"></i>', array('id' => 'button-edit-' . $id, 'class' => 'btn', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('referensi_jenis_kompetensi/edit/' . $id))) . ' ';
			//}
			 if ($this->access_right->otoritas('delete')) {
				$action .= anchor(null, '<i class="icon-trash"></i>', array('id' => 'button-delete-' . $id, 'class' => 'btn', 'onclick' => 'delete_row(this.id)', 'data-source' => base_url('list_pegawai_login/delete/' . $id)));
			}
            $rows[] = array(
                'nik' => $value->nik,
				'nama' => $value->nama,
				'last_login' => $value->last_login,
				//'kd_cabang' => $value->kd_cabang,
				'nama_cabang' => $value->nama_cabang,
				'nama_unit_kerja' => $value->nama_unit_kerja,
				'nama_jabatan' => $value->nama_jabatan
               // 'nama_propinsi' => $value->nama_propinsi,
               // 'aksi' => !empty($action) ? $action : '<i class="icon-lock denied-color" title="Acces Denied"></i>'
            );
        }

        return array('rows' => $rows, 'total' => $total);
    }

   /*public function options($default = '--Pilih Jenis Kompetensi--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->jenis_kompetensi] = $row->nama_jenis;
        }
        return $options;
    }*/

    /*public function create($data) {
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->table, $data, array('jenis_kompetensi' => $id));
    }*/

    public function delete($id) {
        return $this->db->delete($this->table, array('id' => $id));
    } 

    //added by Nisa
    public function get_all() {
        $hasil = array();
        $query = $this->db->get($this->table);
        foreach ($query->result() as $data) {
            $hasil[] = $data;
        }
        return $hasil;
    }
	
	
	
	/*public function tampilkan_pegawai_login() {
	
	$this->load->model('parameter_model');
	$parameter = $this->parameter_model->get();
            if ($array_login['kd_roles'] == $parameter->role_sdm) {
                $is_sdm = true;
            } else {
                $is_sdm = false;
            }
            $array_login['is_sdm'] = $is_sdm;

            $this->session->set_userdata($array_login);
            //redirect('dashboard');
	


	if ($array_login) {
	
        $query = $this->db->query("select 
        hr_pegawai.nik,
        to_char(m_user.last_login, 'dd-mm-yyyy') as tgl_login,
        hr_pegawai.nama,
        hr_pegawai.kd_cabang,
		hr_ref_cabang.kd_cabang,
		hr_ref_unit_kerja.kd_cabang,
		hr_ref_unit_kerja.nama_unit_kerja,
		hr_jabatan.kd_jabatan,
        from hr_pegawai        
        left join 
        m_user 
        on hr_pegawai.nik=m_user.nik
        left join 
        hr_ref_cabang 
        on hr_pegawai.kd_cabang=hr_ref_cabang.kd_cabang
		left join 
        hr_ref_unit_kerja 
        on hr_ref_cabang.kd_cabang=hr_ref_unit_kerja.kd_cabang
		left join 
        hr_jabatan 
        on hr_pegawai.nik=hr_
        where hr_ref_cabang.kd_cabang <= '$cabang_user'
        order by m_user.last_login DESC limit $this->limit offset $this->offset
        ");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }
	} */
 
	
	 
   
}// end of class

?>
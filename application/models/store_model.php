<?php
class store_model extends MY_Model {

    public $relation = array(
                        'area' => array('m_area', 'id_area', 'id_area'),
                        'city' => array('m_kota', 'id_kota', 'id_kota'),
                        'setting' => array('m_cabang_setting', 'id_cabang', 'id_cabang'),
                        'owner' => array('m_owner', 'id_owner', 'id_owner'),
                        'target' => array('zn_store_data','store_id','id_cabang')
                    );

    public function __construct() {
        parent::__construct();
        $this->table = 'm_cabang';
        $this->primary = 'id_cabang';
        $this->flex = true;
    }

    private $store          = 'm_cabang';
    private $store_setting  = 'm_cabang_setting';
    private $area           = 'm_area';
    private $negara         = 'm_negara';

    public function update_tiket(){
        $this->load->model('range_tiket_model');
        $this->db->from('m_item');
        $this->db->where('itemtype_id',2);
        //$this->db->where('item_id',11228);
        $hasil = $this->db->get();
        $i=1;
        foreach ($hasil->result() as $row) {
            
            if(empty($row->tiket) && $row->harga<=1000000){
                $tiket = $this->range_tiket_model->get_tiket($row->harga,$itemtype_id,2);
                $update = array('tiket'=>$tiket);
                $this->db->where('item_id',$row->item_id);
                $this->db->update('m_item',$update);
                $i++;
            }
            # code...
        }
        echo $i;
    }

    public function base_join(){
        $this->db->select('m_cabang.*,nama_kota,prop.id_provinsi,p.id_pulau,n.id_negara');
        $this->db->from($this->store);
        $this->db->join($this->store_setting. ' st','st.id_cabang = m_cabang.id_cabang','left');
        $this->db->join('m_area'. ' a','a.id_area = m_cabang.id_area','left');
        $this->db->join('m_kota'. ' k','k.id_kota = m_cabang.id_kota','left');
        $this->db->join('m_provinsi'. ' prop','prop.id_provinsi = k.id_provinsi','left');
        $this->db->join('m_pulau'. ' p','p.id_pulau = prop.id_pulau','left');
        $this->db->join('m_negara'. ' n','n.id_negara = p.id_negara','left');
        return $this->db;
    }    


    private function data($condition = array()) {
        $this->base_join();
        $this->db->select("m_cabang.id_cabang , m_cabang.kd_cabang, m_cabang.nama_cabang, n.nama_negara, p.nama_pulau, prop.nama_provinsi, k.nama_kota, a.nama_area, st.store_area, m_cabang.id_owner ");        
        $this->db->where_condition($condition);
        return $this->db;
    }

    

    public function get_by_id($id) {
        $condition[$this->store.'.id_cabang'] = $id;
        $this->base_join();
        $this->db->where_condition($condition);
        return $this->db->get();
    }


    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function data_table() {
        // Total Record
        $total = $this->data()->count_all_results();

        // List Data
        //$this->db->limit($this->limit, $this->offset);
        $data = $this->data()->get()->result_array();
        $rows = array();

        foreach ($data as $key => $value) {
            $id = $value['id_cabang'];
            $id_enc = url_base64_encode($id);            
            $action = '';
            $action .= '';

            $temp = array();

            if ($this->access_right->otoritas('edit')) {
                $action .= view::button_edit($id_enc, array('onclick'=>'btnLoadNextPage(this)'));
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= view::button_delete($id_enc, array('onclick'=>'btnLoadNextPage(this)'));
            }

            // mengganti index id_cabang menjadi primary_key
            $temp['primary_key']  = $value['id_cabang'];
            unset($value['id_cabang']);
            $temp = hgenerator::merge_array($temp, $value);

            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $temp['aksi'] = view::render_button_group_raw($action);
            }

            $rows[] = $temp;
        }

        return array('rows' => $rows, 'total' => $total);
    }   

    

    public function create($data) {
        /*
            data = array (
                kd_cabang, alamat, telp, fax,
                setting = array (
                            coin, rent_charge, service_charge, budget, target, dll
                        )
            )
        */
    
        $this->db->trans_start();

        $setting    = $data['setting'];
        unset($data['setting']);
        $store      = $data;

        // insert to store
        $return = $this->db->insert($this->store, $data);
        $id_store = $this->db->insert_id();

        // insert to store_setting
        $setting['id_cabang'] = $id_store;
        $return = $this->db->insert($this->store_setting,$setting);

        $this->db->trans_complete();
        return $return;
    }

    public function update($data,$id) {
        /*
            id  = id_cabang
            data = array (
                kd_cabang, alamat, telp, fax,
                setting = array (
                            coin, rent_charge, service_charge, budget, target, dll
                        )
            )
        */
    
        $this->db->trans_start();

        $setting    = $data['setting'];
        unset($data['setting']);
        $store      = $data;

        // edit store
        if($data)
            $return = $this->db->update($this->store, $data, array('id_cabang' => $id));

        // edit store_setting
        if($setting)
            $return = $this->db->update($this->store_setting, $setting, array('id_cabang' => $id));

        $this->db->trans_complete();
        return $return;
    }


    public function delete($id) {

        $this->db->trans_start();
       
        // edit setting
        $return = $this->db->delete($this->store_setting, array('id_cabang' => $id));

        // delete store
        $return = $this->db->delete($this->store,  array('id_cabang' => $id));

        $this->db->trans_complete();
    }
    
    
    public function options($default = '--Select Branch--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_cabang] = $row->nama_cabang ;
        }
        return $options;
    }  

    public function options_empty() {
    	$data = $this->data()->get();
    	$options = array();
    	$options['']  = 'Select Store';
    	foreach ($data->result() as $row) {
    		$options[$row->id_cabang] = $row->kd_cabang. '-' .$row->nama_cabang ;
    	}
    	return $options;
    }
}

?>
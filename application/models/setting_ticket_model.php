<?php 

class setting_ticket_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'm_setting_ticket';
        $this->primary = 'setting_id';
        $this->flex = true;
    }

    public function relation(){
        return array(
            'type' => array('zn_transaction_type', 'transactiontype_id', 'status_type'),
            'pr' => array('m_status_pr', 'status_id', 'status_id'),
            'po' => array('m_status_po', 'status_id', 'status_id'),
            'service' => array('m_status_service', 'status_id', 'status_id'),
            'repair' => array('m_status_repair', 'status_id', 'status_id'),
        );
    }

    public function update($data, $id) {
        $this->db->trans_start();

        // update hadiah
        $data['setting_qty']     = hgenerator::switch_number($data['setting_qty']);
        $return = $this->db->update($this->table, $data, array('setting_id' => $id));
        return $return;
    }

}
<?php 

class item_supplier_default_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'm_item_supplier_default';
        $this->primary = 'default_id';
    }

    public function relation(){
        return array(
            'hadiah' => array('m_hadiah','item_id','item_id'),
            'mesin'  => array('m_mesin','item_id','item_id'),
            'jenis_mesin' => array('m_jenis_mesin','id_jenis_mesin','id_jenis_mesin','m_mesin'),
            'machine' => array('m_mesin','id_mesin','machine_id','zn_transaction_machine'),
        );
    }

}
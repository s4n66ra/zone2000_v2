<?php 

class repair_model extends MY_Model {

    public $limitStore = true;
    
    public function __construct() {
        parent::__construct();
        $this->table = 'zn_transaction_repair';
        $this->primary = 'transactionrepair_id';
        $this->flex = true;
    }

    public function relation(){
        return array(
            'transaction_machine' => array('zn_transaction_machine','transaction_id','transaction_id'),
            'transaction_store' => array('zn_transaction_store','transaction_id','transaction_id'),
            'transaction_device' => array('zn_transaction_device', 'transaction_id','transaction_id'),
            'transaction' => array('zn_transaction', 'transaction_id','transaction_id'),
            'document' => array('zn_transaction_document', 'transaction_id','transaction_id'),

            'service' => array('zn_transaction_service', 'transactionservice_id', 'transactionservice_id'),
            'machine' => array('m_mesin','id_mesin','machine_id', 'transaction_machine'),
            'machine_type' => array('m_jenis_mesin','id_jenis_mesin','id_jenis_mesin','machine'),
            
            'store' => array('m_cabang', 'id_cabang', 'store_id'),
            'pegawai' => array('m_pegawai','id_pegawai','technician_id'),
        );
    }
}
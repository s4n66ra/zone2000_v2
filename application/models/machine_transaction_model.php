<?php
class machine_transaction_model extends MY_Model {

    public $limit;
    public $offset;
    public $keyword;

    public function __construct() {
        parent::__construct();
        $this->table = 'zn_transaction';
        $this->primary = 'transaction_id';
    }

    public function relation(){
        return array(
            'machine' => array('m_mesin','id_mesin','machine_id','zn_transaction_machine'),
            'store' => array('m_cabang','id_store','id_cabang'),
            'transaction_machine' => array('zn_transaction_machine','transaction_id','transaction_id'),
            'transaction_meter' => array('zn_transaction_meter','transaction_id','transaction_id'),
            'transaction_store' => array('zn_transaction_store','transaction_id','transaction_id'),
            'transaction_device' => array('zn_transaction_device', 'transaction_id','transaction_id'),
            'cabang' => array('m_cabang', 'id_cabang', 'store_id', 'zn_transaction_store'),
            'transaction_type' => array('zn_transaction_type', 'transactiontype_id', 'transactiontype_id')
        );
    }



    private function data($condition = array(), $like= array()) {
    
        $zn_transaction = $this->machine_transaction_model
                        ->with(array("transaction_machine","transaction_meter", "transaction_device", "transaction_store", "cabang", "machine", "transaction_type", "transaction_swipe"))
                        ->db();
        return $zn_transaction;
    }

    public function get_by_id($id) {
        $condition['id_mesin'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_detail_by_id($id) {
        $condition['b.id'] = $id;
        $this->data_detail($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function data_table_by_keyword($keyword = ''){
        $like['rec_user'] = $keyword;
        $this->data(array(),$like);
        return $this->db;
    }

    public function data_table() {

        if ($this->access_right->otoritas('edit') && $this->access_right->otoritas('delete')) {
            // Limit dan Offset Data
            $this->db->limit($this->limit, $this->offset);
            $this->db->like(array('rec_user' => $this->keyword));
            $data = $this->data()->get()->result();


            // Hitung Total row menggunakan Keyword
            $total = $this->data_table_by_keyword($this->keyword)->count_all_results();
            $rows = array();

            foreach ($data as $key => $value) {
                $id = $value->id_mesin;
                $id_enc = url_base64_encode($id);  
                $action = '';
                $action .= '';

                if($value->transactiontype_id==12){
                    $temp = array(
                        'primary_key' =>$value->transaction_id,
                        'device_id' => $value->device_id,
                        'machine_name' => $value->kd_mesin,
                        'machine_transaction_type' => $value->transactiontype_name,
                        'id_cabang' => $value->nama_cabang,
                        'num_diff' => $value->harga_game,
                        'rec_created' => $value->rec_created
                    );
                }else{
                    $temp = array(
                        'primary_key' =>$value->transaction_id,
                        'device_id' => $value->device_id,
                        'machine_name' => $value->kd_mesin,
                        'machine_transaction_type' => $value->transactiontype_name,
                        'id_cabang' => $value->nama_cabang,
                        'num_diff' => $value->num_diff."zzz",
                        'rec_created' => $value->rec_created
                    );                    
                }



                $rows[] = $temp;
            }

        }


        return array('rows' => $rows, 'total' => $total);
    }   


    public function create($data) { 
        return $this->db->insert($this->mesin, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->mesin, $data, array('id_mesin' => $id));
    }

    public function create_detail($data) {
        return $this->db->insert($this->table4, $data);
    }

    public function update_detail($data, $id) {
        return $this->db->update($this->table4, $data, array('id' => $id));
    }

    public function delete($id) {
        return $this->db->delete($this->mesin, array('id_mesin' => $id));
    }

    public function delete_detail($id) {
        return $this->db->delete($this->table4, array('id' => $id));
    }
    
    
    public function options($default = '--Pilih mesin--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_mesin] = $row->nama_mesin ;
        }
        return $options;
    }

    public function options_empty($field, $value) {
        $condition = array();
        if(is_array($field)){
            $condition = $field;
        }else{
            $condition[$field] = $value;
        }
        $data = $this->mesin_model->with('jenis_mesin')->db()
                ->where('id_cabang',$this->session->userdata('id_cabang'))
                ->get()->result();
        $options = array();
        $options['']  = '';
        foreach ($data as $row) {
            $options[$row->id_mesin] = $row->kd_mesin.' - '.$row->nama_jenis_mesin ;
        }
        return $options;
    }

    
}

?>
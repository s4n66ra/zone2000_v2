<?php
class pembelian_hadiah_model extends My_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $pembelian_hadiah  = 'zn_pembelian_hadiah';
    private $pegawai = 'm_pegawai';

    private function data($condition = array()) {
        $this->db->from($this->pembelian_hadiah);
        $this->db->join($this->cabang,$this->cabang.'.id_cabang='.$this->pembelian_hadiah.'.id_cabang','left');
        $this->db->join($this->supplier,$this->supplier.'.id_supplier='.$this->pembelian_hadiah.'.id_supplier','left');
        $this->db->where_condition($condition);
        return $this->db;
    }

    private function data_detail($condition = array()) {
        // =============Filtering===============
        //$condition = array();
        $kd_bbm= $this->input->post("kd_bbm");
        $id_supplier= $this->input->post("id_supplier");
        $id_purchase_order = $this->input->post('id_purchase_order');

        $id_bbm = $this->input->post('id_bbm');
        if(!empty($kd_bbm)){
            $condition["a.kd_bbm like '%$kd_bbm%'"]=null;
        }
        if(!empty($id_supplier)){
            $condition["a.id_supplier"]=$id_supplier;
        }
        if(!empty($id_purchase_order)){
            $condition["a.id_purchase_order"]=$id_purchase_order;
        }
        if(!empty($id_bbm)){
            $condition["a.id_bbm"]=$id_bbm;
        }

        $tanggal_awal = hgenerator::switch_tanggal($this->input->post('tanggal_awal'));
        $tanggal_akhir = hgenerator::switch_tanggal($this->input->post('tanggal_akhir'));
        //--------end filtering-----------------------------------


        $this->db->select('*,b.id as id_bbm_barang');
        $this->db->from($this->table  . ' a');
        $this->db->join($this->table4 . ' b',' a.id_bbm = b.id_bbm');
        $this->db->join($this->table5 . ' c',' b.id_barang = c.id_barang','left');
        $this->db->join($this->table6 . ' d',' c.id_satuan = d.id_satuan','left');
        $this->db->join($this->table7 . ' e',' e.id_kategori = c.id_kategori','left');
        $this->db->order_by('c.kd_barang ASC');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_by_id($id) {
        $condition['id_pembelian_hadiah'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_detail_by_id($id) {
        $condition['b.id'] = $id;
        $this->data_detail($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function data_table() {
        // Total Record
        $total = $this->data()->count_all_results();

        // List Data
        //$this->db->limit($this->limit, $this->offset);
        $data = $this->data()->get()->result();
        $rows = array();

        foreach ($data as $key => $value) {
            $id = $value->id_pembelian_hadiah;
            $id_enc = url_base64_encode($id);
            $action = '';
            $action .= '';

            if ($this->access_right->otoritas('edit')) {
                $action .= view::button_edit($id_enc, array('full-width'=>0));
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= view::button_delete($id_enc, array('full-width'=>0));
            }

            $temp = array(
                    'primary_key' =>$value->id_pembelian,
                    'kd_pembelian' => $value->kd_pembelian,
                    'nama_cabang' => $value->nama_cabang,
                    'nama_supplier' => $value->nama_supplier,
                    'status'=> $value->status,
                    'status_bayar'=> $value->status_bayar,
                    'tgl_dibuat'=> $value->tgl_dibuat,
                    'tgl_sampai'=> $value->tgl_sampai,
                    'aksi' => $action
            );

            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $temp['aksi'] = view::render_button_group_raw($action);
            }

            $rows[] = $temp;

        }

        return array('rows' => $rows, 'total' => $total);
    }   

    public function data_table_excel() {
        //=============Tahun Aktif====================
        $tahun_aktif = $this->input->post('tahun_aktif');
        if(empty($tahun_aktif))
        {
            $tahun_aktif = date('Y');
        }
        $tahun_aktif_awal = $tahun_aktif.'-01-01';
        $tahun_aktif_akhir = $tahun_aktif.'-12-31';
        $condition["a.tanggal_bbm >= '$tahun_aktif_awal'"] = null ;
        $condition["a.tanggal_bbm <= '$tahun_aktif_akhir'"] = null;
        //-----------end tahunaktif-------------------
        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $rows[] = array(
                'tanggal_bbm' => hgenerator::switch_tanggal($value->tanggal_bbm),
                'kd_bbm' => $value->kd_bbm,
                'kd_po' => $value->kd_po,
                'supplier' => $value->nama_supplier,
            );
        }

        return array('rows' => $rows, 'total' => $total);
    }     



    public function data_table_detail() {
        $id_bbm = $this->input->post('id_bbm');
        // Total Record
        $total = $this->data_detail()->count_all_results();

        // List Data
        $this->db->limit($this->limit, $this->offset);
        $data_detail = $this->data_detail()->get();
        $rows = array();

        foreach ($data_detail->result() as $value) {
            $id = $value->id_bbm_barang;
            $action = '';

            if ($this->access_right->otoritas('edit')) {
                $action .= anchor(null, '<i class="icon-edit"></i>', array('id' => 'button-edit-detail-' . $id, 'class' => 'btn', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('bbm/edit_detail/' .$id_bbm.'/'. $id))) . ' ';
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= anchor(null, '<i class="icon-trash"></i>', array('id' => 'button-delete-detail-' . $id, 'class' => 'btn', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('bbm/delete_detail/' .$id_bbm.'/'. $id)));
            }

            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $rows[] = array(
                    'primary_key' => $id,
                    'kd_barang'=>$value->kd_barang,
                    'nama_barang' => $value->nama_barang,
                    'jumlah' => $value->jumlah_barang,
                    'satuan' => $value->nama_satuan,
                    'kategori' => $value->nama_kategori,
                    'aksi' => $action
                );
            }else{
                $rows[] = array(
                    'primary_key' => $id,
                    'kd_barang'=>$value->kd_barang,
                    'nama_barang' => $value->nama_barang,
                    'jumlah' => $value->jumlah_barang,
                    'satuan' => $value->nama_satuan,
                    'kategori' => $value->nama_kategori,
                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    }    

    public function create($data) {	
        return $this->db->insert($this->pembelian_hadiah, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->pembelian_hadiah, $data, array('id_pembelian_hadiah' => $id));
    }

    public function create_detail($data) { 
        return $this->db->insert($this->table4, $data);
    }

    public function update_detail($data, $id) {
        return $this->db->update($this->table4, $data, array('id' => $id));
    }

    public function delete($id) {
        return $this->db->delete($this->pembelian_hadiah, array('id_pembelian_hadiah' => $id));
    }

    public function delete_detail($id) {
        return $this->db->delete($this->table4, array('id' => $id));
    }
    
    
    public function options($default = '--Pilih pembelian_hadiah--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_pembelian_hadiah] = $row->nama_pembelian_hadiah ;
        }
        return $options;
    }

    

    public function data_table_detail_excel() {
        // Total Record
        $total = $this->data_detail()->count_all_results();

        // List Data
        $data_detail = $this->data_detail()->get();
        $rows = array();

        foreach ($data_detail->result() as $value) {
            $id = $value->id_bbm_barang;
            $action = '';
            $rows[] = array(
                'primary_key' => $id,
                'kd_barang'=>$value->kd_barang,
                'nama_barang' => $value->nama_barang,
                'jumlah' => $value->jumlah_barang,
                'satuan' => $value->nama_satuan,
                'kategori' => $value->nama_kategori,
            );
        }

        return array('rows' => $rows, 'total' => $total);
    }    

    public function options_empty() {
        $data = $this->data()->get();
        $options = array();
        $options['']  = '';
        foreach ($data->result() as $row) {
            $options[$row->id_pembelian_hadiah] = $row->nama_pembelian_hadiah ;
        }
        return $options;
    }

    
}

?>
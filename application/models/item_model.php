<?php 

class item_model extends MY_Model {

    public $flex = true;

    public function __construct() {
        parent::__construct();
        $this->table    = 'm_item';
        $this->primary  = 'item_id';
    }

    public function relation(){
        
        return array(
            'hadiah' => array('m_hadiah','item_id','item_id'),
            'mesin'  => array('m_mesin','item_id','item_id'),
            'jenis_mesin' => array('m_jenis_mesin','id_jenis_mesin','id_jenis_mesin','mesin'),
            'item_supplier' => array('m_item_supplier', 'item_id', 'item_id'),
            'supplier' => array('m_supplier', 'id_supplier', 'supplier_id', 'item_supplier'),
            'repair_sparepart' => array('zn_repair_sparepart', 'item_id', 'item_id'),
        );

    }

    public function get_by_id($id){
    
        if($id != ''){
            $condition["item_id"] = $id;
            $this->data2($condition);
        }

        return $this->db->get();
    
    }


    public function get_by_key($key){
    
        if($key != ''){
            $condition["item_key"] = $key;
            $this->data2($condition);
        }
    
        return $this->db->get();

    }

    public function create($data){
        $this->db->insert('m_item',$data);

        $data = array(
            'kd_tiket' => $data['item_key'],
            'nama_tiket' => $data['item_name'],
            'item_key' => $this->input->post('kd_tiket'),
            'item_id' => $this->id_hasil
        );

    }


    public function data(){
        return $inventory_sum = $this->item_model
                            ->with(array("hadiah","machine"));
    }

    public function data2($condition = ''){
        $this->db->from('m_item');
        $this->db->where_condition($condition);
        return $this->db;
    }


    public function options_empty($itemtype_id = '') {

        if($itemtype_id != ''){
            $data = $this
                    ->where('itemtype_id', $itemtype_id)
                    ->get()->result();

        }else{
            $data = $this->get()->result();
        }

        $options = array();
        $options['']  = '';
        foreach ($data as $row) {
            $options[$row->item_id] = $row->item_key. ' - '.$row->item_name.' - '.$row->item_barcode  ;
        }
        return $options;
    }

    public function options_empty_supplier($item_id) {
        $data = $this->with(array('item_supplier', 'supplier'))->where('this.item_id', $item_id)->get()->result();
        $options = array();       
        $options['']  = ''; 
        foreach ($data as $row) {
            $options[$row->supplier_id] = $row->nama_supplier.' - '.$row->price;
        }
        return $options;
    }

    public function options_supplier($item_id) {
        $data = $this->with(array('item_supplier', 'supplier'))->where('this.item_id', $item_id)->get()->result();
        $options = array();        
        foreach ($data as $row) {
            $options[$row->supplier_id] = $row->nama_supplier.' - '.$row->price;
        }
        return $options;
    }
    
    public function options_sparepart(){
        $data = $this->where('itemtype_id', itemtype::ITEM_SPAREPART)->get()->result();
        $option = array();
        foreach ($data as $key => $row) {
            $option[$row->item_id] = $row->item_key.' - '.$row->item_name.' - '.$row->item_barcode;
        }
        return $option;
    }

    public function options($type = itemtype::ITEM_MACHINETYPE){
        $data = $this->where('itemtype_id', $type)->get()->result();
        $option = array();
        foreach ($data as $key => $row) {
            $option[$row->item_id] = $row->item_key.' - '.$row->item_name.' - '.$row->item_barcode;
        }
        return $option;
    }

    public function options_sparepart_select2(){
        return array_merge(array('' => ''), $this->_options());
    }

    public function getItemsByIdAndKeyword($itemtype_id=''){
        if($itemtype_id != ''){
            $data = $this
                    ->where('itemtype_id', $itemtype_id)
                    ->get()->result();
            $data2 = $this
                    ->where('(itemtype_id = 5 or itemtype_id = 6)', NULL)
                    ->get()->result();

        }else{
            $itemType = array('2','3','5','6');
            $data = $this->where_in('itemtype_id',$itemType)->get()->result();

            $data3 = $this->db->from('m_mesin')
                ->join('m_item','m_item.item_key = m_mesin.kd_mesin','LEFT')
                ->get()->result();

        }

        $options = array();
        $options['']  = '';
        foreach ($data as $row) {
            $options[$row->item_id] = $row->item_key. ' - '.$row->item_name.' - '.$row->item_barcode ;
        }
        if(!empty($data2))
            foreach ($data2 as $row) {
                $options[$row->item_id] = $row->item_key. ' - '.$row->item_name.' - '.$row->item_barcode ;
            }
        if(!empty($data3))
            foreach ($data3 as $row) {
                $string = str_replace(' ', '', $row->serial_number);
                $options[$row->id_mesin] = $row->serial_number. ' - '.$row->item_name.' - '.$string ;
            }
        return $options;
    }

    public function getAllItem(){
        if($itemtype_id != ''){
            $data = $this
                    ->get()->result();

        }else{
            $data = $this->get()->result();
        }

        $options = array();
        $options['']  = '';
        foreach ($data as $row) {
            $options[$row->item_id] = $row->item_key. ' - '.$row->item_name.' - '.$row->item_barcode  ;
        }
        return $options;
    }
}
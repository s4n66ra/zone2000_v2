<?php 

class stok_ofname_sc_mutasi_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'zn_stok_ofname';
        $this->primary = 'so_id';
        $this->limitStore = true;
    }

    public function relation(){
        return array(
            'store' => array("m_cabang", "id_cabang", "store_id"),
        	'item' => array("m_item", "item_id", "item_id")
        );
    }

}
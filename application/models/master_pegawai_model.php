<?php

class master_pegawai_model extends CI_Model {

    public $limit;
    public $offset;
    private $table = 'hr_pegawai';
    private $table2 = 'hr_ref_jabatan';
    private $table3 = 'hr_ref_unit_kerja';
    private $table4 = 'hr_ref_cabang';
    private $table5 = 'hr_pegawai_detail';
    private $table6 = 'hr_mpeg_sekolah';
    private $table7 = 'hr_ref_level';
    //Table untuk Keluarga
    private $table8 = 'hr_pegawai_keluarga';
    private $table9 = 'hr_ref_jenjang';
    private $table10 = 'hr_keluarga_status';
    //Table untuk Pendidikan
    private $table11 = 'hr_mpeg_sekolah';
    //Table untuk Pelatihan
    private $table12 = 'hr_mpeg_kursus';
    //Table untuk Karir
    private $table13 = 'hr_mpeg_karir';
    //Table untuk Direksi
    private $table14 = 'hr_masa_jabat_pengurus';
    //Table untuk Kota
    private $table15 = 'hr_kota';
    //Table untuk Propinsi
    private $table16 = 'hr_propinsi';
    private $table17 = 'ref_status_pegawai';
    private $table18 = 'hr_mapping_jabatan';
    private $table19 = 'hr_ref_status_kepegawaian';
    private $table20 = 'hr_ref_plafond_seragam';
    private $is_sdm;

    public function __construct() {
        parent::__construct();

        $this->is_sdm = $this->session->userdata('is_sdm');
    }

    public function data($condition = array(), $distinct = true) {
        $dis = '';
        if ($distinct)
            $dis = 'DISTINCT ON (a.nik)';

        $this->db->select($dis . "a.nik, a.nama, a.kd_level, a.apr, a.id, a.status_kerja, a.kd_jabatan, a.kd_unit_kerja, a.kd_cabang, a.no_sk, 
                     a.tgl_masuk, a.tanggal_hitung, a.tanggal_inisialisasi, a.tanggal_sk, a.tanggal_pengangkatan, a.tgl_capeg, a.tanggal_sk, a.status_aktif, a.kd_cabang_penempatan,                     
                     e.g_depan, e.g_belakang, e.tmp_lahir as kode_tmp_lahir, to_char(a.tgl_masuk, 'yyyy') as tahun_masuk, 
                     e.tgl_lahir, e.gender, e.status_kawin, 
                     e.agama,e.suku,e.kebangsaan,e.no_ktp,e.npwp,e.alamat,e.alamat_ktp,e.rtrw,e.kelurahan,e.kecamatan,e.kota,e.propinsi,
                     e.kd_pos,e.telpon,e.email,e.mobile, e.rekening, e.no_jamsostek, e.foto, e.st_rumah_dinas,                               
                     b.nama_jabatan, c.nama_unit_kerja, d.nama_cabang, f.jenjang,
                     h.*, i.*, g.keterangan as keterangan_level,(select d.nama_cabang from hr_pegawai x join hr_ref_cabang d on a.kd_cabang_penempatan= d.kd_cabang where x.nik= a.nik) as nama_penempatan,
		     j.status_kepegawaian,e.apr AS apr_detail,(select k.jenjang as nama_jenjang from hr_ref_jenjang k where k.jenjang_id = CAST(f.jenjang AS integer)),
			 (select x.nama_kota from hr_kota x where x.kd_kota = e.tmp_lahir) as tmp_lahir,
			 (select max(x.tahun_keluar) from hr_mpeg_sekolah x where x.nik=a.nik) as tahun_keluar_sekolah,
			 (select ja.jenjang  from hr_mpeg_sekolah xa,hr_ref_jenjang ja where xa.nik=a.nik and ja.jenjang_id = CAST(xa.jenjang AS integer) and xa.tahun_keluar = (select max(x.tahun_keluar) from hr_mpeg_sekolah x where x.nik=a.nik)) as pendidikan_terakhir", FALSE);

        $this->db->from($this->table . ' a');
        $this->db->join($this->table2 . ' b', 'a.kd_jabatan = b.kd_jabatan', 'left');
        $this->db->join($this->table3 . ' c', 'a.kd_unit_kerja = c.kd_unit_kerja', 'left');
        $this->db->join($this->table4 . ' d', 'a.kd_cabang = d.kd_cabang', 'left');
        $this->db->join($this->table5 . ' e', 'a.nik = e.nik', 'left');
        $this->db->join($this->table6 . ' f', 'a.nik = f.nik', 'left');
        $this->db->join($this->table7 . ' g', 'a.kd_level = g.kd_level', 'left');
        $this->db->join($this->table15 . ' h', 'e.kota = h.kd_kota', 'left');
        $this->db->join($this->table16 . ' i', 'e.propinsi = i.kd_propinsi', 'left');
        $this->db->join($this->table19 . ' j', 'a.status_kerja = j.kode_status_kepegawaian', 'left');

        $this->db->where_condition($condition);

        return $this->db;
    }
	
	public function update($data, $nik){
		return $this->db->update($this->table, $data, array('nik' => $nik));
	}
	
    public function data2($condition = array(), $distinct = true) {
        $dis = '';
        if ($distinct)
            $dis = 'DISTINCT ON (a.nik)';

        $this->db->select($dis . "a.nik, a.nama, a.kd_level, a.apr, a.id, a.status_kerja, a.kd_jabatan, a.kd_unit_kerja, a.kd_cabang, a.no_sk, 
            k.plafond,
                     a.tgl_masuk, a.tanggal_hitung, a.tanggal_inisialisasi, a.tanggal_sk, a.tanggal_pengangkatan, a.tgl_capeg, a.tanggal_sk, a.status_aktif, a.kd_cabang_penempatan,                     
                     e.g_depan, e.g_belakang, e.tmp_lahir as kode_tmp_lahir, to_char(a.tgl_masuk, 'yyyy') as tahun_masuk, 
                     e.tgl_lahir, e.gender, e.status_kawin, 
                     e.agama,e.suku,e.kebangsaan,e.no_ktp,e.npwp,e.alamat,e.alamat_ktp,e.rtrw,e.kelurahan,e.kecamatan,e.kota,e.propinsi,
                     e.kd_pos,e.telpon,e.email,e.mobile, e.rekening, e.no_jamsostek, e.foto,                               
                     b.nama_jabatan, c.nama_unit_kerja, d.nama_cabang, f.jenjang,
                     h.*, i.*, g.keterangan as keterangan_level,(select d.nama_cabang from hr_pegawai x join hr_ref_cabang d on a.kd_cabang_penempatan= d.kd_cabang where x.nik= a.nik) as nama_penempatan,
             j.status_kepegawaian,e.apr AS apr_detail,(select k.jenjang as nama_jenjang from hr_ref_jenjang k where k.jenjang_id = CAST(f.jenjang AS integer)),
             (select x.nama_kota from hr_kota x where x.kd_kota = e.tmp_lahir) as tmp_lahir,
             (select max(x.tahun_keluar) from hr_mpeg_sekolah x where x.nik=a.nik) as tahun_keluar_sekolah", FALSE);

        $this->db->from($this->table . ' a');
        $this->db->join($this->table2 . ' b', 'a.kd_jabatan = b.kd_jabatan', 'left');
        $this->db->join($this->table3 . ' c', 'a.kd_unit_kerja = c.kd_unit_kerja', 'left');
        $this->db->join($this->table4 . ' d', 'a.kd_cabang = d.kd_cabang', 'left');
        $this->db->join($this->table5 . ' e', 'a.nik = e.nik', 'left');
        $this->db->join($this->table6 . ' f', 'a.nik = f.nik', 'left');
        $this->db->join($this->table7 . ' g', 'a.kd_level = g.kd_level', 'left');
        $this->db->join($this->table15 . ' h', 'e.kota = h.kd_kota', 'left');
        $this->db->join($this->table16 . ' i', 'e.propinsi = i.kd_propinsi', 'left');
        $this->db->join($this->table19 . ' j', 'a.status_kerja = j.kode_status_kepegawaian', 'left');
        $this->db->join($this->table20 . ' k', 'k.kd_jabatan = a.kd_jabatan', 'left');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_by_id($id, $distinct = true) {
        $kode = str_replace('-', ' ', $id);
        $condition['a.nik'] = $kode;
        $this->data($condition, $distinct);
        return $this->db->get();
    }

    public function get_by_id_detail($id) {
        $kode = str_replace('-', ' ', $id);
        $condition['a.nik'] = $kode;
        $condition['e.apr'] = 'T';
        $this->data($condition);
        return $this->db->get();
    }

    public function get_data($condition = array(), $distinct = true) {
        $this->data($condition, $distinct);
        return $this->db->get();
    }

    public function get_data2($condition = array(), $distinct = true) {
        $this->data2($condition, $distinct);
        return $this->db->get();
    }

    public function get_count($condition = array()) {
        $this->data($condition, $distinct = false);
        return $this->db->count_all_results();
    }

    public function get_generate($prefix) {
        $this->db->distinct();
        $condition = array("a.nik ILIKE '$prefix%'" => null);
        $this->db->where($condition);
        if ($this->db->count_all_results($this->table . ' a', 'a.nik') > 0) {
            $this->db->from($this->table . ' a');
            $this->db->where($condition);
            $this->db->order_by('a.nik', 'DESC');
            $this->db->limit(1);
            return $this->db->get()->row()->nik + 1;
        } else {
            return $prefix . '00001';
        }
    }

    public function get_generate_max($prefix) {
        $this->db->distinct();
        $condition = array("a.nik ILIKE '$prefix%'" => null);
        $this->db->where($condition);
        if ($this->db->count_all_results($this->table . ' a', 'a.nik') > 0) {
            $this->db->from($this->table . ' a');
            $this->db->where($condition);
            $this->db->order_by('a.nik', 'DESC');
            //$this->db->limit(1);
            return $this->db->get()->row()->nik + 1;
        } else {
            return $prefix . '00001';
        }
    }

    public function data_hitung_all($condition = '') {

        $nik_s = $this->session->userdata('nik_session');

        $this->db->select("count(distinct a.nik) as total,", FALSE);
        $this->db->from($this->table . ' a');
        $this->db->join($this->table2 . ' b', 'a.kd_jabatan = b.kd_jabatan', 'left');
        $this->db->join($this->table3 . ' c', 'a.kd_unit_kerja = c.kd_unit_kerja', 'left');
        $this->db->join($this->table4 . ' d', 'a.kd_cabang = d.kd_cabang', 'left');
        $this->db->join($this->table5 . ' e', 'a.nik = e.nik', 'left');
        $this->db->join($this->table6 . ' f', 'a.nik = f.nik', 'left');
        $this->db->join($this->table7 . ' g', 'a.kd_level = g.kd_level', 'left');
        $this->db->join($this->table15 . ' h', 'e.kota = h.kd_kota', 'left');
        $this->db->join($this->table16 . ' i', 'e.propinsi = i.kd_propinsi', 'left');
        $this->db->join($this->table19 . ' j', 'a.status_kerja = j.kode_status_kepegawaian', 'left');

        //Jika sdm
        if ($this->is_sdm == '') {
            $this->db->where('a.nik', $nik_s);
        }

        $status = $this->input->post('status');
        if (!empty($status))
            $condition['a.status_kerja'] = $status;
        else
        if (empty($status)) {
            $condition["status_kerja <> '15' and status_kerja <> '16'"] = null;
        }


        $this->db->where($condition);

        return $this->db->get()->row()->total;
    }

    public function data_table() {
        // Filtering
        $condition = array();

        $nik_s = $this->session->userdata('nik_session');
        $grup_id = $this->session->userdata('kd_roles');

        $txtnik = $this->input->post('txtnik');
        $explode_nik = explode(' - ', $txtnik);
        $select_nama = $this->input->post('select_nama');
        $select_level = $this->input->post('select_level');
        $select_jabatan = $this->input->post('select_jabatan');
        $select_cabang = $this->input->post('select_cabang');
        $select_unit_kerja = $this->input->post('select_unit_kerja');
        $select_pendidikan = $this->input->post('select_pendidikan');
        $gender = $this->input->post('gender');
        $status = $this->input->post('status');

        if (!empty($txtnik))
            $condition['LOWER(a.nik) LIKE ' . " '%" . strtolower($explode_nik[0]) . "%' "] = null;
        if (!empty($select_nama))
            $condition['LOWER(a.nama) LIKE ' . " '%" . strtolower($select_nama) . "%' "] = null;

        if (!empty($select_level))
            $condition['g.kd_level'] = $select_level;

        if (!empty($select_jabatan))
            $condition['b.kd_jabatan'] = $select_jabatan;
        if (!empty($select_cabang))
            $condition['d.kd_cabang'] = $select_cabang;
        if (!empty($select_unit_kerja))
            $condition['c.kd_unit_kerja'] = $select_unit_kerja;

        if (!empty($select_pendidikan)) {
            $condition['f.jenjang'] = $select_pendidikan;
            $condition["f.tahun_keluar = (select max(x.tahun_keluar) from hr_mpeg_sekolah x where x.nik=a.nik)"] = null;
        }
        if (!empty($gender))
            $condition['e.gender'] = $gender;
        if (!empty($status))
            $condition['a.status_kerja'] = $status;
        else
        if (empty($status)) {
            $condition["status_kerja <> '15' and status_kerja <> '16'"] = null;
        }

        //Jika sdm
        if ($this->is_sdm == '') {
            $condition['a.nik'] = $nik_s;
        }



        // Total Record
        //$total = $this->data($condition)->count_all_results('', 'a.nik');
        $total = $this->data_hitung_all($condition);

        // List Data
        $this->db->order_by('a.nik', 'DESC');
        $this->db->limit($this->limit, $this->offset);
        $data = $this->data($condition)->get();
        $rows = array();

        $idx = 0;

        foreach ($data->result() as $value) {
            $idx++;
            $id = str_replace(' ', '-', $value->nik);

            $action = '';
            $action = anchor(null, '<i class="icon-zoom-in"></i>', array('id' => 'button-detail-pegawai-' . $idx, 'class' => 'btn transparant btn-tooltip btn-small', 'title' => 'Lihat Detail', 'onclick' => 'drildown(this.id)', 'rel' => $id, 'parent' => 'mytable', 'data-source' => base_url('pegawai/detail/' . $id))) . ' ';
            if ($this->access_right->otoritas('edit')) {
                $action .= anchor('pegawai/edit/' . $id, '<i class="icon-edit"></i>', array('id' => 'button-edit-' . $idx, 'class' => 'btn transparant btn-tooltip btn-small', 'title' => 'Ubah Data')) . ' ';
            }
            $action .= anchor('pegawai/cetak_detail_pegawai/' . $id, '<i class="icon-print"></i>', array('id' => 'button-detail-print-' . $idx, 'class' => 'btn transparant btn-tooltip btn-small', 'title' => 'Print Detail Profil', 'target' => '_blank'));

            $rows[] = array(
                'primary_key' => $id,
                'nik' => $value->nik,
                'nama' => $value->nama,
                'kd_level' => $value->keterangan_level,
                'kd_cabang' => $value->nama_cabang,
                'kd_cabang_penempatan' => $value->nama_penempatan,
                'kd_unit_kerja' => $value->nama_unit_kerja,
                'kd_jabatan' => $value->nama_jabatan,
                'status_kerja' => $value->status_kepegawaian,
                'jenjang' => '',
                'gender' => !empty($value->gender) ? hgenerator::opsi_gender($value->gender) : '',
                //'tahun_masuk' => $value->tgl_masuk,
                'aksi' => '<div style="min-width:120px;">' . $action . '</div>'
            );
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function data_table_cetak() {
        // Filtering
        $condition = array();

        $nik_s = $this->session->userdata('nik_session');
        //$grup_id = $this->session->userdata('kd_roles');

        $txtnik = $this->input->post('txtnik');
        $explode_nik = explode(' - ', $txtnik);
        $select_nama = $this->input->post('select_nama');
        $select_level = $this->input->post('select_level');
        $select_jabatan = $this->input->post('select_jabatan');
        $select_cabang = $this->input->post('select_cabang');
        $select_unit_kerja = $this->input->post('select_unit_kerja');

        $select_pendidikan = $this->input->post('select_pendidikan');
        $gender = $this->input->post('gender');
        $status = $this->input->post('status');

        if (!empty($txtnik))
            $condition['LOWER(a.nik) LIKE ' . " '%" . strtolower($explode_nik[0]) . "%' "] = null;
        if (!empty($select_nama))
            $condition['LOWER(a.nama) LIKE ' . " '%" . strtolower($select_nama) . "%' "] = null;

        if (!empty($select_level))
            $condition['g.kd_level'] = $select_level;

        if (!empty($select_jabatan))
            $condition['b.kd_jabatan'] = $select_jabatan;
        if (!empty($select_cabang))
            $condition['d.kd_cabang'] = $select_cabang;
        if (!empty($select_unit_kerja))
            $condition['c.kd_unit_kerja'] = $select_unit_kerja;

        if (!empty($select_pendidikan))
            $condition['f.jenjang'] = $select_pendidikan;
        if (!empty($gender))
            $condition['e.gender'] = $gender;
        if (!empty($status))
            $condition['a.status_kerja'] = $status;

        //jika bukan sdm, maka hanya dapat print data dirinya sendiri.
        if ($this->is_sdm == '')
            $condition['a.nik'] = $nik_s;

        $condition["status_kerja <> '15' and status_kerja <> '16'"] = null;


        // Total Record
        $total = $this->data($condition, $distinct = true)->count_all_results();

        // List Data
        $this->db->order_by('a.nik', 'DESC');
        $data = $this->data($condition, $distinct = true)->get();
        $rows = array();

        $idx = 0;

        foreach ($data->result() as $value) {
            $idx++;
            $rows[] = array(
                'nik' => $value->nik,
                'nama' => $value->nama,
                'kd_level' => $value->keterangan_level,
                'kd_cabang' => $value->nama_cabang,
                'kd_cabang_penempatan' => $value->nama_penempatan,
                'kd_unit_kerja' => $value->nama_unit_kerja,
                'kd_jabatan' => $value->nama_jabatan,
                'status_kerja' => $value->status_kepegawaian,
                'jenjang' => $value->pendidikan_terakhir,
                'gender' => !empty($value->gender) ? hgenerator::opsi_gender($value->gender) : ''
            );
        }

        return array('rows' => $rows, 'total' => $total);
    }

    private function data_keluarga($condition = array()) {

        $this->db->from($this->table8 . ' a');
        $this->db->join($this->table9 . ' b', 'a.jenjang_id = b.jenjang_id', 'left');
        $this->db->join($this->table10 . ' c', 'a.keluarga_status_id = c.keluarga_status_id', 'left');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function data_table_keluarga() {
        // Filtering
        $condition = array();

        // Total Record
        $total = $this->data_keluarga($condition)->count_all_results();

        // List Data
        $this->db->order_by('a.nik');
        $this->db->limit($this->limit, $this->offset);
        $data = $this->data_keluarga($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {

            $id = $value->nik;

            $action = anchor(null, '<i class="icon-edit"></i>', array('id' => 'button-edit-' . $id, 'class' => 'btn', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('pegawai/edit_keluarga/' . $id))) . ' ';
            $action .= anchor(null, '<i class="icon-trash"></i>', array('id' => 'button-delete-' . $id, 'class' => 'btn', 'onclick' => 'delete_row(this.id)', 'data-source' => base_url('pegawai/delete_keluarga/' . $id)));

            $rows[] = array(
                //'nik' => $value->nik,
                'keluarga_status' => $value->keluarga_status,
                'keluarga_nama' => $value->keluarga_nama,
                'keluarga_jenis' => $value->keluarga_jenis,
                'jenjang' => $value->jenjang,
                'keluarga_gender' => $value->keluarga_gender,
                'aksi' => $action
            );
        }

        return array('rows' => $rows, 'total' => $total);
    }

    private function data_pendidikan($condition = array()) {

        $this->db->from($this->table11 . ' a');
        $this->db->join($this->table9 . ' b', 'a.jenjang = b.jenjang', 'left');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function data_table_pendidikan() {
        // Filtering
        $condition = array();

        // Total Record
        $total = $this->data_pendidikan($condition)->count_all_results();

        // List Data
        $this->db->order_by('a.nik');
        $this->db->limit($this->limit, $this->offset);
        $data = $this->data_pendidikan($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {

            $id = $value->nik;

            $action = anchor(null, '<i class="icon-edit"></i>', array('id' => 'button-edit-' . $id, 'class' => 'btn', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('pegawai/edit_pendidikan/' . $id))) . ' ';
            $action .= anchor(null, '<i class="icon-trash"></i>', array('id' => 'button-delete-' . $id, 'class' => 'btn', 'onclick' => 'delete_row(this.id)', 'data-source' => base_url('pegawai/delete_pendidikan/' . $id)));

            $rows[] = array(
                //'nik' => $value->nik,
                'nama_sekolah' => $value->nama_sekolah,
                'jenjang' => $value->jenjang,
                'jurusan' => $value->jurusan,
                'tahun_masuk' => $value->tahun_masuk,
                'tahun_keluar' => $value->tahun_keluar,
                'no_ijazah' => $value->no_ijazah,
                'tanggal_ijazah' => $value->tanggal_ijazah,
                'gelar' => $value->gelar,
                'nilai' => $value->nilai,
                'aksi' => $action
            );
        }

        return array('rows' => $rows, 'total' => $total);
    }

    private function data_pelatihan($condition = array()) {

        $this->db->from($this->table12 . ' a');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function data_table_pelatihan() {
        // Filtering
        $condition = array();

        // Total Record
        $total = $this->data_pelatihan($condition)->count_all_results();

        // List Data
        $this->db->order_by('a.nik');
        $this->db->limit($this->limit, $this->offset);
        $data = $this->data_pelatihan($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {

            $id = $value->nik;

            $action = anchor(null, '<i class="icon-edit"></i>', array('id' => 'button-edit-' . $id, 'class' => 'btn', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('pegawai/edit_pelatihan/' . $id))) . ' ';
            $action .= anchor(null, '<i class="icon-trash"></i>', array('id' => 'button-delete-' . $id, 'class' => 'btn', 'onclick' => 'delete_row(this.id)', 'data-source' => base_url('pegawai/delete_pelatihan/' . $id)));

            $rows[] = array(
                //'nik' => $value->nik,
                'nama_kursus' => $value->nama_kursus,
                'lembaga' => $value->lembaga,
                'alamat' => $value->alamat,
                'tanggal_masuk' => $value->tanggal_masuk,
                'tanggal_keluar' => $value->tanggal_keluar,
                'no_sertifikat' => $value->no_sertifikat,
                'tanggal_sertifikat' => $value->tanggal_sertifikat,
                'aksi' => $action
            );
        }

        return array('rows' => $rows, 'total' => $total);
    }

    private function data_karir($condition = array()) {

        $this->db->from($this->table13 . ' a');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function data_table_karir() {
        // Filtering
        $condition = array();

        // Total Record
        $total = $this->data_karir($condition)->count_all_results();

        // List Data
        $this->db->order_by('a.nik');
        $this->db->limit($this->limit, $this->offset);
        $data = $this->data_karir($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {

            $id = $value->nik;

            $action = anchor(null, '<i class="icon-edit"></i>', array('id' => 'button-edit-' . $id, 'class' => 'btn', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('pegawai/edit_pelatihan/' . $id))) . ' ';
            $action .= anchor(null, '<i class="icon-trash"></i>', array('id' => 'button-delete-' . $id, 'class' => 'btn', 'onclick' => 'delete_row(this.id)', 'data-source' => base_url('pegawai/delete_pelatihan/' . $id)));

            $rows[] = array(
                //'nik' => $value->nik,
                'nama_perusahaan' => $value->nama_perusahaan,
                'nama_pekerjaan' => $value->nama_pekerjaan,
                'jabatan_terakhir' => $value->jabatan_terakhir,
                'tanggal_masuk' => $value->tanggal_masuk,
                'tanggal_keluar' => $value->tanggal_keluar,
                'gaji' => $value->gaji,
                'alasan_keluar' => $value->alasan_keluar,
                'aksi' => $action
            );
        }

        return array('rows' => $rows, 'total' => $total);
    }

    private function data_approval($condition = array()) {
        $this->db->select("DISTINCT(a.nik),
                            a.nama,
                            to_char(a.tgl_masuk, 'dd-mm-yyyy') as tgl_masuk, 
                            to_char(a.tgl_capeg, 'dd-mm-yyyy') as tgl_capeg,
                            b.nama_jabatan, 
                            c.nama_unit_kerja, 
                            d.nama_cabang,
                            b.kd_jabatan,
                            c.kd_unit_kerja,
                            d.kd_cabang", FALSE);
        $this->db->from($this->table . ' a');
        $this->db->join($this->table2 . ' b', 'a.kd_jabatan = b.kd_jabatan', 'left');
        $this->db->join($this->table3 . ' c', 'a.kd_unit_kerja = c.kd_unit_kerja', 'left');
        $this->db->join($this->table4 . ' d', 'a.kd_cabang = d.kd_cabang', 'left');

        $this->db->where("a.apr ='F'");

        $this->db->where_condition($condition);

        return $this->db;
    }

    public function data_table_approval() {
        // Filtering
        $condition = array();

        $nik_s = $this->session->userdata('nik_session');
        $grup_id = $this->session->userdata('kd_roles');

        $txtnik = $this->input->post('txtnik');
        $select_nama = $this->input->post('select_nama');
        $select_cabang = $this->input->post('select_cabang');
        $select_unit_kerja = $this->input->post('select_unit_kerja');
        $select_jabatan = $this->input->post('select_jabatan');


        if (!empty($txtnik))
            $condition['LOWER(a.nik) LIKE ' . " '%" . strtolower($txtnik) . "%' "] = null;
        if (!empty($select_nama))
            $condition['LOWER(a.nama) LIKE ' . " '%" . strtolower($select_nama) . "%' "] = null;

        if (!empty($select_cabang))
            $condition['a.kd_cabang'] = $select_cabang;
        if (!empty($select_unit_kerja))
            $condition['a.kd_unit_kerja'] = $select_unit_kerja;
        if (!empty($select_jabatan))
            $condition['a.kd_jabatan'] = $select_jabatan;

        // Total Record
        $total = $this->data_approval($condition)->count_all_results();

        // List Data
        $this->db->order_by('a.nik');
        $this->db->limit($this->limit, $this->offset);
        $data = $this->data_approval($condition)->get();
        $rows = array();
        $idx = 0;

        foreach ($data->result() as $value) {
            $idx++;
            $id = hgenerator::encode_npp($value->nik);

            $action = anchor(null, '<i class="icon-retweet"></i>', array('id' => 'button-compare-pegawai-' . $idx, 'class' => 'btn transparant btn-tooltip btn-small', 'title' => 'Bandingkan Data', 'onclick' => 'drildown(this.id)', 'rel' => $id, 'parent' => 'mytable', 'data-source' => base_url('pegawai/compare/' . $id))) . ' ';
            $action .= anchor(null, '<i class="icon-ok"></i>', array('id' => 'button-approve-' . $idx, 'class' => 'btn transparant btn-tooltip btn-small', 'title' => 'Setuju', 'onclick' => 'approve_row(this.id)', 'data-source' => base_url('pegawai/approve/' . $id . '/T'))) . ' ';
            $action .= anchor(null, '<i class="icon-remove"></i>', array('id' => 'button-reject-' . $idx, 'class' => 'btn transparant btn-tooltip btn-small', 'title' => 'Tolak', 'onclick' => 'reject_row(this.id)', 'data-source' => base_url('pegawai/approve/' . $id . '/R'))) . ' ';
            //$action = anchor('pegawai/compare/' . $id, '<i class="icon-retweet"></i>', array('id' => 'button-compare-' . $idx, 'class' => 'btn light_blue btn-small', 'title' => 'Bandingkan'));

            $rows[] = array(
                'primary_key' => $id,
                'nik' => $value->nik,
                'nama' => $value->nama,
                'nama_cabang' => $value->nama_cabang,
                'nama_unit_kerja' => $value->nama_unit_kerja,
                'nama_jabatan' => $value->nama_jabatan,
                'aksi' => $action
            );
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function get_detail($condition = array()) {
        $this->db->from($this->table5);
        $this->db->where_condition($condition);
        return $this->db;
    }

    public function data_table_foto() {
        // Filtering
        $condition = array();
        $kata_kunci = $this->input->post('kata_kunci');

        if (!empty($kata_kunci))
            $condition['AND']['LOWER(a.nik) LIKE' . " '%" . strtolower($kata_kunci) . "%'"] = null;
        $condition['OR']['LOWER(a.nama) LIKE' . " '%" . strtolower($kata_kunci) . "%'"] = null;

        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        $this->db->order_by('a.nik', 'DESC');
        $this->db->limit($this->limit, $this->offset);
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {

            $id = $value->nik;

            $action = anchor(null, '<i class="icon-edit"></i>', array('id' => 'button-edit-foto-' . $id, 'class' => 'btn', 'title' => 'Ubah Foto', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('manajemen_file/edit/' . $id)));

            if (!empty($value->foto)) {
                $gambar = "file/foto/" . $value->foto;
            } else {
                $gambar = "images/no_foto.gif";
            }

            list($width, $height) = getimagesize($gambar);
            if ($width < $height) {
                $panjang = '150px';
                $lebar = '200px';
            } else {
                $panjang = '200px';
                $lebar = '150px';
            }

            $rows[] = array(
                'nik' => $value->nik,
                'nama' => $value->nama,
                'foto' => '<div class="fileupload-new thumbnail" style="width: ' . $panjang . '; height: ' . $lebar . ';"> <img src="' . base_url() . $gambar . '"></div>',
                'aksi' => $action
            );
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function approve($id, $st) {

        $this->db->trans_begin();

        if ($st == 'T') {

            //approve hapus
            $this->db->delete($this->table5, array('nik' => $id, 'apr' => 'H'));

            if ($this->get_detail(array('nik' => $id))->count_all_results() >= 2) {
                //1.Hapus hr_peg_detail where nik AND apr='T'
                $this->db->delete($this->table5, array('nik' => $id, 'apr' => 'T'));
            }

            //2.Update hr_peg_detail SET apr='T' where nik AND apr='F'
            $this->db->update($this->table5, array('apr' => 'T'), array('nik' => $id, 'apr' => 'F'));

            /* ============update ke moodle ============ */

            $moodle_db = $this->load->database(ACTIVE_GROUP_DB . '_moodle', TRUE); /* database conection for moodle */

            //update data pegawai ke table  learn_user database moodle
            $get_detail_nik = $this->master_pegawai_model->get_data(array('a.nik' => $id));
            foreach ($get_detail_nik->result() as $val) {
                $moodle['username'] = $val->nik;
                $moodle['firstname'] = $val->nama;
                $moodle['institution'] = $val->nama_jabatan;
                $moodle['department'] = $val->nama_unit_kerja;
                $moodle['city'] = $val->nama_cabang;
                $moodle['email'] = $val->email;
            }
            $moodle_db->update('learn_user', $moodle, array('username' => $id));
            /* ============tutup update ke moodle ============ */


            //3.Ambil nama_baru dari hr_peg_detail where nik AND apr='T'
            $nam_pegawai = $this->db->query("SELECT nama FROM hr_pegawai_detail WHERE nik='$id' AND apr='T'");
            foreach ($nam_pegawai->result_array() as $row) {
                $nama_baru = $row['nama'];
            }

            //Update hr_pegawai_keluarga where nik AND apr='F'
            //$this->db->update('hr_pegawai_keluarga', array('apr' => 'T'), array('nik' => $id));
            $data_keluarga = $this->db->get_where($this->table8, array('apr' => 'F', 'nik' => $id));
            foreach ($data_keluarga->result() as $value) {
                $this->db->delete($this->table8, array('keluarga_id' => $value->id_edit));
                $this->db->update($this->table8, array('apr' => 'T', 'id_edit' => NULL), array('keluarga_id' => $value->keluarga_id));
            }
            $this->db->update($this->table8, array('apr' => 'T'), array('apr' => 'E'));
            //approve hapus
            $this->db->delete($this->table8, array('nik' => $id, 'apr' => 'H'));

            //Update hr_mpeg_sekolah where nik AND apr='F'
            //$this->db->update('hr_mpeg_sekolah', array('apr' => 'T'), array('nik' => $id));
            $data_sekolah = $this->db->get_where($this->table11, array('apr' => 'F', 'nik' => $id));
            foreach ($data_sekolah->result() as $value) {
                $this->db->delete($this->table11, array('mpeg_sekolah_id' => $value->id_edit));
                $this->db->update($this->table11, array('apr' => 'T', 'id_edit' => NULL), array('mpeg_sekolah_id' => $value->mpeg_sekolah_id));
            }
            $this->db->update($this->table11, array('apr' => 'T'), array('apr' => 'E'));
            //approve hapus
            $this->db->delete($this->table11, array('nik' => $id, 'apr' => 'H'));

            //Update hr_mpeg_kursus where nik AND apr='F'
            //$this->db->update('hr_mpeg_kursus', array('apr' => 'T'), array('nik' => $id));
            $data_kursus = $this->db->get_where($this->table12, array('apr' => 'F', 'nik' => $id));
            foreach ($data_kursus->result() as $value) {
                $this->db->delete($this->table12, array('mpeg_khusus_id' => $value->id_edit));
                $this->db->update($this->table12, array('apr' => 'T', 'id_edit' => NULL), array('mpeg_khusus_id' => $value->mpeg_khusus_id));
            }
            $this->db->update($this->table12, array('apr' => 'T'), array('apr' => 'E'));
            //approve hapus
            $this->db->delete($this->table12, array('nik' => $id, 'apr' => 'H'));

            //Update hr_mpeg_karir where nik AND apr='F'
            //$this->db->update('hr_mpeg_karir', array('apr' => 'T'), array('nik' => $id));
            $data_karir = $this->db->get_where($this->table13, array('apr' => 'F', 'nik' => $id));
            foreach ($data_karir->result() as $value) {
                $this->db->delete($this->table13, array('mpeg_karir_id' => $value->id_edit));
                $this->db->update($this->table13, array('apr' => 'T', 'id_edit' => NULL), array('mpeg_karir_id' => $value->mpeg_karir_id));
            }
            $this->db->update($this->table13, array('apr' => 'T'), array('apr' => 'E'));
            //approve hapus
            $this->db->delete($this->table13, array('nik' => $id, 'apr' => 'H'));


            //4.Update hr_peg SET nama=nama_baru AND apr='T' where nik
            $this->db->update($this->table, array('nama' => $nama_baru, 'apr' => 'T'), array('nik' => $id));
        } elseif ($st == 'R') {
            //1.Hapus hr_peg_detail where nik AND apr='F'
            $this->db->delete($this->table5, array('nik' => $id, 'apr' => 'F'));

            //Hapus hr_pegawai_keluarga where nik AND apr='F'
            //$this->db->delete('hr_pegawai_keluarga', array('nik' => $id, 'apr' => 'F'));
            $data_keluarga = $this->db->get_where($this->table8, array('apr' => 'F', 'nik' => $id));
            foreach ($data_keluarga->result() as $value) {
                $this->db->delete($this->table8, array('keluarga_id' => $value->keluarga_id));
                $this->db->update($this->table8, array('apr' => 'T'), array('keluarga_id' => $value->id_edit));
            }
            $this->db->delete($this->table8, array('apr' => 'E'));
            //reject hapus
            $this->db->update($this->table8, array('apr' => 'T'), array('nik' => $id, 'apr' => 'H'));

            //Hapus hr_mpeg_sekolah where nik AND apr='F'
            //$this->db->delete('hr_mpeg_sekolah', array('nik' => $id, 'apr' => 'F'));
            $data_sekolah = $this->db->get_where($this->table11, array('apr' => 'F', 'nik' => $id));
            foreach ($data_sekolah->result() as $value) {
                $this->db->delete($this->table11, array('mpeg_sekolah_id' => $value->mpeg_sekolah_id));
                $this->db->update($this->table11, array('apr' => 'T'), array('mpeg_sekolah_id' => $value->id_edit));
            }
            $this->db->delete($this->table11, array('apr' => 'E'));
            //reject hapus
            $this->db->update($this->table11, array('apr' => 'T'), array('nik' => $id, 'apr' => 'H'));

            //Hapus hr_mpeg_kursus where nik AND apr='F'
            //$this->db->delete('hr_mpeg_kursus', array('nik' => $id, 'apr' => 'F'));
            $data_kursus = $this->db->get_where($this->table12, array('apr' => 'F', 'nik' => $id));
            foreach ($data_kursus->result() as $value) {
                $this->db->delete($this->table12, array('mpeg_khusus_id' => $value->mpeg_khusus_id));
                $this->db->update($this->table12, array('apr' => 'T'), array('mpeg_khusus_id' => $value->id_edit));
            }
            $this->db->delete($this->table12, array('apr' => 'E'));
            //reject hapus
            $this->db->update($this->table12, array('apr' => 'T'), array('nik' => $id, 'apr' => 'H'));

            //Hapus hr_mpeg_karir where nik AND apr='F'
            //$this->db->delete('hr_mpeg_karir', array('nik' => $id, 'apr' => 'F'));
            $data_karir = $this->db->get_where($this->table13, array('apr' => 'F', 'nik' => $id));
            foreach ($data_karir->result() as $value) {
                $this->db->delete($this->table13, array('mpeg_karir_id' => $value->mpeg_karir_id));
                $this->db->update($this->table13, array('apr' => 'T'), array('mpeg_karir_id' => $value->id_edit));
            }
            $this->db->delete($this->table13, array('apr' => 'E'));
            //reject hapus
            $this->db->update($this->table13, array('apr' => 'T'), array('nik' => $id, 'apr' => 'H'));

            //2.Update hr_peg SET apr='T' where nik
            $this->db->update($this->table, array('apr' => 'T'), array('nik' => $id));
        }


        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    //add
    public function count_by_year() {
        $this->db->select('count(*)');
        $this->db->where("(extract(year from tgl_masuk) = (select extract(year from now()::date)))");
        $query = $this->db->get('hr_pegawai');
        $result = $query->result_array();
        return $result[0]['count'];
    }

    function count_pegawai() {
        $hasil = 0;
        $unit_kerja = $this->input->post('unit_kerja');
        $kd_jabatan = $this->session->userdata('kd_jabatan');
        $lokasi_kerja = $this->input->post('lokasi_kerja');
        $nik = $this->input->post('txtnik');
        $nama = $this->input->post('txtnama');
        $nik_s = $this->session->userdata('nik_session');
        $filter = "";
        $filter_user = "";
        $grup_id = "";
        $filter_pegawai = "";
        $kd_cabang_user = $this->session->userdata('cabang_user');
        $kd_unit_kerja_user = $this->session->userdata('kd_unit_kerja');
        $grup_id = $this->session->userdata('kd_roles');
        $jabatan = $this->input->post('jabatan');
        $level = $this->input->post('level');
        $status2 = $this->input->post('status');
        $gender = $this->input->post('gender');
        $tahun_masuk = $this->input->post('tahun_masuk');
        $jenjang = $this->input->post('jenjang');
        if ($lokasi_kerja == '' and $unit_kerja == '' and $nik == '' and $nama == '' and $level == '' and $jabatan == '') {
            $filter_user = "";
        }
        if ($lokasi_kerja != '') {
            $filter_user = " and hr_pegawai.kd_cabang='$lokasi_kerja'";
        }
        if ($unit_kerja != '') {
            $filter_user = $filter_user . " and hr_pegawai.kd_unit_kerja='$unit_kerja'";
        }
        if ($nik != '') {
            $filter_user = $filter_user . " and hr_pegawai.nik ilike '%$nik%'";
        }
        if ($nama != '') {
            $filter_user = $filter_user . " and  hr_pegawai.nama ilike '%$nama%'";
        }
        if ($jabatan != '') {
            $filter_user = $filter_user . " and hr_pegawai.kd_jabatan='$jabatan'";
        }
        if ($level != '') {
            $filter_user = $filter_user . " and hr_pegawai.kd_level='$level'";
        }
        if ($status2 != '') {
            if ($status2 == 'Pegawai Tetap')
                $filter_user = $filter_user . " and hr_pegawai.kd_level != '0'";
            else
                $filter_user = $filter_user . " and hr_pegawai.kd_level = '0'";
        }
        if ($gender != '') {
            if ($gender == 'Perempuan')
                $filter_user = $filter_user . " and hr_pegawai_detail.gender = 'Perempuan'";
            else
                $filter_user = $filter_user . " and hr_pegawai_detail.gender = 'Laki-laki'";
        }
        if ($tahun_masuk != '') {
            $filter_user = $filter_user . "  AND to_char(hr_pegawai.tgl_masuk, 'YYYY') = '$tahun_masuk'";
        }
        if ($jenjang != '') {
            $filter_user = $filter_user . "  AND view_pendidikan_terakhir.jenjang = '$jenjang' ";
        }
        $u_kerja_temp = "";
        $get_unit_kerja = $this->db->query("select kd_unit_kerja from hr_unit_kerja_cabang where kd_cabang='000'");
        foreach ($get_unit_kerja->result_array() as $row) {
            $u_kerja_temp = $u_kerja_temp . " or hr_pegawai.kd_unit_kerja='" . $row['kd_unit_kerja'] . "'";
        }
        $status = $this->session->userdata('status');
        $nik_session = $this->session->userdata('nik_session');
        if ($grup_id != '4' && $grup_id != 14):
            $filter = " AND hr_pegawai.nik = '$nik_s'";
        else:
            $filter = "";
        endif;
        $query = "  SELECT DISTINCT(hr_pegawai.nik), hr_pegawai.nama, hr_pegawai.kd_level, hr_pegawai.apr, hr_pegawai.id,
                    to_char(hr_pegawai.tgl_masuk, 'dd-mm-yyyy') as tgl_masuk, to_char(hr_pegawai.tgl_capeg, 'dd-mm-yyyy') as tgl_capeg,                     
                    hr_pegawai_detail.g_depan, hr_pegawai_detail.g_belakang, hr_pegawai_detail.tmp_lahir, 
                    to_char(hr_pegawai_detail.tgl_lahir, 'dd-mm-yyyy') as tgl_lahir, hr_pegawai_detail.gender,                                
                    hr_ref_jabatan.nama_jabatan, hr_ref_unit_kerja.nama_unit_kerja, hr_ref_cabang.nama_cabang              
                    FROM hr_pegawai
                    LEFT JOIN hr_ref_jabatan ON hr_pegawai.kd_jabatan=hr_ref_jabatan.kd_jabatan
                    LEFT JOIN hr_ref_unit_kerja ON hr_pegawai.kd_unit_kerja=hr_ref_unit_kerja.kd_unit_kerja
                    LEFT JOIN hr_ref_cabang ON hr_pegawai.kd_cabang=hr_ref_cabang.kd_cabang 
                    LEFT JOIN hr_pegawai_detail ON hr_pegawai.nik=hr_pegawai_detail.nik
                    LEFT JOIN view_pendidikan_terakhir ON view_pendidikan_terakhir.nik = hr_pegawai.nik
                    WHERE (hr_pegawai.status_aktif='Aktif' OR hr_pegawai.status_aktif = 'Non Job')
                    AND length(hr_pegawai.kd_level) < 3  AND hr_pegawai.kd_level != 'ho' AND hr_pegawai_detail.apr = 'T'
                    $filter_user $filter $filter_pegawai";
        $q = $this->db->query($query);
        return $q->num_rows();
    }

// MENAMBAH DATA PEGAWAI BARU
    function tambah_pegawai_baru_model_new() {
        $tgl_masuk_pegawai = '1900-01-01';
        $apr_by = $this->session->userdata('nik_session');
        $apr_date = date("Y-m-d");
        $ptkp = '';
        $k_natura = '';
        $jumlah_anak = '';
        $status_keluarga = '';
        /* hr_pegawai */
        $nama_pegawai = $this->input->post('nama');
        $nik = $this->input->post('txtnik_hide');
        $gelar_depan = $this->input->post('g_depan');
        $gelar_belakang_pegawai = $this->input->post('g_belakang');
        $tmp_lahir = $this->input->post('tmp_lahir');
        $gol_darah = $this->input->post('select_gol_darah');
        $tgl_lahir = $this->input->post('tgl_lahir');
        if ($this->input->post('tgl_lahir') == '')
            $tgl_lahir = '01-01-1000';
        else
            $tgl_lahir = $this->input->post('tgl_lahir');
        $gender = $this->input->post('gender');
        $select_status_kawin_pegawai = $this->input->post('status_kawin');
        $agama_pagawai = $this->input->post('agama');
        $suku_marga_pegawai = $this->input->post('suku');
        $alamat_pegawai = $this->input->post('alamat');
        $rtrw_pegawai = $this->input->post('rtrw');
        $desa_kelurahan_pegawai = $this->input->post('kelurahan');
        $kecamatan = $this->input->post('kecamatan');
        $kab_or_kota_pegawai = $this->input->post('select_kota');
        $kode_pos = $this->input->post('kd_pos');
        $prov_pegawai = $this->input->post('select_propinsi');
        $telf_pegawai = $this->input->post('telpon');
        $hp_pegawai = $this->input->post('mobile');

        $email = $this->input->post('email');
        $j_pegawai = $this->input->post('status_kerja_pegawai');
        $status_pegawai = $this->input->post('status_aktif_pegawai');
        $kebangsaan = $this->input->post('kebangsaan');
        $alamat = $this->input->post('alamat');
        $alamat_ktp = $this->input->post('alamat_ktp');
        $st_rumah_dinas = $this->input->post('st_rumah_dinas');

        // added by dina
        $rtrw_ktp = $this->input->post('rtrw_ktp');
        $desa_kelurahan_ktp = $this->input->post('kelurahan_ktp');
        $kecamatan_ktp = $this->input->post('kecamatan_ktp');
        $kab_or_kota_ktp = $this->input->post('select_kota_ktp');
        $kode_pos_ktp = $this->input->post('kd_pos_ktp');
        $prov_ktp = $this->input->post('select_propinsi_ktp');
        $lokasi_penempatan = $this->input->post('lokasi_penempatan');
        //echo $lokasi_penempatan;

        /* mutasi */

        $kd_jabatan = $this->input->post('select_jabatan');
        $kd_unit_kerja = $this->input->post('select_unit_kerja');
        $kd_lokasi = $this->input->post('select_cabang');
        //dina
        //$lokasi_penempatan = $this->input->post('select_cabang');


        /* Peminta */
        $peminta = $this->input->post('peminta');
        /* Pangkat */
        $select_pangkat = $this->input->post('select_level');
        /* Seleksi */
        $noid_seleksi = $this->input->post('no_seleksi');
        /* Date Properties */
        $tgl_capeg = $this->input->post('tgl_capeg');
        $tgl_masuk_pegawai = $this->input->post('tgl_masuk');
        $no_sk = $this->input->post('no_sk');
        $tgl_sk = $this->input->post('tanggal_sk');
        $mulai_berlaku_sk = $this->input->post('mulai_berlaku_sk');
        $akhir_berlaku_sk = $this->input->post('akhir_berlaku_sk');


        $no_rek = $this->input->post('rekening');

        $npwp = $this->input->post('npwp');
        $no_ktp = $this->input->post('no_ktp');
        $no_jamsostek = $this->input->post('no_jamsostek');
        //Data User //
        $roles_id = $this->input->post('select_roles');
//        $count_jumlah_anak = $this->db->query("select count(nik) as jumlah_anak from hr_mpeg_anak where nik='$nik'");
//        foreach ($count_jumlah_anak->result_array() as $row) {
//            $jumlah_anak = $row['jumlah_anak'];
//        }
//        if ($jumlah_anak > 3) {
//            $jumlah_anak = 3;
//        }



        if ($gender == 'P' and $select_pangkat == 0) {
            $k_natura = "b";
            $ptkp = "tk";
        }
        if ($gender == 'L' and $select_pangkat == 0) {
            $k_natura = "bl";
            $ptkp = "tk";
        }

        if ($select_status_kawin_pegawai == 'BK' and $gender == 'P' and $select_pangkat > 0) {
            $k_natura = "b";
            $ptkp = "tk";
        }
        if ($select_status_kawin_pegawai == 'BK' and $gender == 'L' and $select_pangkat > 0) {
            $k_natura = "k0";
            $ptkp = "tk";
        }

        if ($select_status_kawin_pegawai == 'K' and $gender == 'P' and $select_pangkat > 0) {
            $k_natura = "b";
            $ptkp = "k";
        }
        if ($select_status_kawin_pegawai == 'K' and $gender == 'L' and $select_pangkat > 0) {
            $k_natura = "k0";
            $ptkp = "k";
        }


        if ($select_status_kawin_pegawai == 'K' and $gender == 'L' and $select_pangkat > 0) {
            $k_natura = "k";
            $ptkp = "k";
        }
        if ($select_status_kawin_pegawai == 'K' and $gender == 'P' and $select_pangkat > 0) {
            $k_natura = "kts";
            $ptkp = "k";
        }
        if ($select_status_kawin_pegawai == 'BK' and $gender == 'L' and $select_pangkat > 0) {
            $k_natura = "k";
            $ptkp = "k";
        }
        if ($select_status_kawin_pegawai == 'BK' and $gender == 'P' and $select_pangkat > 0) {
            $k_natura = "kts";
            $ptkp = "k";
        }


        if ($gender == 'L' and $select_status_kawin_pegawai == 'K') {
            $status_keluarga = 'X/0';
        }
        if ($gender == 'P' and $select_status_kawin_pegawai == 'K') {
            $status_keluarga = 'Y/0';
        }
        if ($gender == 'P' and $select_status_kawin_pegawai == 'D') {
            $status_keluarga = 'X/0';
        }
        if ($gender == 'P' and $select_status_kawin_pegawai == 'J') {
            $status_keluarga = 'Y/0';
        }

        if ($gender == 'L' and $select_pangkat == '0' and $select_status_kawin_pegawai == 'BK') {
            $status_keluarga = 'BL';
        }
        if ($gender == 'P' and $select_pangkat == '0' and $select_status_kawin_pegawai == 'BK') {
            $status_keluarga = 'BP';
        }

        if ($gender == 'L' and $select_status_kawin_pegawai == 'BK') {
            $status_keluarga = 'BL';
        }
        if ($gender == 'P' and $select_status_kawin_pegawai == 'BK') {
            $status_keluarga = 'BP';
        }

        //echo "PTKP=" . $ptkp . "<br>";
        //echo "NATURA=" . $k_natura . "<br>";
        //echo "STATUS KELUARGA=" . $status_keluarga . "<br>";


        switch ($gender) {
            case '2':
                $gender = "2";
                break;
            case '1':
                $gender = "1";
                break;
        }

        switch ($select_status_kawin_pegawai) {
            case 'K':
                $select_status_kawin_pegawai = "Kawin";
                break;
            case 'BK':
                $select_status_kawin_pegawai = "Belum Kawin";
                break;
            case 'D':
                $select_status_kawin_pegawai = "Duda";
                break;
            case 'J':
                $select_status_kawin_pegawai = "Janda";
                break;
        }

        $tgl_lahir = hgenerator::switch_tanggal($tgl_lahir);
        $tgl_masuk_pegawai = hgenerator::switch_tanggal($tgl_masuk_pegawai);
        $tgl_capeg = hgenerator::switch_tanggal($tgl_capeg);
        $tgl_sk = hgenerator::switch_tanggal($tgl_sk);

        $data_pegawai = array(
            'nik' => $nik,
            'nama' => $nama_pegawai,
            'kd_level' => !empty($select_pangkat) ? $select_pangkat : null,
            'status_aktif' => 'Aktif',
            'tgl_capeg' => $tgl_capeg,
            'tgl_masuk' => $tgl_masuk_pegawai,
            'kd_unit_kerja' => !empty($kd_unit_kerja) ? $kd_unit_kerja : null,
            'kd_jabatan' => !empty($kd_jabatan) ? $kd_jabatan : null,
            'tanggal_sk' => $tgl_sk,
            'no_sk' => $no_sk,
            'status_kerja' => $j_pegawai,
            'kd_cabang' => !empty($kd_lokasi) ? $kd_lokasi : null,
            'apr' => 'T',
            'apr_by' => $apr_by,
            'apr_tanggal' => $apr_date,
            'kd_cabang_penempatan' => empty($lokasi_penempatan) ? $lokasi_penempatan : null
        );

        $data_detail = array(
            'nik' => $nik,
            'nama' => $nama_pegawai,
            'tmp_lahir' => !empty($tmp_lahir) ? $tmp_lahir : null,
            'gender' => $gender,
            'g_depan' => $gelar_depan,
            'g_belakang' => $gelar_belakang_pegawai,
            'gender' => $gender,
            'tgl_lahir' => $tgl_lahir,
            'kebangsaan' => $kebangsaan,
            'foto' => '',
            'alamat' => $alamat,
            'alamat_ktp' => $alamat_ktp,
            'rtrw' => $rtrw_pegawai,
            'kelurahan' => $desa_kelurahan_pegawai,
            'kecamatan' => $kecamatan,
            'kota' => $kab_or_kota_pegawai,
            'propinsi' => $prov_pegawai,
            'kd_pos' => $kode_pos,
            'telpon' => $telf_pegawai,
            'email' => $email,
            'mobile' => $hp_pegawai,
            'suku' => $suku_marga_pegawai,
            'agama' => $agama_pagawai,
            'status_kawin' => $select_status_kawin_pegawai,
            'nik_tmp' => '',
            'apr' => 'T',
            'rekening' => $no_rek,
            'npwp' => $npwp,
            'no_ktp' => $no_ktp,
            'no_jamsostek' => $no_jamsostek,
            'rtrw_ktp' => $rtrw_ktp,
            'kelurahan_ktp' => $desa_kelurahan_ktp,
            'kecamatan_ktp' => $kecamatan_ktp,
            'select_kota_ktp' => !empty($kab_or_kota_ktp) ? $kab_or_kota_ktp : null,
            'kd_pos_ktp' => $kode_pos_ktp,
            'select_propinsi_ktp' => !empty($prov_ktp) ? $prov_ktp : null,
            'st_rumah_dinas' => $st_rumah_dinas
        );

        //jika checkbox di klik
        $klik = $this->input->post('chkBoxb');
        if (empty($klik)) {
            $data_detail['alamat'] = $alamat;
            $data_detail['rtrw'] = $rtrw_pegawai;
            $data_detail['kelurahan'] = $desa_kelurahan_pegawai;
            $data_detail['kecamatan'] = $kecamatan;
            $data_detail['kota'] = !empty($kab_or_kota_pegawai) ? $kab_or_kota_pegawai : null;
            $data_detail['propinsi'] = !empty($prov_pegawai) ? $prov_pegawai : null;
            $data_detail['kd_pos'] = $kode_pos;
        } else {
            $data_detail['alamat'] = $alamat_ktp;
            $data_detail['rtrw'] = $rtrw_ktp;
            $data_detail['kelurahan'] = $desa_kelurahan_ktp;
            $data_detail['kecamatan'] = $kecamatan_ktp;
            $data_detail['kota'] = !empty($kab_or_kota_ktp) ? $kab_or_kota_ktp : null;
            $data_detail['propinsi'] = !empty($prov_ktp) ? $prov_ktp : null;
            $data_detail['kd_pos'] = $kode_pos_ktp;
        }

        $potongan_gaji = $this->db->query("select * from hr_ref_potongan_gaji");
        foreach ($potongan_gaji->result_array() as $row) {
            $data_potongan_gaji = array
                (
                'nik' => $nik,
                'kd_potongan_gaji' => $row['kd_potongan_gaji'],
                'nominal' => $row['nominal']
            );

            $this->db->insert('hr_setup_potongan_gaji', $data_potongan_gaji);
        }

        $data_mutasi = array(
            'nik' => $nik,
            'kd_unit_kerja' => $kd_unit_kerja,
            'kd_jabatan' => $kd_jabatan,
            'kd_level' => $select_pangkat,
            'kd_cabang' => $kd_lokasi,
            'apr_by' => $apr_by,
            'apr_tanggal' => $apr_date,
            'tanggal_sk' => $tgl_sk,
            'no_sk' => $no_sk,
            'tanggal_mulai_berlaku' => $tgl_sk,
            'mulai_berlaku_sk' => $mulai_berlaku_sk,
            'akhir_berlaku_sk' => $akhir_berlaku_sk,
            'status' => 'T'
        );

        $data_pangkat = array(
            'nik' => $nik,
            'kd_unit_kerja' => $kd_unit_kerja,
            'kd_jabatan' => $kd_jabatan,
            'kd_level' => $select_pangkat,
            'kd_cabang' => $kd_lokasi,
            'apr_by' => $apr_by,
            'tanggal_sk' => $tgl_sk,
            'tanggal_mulai_berlaku' => $tgl_sk,
            'no_sk' => $no_sk,
            'apr_tanggal' => $apr_date,
            'mulai_berlaku_sk' => $mulai_berlaku_sk,
            'akhir_berlaku_sk' => $akhir_berlaku_sk,
            'status' => 'T'
        );


        $nilai = $select_pangkat * 10;

        $data_penilaian = array(
            'nik' => $nik,
            'keterangan' => 'Pegawai Baru',
            'aspek' => 'Pegawai Baru',
            'nilai' => $nilai
        );


        $data_setup_tunjangan = array(
            'nik' => $nik,
            'persentase_gaji' => '100',
            'nilai_kontrak' => '0',
            'status_keluarga' => $status_keluarga,
            'status_ptkp' => $ptkp,
            'kelompok_natura' => $k_natura
        );
        $data_user = array(
            'nik' => $nik,
            'user_name' => $nik,
            'user_password' => md5($nik),
            'grup_id' => $roles_id
        );
        $data_tunjangan_khusus = array(
            'nik' => $nik,
            'tunjangan_jabatan' => '1',
        );

        $data_user_moodle = array(
            'auth' => 'db',
            'confirmed' => '1',
            'mnethostid' => '1',
            'username' => $nik,
            'password' => 'not cached',
            'firstname' => $nama_pegawai,
            'lastname' => $nik,
            'email' => $email,
            'institution' => $kd_lokasi,
            'department' => $kd_jabatan,
            'city' => $kd_unit_kerja
        );

        $this->db->trans_begin();

        $this->db->insert('hr_pegawai', $data_pegawai); // INSERT KODE LOKASI PENEMPATAN
        $this->db->insert('hr_pegawai_detail', $data_detail); // INSERT ALAMAT KTP
        $this->db->insert('hr_setup_tunjangan_sejahtera', $data_setup_tunjangan);
        $this->db->insert('m_user', $data_user);
        $this->db->insert('hr_tunjangan_khusus', $data_tunjangan_khusus);

        if ((!empty($kd_unit_kerja)) || (!empty($kd_jabatan)) || (!empty($kd_lokasi)) || (!empty($lokasi_penempatan))) {
            $this->db->insert('hr_jabatan', $data_mutasi);
            //$this->db->insert_id();
            $this->db->insert('hr_pangkat', $data_pangkat);
        }

        $this->db->query("update hr_seleksi_pegawai set status_seleksi='Diperkerjakan' where no_seleksi='$noid_seleksi'");

        //=========Insert data pegawai ke database moodle
        $moodle_db = $this->load->database(ACTIVE_GROUP_DB . '_moodle', TRUE); /* database conection for moodle */
        //Insert data pegawai ke table  learn_user database moodle
        $moodle_db->insert('learn_user', $data_user_moodle);
        //===========================================================

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    //DIPAKE	
    function edit_employee_model() {
        $tipe = 'UPDATE';
        $apr_by = $this->session->userdata('nik_session');
        $apr_date = date("Y-m-d");
        $kd_roles = $this->session->userdata('kd_roles');
        $apr = "";
        if ($this->is_sdm) {
            $apr = "T";
        } else {
            $apr = "F";
        }

        $nama_pegawai = $this->input->post('nama');
        $nik = $this->input->post('txtnik_hide');
        $gelar_depan = $this->input->post('g_depan');
        $gelar_belakang_pegawai = $this->input->post('g_belakang');
        $tmp_lahir = $this->input->post('tmp_lahir');
        $gol_darah = $this->input->post('select_gol_darah');
        //$tgl_lahir = $this->input->post('tgl_lahir');
        if ($this->input->post('tgl_lahir') == '')
            $tgl_lahir = '01-01-1000';
        else
            $tgl_lahir = $this->input->post('tgl_lahir');
        $gender = $this->input->post('gender');
        $select_status_kawin_pegawai = $this->input->post('status_kawin');
        $agama_pagawai = $this->input->post('agama');
        $suku_marga_pegawai = $this->input->post('suku');
        $alamat_pegawai = $this->input->post('alamat');
        $rtrw_pegawai = $this->input->post('rtrw');
        $desa_kelurahan_pegawai = $this->input->post('kelurahan');
        $kecamatan = $this->input->post('kecamatan');
        $kab_or_kota_pegawai = $this->input->post('select_kota');
        $kode_pos = $this->input->post('kd_pos');
        $prov_pegawai = $this->input->post('select_propinsi');
        $telf_pegawai = $this->input->post('telpon');
        $hp_pegawai = $this->input->post('mobile');
        $st_rumah_dinas = $this->input->post('st_rumah_dinas');

        // added by dina
        $rtrw_ktp = $this->input->post('rtrw_ktp');
        $desa_kelurahan_ktp = $this->input->post('kelurahan_ktp');
        $kecamatan_ktp = $this->input->post('kecamatan_ktp');
        $kab_or_kota_ktp = $this->input->post('select_kota_ktp');
        $kode_pos_ktp = $this->input->post('kd_pos_ktp');
        $prov_ktp = $this->input->post('select_propinsi_ktp');
        $lokasi_penempatan2 = $this->input->post('lokasi_penempatan2');
        //echo $lokasi_penempatan2;  

        $email = $this->input->post('email');
        $j_pegawai = $this->input->post('status_kerja_pegawai');
        $status_pegawai = $this->input->post('status_aktif_pegawai');
        $kebangsaan = $this->input->post('kebangsaan');
        $alamat = $this->input->post('alamat');
        $alamat_ktp = $this->input->post('alamat_ktp');
        $tgl_capeg = $this->input->post('tgl_capeg');
        $tgl_masuk_pegawai = $this->input->post('tgl_masuk');
        $kd_unit_kerja = $this->input->post('select_unit_kerja');
        $kd_jabatan = $this->input->post('select_jabatan');
        $kd_lokasi = $this->input->post('select_cabang');

        $norek = $this->input->post('rekening');
        $npwp = $this->input->post('npwp');
        $no_ktp = $this->input->post('no_ktp');
        $no_jamsostek = $this->input->post('no_jamsostek');
        $no_sk = $this->input->post('no_sk');
        $tgl_sk = $this->input->post('tanggal_sk');
        $tanggal_sk = hgenerator::switch_tanggal($tgl_sk);
        $select_pangkat = $this->input->post('select_level');
        $status_keluarga = "";
        $ptkp = "";
        $k_natura = "";

        $lev_pegawai = $this->db->query("select kd_level from hr_pegawai where nik='$nik'");
        foreach ($lev_pegawai->result_array() as $row) {
            $kd_level = $row['kd_level'];
        }

        /*
          if ($tgl_lahir == '') {
          $tgl_lahir = '1900-01-01';
          } else {
          $tgl_lahir = explode("-", $tgl_lahir);
          $hari = $tgl_lahir[0];
          $bulan = $tgl_lahir[1];
          $tahun = $tgl_lahir[2];
          $tgl_lahir = "$tahun-$bulan-$hari";
          }
          if ($tgl_masuk_pegawai == '') {
          $tgl_masuk_pegawai = '1900-01-01';
          } else {
          $tgl_masuk_pegawai = explode("-", $tgl_masuk_pegawai);
          $hari = $tgl_masuk_pegawai[0];
          $bulan = $tgl_masuk_pegawai[1];
          $tahun = $tgl_masuk_pegawai[2];
          $tgl_masuk_pegawai = "$tahun-$bulan-$hari";
          }

          if ($tgl_capeg == '') {
          $tgl_capeg = '1900-01-01';
          } else {
          $tgl_capeg = explode("-", $tgl_capeg);
          $hari = $tgl_capeg[0];
          $bulan = $tgl_capeg[1];
          $tahun = $tgl_capeg[2];
          $tgl_capeg = "$tahun-$bulan-$hari";
          } */


        $tgl_lahir = hgenerator::switch_tanggal($tgl_lahir);
        $tgl_masuk_pegawai = hgenerator::switch_tanggal($tgl_masuk_pegawai);
        $tgl_capeg = hgenerator::switch_tanggal($tgl_capeg);

        $roles_id = $this->input->post('grup_roles');

//        switch ($gender) {
//            case 'P':
//                $gender = "Perempuan";
//                break;
//            case 'L':
//                $gender = "Laki-laki";
//                break;
//        }

        switch ($select_status_kawin_pegawai) {
            case 'K':
                $select_status_kawin_pegawai = "Kawin";
                break;
            case 'BK':
                $select_status_kawin_pegawai = "Belum Kawin";
                break;
            case 'D':
                $select_status_kawin_pegawai = "Duda";
                break;
            case 'J':
                $select_status_kawin_pegawai = "Janda";
                break;
        }

        if ($this->get_detail(array('nik' => $nik))->count_all_results() >= 2) {
            $foto_pegawai = $this->db->query("SELECT foto FROM hr_pegawai_detail WHERE nik='$nik' AND apr='T'");
        } else {
            $foto_pegawai = $this->db->query("SELECT foto FROM hr_pegawai_detail WHERE nik='$nik'");
        }
        foreach ($foto_pegawai->result_array() as $row) {
            $foto_peg = $row['foto'];
        }

        $data_pegawai = array(
            'nik' => $nik,
            'nama' => $nama_pegawai,
            'kd_level' => !empty($select_pangkat) ? $select_pangkat : null,
            'status_aktif' => 'Aktif',
            'tgl_capeg' => $tgl_capeg,
            'tgl_masuk' => $tgl_masuk_pegawai,
            'kd_unit_kerja' => !empty($kd_unit_kerja) ? $kd_unit_kerja : null,
            'kd_jabatan' => !empty($kd_jabatan) ? $kd_jabatan : null,
            'tanggal_sk' => $tgl_sk,
            'no_sk' => $no_sk,
            'status_kerja' => $j_pegawai,
            'kd_cabang' => !empty($kd_lokasi) ? $kd_lokasi : null,
            'apr' => 'T',
            'apr_by' => $apr_by,
            'apr_tanggal' => $apr_date,
            'kd_cabang_penempatan' => !empty($lokasi_penempatan2) ? $lokasi_penempatan2 : null,
            'status_kerja' => $j_pegawai
        );

        $data_detail = array(
            'nik' => $nik,
            'nama' => $nama_pegawai,
            'g_depan' => $gelar_depan,
            'g_belakang' => $gelar_belakang_pegawai,
            'tmp_lahir' => !empty($tmp_lahir) ? $tmp_lahir : null,
            'tgl_lahir' => $tgl_lahir,
            'gol_darah' => $gol_darah,
            'suku' => $suku_marga_pegawai,
            'agama' => $agama_pagawai,
            'status_kawin' => $select_status_kawin_pegawai,
            'kebangsaan' => $kebangsaan,
            'gender' => $gender,
            'foto' => $foto_peg,
            'alamat_ktp' => $alamat_ktp,
            'telpon' => $telf_pegawai,
            'email' => $email,
            'mobile' => $hp_pegawai,
            'nik_tmp' => $nik,
            'apr' => $apr,
            'rekening' => $norek,
            'npwp' => $npwp,
            'no_ktp' => $no_ktp,
            'no_jamsostek' => $no_jamsostek,
            'rtrw_ktp' => $rtrw_ktp,
            'kelurahan_ktp' => $desa_kelurahan_ktp,
            'kecamatan_ktp' => $kecamatan_ktp,
            'select_kota_ktp' => !empty($kab_or_kota_ktp) ? $kab_or_kota_ktp : null,
            'kd_pos_ktp' => $kode_pos_ktp,
            'select_propinsi_ktp' => !empty($prov_ktp) ? $prov_ktp : null,
            'st_rumah_dinas' => $st_rumah_dinas
        );

        //jika checkbox di klik
        $klik = $this->input->post('chkBoxb');
        if (empty($klik)) {
            $data_detail['alamat'] = $alamat;
            $data_detail['rtrw'] = $rtrw_pegawai;
            $data_detail['kelurahan'] = $desa_kelurahan_pegawai;
            $data_detail['kecamatan'] = $kecamatan;
            $data_detail['kota'] = !empty($kab_or_kota_pegawai) ? $kab_or_kota_pegawai : null;
            $data_detail['propinsi'] = !empty($prov_pegawai) ? $prov_pegawai : null;
            $data_detail['kd_pos'] = $kode_pos;
        } else {
            $data_detail['alamat'] = $alamat_ktp;
            $data_detail['rtrw'] = $rtrw_ktp;
            $data_detail['kelurahan'] = $desa_kelurahan_ktp;
            $data_detail['kecamatan'] = $kecamatan_ktp;
            $data_detail['kota'] = !empty($kab_or_kota_ktp) ? $kab_or_kota_ktp : null;
            $data_detail['propinsi'] = !empty($prov_ktp) ? $prov_ktp : null;
            $data_detail['kd_pos'] = $kode_pos_ktp;
        }

        if ($this->is_sdm <> '' && $this->is_sdm == 1) {
            $this->db->query("update 
        hr_pegawai
        set 
        nama='$nama_pegawai', 
        tgl_masuk='$tgl_masuk_pegawai',
        tgl_capeg='$tgl_capeg',
        status_aktif='Aktif',
        no_sk='$no_sk',
        tanggal_sk='$tanggal_sk',
        apr='$apr',
        apr_by='$apr_by',
        apr_tanggal='$apr_date',
		kd_cabang_penempatan='$lokasi_penempatan2',
		status_kerja = '$j_pegawai'
        where nik='$nik'");
        }

        $this->db->trans_begin();

        if ($this->is_sdm == '') {
            if ($this->get_detail(array('nik' => $nik, 'apr' => 'F'))->count_all_results()) {
                $this->db->update('hr_pegawai_detail', $data_detail, array('nik' => $nik, 'apr' => 'F'));
            } else {
                $this->db->insert('hr_pegawai_detail', $data_detail);
            }
            $this->db->update($this->table, array('apr' => 'F'), array('nik' => $nik));
        }

        if ($this->is_sdm <> '' && $this->is_sdm == 1) {

            if ($this->get_detail(array('nik' => $nik, 'apr' => 'F'))->count_all_results()) {
                $this->db->delete('hr_pegawai_detail', array('nik' => $nik, 'apr' => 'T'));
                $this->db->update('hr_pegawai_detail', $data_detail, array('nik' => $nik, 'apr' => 'F'));
            } else {
                $this->db->update('hr_pegawai_detail', $data_detail, array('nik' => $nik));
            }
            $this->db->update($this->table, array('apr' => 'T'), array('nik' => $nik, 'apr' => 'F'));
            //$this->db->update('hr_pegawai_detail', $data_detail);
            //$this->db->where('nik', $nik);
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function save_foto($data, $condition) {
        $this->db->update('hr_pegawai_detail', $data, $condition);
    }

    /*    public $limit = 0;
      private $pagesize = 10;
      public $offset = 0;

      function Master_pegawai_model() {
      parent::__construct();
      $this->load->model('model_tools/tools');
      } */

    /* function getComboJenjang() {
      $this->db->from('hr_ref_jenjang');
      $query = $this->db->get();
      $hasil = array('' => '-Semua Pendidikan-');
      if ($query->num_rows() > 0) {
      foreach ($query->result() as $data) {
      $hasil[$data->jenjang] = $data->jenjang;
      }
      }
      return $hasil;
      }

      function getComboTahun() {
      $tahun = date('Y');
      //$this->db->select('min(tgl_masuk) as tahunAwal');
      //$this->db->from("hr_pegawai");
      //$query = $this->db->get();
      $hasil = "";
      for ($idx = $tahun; $idx >= 1975; $idx--) {
      $hasil .= "<option>$idx</option>";
      }
      return $hasil;
      } */

    function load_data_from_seleksi() {
        $no_seleksi = $this->input->post('select_noid_seleksi');
        $query = $this->db->query("select 

    hr_seleksi_pegawai.nama,
    hr_seleksi_pegawai.tmp_lahir,
    to_char(tgl_lahir, 'dd-mm-yyyy') as tgl_lahir,  
    /*date_format(tgl_lahir, '%d-%m-%y') as tgl_lahir, */
    hr_seleksi_pegawai.gender,
    hr_seleksi_pegawai.gol_darah,
    hr_seleksi_pegawai.agama,    
    hr_seleksi_pegawai.status_kawin,    
    hr_seleksi_pegawai.foto,    
    hr_seleksi_pegawai.alamat,    
    hr_seleksi_pegawai.kota,  
    hr_seleksi_pegawai.kd_pos,      
    hr_seleksi_pegawai.telpon,  
    hr_seleksi_pegawai.email,  
    hr_seleksi_pegawai.mobile,


    hr_seleksi_pegawai.ayah_nama,    
    hr_seleksi_pegawai.ayah_alamat,  
    hr_seleksi_pegawai.ayah_pekerjaan,      
    hr_seleksi_pegawai.ayah_pendidikan,       

    hr_seleksi_pegawai.ibu_nama,    
    hr_seleksi_pegawai.ibu_alamat,  
    hr_seleksi_pegawai.ibu_pekerjaan,      
    hr_seleksi_pegawai.ibu_pendidikan,   
    
    hr_seleksi_pegawai.tgl_seleksi,    
    hr_seleksi_pegawai.gaji_yang_diminta,  
    hr_seleksi_pegawai.status_seleksi,      
    hr_seleksi_pegawai.kualifikasi_pddk,   
    
    hr_seleksi_pegawai.jurusan,    
    hr_seleksi_pegawai.tahun_lulus,  
    hr_seleksi_pegawai.nama_sekolah,      
    hr_seleksi_pegawai.nilai,

    hr_seleksi_pegawai.add_tanggal,
    hr_seleksi_pegawai.no_seleksi
    from hr_seleksi_pegawai
    where no_seleksi  ='$no_seleksi'    
    ");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    function load_data_from_seleksi_approved($no_seleksi) {
        $query = $this->db->query("select 

    hr_seleksi_pegawai.nama,
    hr_seleksi_pegawai.tmp_lahir,
    to_char(tgl_lahir, 'dd-mm-yyyy') as tgl_lahir,  
    /*date_format(tgl_lahir, '%d-%m-%y') as tgl_lahir, */
    hr_seleksi_pegawai.gender,
    hr_seleksi_pegawai.gol_darah,
    hr_seleksi_pegawai.agama,    
    hr_seleksi_pegawai.status_kawin,    
    hr_seleksi_pegawai.foto,    
    hr_seleksi_pegawai.alamat,    
    hr_seleksi_pegawai.kota,  
    hr_seleksi_pegawai.kd_pos,      
    hr_seleksi_pegawai.telpon,  
    hr_seleksi_pegawai.email,  
    hr_seleksi_pegawai.mobile,


    hr_seleksi_pegawai.ayah_nama,    
    hr_seleksi_pegawai.ayah_alamat,  
    hr_seleksi_pegawai.ayah_pekerjaan,      
    hr_seleksi_pegawai.ayah_pendidikan,       

    hr_seleksi_pegawai.ibu_nama,    
    hr_seleksi_pegawai.ibu_alamat,  
    hr_seleksi_pegawai.ibu_pekerjaan,      
    hr_seleksi_pegawai.ibu_pendidikan,   
    
    hr_seleksi_pegawai.tgl_seleksi,    
    hr_seleksi_pegawai.gaji_yang_diminta,  
    hr_seleksi_pegawai.status_seleksi,      
    hr_seleksi_pegawai.kualifikasi_pddk,   
    
    hr_seleksi_pegawai.jurusan,    
    hr_seleksi_pegawai.tahun_lulus,  
    hr_seleksi_pegawai.nama_sekolah,      
    hr_seleksi_pegawai.nilai,

    hr_seleksi_pegawai.add_tanggal,
    hr_seleksi_pegawai.no_seleksi
    from hr_seleksi_pegawai
    where no_seleksi  ='$no_seleksi'    
    ");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    function load_no_seleksi_mdl($nik_perm = '') {
        $kd_jabatan = "";
        $kd_cabang = "";
        $nik_kadiv = $this->session->userdata('nik_session');
        if ($nik_perm == '') {
            $nik_perm = 0;
        }
        $query = $this->db->query("select *,hp.* from hr_seleksi_pegawai hs left join 
            hr_permintaan_pegawai hp on hs.permintaan_id=hp.permintaan_pegawai_id
            left join v_nilai_seleksi as v on hs.seleksi_pegawai_id=v.seleksi_pegawai_id
            left join hr_penempatan hpm on hs.seleksi_pegawai_id!=hpm.seleksi_pegawai_id
            where hs.status_seleksi='Lulus' and hp.permintaan_pegawai_id='$nik_perm' and hp.nik='$nik_kadiv' and hp.sisa_formasi > 0");
        foreach ($query->result_array() as $row) {
            $kd_jabatan = $row['kd_jabatan'];
            $kd_cabang = $row['kd_cabang'];
            $kd_unit_kerja = $row['kd_unit_kerja'];
        }
        if ($nik_perm == '') {
            $filter = "and hs.kd_jabatan='null' and hp.kd_cabang='null'";
        } else {
            $filter = " and hs.kd_jabatan='$kd_jabatan' ";
        }
        $q = $this->db->query("select * from hr_seleksi_pegawai hs left join 
            hr_permintaan_pegawai hp on hs.permintaan_id=hp.permintaan_pegawai_id
            left join v_nilai_seleksi as v on hs.seleksi_pegawai_id=v.seleksi_pegawai_id
            where hs.status_seleksi='Lulus' and hp.permintaan_pegawai_id='$nik_perm' " . $filter . " and hp.nik='$nik_kadiv' and hp.sisa_formasi > 0");
        return $q;
    }

    function load_keahlian($nik) {
        $query = $this->db->query("select  mpeg_keahlian,nama_keahlian,tingkat_kemampuan from  hr_mpeg_keahlian where nik='$nik'");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    function load_penilaian($nik) {
        $query = $this->db->query("select  kd_penilaian,keterangan,aspek,nilai from    hr_penilaian where nik='$nik'");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    function load_permitaan() {
        $query = $this->db->query("select
        hr_permintaan_pegawai.permintaan_pegawai_id,     
        hr_permintaan_pegawai.nik,
        hr_pegawai.nama,
        hr_permintaan_pegawai.kd_unit_kerja,
        hr_permintaan_pegawai.kd_cabang,
        hr_permintaan_pegawai.kd_jabatan ,     
        hr_ref_jabatan.nama_jabatan, 
        hr_ref_unit_kerja.nama_unit_kerja, 
        hr_ref_cabang.nama_cabang,
        hr_ref_jabatan.kd_jabatan,
        hr_ref_unit_kerja.kd_unit_kerja,
        hr_ref_cabang.kd_cabang
        from hr_permintaan_pegawai
        LEFT JOIN hr_ref_jabatan
        ON hr_permintaan_pegawai.kd_jabatan=hr_ref_jabatan.kd_jabatan
        LEFT JOIN hr_pegawai
        ON hr_permintaan_pegawai.nik=hr_pegawai.nik
        LEFT JOIN hr_ref_unit_kerja
        ON hr_permintaan_pegawai.kd_unit_kerja=hr_ref_unit_kerja.kd_unit_kerja
        LEFT JOIN hr_ref_cabang
        ON hr_permintaan_pegawai.kd_cabang=hr_ref_cabang.kd_cabang 
        LEFT JOIN hr_seleksi_pegawai
        ON hr_seleksi_pegawai.permintaan_id=hr_permintaan_pegawai.permintaan_pegawai_id
        where hr_permintaan_pegawai.status='F' and hr_permintaan_pegawai.sisa_formasi > 0");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    function load_permintaan_pegawai() {
        $nik = $this->input->post('select_nik_perm');
        $query = $this->db->query("select nik,
        hr_permintaan_pegawai.permintaan_pegawai_id, 
        hr_permintaan_pegawai.kd_unit_kerja,
        hr_permintaan_pegawai.kd_cabang,
        hr_permintaan_pegawai.kd_jabatan ,     
        hr_ref_jabatan.nama_jabatan, 
        hr_ref_unit_kerja.nama_unit_kerja, 
        hr_ref_cabang.nama_cabang,
        hr_ref_jabatan.kd_jabatan,
        hr_ref_unit_kerja.kd_unit_kerja,
        hr_ref_cabang.kd_cabang
        from hr_permintaan_pegawai
        LEFT JOIN hr_ref_jabatan
        ON hr_permintaan_pegawai.kd_jabatan=hr_ref_jabatan.kd_jabatan
        LEFT JOIN hr_ref_unit_kerja
        ON hr_permintaan_pegawai.kd_unit_kerja=hr_ref_unit_kerja.kd_unit_kerja
        LEFT JOIN hr_ref_cabang
        ON hr_permintaan_pegawai.kd_cabang=hr_ref_cabang.kd_cabang 
        where hr_permintaan_pegawai.permintaan_pegawai_id='$nik'");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    function load_permintaan_pegawai_approved($permintaan_pegawai_id = 0) {
        $nik = $this->input->post('select_nik_perm');
        $query = $this->db->query("select nik,
        hr_permintaan_pegawai.permintaan_pegawai_id, 
        hr_permintaan_pegawai.kd_unit_kerja,
        hr_permintaan_pegawai.kd_cabang,
        hr_permintaan_pegawai.kd_jabatan ,     
        hr_ref_jabatan.nama_jabatan, 
        hr_ref_unit_kerja.nama_unit_kerja, 
        hr_ref_cabang.nama_cabang,
        hr_ref_jabatan.kd_jabatan,
        hr_ref_unit_kerja.kd_unit_kerja,
        hr_ref_cabang.kd_cabang
        from hr_permintaan_pegawai
        LEFT JOIN hr_ref_jabatan
        ON hr_permintaan_pegawai.kd_jabatan=hr_ref_jabatan.kd_jabatan
        LEFT JOIN hr_ref_unit_kerja
        ON hr_permintaan_pegawai.kd_unit_kerja=hr_ref_unit_kerja.kd_unit_kerja
        LEFT JOIN hr_ref_cabang
        ON hr_permintaan_pegawai.kd_cabang=hr_ref_cabang.kd_cabang 
        where hr_permintaan_pegawai.permintaan_pegawai_id='$permintaan_pegawai_id'");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    function add_anak_by_seleksi($data_anak) {

        $this->db->insert('hr_mpeg_anak', $data_anak);
    }

    function add_anak_mdl() {
        $nik = $_POST['nik'];
        $nama = $_POST['nama_anak'];
        $tmp_lahir = $_POST['tmp_lahir'];
        $tgl_lahir = $_POST['tgl_lahir'];
        $gender = $_POST['gender_anak'];
        $akte = $_POST['akte_lahir'];
        $status_nikah = $_POST['status_anak'];
        $pendidikan = $_POST['pendidikan_anak'];
        $keterangan = $_POST['keterangan_anak'];
        if ($gender == 'Perempuan') {
            $gender = 'P';
        }
        if ($gender == 'Laki-laki') {
            $gender = 'L';
        }
        if ($status_nikah == 'BELUM KAWIN') {
            $status_nikah = 'BK';
        }
        if ($status_nikah == 'KAWIN') {
            $status_nikah = 'K';
        }
        $tgl_lahir = date("Y-m-d", strtotime($tgl_lahir));
        $data_anak = array('nik' => $nik,
            'nama' => $nama,
            'status_anak' => $status_nikah,
            'tmp_lahir' => $tmp_lahir,
            'tgl_lahir' => $tgl_lahir,
            'no_akte_lahir' => $akte,
            'gender' => $gender,
            'pendidikan' => $pendidikan,
            'status_nikah' => $status_nikah,
            'status_pendidikan' => $pendidikan,
            'keterangan' => $keterangan,
            'apr' => 'T'
        );
        $this->db->insert('hr_mpeg_anak', $data_anak);
        $this->db->query("delete from hr_mpeg_anak where nama='' and nik='$nik'");
    }

    function add_anak_mdl_from_edit() {

        $nik = $_POST['txtnik_hide'];
        $nama = $_POST['txtnama_anak2'];
        $tmp_lahir = $_POST['txttempat_lahir_anak2'];
        $tgl_lahir = $_POST['tgl_lahir_anak2'];
        $gender = $_POST['select_gender_anak_2'];
        $akte = $_POST['txt_akte_kelahiran_anak2'];
        $status_nikah = $_POST['select_status_anak_2'];
        $pendidikan = $_POST['select_pendidikan_anak2'];
        $keterangan = $_POST['txtketerangan_anak2'];
        $kd_roles = $this->session->userdata('kd_roles');
        if ($this->is_sdm) {
            $apr = 'T';
        } else {
            $apr = 'A';
        }
        if ($gender == 'Perempuan') {
            $gender = 'P';
        }
        if ($gender == 'Laki-laki') {
            $gender = 'L';
        }
        if ($status_nikah == 'BELUM KAWIN') {
            $status_nikah = 'BK';
        }
        if ($status_nikah == 'KAWIN') {
            $status_nikah = 'K';
        }
        $tgl_lahir = date("Y-m-d", strtotime($tgl_lahir));
        $data_anak = array('nik' => $nik,
            'nama' => $nama,
            'status_anak' => $status_nikah,
            'tmp_lahir' => $tmp_lahir,
            'tgl_lahir' => $tgl_lahir,
            'no_akte_lahir' => $akte,
            'gender' => $gender,
            'pendidikan' => $pendidikan,
            'status_nikah' => $status_nikah,
            'status_pendidikan' => $pendidikan,
            'keterangan' => $keterangan,
            'apr' => $apr
        );
        if ($nama != '') {
            $this->db->insert('hr_mpeg_anak', $data_anak);
            $this->db->query("delete from hr_mpeg_anak where nama='' and nik='$nik'");
        }
    }

    function update_anak_mdl() {
        $nik = $_POST['txtnik_hide'];
        $nama = $this->input->post('txtnama_anak2');
        $tmp_lahir = $_POST['txttempat_lahir_anak2'];
        $tgl_lahir = $_POST['tgl_lahir_anak2'];
        $gender = $_POST['select_gender_anak_2'];
        $akte = $_POST['txt_akte_kelahiran_anak2'];
        $status_nikah = $_POST['select_status_anak_2'];
        $pendidikan = $_POST['select_pendidikan_anak2'];
        $keterangan = $_POST['txtketerangan_anak2'];
        $noid = $_POST['noid_anak'];
        $tgl_lahir = date("Y-m-d", strtotime($tgl_lahir));
        $kd_roles = $this->session->userdata('kd_roles');
        if ($gender == 'Perempuan') {
            $gender = 'P';
        }
        if ($gender == 'Laki-laki') {
            $gender = 'L';
        }
        if ($status_nikah == 'Kawin') {
            $status_nikah = 'K';
        }
        if ($status_nikah == 'Belum Kawin') {
            $status_nikah = 'BK';
        }
        if ($this->is_sdm) {
            $apr = 'T';
        } else {
            $apr = 'F';
        }
        $data_anak = array('nik' => $nik,
            'nama' => $nama,
            'status_anak' => $status_nikah,
            'tmp_lahir' => $tmp_lahir,
            'gender' => $gender,
            'no_akte_lahir' => $akte,
            'tgl_lahir' => $tgl_lahir,
            'pendidikan' => $pendidikan,
            'status_nikah' => $status_nikah,
            'status_pendidikan' => $pendidikan,
            'keterangan' => $keterangan,
            'apr' => $apr,
            'id_edit' => $noid
        );
        if ($this->is_sdm) {

            $this->db->where('mpeg_anak_id', $noid);
            $this->db->update('hr_mpeg_anak', $data_anak);
        } else {
            $this->db->insert('hr_mpeg_anak', $data_anak);
        }
    }

    function delete_anak_mdl() {
        $nik = $_POST['txtnik_hide'];
        $noid = $_POST['noid_anak'];
        $apr = "";
        $kd_roles = $this->session->userdata('kd_roles');
        if ($this->is_sdm) {
            $apr = 'T';
        } else {
            $apr = 'D';
        }
        $data_anak = array(
            'nik' => $nik,
            'apr' => $apr,
            'id_edit' => $noid);


        if ($this->is_sdm) {

            $this->db->query("delete from  	hr_mpeg_anak where mpeg_anak_id='$noid'");
        } else {
            $this->db->insert('hr_mpeg_anak', $data_anak);
            $this->db->query("update hr_pegawai set apr='F' where nik='$nik'");
        }
    }

    function add_pendidikan_mdl() {
        $nik = $this->input->post('nik');
        $nama_sekolah = $this->input->post('nama_sekolah');
        $jenjang = $this->input->post('jenjang');
        $jurusan = $this->input->post('jurusan');
        $tahun_masuk = $this->input->post('masuk_sekolah');
        $tahun_keluar = $this->input->post('keluar_sekolah');
        $nilai = $this->input->post('nilai');
        $no_ijasah = $this->input->post('no_ijasah');
        $jurusan = $this->input->post('jurusan');
        $gelar = $this->input->post('gelar');
        $jenjang = $this->input->post('jenjang');
        $apr = $this->session->userdata('kd_roles');
        $tgl_ijasah = date('Y-m-d', strtotime($this->input->post('tglijasah')));
        $kd_roles = $this->session->userdata('kd_roles');
        if ($this->is_sdm) {
            $apr = 'T';
        } else {
            $apr = 'A';
        }
        if ($nilai == '') {
            $nilai = 0;
        }
        if ($nilai == ' ') {
            $nilai = 0;
        }
        $data_pendidikan = array(
            'nik' => $nik,
            'nama_sekolah' => $nama_sekolah,
            'jenjang' => $jenjang,
            'jurusan' => $jurusan,
            'tanggal_ijazah' => $tgl_ijasah,
            'tahun_masuk' => $tahun_masuk,
            'tahun_keluar' => $tahun_keluar,
            'nilai' => $nilai,
            'no_ijazah' => $no_ijasah,
            'gelar' => $gelar,
            'jurusan' => $jurusan,
            'apr' => $apr
        );
        $this->db->insert('hr_mpeg_sekolah', $data_pendidikan);
        $this->db->query("delete from  hr_mpeg_sekolah where nama_sekolah='' and nik='$nik'");
    }

    function add_pendidikan_by_seleksi($data_pendidikan) {
        $this->db->insert('hr_mpeg_sekolah', $data_pendidikan);
        //$this->db->query("delete from  hr_mpeg_sekolah where nama_sekolah='' and nik='$nik'");
    }

    function add_pendidikan_mdl_edit() {
        $nik = $this->input->post('nik');
        $nama_sekolah = $this->input->post('nama_sekolah');
        $jenjang = $this->input->post('jenjang');
        $jurusan = $this->input->post('jurusan_sekolah');
        $tahun_masuk = $this->input->post('masuk_sekolah');
        $tahun_keluar = $this->input->post('keluar_sekolah');
        $nilai = $this->input->post('nilai');
        $no_ijasah = $this->input->post('no_ijasah');
        $gelar = $this->input->post('gelar');
        $jenjang = $this->input->post('jenjang');
        $apr = $this->session->userdata('kd_roles');
        $tgl_ijasah = date('Y-m-d', strtotime($this->input->post('tgl_ijasah')));
        $stts = "";
        if ($nilai == '') {
            $nilai = 0;
        }
        if ($nilai == ' ') {
            $nilai = 0;
        }
        if ($apr == 4) {
            $stts = 'T';
        } else {
            $stts = 'F';
        }
        $data_pendidikan = array(
            'nik' => $nik,
            'nama_sekolah' => $nama_sekolah,
            'jenjang' => $jenjang,
            'jurusan' => $jurusan,
            'tanggal_ijazah' => $tgl_ijasah,
            'tahun_masuk' => $tahun_masuk,
            'tahun_keluar' => $tahun_keluar,
            'nilai' => $nilai,
            'no_ijazah' => $no_ijasah,
            'gelar' => $gelar,
            'apr' => $stts
        );
        if ($nama_sekolah != '') {
            $this->db->insert('hr_mpeg_sekolah', $data_pendidikan);
            $this->db->query("delete from  hr_mpeg_sekolah where nama_sekolah='' and nik='$nik'");
        }
    }

    function add_kompetensi_pendidikan_mdl() {
        $nik = $this->input->post('nik');
        $jurusan = $this->input->post('jurusan');
        $jenjang = $this->input->post('jenjang');
        $kompetensi = $jenjang . "-" . $jurusan;
        if ($jenjang == 'SMA') {
            $data_kompetensi = array(
                'nik' => $nik,
                'jenis_kompetensi' => '1',
                'kompetensi' => $kompetensi,
                'id_kompetensi' => '1',
            );
            $this->db->insert('hr_kompetensi_pegawai', $data_kompetensi);
        }
        if ($jenjang == 'D1') {
            $data_kompetensi = array(
                'nik' => $nik,
                'jenis_kompetensi' => '1',
                'kompetensi' => $kompetensi,
                'id_kompetensi' => '1',
            );
            $this->db->insert('hr_kompetensi_pegawai', $data_kompetensi);
        }
        if ($jenjang == 'D2') {
            $data_kompetensi = array(
                'nik' => $nik,
                'jenis_kompetensi' => '1',
                'kompetensi' => $kompetensi,
                'id_kompetensi' => '1',
            );
            $this->db->insert('hr_kompetensi_pegawai', $data_kompetensi);
        }
        if ($jenjang == 'D3') {
            $data_kompetensi = array(
                'nik' => $nik,
                'jenis_kompetensi' => '1',
                'kompetensi' => $kompetensi,
                'id_kompetensi' => '1',
            );
            $this->db->insert('hr_kompetensi_pegawai', $data_kompetensi);
        }
        if ($jenjang == 'S1') {
            $data_kompetensi = array(
                'nik' => $nik,
                'jenis_kompetensi' => '1',
                'kompetensi' => $kompetensi,
                'id_kompetensi' => '1',
            );
            $this->db->insert('hr_kompetensi_pegawai', $data_kompetensi);
        }
        if ($jenjang == 'S2') {
            $data_kompetensi = array(
                'nik' => $nik,
                'jenis_kompetensi' => '1',
                'kompetensi' => $kompetensi,
                'id_kompetensi' => '1',
            );
            $this->db->insert('hr_kompetensi_pegawai', $data_kompetensi);
        }
        if ($jenjang == 'S3') {
            $data_kompetensi = array(
                'nik' => $nik,
                'jenis_kompetensi' => '1',
                'kompetensi' => $kompetensi,
                'id_kompetensi' => '1',
            );
            $this->db->insert('hr_kompetensi_pegawai', $data_kompetensi);
        }
    }

    function add_kompetensi_pelatihan() {
        $nik = $this->input->post('nik');
        $id = $this->input->post('id');
        $nm_kompetensi = $this->input->post('nama_kompetensi');
        $kompetensi = $this->input->post('j_kompetensi');

        $data_kompetensi = array(
            'nik' => $nik,
            'jenis_kompetensi' => $kompetensi,
            'kompetensi' => $nm_kompetensi,
            'id_kompetensi' => $kompetensi,
            'apr' => 'F'
        );
        $this->db->insert('hr_kompetensi_pegawai', $data_kompetensi);
        $this->db->query("update hr_pegawai set apr='F' where nik='$nik'");
    }

    function update_kompetensi_pelatihan() {
        $nik = $this->input->post('nik');
        $id = $this->input->post('id');
        $nm_kompetensi = $this->input->post('nama_kompetensi');
        $kompetensi = $this->input->post('j_kompetensi');
        $data_kompetensi = array(
            'nik' => $nik,
            'jenis_kompetensi' => $kompetensi,
            'kompetensi' => $nm_kompetensi,
            'id_kompetensi' => $kompetensi,
            'apr' => 'F',
            'id_edit' => $id
        );
        $this->db->insert('hr_kompetensi_pegawai', $data_kompetensi);
        $this->db->query("update hr_pegawai set apr='F' where nik='$nik'");
    }

    function delete_kompetensi() {
        $nik = $this->input->post('nik');
        $id = $this->input->post('id');
        $nm_kompetensi = $this->input->post('nama_kompetensi');
        $kompetensi = $this->input->post('j_kompetensi');
        $data_kompetensi = array(
            'nik' => $nik,
            'apr' => 'D',
            'id_edit' => $id
        );
        $this->db->insert('hr_kompetensi_pegawai', $data_kompetensi);
        $this->db->query("update hr_pegawai set apr='F' where nik='$nik'");
    }

    function update_pendidikan() {
        $nik = $this->input->post('nik');
        $nama_sekolah = $this->input->post('nama_sekolah');
        $jenjang = $this->input->post('jenjang');
        $jurusan = $this->input->post('jurusan_sekolah');
        $tahun_masuk = $this->input->post('masuk_sekolah');
        $tahun_keluar = $this->input->post('keluar_sekolah');
        $nilai = $this->input->post('nilai');
        $no_ijasah = $this->input->post('no_ijasah');
        $tgl_ijasah = date('Y-m-d', strtotime($this->input->post('tgl_ijasah')));
        $gelar = $this->input->post('gelar');
        $noid_pendidikan = $this->input->post('noid_pendidikan');
        $kd_roles = $this->session->userdata('kd_roles');
        if ($this->is_sdm) {
            $apr = "T";
        } else {
            $apr = 'F';
        }
        if ($nilai == '') {
            $nilai = 0;
        }
        $data_pendidikan = array(
            'nik' => $nik,
            'nama_sekolah' => $nama_sekolah,
            'jenjang' => $jenjang,
            'tahun_masuk' => $tahun_masuk,
            'tahun_keluar' => $tahun_keluar,
            'nilai' => $nilai,
            'jurusan' => $jurusan,
            'tanggal_ijazah' => $tgl_ijasah,
            'no_ijazah' => $no_ijasah,
            'gelar' => $gelar,
            'apr' => $apr,
            'id_edit' => $noid_pendidikan,
        );




        if ($this->is_sdm) {

            $this->db->where('mpeg_sekolah_id', $noid_pendidikan);
            $this->db->update('hr_mpeg_sekolah', $data_pendidikan);
        } else {
            $this->db->insert('hr_mpeg_sekolah', $data_pendidikan);
            $this->db->query("update hr_pegawai set apr='F' where nik='$nik'");
        }
    }

    function delete_pendidikan_mdl() {
        $nik = $this->input->post('nik');
        $noid_pendidikan = $this->input->post('noid_pendidikan');
        $kd_roles = $this->session->userdata('kd_roles');
        $data_pendidikan = array('nik' => $nik,
            'apr' => 'D',
            'id_edit' => $noid_pendidikan);

        if ($this->is_sdm) {
            $this->db->query("delete from  	hr_mpeg_sekolah where mpeg_sekolah_id='$noid_pendidikan'");
            $this->db->query("update hr_pegawai set apr='F' where nik='$nik'");
        } else {
            $this->db->insert('hr_mpeg_sekolah', $data_pendidikan);
            $this->db->query("update hr_pegawai set apr='F' where nik='$nik'");
        }
    }

    function add_kursus_mdl() {
        $nik = $this->input->post('nik');
        $nama_kursus = $this->input->post('nama_kursus');
        $alamat_kursus = $this->input->post('alamat_kursus');
        $masuk_kursus = $this->input->post('masuk_kursus');
        $keluar_kursus = $this->input->post('keluar_kursus');
        $lembaga = $this->input->post('lembaga');
        $no_ijasah_kursus = $this->input->post('no_ijasah_kursus');
        $tgl_sertifikat = $this->input->post('tgl_sertifikat');
        $apr = $this->session->userdata('kd_roles');
        $masuk_kursus = date("Y-m-d", strtotime($masuk_kursus));
        $keluar_kursus = date("Y-m-d", strtotime($keluar_kursus));
        $tgl_sertifikat = date("Y-m-d", strtotime($tgl_sertifikat));

        $data_kursus = array(
            'nik' => $nik,
            'nama_kursus' => $nama_kursus,
            'lembaga' => $lembaga,
            'alamat' => $alamat_kursus,
            'no_sertifikat' => $no_ijasah_kursus,
            'tanggal_sertifikat' => $tgl_sertifikat,
            'tanggal_masuk' => $masuk_kursus,
            'tanggal_keluar' => $keluar_kursus,
            'apr' => 'T'
        );
        $this->db->insert('hr_mpeg_kursus', $data_kursus);
        $this->db->query("delete from hr_mpeg_kursus where nama_kursus=''");
    }

    function add_kursus_by_seleksi($data_kursus) {
        $this->db->insert('hr_mpeg_kursus', $data_kursus);
    }

    function add_kursus_mdl_edit() {

        $nik = $this->input->post('nik');
        $nama_kursus = $this->input->post('nama_kursus');
        $alamat_kursus = $this->input->post('alamat_kursus');
        $masuk_kursus = $this->input->post('masuk_kursus');
        $keluar_kursus = $this->input->post('keluar_kursus');
        $lembaga = $this->input->post('lembaga');
        $no_ijasah_kursus = $this->input->post('no_ijasah_kursus');
        $tgl_sertifikat = $this->input->post('tgl_sertifikat');
        $kd_roles = $this->session->userdata('kd_roles');
        $apr = "";
        if ($this->is_sdm) {
            $apr = "T";
        } else {
            $apr = 'A';
        }
        $masuk_kursus = date("Y-m-d", strtotime($masuk_kursus));
        $keluar_kursus = date("Y-m-d", strtotime($keluar_kursus));
        $tgl_sertifikat = date("Y-m-d", strtotime($tgl_sertifikat));
        $data_kursus = array(
            'nik' => $nik,
            'nama_kursus' => $nama_kursus,
            'lembaga' => $lembaga,
            'alamat' => $alamat_kursus,
            'no_sertifikat' => $no_ijasah_kursus,
            'tanggal_sertifikat' => $tgl_sertifikat,
            'tanggal_masuk' => $masuk_kursus,
            'tanggal_keluar' => $keluar_kursus,
            'apr' => $apr
        );
        if ($nama_kursus != '') {
            $this->db->insert('hr_mpeg_kursus', $data_kursus);
            $this->db->query("delete from hr_mpeg_kursus where nama_kursus=''");
        }
    }

    function update_kursus_mdl() {
        $nik = $this->input->post('nik');
        $nama_kursus = $this->input->post('nama_kursus');
        $alamat_kursus = $this->input->post('alamat_kursus');
        $masuk_kursus = $this->input->post('masuk_kursus');
        $keluar_kursus = $this->input->post('keluar_kursus');
        $lembaga = $this->input->post('lembaga');
        $no_ijasah_kursus = $this->input->post('no_ijasah_kursus');
        $tgl_sertifikat = $this->input->post('tgl_sertifikat');
        $noid = $this->input->post('noid_kursus');
        $apr = $this->session->userdata('kd_roles');
        $masuk_kursus = date("Y-m-d", strtotime($masuk_kursus));
        $keluar_kursus = date("Y-m-d", strtotime($keluar_kursus));
        $tgl_sertifikat = date("Y-m-d", strtotime($tgl_sertifikat));
        $kd_roles = $this->session->userdata('kd_roles');
        $apr = "";
        if ($this->is_sdm) {
            $apr = "T";
        } else {
            $apr = 'F';
        }
        $data_kursus = array(
            'nik' => $nik,
            'nama_kursus' => $nama_kursus,
            'lembaga' => $lembaga,
            'alamat' => $alamat_kursus,
            'no_sertifikat' => $no_ijasah_kursus,
            'tanggal_sertifikat' => $tgl_sertifikat,
            'tanggal_masuk' => $masuk_kursus,
            'tanggal_keluar' => $keluar_kursus,
            'apr' => $apr,
            'id_edit' => $noid,
        );
        if ($this->is_sdm) {

            $this->db->where('mpeg_khusus_id', $noid);
            $this->db->update('hr_mpeg_kursus', $data_kursus);
            echo"Asu";
        } else {
            $this->db->insert('hr_mpeg_kursus', $data_kursus);
            $this->db->query("update hr_pegawai set apr='F' where nik='$nik'");
        }
    }

    function delete_kursus_mdl() {
        $nik = $this->input->post('nik');
        $noid = $this->input->post('noid_kursus');
        $kd_roles = $this->session->userdata('kd_roles');
        $data_kursus = array(
            'nik' => $nik,
            'apr' => 'D',
            'id_edit' => $noid
        );

        if ($this->is_sdm) {
            $this->db->query("delete from  	hr_mpeg_kursus where mpeg_khusus_id='$noid'");
        } else {
            $this->db->insert('hr_mpeg_kursus', $data_kursus);
            $this->db->query("update hr_pegawai set apr='F' where nik='$nik'");
        }
    }

    function add_karir_mdl() {
        $nik = $this->input->post('nik');
        $nama_perusahaan_jabatan = $this->input->post('nama_perusahaan_jabatan');
        $nama_pekerjaan = $this->input->post('nama_pekerjaan');
        $jabatan_terakhir_jabatan = $this->input->post('jabatan_terakhir_jabatan');
        $masuk_jabatan = $this->input->post('masuk_jabatan');
        $keluar_jabatan = $this->input->post('keluar_jabatan');
        $gaji_terakhir_jabatan = $this->input->post('gaji_terakhir_jabatan');
        $alasan_keluar_jabatan = $this->input->post('alasan_keluar_jabatan');
        $apr = $this->session->userdata('kd_roles');
        $masuk_jabatan = date('Y-m-d', strtotime($masuk_jabatan));
        $keluar_jabatan = date('Y-m-d', strtotime($keluar_jabatan));
        $kd_roles = $this->session->userdata('kd_roles');
        if ($gaji_terakhir_jabatan == '') {
            $gaji_terakhir_jabatan = 0;
        }
        if ($gaji_terakhir_jabatan == ' ') {
            $gaji_terakhir_jabatan = 0;
        }
        if ($this->is_sdm) {
            $apr = "T";
        } else {
            $apr = 'A';
        }
        $data_karir = array(
            'nik' => $nik,
            'nama_perusahaan' => $nama_perusahaan_jabatan,
            'nama_pekerjaan' => $nama_pekerjaan,
            'jabatan_terakhir' => $jabatan_terakhir_jabatan,
            'tanggal_masuk' => $masuk_jabatan,
            'tanggal_keluar' => $keluar_jabatan,
            'gaji' => $gaji_terakhir_jabatan,
            'alasan_keluar' => $alasan_keluar_jabatan,
            'apr' => $apr
        );
        if ($nama_perusahaan_jabatan != '') {
            $this->db->insert('hr_mpeg_karir', $data_karir);
            $this->db->query("delete from hr_mpeg_karir where nama_perusahaan='' and nik='$nik'");
        }
    }

    function add_karir_by_seleksi($data_karir) {
        $this->db->insert('hr_mpeg_karir', $data_karir);
    }

    function add_karir_mdl_edit() {
        $nik = $this->input->post('nik');
        $nama_perusahaan_jabatan = $this->input->post('nama_perusahaan_jabatan');
        $nama_pekerjaan = $this->input->post('nama_pekerjaan');
        $jabatan_terakhir_jabatan = $this->input->post('jabatan_terakhir_jabatan');
        $masuk_jabatan = $this->input->post('masuk_jabatan');
        $keluar_jabatan = $this->input->post('keluar_jabatan');
        $gaji_terakhir_jabatan = $this->input->post('gaji_terakhir_jabatan');
        $alasan_keluar_jabatan = $this->input->post('alasan_keluar_jabatan');


        $masuk_jabatan = date('Y-m-d', strtotime($masuk_jabatan));
        $keluar_jabatan = date('Y-m-d', strtotime($keluar_jabatan));
        if ($gaji_terakhir_jabatan == '') {
            $gaji_terakhir_jabatan = 0;
        }
        $kd_roles = $this->session->userdata('kd_roles');
        $apr = "";
        if ($this->is_sdm) {
            $apr = "T";
        } else {
            $apr = 'A';
        }
        $data_karir = array(
            'nik' => $nik,
            'nama_perusahaan' => $nama_perusahaan_jabatan,
            'nama_pekerjaan' => $nama_pekerjaan,
            'jabatan_terakhir' => $jabatan_terakhir_jabatan,
            'tanggal_masuk' => $masuk_jabatan,
            'tanggal_keluar' => $keluar_jabatan,
            'gaji' => $gaji_terakhir_jabatan,
            'alasan_keluar' => $alasan_keluar_jabatan,
            'apr' => $apr
        );

        $this->db->insert('hr_mpeg_karir', $data_karir);
        $this->db->query("delete from hr_mpeg_karir where nama_perusahaan='' and nik='$nik'");
    }

    function update_karir_mdl() {
        $nik = $this->input->post('nik');
        $nama_perusahaan_jabatan = $this->input->post('nama_perusahaan_jabatan');
        $nama_pekerjaan = $this->input->post('nama_pekerjaan');
        $jabatan_terakhir_jabatan = $this->input->post('jabatan_terakhir_jabatan');
        $masuk_jabatan = $this->input->post('masuk_jabatan');
        $keluar_jabatan = $this->input->post('keluar_jabatan');
        $gaji_terakhir_jabatan = $this->input->post('gaji_terakhir_jabatan');
        $alasan_keluar_jabatan = $this->input->post('alasan_keluar_jabatan');
        $noid = $this->input->post('noid');
        $kd_roles = $this->session->userdata('kd_roles');
        $masuk_jabatan = date('Y-m-d', strtotime($masuk_jabatan));
        $keluar_jabatan = date('Y-m-d', strtotime($keluar_jabatan));
        $kd_roles = $this->session->userdata('kd_roles');
        if ($this->is_sdm) {
            $apr = "T";
        } else {
            $apr = 'F';
        }

        $data_karir = array(
            'nik' => $nik,
            'nama_perusahaan' => $nama_perusahaan_jabatan,
            'nama_pekerjaan' => $nama_pekerjaan,
            'jabatan_terakhir' => $jabatan_terakhir_jabatan,
            'tanggal_masuk' => $masuk_jabatan,
            'tanggal_keluar' => $keluar_jabatan,
            'gaji' => $gaji_terakhir_jabatan,
            'alasan_keluar' => $alasan_keluar_jabatan,
            'apr' => $apr,
            'id_edit' => $noid,
        );

        /* $this->db->where('mpeg_karir_id', $noid);
          $this->db->update('hr_mpeg_karir', $data_karir); */


        if ($this->is_sdm) {
            //$this->db->query("delete from  	hr_mpeg_kursus where mpeg_khusus_id='$noid'");
            $this->db->where('mpeg_karir_id', $noid);
            $this->db->update('hr_mpeg_karir', $data_karir);
        } else {
            $this->db->insert('hr_mpeg_karir', $data_karir);
            $this->db->query("update hr_pegawai set apr='F' where nik='$nik'");
        }
    }

    function delete_karir_mdl() {
        $nik = $_POST['nik'];
        $noid = $_POST['noid'];
        $kd_roles = $this->session->userdata('kd_roles');
        $data_kursus = array(
            'nik' => $nik,
            'apr' => 'D',
            'id_edit' => $noid
        );

        if ($this->is_sdm) {
            $this->db->query("delete from  	hr_mpeg_karir where mpeg_karir_id='$noid'");
        } else {
            $this->db->insert('hr_mpeg_karir', $data_kursus);
            $this->db->query("update hr_pegawai set apr='F' where nik='$nik'");
        }
    }

    function load_kompetensi_pegawai($nik) {
        $query = $this->db->query("select hr_kompetensi_pegawai.id,hr_kompetensi_pegawai.nik,hr_kompetensi_pegawai.jenis_kompetensi,hr_kompetensi_pegawai.keterangan,hr_kompetensi_pegawai.id_kompetensi,hr_kompetensi.nama_kompetensi from hr_kompetensi_pegawai 
                left join hr_kompetensi 
                on hr_kompetensi_pegawai.id_kompetensi=hr_kompetensi.id_kompetensi
                where hr_kompetensi_pegawai.id_kompetensi > '1'
                and nik='$nik'");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    function load_kompetensi() {
        $query = $this->db->query("select * from hr_kompetensi where id_kompetensi > '0' ");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    function load_approval_pegawai() {
        $unit_kerja = $this->input->post('unit_kerja');
        $kd_jabatan = $this->session->userdata('kd_jabatan');
        $lokasi_kerja = $this->input->post('lokasi_kerja');
        $nik = $this->input->post('txtnik');
        $nama = $this->input->post('txtnama');
        $nik_s = $this->session->userdata('nik_session');
        $filter = "";
        $filter_user = "";
        $grup_id = "";
        $filter_pegawai = "";
        $kd_cabang_user = $this->session->userdata('cabang_user');
        $kd_unit_kerja_user = $this->session->userdata('kd_unit_kerja');
        $grup_id = $this->session->userdata('kd_roles');

        if ($lokasi_kerja == '' and $unit_kerja == '' and $nik == '' and $nama == '') {
            $filter_user = "";
        }
        // $filter_user="and hr_pegawai.kd_cabang='$kd_cabang_user' and   hr_pegawai.kd_unit_kerja='$kd_unit_kerja_user'";
        if ($lokasi_kerja != '') {
            $filter_user = " and hr_pegawai.kd_cabang='$lokasi_kerja'";
        }
        if ($unit_kerja != '') {
            $filter_user = $filter_user . " and hr_pegawai.kd_unit_kerja='$unit_kerja'";
        }
        if ($nik != '') {
            $filter_user = $filter_user . " and hr_pegawai.nik ilike '%$nik%'";
        }
        if ($nama != '') {
            $filter_user = $filter_user . " and  hr_pegawai.nama ilike '%$nama%'";
        }

        $query = $this->db->query("select 
                    DISTINCT(hr_pegawai.nik),
                    hr_pegawai.nama,
                    
                    to_char(hr_pegawai.tgl_masuk, 'dd-mm-yyyy') as tgl_masuk, 
                    to_char(hr_pegawai.tgl_capeg, 'dd-mm-yyyy') as tgl_capeg,
                    /*date_format(hr_pegawai.tgl_masuk, '%d-%m-%y') as tgl_masuk,
                    date_format(hr_pegawai.tgl_capeg, '%d-%m-%y') as tgl_capeg,*/
                    hr_ref_jabatan.nama_jabatan, 
                    hr_ref_unit_kerja.nama_unit_kerja, 
                    hr_ref_cabang.nama_cabang,
                    hr_ref_jabatan.kd_jabatan,
                    hr_ref_unit_kerja.kd_unit_kerja,
                    hr_ref_cabang.kd_cabang
                    FROM hr_pegawai
                    LEFT JOIN hr_ref_jabatan
                    ON hr_pegawai.kd_jabatan=hr_ref_jabatan.kd_jabatan
                    LEFT JOIN hr_ref_unit_kerja
                    ON hr_pegawai.kd_unit_kerja=hr_ref_unit_kerja.kd_unit_kerja
                    LEFT JOIN hr_ref_cabang
                    ON hr_pegawai.kd_cabang=hr_ref_cabang.kd_cabang 
                    
                    where hr_pegawai.apr ='F'  
                    $filter_user $filter $filter_pegawai 
                    LIMIT $this->limit OFFSET $this->offset");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    function count_approval() {
        $unit_kerja = $this->input->post('unit_kerja');
        $kd_jabatan = $this->session->userdata('kd_jabatan');
        $lokasi_kerja = $this->input->post('lokasi_kerja');
        $nik = $this->input->post('txtnik');
        $nama = $this->input->post('txtnama');
        $nik_s = $this->session->userdata('nik_session');
        $filter = "";
        $filter_user = "";
        $grup_id = "";
        $filter_pegawai = "";
        $kd_cabang_user = $this->session->userdata('cabang_user');
        $kd_unit_kerja_user = $this->session->userdata('kd_unit_kerja');
        $grup_id = $this->session->userdata('kd_roles');

        if ($lokasi_kerja == '' and $unit_kerja == '' and $nik == '' and $nama == '') {
            $filter_user = "";
        }
        // $filter_user="and hr_pegawai.kd_cabang='$kd_cabang_user' and   hr_pegawai.kd_unit_kerja='$kd_unit_kerja_user'";
        if ($lokasi_kerja != '') {
            $filter_user = " and hr_pegawai.kd_cabang='$lokasi_kerja'";
        }
        if ($unit_kerja != '') {
            $filter_user = $filter_user . " and hr_pegawai.kd_unit_kerja='$unit_kerja'";
        }
        if ($nik != '') {
            $filter_user = $filter_user . " and hr_pegawai.nik ilike '%$nik%'";
        }
        if ($nama != '') {
            $filter_user = $filter_user . " and  hr_pegawai.nama ilike '%$nama%'";
        }
        $query = $this->db->query("select 
                    DISTINCT(hr_pegawai.nik),
                    hr_pegawai.nama,
                    
                    to_char(hr_pegawai.tgl_masuk, 'dd-mm-yyyy') as tgl_masuk, 
                    to_char(hr_pegawai.tgl_capeg, 'dd-mm-yyyy') as tgl_capeg,
                    /*date_format(hr_pegawai.tgl_masuk, '%d-%m-%y') as tgl_masuk,
                    date_format(hr_pegawai.tgl_capeg, '%d-%m-%y') as tgl_capeg,*/
                    hr_ref_jabatan.nama_jabatan, 
                    hr_ref_unit_kerja.nama_unit_kerja, 
                    hr_ref_cabang.nama_cabang,
                    hr_ref_jabatan.kd_jabatan,
                    hr_ref_unit_kerja.kd_unit_kerja,
                    hr_ref_cabang.kd_cabang
                    FROM hr_pegawai
                    LEFT JOIN hr_ref_jabatan
                    ON hr_pegawai.kd_jabatan=hr_ref_jabatan.kd_jabatan
                    LEFT JOIN hr_ref_unit_kerja
                    ON hr_pegawai.kd_unit_kerja=hr_ref_unit_kerja.kd_unit_kerja
                    LEFT JOIN hr_ref_cabang
                    ON hr_pegawai.kd_cabang=hr_ref_cabang.kd_cabang 
                    
                    where hr_pegawai.apr ='F' 
                    $filter_user $filter $filter_pegawai ");
        return $query->num_rows();
    }

    function approve_pegaawai_by_kd_roles() {
        $nik = $this->input->post('nik');
        $noid = 0;
        $status_kawin = '';
        $ptkp = '';
        $k_natura = '';
        $status_kawin = '';
        $status_keluarga = '';
        $gender = '';
        $q_profil = $this->db->query("select status_kawin,gender  from hr_pegawai_detail where nik='$nik'");
        if ($q_profil->num_rows() > 0):
            $row_profil = $q_profil->row();
            $status_kawin = $row_profil->status_kawin;
            $gender = $row_profil->gender;
        endif;
        $q_lev = $this->db->query("select kd_level  from hr_pegawai where nik='$nik'");
        if ($q_lev->num_rows() > 0):
            $row_lev = $q_lev->row();
            $kd_level = $row_lev->kd_level;

        endif;
        $get_property_tunjangan = $this->db->query("select hr_pegawai.kd_level ,hr_pegawai_detail.gender,hr_pegawai_detail.status_kawin,m_user.grup_id from hr_pegawai
								left join hr_pegawai_detail on hr_pegawai_detail.nik=hr_pegawai.nik
								left join m_user on m_user.nik=hr_pegawai_detail.nik
								where hr_pegawai.nik='$nik'
								");

        foreach ($get_property_tunjangan->result() as $row_t) {
            $select_status_kawin_pegawai = $row_t->status_kawin;
            $select_pangkat = $row_t->kd_level;
            $gender = $row_t->gender;
        }

        if (($gender == 'P') or ( $gender == 'Perempuan')) {
            $gender = 'P';
        }
        if (($gender == 'L') or ( $gender == 'Laki-laki')) {
            $gender = 'L';
        }

        if (($select_status_kawin_pegawai == 'BK') or ( $select_status_kawin_pegawai == 'Belum Kawin')) {
            $select_status_kawin_pegawai = 'BK';
        }

        if (($select_status_kawin_pegawai == 'K') or ( $select_status_kawin_pegawai == 'Kawin')) {
            $select_status_kawin_pegawai = 'K';
        }



        $count_jumlah_anak = $this->db->query("select count(nik) as jumlah_anak from hr_mpeg_anak where nik='$nik'");
        foreach ($count_jumlah_anak->result_array() as $row) {
            $jumlah_anak = $row['jumlah_anak'];
        }
        if ($jumlah_anak > 3) {
            $jumlah_anak = 3;
        }




        if ($gender == 'P' and $select_pangkat == 0) {
            $k_natura = "b";
            $ptkp = "tk";
        }
        if ($gender == 'L' and $select_pangkat == 0) {
            $k_natura = "bl";
            $ptkp = "tk";
        }

        if ($select_status_kawin_pegawai == 'BK' and $jumlah_anak == 0 and $gender == 'P' and $select_pangkat > 0) {
            $k_natura = "b";
            $ptkp = "tk";
        }
        if ($select_status_kawin_pegawai == 'BK' and $jumlah_anak == 0 and $gender == 'L' and $select_pangkat > 0) {
            $k_natura = "k0";
            $ptkp = "tk";
        }

        if ($select_status_kawin_pegawai == 'K' and $jumlah_anak == 0 and $gender == 'P' and $select_pangkat > 0) {
            $k_natura = "b";
            $ptkp = "k";
        }
        if ($select_status_kawin_pegawai == 'K' and $jumlah_anak == 0 and $gender == 'L' and $select_pangkat > 0) {
            $k_natura = "k0";
            $ptkp = "k";
        }


        if ($select_status_kawin_pegawai == 'K' and $gender == 'L' and $jumlah_anak > 0 and $select_pangkat > 0) {
            $k_natura = "k" . $jumlah_anak;
            $ptkp = "k";
        }
        if ($select_status_kawin_pegawai == 'K' and $gender == 'P' and $jumlah_anak > 0 and $select_pangkat > 0) {
            $k_natura = "kts" . $jumlah_anak;
            $ptkp = "k";
        }
        if ($select_status_kawin_pegawai == 'BK' and $gender == 'L' and $jumlah_anak > 0 and $select_pangkat > 0) {
            $k_natura = "k" . $jumlah_anak;
            $ptkp = "k";
        }
        if ($select_status_kawin_pegawai == 'BK' and $gender == 'P' and $jumlah_anak > 0 and $select_pangkat > 0) {
            $k_natura = "kts" . $jumlah_anak;
            $ptkp = "k";
        }


        if ($gender == 'L' and $select_status_kawin_pegawai == 'K') {
            $status_keluarga = 'X/' . $jumlah_anak;
        }
        if ($gender == 'P' and $select_status_kawin_pegawai == 'K') {
            $status_keluarga = 'Y/' . $jumlah_anak;
        }
        if ($gender == 'P' and $select_status_kawin_pegawai == 'D') {
            $status_keluarga = 'X/' . $jumlah_anak;
        }
        if ($gender == 'P' and $select_status_kawin_pegawai == 'J') {
            $status_keluarga = 'Y/' . $jumlah_anak;
        }

        if ($gender == 'L' and $select_pangkat == '0' and $select_status_kawin_pegawai == 'BK') {
            $status_keluarga = 'BL';
        }
        if ($gender == 'P' and $select_pangkat == '0' and $select_status_kawin_pegawai == 'BK') {
            $status_keluarga = 'BP';
        }

        if ($gender == 'L' and $select_status_kawin_pegawai == 'BK') {
            $status_keluarga = 'BL';
        }
        if ($gender == 'P' and $select_status_kawin_pegawai == 'BK') {
            $status_keluarga = 'BP';
        }

        $data_setup_tunjangan = array(
            'nik' => $nik,
            'jumlah_anak' => $jumlah_anak,
            'persentase_gaji' => '100',
            'nilai_kontrak' => '0',
            'status_keluarga' => $status_keluarga,
            'status_ptkp' => $ptkp,
            'kelompok_natura' => $k_natura
        );
        $this->db->trans_start();
        $this->db->where('nik', $nik);
        $this->db->update('hr_setup_tunjangan_sejahtera', $data_setup_tunjangan);
        $this->db->trans_complete();
    }

    function approve_pegaawai_approval_mdl($nik) {
        $lnik = $nik;
        $replacer = array("%20");
        $replacewith = array(" ");
        $nik = str_replace($replacer, $replacewith, $lnik);
        $noid = 0;
        $status_kawin = '';
        $ptkp = '';
        $k_natura = '';
        $status_kawin = '';
        $status_keluarga = '';
        $gender = '';
        //*-------------- GET STATUS KTS ---------------*//
        $q_profil = $this->db->query("select status_kawin,gender  from hr_pegawai_detail where nik='$nik'");
        if ($q_profil->num_rows() > 0):
            $row_profil = $q_profil->row();
            $status_kawin = $row_profil->status_kawin;
            $gender = $row_profil->gender;
        endif;
        $q_lev = $this->db->query("select kd_level  from hr_pegawai where nik='$nik'");
        if ($q_lev->num_rows() > 0):
            $row_lev = $q_lev->row();
            $kd_level = $row_lev->kd_level;

        endif;
        /* ----------------------------------------------- */
        //*-------------- UPDATE / DELETE  ANAK ----------*/
        $query_anak = $this->db->query("select id_edit from hr_mpeg_anak where apr='F' and nik='$nik'");
        foreach ($query_anak->result_array() as $row) {
            $noid = $row['id_edit'];
            if ($noid == '') {
                $noid = 0;
            }
            $this->db->query("delete from hr_mpeg_anak where hr_mpeg_anak.mpeg_anak_id='$noid' and apr='T'");
            $this->db->query("update hr_mpeg_anak set apr='T' where id_edit='$noid' and apr='F'");
        }
        //*-------------- ADD  ANAK ----------*/
        $query_anak = $this->db->query("select mpeg_anak_id from hr_mpeg_anak where apr='A' and nik='$nik'");
        foreach ($query_anak->result_array() as $row) {
            $noid = $row['mpeg_anak_id'];
            if ($noid == '') {
                $noid = 0;
            }
            $this->db->query("update hr_mpeg_anak set apr='T' where mpeg_anak_id='$noid'");
        }
        //*------------------ DELETE  ANAK --------------*/
        $query_anak_delete = $this->db->query("select id_edit from hr_mpeg_anak where apr='D' and nik='$nik'");
        foreach ($query_anak_delete->result_array() as $row_anak) {
            $noid = $row_anak['id_edit'];
            if ($noid == '') {
                $noid = 0;
            }
            $this->db->query("delete from hr_mpeg_anak where hr_mpeg_anak.mpeg_anak_id='$noid'");
            $this->db->query("delete from hr_mpeg_anak where hr_mpeg_anak.id_edit='$noid' and apr='D'");
        }
        /* ----------------------------------------------- */
        //*-------------- UPDATE / DELETE  SEKOLAH ----------*/
        $query_pendidikan = $this->db->query("select id_edit from hr_mpeg_sekolah where apr='F' and nik='$nik'");
        foreach ($query_pendidikan->result_array() as $row) {
            $noid = $row['id_edit'];
            if ($noid == '') {
                $noid = 0;
            }
            $this->db->query("delete from  	hr_mpeg_sekolah where mpeg_sekolah_id='$noid'");
            $this->db->query("update 	hr_mpeg_sekolah set apr='T' where id_edit='$noid'");
        }
        //*-------------- ADD  PENDIDIKAN ----------*/
        $query_anak_add = $this->db->query("select mpeg_sekolah_id from hr_mpeg_sekolah where apr='A' and nik='$nik'");
        foreach ($query_anak_add->result_array() as $row) {
            $noid = $row['mpeg_sekolah_id'];
            if ($noid == '') {
                $noid = 0;
            }
            $this->db->query("update hr_mpeg_sekolah set apr='T' where mpeg_sekolah_id='$noid'");
        }
        //*------------------ DELETE  PENDIDIKAN --------------*/
        $query_pendidikan_delete = $this->db->query("select id_edit from hr_mpeg_sekolah where apr='D' and nik='$nik'");
        foreach ($query_pendidikan_delete->result_array() as $row_pddk) {
            $noid = $row_pddk['id_edit'];
            if ($noid == '') {
                $noid = 0;
            }
            $this->db->query("delete from hr_mpeg_sekolah where hr_mpeg_sekolah.mpeg_sekolah_id='$noid'");
            $this->db->query("delete from hr_mpeg_sekolah where hr_mpeg_sekolah.id_edit='$noid' and apr='D'");
        }
        /* ----------------------------------------------- */
        //*-------------- UPDATE / DELETE  PELATIHAN ----------*/
        $query_pelatihan = $this->db->query("select id_edit from hr_mpeg_kursus where apr='F' and nik='$nik'");
        foreach ($query_pelatihan->result_array() as $row_pelatihan) {
            $noid = $row_pelatihan['id_edit'];
            if ($noid == '') {
                $noid = 0;
            }
            $this->db->query("delete from  	hr_mpeg_kursus where mpeg_khusus_id='$noid'");
            $this->db->query("update 	hr_mpeg_kursus set apr='T' where id_edit='$noid'");
        }
        //*-------------- ADD  PELATIHAN ----------*/
        $query_pelatihan = $this->db->query("select mpeg_khusus_id from hr_mpeg_kursus where apr='A' and nik='$nik'");
        foreach ($query_pelatihan->result_array() as $row_pelatihan) {
            $noid = $row_pelatihan['mpeg_khusus_id'];
            if ($noid == '') {
                $noid = 0;
            }
            $this->db->query("update hr_mpeg_kursus set apr='T' where mpeg_khusus_id='$noid'");
        }
        //*------------------ DELETE  PELATIHAN --------------*/
        $query_pelatihan_delete = $this->db->query("select id_edit from hr_mpeg_kursus where apr='D' and nik='$nik'");
        foreach ($query_pelatihan_delete->result_array() as $row_pelth) {
            $noid = $row_pelth['id_edit'];
            if ($noid == '') {
                $noid = 0;
            }
            $this->db->query("delete from hr_mpeg_kursus where hr_mpeg_kursus.mpeg_khusus_id='$noid'");
            $this->db->query("delete from hr_mpeg_kursus where hr_mpeg_kursus.id_edit='$noid' and apr='D'");
        }
        /* ----------------------------------------------- */
        //*-------------- UPDATE / DELETE  KARIR ----------*/
        $query_karir = $this->db->query("select id_edit from hr_mpeg_karir where apr='F' and nik='$nik'");
        foreach ($query_karir->result_array() as $row_pelatihan) {
            $noid = $row_pelatihan['id_edit'];
            if ($noid == '') {
                $noid = 0;
            }
            $this->db->query("delete from  	hr_mpeg_karir where mpeg_karir_id='$noid'");
            $this->db->query("update 	hr_mpeg_karir set apr='T' where id_edit='$noid'");
        }
        //*-------------- ADD  KARIR ----------*/
        $query_karir = $this->db->query("select mpeg_karir_id from hr_mpeg_karir where apr='A' and nik='$nik'");
        foreach ($query_karir->result_array() as $row_pelatihan) {
            $noid = $row_pelatihan['mpeg_karir_id'];
            if ($noid == '') {
                $noid = 0;
            }
            $this->db->query("update hr_mpeg_karir set apr='T' where mpeg_karir_id='$noid'");
        }
        //*------------------ DELETE  KARIR --------------*/
        $query_karir_delete = $this->db->query("select id_edit from hr_mpeg_karir where apr='D' and nik='$nik'");
        foreach ($query_karir_delete->result_array() as $row_karir) {
            $noid = $row_karir['id_edit'];
            if ($noid == '') {
                $noid = 0;
            }
            $this->db->query("delete from hr_mpeg_karir where hr_mpeg_karir.mpeg_karir_id='$noid'");
            $this->db->query("delete from hr_mpeg_karir where hr_mpeg_karir.id_edit='$noid' and apr='D'");
        }

        $this->db->query("update hr_mpeg_anak set apr='T' where nik='$nik' and apr='F'");
        $this->db->query("update hr_mpeg_anak set apr='T' where nik='$nik' and apr='F'");
        $this->db->query("update hr_mpeg_karir set apr='T' where nik='$nik' and apr='F' ");
        $this->db->query("update hr_mpeg_sekolah set apr='T' where nik='$nik' and apr='F' ");

        $this->db->query("delete from hr_mpeg_karir where apr='D' and nik='$nik'");
        $this->db->query("update hr_pegawai set apr='T' where nik='$nik'");
        $this->db->query("delete from hr_pegawai_detail where nik='$nik' and apr='T'");
        $this->db->query("update hr_pegawai_detail set apr='T' where nik='$nik' and apr='F'");

        $get_property_tunjangan = $this->db->query("select hr_pegawai.kd_level ,hr_pegawai_detail.gender,hr_pegawai_detail.status_kawin,m_user.grup_id from hr_pegawai
								left join hr_pegawai_detail on hr_pegawai_detail.nik=hr_pegawai.nik
								left join m_user on m_user.nik=hr_pegawai_detail.nik
								where hr_pegawai.nik='$nik'
								");

        foreach ($get_property_tunjangan->result() as $row_t) {
            $select_status_kawin_pegawai = $row_t->status_kawin;
            $select_pangkat = $row_t->kd_level;
            $gender = $row_t->gender;
        }

        if (($gender == 'P') or ( $gender == 'Perempuan')) {
            $gender = 'P';
        }
        if (($gender == 'L') or ( $gender == 'Laki-laki')) {
            $gender = 'L';
        }

        if (($select_status_kawin_pegawai == 'BK') or ( $select_status_kawin_pegawai == 'Belum Kawin')) {
            $select_status_kawin_pegawai = 'BK';
        }

        if (($select_status_kawin_pegawai == 'K') or ( $select_status_kawin_pegawai == 'Kawin')) {
            $select_status_kawin_pegawai = 'K';
        }



        $count_jumlah_anak = $this->db->query("select count(nik) as jumlah_anak from hr_mpeg_anak where nik='$nik'");
        foreach ($count_jumlah_anak->result_array() as $row) {
            $jumlah_anak = $row['jumlah_anak'];
        }
        if ($jumlah_anak > 3) {
            $jumlah_anak = 3;
        }




        if ($gender == 'P' and $select_pangkat == 0) {
            $k_natura = "b";
            $ptkp = "tk";
        }
        if ($gender == 'L' and $select_pangkat == 0) {
            $k_natura = "bl";
            $ptkp = "tk";
        }

        if ($select_status_kawin_pegawai == 'BK' and $jumlah_anak == 0 and $gender == 'P' and $select_pangkat > 0) {
            $k_natura = "b";
            $ptkp = "tk";
        }
        if ($select_status_kawin_pegawai == 'BK' and $jumlah_anak == 0 and $gender == 'L' and $select_pangkat > 0) {
            $k_natura = "k0";
            $ptkp = "tk";
        }

        if ($select_status_kawin_pegawai == 'K' and $jumlah_anak == 0 and $gender == 'P' and $select_pangkat > 0) {
            $k_natura = "b";
            $ptkp = "k";
        }
        if ($select_status_kawin_pegawai == 'K' and $jumlah_anak == 0 and $gender == 'L' and $select_pangkat > 0) {
            $k_natura = "k0";
            $ptkp = "k";
        }


        if ($select_status_kawin_pegawai == 'K' and $gender == 'L' and $jumlah_anak > 0 and $select_pangkat > 0) {
            $k_natura = "k" . $jumlah_anak;
            $ptkp = "k";
        }
        if ($select_status_kawin_pegawai == 'K' and $gender == 'P' and $jumlah_anak > 0 and $select_pangkat > 0) {
            $k_natura = "kts" . $jumlah_anak;
            $ptkp = "k";
        }
        if ($select_status_kawin_pegawai == 'BK' and $gender == 'L' and $jumlah_anak > 0 and $select_pangkat > 0) {
            $k_natura = "k" . $jumlah_anak;
            $ptkp = "k";
        }
        if ($select_status_kawin_pegawai == 'BK' and $gender == 'P' and $jumlah_anak > 0 and $select_pangkat > 0) {
            $k_natura = "kts" . $jumlah_anak;
            $ptkp = "k";
        }


        if ($gender == 'L' and $select_status_kawin_pegawai == 'K') {
            $status_keluarga = 'X/' . $jumlah_anak;
        }
        if ($gender == 'P' and $select_status_kawin_pegawai == 'K') {
            $status_keluarga = 'Y/' . $jumlah_anak;
        }
        if ($gender == 'P' and $select_status_kawin_pegawai == 'D') {
            $status_keluarga = 'X/' . $jumlah_anak;
        }
        if ($gender == 'P' and $select_status_kawin_pegawai == 'J') {
            $status_keluarga = 'Y/' . $jumlah_anak;
        }

        if ($gender == 'L' and $select_pangkat == '0' and $select_status_kawin_pegawai == 'BK') {
            $status_keluarga = 'BL';
        }
        if ($gender == 'P' and $select_pangkat == '0' and $select_status_kawin_pegawai == 'BK') {
            $status_keluarga = 'BP';
        }

        if ($gender == 'L' and $select_status_kawin_pegawai == 'BK') {
            $status_keluarga = 'BL';
        }
        if ($gender == 'P' and $select_status_kawin_pegawai == 'BK') {
            $status_keluarga = 'BP';
        }

        $data_setup_tunjangan = array(
            'nik' => $nik,
            'jumlah_anak' => $jumlah_anak,
            'persentase_gaji' => '100',
            'nilai_kontrak' => '0',
            'status_keluarga' => $status_keluarga,
            'status_ptkp' => $ptkp,
            'kelompok_natura' => $k_natura
        );
        $this->db->trans_start();
        $this->db->where('nik', $nik);
        $this->db->update('hr_setup_tunjangan_sejahtera', $data_setup_tunjangan);
        $this->db->trans_complete();
    }

    function reject_approval_mdl($nik) {
        $lnik = $nik;
        $replacer = array("%20");
        $replacewith = array(" ");
        $nik = str_replace($replacer, $replacewith, $lnik);
        $query_anak = $this->db->query("select mpeg_anak_id from hr_mpeg_anak where apr='F' and nik='$nik'");
        foreach ($query_anak->result_array() as $row) {
            $noid = $row['mpeg_anak_id'];
            $this->db->query("delete from  hr_mpeg_anak where mpeg_anak_id='$noid'");
        }
        $this->db->query("delete from hr_mpeg_anak where apr='D' and nik='$nik'");

        $query_pendidikan = $this->db->query("select mpeg_sekolah_id from hr_mpeg_sekolah where apr='F' and nik='$nik'");
        foreach ($query_pendidikan->result_array() as $row) {
            $noid = $row['mpeg_sekolah_id'];
            $this->db->query("delete from  	hr_mpeg_sekolah where mpeg_sekolah_id='$noid'");
        }
        $this->db->query("delete from hr_mpeg_sekolah where apr='D' and nik='$nik'");

        $query_pendidikan = $this->db->query("select mpeg_khusus_id from hr_mpeg_kursus where apr='F' and nik='$nik'");
        foreach ($query_pendidikan->result_array() as $row) {
            $noid = $row['mpeg_khusus_id'];
            $this->db->query("delete from  	hr_mpeg_kursus where mpeg_khusus_id='$noid'");
        }
        $this->db->query("delete from hr_mpeg_kursus where apr='D' and nik='$nik'");

        $query_pendidikan = $this->db->query("select mpeg_karir_id from hr_mpeg_karir where apr='F' and nik='$nik'");
        foreach ($query_pendidikan->result_array() as $row) {
            $noid = $row['mpeg_karir_id'];
            $this->db->query("delete from  	hr_mpeg_karir where mpeg_karir_id='$noid'");
        }
        $this->db->query("delete from hr_mpeg_karir where apr='D' and nik='$nik'");
        $query_pendidikan = $this->db->query("select id from hr_kompetensi_pegawai where apr='F' and nik='$nik'");
        foreach ($query_pendidikan->result_array() as $row) {
            $noid = $row['id'];
            $this->db->query("delete from  	hr_kompetensi_pegawai where id='$noid'");
        }
        $this->db->query("delete from hr_kompetensi_pegawai where apr='D' and nik='$nik'");




        $this->db->query("delete from  hr_pegawai_detail where nik_tmp='$nik' and apr='F'");
        $this->db->query("update hr_pegawai set apr='T' where nik='$nik'");
    }

    function disabled_user_mdl() {
        $nik = $_POST['nik'];
        $kd_roles = $this->session->userdata('kd_roles');
        if ($this->is_sdm) {
            $this->db->query("update hr_pegawai set apr='T' where nik='$nik'");
            $this->approve_pegaawai_by_kd_roles();
        } else {
            $this->db->query("update hr_pegawai set apr='F' where nik='$nik'");
        }
    }

    function cetak_paper_mdl() {
        /* get the session */
        $cabang_user = $this->session->userdata('cabang_user');
        $unit_kerja_user = $this->session->userdata('unit_kerja_ses');
        $kd_roles = $this->session->userdata('kd_roles');
        $nik_session = $this->session->userdata('nik_session');

        /* --------------- */

        $flokasi = $this->input->post('filter_lokasi');
        $funitkerja = $this->input->post('filter_unit_kerja');
        $kd_unit_kerja = "";
        $kd_cabang = "";

        $flokasi = $this->input->post('filter_lokasi');
        $funitkerja = $this->input->post('filter_unit_kerja');
        $kd_cabang = $flokasi;
        $kd_unit_kerja = $funitkerja;

        if ($kd_roles = '19') {
            $filter_pegawai = "and hr_pegawai.nik='$nik_session'";
        }


        $query = "SELECT   
                    hr_pegawai.nik, 
                    hr_pegawai.nama, 
                    hr_pegawai.apr, 
                    to_char(hr_pegawai.tgl_masuk, 'dd-mm-yyyy') as tgl_masuk, 
                    to_char(hr_pegawai.tgl_capeg, 'dd-mm-yyyy') as tgl_capeg,                     
                    hr_pegawai_detail.g_depan, 
                    hr_pegawai_detail.g_belakang, 
                    hr_pegawai_detail.tmp_lahir, 
                    to_char(hr_pegawai_detail.tgl_lahir, 'dd-mm-yyyy') as tgl_lahir,
                    hr_pegawai_detail.gender,                 
                                
                    hr_ref_jabatan.nama_jabatan, 
                    hr_ref_unit_kerja.nama_unit_kerja, 
                    hr_ref_cabang.nama_cabang,
                    hr_ref_jabatan.kd_jabatan,
                    hr_ref_unit_kerja.kd_unit_kerja,
                    hr_ref_cabang.kd_cabang
                    FROM hr_pegawai
                    LEFT JOIN hr_ref_jabatan
                    ON hr_pegawai.kd_jabatan=hr_ref_jabatan.kd_jabatan
                    LEFT JOIN hr_ref_unit_kerja
                    ON hr_pegawai.kd_unit_kerja=hr_ref_unit_kerja.kd_unit_kerja
                    LEFT JOIN hr_ref_cabang
                    ON hr_pegawai.kd_cabang=hr_ref_cabang.kd_cabang 
                    LEFT JOIN hr_pegawai_detail
                    ON hr_pegawai.nik=hr_pegawai_detail.nik 

                    where 
                    hr_pegawai.status_aktif='Aktif' 
                    ORDER BY hr_pegawai.nama ASC LIMIT 20";

        $q = $this->db->query($query);
        foreach ($q->result() as $data) {
            $hasil[] = $data;
        }
        return $hasil;
    }

    /*    function simpan_workflow_penempatan($penempatan_id=0) {
      $query_nodes = $this->db->query("select nodes from hr_detail_workflow hdw left join hr_ref_workflow hrw
      on hdw.id_workflow=hrw.id_workflow where hdw.id_workflow=7
      order by hdw.urutan
      ");
      $kd_cabang = $this->session->userdata('cabang_user');
      $nodes = $this->tools->getObjeckWorkflow($kd_cabang); // ambil objeck - objeck workflownya

      foreach ($query_nodes->result() as $row_nodes) {

      $data['penempatan_id'] = $penempatan_id;

      $data['nik'] = $nodes[$row_nodes->nodes]['nik'];
      $data['kd_jabatan'] = $nodes[$row_nodes->nodes]['kd_jabatan'];

      $data['add_tanggal'] = date('Y-m-d');
      $data['step'] = $row_nodes->nodes;

      if ($row_nodes->nodes == 'pemohon') {
      $data['status'] = 'DONE';
      } elseif ($row_nodes->nodes == 'sdm') {
      $data['status'] = 'ENABLED';
      } else {
      $data['status'] = 'DISABLED';
      }
      $this->db->insert('hr_apr_penempatan', $data);
      }
      } */

    function edit_workflow_sppd($sppd_id = 0) {
        $data['status'] = 'ENABLED';
        $this->db->where('step', 'atasan1');
        $this->db->where('sppd_id', $sppd_id);
        $this->db->update('hr_apr_sppd', $data);

        $data['status'] = 'DONE';
        $this->db->where('step', 'pemohon');
        $this->db->where('sppd_id', $sppd_id);
        $this->db->update('hr_apr_sppd', $data);
    }

    function simpan_pengajuan_penempatan() {

        $data['kd_unit_kerja'] = $this->input->post('kd_unit_kerja');
        $data['jabatan'] = $this->input->post('kd_jabatan');
        $data['nama'] = $this->input->post('nama');
        $data['no_seleksi'] = $this->input->post('no_seleksi');
        $data['kd_cabang'] = $this->input->post('kd_cabang');
        $data['nilai'] = $this->input->post('nilai');
        $data['Pendidikan'] = $this->input->post('pendidikan');
        $data['seleksi_pegawai_id'] = $this->input->post('seleksi_pegawai_id');


        $this->db->insert('hr_penempatan', $data);
        return $this->db->insert_id();
    }

    function edit_pengajuan_sppd($sppd_id) {
        $data['kd_unit_kerja'] = $this->input->post('kd_unit_kerja');
        $data['kd_jabatan'] = $this->input->post('jabatan_pemohon');
        $data['kd_level'] = $this->input->post('kd_level');
        $data['kd_sppd'] = $this->input->post('kd_sppd');
        $data['keterangan'] = $this->input->post('alasan_sppd');
        $data['lama_sppd'] = $this->input->post('lamat_sppd');
        $data['kd_cabang'] = $this->input->post('kd_cabang');
        $data['tgl_mulai_sppd'] = date('Y-m-d', strtotime($this->input->post('tgl_mulai')));
        $data['tgl_selesai_sppd'] = date('Y-m-d', strtotime($this->input->post('tgl_selesai')));
        $data['kota_asal'] = $this->input->post('kota_asal');
        $data['kota_tujuan'] = $this->input->post('kota_tujuan');
        $data['total_biaya'] = $this->input->post('total_biaya_sppd_karyawan');
        $data['biaya_angkutan'] = $this->input->post('biaya_angkutan');
        $data['biaya_harian'] = $this->input->post('biaya_harian');

        $this->db->where('sppd_id', $sppd_id);
        $this->db->update('hr_sppd', $data);
        return $sppd_id;
    }

    function approvedPenempatan() {
        //$step=$this->input->post('step');
        $penempatan_id = $this->input->post('penempatan_id');
        $grup_id = $this->session->userdata('kd_roles');
        $id_jabatan_baru = $this->input->post('txtjab');
        $cabang = "";
        $unit_kerja = "";
        $jabatan = "";
        if ($id_jabatan_baru == '')
            $id_jabatan_baru = 0;
        $get_unit_kerja = $this->db->query("select 
                        kd_unit_kerja,kd_jabatan,kd_cabang 
                        FROM 
                        hr_permintaan_pegawai WHERE 
                        permintaan_pegawai_id='$id_jabatan_baru'");
        if ($get_unit_kerja->num_rows() > 0):
            $row_profil = $get_unit_kerja->row();
            $unit_kerja = $row_profil->kd_unit_kerja;
            $jabatan = $row_profil->kd_jabatan;
            $cabang = $row_profil->kd_cabang;
        endif;



        /* ----- Get Next Step And Urutan --- */
        $grup_id = $this->session->userdata('kd_roles');
        $next_step = "";
        $nodes = "";
        $position_dok = "";
        $next_step_urutan = "";
        /* --> Get Next Roles */
        $get_my_step = $this->db->query("select urutan from hr_detail_workflow where nodes = '$grup_id'  and id_workflow='13'");
        foreach ($get_my_step->result_array() as $row_urutan) {
            $next_step_urutan = $row_urutan['urutan'];
        }
        $get_my_step = $this->db->query("select urutan,nodes from hr_detail_workflow where urutan > '$next_step_urutan' and id_workflow='13' ORDER BY urutan ASC limit 1 ");
        foreach ($get_my_step->result_array() as $row) {
            $next_step = $row['nodes'];
        }
        //$add_by=$this->input->post('add_by');
        if ($grup_id != '4' and $grup_id != '6'):
            $data['status'] = $next_step;
        endif;
        if ($grup_id == '6'):
            $data['no_sk'] = 'DIR/PENEMPATAN/' . $penempatan_id . '/' . date('Y');
            $data['tgl_sk'] = date('Y-m-d');
            $data['status'] = 'Finished';
        endif;
        if ($grup_id == '4'):
            $data['jabatan'] = $jabatan;
            $data['kd_unit_kerja'] = $unit_kerja;
            $data['kd_cabang'] = $cabang;
            $data['status'] = $next_step;
        endif;



        $this->db->where('penempatan_id', $this->input->post('penempatan_id'));
        $this->db->update('hr_penempatan', $data);
    }

    function return_to_sdm() {
        $penempatan_id = $this->input->post('penempatan_id');
        $grup_id = $this->session->userdata('kd_roles');
        $id_jabatan_baru = $this->input->post('txtjab');
        $cabang = "";
        $unit_kerja = "";

        $data['status'] = 4;

        $this->db->where('penempatan_id', $this->input->post('penempatan_id'));
        $this->db->update('hr_penempatan', $data);
    }

    function returnedPenempatan() {
        /* $add_by=$this->input->post('add_by');
          $data['status']='Return By '.$add_by;
          $this->db->where('penempatan_id',$this->input->post('penempatan_id'));
          $this->db->update('hr_penempatan', $data); */
        //$step=$this->input->post('step');
        $penempatan_id = $this->input->post('penempatan_id');
        $grup_id = $this->session->userdata('kd_roles');
        $id_jabatan_baru = $this->input->post('txtjab');
        $cabang = "";
        $unit_kerja = "";
        $jabatan = "";
        if ($id_jabatan_baru == '')
            $id_jabatan_baru = 0;
        $get_unit_kerja = $this->db->query("select 
                        kd_unit_kerja,kd_jabatan,kd_cabang 
                        FROM 
                        hr_permintaan_pegawai WHERE 
                        permintaan_pegawai_id='$id_jabatan_baru'");
        if ($get_unit_kerja->num_rows() > 0):
            $row_profil = $get_unit_kerja->row();
            $unit_kerja = $row_profil->kd_unit_kerja;
            $jabatan = $row_profil->kd_jabatan;
            $cabang = $row_profil->kd_cabang;
        endif;



        /* ----- Get Next Step And Urutan --- */
        $grup_id = $this->session->userdata('kd_roles');
        $next_step = "";
        $nodes = "";
        $position_dok = "";
        $next_step_urutan = "";
        /* --> Get Next Roles */
        $get_my_step = $this->db->query("select urutan from hr_detail_workflow where nodes = '$grup_id'  and id_workflow='13'");
        foreach ($get_my_step->result_array() as $row_urutan) {
            $next_step_urutan = $row_urutan['urutan'];
        }
        $get_my_step = $this->db->query("select urutan,nodes from hr_detail_workflow where urutan < '$next_step_urutan' and id_workflow='13' ORDER BY urutan DESC limit 1 ");
        foreach ($get_my_step->result_array() as $row) {
            $next_step = $row['nodes'];
        }
        if ($grup_id == '6'):

            $data['no_sk'] = 'DIR/PENEMPATAN/' . $penempatan_id . '/' . date('Y');
            $data['tgl_sk'] = date('Y-m-d');
            $data['status'] = $next_step;
        endif;
        if ($grup_id == '4'):
            $data['jabatan'] = $jabatan;
            $data['kd_unit_kerja'] = $unit_kerja;
            $data['kd_cabang'] = $cabang;
            $data['status'] = $next_step;
        endif;
        //$add_by=$this->input->post('add_by');
        if ($grup_id != '4' and $grup_id != '6'):
            $data['status'] = $next_step;
        endif;
        $this->db->where('penempatan_id', $this->input->post('penempatan_id'));
        $this->db->update('hr_penempatan', $data);
    }

    function rejectedPenempatan() {
        $add_by = $this->input->post('add_by');
        $data['status'] = 'Rejected By ' . $add_by;
        $this->db->where('penempatan_id', $this->input->post('penempatan_id'));
        $this->db->update('hr_penempatan', $data);
        $penempatan_id = $this->input->post('penempatan_id');
        $query = $this->db->query("select no_seleksi from hr_penempatan where penempatan_id='$penempatan_id'");
        foreach ($query->result_array() as $row) {
            $no_seleksi = $row['no_seleksi'];
        }

        $data_2['status_seleksi'] = 'Penolakan Penempatan';
        $this->db->where('no_seleksi', $no_seleksi);
        $this->db->update('hr_seleksi_pegawai', $data_2);
    }

    function addHistoryKomentar() {
        $grup_id = $this->session->userdata('kd_roles');
        $query = $this->db->query("select grup_deskripsi from m_grup where grup_id='$grup_id'");
        foreach ($query->result_array() as $row) {
            $desk = $row['grup_deskripsi'];
        }
        $nik = $this->session->userdata('nik_session');

        $data['cuti_id'] = $this->input->post('penempatan_id');
        $data['add_by'] = $nik . '/' . $desk;
        $data['tgl_by'] = date('Y-m-d');
        $data['detail'] = $this->input->post('komentar');
        $data['id_workflow'] = '13';
        $this->db->insert('hr_komentar', $data);
    }

    /*    function switch_step() {
      $penempatan_id = $this->input->post('penempatan_id');
      $data['status'] = 'DONE';
      $this->db->where('kd_apr_penempatan', $this->input->post('kd_appr_penempatan'));
      $this->db->update('hr_apr_penempatan', $data);


      $data['status'] = 'ENABLED';
      $apr_penempatan_id_next = $this->input->post('kd_appr_penempatan') + 1;
      $this->db->where('penempatan_id', $penempatan_id);
      $this->db->where('kd_apr_penempatan', $apr_penempatan_id_next);
      $this->db->update('hr_apr_penempatan', $data);


      $cekStep = $this->db->query("select * from hr_apr_penempatan where penempatan_id='$penempatan_id'
      and kd_apr_penempatan='$apr_penempatan_id_next' and status='ENABLED'
      ");

      //================ UPDATE FINISHED JIKA MEMANG SUDAH TIDAK ADA NODE LAGI ===============
      if ($cekStep->num_rows() == 0) {
      $data['status'] = 'Finished';
      $this->db->where('penempatan_id', $this->input->post('penempatan_id'));
      $this->db->update('hr_penempatan', $data);
      }
      //================ END UPDATE FINISHED JIKA MEMANG SUDAH TIDAK ADA NODE LAGI ===============
      } */

    /*    function switch_step_returned() {
      $penempatan_id = $this->input->post('penempatan_id');
      $data['status'] = 'DISABLED';
      $this->db->where('penempatan_id', $this->input->post('penempatan_id'));
      $this->db->update('hr_apr_penempatan', $data);
      } */

    /*    function cekStatusFlowPenempatan() {
      $nik = $this->session->userdata('nik_session');
      $query = $this->db->query("select nik from hr_apr_penempatan where nik like '%00 00 022%' and status='ENABLED'");

      return $query;
      } */

    function load_data_persetujuan_penempatan($page, $jum_per_page) {

        $nik_x = $this->session->userdata('nik_session');
        $grup_id = $this->session->userdata('kd_roles');
        $query = $this->db->query("select 
                hr_penempatan.no_seleksi,
                hr_penempatan.nama,
                hr_penempatan.jabatan,
                hr_penempatan.status,
                hr_penempatan.status_penempatan,
                hr_penempatan.seleksi_pegawai_id,                 
                hr_ref_cabang.nama_cabang,
                hr_ref_jabatan.nama_jabatan,
                hr_ref_unit_kerja.nama_unit_kerja, 
                hr_penempatan.penempatan_id
                from hr_penempatan
                left join hr_ref_cabang
                on 
                hr_penempatan.kd_cabang=hr_ref_cabang.kd_cabang
                left join hr_ref_unit_kerja
                on 
                hr_penempatan.kd_unit_kerja=hr_ref_unit_kerja.kd_unit_kerja
                left join hr_ref_jabatan
                on 
                hr_penempatan.jabatan =hr_ref_jabatan.kd_jabatan
                where hr_penempatan.status ='$grup_id'
                ");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    /*    function count_data_persetujuan_penempatan() {

      $nik_x = $this->session->userdata('nik_session');

      $query = $this->db->query("select
      hac.penempatan_id,hc.no_seleksi,hp.nama,hu.nama_unit_kerja,
      hrc.nama_cabang,
      hac.step,
      hac.kd_apr_penempatan,
      hrj.nama_jabatan,hac.nik

      from hr_apr_penempatan as hac left join hr_penempatan as hc
      on hac.penempatan_id=hc.penempatan_id
      left join hr_pegawai as hp
      on hp.nik=hac.nik
      LEFT JOIN hr_ref_unit_kerja as hu
      on hc.kd_unit_kerja=hu.kd_unit_kerja
      LEFT JOIN hr_ref_cabang as hrc
      on hc.kd_cabang=hrc.kd_cabang
      LEFT JOIN hr_ref_jabatan hrj
      on hc.jabatan=hrj.kd_jabatan
      where hac.nik='$nik_x' and hac.status='ENABLED'
      ORDER by hc.penempatan_id DESC

      ");

      if ($query->num_rows() > 0) {
      foreach ($query->result() as $data) {
      $hasil[] = $data;
      }
      //return $hasil;
      }

      return $query->num_rows();
      } */

    function load_jabatan($nik) {
        $query = $this->db->query("select to_char(tanggal_sk, 'dd-mm-yyyy') as tanggal_sk, hr_jabatan.no_sk, 
        to_char(tanggal_mulai_berlaku, 'dd-mm-yyyy') as tanggal_mulai_berlaku, hr_ref_cabang.nama_cabang,
        hr_ref_jabatan.nama_jabatan, hr_ref_unit_kerja.nama_unit_kerja, hr_jabatan.kd_level
        FROM hr_jabatan	
        LEFT JOIN hr_ref_jabatan ON hr_jabatan.kd_jabatan=hr_ref_jabatan.kd_jabatan
        LEFT JOIN hr_ref_unit_kerja ON hr_jabatan.kd_unit_kerja=hr_ref_unit_kerja.kd_unit_kerja
        LEFT JOIN hr_ref_cabang ON hr_jabatan.kd_cabang=hr_ref_cabang.kd_cabang    
        where hr_jabatan.nik = '$nik' ORDER by hr_jabatan.tanggal_sk ");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    function load_pangkat($nik) {
        $query = $this->db->query("select kd_level, no_sk, to_char(tanggal_sk, 'dd-mm-yyyy') as tanggal_sk,
                nama_unit_kerja, nama_cabang, nama_jabatan, level_lama, keterangan
                FROM hr_pangkat
                LEFT JOIN hr_ref_jabatan ON hr_pangkat.kd_jabatan = hr_ref_jabatan.kd_jabatan
                LEFT JOIN hr_ref_cabang ON hr_pangkat.kd_cabang = hr_ref_cabang.kd_cabang
                LEFT JOIN hr_ref_unit_kerja ON hr_pangkat.kd_unit_kerja = hr_ref_unit_kerja.kd_unit_kerja
                WHERE nik='$nik' AND status = 'T' ORDER by hr_pangkat.tanggal_sk");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    function load_data_penempatan() {

        $nik_x = $this->session->userdata('nik_session');
        $grup_id = $this->session->userdata('kd_roles');
        $unit_kerja = $this->session->userdata('kd_unit_kerja');

        $filter = '';
        if ($grup_id != 4):
            $filter.=" and hr_penempatan.kd_unit_kerja='$unit_kerja'";
        endif;


        $query = $this->db->query("select 
                hr_penempatan.no_seleksi,
                hr_penempatan.nama,
                hr_penempatan.jabatan,
                hr_penempatan.status,
                hr_penempatan.status_penempatan,
                hr_penempatan.seleksi_pegawai_id,                 
                hr_ref_cabang.nama_cabang,
                hr_ref_jabatan.nama_jabatan,
                hr_ref_unit_kerja.nama_unit_kerja, 
                hr_penempatan.penempatan_id,
				hr_seleksi_pegawai.gender,
				hr_seleksi_pegawai.permintaan_id,
				hr_seleksi_pegawai.status_seleksi
				
				
                from hr_penempatan
                left join hr_ref_cabang
                on 
                hr_penempatan.kd_cabang=hr_ref_cabang.kd_cabang
                left join hr_ref_unit_kerja
                on 
                hr_penempatan.kd_unit_kerja=hr_ref_unit_kerja.kd_unit_kerja
                left join hr_ref_jabatan
                on 
                hr_penempatan.jabatan =hr_ref_jabatan.kd_jabatan
				left join hr_seleksi_pegawai on hr_seleksi_pegawai.no_seleksi=hr_penempatan.no_seleksi
				where hr_seleksi_pegawai.status_seleksi='Lulus'
                limit $this->limit offset $this->offset 
                ");


        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    function count_data_penempatan() {


        $nama = $this->input->post('nama');
        $filter = "";

        if ($nama != '') {
            $filter = $filter . " and hr_penempatan.nama like '%$nama%'";
        }

        $query = $this->db->query("select 
                count(1) as jumlah
                from hr_penempatan
                left join hr_ref_cabang
                on 
                hr_penempatan.kd_cabang=hr_ref_cabang.kd_cabang
                left join hr_ref_unit_kerja
                on 
                hr_penempatan.kd_unit_kerja=hr_ref_unit_kerja.kd_unit_kerja
                left join hr_ref_jabatan
                on 
                hr_penempatan.jabatan =hr_ref_jabatan.kd_jabatan
				left join hr_seleksi_pegawai on hr_seleksi_pegawai.no_seleksi=hr_seleksi_pegawai.no_seleksi
				where hr_seleksi_pegawai.status_seleksi='Lulus'
                ");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil = $data->jumlah;
            }
            return $hasil;
        }
    }

    function updateJmlPermintaan() {
        $permintaan_id = $this->input->post('txtid_peminta');
        $penempatan_id = $this->input->post('txtid_penempatan');



        $query = $this->db->query("select sisa_formasi from hr_permintaan_pegawai 
                where permintaan_pegawai_id='$permintaan_id'");
        $baris = $query->row();
        $jml_formasi = $baris->sisa_formasi - 1;
        $data['sisa_formasi'] = $jml_formasi;

        $this->db->where('permintaan_pegawai_id', $permintaan_id);
        $this->db->update('hr_permintaan_pegawai', $data);

        $data2['status_penempatan'] = 1;
        $this->db->where('penempatan_id', $penempatan_id);
        $this->db->update('hr_penempatan', $data2);
    }

    function tambah_pegawai_baru_mdl_new() {
        $tgl_masuk_pegawai = '1900-01-01';
        $apr_by = $this->session->userdata('nik_session');
        $apr_date = date("Y-m-d");
        $ptkp = '';
        $k_natura = '';
        $jumlah_anak = '';
        $status_keluarga = '';
        /* hr_pegawai */
        $nama_pegawai = $this->input->post('txtnama_pegawai');
        $nik = $this->input->post('txtnik_hide');
        $gelar_depan = $this->input->post('txt_gelar_depan');
        $gelar_belakang_pegawai = $this->input->post('txt_gelar_belakang_pegawai');
        $tmp_lahir = $this->input->post('txt_tmp_lahir');
        $gol_darah = $this->input->post('select_gol_darah');
        $tgl_lahir = $this->input->post('txt_tgl_lahir');
        $gender = $this->input->post('select_jenis_kelamin_pegawai');
        $select_status_kawin_pegawai = $this->input->post('select_status_kawin_pegawai');
        $agama_pagawai = $this->input->post('agama_pagawai');
        $suku_marga_pegawai = $this->input->post('txt_suku_marga_pegawai');
        $alamat_pegawai = $this->input->post('txt_alamat_pegawai');
        $rtrw_pegawai = $this->input->post('txt_rtrw_pegawai');
        $desa_kelurahan_pegawai = $this->input->post('txt_desa_kelurahan_pegawai');
        $kecamatan = $this->input->post('txt_kecamatan');
        $kab_or_kota_pegawai = $this->input->post('txt_kab_or_kota_pegawai');
        $kode_pos = $this->input->post('txt_kode_pos');
        $prov_pegawai = $this->input->post('txt_prov_pegawai');
        $telf_pegawai = $this->input->post('txt_telf_pegawai');
        $hp_pegawai = $this->input->post('txt_hp_pegawai');



        $email = $this->input->post('txtemail');
        $j_pegawai = $this->input->post('status_kerja_pegawai');
        $status_pegawai = $this->input->post('status_aktif_pegawai');
        $kebangsaan = $this->input->post('txt_kebangsaan');
        $alamat = $this->input->post('txt_alamat_pegawai');
        $pasangan_nama = $this->input->post('txtnama_istri');
        $pasangan_tgl_lahir = $this->input->post('txt_tgl_lahir_istri');
        $pasangan_agama = $this->input->post('txt_agama_istri');
        $pasangan_pendidikan = $this->input->post('select_pendidikan_istri');
        $pasangan_pekerjaan = $this->input->post('select_pekerjaan_istri');
        $pasangan_status = $this->input->post('select_status_istri');
        $pasangan_tgl_nikah = $this->input->post('select_tgl_pernikahan_istri');
        $pasangan_tmp_lahir = $this->input->post('txt_tmpt_lahir_istri');

        $ayah_nama = $this->input->post('ayah_nama');
        $ayah_tmp_lahir = $this->input->post('ayah_tmplahir');

        $ayah_agama = $this->input->post('ayah_agama');
        $ayah_pendidikan = $this->input->post('ayah_pendidikan');
        $ayah_pekerjaan = $this->input->post('ayah_pekerjaan');
        $ayah_status = $this->input->post('ayah_status');
        $ayah_alamat = $this->input->post('ayah_alamat');
        $ayah_tgl_lahir = $this->input->post('ayah_tgllahir');

        $ibu_nama = $this->input->post('ibu_nama');
        $ibu_tmp_lahir = $this->input->post('ibu_tmplahir');
        $ibu_agama = $this->input->post('ibu_agama');
        $ibu_pendidikan = $this->input->post('ibu_pendidikan');
        $ibu_pekerjaan = $this->input->post('ibu_pekerjaan');
        $ibu_status = $this->input->post('ibu_status');
        $ibu_alamat = $this->input->post('ibu_alamat');
        $ibu_tgl_lahir = $this->input->post('ibu_tgllahir');

        $ayah_mertua_nama = $this->input->post('mertua_laki_nama');
        $ayah_mertua_tmp_lahir = $this->input->post('mertua_laki_tmp_lahir');
        $ayah_mertua_agama = $this->input->post('select_agama_mertua_laki');
        $ayah_mertua_pendidikan = $this->input->post('mertua_laki_pendidikan');
        $ayah_mertua_pekerjaan = $this->input->post('mertua_laki_pekerjaan');
        $ayah_mertua_status = $this->input->post('mertua_laki_status');
        $ayah_mertua_alamat = $this->input->post('mertua_laki_alamat');
        $ayah_mertua_tgl_lahir = $this->input->post('mertua_laki_tgl_lahir');

        $ibu_mertua_nama = $this->input->post('mertua_perempuan_nama');
        $ibu_mertua_tmp_lahir = $this->input->post('mertua_perempuan_tmp_lahir');
        $ibu_mertua_agama = $this->input->post('mertua_perempuan_agama');
        $ibu_mertua_pendidikan = $this->input->post('mertua_perempuan_pendidikan');
        $ibu_mertua_pekerjaan = $this->input->post('mertua_perempuan_pekerjaan');
        $ibu_mertua_status = $this->input->post('mertua_perempuan_status');
        $ibu_mertua_alamat = $this->input->post('ibu_mertua_alamat');
        $ibu_mertua_tgl_lahir = $this->input->post('mertua_perempuan_tgl_lahir');

        /* mutasi */

        $kd_jabatan = $this->input->post('select_jabatan');
        $kd_unit_kerja = $this->input->post('select_unit_kerja');
        $kd_lokasi = $this->input->post('select_lokasi_kerja');


        /* Peminta */
        $peminta = $this->input->post('peminta');
        /* Pangkat */
        $select_pangkat = $this->input->post('select_pangkat');
        /* Seleksi */
        $noid_seleksi = $this->input->post('no_seleksi');
        /* Date Properties */
        $tgl_capeg = $this->input->post('txt_tgl_capeg');
        $tgl_masuk_pegawai = $this->input->post('txt_tgl_masuk_pegawai');
        $no_sk = $this->input->post('txt_nomor_sk');
        $tgl_sk = $this->input->post('txt_tgl_sk');
        $no_rek = $this->input->post('no_rek_peg');

        $npwp = $this->input->post('npwp');
        $no_ktp = $this->input->post('no_ktp');
        $no_jamsostek = $this->input->post('no_jamsostek');
        //$pasangan_tgl_lahir=$this->input->post('txt_tgl_lahir_istri');
        //$pasangan_tgl_nikah=$this->input->post('select_tgl_pernikahan_istri');        
        //$ayah_tgl_lahir=$this->input->post('ayah_tgllahir');
        //$ibu_tgl_lahir=$this->input->post('ibu_tgllahir');
        //$ayah_mertua_tgl_lahir=$this->input->post('mertua_laki_tgl_lahir');
        //$ibu_mertua_tgl_lahir=$this->input->post('mertua_perempuan_tgl_lahir');
        //Data User //
        $roles_id = $this->input->post('grup_roles');
        $count_jumlah_anak = $this->db->query("select count(nik) as jumlah_anak from hr_mpeg_anak where nik='$nik'");
        foreach ($count_jumlah_anak->result_array() as $row) {
            $jumlah_anak = $row['jumlah_anak'];
        }
        if ($jumlah_anak > 3) {
            $jumlah_anak = 3;
        }



        if ($gender == 'P' and $select_pangkat == 0) {
            $k_natura = "b";
            $ptkp = "tk";
        }
        if ($gender == 'L' and $select_pangkat == 0) {
            $k_natura = "bl";
            $ptkp = "tk";
        }

        if ($select_status_kawin_pegawai == 'BK' and $jumlah_anak == 0 and $gender == 'P' and $select_pangkat > 0) {
            $k_natura = "b";
            $ptkp = "tk";
        }
        if ($select_status_kawin_pegawai == 'BK' and $jumlah_anak == 0 and $gender == 'L' and $select_pangkat > 0) {
            $k_natura = "k0";
            $ptkp = "tk";
        }

        if ($select_status_kawin_pegawai == 'K' and $jumlah_anak == 0 and $gender == 'P' and $select_pangkat > 0) {
            $k_natura = "b";
            $ptkp = "k";
        }
        if ($select_status_kawin_pegawai == 'K' and $jumlah_anak == 0 and $gender == 'L' and $select_pangkat > 0) {
            $k_natura = "k0";
            $ptkp = "k";
        }


        if ($select_status_kawin_pegawai == 'K' and $gender == 'L' and $jumlah_anak > 0 and $select_pangkat > 0) {
            $k_natura = "k" . $jumlah_anak;
            $ptkp = "k";
        }
        if ($select_status_kawin_pegawai == 'K' and $gender == 'P' and $jumlah_anak > 0 and $select_pangkat > 0) {
            $k_natura = "kts" . $jumlah_anak;
            $ptkp = "k";
        }
        if ($select_status_kawin_pegawai == 'BK' and $gender == 'L' and $jumlah_anak > 0 and $select_pangkat > 0) {
            $k_natura = "k" . $jumlah_anak;
            $ptkp = "k";
        }
        if ($select_status_kawin_pegawai == 'BK' and $gender == 'P' and $jumlah_anak > 0 and $select_pangkat > 0) {
            $k_natura = "kts" . $jumlah_anak;
            $ptkp = "k";
        }


        if ($gender == 'L' and $select_status_kawin_pegawai == 'K') {
            $status_keluarga = 'X/' . $jumlah_anak;
        }
        if ($gender == 'P' and $select_status_kawin_pegawai == 'K') {
            $status_keluarga = 'Y/' . $jumlah_anak;
        }
        if ($gender == 'P' and $select_status_kawin_pegawai == 'D') {
            $status_keluarga = 'X/' . $jumlah_anak;
        }
        if ($gender == 'P' and $select_status_kawin_pegawai == 'J') {
            $status_keluarga = 'Y/' . $jumlah_anak;
        }

        if ($gender == 'L' and $select_pangkat == '0' and $select_status_kawin_pegawai == 'BK') {
            $status_keluarga = 'BL';
        }
        if ($gender == 'P' and $select_pangkat == '0' and $select_status_kawin_pegawai == 'BK') {
            $status_keluarga = 'BP';
        }

        if ($gender == 'L' and $select_status_kawin_pegawai == 'BK') {
            $status_keluarga = 'BL';
        }
        if ($gender == 'P' and $select_status_kawin_pegawai == 'BK') {
            $status_keluarga = 'BP';
        }

        //echo "PTKP=" . $ptkp . "<br>";
        //echo "NATURA=" . $k_natura . "<br>";
        //echo "STATUS KELUARGA=" . $status_keluarga . "<br>";


        switch ($gender) {
            case 'L':
                $gender = "Laki-laki";
                break;
            case 'P':
                $gender = "Perempuan";
                break;
        }

        switch ($select_status_kawin_pegawai) {
            case 'K':
                $select_status_kawin_pegawai = "Kawin";
                break;
            case 'BK':
                $select_status_kawin_pegawai = "Belum Kawin";
                break;
            case 'D':
                $select_status_kawin_pegawai = "Duda";
                break;
            case 'J':
                $select_status_kawin_pegawai = "Janda";
                break;
        }

        $tgl_lahir = date('Y-m-d', strtotime($tgl_lahir));
        $tgl_masuk_pegawai = date('Y-m-d', strtotime($tgl_masuk_pegawai));
        $tgl_capeg = date('Y-m-d', strtotime($tgl_capeg));
        $tgl_sk = date('Y-m-d', strtotime($tgl_sk));
        $pasangan_tgl_lahir = date('Y-m-d', strtotime($pasangan_tgl_lahir));
        $ibu_mertua_tgl_lahir = date('Y-m-d', strtotime($ibu_mertua_tgl_lahir));
        $ibu_tgl_lahir = date('Y-m-d', strtotime($ibu_tgl_lahir));
        $ayah_tgl_lahir = date('Y-m-d', strtotime($ayah_tgl_lahir));
        $ayah_mertua_tgl_lahir = date('Y-m-d', strtotime($ayah_mertua_tgl_lahir));
        $pasangan_tgl_nikah = date('Y-m-d', strtotime($pasangan_tgl_nikah));

        $data_pegawai = array(
            'nik' => $nik,
            'nama' => $nama_pegawai,
            'kd_level' => $select_pangkat,
            'status_aktif' => 'Aktif',
            'tgl_capeg' => $tgl_capeg,
            'tgl_masuk' => $tgl_masuk_pegawai,
            'kd_unit_kerja' => $kd_unit_kerja,
            'kd_jabatan' => $kd_jabatan,
            'tanggal_sk' => $tgl_sk,
            'no_sk' => $no_sk,
            'status_kerja' => $j_pegawai,
            'kd_cabang' => $kd_lokasi,
            'apr' => 'T',
            'apr_by' => $apr_by,
            'apr_tanggal' => $apr_date
        );

        $data_detail = array(
            'nik' => $nik,
            'nama' => $nama_pegawai,
            'tmp_lahir' => $tmp_lahir,
            'gender' => $gender,
            'g_depan' => $gelar_depan,
            'g_belakang' => $gelar_belakang_pegawai,
            'gender' => $gender,
            'tgl_lahir' => $tgl_lahir,
            'kebangsaan' => $kebangsaan,
            'foto' => '',
            'alamat' => $alamat,
            'rtrw' => $rtrw_pegawai,
            'kelurahan' => $desa_kelurahan_pegawai,
            'kecamatan' => $kecamatan,
            'kota' => $kab_or_kota_pegawai,
            'propinsi' => $prov_pegawai,
            'kd_pos' => $kode_pos,
            'telpon' => $telf_pegawai,
            'email' => $email,
            'mobile' => $hp_pegawai,
            'suku' => $suku_marga_pegawai,
            'agama' => $agama_pagawai,
            'status_kawin' => $select_status_kawin_pegawai,
            'pasangan_nama' => $pasangan_nama,
            'pasangan_pekerjaan' => $pasangan_pekerjaan,
            'pasangan_tmp_lahir' => $pasangan_tmp_lahir,
            'pasangan_agama' => $pasangan_agama,
            'pasangan_pendidikan' => $pasangan_pendidikan,
            'pasangan_status' => $pasangan_status,
            'pasangan_tgl_lahir' => $pasangan_tgl_lahir,
            'pasangan_tgl_nikah' => $pasangan_tgl_nikah,
            'ayah_nama' => $ayah_nama,
            'ayah_alamat' => $ayah_alamat,
            'ayah_pekerjaan' => $ayah_pekerjaan,
            'ayah_tmp_lahir' => $ayah_tmp_lahir,
            'ayah_agama' => $ayah_agama,
            'ayah_pendidikan' => $ayah_pendidikan,
            'ayah_status' => $ayah_status,
            'ibu_nama' => $ibu_nama,
            'ibu_alamat' => $ibu_alamat,
            'ibu_pekerjaan' => $ibu_pekerjaan,
            'ibu_tmp_lahir' => $ibu_tmp_lahir,
            'ibu_agama' => $ibu_agama,
            'ibu_pendidikan' => $ibu_pendidikan,
            'ibu_status' => $ibu_status,
            'mertua_laki_nama' => $ayah_mertua_nama,
            'mertua_laki_alamat' => $ayah_mertua_alamat,
            'mertua_laki_pekerjaan' => $ayah_mertua_pekerjaan,
            'mertua_laki_tmp_lahir' => $ayah_mertua_tmp_lahir,
            'mertua_laki_agama' => $ayah_mertua_agama,
            'mertua_laki_pendidikan' => $ayah_mertua_pendidikan,
            'mertua_laki_status' => $ayah_mertua_status,
            'mertua_laki_tgl_lahir' => $ayah_mertua_tgl_lahir,
            'mertua_perempuan_nama' => $ibu_mertua_nama,
            'mertua_perempuan_alamat' => $ibu_mertua_alamat,
            'mertua_perempuan_pekerjaan' => $ibu_mertua_pekerjaan,
            'mertua_perempuan_tmp_lahir' => $ibu_mertua_tmp_lahir,
            'mertua_perempuan_tgl_lahir' => $ibu_mertua_tgl_lahir,
            'mertua_perempuan_agama' => $ibu_mertua_agama,
            'mertua_perempuan_pendidikan' => $ibu_mertua_pendidikan,
            'mertua_perempuan_status' => $ibu_mertua_status,
            /* 'gol_darah'=>$gol_darah, */
            'pasangan_tgl_nikah' => $pasangan_tgl_nikah,
            'pasangan_tgl_lahir' => $pasangan_tgl_lahir,
            'ayah_tgl_lahir' => $ayah_tgl_lahir,
            'ibu_tgl_lahir' => $ibu_tgl_lahir,
            'mertua_laki_tgl_lahir' => $ayah_mertua_tgl_lahir,
            'mertua_perempuan_tgl_lahir' => $ibu_mertua_tgl_lahir,
            'nik_tmp' => '',
            'apr' => 'T',
            'rekening' => $no_rek,
            'npwp' => $npwp,
            'no_ktp' => $no_ktp,
            'no_jamsostek' => $no_jamsostek
        );

        $potongan_gaji = $this->db->query("select * from hr_ref_potongan_gaji");
        foreach ($potongan_gaji->result_array() as $row) {
            $data_potongan_gaji = array
                (
                'nik' => $nik,
                'kd_potongan_gaji' => $row['kd_potongan_gaji'],
                'nominal' => $row['nominal']
            );

            $this->db->insert('hr_setup_potongan_gaji', $data_potongan_gaji);
        }


        $data_mutasi = array(
            'nik' => $nik,
            'kd_unit_kerja' => $kd_unit_kerja,
            'kd_jabatan' => $kd_jabatan,
            'kd_level' => $select_pangkat,
            'kd_cabang' => $kd_lokasi,
            'apr_by' => $apr_by,
            'apr_tanggal' => $apr_date,
            'tanggal_sk' => $tgl_sk,
            'no_sk' => $no_sk,
            'tanggal_mulai_berlaku' => $tgl_sk,
            'status' => 'T'
        );

        $data_pangkat = array(
            'nik' => $nik,
            'kd_unit_kerja' => $kd_unit_kerja,
            'kd_jabatan' => $kd_jabatan,
            'kd_level' => $select_pangkat,
            'kd_cabang' => $kd_lokasi,
            'apr_by' => $apr_by,
            'tanggal_sk' => $tgl_sk,
            'tanggal_mulai_berlaku' => $tgl_sk,
            'no_sk' => $no_sk,
            'apr_tanggal' => $apr_date,
            'status' => 'T'
        );


        $nilai = $select_pangkat * 10;

        $data_penilaian = array(
            'nik' => $nik,
            'keterangan' => 'Pegawai Baru',
            'aspek' => 'Pegawai Baru',
            'nilai' => $nilai
        );


        $data_setup_tunjangan = array(
            'nik' => $nik,
            'jumlah_anak' => $jumlah_anak,
            'persentase_gaji' => '100',
            'nilai_kontrak' => '0',
            'status_keluarga' => $status_keluarga,
            'status_ptkp' => $ptkp,
            'kelompok_natura' => $k_natura
        );

        /* $data_user = array
          (
          'nik' => $nik,
          'user_name' => $nik,
          'user_password' => md5($nik),
          'grup_id' => $roles_id
          );
         */
        $data_tunjangan_khusus = array(
            'nik' => $nik,
            'tunjangan_jabatan' => '1',
        );

        $this->db->trans_start();
        $this->db->insert('hr_pegawai', $data_pegawai);
        $this->db->insert('hr_pegawai_detail', $data_detail);
        $this->db->insert('hr_jabatan', $data_mutasi);
        $this->db->insert('hr_setup_tunjangan_sejahtera', $data_setup_tunjangan);
        $this->db->insert('hr_pangkat', $data_pangkat);
        // $this->db->insert('m_user', $data_user);
        $this->db->insert('hr_tunjangan_khusus', $data_tunjangan_khusus);
        $this->db->query("update hr_seleksi_pegawai set status_seleksi='Diperkerjakan' where no_seleksi='$noid_seleksi'");
        $this->db->trans_complete();

        $q0 = $this->db->query("select user_id from m_user where nik='$nik'");
        foreach ($q0->result_array() as $row) {
            $user_id = $row['user_id'];
        }

        $q_m_roles = $this->db->query("select * from m_roles where grup_id='$roles_id'");
        foreach ($q_m_roles->result_array() as $row) {
            $id_menu = $row['menu_id'];
            $id_grup = $row['grup_id'];

            $is_view = $row['is_view'];
            $is_add = $row['is_add'];
            $is_edit = $row['is_edit'];
            $is_delete = $row['is_delete'];
            // $is_export = $row['is_export'];
            $is_print = $row['is_print'];
            $is_approve = $row['is_approve'];
            if ($is_view == 't') {
                $is_view = 'y';
            } else {
                $is_view = 'f';
            }
            if ($is_add == 't') {
                $is_add = 'y';
            } else {
                $is_add = 'f';
            }
            if ($is_edit == 't') {
                $is_edit = 'y';
            } else {
                $is_edit = 'f';
            }
            if ($is_delete == 't') {
                $is_delete = 'y';
            } else {
                $is_delete = 'f';
            }
            /* if ($is_export == 't') {
              $is_export = 'y';
              } else {
              $is_export = 'f';
              } */
            if ($is_print == 'T') {
                $is_print = 'y';
            } else {
                $is_print = 'f';
            }
            if ($is_approve == 'T') {
                $is_approve = 'y';
            } else {
                $is_approve = 'f';
            }


            $this->db->query(
                    "insert into m_user_akses (kd_menu,kd_grup,userid,is_view,is_add,is_edit,is_del,is_print,is_apr)  values ('$id_menu','$roles_id','$user_id','$is_view','$is_add','$is_edit','$is_delete','$is_print','$is_approve')");
        }

        /* ---- Insert kompetensi ---- */

        $query = $this->db->query("select id_kompetensi from hr_kompetensi where id_kompetensi != 0 ")->result();
        foreach ($query as $data) {
            $id_kompetensi = $this->input->post('ck' . $data->id_kompetensi);

            if ($id_kompetensi == 'Y') {
                $id_kompetensi = $data->id_kompetensi;

                $data_komp_pegawai = array(
                    'nik' => $nik,
                    'jenis_kompetensi' => '2',
                    'apr' => 'T',
                    'id_kompetensi' => $id_kompetensi
                );
                $this->db->insert('hr_kompetensi_pegawai', $data_komp_pegawai);
            }
        }
        /* ----------------------- */
    }

//end of 

    function load_ref_jenjang() {
        $q = $this->db->query("select jenjang  from hr_ref_jenjang");
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    function load_ref_level() {
        $q = $this->db->query("select kd_level from hr_ref_level 
            where length(kd_level) < 3 AND kd_level != 'ho' 
            order by   
			urut   
			ASC");
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    function load_ref_level_2() {
        $q = $this->db->query("select kd_level from hr_ref_level 
            where length(kd_level) > 2 or  kd_level = 'ho' 
            order by   
			urut   
			ASC");
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    function load_data_anak_edit($anak_id = 0) {

        $this->db->select('*');
        $this->db->where('mpeg_anak_id', $anak_id);
        $query = $this->db->get('hr_mpeg_anak');
        return $query;
    }

    function load_data_pendidikan_edit($sekolah_id = 0) {

        $this->db->select('*');
        $this->db->where('mpeg_sekolah_id', $sekolah_id);

        $query = $this->db->get('hr_mpeg_sekolah');
        return $query;
    }

    function load_data_kursus_edit($kursus_id = 0) {

        $this->db->select('*');
        $this->db->where('mpeg_khusus_id', $kursus_id);
        $query = $this->db->get('hr_mpeg_kursus');
        return $query;
    }

    function load_data_karir_edit($karir_id = 0) {

        $this->db->select('*');
        $this->db->where('mpeg_karir_id', $karir_id);
        $query = $this->db->get('hr_mpeg_karir');
        return $query;
    }

    function load_edit_kompetensi($kompetensi_id = 0) {
        $query = $this->db->query("select hr_kompetensi_pegawai.id,hr_kompetensi_pegawai.nik,hr_kompetensi_pegawai.jenis_kompetensi,
            hr_kompetensi_pegawai.keterangan,hr_kompetensi_pegawai.id_kompetensi,hr_kompetensi.nama_kompetensi,hr_kompetensi_pegawai.kompetensi from hr_kompetensi_pegawai 
            left join hr_kompetensi 
            on hr_kompetensi_pegawai.jenis_kompetensi=hr_kompetensi.id_kompetensi
            where hr_kompetensi_pegawai.id='$kompetensi_id'");

        return $query;
    }

    /* -------- ADDED ------ */

    function getComboKompetensi($no) {

        $q = $this->db->query("select *  from hr_kompetensi_pelamar 
            left join hr_kompetensi 
            on hr_kompetensi_pelamar.id_kompetensi=hr_kompetensi.id_kompetensi
            where id_pelamar ='$no' ");
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    /* ------------------- */

    function load_get_data_penempatan() {
        $nik = $this->input->post('nik');
        $filter = "";
        if ($nik != '') {
            $filter = $filter . " and hr_penempatan.no_seleksi like '%$nik%'";
        }

        $nik2 = $this->input->post('select_nama');
        if ($nik2 != '') {
            $filter = $filter . " and hr_penempatan.nama like '%$nik2%'";
        }
        $cabang = $this->input->post('select_cabang');
        if ($cabang != '') {
            $filter = $filter . " and hr_penempatan.kd_cabang = '$cabang'";
        }
        $unit_kerja = $this->input->post('select_unit_kerja');
        if ($unit_kerja != '') {
            $filter = $filter . " and hr_penempatan.kd_unit_kerja = '$unit_kerja'";
        }

        $q = $this->db->query("select hr_penempatan.nama,
                hr_penempatan.no_seleksi,
                hr_penempatan.seleksi_pegawai_id,
                hr_penempatan.no_sk,
                hr_penempatan.tgl_sk,
                hr_penempatan.status,
                hr_ref_jabatan.nama_jabatan,
                hr_ref_unit_kerja.nama_unit_kerja,
                hr_ref_cabang.nama_cabang
                from hr_penempatan
                left join hr_ref_cabang
                on hr_penempatan.kd_cabang=hr_ref_cabang.kd_cabang
                left join hr_ref_unit_kerja
                on hr_penempatan.kd_unit_kerja=hr_ref_unit_kerja.kd_unit_kerja
                left join hr_ref_jabatan
                on hr_penempatan.jabatan=hr_ref_jabatan.kd_jabatan
                where     hr_penempatan.nama !=''  $filter
                LIMIT $this->limit OFFSET $this->offset
                ");

        if ($q->num_rows() > 0) {
            foreach ($q->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    function count_penempatan() {
        $filter = "";
        $nik = $this->input->post('nik');
        if ($nik != '') {
            $filter = $filter . " and hr_penempatan.no_seleksi like '%$nik%'";
        }

        $nik2 = $this->input->post('select_nama');
        if ($nik2 != '') {
            $filter = $filter . " and hr_penempatan.nama like '%$nik2%'";
        }
        $cabang = $this->input->post('select_cabang');
        if ($cabang != '') {
            $filter = $filter . " and hr_penempatan.kd_cabang = '$cabang'";
        }
        $unit_kerja = $this->input->post('select_unit_kerja');
        if ($unit_kerja != '') {
            $filter = $filter . " and hr_penempatan.kd_unit_kerja = '$unit_kerja'";
        }

        $q = $this->db->query("select count(1) as jumlah from hr_penempatan where     hr_penempatan.nama !=''  $filter ");
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $data) {
                $hasil = $data->jumlah;
            }
            return $hasil;
        }
    }

    function load_sekolah_pelamar($noid) {
        $q = $this->db->query("select * from hr_seleksi_pegawai where seleksi_pegawai_id='$noid'");
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    function getReport() {
        $hasil = array();
        $query = $this->db->query(" SELECT hr_pegawai.nik, hr_pegawai.nama, q1.kd_cabang_induk, hr_ref_cabang.nama_cabang, max(hr_ref_jenjang.jenjang_id) AS sekolah, hr_pegawai_detail.gender
                                    FROM hr_pegawai
                                    LEFT JOIN hr_mpeg_sekolah ON hr_pegawai.nik = hr_mpeg_sekolah.nik
                                    LEFT JOIN hr_ref_jenjang ON hr_mpeg_sekolah.jenjang = hr_ref_jenjang.jenjang
                                    LEFT JOIN (select hr_pegawai.nik, hr_ref_cabang.kd_cabang_induk, hr_ref_cabang.nama_cabang FROM hr_pegawai LEFT JOIN hr_ref_cabang ON hr_ref_cabang.kd_cabang = hr_pegawai.kd_cabang) as q1
                                    ON q1.nik = hr_pegawai.nik
                                    LEFT JOIN hr_ref_cabang ON q1.kd_cabang_induk = hr_ref_cabang.kd_cabang
                                    LEFT JOIN hr_pegawai_detail ON hr_pegawai.nik = hr_pegawai_detail.nik
                                    WHERE hr_pegawai.status_aktif = 'Aktif' AND hr_pegawai.kd_level != 'ho' AND length(kd_level) < 3
                                    GROUP BY hr_pegawai.nik, hr_pegawai.nama, hr_ref_cabang.nama_cabang, q1.kd_cabang_induk, hr_pegawai_detail.gender
                                    ORDER BY q1.kd_cabang_induk, sekolah, hr_pegawai.nik");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data)
                $hasil[] = $data;
        }
        return $hasil;
    }

    function countUltah() {
        $q = $this->db->query("select hr_pegawai_detail.tgl_lahir from hr_pegawai_detail 
		where date_part('day', tgl_lahir) = date_part('day', CURRENT_DATE) And date_part('MONTH', tgl_lahir) = date_part('MONTH', CURRENT_DATE)");
        if ($q->num_rows() > 0) {
            return $q->num_rows();
        }
    }

    function load_ulang_tahun() {

        $q = $this->db->query("select hr_pegawai.id,hr_pegawai.kd_level,hr_pegawai_detail.gender, hr_pegawai.nik,hr_pegawai.nama,hr_pegawai_detail.tgl_lahir,hr_ref_cabang.nama_cabang,hr_ref_unit_kerja.nama_unit_kerja,hr_ref_jabatan.nama_jabatan from hr_pegawai_detail 
		left join hr_pegawai on hr_pegawai.nik=hr_pegawai_detail.nik
		left join hr_ref_cabang on hr_ref_cabang.kd_cabang=hr_pegawai.kd_cabang
		left join hr_ref_unit_kerja on hr_ref_unit_kerja.kd_unit_kerja=hr_pegawai.kd_unit_kerja
		left join hr_ref_jabatan on hr_ref_jabatan.kd_jabatan=hr_pegawai.kd_jabatan
 		where date_part('day', tgl_lahir) = date_part('day', CURRENT_DATE) And date_part('MONTH', tgl_lahir) = date_part('MONTH', CURRENT_DATE)
		LIMIT $this->limit OFFSET $this->offset
		");
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    function save_profil() {
        $this->db->trans_start();
        if ($this->session->userdata('kd_roles') == 4) {
            $apr = 'T';
        } else {
            $apr = 'A';
        }

        //simpan data pokok
        $savepokok = $this->master_pegawai_model->tambah_pegawai_baru_mdl_new();

        //simpan anak
        if (isset($_POST['nama_anak'])) {
            for ($i = 0; $i < count($_POST['nama_anak']); $i++) {

                $data_anak = array('nik' => $_POST['txtnik_hide'],
                    'nama' => $_POST['nama_anak'][$i],
                    'status_anak' => $_POST['status_anak_2'][$i],
                    'tmp_lahir' => $_POST['temp_lahir_anak'][$i],
                    'tgl_lahir' => $_POST['tgl_lahir_anakk'][$i],
                    'no_akte_lahir' => $_POST['akte_kelahiran_anak'][$i],
                    'gender' => $_POST['gender_anak_2'][$i],
                    'pendidikan' => $_POST['pendidikan_anakk'][$i],
                    'status_nikah' => $_POST['status_anak_2'][$i],
                    'status_pendidikan' => $_POST['pendidikan_anakk'][$i],
                    'keterangan' => $_POST['keterangan_anak'][$i],
                    'apr' => $apr
                );

                $saveanak = $this->master_pegawai_model->add_anak_by_seleksi($data_anak);
            }
        }//tutup looping anak
        //simpan pendidikan
        if (isset($_POST['sklh_institution'])) {
            for ($i = 0; $i < count($_POST['sklh_institution']); $i++) {

                if ($_POST['sklh_gpa'][$i] == '') {
                    $_POST['sklh_gpa'][$i] = 0;
                }

                $data_pendidikan = array(
                    'nik' => $_POST['txtnik_hide'],
                    'nama_sekolah' => $_POST['sklh_institution'][$i],
                    'jenjang' => $_POST['sklh_qualification'][$i],
                    'jurusan' => $_POST['sklh_major'][$i],
                    'tanggal_ijazah' => '01-01-1900',
                    'tahun_masuk' => $_POST['sklh_startdate'][$i],
                    'tahun_keluar' => $_POST['sklh_enddate'][$i],
                    'nilai' => $_POST['sklh_gpa'][$i],
                    'no_ijazah' => '0',
                    'gelar' => $_POST['sklh_gelar'][$i],
                    'apr' => $apr
                );

                $savependidikan = $this->master_pegawai_model->add_pendidikan_by_seleksi($data_pendidikan);
                //$this->insert_kompetensi_pendidikan();
            }//tutup looping pendidikan
        }

        //simpan kursus
        if (isset($_POST['course_name'])) {
            for ($i = 0; $i < count($_POST['course_name']); $i++) {

                $data_kursus = array(
                    'nik' => $_POST['txtnik_hide'],
                    'nama_kursus' => $_POST['course_name'][$i],
                    'lembaga' => $_POST['course_institution'][$i],
                    'alamat' => $_POST['course_address'][$i],
                    'no_sertifikat' => $_POST['course_certificate'][$i],
                    'tanggal_sertifikat' => $_POST['course_certificate_date'][$i],
                    'tanggal_masuk' => $_POST['course_startdate'][$i],
                    'tanggal_keluar' => $_POST['course_enddate'][$i],
                    'apr' => $apr
                );

                $savekursus = $this->master_pegawai_model->add_kursus_by_seleksi($data_kursus);
            }//tutup looping kursus
        }

        //simpan karir
        if (isset($_POST['work_company'])) {
            for ($i = 0; $i < count($_POST['work_company']); $i++) {

                $data_karir = array(
                    'nik' => $_POST['txtnik_hide'],
                    'nama_perusahaan' => $_POST['work_company'][$i],
                    'nama_pekerjaan' => $_POST['work_position'][$i],
                    'jabatan_terakhir' => $_POST['work_position'][$i],
                    'tanggal_masuk' => $_POST['work_startdate'][$i],
                    'tanggal_keluar' => $_POST['work_enddate'][$i],
                    'gaji' => $_POST['work_salary'][$i],
                    'alasan_keluar' => $_POST['work_leave'][$i],
                    'apr' => $apr
                );

                $savekarir = $this->master_pegawai_model->add_karir_by_seleksi($data_karir);
            }//tutup looping pendidikan
        }

        //delete dari table new_employee
        //tutup sementara
        //$this->rm_newemployee_model->delete($_POST['jobseekerid']);

        if ($this->db->trans_complete())
            return true;
    }

    /* added by Nisa */

    function load_data_semua_pegawai_tanpa_paging() {
        //post
        $nik_pengajar = $this->input->post('nik_pengajar');
        $plth_mulai = $this->input->post('plth_mulai');
        $plth_akhir = $this->input->post('plth_akhir');
        $unit_kerja = $this->input->post('unit_kerja');
        $lokasi_kerja = $this->input->post('lokasi_kerja');
        $nik = $this->input->post('txtnik');
        $nama = $this->input->post('txtnama');
        $jabatan = $this->input->post('jabatanx');
        $level = $this->input->post('level');
        $status2 = $this->input->post('status');
        $gender = $this->input->post('gender');
        $tahun_masuk = $this->input->post('tahun_masuk');
        $jenjang = $this->input->post('jenjang');

        //session
        $nik_s = $this->session->userdata('nik_session');
        $kd_jabatan = $this->session->userdata('kd_jabatan');
        $kd_cabang_user = $this->session->userdata('cabang_user');
        $kd_unit_kerja_user = $this->session->userdata('kd_unit_kerja');
        $grup_id = $this->session->userdata('kd_roles');
        $mapping_jabatan = $this->session->userdata('id_mapping_jabatan');

        //parameter
        $filter = "";
        $filter_user = "";
//        $grup_id = "";
        $filter_pegawai = "";

        if (($plth_mulai == '') && ($plth_akhir == '')) {

            return array('rows' => array(), 'total' => 0);
        } else {
            $this->db->select("DISTINCT(hr_pegawai.nik), hr_pegawai.nama, hr_pegawai.kd_cabang,hr_pegawai.kd_level, hr_pegawai.apr, hr_pegawai.id,
                        to_char(hr_pegawai.tgl_masuk, 'dd-mm-yyyy') as tgl_masuk, to_char(hr_pegawai.tgl_capeg, 'dd-mm-yyyy') as tgl_capeg,                     
                         hr_pegawai_detail.g_depan, hr_pegawai_detail.g_belakang, hr_pegawai_detail.tmp_lahir, to_char(hr_pegawai.tgl_masuk, 'yyyy') as tahun_masuk, 
                         to_char(hr_pegawai_detail.tgl_lahir, 'dd-mm-yyyy') as tgl_lahir, hr_pegawai_detail.gender,hr_pegawai.kd_jabatan,                                
                         hr_ref_jabatan.nama_jabatan, hr_ref_unit_kerja.nama_unit_kerja,hr_pegawai.kd_unit_kerja, hr_ref_cabang.nama_cabang,
                         (select count(distinct a.nik)
                            from hr_diklat_dinas d,hr_pelatihan p,hr_absensi a
                            where d.kd_diklat_dinas = p.kd_diklat_dinas and a.nik=p.nik and a.keterangan is not null
                            and p.nik=hr_pegawai.nik and (p.status = 'T' or p.status = '0')
                            and ((d.tanggal_awal >='" . hgenerator::switch_tanggal($plth_mulai) . "' and d.tanggal_akhir <= '" . hgenerator::switch_tanggal($plth_akhir) . "')
                            or (a.tgl_absensi >= '" . hgenerator::switch_tanggal($plth_mulai) . "' and a.tgl_absensi <= '" . hgenerator::switch_tanggal($plth_akhir) . "'))) as keterangan,
                            id_mapping_jabatan", FALSE);
            $this->db->from('hr_pegawai');
            $this->db->join('hr_ref_jabatan', 'hr_pegawai.kd_jabatan=hr_ref_jabatan.kd_jabatan', 'left');
            $this->db->join('hr_ref_unit_kerja', 'hr_pegawai.kd_unit_kerja=hr_ref_unit_kerja.kd_unit_kerja', 'left');
            $this->db->join('hr_ref_cabang', 'hr_pegawai.kd_cabang=hr_ref_cabang.kd_cabang', 'left');
            $this->db->join('hr_pegawai_detail', 'hr_pegawai.nik=hr_pegawai_detail.nik', 'left');
            $this->db->join('hr_mapping_jabatan m', 'm.kd_cabang=hr_pegawai.kd_cabang and  m.kd_unit_kerja=hr_pegawai.kd_unit_kerja and  m.kd_jabatan=hr_pegawai.kd_jabatan');
            //$this->db->where("(hr_pegawai.status_aktif='Aktif' OR hr_pegawai.status_aktif = 'Non Job')
            //             AND length(hr_pegawai.kd_level) < 3  AND hr_pegawai.kd_level != 'ho' AND hr_pegawai_detail.apr = 'T'");
            $this->db->order_by('hr_ref_cabang.nama_cabang,hr_ref_unit_kerja.nama_unit_kerja,hr_pegawai.kd_unit_kerja,hr_pegawai.nik', null, FALSE);

            $this->db->where("m.parent", $mapping_jabatan);

            if ($grup_id == '4') {
                if (($nik == '') && ($nama == '') && ($jabatan == '') && ($level == '') && ($lokasi_kerja == '') && ($unit_kerja == '') && ($gender == '') && ($status2 == '')) {
                    $this->db->where("hr_pegawai.kd_cabang", $this->session->userdata('cabang_user'));
                    $this->db->where("hr_pegawai.kd_unit_kerja", $kd_unit_kerja_user);
                } else {
                    if ($lokasi_kerja != '') {
                        $this->db->where("hr_pegawai.kd_cabang", $lokasi_kerja);
                    }
                    if ($unit_kerja != '') {
                        $this->db->where("hr_pegawai.kd_unit_kerja", $unit_kerja);
                    }
                }
            } else if ($grup_id <> '4') {
                $this->db->where("hr_pegawai.kd_cabang", $kd_cabang_user);
                $this->db->where("hr_pegawai.kd_unit_kerja", $kd_unit_kerja_user);
            }

            if ($nik != '') {
                if ($nik == $nik_pengajar)
                    $this->db->where("hr_pegawai.nik = '='", null, FALSE);
                else
                    $this->db->like("hr_pegawai.nik", $nik);
            }
            else
            if ($nik == '') {
                if ($nik_pengajar != '')
                    $this->db->where("hr_pegawai.nik <> '" . $nik_pengajar . "'", null, FALSE);
            }

            if ($nama != '')
                $this->db->like("lower(hr_pegawai.nama)", strtolower($nama));
            if ($jabatan != '')
                $this->db->where("hr_pegawai.kd_jabatan", $jabatan);
            if ($level != '')
                $this->db->where("hr_pegawai.kd_level", $level);
            if ($status2 != '') {
                if ($status2 == 'Pegawai Tetap')
                    $this->db->where("hr_pegawai.kd_level <> ", '0');
                else
                    $this->db->where("hr_pegawai.kd_level", '0');
            }
            if ($gender != '') {
                $this->db->where("hr_pegawai_detail.gender", $gender);
            }

            if ($tahun_masuk != '')
                $this->db->where("to_char(hr_pegawai.tgl_masuk, 'YYYY')", $tahun_masuk);
            if ($jenjang != '')
                $this->db->where("view_pendidikan_terakhir.jenjang", $jenjang);


            $q = $this->db->get();
            if ($q->num_rows() > 0) {
                foreach ($q->result() as $data) {


                    if (strpos($data->nik, ' ') !== FALSE) {
                        $nik = $data->nik;
                        $id = str_replace(' ', '-', $nik);
                    } else
                        $id = $data->nik;

                    $rows[] = array(
                        'primary_key' => $id,
                        //'cb' => form_checkbox('checkpeserta', $id, FALSE, 'class="checkbox" id="checkbox_' . $id . '"'),
                        'cb' => '<a href="javascript:void(0)" rel="btn-add-peserta-' . $data->nik . '" id="' . $data->nik . '" class="btn tbh" onclick="add_this(this.id)"><i class="icon-plus"></i></a>',
                        'nik' => form_hidden('nikpeserta[]', $data->nik) . ($data->keterangan > 0 ? '<a href="javascript:void(0)" id="drildown_key_one_' . $id . '" rel="' . $id . '" data-source="' . base_url() . 'pengajuan_pelatihan/getketerangan/?mulai=' . $plth_mulai . '&amp;akhir=' . $plth_akhir . '" parent="one" onclick="drildown(this.id)"><span class="spred" style="color:red;">' . $data->nik . '</span></a>' : $data->nik),
                        'nama' => ($data->keterangan > 0 ? '<a href="javascript:void(0)" id="drildown_key_one_' . $id . '" rel="' . $id . '" data-source="' . base_url() . 'pengajuan_pelatihan/getketerangan/?mulai=' . $plth_mulai . '&amp;akhir=' . $plth_akhir . '" parent="one" onclick="drildown(this.id)"><span class="spred" style="color:red;">' . $data->nama . '</span></a>' : $data->nama),
                        //'level' => $data->kd_level . form_hidden('map_jabatan[]', $data->id_mapping_jabatan),
                        'lokasi' => form_hidden('namacabang[]', $data->kd_cabang) . $data->nama_cabang,
                        'unit' => form_hidden('unitkerja[]', $data->kd_unit_kerja) . $data->nama_unit_kerja,
                        'jabatan' => form_hidden('jabatan[]', $data->kd_jabatan) . $data->nama_jabatan . form_hidden('map_jabatan[]', $data->id_mapping_jabatan),
                        'status' => $data->kd_level != '0' ? 'Tetap' : 'Kontrak',
                    );
                }
                return array('rows' => $rows, 'total' => 0);
            } else {
                return array('rows' => array(), 'total' => 0);
            }
        }
    }

    function load_data_semua_pegawai() {
        $unit_kerja = $this->input->post('unit_kerja');
        $kd_jabatan = $this->session->userdata('kd_jabatan');
        $lokasi_kerja = $this->input->post('lokasi_kerja');
        $nik = $this->input->post('txtnik');
        $nama = $this->input->post('txtnama');
        $nik_s = $this->session->userdata('nik_session');
        $filter = "";
        $filter_user = "";
        $grup_id = "";
        $filter_pegawai = "";
        $kd_cabang_user = $this->session->userdata('cabang_user');
        $kd_unit_kerja_user = $this->session->userdata('kd_unit_kerja');
        $grup_id = $this->session->userdata('kd_roles');
        $jabatan = $this->input->post('jabatan');
        $level = $this->input->post('level');
        $status2 = $this->input->post('status');
        $gender = $this->input->post('gender');
        $tahun_masuk = $this->input->post('tahun_masuk');
        $jenjang = $this->input->post('jenjang');

        $this->db->select("DISTINCT(hr_pegawai.nik), hr_pegawai.nama, hr_pegawai.kd_level, hr_pegawai.apr, hr_pegawai.id,
      to_char(hr_pegawai.tgl_masuk, 'dd-mm-yyyy') as tgl_masuk, to_char(hr_pegawai.tgl_capeg, 'dd-mm-yyyy') as tgl_capeg,
      hr_pegawai_detail.g_depan, hr_pegawai_detail.g_belakang, hr_pegawai_detail.tmp_lahir, to_char(hr_pegawai.tgl_masuk, 'yyyy') as tahun_masuk,
      to_char(hr_pegawai_detail.tgl_lahir, 'dd-mm-yyyy') as tgl_lahir, hr_pegawai_detail.gender,
      hr_ref_jabatan.nama_jabatan, hr_ref_unit_kerja.nama_unit_kerja, hr_ref_cabang.nama_cabang ", FALSE);
        $this->db->from('hr_pegawai');
        $this->db->join('hr_ref_jabatan', 'hr_pegawai.kd_jabatan=hr_ref_jabatan.kd_jabatan', 'left');
        $this->db->join('hr_ref_unit_kerja', 'hr_pegawai.kd_unit_kerja=hr_ref_unit_kerja.kd_unit_kerja', 'left');
        $this->db->join('hr_ref_cabang', 'hr_pegawai.kd_cabang=hr_ref_cabang.kd_cabang', 'left');
        $this->db->join('hr_pegawai_detail', 'hr_pegawai.nik=hr_pegawai_detail.nik', 'left');
        $this->db->join('view_pendidikan_terakhir', 'view_pendidikan_terakhir.nik = hr_pegawai.nik', 'left');
        $this->db->where("(hr_pegawai.status_aktif='Aktif' OR hr_pegawai.status_aktif = 'Non Job')
      AND length(hr_pegawai.kd_level) < 3  AND hr_pegawai.kd_level != 'ho' AND hr_pegawai_detail.apr = 'T'");

        if ($lokasi_kerja != '')
            $this->db->where("hr_pegawai.kd_cabang", $lokasi_kerja);
        if ($unit_kerja != '')
            $this->db->where("hr_pegawai.kd_unit_kerja", $unit_kerja);
        if ($nik != '')
            $this->db->like("hr_pegawai.nik", $nik);
        if ($nama != '')
            $this->db->like("hr_pegawai.nama", $nama);
        if ($jabatan != '')
            $this->db->where("hr_pegawai.kd_jabatan", $jabatan);
        if ($level != '')
            $this->db->where("hr_pegawai.kd_level", $level);
        if ($status2 != '') {
            if ($status2 == 'Pegawai Tetap')
                $this->db->where("hr_pegawai.kd_level <> ", '0');
            else
                $this->db->where("hr_pegawai.kd_level", '0');
        }
        if ($gender != '') {
            $this->db->where("hr_pegawai_detail.gender", $gender);
        }

        if ($tahun_masuk != '')
            $this->db->where("to_char(hr_pegawai.tgl_masuk, 'YYYY')", $tahun_masuk);
        if ($jenjang != '')
            $this->db->where("view_pendidikan_terakhir.jenjang", $jenjang);

        //  $this->db->limit($this->limit, $this->offset);

        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    function getComboTahun() {
        $tahun = date('Y');
        //$this->db->select('min(tgl_masuk) as tahunAwal');
        //$this->db->from("hr_pegawai");
        //$query = $this->db->get();
        $hasil = "";
        for ($idx = $tahun; $idx >= 1975; $idx--) {
            $hasil .= "<option>$idx</option>";
        }
        return $hasil;
    }

    function getComboJenjang() {
        $this->db->from('hr_ref_jenjang');
        $query = $this->db->get();
        $hasil = array('' => '-Semua Pendidikan-');
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[$data->jenjang] = $data->jenjang;
            }
        }
        return $hasil;
    }

    function load_data_semua_pegawai_tanpa_paging2() {
        $kd_jabatan = $this->session->userdata('kd_jabatan');
        $nik_s = $this->session->userdata('nik_session');
        $filter = "";
        $filter_user = "";
        $grup_id = "";
        $filter_pegawai = "";
        $kd_cabang_user = $this->session->userdata('cabang_user');
        $kd_unit_kerja_user = $this->session->userdata('kd_unit_kerja');
        $grup_id = $this->session->userdata('kd_roles');
        $plth_mulai = $this->input->post('plth_mulai');
        $plth_akhir = $this->input->post('plth_akhir');

        $unit_kerja = $this->input->post('unit_kerja_pengajar');
        $lokasi_kerja = $this->input->post('lokasi_kerja_pengajar');
        $nik = $this->input->post('txtnik_pengajar');
        $nama = $this->input->post('txtnama_pengajar');
        $jabatan = $this->input->post('jabatan_pengajar');
        $level = $this->input->post('level_pengajar');
        $status2 = $this->input->post('status_pengajar');
        $gender = $this->input->post('gender_pengajar');
        $tahun_masuk = $this->input->post('tahun_masuk_pengajar');
        $jenjang = $this->input->post('jenjang_pengajar');

        if (($plth_mulai == 'x') && ($plth_akhir == 'x')) {

            return array('rows' => array(), 'total' => 0);
        } else {

            $this->db->select("DISTINCT(hr_pegawai.nik), hr_pegawai.nama, hr_pegawai.kd_cabang,hr_pegawai.kd_level, hr_pegawai.apr, hr_pegawai.id,
                        to_char(hr_pegawai.tgl_masuk, 'dd-mm-yyyy') as tgl_masuk, to_char(hr_pegawai.tgl_capeg, 'dd-mm-yyyy') as tgl_capeg,                     
                         hr_pegawai_detail.g_depan, hr_pegawai_detail.g_belakang, hr_pegawai_detail.tmp_lahir, to_char(hr_pegawai.tgl_masuk, 'yyyy') as tahun_masuk, 
                         to_char(hr_pegawai_detail.tgl_lahir, 'dd-mm-yyyy') as tgl_lahir, hr_pegawai_detail.gender,                                
                         hr_ref_jabatan.nama_jabatan, hr_ref_unit_kerja.nama_unit_kerja,hr_pegawai.kd_unit_kerja, hr_ref_cabang.nama_cabang,
                         (select count(distinct a.nik)
                            from hr_diklat_dinas d,hr_pelatihan p,hr_absensi a
                            where d.kd_diklat_dinas = p.kd_diklat_dinas and a.nik=p.nik and a.keterangan is not null
                            and p.nik=hr_pegawai.nik and (p.status = 'T' or p.status = '0')
                            and ((d.tanggal_awal >='" . $plth_mulai . "' and d.tanggal_akhir <= '" . $plth_akhir . "')
                            or (a.tgl_absensi >= '" . $plth_mulai . "' and a.tgl_absensi <= '" . $plth_akhir . "'))) as keterangan", FALSE);
            $this->db->from('hr_pegawai');
            $this->db->join('hr_ref_jabatan', 'hr_pegawai.kd_jabatan=hr_ref_jabatan.kd_jabatan', 'left');
            $this->db->join('hr_ref_unit_kerja', 'hr_pegawai.kd_unit_kerja=hr_ref_unit_kerja.kd_unit_kerja', 'left');
            $this->db->join('hr_ref_cabang', 'hr_pegawai.kd_cabang=hr_ref_cabang.kd_cabang', 'left');
            $this->db->join('hr_pegawai_detail', 'hr_pegawai.nik=hr_pegawai_detail.nik', 'left');
            //$this->db->where("(hr_pegawai.status_aktif='Aktif' OR hr_pegawai.status_aktif = 'Non Job')
            //             AND length(hr_pegawai.kd_level) < 3  AND hr_pegawai.kd_level != 'ho' AND hr_pegawai_detail.apr = 'T'");

            if ($lokasi_kerja != '') {
                $this->db->where("hr_pegawai.kd_cabang", $lokasi_kerja);
            } else {
                $this->db->where("hr_pegawai.kd_cabang", $this->session->userdata('cabang_user'));
            }
            if ($unit_kerja != '')
                $this->db->where("hr_pegawai.kd_unit_kerja", $unit_kerja);
            if ($nik != '')
                $this->db->where("hr_pegawai.nik", $nik);
            if ($nama != '')
                $this->db->like("lower(hr_pegawai.nama)", strtolower($nama));
            //$this->db->like("hr_pegawai.nama", $nama);
            if ($jabatan != '')
                $this->db->where("hr_pegawai.kd_jabatan", $jabatan);
            if ($level != '')
                $this->db->where("hr_pegawai.kd_level", $level);
            if ($status2 != '') {
                if ($status2 == 'Pegawai Tetap')
                    $this->db->where("hr_pegawai.kd_level <> ", '0');
                else
                    $this->db->where("hr_pegawai.kd_level", '0');
            }
            if ($gender != '') {
                $this->db->where("hr_pegawai_detail.gender", $gender);
            }

            if ($tahun_masuk != '')
                $this->db->where("to_char(hr_pegawai.tgl_masuk, 'YYYY')", $tahun_masuk);
            if ($jenjang != '')
                $this->db->where("view_pendidikan_terakhir.jenjang", $jenjang);


            $q = $this->db->get();
            if ($q->num_rows() > 0) {
                foreach ($q->result() as $data) {


                    if (strpos($data->nik, ' ') !== FALSE) {
                        $nik = $data->nik;
                        $id = str_replace(' ', '-', $nik);
                    } else
                        $id = $data->nik;

                    $rows[] = array(
                        'primary_key' => $id,
                        //'cb' => form_checkbox('checkpeserta', $id, FALSE, 'class="radio_pengajar" id="radio_' . $id . '"'),         
                        'cb' => '<a href="javascript:void(0)" rel="btn-add-pengajar-' . $data->nik . '" id="' . $data->nik . '" class="btn tbh" onclick="add_ini(this.id)"><i class="icon-plus"></i></a>',
                        //'nik' => form_hidden('nikpengajar[]', $data->nik) . $data->nik,
                        //'nama' => $data->nama,
                        'nik' => form_hidden('nikpeserta[]', $data->nik) . ($data->keterangan > 0 ? '<a href="javascript:void(0)" id="drildown_key_one_pengajar_' . $id . '" rel="' . $id . '" data-source="' . base_url() . 'pengajuan_pelatihan/getketerangan/?mulai=' . $plth_mulai . '&amp;akhir=' . $plth_akhir . '" parent="one_pengajar" onclick="drildown(this.id)"><span class="spred" style="color:red;">' . $data->nik . '</span></a>' : $data->nik),
                        'nama' => ($data->keterangan > 0 ? '<a href="javascript:void(0)" id="drildown_key_one_pengajar_' . $id . '" rel="' . $id . '" data-source="' . base_url() . 'pengajuan_pelatihan/getketerangan/?mulai=' . $plth_mulai . '&amp;akhir=' . $plth_akhir . '" parent="one_pengajar" onclick="drildown(this.id)"><span class="spred" style="color:red;">' . $data->nama . '</span></a>' : $data->nama),
                        //'level' => $data->kd_level,
                        'lokasi' => $data->nama_cabang,
                        'unit' => $data->nama_unit_kerja,
                        'jabatan' => $data->nama_jabatan,
                        'status' => $data->kd_level != '0' ? 'Tetap' : 'Kontrak',
                    );
                }
                return array('rows' => $rows, 'total' => 0);
            } else {
                return array('rows' => array(), 'total' => 0);
            }
        }
    }

    function load_data_peserta2() {
        $id_diklat = $this->input->post('id_diklat');
        $nik_pengajar = $this->input->post('nik_pengajar');
        $plth_mulai = $this->input->post('plth_mulai');
        $plth_akhir = $this->input->post('plth_akhir');
        $unit_kerja = $this->input->post('unit_kerja');
        $kd_jabatan = $this->session->userdata('kd_jabatan');
        $lokasi_kerja = $this->input->post('lokasi_kerja');
        $nik = $this->input->post('txtnik');
        $nama = $this->input->post('txtnama');
        $nik_s = $this->session->userdata('nik_session');
        $filter = "";
        $filter_user = "";
        $grup_id = "";
        $filter_pegawai = "";
        $kd_cabang_user = $this->session->userdata('cabang_user');
        $kd_unit_kerja_user = $this->session->userdata('kd_unit_kerja');
        $grup_id = $this->session->userdata('kd_roles');
        $jabatan = $this->input->post('jabatan');
        $level = $this->input->post('level');
        $status2 = $this->input->post('status');
        $gender = $this->input->post('gender');
        $tahun_masuk = $this->input->post('tahun_masuk');
        $jenjang = $this->input->post('jenjang');

        if (($plth_mulai == '') && ($plth_akhir == '')) {

            return array('rows' => array(), 'total' => 0);
        } else {
            $this->db->select("DISTINCT(hr_pegawai.nik), hr_pegawai.nama, hr_pegawai.kd_cabang,hr_pegawai.kd_level, hr_pegawai.apr, hr_pegawai.id,
                        to_char(hr_pegawai.tgl_masuk, 'dd-mm-yyyy') as tgl_masuk, to_char(hr_pegawai.tgl_capeg, 'dd-mm-yyyy') as tgl_capeg,                     
                         hr_pegawai_detail.g_depan, hr_pegawai_detail.g_belakang, hr_pegawai_detail.tmp_lahir, to_char(hr_pegawai.tgl_masuk, 'yyyy') as tahun_masuk, 
                         to_char(hr_pegawai_detail.tgl_lahir, 'dd-mm-yyyy') as tgl_lahir, hr_pegawai_detail.gender,                                
                         hr_ref_jabatan.nama_jabatan, hr_ref_unit_kerja.nama_unit_kerja,hr_pegawai.kd_unit_kerja, hr_ref_cabang.nama_cabang,
                         (select count(distinct a.nik)
                            from hr_diklat_dinas d,hr_pelatihan p,hr_absensi a
                            where d.kd_diklat_dinas = p.kd_diklat_dinas and a.nik=p.nik and a.keterangan is not null
                            and p.nik=hr_pegawai.nik and (p.status = 'T' or p.status = '0')
                            and ((d.tanggal_awal >='" . hgenerator::switch_tanggal($plth_mulai) . "' and d.tanggal_akhir <= '" . hgenerator::switch_tanggal($plth_akhir) . "')
                            or (a.tgl_absensi >= '" . hgenerator::switch_tanggal($plth_mulai) . "' and a.tgl_absensi <= '" . hgenerator::switch_tanggal($plth_akhir) . "'))) as keterangan", FALSE);
            $this->db->from('hr_pegawai');
            $this->db->join('hr_ref_jabatan', 'hr_pegawai.kd_jabatan=hr_ref_jabatan.kd_jabatan');
            $this->db->join('hr_ref_unit_kerja', 'hr_pegawai.kd_unit_kerja=hr_ref_unit_kerja.kd_unit_kerja');
            $this->db->join('hr_ref_cabang', 'hr_pegawai.kd_cabang=hr_ref_cabang.kd_cabang');
            $this->db->join('hr_pegawai_detail', 'hr_pegawai.nik=hr_pegawai_detail.nik');
            $this->db->join('hr_pelatihan pp', 'hr_pegawai.nik<>pp.nik');
            $this->db->where("(hr_pegawai.status_aktif='Aktif' OR hr_pegawai.status_aktif = 'Non Job')
                         AND length(hr_pegawai.kd_level) < 3  AND hr_pegawai.kd_level != 'ho' AND hr_pegawai_detail.apr = 'T'
                         and pp.kd_diklat_dinas='$id_diklat'");
            $this->db->order_by('hr_ref_cabang.nama_cabang,hr_ref_unit_kerja.nama_unit_kerja,hr_pegawai.kd_unit_kerja,hr_pegawai.nik', null, FALSE);

            //$this->db->where("hr_pegawai.nama", $nik_pengajar);



            if ($lokasi_kerja != '') {
                $this->db->where("hr_pegawai.kd_cabang", $lokasi_kerja);
            } else {
                $this->db->where("hr_pegawai.kd_cabang", $this->session->userdata('cabang_user'));
            }
            if ($unit_kerja != '')
                $this->db->where("hr_pegawai.kd_unit_kerja", $unit_kerja);
            if ($nik != '') {
                if ($nik == $nik_pengajar)
                    $this->db->where("hr_pegawai.nik = '='", null, FALSE);
                else
                    $this->db->like("hr_pegawai.nik", $nik);
            }
            else
            if ($nik == '') {
                if ($nik_pengajar != '')
                    $this->db->where("hr_pegawai.nik <> '" . $nik_pengajar . "'", null, FALSE);
            }

            if ($nama != '')
                $this->db->like("lower(hr_pegawai.nama)", strtolower($nama));
            if ($jabatan != '')
                $this->db->where("hr_pegawai.kd_jabatan", $jabatan);
            if ($level != '')
                $this->db->where("hr_pegawai.kd_level", $level);
            if ($status2 != '') {
                if ($status2 == 'Pegawai Tetap')
                    $this->db->where("hr_pegawai.kd_level <> ", '0');
                else
                    $this->db->where("hr_pegawai.kd_level", '0');
            }
            if ($gender != '') {
                $this->db->where("hr_pegawai_detail.gender", $gender);
            }

            if ($tahun_masuk != '')
                $this->db->where("to_char(hr_pegawai.tgl_masuk, 'YYYY')", $tahun_masuk);
            if ($jenjang != '')
                $this->db->where("view_pendidikan_terakhir.jenjang", $jenjang);


            $q = $this->db->get();
            if ($q->num_rows() > 0) {
                foreach ($q->result() as $data) {


                    if (strpos($data->nik, ' ') !== FALSE) {
                        $nik = $data->nik;
                        $id = str_replace(' ', '-', $nik);
                    } else
                        $id = $data->nik;

                    $rows[] = array(
                        'primary_key' => $id,
                        'cb' => form_checkbox('checkpeserta', $id, FALSE, 'class="checkbox" id="checkbox_' . $id . '"'),
                        'nik' => form_hidden('nikpeserta[]', $data->nik) . ($data->keterangan > 0 ? '<a href="javascript:void(0)" id="drildown_key_one_' . $id . '" rel="' . $id . '" data-source="' . base_url() . 'pengajuan_pelatihan/getketerangan/?mulai=' . $plth_mulai . '&amp;akhir=' . $plth_akhir . '" parent="one" onclick="drildown(this.id)"><span class="spred" style="color:red;">' . $data->nik . '</span></a>' : $data->nik),
                        'nama' => ($data->keterangan > 0 ? '<a href="javascript:void(0)" id="drildown_key_one_' . $id . '" rel="' . $id . '" data-source="' . base_url() . 'pengajuan_pelatihan/getketerangan/?mulai=' . $plth_mulai . '&amp;akhir=' . $plth_akhir . '" parent="one" onclick="drildown(this.id)"><span class="spred" style="color:red;">' . $data->nama . '</span></a>' : $data->nama),
                        'level' => $data->kd_level,
                        'lokasi' => form_hidden('namacabang[]', $data->kd_cabang) . $data->nama_cabang,
                        'unit' => form_hidden('unitkerja[]', $data->kd_unit_kerja) . $data->nama_unit_kerja,
                        'jabatan' => $data->nama_jabatan,
                        'status' => $data->kd_level != '0' ? 'Tetap' : 'Kontrak',
                    );
                }
                return array('rows' => $rows, 'total' => 0);
            } else {
                return array('rows' => array(), 'total' => 0);
            }
        }
    }

    public function detail_get_by_id($id) {
        $this->db->select("hr_pegawai.nik, hr_pegawai.nama, hr_pegawai.kd_cabang,hr_pegawai.kd_level, hr_pegawai.apr, hr_pegawai.id,
                        hr_ref_jabatan.nama_jabatan, hr_ref_unit_kerja.nama_unit_kerja,hr_pegawai.kd_unit_kerja, hr_ref_cabang.nama_cabang");
        $this->db->join('hr_ref_jabatan', 'hr_pegawai.kd_jabatan=hr_ref_jabatan.kd_jabatan');
        $this->db->join('hr_ref_unit_kerja', 'hr_pegawai.kd_unit_kerja=hr_ref_unit_kerja.kd_unit_kerja');
        $this->db->join('hr_ref_cabang', 'hr_pegawai.kd_cabang=hr_ref_cabang.kd_cabang');
        $this->db->where("hr_pegawai.nik", $id);
        $query = $this->db->get('hr_pegawai')->result_array();
        return $query;
    }

    /* added by Nisa */

    public function get_data_peserta($condition = array(), $distinct = true, $filter = array()) {
        //filtering jika sdm
        $this->data_with_mapping_id($condition, $distinct);
        $this->db->order_by('k.kd_jabatan,k.parent');
        return $this->db->get();
    }

    public function data_with_mapping_id($condition = array(), $distinct = true) {
        $plth_mulai = $this->input->post('plth_mulai');
        $plth_akhir = $this->input->post('plth_akhir');
        $kompetensi = $this->input->post('kompetensi');
        $this->db->distinct($distinct);

        $this->db->select("a.*, b.nama_jabatan, c.nama_unit_kerja, d.nama_cabang,k.*,f.*,
              (select count(cu.nik) from hr_cuti cu
                where  (
                  ((cu.tgl_mulai_cuti >= '" . hgenerator::switch_tanggal($plth_mulai) . "' AND cu.tgl_selesai_cuti <= '" . hgenerator::switch_tanggal($plth_akhir) . "')
                OR (cu.tgl_mulai_cuti <= '" . hgenerator::switch_tanggal($plth_mulai) . "' AND cu.tgl_selesai_cuti >= '" . hgenerator::switch_tanggal($plth_akhir) . "')
                OR (cu.tgl_mulai_cuti <= '" . hgenerator::switch_tanggal($plth_mulai) . "' AND cu.tgl_selesai_cuti >= '" . hgenerator::switch_tanggal($plth_akhir) . "'))                        
                     AND cu.nik=a.nik and cu.st_apr = 'T'
                )) as jml_cuti,
              (select count(p.nik) from hr_diklat_dinas db, hr_pelatihan p
                where  (
					db.kd_diklat_dinas = p.kd_diklat_dinas and 
					((db.tanggal_awal >= '" . hgenerator::switch_tanggal($plth_mulai) . "' and db.tanggal_akhir >= '" . hgenerator::switch_tanggal($plth_mulai) . "')
					OR (db.tanggal_awal <= '" . hgenerator::switch_tanggal($plth_akhir) . "' AND db.tanggal_akhir >= '" . hgenerator::switch_tanggal($plth_akhir) . "') 
					OR (db.tanggal_awal <= '" . hgenerator::switch_tanggal($plth_mulai) . "' AND db.tanggal_akhir >= '" . hgenerator::switch_tanggal($plth_akhir) . "')
					OR (db.tanggal_awal <= '" . hgenerator::switch_tanggal($plth_mulai) . "' AND db.tanggal_akhir <= '" . hgenerator::switch_tanggal($plth_akhir) . "'))
					and p.nik = a.nik AND (p.status='T' or p.status='0'))
				) as jml_pelatihan", FALSE);
        $this->db->from($this->table . ' a');
        $this->db->join($this->table2 . ' b', 'a.kd_jabatan = b.kd_jabatan');
        $this->db->join($this->table3 . ' c', 'a.kd_unit_kerja = c.kd_unit_kerja');
        $this->db->join($this->table4 . ' d', 'a.kd_cabang = d.kd_cabang');
        $this->db->join($this->table18 . ' k', 'a.kd_cabang = k.kd_cabang and a.kd_unit_kerja = k.kd_unit_kerja and a.kd_jabatan = k.kd_jabatan');
        $this->db->join($this->table19 . ' f', 'f.kode_status_kepegawaian = a.status_kerja');
        $this->db->where("f.kode_status_kepegawaian <> ", '15');
        $this->db->where("f.kode_status_kepegawaian <> ", '16');
        $this->db->where("(select count(nik) from hr_kompetensi_pegawai h where h.nik = a.nik and h.id_kompetensi = '$kompetensi') = ", '0');

        $this->db->where_condition($condition);

        return $this->db;
    }

    private $data_maping = array();

    public function view_child_maping() {
        //post
        $plth_mulai = $this->input->post('plth_mulai');
        $plth_akhir = $this->input->post('plth_akhir');

        $txtnik = $this->input->post('txtnik');
        $explode_txtnik = explode(" - ", $txtnik);
        $nik = $explode_txtnik[0];
        $fnama = $this->input->post('txtnama');
        $jabatan = $this->input->post('jabatanx');
        $unit_kerja = $this->input->post('unit_kerja');
        $lokasi_kerja = $this->input->post('lokasi_kerja');
        $level = $this->input->post('level');
        $status2 = $this->input->post('status');
        $unit_sdm = $this->parameter_model->get()->unit_sdm;

        //session
        $is_sdm = $this->session->userdata('is_sdm');
        $user_nik = $this->session->userdata('nik_session');
        $user_nama = $this->session->userdata('nama_session');
        $kd_jabatan = $this->session->userdata('kd_jabatan');
        $kd_cabang_user = $this->session->userdata('cabang_user');
        $kd_unit_kerja_user = $this->session->userdata('kd_unit_kerja');
        $first_sdm = $this->jabatan_unit_kerja_model->get_first_sdm_mapping();
        $session_mapping_jabatan = $this->session->userdata('id_mapping_jabatan');

        $condition = array();
        $list_pegawai = array();

        //parent
        //jika bukan sdm
        if ($is_sdm == '') {

            $condition['a.nik'] = $user_nik;

            if (((!empty($txtnik)) && ($nik != $user_nik)) || ((!empty($jabatan)) && ($jabatan != $kd_jabatan)))
                $condition['a.nik'] = 'x';

            if (!empty($status2))
                $condition['a.status_kerja'] = $status2;

            $list_nama = $this->master_pegawai_model->get_data_peserta($condition);
        }
        //jika sdm
        else if (($is_sdm <> '') && ($is_sdm == '1')) {
            if ((empty($txtnik)) && (empty($jabatan)) && (empty($lokasi_kerja)) && ($unit_kerja == $kd_unit_kerja_user) && (empty($status2)))
                $condition['a.kd_cabang'] = $kd_cabang_user;
            $condition['a.kd_unit_kerja'] = $kd_unit_kerja_user;

            if (!empty($txtnik))
                $condition['a.nik'] = $nik;
            if (!empty($jabatan))
                $condition['a.kd_jabatan'] = $jabatan;
            if (!empty($lokasi_kerja))
                $condition['a.kd_cabang'] = $lokasi_kerja;
            if (!empty($unit_kerja))
                $condition['a.kd_unit_kerja'] = $unit_kerja;
            if (!empty($status2))
                $condition['a.status_kerja'] = $status2;

            $list_nama = $this->master_pegawai_model->get_data_peserta($condition);
        }


        foreach ($list_nama->result() as $data) {

            $id = $data->nik;
            $list_pegawai[] = array(
                'primary_key' => $data->nik,
                'nik' => form_hidden('nikpeserta[]', $data->nik) . (($data->jml_cuti > 0 || $data->jml_pelatihan > 0) ? '<a href="javascript:void(0)" id="drildown_key_one_' . $id . '" rel="' . $id . '" data-source="' . base_url() . 'pengajuan_pelatihan/getketerangan/?mulai=' . $plth_mulai . '&amp;akhir=' . $plth_akhir . '" parent="one" onclick="drildown(this.id)"><span class="spred" style="color:red;">' . $data->nik . '</span></a>' : $data->nik),
                'nama' => (($data->jml_cuti > 0 || $data->jml_pelatihan > 0) ? '<a href="javascript:void(0)" id="drildown_key_one_' . $id . '" rel="' . $id . '" data-source="' . base_url() . 'pengajuan_pelatihan/getketerangan/?mulai=' . $plth_mulai . '&amp;akhir=' . $plth_akhir . '" parent="one" onclick="drildown(this.id)"><span class="spred" style="color:red;">' . $data->nama . '</span></a>' : $data->nama),
                'lokasi' => form_hidden('namacabang[]', $data->kd_cabang) . $data->nama_cabang,
                'unit' => form_hidden('unitkerja[]', $data->kd_unit_kerja) . $data->nama_unit_kerja,
                'jabatan' => form_hidden('jabatan[]', $data->kd_jabatan) . $data->nama_jabatan . form_hidden('map_jabatan[]', $data->id_mapping_jabatan),
                'status' => $data->status_kepegawaian,
                'cb' => '<a href="javascript:void(0)" rel="btn-add-peserta-' . $data->nik . '" id="' . $data->nik . '" class="btn tbh" onclick="add_this(this.id)"><i class="icon-plus"></i></a>',
            );
        }

        $maping_id = $this->session->userdata('id_mapping_jabatan'); // Atasan

        $this->get_child_maping($maping_id);

        if ($is_sdm == '') {
            // Melooping hasil get_child_maping untuk mencari data pegawai
            foreach ($this->data_maping as $value) {

                $filter['a.kd_jabatan'] = $value['kd_jabatan'];
                $filter['a.kd_unit_kerja'] = $value['kd_unit_kerja'];
                $filter['a.kd_cabang'] = $value['kd_cabang'];

                if (!empty($txtnik))
                    $filter['a.nik'] = $nik;

                if ((!empty($jabatan)) && ($jabatan != $value['kd_jabatan']))
                    $filter['a.kd_jabatan'] = 'x';

                if (!empty($status2))
                    $filter['a.status_kerja'] = $status2;

                $filter["a.status_kerja <> '15' and  a.status_kerja <> '16'"] = null;


                $nama2 = $this->master_pegawai_model->get_data_peserta($filter, false);

                foreach ($nama2->result() as $data2) {
                    $id = $data2->nik;
                    $list_pegawai[] = array(
                        'primary_key' => $data2->nik,
                        'nik' => form_hidden('nikpeserta[]', $data2->nik) . (($data2->jml_cuti > 0 || $data2->jml_pelatihan > 0) ? '<a href="javascript:void(0)" id="drildown_key_one_' . $id . '" rel="' . $id . '" data-source="' . base_url() . 'pengajuan_pelatihan/getketerangan/?mulai=' . $plth_mulai . '&amp;akhir=' . $plth_akhir . '" parent="one" onclick="drildown(this.id)"><span class="spred" style="color:red;">' . $data2->nik . '</span></a>' : $data2->nik),
                        'nama' => (($data2->jml_cuti > 0 || $data2->jml_pelatihan > 0) ? '<a href="javascript:void(0)" id="drildown_key_one_' . $id . '" rel="' . $id . '" data-source="' . base_url() . 'pengajuan_pelatihan/getketerangan/?mulai=' . $plth_mulai . '&amp;akhir=' . $plth_akhir . '" parent="one" onclick="drildown(this.id)"><span class="spred" style="color:red;">' . $data2->nama . '</span></a>' : $data2->nama),
                        'lokasi' => form_hidden('namacabang[]', $data2->kd_cabang) . $data2->nama_cabang,
                        'unit' => form_hidden('unitkerja[]', $data2->kd_unit_kerja) . $data2->nama_unit_kerja,
                        'jabatan' => form_hidden('jabatan[]', $data2->kd_jabatan) . $data2->nama_jabatan . form_hidden('map_jabatan[]', $data2->id_mapping_jabatan),
                        'status' => $data2->status_kepegawaian,
                        'cb' => '<a href="javascript:void(0)" rel="btn-add-peserta-' . $data2->nik . '" id="' . $data2->nik . '" class="btn tbh" onclick="add_this(this.id)"><i class="icon-plus"></i></a>',
                    );
                }
            }
        }

        return array('rows' => $list_pegawai, 'total' => 0);
    }

    public function get_child_maping($maping_id) {
        $list = $this->jabatan_unit_kerja_model->get_data(array('a.parent' => $maping_id));

        if ($list->num_rows() > 0) {
            foreach ($list->result() as $row) {
                $this->data_maping[] = array(
                    //'id_mapping_jabatan' => $row->id_mapping_jabatan,
                    'kd_cabang' => $row->kd_cabang,
                    'kd_unit_kerja' => $row->kd_unit_kerja,
                    'kd_jabatan' => $row->kd_jabatan
                );

                $this->get_child_maping($row->id_mapping_jabatan);
            }
        }
    }

}

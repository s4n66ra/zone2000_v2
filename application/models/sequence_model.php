<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class sequence_model extends MY_Model {

	public $table 	= 'm_sequence';
    public $primary = 'sequence_id';
    public $flex 	= true;

	public function __construct() {
        parent::__construct();
    }

    public function getSequence($seq = 'sequnce_id'){
    	$data = $this->get()->row_array();
    	return $data[$seq]+1;
    }

    public function raiseSequence($seq = 'sequence_id'){
        $this->set($seq, $seq.'+1', FALSE)->update();
        //return $this->get()->row_array()[$seq];
        return $this->get()->row_array();
    }

}

/* End of file sequence_model.php */
/* Location: ./application/models/sequence_model.php */
<?php

/**
 * Description of parameter_model
 *
 * @author Warman Suganda
 */
class parameter_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;

    private function data($condition = array()) {
        $this->db->select('a.*');
        $this->db->from($this->table . ' a');

        //$this->db->where_condition($condition);
        $this->db->where($condition);

        return $this->db;
    }

    public function get() {

        $parameter = array();
        $fields = $this->db->list_fields($this->table);
        foreach ($fields as $field) {
            $parameter[$field] = '';
        }

        $condition['a.kd_parameter'] = 1;
        $data = $this->get_data($condition);
        if ($data->num_rows() > 0) {
            $row = $data->row();
            foreach ($fields as $field) {
                $parameter[$field] = $row->{$field};
            }
        }
        return (object) $parameter;
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function update($data, $id) {
        return $this->db->update($this->table, $data, array('kd_parameter' => $id));
    }

    public function get_sdm_unit($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }
    
}

/* End of file parameter_model.php */
/* Location: ./application/models/parameter_model.php */

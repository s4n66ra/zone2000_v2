<?php
class divisi_model extends My_Model {
    public $limitStore = true;
    public function __construct() {
        parent::__construct();
        $this->table = 'm_divisi';
        $this->primary='id_divisi';
        $this->flex = true;
    }

    public $limit;
    public $offset;
    private $divisi  = 'm_divisi';
    private $table2 = 'supplier';
    private $table3 = 'purchase_order';
    private $table4 = 'bbm_barang';
    private $table5 = 'barang';
    private $table6 = 'divisi';
    private $table7 = 'kategori';

    public function relation(){
        return array(
            'item' => array('m_item','item_id','item_id'),
            'type' => array('m_jenis_buying', 'jenis_buying_id', 'jenis_buying_id'),
            'outlet' => array('m_cabang','id_cabang','store_id'),
            'store' => array('m_cabang', 'id_cabang', 'store_id'),
        );
    }

    private function data($condition = array()) {        
        $this->db->from($this->table);
        $this->db->where_condition($condition);
        return $this->db;
    }

    private function data_detail($condition = array()) {
        // =============Filtering===============
        //$condition = array();
        $kd_bbm= $this->input->post("kd_bbm");
        $id_supplier= $this->input->post("id_supplier");
        $id_purchase_order = $this->input->post('id_purchase_order');

        $id_bbm = $this->input->post('id_bbm');
        if(!empty($kd_bbm)){
            $condition["a.kd_bbm like '%$kd_bbm%'"]=null;
        }
        if(!empty($id_supplier)){
            $condition["a.id_supplier"]=$id_supplier;
        }
        if(!empty($id_purchase_order)){
            $condition["a.id_purchase_order"]=$id_purchase_order;
        }
        if(!empty($id_bbm)){
            $condition["a.id_bbm"]=$id_bbm;
        }

        $tanggal_awal = hgenerator::switch_tanggal($this->input->post('tanggal_awal'));
        $tanggal_akhir = hgenerator::switch_tanggal($this->input->post('tanggal_akhir'));
        //--------end filtering-----------------------------------


        $this->db->select('*,b.id as id_bbm_barang');
        $this->db->from($this->table  . ' a');
        $this->db->join($this->table4 . ' b',' a.id_bbm = b.id_bbm');
        $this->db->join($this->table5 . ' c',' b.id_barang = c.id_barang','left');
        $this->db->join($this->table6 . ' d',' c.id_divisi = d.id_divisi','left');
        $this->db->join($this->table7 . ' e',' e.id_kategori = c.id_kategori','left');
        $this->db->order_by('c.kd_barang ASC');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_by_id($id) {
        $condition['buying_id'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_detail_by_id($id) {
        $condition['b.id'] = $id;
        $this->data_detail($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function data_table() {
        // Total Record
        $total = $this->data()->count_all_results();

        // List Data
        //$this->db->limit($this->limit, $this->offset);
        $data = $this->data()->get()->result();
        $rows = array();

        foreach ($data as $key => $value) {
            $id = $value->id_divisi;
            $id_enc = url_base64_encode($id);
            $action = '';
            $action .= '';

            if ($this->access_right->otoritas('edit')) {
                $action .= view::button_edit($id_enc, array('full-width'=>0));
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= view::button_delete($id_enc, array('full-width'=>0));
            }

            $temp = array(
                'primary_key' =>$value->id_divisi,
                'kd_divisi' => $value->kd_divisi,
                'nama_divisi' => $value->nama_divisi,
                'harga' => $value->harga,
                'jumlah_tiket' => $value->jumlah_tiket,
                'aksi' => $action
            );

            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $temp['aksi'] = view::render_button_group_raw($action);
            }
            $rows[] = $temp;
            
        }

        return array('rows' => $rows, 'total' => $total);
    }   
 

    

    
    
    public function options($default = '--Pilih divisi--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_divisi] = $row->nama_divisi ;
        }
        return $options;
    }

    public function options_empty() {
        $data = $this->data()->get();
        $options = array();
        $options['']  = '';
        foreach ($data->result() as $row) {
            $options[$row->id_divisi] = $row->nama_divisi ;
        }
        return $options;
    }
    

    public function data_table_detail_excel() {
        // Total Record
        $total = $this->data_detail()->count_all_results();

        // List Data
        $data_detail = $this->data_detail()->get();
        $rows = array();

        foreach ($data_detail->result() as $value) {
            $id = $value->id_bbm_barang;
            $action = '';
            $rows[] = array(
                'primary_key' => $id,
                'kd_barang'=>$value->kd_barang,
                'nama_barang' => $value->nama_barang,
                'jumlah' => $value->jumlah_barang,
                'divisi' => $value->nama_divisi,
                'kategori' => $value->nama_kategori,
            );
        }

        return array('rows' => $rows, 'total' => $total);
    }    

    public function options_jenis() {

        $data = $this->db
                ->from('m_jenis_buying')
                ->get()->result();


        $options = array();
        //$options['']  = '';
        foreach ($data as $row) {
            $options[$row->jenis_buying_id] =$row->nama_jenis ;
        }
        return $options;
    }

    
}

?>
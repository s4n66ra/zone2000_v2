<?php 

class store_setting_model extends MY_Model {

    public $table = 'm_cabang_setting';
    public $primary = 'id';
    public $flex = true;

    public function __construct() {
        parent::__construct();
    }

    public function relation(){
        return array(
            'store' => array('m_cabang', 'id_cabang', 'id_cabang'),
        );
    }

}
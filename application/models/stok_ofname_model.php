<?php 

class stok_ofname_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'zn_stok_ofname';
        $this->primary = 'so_id';
        $this->limitStore = true;
        $this->flex = true;
    }

    public function relation(){
        return array(
            'store' => array("m_cabang", "id_cabang", "store_id"),
        	'item' => array("m_item", "item_id", "item_id")
        );
    }

    public function get_by_id($id){
        $this->db->from('zn_stok_ofname so');
        $this->db->join('m_item m','so.item_id = m.item_id');
        $this->db->where('so_id',$id);
        return $this->db->get();

    }

}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class shipping_koli_model extends MY_Model {

    public $table   = 'zn_shipping_koli';
    public $primary = 'shippingkoli_id';
    public $flex    = true;
    public $limitStore = true;

    public function __construct() {
        parent::__construct();
    }

    public function relation(){
        return array(
            'koli' => array('zn_koli', 'koli_id', 'koli_id'),
            'shipping' => array('zn_shipping', 'shipping_id', 'shipping_id'),
            'store' => array('m_cabang', 'id_cabang', 'store_id','shipping'),
            'storekoli' => array('m_cabang', 'id_cabang', 'store_id','koli'),
            'employee' => array('m_pegawai','user_id','user_id','shipping'),
            'supplier' => array('m_supplier','id_supplier','supplier_id','koli'),
        );
    }

}
<?php
class stok_sparepart_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'm_stok_sparepart';
        $this->primary= 'id_stok_sparepart';
    }

    public function relation(){
    	return array(
    		'sparepart' => array('m_sparepart','id_sparepart','id_sparepart'),
    	);
    }

}
<?php
class po_store_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->flex = true;
        $this->limitStore = false;
        $this->table    = 'zn_po_store';
        $this->primary  = 'postore_id';
    }

    public function relation(){
        return array(
            'po' => array('zn_purchase_order', 'po_id', 'po_id'),
            'detail' => array('zn_po_store_detail','postore_id', 'postore_id'),
            'store' => array('m_cabang','id_cabang', 'store_id'),
            'supplier' => array('m_supplier', 'id_supplier', 'supplier_id'),
            'prdetail' => array('zn_purchase_request_detail', 'prdetail_id', 'prdetail_id', 'detail'),
            'item' => array('m_item', 'item_id', 'item_id', 'detail_po'),
            'owner' => array('m_owner', 'id_owner', 'id_owner', 'store'),
            'postore_koli' => array('zn_receiving_postore_koli', 'postore_id', 'postore_id'),
            'detail_po' => array('zn_purchase_order_detail','postore_id','postore_id'),
            
            'koli' => array('zn_koli','koli_id','koli_id','postore_koli'),

            // 'shipping_koli '
            // 'shipping' => array('zn_shipping','shipping_id','shipping_id')
        );
    }

    public function update($data = NULL, $id = NULL) {
        $this->load->model('purchase_order_detail_model');

        $arset = $this->getArSet();
        if($data!=NULL)        
            $arset = array_merge($arset, $data);
        $arwhere = $this->getArWhere();
        parent::update($data, $id);
        if(array_key_exists('postore_status', $arset)){
            // jika update mengandung pr_status maka semua purchase_detail diupdate juga detail_statusnya
            $status = $arset['postore_status'];
            $model = $this->purchase_order_detail_model;

            if($id){                
                $model = $model->where('this.postore_id', $id);
            }

            if($arwhere){
                // $pos = strpos($arwhere, '`this`.`koli_id`');
                // if($pos!==false){
                //     $arwhere = str_replace('`this`.`koli_id`', '-xxx-', $arwhere);
                //     $arwhere = str_replace('`koli_id`', '-xxx-', $arwhere);
                // } else {
                //     $arwhere = str_replace('`koli_id`', '-xxx-', $arwhere);
                // }

                // $arwhere = str_replace('-xxx-', '`postore_koli`.`koli_id`', $arwhere);
                $model = $model->where($arwhere);
            }

            $model = $model
                ->with(array('postore','postore_koli','koli'))
                ->set('detail_status', $status)
                ->update();
        }
        
    }

    public function update_postore($data = NULL, $id = NULL){
        $this->db->set($data)->where_in('postore_id',$id)->update('zn_po_store');
    }
}

?>
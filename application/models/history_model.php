<?php 

class history_model extends MY_Model {

    public $table = 'zn_history';
    public $primary = 'history_id';
    public $flex = true;

    public function __construct() {
        parent::__construct();
    }

    public function relation(){
        return array(
            'store' => array('m_cabang', 'id_cabang', 'store_id'),
            'store_area' => array('zn_history_store_area', 'history_id', 'history_id'),
        );
    }

}
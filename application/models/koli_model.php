<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class koli_model extends MY_Model {

	public $table 	= 'zn_koli';
    public $primary = 'koli_id';
    public $flex 	= true;

    public function __construct() {
        parent::__construct();
    }

    public function relation(){
        return array(
            'store' => array('m_cabang', 'id_cabang', 'store_id'),
            'supplier' => array('m_supplier', 'id_supplier', 'supplier_id'),
            'shipping' => array('zn_shipping_koli', 'koli_id', 'koli_id'),
            'sp' => array('zn_shipping', 'shipping_id','shipping_id','shipping'),

            'postore_koli' => array('zn_receiving_postore_koli', 'koli_id', 'koli_id'),
            'postore' => array('zn_po_store', 'postore_id', 'postore_id', 'postore_koli'),
            'receiving' => array('zn_receiving','receiving_id','receiving_id','postore_koli'),
            'po_detail' => array('zn_purchase_order_detail','postore_id','postore_id','postore_koli'),
            'item' => array('m_item','item_id','item_id','po_detail'),
            'po' => array('zn_purchase_order','po_id','po_id','postore'),
            'pr_status' => array('m_pr_status','pr_id','koli_status'),
        );
    }

    public function update_koli($data,$where){
        $this->db->where('koli_id',$where);
        $this->db->update($this->table,$data);
    }

    public function update($data = NULL, $id = NULL) {
        $this->load->model('po_store_model');

        $arset = $this->getArSet();
        if($data!=NULL)        
            $arset = array_merge($arset, $data);
        $arwhere = $this->getArWhere();
        parent::update($data, $id);
        if(array_key_exists('koli_status', $arset)){
            // jika update mengandung pr_status maka semua purchase_detail diupdate juga detail_statusnya
            $status = $arset['koli_status'];
            $model = $this->po_store_model;

            if($id){                
                $model = $model->where('postore_koli.koli_id', $id);
            }

            if($arwhere){
                $pos = strpos($arwhere, '`this`.`koli_id`');
                if($pos!==false){
                    $arwhere = str_replace('`this`.`koli_id`', '-xxx-', $arwhere);
                    $arwhere = str_replace('`koli_id`', '-xxx-', $arwhere);
                } else {
                    $arwhere = str_replace('`koli_id`', '-xxx-', $arwhere);
                }

                $arwhere = str_replace('-xxx-', '`postore_koli`.`koli_id`', $arwhere);
                $model = $model->where($arwhere);   
            }

            $model = $model
                ->with(array('postore_koli','koli'))
                ->set('postore_status', $status)
                ->update();
        }
        
    }

}

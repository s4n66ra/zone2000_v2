<?php
class kategori_mesin_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table    = 'm_kategori_mesin';
        $this->primary  = 'id_kategori_mesin';
    }

    private function data($condition = array()) {        
        $this->db->from($this->table);
        $this->db->where_condition($condition);
        return $this->db;
    }

    public function get_by_id($id) {
        $condition[$this->primary] = $id;
        $this->data($condition);
        return $this->db->get();
    }


    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function data_table() {
        // Total Record
        $total = $this->data()->count_all_results();

        // List Data
        //$this->db->limit($this->limit, $this->offset);
        $data = $this->data()->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $id = $value->{$this->primary};
            $id_enc = url_base64_encode($id);
            $action = '';
            $action .= '';

            if ($this->access_right->otoritas('edit')) {
                $action .= view::button_edit($id_enc);
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= view::button_delete($id_enc);
            }

            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $rows[] = array(
                    'primary_key' =>$value->{$this->primary},
                    'kd_kategori_mesin' => $value->kd_kategori_mesin,
                    'nama_kategori_mesin' => $value->nama_kategori_mesin,
                    'aksi' => view::render_button_group_raw($action),
                );
            }else{
                $rows[] = array(
                    'primary_key' =>$value->{$this->primary},
                    'kd_kategori_mesin' => $value->kd_kategori_mesin,
                    'nama_kategori_mesin' => $value->nama_kategori_mesin,
                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    }      

    public function create($data) {	
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->table, $data, array($this->primary => $id));
    }


    public function delete($id) {
        return $this->db->delete($this->table, array($this->primary => $id));
    }

    
    
    public function options($default = '--Pilih Kode Kategori Mesin--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->{$this->primary}] = $row->nama_kategori_mesin ;
        }
        return $options;
    }


    public function options_empty() {
        $data = $this->data()->get();
        $options = array();
        $options['']  = '';
        foreach ($data->result() as $row) {
            $options[$row->{$this->primary}] = $row->nama_kategori_mesin ;
        }
        return $options;
    }

    public function raiseSequence($kategori_id = NULL){
        $seq = 'current_id';
        //$data = array('current_id'=>'current_id + 1');
        //$this->update($data,$kategori_id);
        $this->db->set($seq, $seq.'+1', FALSE)->where('id_kategori_mesin',$kategori_id)->update($this->table);
        //return $this->get()->row_array()[$seq];
        return $this->get()->row_array();
    }

    
}

?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class receiving_ticket_model extends MY_Model {

    public $table   = 'zn_receiving';
    public $primary = 'receiving_id';
    public $flex    = true;
    public $limitStore = true;

    public function __construct() {
        parent::__construct();
    }

    public function relation(){
        return array(
            'store' => array('m_cabang', 'id_cabang', 'store_id'),
            'employee' => array('m_pegawai','user_id','user_id'),
        );
    }

    public function create($data = array()){
        $this->load->model('transaction_model');
        $data['user_id']    = $this->session->userdata('user_id');
        $data['store_id']  = $this->session->userdata('store_id');
        //$data['date_created'] = date::now_sql();
        $this->transaction_model->transactiontype_id = transactiontype::RECEIVING;
        $transaction_id = $this->transaction_model->createTransaction();
        
        $data['transaction_id'] = $transaction_id;
        $this->db->last_query();
        parent::create($data);
    }

    public function getpostore_id($receiving_id){
        $this->db->select('postore_id,koli.koli_id,qty,note,koli_code,date_created');
        $this->db->where('receiving.receiving_id',$receiving_id);
        $this->db->from('zn_receiving_postore_koli receiving');
        $this->db->join('zn_koli koli','koli.koli_id = receiving.koli_id');
        $this->db->join('zn_receiving','zn_receiving.receiving_id = receiving.receiving_id');
        return $this->db->get()->row();

    }

}
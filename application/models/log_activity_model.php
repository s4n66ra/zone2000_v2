<?php

class log_activity_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $table = 'zn_log';
   // private $table2 = 'hr_propinsi';

    private function data($condition = array()) {
        $this->db->from($this->table . ' a');
        $this->db->join($this->table2 . ' b', 'a.nik = b.nik', 'left');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_by_id($id) {
        $condition['a.id'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function data_table() {
        // Filtering
        $condition = array();
        $tgl_akses = $this->input->post('tgl_akses');
		$tgl_akses2 = $this->input->post('tgl_akses2');
		$username = $this->input->post('username');
		$activity = $this->input->post('activity');
		$nama = $this->input->post('nama_pegawai');
		
		// buat pencarian
        if (!empty($tgl_akses) && !empty($tgl_akses2)) {
			 $condition['date(a.tgl_akses) >= '] = hgenerator::switch_tanggal($tgl_akses);
			 $condition['date(a.tgl_akses) <= '] = hgenerator::switch_tanggal($tgl_akses2);
 
        }
		if(!empty($username)) {
			$condition['LOWER(a.username) LIKE ' . " '%" . strtolower($username) . "%' "] = null;
		}
		if(!empty($activity)) {
			$condition['LOWER(a.activity) LIKE ' . " '%" . strtolower($activity) . "%' "] = null;
		}
		if(!empty($nama)) {
			$condition['LOWER(b.nama) LIKE ' . " '%" . strtolower($nama) . "%' "] = null;
		}

        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        $this->db->order_by('a.id','desc');
        $this->db->limit($this->limit, $this->offset);
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {

            $id = $value->id;  // ??
			// buat button edit n hapus dan pake otoritas juga
			$action = '';
			// jangan lupa pake hak akses buat di gambar edit / hapus
			// if ($this->access_right->otoritas('edit')) {
				//$action .= anchor(null, '<i class="icon-edit"></i>', array('id' => 'button-edit-' . $id, 'class' => 'btn', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('referensi_jenis_kompetensi/edit/' . $id))) . ' ';
			//}
			 /*if ($this->access_right->otoritas('delete')) {
				$action = anchor(null, '<i class="icon-trash"></i>', array('id' => 'button-delete-' . $id, 'class' => 'btn', 'onclick' => 'delete_row(this.id)', 'data-source' => base_url('log_activity/delete/' . $id)));
			}*/
            $rows[] = array(
				'id' => $value->id,
                'nik' => $value->nama,
				'username' => $value->username,
				'tgl_akses' => !empty($value->tgl_akses) ? date('d-m-y H:i:s', strtotime($value->tgl_akses)) : '-',
				'activity' => $value->activity,		
               // 'nama_propinsi' => $value->nama_propinsi,
                'aksi' => !empty($action) ? $action : '<i class="icon-lock denied-color" title="Acces Denied"></i>'
            );
        }

        return array('rows' => $rows, 'total' => $total);
    }

   /* public function options($default = '--Pilih Jenis Kompetensi--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->jenis_kompetensi] = $row->nama_jenis;
        }
        return $options;
    }*/

    public function create($data) {
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->table, $data, array('jenis_kompetensi' => $id));
    }

    public function delete($id) {
        return $this->db->delete($this->table, array('id' => $id));
    }

    //added by Nisa
    public function get_all() {
        $hasil = array();
        $query = $this->db->get($this->table);
        foreach ($query->result() as $data) {
            $hasil[] = $data;
        }
        return $hasil;
    }

}

?>
<?php
class notification_pr_model extends MY_Model {

    public $id;

    public function __construct() {
        parent::__construct();
        $this->flex = true;
        $this->table    = 'zn_notification_pr';
        $this->primary  = 'id';
    }

    public function relation(){
        return array(
            'purchase_request' => array('zn_purchase_request','pr_id', 'pr_id'),
            'notif' => array('zn_notification', 'notif_id', 'notif_id'),
        );
    }

    
}

?>
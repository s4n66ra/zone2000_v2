<?php
class pulau_model extends MY_Model {

    private $pulau  = 'm_pulau';
    public $primary_key = 'id_pulau';

    public function __construct() {
        parent::__construct();
        $this->table = 'm_pulau';
    }

    private function data($condition = array()) {        
        $this->db->from($this->pulau);
        $this->db->where_condition($condition);
        return $this->db;
    }

    public function get_by_id($id) {
        $condition['id_pulau'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function data_table() {
        // Total Record
        $total = $this->data()->count_all_results();

        // List Data
        $this->db->limit($this->limit, $this->offset);
        //$this->db->limit($this->limit, 0);
        $data = $this->data()->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $id = $value->id_pulau;
            $id_enc = url_base64_encode($id);
            $action = array();

            if ($this->access_right->otoritas('edit')) {
                $action[] = view::button_edit($id_enc);
            }

            if ($this->access_right->otoritas('delete')) {
                $action[] = view::button_delete($id_enc);
            }

            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $rows[] = array(
                    'primary_key' =>$value->id_pulau,
                    'kd_pulau' => $value->kd_pulau,
                    'nama_pulau' => $value->nama_pulau,
                    'aksi' => view::render_button_group($action),
                );
            }else{
                $rows[] = array(
                    'primary_key' =>$value->id_pulau,
                    'kd_pulau' => $value->kd_pulau,
                    'nama_pulau' => $value->nama_pulau
                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    }       

    public function create($data) {	
        return $this->db->insert($this->pulau, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->pulau, $data, array('id_pulau' => $id));
    }

    public function delete($id) {
        return $this->db->delete($this->pulau, array('id_pulau' => $id));
    }
    
    public function options($default = '--Pilih Pulau--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_pulau] = $row->nama_pulau ;
        }
        return $options;
    }

    public function options_empty() {
        $data = $this->data()->get();
        $options = array();
        $options['']  = '';
        foreach ($data->result() as $row) {
            $options[$row->id_pulau] = $row->nama_pulau ;
        }        
        return $options;
    }
    
}

?>
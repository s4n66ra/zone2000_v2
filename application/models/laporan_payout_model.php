<?php
class laporan_payout_model extends MY_Model {
    //public $limitStore = true;
    public function __construct() {
        parent::__construct();
        $this->table = 'zn_transaction';
        $this->primary='transaction_id';
        $this->flex = true;
    }
    public $selected_store = '';
    public $limit = 1000;
    public $offset;
    private $hadiah  = 'm_hadiah';
    private $table2 = 'supplier';
    private $table3 = 'purchase_order';
    private $table4 = 'bbm_barang';
    private $table5 = 'barang';
    private $table6 = 'satuan';
    private $table7 = 'kategori';

    public function relation(){
        return array(
            'm_tekno' => array('m_mesin_teknogame','kd_mesin','kode_mesin'),
            'm_mesin' => array('m_mesin', 'id_mesin', 'id_mesin','m_tekno'),
            'zn_mesin' => array('zn_transaction_machine','transaction_id', 'transaction_id'),
            'zn_trans' => array('zn_transaction', 'transaction_id', 'transaction_id','zn_mesin'),
            'm_jenis'  => array('m_jenis_mesin','id_jenis_mesin','id_jenis_mesin','m_mesin')
        );
    }

    private function data($condition = array()) {        
        $this->db->from($this->hadiah);
        $this->db->where_condition($condition);
        return $this->db;
    }

    function data_detail($condition = array()) {
        $sistem_date = $this->session->userdata['sistem_date'];
        if(!empty($sistem_date)){
            $time=strtotime($sistem_date);
            $month=date("m",$time);
            $year=date("Y",$time);
        }else{
            $month = date("m");
            $year = date("Y");
        }
        // =============Filtering===============
        //$condition = array();
        if($condition["id_cabang"]){
            $filter ="and (rec_created <= '".$year.'-'.$month."-31') and (rec_created >= '".$year.'-'.$month."-01') and cabang.id_cabang=".$condition["id_cabang"];
        }else{
            $filter ="and (rec_created <= '".$year.'-'.$month."-31') and (rec_created >= '".$year.'-'.$month."-01')";
        }
        $filter_tekno = " where (tanggal <= '".$year.'-'.$month."-31')and (tanggal >= '".$year.'-'.$month."-01')";
        $query = "SELECT
                `nama_jenis_mesin` AS NO,
                `nama_jenis_mesin`,
                `serial_number`,
                `nama_cabang`,
                sum(num_ticket) as tiket,
                sum(num_ticket) * 30 as jum_tiket,
                jumlah
            FROM
                (
                    SELECT
                        `this`.`kode_mesin`,
                        sum(harga) AS jumlah
                    FROM
                        (
                            `zn_transaction_teknogame` this
                        ) 
                    $filter_tekno
                    GROUP BY
                        `this`.`kode_mesin`
                    ORDER BY
                        `jumlah` DESC
                ) AS zn_tekno
            LEFT JOIN `m_mesin_teknogame` m_tekno ON `m_tekno`.`kd_mesin` = `zn_tekno`.`kode_mesin`
            LEFT JOIN `m_mesin` m_mesin ON `m_mesin`.`id_mesin` = `m_tekno`.`id_mesin`
            LEFT JOIN `zn_transaction_machine` zn_mesin ON `zn_mesin`.`machine_id` = `m_mesin`.`id_mesin`
            LEFT JOIN `zn_transaction` zn_trans ON `zn_trans`.`transaction_id` = `zn_mesin`.`transaction_id`
            LEFT JOIN `m_jenis_mesin` m_jenis ON `m_jenis`.`id_jenis_mesin` = `m_mesin`.`id_jenis_mesin`
            LEFT JOIN `m_cabang` cabang ON `cabang`.`id_cabang` = `m_mesin`.`id_cabang`
            WHERE
                zn_trans.transactiontype_id = 26
                $filter
            GROUP BY
                zn_mesin.machine_id";

            /*"ORDER BY
                `kode_mesin`,
                `nama_jenis_mesin` ASC
                ";*/
        //--------end filtering-----------------------------------


        $result = $this->db->query($query);
        //$row = $result->result();
        //var_dump($row);
        $no = 0;
        if($result->num_rows() >0){
            foreach ($result->result() as $row) {
                # code...
                $temp[$no]['no'] = $row->nama_jenis_mesin;
                $temp[$no]['machine'] = $row->nama_jenis_mesin;
                $temp[$no]['register'] = $row->serial_number;
                $temp[$no]['nama_cabang'] = $row->nama_cabang;
                $temp[$no]['tiket'] = $row->tiket;
                $temp[$no]['value'] = $row->jum_tiket;
                $temp[$no]['omzet'] = $row->jumlah;
                $temp[$no]['payout'] = hgenerator::rupiah($row->jum_tiket*100/$row->jumlah).'%';
                $no++;
            }
        }else{
             $temp[$no]['no'] = '';
                $temp[$no]['machine'] = '';
                $temp[$no]['register'] = '';
                $temp[$no]['nama_cabang'] = '';
                $temp[$no]['tiket'] = '';
                $temp[$no]['value'] = '';
                $temp[$no]['omzet'] = '';
                $temp[$no]['payout'] = '';
        }
        $this->selected_store = $temp[$no]['nama_cabang'];
        return $temp;
    }

    public function get_by_id($id) {
        $condition['selling_id'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_detail_by_id($id) {
        $condition['b.id'] = $id;
        $this->data_detail($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function data_table() {
        // Total Record
        $total = $this->data()->count_all_results();

        // List Data
        //$this->db->limit($this->limit, $this->offset);
        $data = $this->data()->get()->result();
        $rows = array();

        foreach ($data as $key => $value) {
            $id = $value->id_hadiah;
            $id_enc = url_base64_encode($id);
            $action = '';
            $action .= '';

            if ($this->access_right->otoritas('edit')) {
                $action .= view::button_edit($id_enc, array('full-width'=>0));
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= view::button_delete($id_enc, array('full-width'=>0));
            }

            $temp = array(
                'primary_key' =>$value->id_hadiah,
                'kd_hadiah' => $value->kd_hadiah,
                'nama_hadiah' => $value->nama_hadiah,
                'harga' => $value->harga,
                'jumlah_tiket' => $value->jumlah_tiket,
                'aksi' => $action
            );

            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $temp['aksi'] = view::render_button_group_raw($action);
            }
            $rows[] = $temp;
            
        }

        return array('rows' => $rows, 'total' => $total);
    }   
 

    /*public function create($data) {
        $return = false;
        $this->db->trans_start();
        
        // INSERT TO ITEM FIRST
        $this->load->model('item_model');
        $item['item_name'] = $data['nama_hadiah'];
        $item['item_key']  = $data['item_key'];
        $item['harga']     = $data['harga'];
        $item['itemtype_id'] = 2;
        $this->item_model->create($item);
        $item_id = $this->db->insert_id();

        // GET ITEM_ID THEN INSERT TO HADIAH
        $data['item_id'] = $item_id;
        $return = $this->db->insert($this->hadiah, $data);
        $this->db->trans_complete();

        return $return;
    }

    public function update($data, $id) {
        $this->db->trans_start();

        // update hadiah
        $data['harga']     = hgenerator::switch_number($data['harga']);
        $return = $this->db->update($this->hadiah, $data, array('id_hadiah' => $id));


        $this->load->model('item_model');
        $item['item_name'] = $data['nama_hadiah'];
        $item['item_key']  = $data['item_key'];
        $item['harga']     = $data['harga'];

        $itemObject = $this->item_model->with('hadiah')->where('id_hadiah', $id)->get()->row();
        if($itemObject->item_id){
            //update item            
            $this->item_model->update($item, $itemObject->item_id);
        }else{
            // insert new
            $item['itemtype_id'] = 2;
            $this->item_model->create($item);
        }
        
        $this->db->trans_complete();
        return $return;
    }

   

    public function delete($id) {
        return $this->db->delete($this->hadiah, array('id_hadiah' => $id));
    }*/

    
    
    public function options($default = '--Pilih hadiah--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_hadiah] = $row->nama_hadiah ;
        }
        return $options;
    }

    public function options_empty() {
        $data = $this->data()->get();
        $options = array();
        $options['']  = '';
        foreach ($data->result() as $row) {
            $options[$row->id_hadiah] = $row->nama_hadiah ;
        }
        return $options;
    }
    

    public function data_table_detail_excel() {
        // Total Record
        $total = $this->data_detail()->count_all_results();

        // List Data
        $data_detail = $this->data_detail()->get();
        $rows = array();

        foreach ($data_detail->result() as $value) {
            $id = $value->id_bbm_barang;
            $action = '';
            $rows[] = array(
                'primary_key' => $id,
                'kd_barang'=>$value->kd_barang,
                'nama_barang' => $value->nama_barang,
                'jumlah' => $value->jumlah_barang,
                'satuan' => $value->nama_satuan,
                'kategori' => $value->nama_kategori,
            );
        }

        return array('rows' => $rows, 'total' => $total);
    }    

    public function options_jenis() {

        $data = $this->db
                ->from('m_jenis_selling')
                ->get()->result();


        $options = array();
        //$options['']  = '';
        foreach ($data as $row) {
            $options[$row->jenis_selling_id] =$row->nama_jenis ;
        }
        return $options;
    }

    
}

?>
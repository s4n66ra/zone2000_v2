<?php

/**
 * Description of roles_model
 *
 * @author Warman Suganda
 */
class roles_model extends MY_Model {

    public $table = 'm_roles';
    public $primary = 'roles_id';
    public $flex = true;

    function __construct() {
        parent::__construct();
    }

    public function relation(){
        return array(
            'group' => array('m_grup','grup_id', 'grup_id'),
            'menu' => array('m_menu', 'kd_menu', 'menu_id'),
            'group_menu' =>array('m_grup_menu', 'kd_grup_menu', 'kd_grup_menu', 'menu')
        );
    }

    public $limit;
    public $offset = 0;
    private $table1 = 'm_grup';
    private $table2 = 'm_roles';
    private $table3 = 'm_menu';

    private function data($condition = array()) {
        $this->db->from($this->table1 . ' a');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_by_id($id) {
        $condition['a.grup_id'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    private function data_roles($condition = array()) {
        $this->db->select('a.*, b.kd_menu, b.nama_menu, b.url, b.kd_parent, b.kd_grup_menu');
        $this->db->from($this->table2 . ' a');
        $this->db->join($this->table3 . ' b', 'a.menu_id = b.kd_menu');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_roles_by_id($id) {
        $condition['a.roles'] = $id;
        $this->data_roles($condition);
        return $this->db->get();
    }

    public function get_data_roles($condition = array()) {
        $this->data_roles($condition);
        $this->db->order_by('b.menu_urutan');
        return $this->db->get();
    }

    public function parsing_roles($condition = array()) {
        $list = $this->get_data_roles($condition);
        $data = array(
            'is_view' => array(), 'is_add' => array(), 'is_edit' => array(), 'is_delete' => array(), 'is_approve' => array(), 'is_import' => array(), 'is_print' => array()
        );
        foreach ($list->result() as $value) {
            $key = $value->menu_id;
            if ($value->is_view == 't')
                $data['is_view'][] = $key;
            if ($value->is_add == 't')
                $data['is_add'][] = $key;
            if ($value->is_edit == 't')
                $data['is_edit'][] = $key;
            if ($value->is_delete == 't')
                $data['is_delete'][] = $key;
            if ($value->is_approve == 't')
                $data['is_approve'][] = $key;
            if ($value->is_import == 't')
                $data['is_import'][] = $key;
            if ($value->is_print == 't')
                $data['is_print'][] = $key;
        }
        return $data;
    }

    public function options($default = '--Pilih Roles--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->grup_id] = $row->grup_nama;
        }
        return $options;
    }


    public function options_unit($default = '--Pilih Unit--', $key = '') {
        $this->db->from('hr_ref_unit_kerja a');

        $data = $this->db->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->kd_unit_kerja] = $row->kd_unit_kerja;
        }
        return $options;
    }

    public function delete($id) {
        $this->db->trans_begin();

        // Insert data grup roles
        $this->db->delete($this->table1, array('grup_id' => $id));
        $this->db->delete($this->table2, array('grup_id' => $id));

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function data_table() {
        // Filtering
        $condition = array();
        $nama_role = $this->input->post('kata_kunci');
        $deskripsi = $this->input->post('deskripsi');
		
        if (!empty($nama_role))
			$condition["a.grup_id"] = $nama_role;
        if (!empty($deskripsi))
            $condition["lower(a.grup_deskripsi) LIKE '%" . strtolower($deskripsi) . "%'"] = null;

        // Total Record
        $total = $this->data($condition)->count_all_results();

        // List Data
        $this->db->order_by('a.grup_id');
        //$this->db->limit($this->limit, $this->offset);
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {

            $id = $value->grup_id;
            $id_enc = url_base64_encode($id);
            $action = '';
            if ($this->access_right->otoritas('edit')) {
                //$action .= '<a href="'.base_url("roles/edit/$id_enc").'" class="btn default btn-xs purple"><i class="fa fa-edit"></i> Edit </a>';
                $action.= view::button_edit($id_enc, array('onclick'=>'btnLoadNextPage(this)'));
            }

            if ($this->access_right->otoritas('delete')) {
                //$action .= '<a href="'.base_url("roles/delete/$id_enc").'" class="btn default btn-xs black"><i class="fa fa-edit"></i> Delete </a>';
                $action.= view::button_delete($id_enc, array('onclick'=>'btnLoadNextPage(this)'));
            }
            $rows[] = array(
                'grup_nama' => $value->grup_nama,
                'grup_deskripsi' => $value->grup_deskripsi,
                'action' => !empty($action) ? view::render_button_group_raw($action) : '<i class="icon-lock denied-color" title="Acces Denied"></i>'
            );
        }

        return array('rows' => $rows, 'total' => $total);
    }

    public function create($data) {
        $this->db->trans_begin();

        // Insert data grup roles
        $this->db->insert($this->table1, array('grup_nama' => $data['grup_nama'], 'grup_deskripsi' => $data['grup_deskripsi'], 'otoritas_data' => $data['otoritas_data']));
        $grup_id = $this->db->insert_id();

        // Save roles
        $this->save_roles($data, $grup_id);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function update($data, $id) {
        $this->db->trans_begin();

        // Insert data grup roles
        $this->db->update($this->table1, array('grup_nama' => $data['grup_nama'], 'grup_deskripsi' => $data['grup_deskripsi']), array('grup_id' => $id));

        // Save roles
        $this->save_roles($data, $id);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function save_roles($data, $grup_id) {

        // Delete Roles
        $this->db->delete($this->table2, array('grup_id' => $grup_id));

        // Parsing roles
        $roles = array();

        if (isset($data['otoritas_menu_view']) && is_array($data['otoritas_menu_view'])) {
            foreach ($data['otoritas_menu_view'] as $value) {
                $roles[$value]['is_view'] = 't';
            }
        }

        if (isset($data['otoritas_menu_add']) && is_array($data['otoritas_menu_add'])) {
            foreach ($data['otoritas_menu_add'] as $value) {
                $roles[$value]['is_add'] = 't';
            }
        }

        if (isset($data['otoritas_menu_edit']) && is_array($data['otoritas_menu_edit'])) {
            foreach ($data['otoritas_menu_edit'] as $value) {
                $roles[$value]['is_edit'] = 't';
            }
        }

        if (isset($data['otoritas_menu_delete']) && is_array($data['otoritas_menu_delete'])) {
            foreach ($data['otoritas_menu_delete'] as $value) {
                $roles[$value]['is_delete'] = 't';
            }
        }

        if (isset($data['otoritas_menu_approve']) && is_array($data['otoritas_menu_approve'])) {
            foreach ($data['otoritas_menu_approve'] as $value) {
                $roles[$value]['is_approve'] = 't';
            }
        }

        if (isset($data['otoritas_menu_export']) && is_array($data['otoritas_menu_export'])) {
            foreach ($data['otoritas_menu_export'] as $value) {
                $roles[$value]['is_import'] = 't';
            }
        }

        if (isset($data['otoritas_menu_print']) && is_array($data['otoritas_menu_print'])) {
            foreach ($data['otoritas_menu_print'] as $value) {
                $roles[$value]['is_print'] = 't';
            }
        }

        foreach ($roles as $menu_id => $value) {
            $data_roles = array(
                'menu_id' => $menu_id,
                'grup_id' => $grup_id,
                'is_view' => isset($value['is_view']) ? $value['is_view'] : 'f',
                'is_add' => isset($value['is_add']) ? $value['is_add'] : 'f',
                'is_edit' => isset($value['is_edit']) ? $value['is_edit'] : 'f',
                'is_delete' => isset($value['is_delete']) ? $value['is_delete'] : 'f',
                'is_approve' => isset($value['is_approve']) ? $value['is_approve'] : 'f',
                'is_import' => isset($value['is_import']) ? $value['is_import'] : 'f',
                'is_print' => isset($value['is_print']) ? $value['is_print'] : 'f'
            );

            $this->db->insert($this->table2, $data_roles);
        }
    }

}

/* End of file roles_model.php */
/* Location: ./application/models/roles_model.php */
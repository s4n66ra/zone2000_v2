<?php
class provinsi_model extends MY_Model {

    public $table = 'm_provinsi';
    public $primary_key = 'id_provinsi';

    public function __construct() {
        parent::__construct();
    }

    public $limit;
    public $offset;
    private $provinsi  = 'm_provinsi';
    private $pulau = 'm_pulau';
    private $table2 = 'supplier';
    private $table3 = 'purchase_order';
    private $table4 = 'bbm_barang';
    private $table5 = 'barang';
    private $table6 = 'satuan';
    private $table7 = 'kategori';

    private function data($condition = array()) {        
        $this->db->from($this->provinsi);
        $this->db->join($this->pulau,$this->pulau.'.id_pulau='.$this->provinsi.'.id_pulau');
        $this->db->where_condition($condition);
        return $this->db;
    }

    

    public function get_by_id($id) {
        $condition['id_provinsi'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_detail_by_id($id) {
        $condition['b.id'] = $id;
        $this->data_detail($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function data_table() {
        // Total Record
        $total = $this->data()->count_all_results();
        
        $condition = array();

        // List Data
        //$this->db->limit($this->limit, $this->offset);
        $data = $this->data($condition)->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $id = $value->id_provinsi;
            $id_enc = url_base64_encode($id);
            $action = '';
            $action .= '';

            if ($this->access_right->otoritas('edit')) {
                $action .= view::button_edit($id_enc);
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= view::button_delete($id_enc);
            }

            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $rows[] = array(
                    'primary_key' =>$value->id_provinsi,
                    'kd_provinsi' => $value->kd_provinsi,
                    'nama_provinsi' => $value->nama_provinsi,
                    'nama_pulau' => $value->nama_pulau,
                    'aksi' => $action
                );
            }else{
                $rows[] = array(
                    'primary_key' =>$value->id_provinsi,
                    'kd_provinsi' => $value->kd_provinsi,
                    'nama_provinsi' => $value->nama_provinsi,
                    'nama_pulau' => $value->nama_pulau
                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    }   

    public function create($data) {	
        return $this->db->insert($this->provinsi, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->provinsi, $data, array('id_provinsi' => $id));
    }

    public function create_detail($data) { 
        return $this->db->insert($this->table4, $data);
    }

    public function update_detail($data, $id) {
        return $this->db->update($this->table4, $data, array('id' => $id));
    }

    public function delete($id) {
        return $this->db->delete($this->provinsi, array('id_provinsi' => $id));
    }

    public function delete_detail($id) {
        return $this->db->delete($this->table4, array('id' => $id));
    }
    
    
    public function options($default = '--Pilih Provinsi--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_provinsi] = $row->nama_provinsi ;
        }
        return $options;
    }

    public function options_empty() {
        $data = $this->data()->get();
        $options = array();
        $options['']  = '';
        foreach ($data->result() as $row) {
            $options[$row->id_provinsi] = $row->nama_provinsi ;
        }
        return $options;
    }

    

    public function data_table_detail_excel() {
        // Total Record
        $total = $this->data_detail()->count_all_results();

        // List Data
        $data_detail = $this->data_detail()->get();
        $rows = array();

        foreach ($data_detail->result() as $value) {
            $id = $value->id_bbm_barang;
            $action = '';
            $rows[] = array(
                'primary_key' => $id,
                'kd_barang'=>$value->kd_barang,
                'nama_barang' => $value->nama_barang,
                'jumlah' => $value->jumlah_barang,
                'satuan' => $value->nama_satuan,
                'kategori' => $value->nama_kategori,
            );
        }

        return array('rows' => $rows, 'total' => $total);
    }    

    
}

?>
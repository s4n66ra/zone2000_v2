<?php 

class history_store_area_model extends MY_Model {

    public $table = 'zn_history_store_area';
    public $primary = 'historystorearea_id';
    public $flex = true;

    public function __construct() {
        parent::__construct();
    }

    public function relation(){
        return array(
            'history' => array('zn_history', 'history_id', 'history_id'),
            'store' => array('m_cabang', 'id_cabang', 'store_id', 'history'),
        );
    }

}
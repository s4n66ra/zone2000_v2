<?php 

class status_pr_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->flex = true;
        $this->table = 'm_status_pr';
        $this->primary = 'statuspr_id';
    }

    public function relation(){
        return array(
            'status' => array("m_status", "status_id", "status_id"),
            'expired'=> array('m_status_expired','status_id', 'status_id'),
        );
    }

}
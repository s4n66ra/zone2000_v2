<?php
class sparepart_model extends MY_Model {
    public $limitStore = FALSE;
    public function __construct() {
        parent::__construct();
        $this->table = 'm_sparepart';
        $this->primary = 'id_sparepart';
    }

    public $limit;
    public $offset;
    private $sparepart  = 'm_sparepart';
    private $table2 = 'supplier';
    private $table3 = 'purchase_order';
    private $table4 = 'bbm_barang';
    private $table5 = 'barang';
    private $table6 = 'satuan';
    private $table7 = 'kategori';

    private function data($condition = array()) {        
        $this->db->from($this->sparepart);
        $this->db->where_condition($condition);
        return $this->db;
    }

    private function data_detail($condition = array()) {
        // =============Filtering===============
        //$condition = array();
        $kd_bbm= $this->input->post("kd_bbm");
        $id_supplier= $this->input->post("id_supplier");
        $id_purchase_order = $this->input->post('id_purchase_order');

        $id_bbm = $this->input->post('id_bbm');
        if(!empty($kd_bbm)){
            $condition["a.kd_bbm like '%$kd_bbm%'"]=null;
        }
        if(!empty($id_supplier)){
            $condition["a.id_supplier"]=$id_supplier;
        }
        if(!empty($id_purchase_order)){
            $condition["a.id_purchase_order"]=$id_purchase_order;
        }
        if(!empty($id_bbm)){
            $condition["a.id_bbm"]=$id_bbm;
        }

        $tanggal_awal = hgenerator::switch_tanggal($this->input->post('tanggal_awal'));
        $tanggal_akhir = hgenerator::switch_tanggal($this->input->post('tanggal_akhir'));
        //--------end filtering-----------------------------------


        $this->db->select('*,b.id as id_bbm_barang');
        $this->db->from($this->table  . ' a');
        $this->db->join($this->table4 . ' b',' a.id_bbm = b.id_bbm');
        $this->db->join($this->table5 . ' c',' b.id_barang = c.id_barang','left');
        $this->db->join($this->table6 . ' d',' c.id_satuan = d.id_satuan','left');
        $this->db->join($this->table7 . ' e',' e.id_kategori = c.id_kategori','left');
        $this->db->order_by('c.kd_barang ASC');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_by_id($id) {
        $condition['id_sparepart'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_detail_by_id($id) {
        $condition['b.id'] = $id;
        $this->data_detail($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function data_table() {
        // Total Record
        $total = $this->data()->count_all_results();

        // List Data
        //$this->db->limit($this->limit, $this->offset);
        $data = $this->data()->get()->result();
        $rows = array();

        foreach ($data as $key => $value) {
            $id = $value->id_sparepart;
            $id_enc = url_base64_encode($id);
            $action = '';
            $action .= '';

            if ($this->access_right->otoritas('edit')) {
                $action .= view::button_edit($id_enc, array('full-width'=>0));
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= view::button_delete($id_enc, array('full-width'=>0));
            }

            $temp = array(
                'primary_key' =>$value->id_sparepart,
                'kd_sparepart' => $value->kd_sparepart,
                'code_exist'=>$value->code_exist,
                'nama_sparepart' => $value->nama_sparepart,
                'harga' => $value->harga,
                'aksi' => $action
            );

            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $temp['aksi'] = view::render_button_group_raw($action);
            }

            $rows[] = $temp;

        }

        return array('rows' => $rows, 'total' => $total);
    }   



    public function create($data) {	
        $return = false;
        $this->db->trans_start();

        // INSERT TO ITEM FIRST
        $this->load->model('item_model');
        $item['item_name'] = $data['nama_sparepart'];
        $item['item_key']  = $data['item_key'];
        //$item['price']     = $data['harga'];
        $item['itemtype_id'] = itemtype::ITEM_SPAREPART;
        $this->item_model->create($item);
        $item_id = $this->db->insert_id();

        // GET ITEM_ID THEN INSERT TO SPAREPART
        $data['item_id'] = $item_id;
        $this->db->insert($this->sparepart, $data);

        $this->db->trans_complete();
        return $this->db->trans_status();
    }

    public function update($data, $id) {
        $return = $this->db->where($this->primary, $id)->update($this->table, $data);
        $part = $this->getById($id)->row();

        $this->load->model('item_model');
        $item['item_name'] = $data['nama_sparepart'];
        $item['item_key']  = $data['item_key'];
        //$item['price']     = $data['harga'];
        $this->item_model->update($item, $part->item_id);

        return $return;
    }

    public function create_detail($data) { 
        return $this->db->insert($this->table4, $data);
    }

    public function update_detail($data, $id) {
        return $this->db->update($this->table4, $data, array('id' => $id));
    }

    public function delete($id) {
        return $this->db->delete($this->sparepart, array('id_sparepart' => $id));
    }

    public function delete_detail($id) {
        return $this->db->delete($this->table4, array('id' => $id));
    }
    
    
    public function options($default = '--Pilih sparepart--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_sparepart] = $row->nama_sparepart ;
        }
        return $options;
    }

    public function options_empty() {
        $data = $this->data()->get();
        $options = array();
        $options['']  = '';
        foreach ($data->result() as $row) {
            $options[$row->{$this->primary}] = $row->nama_sparepart ;
        }
        return $options;
    }
    

    public function data_table_detail_excel() {
        // Total Record
        $total = $this->data_detail()->count_all_results();

        // List Data
        $data_detail = $this->data_detail()->get();
        $rows = array();

        foreach ($data_detail->result() as $value) {
            $id = $value->id_bbm_barang;
            $action = '';
            $rows[] = array(
                'primary_key' => $id,
                'kd_barang'=>$value->kd_barang,
                'nama_barang' => $value->nama_barang,
                'jumlah' => $value->jumlah_barang,
                'satuan' => $value->nama_satuan,
                'kategori' => $value->nama_kategori,
            );
        }

        return array('rows' => $rows, 'total' => $total);
    }    


    
}

?>
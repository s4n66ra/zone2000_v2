<?php
class store_data_model extends MY_Model {

	public $flex = true;

    public function __construct() {
        parent::__construct();
        $this->table = 'zn_store_data';
        $this->primary = 'storedata_id';
    }


    public function relation(){
        return array(
            'store' => array('m_cabang', 'id_cabang', 'store_id'),
        );
    }

}
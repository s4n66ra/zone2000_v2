<?php 

class transaction_model extends MY_Model {
    public $limitStore = true;
    public $limitStoreTable = 'zn_transaction_store';
    public $table   = 'zn_transaction';
    /*public $table = '(
        SELECT
            *
        FROM
        (zn_transaction)
        ORDER BY
            rec_created DESC
    ) AS zn_transaction';*/
    public $primary = 'transaction_id';
    public $transaction_id;
    public $transactiontype_id;

    public function __construct() {
        parent::__construct();
    }

    public function set_limitStore($limitStore){
        $this->limitStore = $limitStore;
    }

    public function relation(){
        return array(
            //'store' => array('m_cabang','id_store','id_cabang'),
            'refill' => array('zn_transaction_refill', 'transaction_id', 'transaction_id'),
            'refill_out' => array('zn_transaction_refill_out', 'transaction_id', 'transaction_id'),
            'item' => array('m_item', 'item_id', 'item_id', 'zn_transaction_refill'),
            'item_out' => array('m_item', 'item_id', 'item_id', 'zn_transaction_refill_out'),
            'pegawai' => array('m_pegawai', 'user_id', 'rec_user'),
            'transaction_machine' => array('zn_transaction_machine','transaction_id','transaction_id'),
            'transaction_meter' => array('zn_transaction_meter','transaction_id','transaction_id'),
            'transaction_store' => array('zn_transaction_store','transaction_id','transaction_id'),
            'transaction_device' => array('zn_transaction_device', 'transaction_id','transaction_id'),
            'transaction_service' => array('zn_transaction_service', 'transaction_id','transaction_id'),
            'transaction_repair' => array('zn_transaction_repair', 'transaction_id','transaction_id'),
            'replenish' => array('zn_transaction_replenish', 'transaction_id','transaction_id'),
            'item_replenish' => array('m_item', 'item_id', 'item_id', 'zn_transaction_replenish'),
            'hadiah' => array('m_hadiah','item_id','item_id','m_item'),
            'machine' => array('m_mesin','id_mesin','machine_id','zn_transaction_machine'),
            'redeem' =>array('zn_transaction_redemption','transaction_id','transaction_id'),
            'item_redeem' => array('m_item','item_id','item_id','zn_transaction_redemption'),
            'cabang' => array('m_cabang', 'id_cabang', 'store_id', 'zn_transaction_store'),
            'area' => array('m_area', 'id_area', 'id_area', 'm_cabang'),
            'kota' => array('m_kota', 'id_kota', 'id_kota', 'm_cabang'),
            'province' => array('m_provinsi', 'id_provinsi', 'id_provinsi', 'm_kota'),
            'jenis_mesin' => array('m_jenis_mesin','id_jenis_mesin','id_jenis_mesin','m_mesin'),

        );
    }

    public function getMostItemRedemp_All(){
        $this->db->query("select item_name, sum(item_qty) AS jumlah from 
            zn_transaction_redemption
            LEFT JOIN m_item ON m_item.item_id = zn_transaction_redemption.item_id
            GROUP BY
                m_item.item_key
            ORDER BY
                jumlah DESC
            LIMIT 10");
        $this->db->get();
        return $this->db->result_array();

    }

    public function getTpayoAwal($num_start = 0,$machine_id = 'xx'){
        $this->db->select('num_end - '.$num_start.' as tot ',FALSE);
        $this->db->from('zn_transaction tr');
        $this->db->join('zn_transaction_machine trm','tr.transaction_id = trm.transaction_id','LEFT');
        $this->db->where('tr.transactiontype_id = 5 and machine_id = '.$machine_id.' and num_end >= '.$num_start.' and num_start <= '.$num_start, NULL);
        return $this->db->get()->row()->tot;
    }

    public function getTpayoMid($num_start = 0,$num_end = 0,$machine_id = 'xx'){
        $this->db->select('sum(num_meter) as tot ');
        $this->db->from('zn_transaction tr');
        $this->db->join('zn_transaction_machine trm','tr.transaction_id = trm.transaction_id','LEFT');
        $this->db->where('tr.transactiontype_id = 5 and machine_id = '.$machine_id.' and num_start >= '.$num_start.' and num_end <= '.$num_end, NULL);
        return $this->db->get()->row()->tot;
    }

    public function getTpayoAkhir($num_end = 0,$machine_id = 'xx'){
        $this->db->select($num_end .' - num_start as  tot ',FALSE);
        $this->db->from('zn_transaction tr');
        $this->db->join('zn_transaction_machine trm','tr.transaction_id = trm.transaction_id','LEFT');
        $this->db->where('tr.transactiontype_id = 5 and machine_id = '.$machine_id.' and num_end >= '.$num_end.' and num_start <= '.$num_end, NULL);
        return $this->db->get()->row()->tot;
    }


    public function createTransaction(){
        $user_id    = $this->session->userdata('user_id');
        $store_id   = $this->session->userdata('store_id');
        $now        = date::now_sql();

        $this->load->model('transaction_store_model');

        $trans['transactiontype_id']    = $this->transactiontype_id;
        $trans['rec_user']              = $user_id;
        $trans['rec_created']           = $now;
        parent::create($trans);
        $transaction_id = $this->db->insert_id();
        $this->transaction_id = $transaction_id;

        // insert to transaction store
        $trans_store['transaction_id']  = $transaction_id;
        $trans_store['store_id']        = $store_id;
        $this->transaction_store_model->create($trans_store);

        return $transaction_id;
    }

    public function top_md(){
        $this->db->select('item_name,jumlah');
        $this->db->from('top_md');
        return $this->db->get()->result_array();
    }

    

}
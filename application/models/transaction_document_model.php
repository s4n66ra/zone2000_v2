<?php

class transaction_document_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'zn_transaction_document';
        $this->primary = 'transactiondocument_id';
    }

}
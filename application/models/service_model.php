<?php 

class service_model extends MY_Model {

    public $limitStore = true;
    
    public function __construct() {
        parent::__construct();
        $this->table = 'zn_service';
        $this->primary = 'transactionservice_id';
        $this->flex = true;
        if($this->session->userdata('store_id') == hprotection::getSC())
            $this->limitStore = FALSE;
    }

    public function relation(){
        return array(
            'transaction_machine' => array('zn_transaction_machine','transaction_id','transaction_id'),
            'transaction_store' => array('zn_transaction_store','transaction_id','transaction_id'),
            'transaction_device' => array('zn_transaction_device', 'transaction_id','transaction_id'),
            'transaction' => array('zn_transaction', 'transaction_id','transaction_id'),
            'document' => array('zn_transaction_document', 'transaction_id','transaction_id'),
            'machine' => array('m_mesin','id_mesin','machine_id'),
            'store' => array('m_cabang', 'id_cabang', 'store_id'),
            'machine_type' => array('m_jenis_mesin','id_jenis_mesin','id_jenis_mesin','machine'),
            'pegawai' => array('m_pegawai','id_pegawai','technician_id'),
            'repair' => array('zn_transaction_repair', 'transactionservice_id', 'transactionservice_id'),
            'detail' => array('zn_service_detail','id_trans_service','transactionservice_id'),
            'status_detail'=> array('m_status_service_new','status_id','id_status','detail'),
            'store_detail' => array('m_cabang', 'id_cabang', 'id_lokasi','detail'),
            'pegawai_detail' => array('m_pegawai','user_id','user_id','detail'),
            'status_service' => array('m_status_service_new','status_id','service_status'),
            'v_detail' => array('v_service_detail','id_trans_service','transactionservice_id'),
            'status_service' => array('m_status_service_new','status_id','service_status'),
            'location'=>array('m_location_service','location_id','id_lokasi','detail'),
        );
    }

    public function getStoreId($machine_id){
        $this->db->select('id_cabang');
        $this->db->from('m_mesin');
        $this->db->where('id_mesin',$machine_id);
        return $this->db->get()->row()->id_cabang;
    }

    public function insert_detail($data){
        $this->db->insert('zn_service_detail',$data);
    }

    public function getTransactionId($id){
        $this->db->select('transaction_id');
        $this->db->from($this->table);
        $this->db->where($this->primary,$id);
        return $this->db->get()->row()->transaction_id;
    }

    public function statusService($status_id){
        $this->db->select('nama');
        $this->db->from('m_status_service_new');
        $this->db->where('status_id',$status_id);
        return $this->db->get()->row()->nama;
    }

    public function getDuedate($id_service){
        $this->db->query('select DATE_ADD(date_created,INTERVAL exp_time DAY) as due_date from zn_service_detail sd left join m_status_service_new ms on ms.status_id = sd.id_status where id_trans_service_detail = (select max(id_trans_service_detail) from zn_service_detail where id_trans_service = '.$id_service.')');
        return $this->db->get()->row()->due_date;
    }

    public function table_main(){
        $db1 = $this->load->database('default', TRUE);
        //$query = $db1->query("SELECT serial_number as no, serial_number, doc_number, nama_jenis_mesin, nama_cabang, rec_created, due_date, loc_name, nama, issue_description FROM (zn_service this) LEFT JOIN m_mesin machine ON machine.id_mesin = this.machine_id LEFT JOIN m_cabang store ON store.id_cabang = this.store_id LEFT JOIN zn_transaction transaction ON transaction.transaction_id = this.transaction_id LEFT JOIN zn_transaction_repair repair ON repair.transactionservice_id = this.transactionservice_id LEFT JOIN m_jenis_mesin machine_type ON machine_type.id_jenis_mesin = machine.id_jenis_mesin LEFT JOIN zn_transaction_document document ON document.transaction_id = this.transaction_id LEFT JOIN v_service_detail v_detail ON v_detail.id_trans_service = this.transactionservice_id LEFT JOIN m_status_service_new status_service ON status_service.status_id = this.service_status WHERE nama_cabang LIKE '%square%' ORDER BY serial_number asc LIMIT 10");
        $query = $db1->query('select * from m_cabang');
        return $query->result();
    }
}
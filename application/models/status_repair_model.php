<?php 

class status_repair_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->flex = true;
        $this->table = 'm_status_repair';
        $this->primary = 'statusrepair_id';
    }

    public function relation(){
        return array(
            'status' => array("m_status", "status_id", "status_id"),
            'expired'=> array('m_status_expired','status_id', 'status_id'),
            'type' => array('zn_transaction_type', 'transactiontype_id', 'status_type', 'status'),
        );
    }

    public function options(){
        $data = $this->with('status')->order_by($this->primary,'asc')->get()->result();
        $option = array();
        foreach ($data as $key => $row) {
            $option[$row->statusrepair_id] = $row->status_name;
        }
        return $option;
    }

    public function options_empty(){
        return array_merge(array('' => ''), $this->_options());
    }

}
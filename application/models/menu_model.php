<?php

/**
 * Description of menu_model
 *
 * @author Warman Suganda
 */
class menu_model extends MY_Model {

    public $limit;
    public $offset;
    public $table = 'm_menu';
    public $primary = 'kd_menu';
    public $flex = true;
    public $relation = array(
                'group_menu' => array('m_grup_menu', 'kd_grup_menu', 'kd_grup_menu'),
            );


    public function __construct() {
        parent::__construct();
    }

    public function getDataCustomTable(){
        $this->load->model('menu_group_model');

        $data = $this->menu_group_model
                    ->with('menu')
                    ->order_by('urutan', 'asc')
                    ->order_by('menu_urutan', 'asc')
                    ->get()->result_array();

        
    }


    private function data($condition = array()) {
        $this->db->from($this->table);
        $this->db->where_condition($condition);
        return $this->db;
    }

    public function get_by_id($id) {
        $condition['kd_menu'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function parsing_data() {
        // Filtering
        $condition = array();

        // List Data
        $this->db->order_by('menu_urutan');
        $data = $this->data($condition)->get();

        $rows = array();

        foreach ($data->result() as $value) {
            $parent_id = !empty($value->kd_parent) ? $value->kd_parent : 0;
            $rows[$value->kd_grup_menu][$parent_id][$value->kd_menu] = array(
                'kd_menu' => $value->kd_menu,
                'kd_grup_menu' => $value->kd_grup_menu,
                'nama_menu' => $value->nama_menu,
                'deskripsi' => $value->deskripsi,
                'menu_urutan' => $value->menu_urutan,
                'url' => $value->url,
                'kd_parent' => $value->kd_parent
            );
        }
        return $rows;
    }

    public function get_urutan_menu($condition = array()) {
        $this->db->select('MAX(menu_urutan) AS last_menu_urutan', false);
        $this->db->from($this->table);

        $this->db->where_condition($condition);

        $urutan = $this->db->get();
        if ($urutan->num_rows() > 0) {
            return $urutan->row()->last_menu_urutan;
        } else {
            return 1;
        }
    }

    public function create($data) {

        $urutan = $this->get_urutan_menu(array('kd_grup_menu' => $data['kd_grup_menu'], 'kd_parent' => !empty($data['kd_parent']) ? $data['kd_parent'] : null));
        $data['menu_urutan'] = $urutan+1;

        parent::create($data);

    }

    public function delete($id) {
        if(!$id) $return;
        $this->load->model('roles_model');
        $this->roles_model->delete($id);
        return parent::delete($id);
    }
    
    public function options($default = NULL, $key = '') {
        $data = $this->get();
        $options = array();

        // if (!empty($default))
        //     $options[$key] = $default;
        $options[''] = '';
        foreach ($data->result() as $row) {
            $options[$row->kd_menu] = $row->nama_menu;
        }
        return $options;
    }    
    

    /* Heubeul */

    function get_all_menu() {
        $grup_c = $this->input->post('grup_menu');
        $filter = "";
        if ($grup_c != '') {
            $filter = "where hr_menu.kd_grup_menu='$grup_c'";
        }
        $query = "select hr_menu.kd_menu, hr_menu.url, hr_menu.kd_grup_menu, hr_menu.nama_menu,
        hr_grup_menu.kd_grup_menu, hr_grup_menu.nama_grup_menu
        from hr_menu
        left join hr_grup_menu on hr_menu.kd_grup_menu= hr_grup_menu.kd_grup_menu
        $filter
        ORDER BY hr_menu.kd_grup_menu, hr_menu.menu_urutan";
        $q = $this->db->query($query);
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    function get_detail_menu($noid) {
        $query = "
    select 
    hr_menu.kd_menu, 
    hr_menu.kd_grup_menu, 
    hr_menu.nama_menu, 
    hr_menu.url, 
	hr_menu.menu_urutan, 
	hr_menu.menu_level, 
	hr_grup_menu.kd_grup_menu,
    hr_grup_menu.nama_grup_menu
    from hr_menu
    left join hr_grup_menu
    on 
    hr_menu.kd_grup_menu= hr_grup_menu.kd_grup_menu
    where kd_menu='$noid'
    ";


        $q = $this->db->query($query);
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    function subMenu($noid = '') {
        $idChild = "";
        $query = $this->db->query("
    select 
    hr_menu.kd_parent, hr_menu.kd_menu from hr_menu 
    where hr_menu.kd_menu='$noid'
    ");
        foreach ($query->result() as $row) {
            $idChild = $row->kd_parent;
        }
        if ($idChild == '') {
            $idChild = 0;
        }

        $query2 = ("select 
    hr_menu.kd_menu, 
    hr_menu.kd_grup_menu, 
    hr_menu.nama_menu, 
    hr_menu.url, 
	hr_menu.menu_urutan, 
	hr_menu.menu_level, 
	hr_grup_menu.kd_grup_menu,
    hr_grup_menu.nama_grup_menu
    from hr_menu
    left join hr_grup_menu
    on 
    hr_menu.kd_grup_menu= hr_grup_menu.kd_grup_menu
    where kd_menu='$idChild' ORDER BY  hr_menu.nama_menu ASC ");

        $q = $this->db->query($query2);
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    function data_menu_all() {
        $query = "
    select 
    hr_menu.kd_menu, 
    hr_menu.kd_grup_menu, 
    hr_menu.nama_menu, 
    hr_menu.url, 
	hr_menu.menu_urutan, 
	hr_menu.menu_level, 
	hr_grup_menu.kd_grup_menu,
    hr_grup_menu.nama_grup_menu
    from hr_menu
    left join hr_grup_menu
    on 
    hr_menu.kd_grup_menu= hr_grup_menu.kd_grup_menu ORDER BY hr_menu.nama_menu ASC    ";


        $q = $this->db->query($query);
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    function retrieve_grup_menu() {
        $query = "
    select 
    hr_grup_menu.nama_grup_menu, 
    hr_grup_menu.kd_grup_menu 
    from hr_grup_menu
    ";
        $q = $this->db->query($query);
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    function add_data_menu_mdl() {
        $nama = $this->input->post('txtnamamenu');
        $url = $this->input->post('txturl');
        $grup = $this->input->post('grup_menu');
        $lev = $this->input->post('txtlevel');
        $ur = $this->input->post('txturutan');
        $parent = $this->input->post('select_sub_menu');
        $data_menu = array
            (
            'kd_grup_menu' => $grup,
            'nama_menu' => $nama,
            'url' => $url,
            'kd_grup_menu' => $grup,
            'menu_level' => $grup,
            'menu_urutan' => $ur,
            'kd_parent' => $parent
        );
        $this->db->insert('hr_menu', $data_menu);

        $q0 = $this->db->query("select kd_menu from hr_menu where nama_menu='$nama'");
        foreach ($q0->result_array() as $row) {
            $id_menu = $row['kd_menu'];
        }

        $q1 = $this->db->query("select user_id,grup_id from hr_user ");
        foreach ($q1->result_array() as $row) {
            $user_id = $row['user_id'];
            $roles_id = $row['grup_id'];

            $this->db->query(
                    "insert into hr_user_akses (kd_menu,kd_grup,userid,is_view,is_add,is_edit,is_del,is_import,is_print,is_apr) values ('$id_menu','$roles_id','$user_id','d','d','d','d','d','d','d')");
        }

        $q1 = $this->db->query("select hr_roles.grup_id from hr_roles  group by grup_id");
        foreach ($q1->result_array() as $row) {
            $gr = $row['grup_id'];
            $this->db->query("insert into hr_roles (menu_id,grup_id,is_view,is_add,is_edit,is_delete,is_import,is_print,is_approve) values ('$id_menu','$gr','f','f','f','f','f','f','f')");
        }
    }

    function update_menu_mdl() {

        $nama_menu = $this->input->post('txtnamamenu');
        $url = $this->input->post('txturl');
        $id = $this->input->post('id');
        $menu_urutan = $this->input->post('txturutan');
        $grup_menu = $this->input->post('grup_menu');
        $parent = $this->input->post('select_sub_menu');

        $data_menu =
                array(
                    'nama_menu' => $nama_menu,
                    'url' => $url,
                    'menu_urutan' => $menu_urutan,
                    'kd_grup_menu' => $grup_menu,
                    'kd_parent' => $parent,
        );

        $this->db->where('kd_menu', $id);
        $this->db->update('hr_menu', $data_menu);
    }

    function delete_menu_mdl($id) {
        $this->db->query("delete from hr_roles where menu_id='$id'");
        $this->db->query("delete from hr_menu where kd_menu='$id'");
    }

    function get_url_by_name($name) {
        $this->db->select('*');
        $this->db->where("nama_menu = '$name'");
        $query = $this->db->get('hr_menu');

        return $query->result_array();
    }

    function changeParent($kd_menu, $kd_parent) {

        $kd_parent_old = $this->getById($kd_menu)->row()->kd_parent;

        if(!$kd_parent || $kd_menu==$kd_parent) return;

        $this->load->model('config_model');
        $layout = $this->config_model->get()->row()->menu;
        $layout = json_decode($layout, true);

        $jmlKetemu = 0;
        // cari posisi lama
        foreach ($layout as $key => $value) {
            foreach ((array)$value['child'] as $k => $v) {
                if($v['id']==$kd_menu){
                    $menuNew = $v;
                    unset($layout[$key]['child'][$k]); 
                    $jmlKetemu++;
                } else {
                    foreach ((array) $v['child'] as $i => $j) {
                        if($j['id']==$kd_menu){
                            $menuNew = $j;
                            unset($layout[$key]['child'][$k]['child'][$i]); 
                            $jmlKetemu++;
                        }
                    }
                }
            }
        }

        if($jmlKetemu==1 && ($kd_menu==$kd_parent || $kd_parent_old==$kd_parent)){
            $replace = false;
        } else {
            if (!$menuNew)
            $menuNew = array('id'=>$kd_menu);

            // tempatkan di posisi baru
            foreach ($layout as $key => $value) {
                foreach ((array)$value['child'] as $k => $v) {
                    if($v['id']==$kd_parent){
                        $layout[$key]['child'][$k]['child'][] = $menuNew;
                        $replace = true;
                        break;
                    }
                }
            }
        }

        if($replace){
            //writeDebug(json_encode($layout));
            $this->config_model->set('menu', json_encode($layout))->update();
        }
    }

    function changeGroup($kd_menu, $kd_grup_menu){

        $menu = $this->getById($kd_menu)->row_array();
        
        // if($menu['kd_grup_menu']!=$kd_grup_menu){
        // atur ulang
        $this->load->model('config_model');
        $layout = $this->config_model->get()->row()->menu;
        $layout = json_decode($layout, true);
        $jmlKetemu = 0;

        // hapus dari posisi lama / dari child nya group
        foreach ($layout as $key => $value) {
            foreach ((array)$value['child'] as $k => $v) {
                if ($v['id']==$kd_menu){
                    $menuNew = $v;
                    unset($layout[$key]['child'][$k]);
                    $jmlKetemu++;
                } else {
                    foreach ((array) $v['child'] as $i => $j) {
                        if($j['id']==$kd_menu){
                            $menuNew = $j;
                            unset($layout[$key]['child'][$k]['child'][$i]); 
                            $jmlKetemu++;
                        }
                    }
                }
            }
        }
        
        if($jmlKetemu==1 && $menu['kd_grup_menu']==$kd_grup_menu){
            $replace = false;

        } else {
            if (!$menuNew) $menuNew = array('id'=>$kd_menu);
            // insert ke posisi baru
            foreach ($layout as $key => $value) {
                if($value['id']== $kd_grup_menu){
                    $layout[$key]['child'][] = $menuNew;
                    $replace = true;
                    break;
                }
            }
        }
        
        // save layout baru ke config
        if($replace)
            $this->config_model->set('menu', json_encode($layout))->update();
        
    }

}

/* End of file menu_model.php */
/* Location: ./application/models/menu_model.php */


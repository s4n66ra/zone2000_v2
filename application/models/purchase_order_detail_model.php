<?php
class purchase_order_detail_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->flex = true;
        $this->table    = 'zn_purchase_order_detail';
        $this->primary  = 'podetail_id';
    }

    public function relation(){
        return array(
            'prdetail' => array('zn_purchase_request_detail','prdetail_id', 'prdetail_id'),
            'po' => array('zn_purchase_order', 'po_id', 'po_id'),
            'item' => array('m_item','item_id', 'item_id'),
            'supplier' => array('m_supplier', 'id_supplier', 'supplier_id', 'po'),
            'store' => array('m_cabang', 'id_cabang', 'store_id'),
            'item_supplier' =>array('m_item_supplier','supplier_id','id_supplier and item_supplier.item_id = item.item_id','supplier'),

            'postore' => array('zn_po_store', 'postore_id', 'postore_id'),
            'postore_koli' => array('zn_receiving_postore_koli', 'postore_id', 'postore_id', 'postore'),
            'koli' => array('zn_koli','koli_id','koli_id','postore_koli'),
            'item_stok' => array('m_item_stok','item_id','item_id','item'),
        );


    }

    public function update($data = NULL, $id = NULL) {
        $this->load->model('purchase_request_detail_model');

        $arset = $this->getArSet();
        if($data!=NULL)        
            $arset = array_merge($arset, $data);
        $arwhere = $this->getArWhere();
        parent::update($data, $id);
        if(array_key_exists('detail_status', $arset)){
            // jika update mengandung pr_status maka semua purchase_detail diupdate juga detail_statusnya
            $status = $arset['detail_status'];
            $model = $this->purchase_request_detail_model;

            if($id){                
                $model = $model->where('this.podetail_id', $id);
            }

            if($arwhere){
                $arwhere = str_replace('`this`.`podetail_id`', '`podetail`.`podetail_id`', $arwhere);
                $model = $model->where($arwhere);   
            }

            $model = $model
                ->with(array('podetail','postore','postore_koli','koli'))
                ->set('this.detail_status', $status)
                ->update();
        }
        
    }
}

?>
<?php
class item_in_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->flex = true;
        $this->limitStore = true;
        $this->table    = 'zn_item_in';
        $this->primary  = 'itemin_id';
    }

    public function relation(){
        return array(
            'item' => array('m_item', 'item_id', 'item_id'),
            'postoredetail' => array('zn_po_store_detail', 'postoredetail_id', 'postoredetail_id'),
        );
    }
}

?>
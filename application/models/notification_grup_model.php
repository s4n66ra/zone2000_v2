<?php
class notification_grup_model extends MY_Model {

    public $id;

    public function __construct() {
        parent::__construct();
        $this->flex = true;
        $this->table    = 'zn_notification_grup';
        $this->primary  = 'id';
    }

    public function relation(){
        return array(
            'zn_notification' => array('zn_notification','notif_id', 'notif_id'),
            'm_grup' => array('m_grup','grup_id','grup_id'),            
        );
    }

    
}

?>
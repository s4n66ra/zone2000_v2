<?php
class mesin_tekno_model extends MY_Model {


    public function __construct() {
        parent::__construct();
        $this->table = 'm_mesin_teknogame';
        $this->primary= 'id_mesin_teknogame';
        $this->flex = true;
        $this->limitStore = false;
    }

    public function relation(){
        return array(
            'mesin' => array('m_mesin','id_mesin','id_mesin'),
            'jenis_mesin' => array('m_jenis_mesin', 'id_jenis_mesin', 'id_jenis_mesin'),
            'store' => array('m_cabang', 'id_cabang', 'store_id'),
            'location' => array('m_location_service','location_id','location_id','mesin'),
        );
    }



    public $limit;
    public $offset;
    private $mesin  = 'm_mesin_teknogame';
    private $jenis_mesin = 'm_jenis_mesin';
    private $cabang = 'm_cabang';
    private $coin = 'm_koin';
    private $owner = 'm_owner';
    private $item = "m_item";

    private $po = 'zn_po_mesin';
    private $supplier = 'm_supplier';
    private $kategori_mesin = 'm_kategori_mesin';

    private $table2 = 'supplier';
    private $table3 = 'purchase_order';
    private $table4 = 'bbm_barang';
    private $table5 = 'barang';
    private $table6 = 'satuan';
    private $table7 = 'kategori';


    private function data($condition = array()) {
        $this->db->from($this->mesin);
        //$this->db->join($this->supplier,$this->supplier.'.id_supplier='.$this->mesin.'.id_supplier');        
        $this->db->join($this->jenis_mesin,$this->jenis_mesin.'.id_jenis_mesin='.$this->mesin.'.id_jenis_mesin');
        $this->db->join($this->cabang,$this->cabang.'.id_cabang='.$this->mesin.'.store_id');
        //$this->db->join($this->coin,$this->coin.'.id_koin='.$this->mesin.'.id_koin');
        //$this->db->join($this->owner,$this->owner.'.id_owner='.$this->mesin.'.id_owner');

        $this->db->where_condition($condition);
        return $this->db;
    }

    private function data_detail($condition = array()) {
        // =============Filtering===============
        //$condition = array();
        $kd_bbm= $this->input->post("kd_bbm");
        $id_supplier= $this->input->post("id_supplier");
        $id_purchase_order = $this->input->post('id_purchase_order');

        $id_bbm = $this->input->post('id_bbm');
        if(!empty($kd_bbm)){
            $condition["a.kd_bbm like '%$kd_bbm%'"]=null;
        }
        if(!empty($id_supplier)){
            $condition["a.id_supplier"]=$id_supplier;
        }
        if(!empty($id_purchase_order)){
            $condition["a.id_purchase_order"]=$id_purchase_order;
        }
        if(!empty($id_bbm)){
            $condition["a.id_bbm"]=$id_bbm;
        }

        $tanggal_awal = hgenerator::switch_tanggal($this->input->post('tanggal_awal'));
        $tanggal_akhir = hgenerator::switch_tanggal($this->input->post('tanggal_akhir'));
        //--------end filtering-----------------------------------


        $this->db->select('*,b.id as id_bbm_barang');
        $this->db->from($this->table  . ' a');
        $this->db->join($this->table4 . ' b',' a.id_bbm = b.id_bbm');
        $this->db->join($this->table5 . ' c',' b.id_barang = c.id_barang','left');
        $this->db->join($this->table6 . ' d',' c.id_satuan = d.id_satuan','left');
        $this->db->join($this->table7 . ' e',' e.id_kategori = c.id_kategori','left');
        $this->db->order_by('c.kd_barang ASC');
        $this->db->where_condition($condition);

        return $this->db;
    }

    public function get_by_id($id) {
        $condition['id_mesin_teknogame'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_detail_by_id($id) {
        $condition['b.id'] = $id;
        $this->data_detail($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function data_table() {
        // Total Record
        $total = $this->data()->count_all_results();

        // List Data
        //$this->db->limit($this->limit, $this->offset);
        $data = $this->data($cond)->get()->result();
        $rows = array();

        foreach ($data as $key => $value) {
            $id = $value->id_mesin;
            $id_enc = url_base64_encode($id);            
            $action = '';
            $action .= '';

            if ($this->access_right->otoritas('edit')) {
                $action .= view::button_edit($id_enc, array('full-width'=>1));
                
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= view::button_delete($id_enc, array('full-width'=>1));
                $action .= view::btn_datatable_detail($id, array("caption" => "Liquidate"));
            }

            $status_mesin = "Fine";
            switch ($value->status){
            	case 0 : $status_mesin = "Broken";break;
            	case 1 : $status_mesin = "Fine";break;
            	case 2 : $status_mesin = "Liquidated";break;
            }
            
            $temp = array(
                'primary_key' =>$value->id_mesin,
                'kd_mesin' => $value->kd_mesin,
                'serial_number' => $value->serial_number,
                'harga' => $value->harga,
                'tgl_beli' => $value->tgl_beli,
                'garansi_toko' => $value->garansi_toko,
                'status' => $status_mesin,
                'nama_supplier' => $value->nama_supplier,
                'nama_jenis_mesin' => $value->nama_jenis_mesin,
                'nama_cabang' => $value->nama_cabang,
                'nama_koin' => $value->nama_koin,
                'nama_owner' => $value->nama_owner,
            );

            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $temp['aksi'] = view::render_button_group_raw($action);
            }

            $rows[] = $temp;
        }

        return array('rows' => $rows, 'total' => $total);
    }   


    public function create($data) {
		$this->db->trans_begin();
		
/*		$tableItemData = array(
				"item_key" => $data['serial_number'],
				"item_name" => "Machine",
				"itemtype_id" => itemtype::ITEM_MESIN
		);
		$this->db->insert($this->item, $tableItemData);
		$itemId = $this->db->insert_id();*/
		//endof-insert into table item

		//$data['item_id'] = $itemId;
		$this->db->insert($this->mesin, $data);
		
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return false;
		} else {
			$this->db->trans_commit();
			return true;
		}
    }

    public function update($data, $id) {
        return $this->db->update($this->mesin, $data, array('id_mesin_teknogame' => $id));
    }

    public function create_detail($data) { 
        return $this->db->insert($this->table4, $data);
    }

    public function update_detail($data, $id) {
        return $this->db->update($this->table4, $data, array('id' => $id));
    }

    public function delete($id) {
    	$mesin = $this->get_by_id($id)->row();
    	$item = $this->getItemByIdMesin($mesin->serial_number)->row();
    	
    	if($mesin == NULL && $$item == NULL)
    		echo "null";
    	
    	$this->db->trans_begin();
    	$this->db->delete($this->mesin, array('id_mesin_teknogame' => $id));
    	//$this->db->delete($this->item, array("item_id" => $item->item_id));
    	
    	if ($this->db->trans_status() === FALSE) {
    		$this->db->trans_rollback();
    		//echo "gagal";
    		return false;
    	} else {
    		$this->db->trans_commit();
    		//echo "berhasil";
    		return true;
    	}
    }
    
    private function getItemByIdMesin($id){
    	return $this->db->get_where($this->item, array("item_key" => $id));
    }

    public function delete_detail($id) {
        return $this->db->delete($this->table4, array('id' => $id));
    }
    
    
    public function options($default = '--Pilih mesin--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_mesin] = $row->nama_mesin ;
        }
        return $options;
    }

    public function options_empty($field, $value) {
        $condition = array();
        if(is_array($field)){
            $condition = $field;
        }else{
            $condition[$field] = $value;
        }
        $data = $this->mesin_model->with('jenis_mesin')->db()
                ->where('id_cabang',$this->session->userdata('id_cabang'))
                ->get()->result();
        $options = array();
        $options['']  = '';
        foreach ($data as $row) {
            $options[$row->id_mesin] = $row->kd_mesin.' - '.$row->nama_jenis_mesin ;
        }
        return $options;
    }

    public function options_serial(){
        $data = $this->with('jenis_mesin')->get()->result();
        $options = array();
        $options['']  = '';
        foreach ($data as $row) {
            $options[$row->id_mesin] = $row->serial_number.' - '.$row->nama_jenis_mesin;
        }
        return $options;
    } 

    public function options2() {
        $data = $this
            ->select('id_mesin,serial_number,nama_jenis_mesin,kd_mesin')
            ->with('jenis_mesin')->get()->result();
        
        $options = array();
        foreach ($data as $row) {
            $options[$row->id_mesin] = $row->serial_number.' - '.$row->nama_jenis_mesin ;
        }
        return $options;
    }   

    public function options_vending() {
        $this->db->select('id_mesin,serial_number,nama_jenis_mesin');
        $this->db->from($this->mesin.' m');
        $this->db->join($this->jenis_mesin.' jm','jm.id_jenis_mesin = m.id_jenis_mesin');
        $this->db->join($this->kategori_mesin.' km','km.id_kategori_mesin = jm.id_kategori_mesin');
        $this->db->where('km.st_vending = 1');
        $data = $this->db->get();
        /*
        $data = $this->with('jenis_mesin')->get()->result();
        */
        $options = array();
        foreach ($data ->result() as $row) {
            $options[$row->id_mesin] = $row->serial_number.' - '.$row->nama_jenis_mesin ;
        }
        return $options;
    }   

    public function options_filter(){
        $data = $this->with(array('jenis_mesin'))->get()->result();
        $options = array();
        $options[''] = 'All';
        foreach ($data as $row) {
            //$options[$row->id_mesin] = $row->serial_number." - ".$row->kd_mesin;
            $options[$row->id_mesin] = $row->kd_mesin;
        }
        return $options;
    }

    
}

?>

<?php
class mesin_kerusakan_model extends MY_Model {

    private $kategori_mesin    = 'm_kategori_mesin';
    private $mesin              = 'm_mesin';

    public function __construct() {
        parent::__construct();
        $this->table = 'zn_mesin_kerusakan';
        $this->primary = 'id_mesin_rusak';
    }

    public function relation(){
        return array(
            'mesin' => array('m_mesin','id_mesin','id_mesin'),
            'jenis_mesin' => array('m_jenis_mesin','id_jenis_mesin','id_jenis_mesin','m_mesin'),
            'service' => array('zn_service','id_mesin_rusak','id_mesin_rusak'),
        );
    }
    
    private function base_join(){
        $this->db->from($this->table);
        $this->with($this->relation());
        return $this->db;
    }

    private function data($condition = array()) {
        $this->db->select($this->table.".".$this->primary.", foto, kd_jenis_mesin, nama_jenis_mesin, km.nama_kategori_mesin, jenis_koin, k.nama_kelas, deskripsi");       
        $this->base_join()->where_condition($condition);
        return $this->db;
    }

    

    public function get_by_id($id) {
        $this->base_join()->where($this->table.'.'.$this->primary,$id);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function data_table() {
        
        // List Data
        $temp = $this->getById(1)->row();
        
        $data = $this->with(array('mesin','jenis_mesin'))
                     ->where($this->mesin.'.id_cabang',$this->session->userdata('id_cabang'))
                     ->limit($this->limit, $this->offset)
                     ->get()
                     ->result_array();
        
        $rows = array();
        $total = sizeof($data);
        foreach ($data as $key => $value) {
            $id = $value[$this->primary];
            $id_enc = url_base64_encode($id);            
            $action = '';

            $temp = array();

            $action .= view::button_complete($id_enc, array('full-width'=>1));

            if ($this->access_right->otoritas('edit')) {
                //$action .= view::button_edit($id_enc, array('full-width'=>0));
            }

            if ($this->access_right->otoritas('delete')) {
                //$action .= view::button_delete($id_enc, array('full-width'=>0));
            }

            // mengisi data
            $temp['primary_key']           = $value['id_mesin_rusak'];
            $temp['kd_mesin_rusak']        = $value['kd_mesin_rusak'];
            $temp['kd_mesin']              = $value['kd_mesin'];
            $temp['nama_jenis_mesin']      = $value['nama_jenis_mesin'];
            $temp['problem']               = $value['problem'];
            $temp['status_kerusakan']      = $value['status_kerusakan'];
            $temp['tgl_mulai']             = $value['tgl_mulai'];
            $temp['tgl_target']           = $value['tgl_target'];


            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $temp['aksi'] = view::render_button_group_raw($action);
            }

            $rows[] = $temp;
        }

        return array('rows' => $rows, 'total' => $total);
    }   


    public function create($data) {
        /*
            data = array (
                kd_jenis_mesin, nama_jenis_mesin, deskripsi, etc   
            )
        */
    
        $this->db->trans_start();

        // insert to tabel mesin_kerusakan
        $return = $this->db->insert($this->table, $data);

        // ubah status di tabel mesin jadi 0 : rusak
        $this->load->model('mesin_model');
        $this->mesin_model->update(array('status'=>0),$data['id_mesin']);

        $this->db->trans_complete();
        return $return;
    }

    public function update($data,$id) {
        /*
            data = array (
                kd_jenis_mesin, nama_jenis_mesin, deskripsi, etc   
            )
        */
    
        // edit kerusakan mesin
        $return = $this->db->update($this->table, $data, array($this->primary => $id));

        // edit status mesin di tabel mesin jika ada perubahan status_perbaikan
        if($data['status_kerusakan']){
            $this->load->model('mesin_model');
            $row = $this->getById($id);
            $return = $this->mesin_model->update(
                array('status' => ($data['status_kerusakan']==1 ? 1 : 0)), 
                $row->id_mesin
            );
        }

        return $return;
    }

    public function delete($id) {

        $this->db->trans_start();
       
        // edit setting
        $return = $this->db->delete($this->table, array($this->primary => $id));

        $this->db->trans_complete();
    }
    
    public function options() {
        $data = $this->with(array('mesin','jenis_mesin'))->db()
                    ->get()->result();
        $options = array(''=>'');
        foreach ($data as $row) {
            $options[$row->{$this->primary}] = $row->kd_mesin.' - '.$row->nama_jenis_mesin ;
        }
        return $options;
    }

    public function options_empty() {
        $condition = array();
        
        if(is_array($field)){
            $condition = $field;
        }else{
            $condition[$field] = $value;
        }

        $data = $this->with(array('mesin','jenis_mesin'))->db()
                    ->where('id_cabang',$this->session->userdata('id_cabang'))
                    ->get();

        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->{$this->primary}] = $row->nama_jenis_mesin ;
        }
        return $options;
    }

    
}

?>
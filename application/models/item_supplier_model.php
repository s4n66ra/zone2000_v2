<?php 

class item_supplier_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->flex = true;
        $this->table = 'm_item_supplier';
        $this->primary = 'itemsupplier_id';
    }

    public function relation(){
        return array(
            'supplier' => array('m_supplier','id_supplier','supplier_id'),
        	'item' => array('m_item', 'item_id', 'item_id')
        );
    }

    public function options_empty($item_id = ''){
        if(!$item_id) return array();

        $data = $this->with('supplier')->where('item_id', $item_id)->get()->result();
        $options = array(''=>'');
        foreach ($data as $row) {
            $options[$row->supplier_id] = $row->nama_supplier ." : Rp. ". $row->price ."(Rp ".$row->sup_price_pax." - Rp ".$row->reg_diskon.") = Rp".$row->final_price ;
        }        
        return $options;
    }

    public function options($item_id = NULL){
        if(!$item_id) return array();

        $data = $this->with('supplier')->where('item_id', $item_id)->get()->result();
        $options = array();
        foreach ($data as $row) {
            $options[$row->supplier_id] = $row->nama_supplier ." : Rp. ". $row->price ;
        }        
        return $options;
    }

    /*public function get_by_id($id){

    }*/

}
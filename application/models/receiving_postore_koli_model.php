<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class receiving_postore_koli_model extends MY_Model {

	public $table 	= 'zn_receiving_postore_koli';
    public $primary = 'receivingpostorekoli_id';
    public $flex 	= true;

    public function __construct() {
        parent::__construct();
    }

    public function relation(){
        return array(
            'koli' => array('zn_koli', 'koli_id', 'koli_id'),
            'store' => array('m_cabang', 'id_cabang', 'store_id', 'koli'),
            'supplier' => array('m_supplier', 'id_supplier', 'supplier_id', 'koli'),
            'po_detail' => array('zn_purchase_order_detail','postore_id','postore_id'),
            'item' => array('m_item','item_id','item_id','po_detail'),
        );
    }

}
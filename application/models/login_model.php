<?php
class login_model extends MY_Model {
	public $table = "m_user";
	public $primary = "user_id";
	public $flex = true;
	private $pegawai = "m_pegawai";
	
	function __construct() {
		parent::__construct ();
	}

	public function relation(){
        return array(
            'pegawai' => array('m_pegawai', 'user_id', 'user_id'),
            'supplier' => array('m_supplier','user_id','user_id'),
        );
    }

	function isUsernameExist() {
		$username = $this->input->post ( 'username' );
		if(!$username || $username == '') return false;

		$user = $this->select('user_name')->where('user_name', $username)->get()->row();
		if($user)
			return true;
		else
			return false;

		//$q = $this->db->query ( "select count(m_user.user_name) as jumlah FROM  m_user WHERE user_name='$username'" );
	}

	function cekSupplier($grup_id) {
		$username = $this->input->post ( 'username' );
		if(!$username || $username == '') return false;

		$user = $this->select('user_name')->where(array('user_name'=> $username,'grup_id'=>$grup_id))->get()->row();
		if($user)
			return true;
		else
			return false;

		//$q = $this->db->query ( "select count(m_user.user_name) as jumlah FROM  m_user WHERE user_name='$username'" );
	}

	function isUserExist() {
		$username = $this->input->post ( 'username' );
		$password = md5 ( $this->input->post ( 'password' ) );

		$user = $this
				->with(array('pegawai'))
				->where('user_name', $username)
				->where('user_password', $password)
				->get()->row();
		// $user = $this->findByPk(1)->with('pegawai')->get()->row();
		// vdebug($user);die;
		return $user;

	}

		function getDataSupplier() {
		$username = $this->input->post ( 'username' );
		$password = md5 ( $this->input->post ( 'password' ) );

		$user = $this
				->with(array('supplier'))
				->where('user_name', $username)
				->where('user_password', $password)
				->get()->row();
		// $user = $this->findByPk(1)->with('pegawai')->get()->row();
		// vdebug($user);die;
		return $user;

	}

	function update($data, $id){
		$this->db->update($this->table, $data, array('user_id'=>$id));
	}
}

?>
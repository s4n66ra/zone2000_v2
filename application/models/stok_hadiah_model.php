<?php
class stok_hadiah_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'm_stok_hadiah';
        $this->primary= 'id_stok_hadiah';
    }

    public function relation(){
    	return array(
    		'hadiah' => array('m_hadiah','id_hadiah','id_hadiah'),
    	);
    }

}
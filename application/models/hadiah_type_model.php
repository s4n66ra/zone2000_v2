<?php
class hadiah_type_model extends My_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'm_hadiah_type';
        $this->primary='hadiahtype_id';
        $this->flex = true;
    }

    public function options() {
        $data = $this->get();
        $options = array();
        foreach ($data->result() as $row) {
            $options[$row->hadiahtype_id] = $row->type_name ;
        }        
        return $options;
    }

}
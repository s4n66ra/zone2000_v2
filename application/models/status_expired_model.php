<?php 

class status_expired_model extends MY_Model {

    public $flex = true;
    
    public function __construct() {
        parent::__construct();
        $this->table = 'm_status_expired';
        $this->primary = 'statusexpired_id';
    }

    public function relation(){
        return array(
        	'status' => array("m_status", "status_id", "status_id"),
            'status_pr' => array("m_status_pr", "status_id", "status_id"),
            'status_po' => array("m_status_po", "status_id", "status_id"),
            'status_service' => array('m_status_service', 'status_id', 'status_id'),
            'status_repair' => array('m_status_repair', 'status_id', 'status_id'),
            'itemtype' => array('m_item_type', 'itemtype_id', 'itemtype_id'),
        );
    }

}
<?php
class kota_model extends MY_Model {

    private $kota  = 'm_kota';

    public $relation = array(
            'province' => array('m_provinsi', 'id_provinsi', 'id_provinsi')
        );

    public function __construct() {
        parent::__construct();
        $this->table = 'm_kota';
        $this->provinsi = 'm_provinsi';
        $this->primary = 'id_kota';
    }

    private function data($condition = array()) {        
        $this->db->from($this->table);
        $this->db->join($this->provinsi,$this->provinsi.'.id_provinsi='.$this->kota.'.id_provinsi','left');
        $this->db->where_condition($condition);
        return $this->db;
    }

    public function get_by_id($id) {
        $condition['id_kota'] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function data_table() {
        // Total Record
        $total = $this->data()->count_all_results();

        // List Data
        //$this->db->limit($this->limit, $this->offset);
        //$this->db->limit($this->limit, 0);
        $data = $this->data()->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $id = $value->id_kota;
            $id_enc = url_base64_encode($id);
            $action = '';

            if ($this->access_right->otoritas('edit')) {
                $action .= view::button_edit($id_enc, array('full-width'=>0));
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= view::button_delete($id_enc, array('full-width'=>0));
            }

            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $rows[] = array(
                    'primary_key' =>$value->id_kota,
                    'kd_kota' => $value->kd_kota,
                    'nama_kota' => $value->nama_kota,
                    'nama_provinsi' => $value->nama_provinsi,
                    'aksi' => $action
                );
            }else{
                $rows[] = array(
                    'primary_key' =>$value->id_kota,
                    'kd_kota' => $value->kd_kota,
                    'nama_kota' => $value->nama_kota,
                    'nama_provinsi' => $value->nama_provinsi
                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    }   

    public function data_table_excel() {
        //=============Tahun Aktif====================
        $total = $this->data()->count_all_results();

        // List Data
        //$this->db->limit($this->limit, $this->offset);
        $this->db->limit($this->limit, 0);
        $data = $this->data()->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $id = $value->id_kota;
            $id_enc = url_base64_encode($id);

            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $rows[] = array(
                    'primary_key' =>$value->id_kota,
                    'kd_kota' => $value->kd_kota,
                    'nama_kota' => $value->nama_kota,
                );
            }else{
                $rows[] = array(
                    'primary_key' =>$value->id_kota,
                    'kd_kota' => $value->kd_kota,
                    'nama_kota' => $value->nama_kota
                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    }     

    public function create($data) { 
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->table, $data, array('id_kota' => $id));
    }

    public function delete($id) {
        return $this->db->delete($this->table, array('id_kota' => $id));
    }
    
    public function options($default = '--Pilih kota--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_kota] = $row->nama_kota ;
        }
        return $options;
    }

    public function options_empty() {
        $data = $this->data()->get();
        $options = array();
        $options['']  = '';
        foreach ($data->result() as $row) {
            $options[$row->id_kota] = $row->nama_kota ;
        }
        return $options;
    }
    
}

?>
<?php
class notification_model extends MY_Model {

    public $notif_id;

    public function __construct() {
        parent::__construct();
        $this->flex = true;
        $this->table    = 'zn_notification';
        $this->primary  = 'notif_id';
    }

    public function relation(){
        return array(
            'notif' => array('m_notification','kd_notif', 'kd_notif'),
            'notif_user' => array('zn_notification_user', 'notif_id', 'notif_id'),
            'notif_pr' => array('zn_notification_pr', 'notif_id', 'notif_id'),
            'purchase_request' => array('zn_purchase_request', 'pr_id', 'pr_id', 'notif_pr'),
            'store' => array('m_cabang','id_cabang','store_id','purchase_request'),
        );
    }


    
}

?>
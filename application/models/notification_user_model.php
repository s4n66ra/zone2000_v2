<?php
class notification_user_model extends MY_Model {

    public $id;

    public function __construct() {
        parent::__construct();
        $this->flex = true;
        $this->table    = 'zn_notification_user';
        $this->primary  = 'id';
    }

    public function relation(){
        return array(
            'notification' => array('zn_notification','notif_id', 'notif_id'),
            'user' => array('m_user','user_id','user_id'),
            'pegawai' => array('m_pegawai', 'user_id', 'user_id'),            
        );
    }

    
}

?>
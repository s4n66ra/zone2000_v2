<?php
class negara_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table    = 'm_negara';
        $this->primary  = 'id_negara';
    }

    private function data($condition = array()) {        
        $this->db->from($this->table);
        $this->db->where_condition($condition);
        return $this->db;
    }

    public function get_by_id($id) {
        $condition[$this->primary] = $id;
        $this->data($condition);
        return $this->db->get();
    }

    public function get_data($condition = array()) {
        $this->data($condition);
        return $this->db->get();
    }

    public function data_table() {
        // Total Record
        $total = $this->data()->count_all_results();

        // List Data
        $this->db->limit($this->limit, $this->offset);
        //$this->db->limit($this->limit, 0);
        $data = $this->data()->get();
        $rows = array();

        foreach ($data->result() as $value) {
            $id = $value->id_negara;
            $id_enc = url_base64_encode($id);
            $action = '';

            if ($this->access_right->otoritas('edit')) {
                $action .= view::button_edit($id_enc, array('full-width'=>0));
            }

            if ($this->access_right->otoritas('delete')) {
                $action .= view::button_delete($id_enc, array('full-width'=>0));
            }

            if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
                $rows[] = array(
                    'primary_key' =>$value->id_negara,
                    'nama_negara' => $value->nama_negara,
                    'aksi' => $action
                );
            }else{
                $rows[] = array(
                    'primary_key' =>$value->id_negara,
                    'nama_negara' => $value->nama_negara
                );
            }
        }

        return array('rows' => $rows, 'total' => $total);
    }   

    public function create($data) {	
        return $this->db->insert($this->table, $data);
    }

    public function update($data, $id) {
        return $this->db->update($this->table, $data, array($this->primary => $id));
    }

    public function delete($id) {
        return $this->db->delete($this->table, array($this->primary => $id));
    }
    
    public function options($default = '--Pilih Negara--', $key = '') {
        $data = $this->data()->get();
        $options = array();

        if (!empty($default))
            $options[$key] = $default;

        foreach ($data->result() as $row) {
            $options[$row->id_negara] = $row->nama_negara ;
        }
        return $options;
    }

    public function options_empty() {
        $data = $this->data()->get();
        $options = array();
        $options['']  = '';
        foreach ($data->result() as $row) {
            $options[$row->id_negara] = $row->nama_negara ;
        }
        return $options;
    }
    
}

?>
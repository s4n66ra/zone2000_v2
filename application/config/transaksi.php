<?php

$config['status_pr']	= 	array(
							0 => 'Draft',
							1 => 'Waiting For Review',
							2 => 'Waiting Confirmation',
							3 => 'Revision',
							4 => 'Rejected',
							5 => 'Approved',
							6 => 'Shiping',
							7 => 'Received',
							8 => 'Cancelled',							
						);
$config['status_transaction']	= 	array(
							0 => 'Draft',
							1 => 'Waiting For Review',
							2 => 'Waiting Confirmation',
							3 => 'Revision',
							4 => 'Rejected',
							5 => 'Approved',
							6 => 'Warehouse',
							7 => 'Shiping',
							8 => 'Received',
							9 => 'Cancelled',							
						);

$config['status_bundle'] = array(
							0 => 'Waiting Confirmation',
							1 => 'Approved',
						);

$config['status_po'] = array(
							0 => 'Open',
							1 => 'Waiting For Receive',
							2 => 'DC Transit',
							3 => 'Ship to Store',
							4 => 'Close',
						);

$config['status_kerusakan']	= 
	array(
		0 => 'broken',
		1 => 'fixed',
		2 => 'inspectiion',
		3 => 'waiting for sparepart',
	);

$config['status_confirm'] = 
	array(
		0 => 'Waiting Approval',
		1 => 'Approved',
		2 => 'Rejected',
	);

$config['type_item'] = 
	array(
		'' => 'All',
		//1 => 'Machine',		
		2 => 'Merchandise',
		3 => 'Sparepart',
		4 => 'Ticket',
		5 => 'Coin',
		6 => 'Swipe Card',
		7 => 'Machine',
	);

$config['type_merchandise'] = 
	array(
		1 => 'Electronic',
		2 => 'Non-Electronic',
	);

$config['status_prCreateType']	= 	array(
							0 => '',
							1 => 'Created By HO',						
						);
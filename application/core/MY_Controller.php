<?php

class MY_Controller extends CI_Controller {
	
	public $class_name;
	public $limit = 5;
	public $currentUsername;

    public $button = array();
    protected $formConfig = array('full-width' => 0,'onclick'=>0);
    protected $formConfigFull = array('full-width' => 1,'onclick'=>0);

	public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

        if($this->class_name!='login' && $this->class_name != 'replenish'){
            // Protection
            hprotection::login();
            $this->access_right->check();
            
        }

        // SET DEFAULT BUTTON
        $this->button['add'] = view::button_add($this->formConfig);
        $this->buttonFull['add'] = view::button_add($this->formConfigFull);
        
        //$this->output->enable_profiler(TRUE);
				
        // Global Model
        if(file_exists(APPPATH."models/".$this->class_name."_model.php")){
            $this->load->model(array($this->class_name . '_model'));
        }
        
        $this->load->library('session');
        $this->currentUsername = $this->session->userdata('user_name');


        // Loading Function for Twig
        $this->twig->add_function('base_url');
        $this->twig->add_function('form_dropdown');
        $this->twig->add_function('viewBreadcumb');

        // Loading Global variable for Twig
        $assets_url = base_url().'assets/';

        $this->twig->addGlobal('assets_url',$assets_url);
        $this->twig->addGlobal('base_url',base_url());
        $this->twig->addGlobal('session', $this->session->userdata);
        $this->twig->addGlobal('nama_pegawai',$this->currentUsername);
        $this->twig->addGlobal('breadcumb', viewBreadcumb());
        $this->twig->addGlobal('test', array('oke'=>'doss'));

/*        // HANDLE ERROR
        $handler = PhpConsole\Handler::getInstance();  
        $handler->setErrorsHandlerLevel(E_ALL & ~E_NOTICE);
        $handler->start(); // initialize handlers*/

    }
    

    
    /**
     * this method is used to decide what buttons will be available:
     * 1. Add
     * 2. Print
     * 3. Import
     */
    protected function getAvailableButtons(){
    	$button_group = array();
    	if($this->access_right->otoritas('add')){
    		$button_group[] = $this->button['add'];
    	}
    	return view::render_button_group($button_group);
    }

    protected function getAvailableButtonsFullWidth(){
        $button_group = array();
        if($this->access_right->otoritas('add')){
            $button_group[] = $this->buttonFull['add'];
        }
        return view::render_button_group($button_group);
    }
    
    protected function getTools(){
    	$button_group = array();
    	if($this->access_right->otoritas('print')){
    		$button_group[] = view::button_export();
    	}
    	
    	if(true){//export
    		//$button_group[] = view::button_export();
    	}
    	return view::render_button_group($button_group, array(), true);
    }

    public function loadController($controller = ''){
        require_once(APPPATH.'controllers/'.$controller.'.php'); //include controller
        $this->$controller = new $controller();
        $this->loader = $this->$controller;
    }

    public function unloadController($controller = ''){
        $this->$controller = NULL;
    }

    public function complete($id_enc = ''){
        $id = url_base64_decode($id_enc);
        $this->form('complete',$id);
    }

    public function delete($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('delete', true);
        $this->add('delete',$id);
    }

    public function debugArray($arr){
        foreach ($arr as $key => $value) {
            echo "<b>KEY : $key</b><br>";
            print_r($value);
            echo "<br> ---------------------------------------------- <br>";
        }
    }
}
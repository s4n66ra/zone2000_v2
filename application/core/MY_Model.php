<?php
class MY_Model extends CI_Model {

	public $limit = 0;
    public $offset = 0;
    public $table;
    public $primary_key;
    public $primary;
    
    public $flex = false;

    public $insert_id;

    // untuk membatasi akses data bagi masing2 store
    public $limitStore = false;
    public $limitStoreTable = NULL;
    public $storeId = 'store_id';
    public $cabangId = 'id_cabang';
    public $default_store_id = 'default_store_id';
    public $excCabang = array('m_mesin','m_cabang','m_cabang_setting');
    public $excDefaultStore = array('m_pegawai');

    // dipakai untuk update dengan join . menyimpang string join
    // ex : join table on a=b join table2 on c=d, dst
    public $stringJoin = '';
    public $joinDirection = 'LEFT';

    // untuk menyimpan relation. cara lain dari method relation()
    public $relation;

    // additional for datatable
    public $where = array();

    public function __construct() {
        parent::__construct();
        if (method_exists($this, 'relation'))
            $this->relation = $this->relation();
    }

/*-------------------------------
SELECT
--------------------------------*/

    public function select($select = '*', $escape = NULL)
    {
        $this->db->select($select, $escape);
        return $this;
    }

    public function getById($id){
        return $this->findByPk($id)->db()->get();
    }

    public function getAll(){
    }

/*-------------------------------
CREATE UPDATE DELETE
--------------------------------*/

    public function create($data) {
        $return = $this->db->insert($this->table, $data);
        $this->insert_id = $this->db->insert_id();
        return $return;
    }

    public function update($data = NULL, $id = NULL,$where = NULL) {
        if($where)
            $this->db->where($where);
        if($id) 
            $this->db->where($this->primary, $id);

        if($this->flex)
            $table = $this->table.' this '.$this->stringJoin;
        else
            $table = $this->table.$this->stringJoin;

        if($data){

            $return = $this->db->update($table, $data);
        }
        else{
            
            $return = $this->db->update($table);
        }

        // membersihkan semua atribut
        $this->_clearAttribute();
        $this->db->ar_join = array();
        
        return $return;
    }

    public function delete($id) {
        $return = $this->db->delete($this->table, array($this->primary => $id));
        return $return;
    }

/*-------------------------------
EXPERIMENT
-------------------------------*/
    public function with($rel){
        if($this->flex){
            return $this->with_flex($rel);
        }
    	            	
    	if(is_array($rel)){
            foreach ($rel as $key => $join)
                $this->_with($join);
    	}else{
    		$this->_with($rel);
    	}

        return $this;
    }

    private function _with($join){        
        $relation = $this->relation;

        $join = $relation[$join];
        if($join[3]){
            $tab = $join[3];
        }
        else
        {
            $tab = $this->table;
        }

        if($join)
            $this->db->join($join[0],$join[0].'.'.$join[1].' = '.$tab.'.'.$join[2], $this->joinDirection);
    }

    public function join($a,$b,$c = NULL){
        $this->db->join($a,$b,$c);
    }


    public function with_flex($rel){              
        if(is_array($rel)){
            foreach ($rel as $key => $join){
                $this->_with_flex($join);
            }
        }else{
            $this->_with_flex($rel);
        }
        return $this;
    }

    private function _with_flex($join){
        $relation = $this->relation;
        $relation_name = $join;
        $join = $relation[$join];
        
        $join_table     = $join[0]; // table join
        $join_table_id  = $join[1]; // primary key table join
        $fk = $join[2]; // primary key
        $table = ($join[3] ? $join[3] : 'this');

        if($join){
            $this->db->join($join_table.' '.$relation_name, $relation_name.'.'.$join_table_id.' = '.$table.'.'.$fk, $this->joinDirection);
            $this->stringJoin.= ' join '.$join_table.' '.$relation_name.' on '.$relation_name.'.'.$join_table_id.' = '.$table.'.'.$fk;
            
        }
    }

    public function withAll(){
        $this->with($this->getAllRelationKey());
        return $this;
    }

    public function getAllRelationKey(){
        $rel = $this->relation;
        $temp = array();
        foreach ($rel as $key => $value) {
            $temp[] = $key;
        }
        return $temp;
    }

    public function setJoinDirection($j){
        $this->joinDirection = $j;
        return $this;
    }

    public function getArSet(){
        $ret = array();
        foreach ($this->db->ar_set as $key => $value) {
            $new = str_replace('`', '', $key);
            $ret[$new] = $value;
        }
        return $ret;
    }

    public function getArWhere(){
        $temp = $this->db->ar_where;
        $t = '';
        foreach ($temp as $key => $value) {
            $t.= $value.' ';
        }
        return $t;
    }

    public function _clearAttribute(){
        $this->stringJoin = '';
    }

    public function table(){
        return $this->db->from($this->table);
    }

    public function getDb()
    {
        return $this->db;
    }

    public function findByPk($id){
        $this->db->where(($this->flex ? 'this' : $this->table).'.'.$this->primary, $id);
        return $this;
    }

    public function getLastInserted(){
        if($this->insert_id)
            return $this->getById($this->insert_id);
    }


/*-------------------------------
TURUNAN DARI CI_MODEL
--------------------------------*/

    public function get(){

        return $this->db()->get();
    }

    public function from($table){
        $this->db->from($table);
        return $this;
    }

    public function distinct($column){
        $this->db->distinct($column);
        return $this;
    }

    public function where_in($key = NULL, $values = NULL)
    {
        $this->db->where_in($key, $values);
        return $this;
    }

    public function where_not_in($key = NULL, $values = NULL)
    {
        $this->db->where_not_in($key, $values);
        return $this;
    }

    public function set($key, $value = '', $escape = TRUE)
    {
        $this->db->set($key, $value, $escape);
        return $this;
    }

    public function limit($value, $offset = '')
    {
        $this->db->limit($value, $offset);
        return $this;
    }

    public function order_by($orderby, $direction = ''){
        if(!empty($orderby) && $orderby != '*'){
            $this->db->order_by($orderby, $direction);
            return $this;
        }
    }

    public function group_by($groupkey){
        $this->db->group_by($groupkey);
        return $this;
    }

    public function insert_batch($set = NULL)
    {
        return $this->db->insert_batch($this->table, $set);
    }

    public function count_all_results($table = '')
    {
        return $this->db()->count_all_results();
    }

    public function where($key, $value = NULL, $escape = TRUE)
    {

        $this->db->where($key, $value, 'AND ', $escape);
        return $this;
    }

    public function or_where($key, $value = NULL, $escape = TRUE)
    {
        $this->db->where($key, $value, $escape);
        return $this;
    }

    public function or_where_in($key, $value = array(), $escape = TRUE)
    {
        $this->db->or_where_in($key, $value, $escape);
        return $this;
    }


    public function like($field, $match = '', $side = 'both')
    {
        $this->db->like($field, $match, $side);
        return $this;
    }

    public function or_like($field, $match = '', $side = 'both')
    {
        $this->db->or_like($field, $match, $side);
        return $this;
    }

    public function or_like_in($field, $match = array(), $side = 'both')
    {

        for($i=0; $i<count($match); $i++){
            $this->db->or_like($field, $match[$i], $side);
        }

        return $this;
    }


    public function db(){
        if(empty($this->limitStoreTable)){
            if($this->flex)
                $tab = 'this.';
            else
                $tab = $this->table.'.';
        }else{
            $tab = $this->limitStoreTable.'.';
        }
        
        if($this->limitStore && !hprotection::isHeadOffice()){
            if(in_array($this->table, $this->excCabang)){
                $this->db->where($tab.$this->cabangId, $this->session->userdata('store_id'));
            }elseif(in_array($this->table, $this->excDefaultStore)){
                $this->db->where($tab.$this->default_store_id, $this->session->userdata('store_id'));
            }else{
                $this->db->where($tab.$this->storeId, $this->session->userdata('store_id'));
            }
            
        }
        //echo $this->table.($this->flex ? ' this' : '');
        $this->db->from($this->table.($this->flex ? ' this' : ''));
        // membersihkan semua atribut
        $this->_clearAttribute();
    	return $this->db;	   
    }

/*-------------------------------
TURUNAN DARI DB DRIVER
--------------------------------*/
    public function escape_like_str($str)
    {
        $this->db->escape_like_str($str);
    }


}
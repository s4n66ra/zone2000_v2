<?php
    include APPPATH.'libraries/WebClientPrint.php';
    use Neodynamic\SDK\Web\WebClientPrint;
    use Neodynamic\SDK\Web\Utils;
    use Neodynamic\SDK\Web\DefaultPrinter;
    use Neodynamic\SDK\Web\InstalledPrinter;
    use Neodynamic\SDK\Web\ClientPrintJob;

    function initPrinter(){
	    session_start();
        $ci =& get_instance();


        echo 'angka = '.$ci->input->post('angka');
        echo 'nama = '.$ci->input->post('nama');
        $angka = $ci->input->post('angka');
        $nama = $ci->input->post('nama');
	    // Process request
	    // Generate ClientPrintJob? only if clientPrint param is in the query string
	    $urlParts = parse_url($_SERVER['REQUEST_URI']);
	    
	    if (isset($urlParts['query'])){
	        $rawQuery = $urlParts['query'];
	        if($rawQuery[WebClientPrint::CLIENT_PRINT_JOB]){
	            parse_str($rawQuery, $qs);
                $sid = $qs['sid'];
                if(isset($sid)){
                    // try to get ini file from cache
                    $cacheFileName = (Utils::strEndsWith(WebClientPrint::$wcpCacheFolder, '/')?WebClientPrint::$wcpCacheFolder:WebClientPrint::$wcpCacheFolder.'/').$sid.'.ini';
                    echo $cacheFileName;

                }




	        }
	    }    	
    }

    initPrinter();
?>
<!DOCTYPE html>
<html>
<body>
    <!-- Store User's SessionId -->
    <input type="hidden" id="sid" name="sid" value="<?php echo session_id(); ?>" />
    <input type="hidden" id="barcodevalue" />
    <input type="hidden" id="mername" />

    <label class="checkbox">
        <input type="checkbox" id="useDefaultPrinter" /> <strong>Use default printer</strong> or...
    </label>

    <div id="loadPrinters">

<!--     	<input type="button" id="btnloadprinter" onclick="javascript:jsWebClientPrint.getPrinters();" value="Load Installed Printers" />
 -->                    
    </div>
    <div id="labelloading"><label>printer loading...</label></div>
    <div id="installedPrinters" style="visibility:hidden">
	    <label for="installedPrinterName">Select an installed Printer:</label>
	    <select name="installedPrinterName" id="installedPrinterName"></select>
    </div>
            
    <input type="button" hidden id="print_label" style="font-size:18px" onclick="javascript:jsWebClientPrint.print('useDefaultPrinter=' + $('#useDefaultPrinter').attr('checked') + '&printerName=' + $('#installedPrinterName').val());" value="Print Label..." />
        
    <script type="text/javascript">

        var wcppGetPrintersDelay_ms = 5000; //5 sec

        function wcpGetPrintersOnSuccess(){
            // Display client installed printers
            if(arguments[0].length > 0){
                var p=arguments[0].split("|");
                var options = '';
                for (var i = 0; i < p.length; i++) {
                    options += '<option>' + p[i] + '</option>';
                }
                $('#installedPrinters').css('visibility','visible');
                $('#installedPrinterName').html(options);
                $('#installedPrinterName').focus();
                $('#loadPrinters').hide();
                $('#labelloading').hide();
                //$("#print_label").removeAttr("hidden");
                $('#load-print').html("<i class='fa fa-print'></i>Print");
                $('#load-print').attr("onclick","javascript:jsWebClientPrint.print('useDefaultPrinter=' + $('#useDefaultPrinter').attr('checked') + '&printerName=' + $('#installedPrinterName').val());");
            }else{
                alert("No printers are installed in your system.");
            }
        }


        function wcpGetPrintersOnFailure() {
            // Do something if printers cannot be got from the client
            alert("No printers are installed in your system.");
        }
    </script>
    
    <!-- Add Reference to jQuery at Google CDN -->

    <?php
    //Specify the ABSOLUTE URL to the php file that will create the ClientPrintJob object
    //In this case, this same page
    echo WebClientPrint::createScript(Utils::getRoot().'/PrintZPLSample/PrintLabel.php')
    ?>
       

</body>
</html>

<?php
class prstatus {
	const DRAFT = 0;
	const WAITING_REVIEW = 1;
	const WAITING_CONFIRM = 2;
	const REVISI = 3;
	const REJECT = 4;
	const APPROVE = 5;
	const WAREHOUSE = 6;
	const SHIP = 7;
	const RECEIVE = 8;
	const CANCEL = 9;
	const SERVICE_CENTER = 10;

	
	public static function getStatus($id){
		switch ($id) {
			case 0:
				$stat = 'DRAFT';
				break;
			case 1:
				$stat = 'WAITING REVIEW';
				break;
			case 9:
				$stat = 'CANCEL';
				break;
			case 2:
				$stat = 'WAITING CONFIRM';
				break;
			case 3:
				$stat = 'REVISI';
				break;
			case 4:
				$stat = 'REJECT';
				break;
			case 5:
				$stat = 'APPROVE';
				break;
			case 6:
				$stat = 'WAREHOUSE';
				break;
			case 7:
				$stat = 'SHIP';
				break;
			case 8:
				$stat = 'RECEIVE';
				break;
			case 10:
				$stat = 'SERVICE CENTER';
				break;
			default:
				# code...
				break;
		}
		return $stat;

	}
}


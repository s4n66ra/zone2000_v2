<?php
use Carbon\Carbon;

class notification{

	public static function viewNotification($username='', $kd_roles=''){
		$ci =& get_instance();
		$ci->load->model(array('notification_model', 'notification_user_model', 'notification_pr_model', 'notification_store_model'));
		$temp_notif = '';
		if($kd_roles==15){
			$content = '';
			$arr_data = $ci->notification_model
					->select('*')
					->with(array('notif', 'notif_user', 'notif_pr', 'purchase_request'))
					->where(array('this.status'=>0, 'notif_user.user_id'=>$ci->session->userdata('user_id')))
					->get()
					->result_array();
			$jml_notif = count($arr_data);
			if($jml_notif>0){
				foreach ($arr_data as $key => $value) {
					foreach ($arr_data[$key] as $k => $v) {
						switch ($k) {
							case 'kd_notif':
								$kd_notif = $v;
								switch ($kd_notif) {
									case '001':
										$content .= '<li>
														<ul class="dropdown-menu-list scroller" style="height: 100%;" data-handle-color="#637283">
															<li>
																<a href="'.base_url().'purchase_order">

																<div class="row" class="col-md-12"><label></label>
																	<div class="form-group">
																		<div class="col-md-8">
																			<span class="label label-sm label-icon label-warning">
																				<i class="fa fa-bell-o"></i>
																			</span>'.'PR - '.$arr_data[$key]['pr_code'].'
																		</div>
																		<div class="col-md-4">
																			<span class="time">'.$arr_data[$key]["rec_created"].'</span>
																			<span class="details">
																		</div>

																	</div>
																</div>
																</span>
																</a>
															</li>
														</ul>
													</li>';
										break;
									case '002':
										$content .= '<li>
														<ul class="dropdown-menu-list scroller" style="height: 40px;" data-handle-color="#637283">
															<li>
																<a href="">
																<span class="time">'.$arr_data[$key]["rec_created"].'</span>
																<span class="details">
																<span class="label label-sm label-icon label-warning">
																<i class="fa fa-bell-o"></i>
																</span>
																'.$arr_data[$key]['message'].'
																</span>
																</a>
															</li>
														</ul>
													</li>';
										break;
									default:
										# code...
										break;
								}

								break;
							
							default:
								# code...
								break;
						}
					}
				}

				$temp_notif = '<li class="dropdown dropdown-extended dropdown-dark dropdown-notification" id="header_notification_bar">
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<i class="icon-bell"></i>
					<span class="badge badge-default">'.$jml_notif.'</span>
					</a>
					<ul class="dropdown-menu" style="height:300px; width:500px; overflow:auto;">
						<li class="external">
							<h3>'.$username.' have <strong>'.$jml_notif.'</strong> notifications</h3>
							<a href="'.base_url().'purchase_order'.'">view all</a>
						</li>
						'.$content.'
						
					</ul>
				</li>';

			}else{
				$temp_notif = '<li class="dropdown dropdown-extended dropdown-dark dropdown-notification" id="header_notification_bar">
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<i class="icon-bell"></i>
					<span class="badge badge-default">'.$jml_notif.'</span>
					</a>
					<ul class="dropdown-menu">
						<li class="external">
							<h3>'.$username.'`s notifications</h3>
							<a href="">view all</a>
						</li>
						'.$content.'
						
					</ul>
				</li>';

			}

		}else{
			//NULL dipake karena di database defaultnya NULL jadi status pake NULL
			$content = '';
			$arr_data = $ci->notification_store_model
					->select('*')
					->with(array('notification', 'store', 'pegawai'))
					->where(array('this.status'=>0, 'this.store_id'=>$ci->session->userdata('store_id')))
					->group_by('this.id')
					->get()
					->result_array();
			$jml_notif = count($arr_data);

			if($jml_notif>0){
				foreach ($arr_data as $key => $value) {
					foreach ($arr_data[$key] as $k => $v) {
						switch ($k) {
							case 'kd_notif':
								$kd_notif = $v;
								switch ($kd_notif) {
									case '001':
										$content .= '<li>
														<ul class="dropdown-menu-list scroller" style="height: 40px;" data-handle-color="#637283">
															<li>
																<a href="javascript:;">
																<span class="time">'.$arr_data[$key]["rec_created"].'</span>
																<span class="details">
																<span class="label label-sm label-icon label-warning">
																<i class="fa fa-bell-o"></i>
																</span>
																'.substr($arr_data[$key]['message'], 0, 35).'
																</span>
																</a>
															</li>
														</ul>
													</li>';
										break;
									case '002':
										$content .= '<li>
														<ul class="dropdown-menu-list scroller" style="height: 40px;" data-handle-color="#637283">
															<li>
																<a href="javascript:;">
																<span class="time">'.$arr_data[$key]["rec_created"].'</span>
																<span class="details">
																<span class="label label-sm label-icon label-warning">
																<i class="fa fa-bell-o"></i>
																</span>
																'.$arr_data[$key]['message'].'
																</span>
																</a>
															</li>
														</ul>
													</li>';
										break;
									case '003' :
										$content .= '<li>
														<ul class="dropdown-menu-list scroller" style="height: 40px;" data-handle-color="#637283">
															<li>
																<a href="'.base_url().'notification">
																<span class="time">'.$arr_data[$key]["rec_created"].'</span>
																<span class="details">
																<span class="label label-sm label-icon label-warning">
																<i class="fa fa-bell-o"></i>
																</span>
																'.substr($arr_data[$key]['custom_message'], 0, 35).'
																</span>
																</a>
															</li>
														</ul>
													</li>';
										break;

									default:
										# code...
										break;
								}

								break;
							
							default:
								# code...
								break;
						}
					}
				}

				$temp_notif = '<li class="dropdown dropdown-extended dropdown-dark dropdown-notification" id="header_notification_bar">
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<i class="icon-bell"></i>
					<span class="badge badge-default">'.$jml_notif.'</span>
					</a>
					<ul class="dropdown-menu" style="height:300px; width:500px; overflow:auto;">
						<li class="external">
							<h3>'.$username.' have <strong>'.$jml_notif.'</strong> notifications</h3>
							<a href="'.base_url().'notification">view all</a>
						</li>
						'.$content.'
						
					</ul>
				</li>';

			}else{
				$temp_notif = '<li class="dropdown dropdown-extended dropdown-dark dropdown-notification" id="header_notification_bar">
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<i class="icon-bell"></i>
					<span class="badge badge-default">'.$jml_notif.'</span>
					</a>
					<ul class="dropdown-menu">
						<li class="external">
							<h3>'.$username.'`s notifications</h3>
							<a href="'.base_url().'notification">view all</a>
						</li>
						'.$content.'
						
					</ul>
				</li>';

			}

		}

		return $temp_notif;


	}

	public static function createNotification($user_id = NULL, $notif_id=NULL){
		$ci =& get_instance();
		$ci->load->model(array('notification_model'));			
	}
	
}
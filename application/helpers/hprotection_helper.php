<?php

/**
 * Description of protection_helper
 *
 * @author Warman Suganda
 */
class hprotection {

    public static function login($status = true) {		
        if ($status) {
            if (!self::status_login()) {
                redirect('login');
                exit;
            }
        } else {
            if (self::status_login()) {
                redirect('dashboard');
                exit;
            }
        }	 
     }
	public static function status_login() {
        $ci = &get_instance();
        return $ci->session->userdata('status_login');
    }

    public static function isHeadOffice(){
        $ci = &get_instance();
        if($ci->session->userdata('is_head_office'))
            return true;
        else
            return false;
    }

    public static function getHO(){
        return 77;
    }

    public static function getSC(){
        return 78;
    }

    public static function getWH(){
        return 79;
    }

    public static function getSupllierGroupId(){
        return 46;
    }

    public static function getScItem(){
        $item_type = array(3);
        return $item_type;
    }

    public static function getDcItem(){
        $item_type = array(2);
        return $item_type;
    }


}

/* End of file hprotection_helper.php */
/* Location: ./application/helpers/hprotection_helper.php */

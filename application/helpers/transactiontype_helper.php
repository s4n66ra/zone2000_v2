<?php

class transactiontype {
	const COIN = 1;//----------- coin in -------------
	const COUT = 2;//----------- coin out -------------
	const TOUT = 3;//----------- Ticket Out -------------
	const MOUT = 4;//----------- Merchandise Out -------------
	const TFIL = 5;//----------- ticket reFill -------------
	const MFIL = 6;//----------- merchandise reFill Vending -------------
	const TCUT = 7;//----------- ticket cut -------------
	const TRDM = 8;//----------- ticket redeem -------------
	const MRDM = 9;//----------- merchandise redeem (tukar merchandise dengan tiket) -------------
	const SREQ = 10;
	const SREP = 11;
	const PR = 15;
	const BUNDLE = 16;
	const PO = 17;
	const MRPL = 20;//---------- merchandise Replenish (pindah dari gudang belakang ke depan)
	const MRDM_REG = 29;//----------- merchandise redeem REGULAR (NON MEMBER) (tukar merchandise dengan tiket) -------------
	const RECEIVING = 18;
	const SHIPPING = 19;
	const MFILO = 21;//----------- merchandise Vending MD Out (Kuras) -------------
	const BYS = 22;//----------- Buying Cash (stok MD) -------------
	const SELL = 23;//----------- Penjualan Langsung -------------
	const PRMO = 24;//----------- Penjualan Promosi -------------
	const CADD = 25;//----------- Penjualan Promosi -------------
	const TPAYO = 26;//----------- Ticket Payout -------------
	const BYSC = 27;//----------- Buying Cash (Sparepart) -------------
	const MTSI = 28;//----------- Mutasi SC IN (Sparepart) -------------

	private $value = COIN;
	
	function __construct($type){
		$this->value = $type;
	}
	
	function getType(){
		return $this->value;
	}
}
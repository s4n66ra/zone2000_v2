<?php

/**
 * Description of generator_helper
 *
 * @author Sanggra Handi A
 */
class hgenerator {

    private static $npp_sparator = '-';
    private static $nama_hari = array('Mon' => 'Senin', 'Tue' => 'Selasa', 'Wed' => 'Rabu', 'Thu' => 'Kamis', 'Fri' => 'Jumat', 'Sat' => 'Sabtu', 'Sun' => 'Minggu');
    private static $jenis_sakit = array('ringan' => 'Ringan', 'sedang' => 'Sedang', 'berat' => 'Berat');
    private static $rawat_inap = array('T' => 'Ya', 'F' => 'Tidak');
    private static $status_approve = array('T' => 'Sudah Disetujui', 'F' => 'Sedang Diproses', 'R' => 'Ditolak', 'B' => 'Dikembalikan', 'C' => 'Dibatalkan');
    public static $color_approve = array('T' => 'green', 'F' => 'grey', 'R' => 'red', 'B' => 'yellow', 'C' => '');
    private static $keterangan_shift_detail = array('M' => 'Masuk', 'P' => 'Pulang', 'BO' => 'Break Out', 'BI' => 'Break In', 'WKM' => 'Waktu Khusus Mulai', 'WKS' => 'Waktu Khusu Selesai');
    public static $keterangan_shift_detail_lainnya = array('WKM', 'WKS');
    public static $kondisi_jam_absen = array('M' => 'min', 'BO' => 'min', 'BI' => 'min', 'P' => 'max');
    public static $font_awesome = array('icon-gift', 'icon-glass', 'icon-globe', 'icon-group', 'icon-hdd', 'icon-headphones', 'icon-print', 'icon-folder-open', 'icon-user');
    public static $st_active = array('t' => 'Aktif', 'f' => 'Tidak Aktif');
    public static $kelompok_jabatan = array('S' => 'Struktural', 'F' => 'Fungsional');
    public static $otoritas_data = array('D' => 'Diri Sendiri', 'K' => 'Koordinator', 'S' => 'Semua');
    public static $nama_agama = array('001' => 'Islam', '002' => 'Kristen Katolik', '003' => 'Kristen Protestan', '004' => 'Hindu', '005' => 'Budha', '006' => 'Konghuchu');
    public static $nama_bulan = array('Jan' => 'Januari', 'Feb' => 'Februari', 'Mar' => 'Maret', 'Apr' => 'April', 'May' => 'Mei', 'Jun' => 'Juni', 'Jul' => 'Juli', 'Aug' => 'Agustus', 'Sep' => 'September', 'Oct' => 'Oktober', 'Nov' => 'November', 'Dec' => 'Desember');
    public static $nama_bangsa = array('WNI' => 'WNI', 'WNA' => 'WNA');
    public static $nama_gender = array('2' => 'Laki-laki', '1' => 'Perempuan');
    public static $nama_gender2 = array('L' => 'Laki-laki', 'P' => 'Perempuan');
    public static $nama_cuti = array('besar' => 'Cuti Besar', 'tahun' => 'Cuti Tahunan');
    public static $nama_kawin = array('BK' => 'Belum Kawin', 'K' => 'Kawin', 'J' => 'Janda', 'D' => 'Duda');
    public static $kategori_lembur = array('HK' => 'Hari Kerja', 'HL' => 'Hari Libur/Hari Istirahat Mingguan');
    public static $cuti_default = array(4, 5, 6, 7, 8);
    public static $lulus = array('T' => 'Lulus', 'F' => 'Tidak Lulus');
    public static $status_keluarga = array('Kandung' => 'Kandung', 'Angkat' => 'Angkat');
    public static $kelompok_penghargaan = array('1' => 'Periodik ', '2' => 'Khusus');
    public static $jenis_dokumen = array('KK' => 'Kartu Keluarga', 'AL' => 'Akta Kelahiran', 'BK' => 'Buku Nikah');
    public static $jenis_dokumen_jabatan = array('NP' => 'Nota Dinas', 'SK' => 'SK', 'NPPGP' => 'NPPGP', 'SPK' => 'SPK');
    public static $satuan_waktu = array('hari' => 'Hari', 'bulan' => 'Bulan', 'tahun' => 'Tahun');
    public static $satuan_nominal = array('rupiah' => 'Rupiah', 'persen' => 'Persen');
    public static $kelas_plafond = array("Kelas I" => "Kelas I", "Kelas II" => "Kelas II", "Kelas III" => "Kelas III", "VIP" => "VIP", "VVIP" => "VVIP");
    public static $kategori_perawatan = array('inap' => 'Rawat Inap', 'jalan' => 'Rawat Jalan', 'kacamata' => 'Kacamata', 'checkup' => 'General Checkup');
    public static $kategori_penyakit = array('ringan' => 'Ringan', 'berat' => 'Berat');
    public static $publish = array('P' => 'Published', 'U' => 'Unpublished');
    public static $status_pengajuan = array("k" => "Keluarga", "p" => "Pegawai");
    public static $status_apr = array("T" => "Disetujui", "F" => "Ditolak", "O" => "Diproses");
    public static $status_apr_conf = array("T" => "Setujui", "F" => "Tolak");
    public static $filter_options = array(
        'box-tanggal' => 'Tanggal',
        'box-bulan' => 'Bulan',
        'box-tahun' => 'Tahun'
    );
        public static $filter_options_new = array(
        'box-bulan' => 'Bulan',
        'box-tahun' => 'Tahun'
    );
    public static $bulan_options = array(
        '01' => 'Januari',
        '02' => 'Febuari',
        '03' => 'Maret',
        '04' => 'April',
        '05' => 'Mei',
        '06' => 'Juni',
        '07' => 'Juli',
        '08' => 'Agustus',
        '09' => 'September',
        '10' => 'Oktober',
        '11' => 'November',
        '12' => 'Desember'
    );

    public static function status_persekot($st) {
        return $st == 'p' ? 'Ya' : 'Tidak';
    }

    public static function render_button_group($list = array()) {
        $bg = '';
        foreach ($list as $value) {
            $bg .= '<div class="btn-group">';
            $bg .= $value;
            $bg .= '</div>';
        }
        return $bg;
    }

    public static function switch_tanggal($tanggal, $format = '') {
        if (!empty($tanggal)) {
            switch ($format) {
                case 1:
                    list($a, $b, $c) = explode('-', date('d-M-Y', strtotime($tanggal)));
                    $b = self::$nama_bulan[$b];
                    $date = $a . ' ' . $b . ' ' . $c;
                    break;

                case 2:
                    list($a, $b, $c) = explode('-', $tanggal);
                    $date = $c . '/' . $b . '/' . $a;
                    break;

                default:
                    list($a, $b, $c) = explode('-', $tanggal);
                    $date = $c . '-' . $b . '-' . $a;
                    break;
            }

            return $date;
        } else {
            return '';
        }
    }

    public static function array_to_option($array = array(), $selected = '') {
        $option = '';
        foreach ($array as $key => $value) {
            $sel = '';
            if ($key == $selected)
                $sel = 'selected="selected"';

            $option .= '<option value="' . $key . '" ' . $sel . '>' . $value . '</option>';
        }
        return $option;
    }

    public static function nama_hari($kode = '', $default = '--Pilih Hari--', $key = '') {
        if (empty($kode)) {

            if (!empty($default)) {
                return self::merge_array(array($key => $default), self::$nama_hari);
            } else {
                return self::$nama_hari;
            }
        } else {
            return isset(self::$nama_hari[$kode]) ? self::$nama_hari[$kode] : '-';
        }
    }

    public static function cuti_default() {
        return self::$cuti_default;
    }

    public static function kategori_lembur($kode = '', $default = '--Pilih Kategori Lembur--', $key = '') {
        if (empty($kode)) {
            if (!empty($default)) {
                return self::merge_array(array($key => $default), self::$kategori_lembur);
            } else {
                return self::$kategori_lembur;
            }
        } else {
            return isset(self::$kategori_lembur[$kode]) ? self::$kategori_lembur[$kode] : '-';
        }
    }

    public static function jenis_sakit($kode = '', $default = '--Pilih Jenis Sakit--', $key = '') {
        if (empty($kode)) {
            if (!empty($default)) {
                return self::merge_array(array($key => $default), self::$jenis_sakit);
            } else {
                return self::$jenis_sakit;
            }
        } else {
            return isset(self::$jenis_sakit[$kode]) ? self::$jenis_sakit[$kode] : '-';
        }
    }

    public static function status_apr_conf($kode = '', $default = '--Pilih Jenis Sakit--', $key = '') {
        if (empty($kode)) {
            if (!empty($default)) {
                return self::merge_array(array($key => $default), self::$status_apr_conf);
            } else {
                return self::$status_apr_conf;
            }
        } else {
            return isset(self::$status_apr_conf[$kode]) ? self::$status_apr_conf[$kode] : '-';
        }
    }

    public static function st_active($kode = '', $default = '--Pilih Status--', $key = '') {
        if (empty($kode)) {
            if (!empty($default)) {
                return self::merge_array(array($key => $default), self::$st_active);
            } else {
                return self::$st_active;
            }
        } else {
            return isset(self::$st_active[$kode]) ? self::$st_active[$kode] : '-';
        }
    }

    public static function otoritas_data($kode = '', $default = '--Pilih Otoritas Data--', $key = '') {
        if (empty($kode)) {
            if (!empty($default)) {
                return self::merge_array(array($key => $default), self::$otoritas_data);
            } else {
                return self::$otoritas_data;
            }
        } else {
            return isset(self::$otoritas_data[$kode]) ? self::$otoritas_data[$kode] : '-';
        }
    }

    public static function kelompok_jabatan($kode = '', $default = '--Pilih Kelompok--', $key = '') {
        if (empty($kode)) {
            if (!empty($default)) {
                return self::merge_array(array($key => $default), self::$kelompok_jabatan);
            } else {
                return self::$kelompok_jabatan;
            }
        } else {
            return isset(self::$kelompok_jabatan[$kode]) ? self::$kelompok_jabatan[$kode] : '-';
        }
    }

    public static function rawat_inap($kode = '', $default = '--Pilih Status--', $key = '') {
        if (empty($kode)) {
            if (!empty($default)) {
                return self::merge_array(array($key => $default), self::$rawat_inap);
            } else {
                return self::$rawat_inap;
            }
        } else {
            return isset(self::$rawat_inap[$kode]) ? self::$rawat_inap[$kode] : '-';
        }
    }

    public static function keterangan_shift_detail($kode = '', $default = '--Pilih Status--', $key = '') {
        if (empty($kode)) {
            if (!empty($default)) {
                return self::merge_array(array($key => $default), self::$keterangan_shift_detail);
            } else {
                return self::$keterangan_shift_detail;
            }
        } else {
            return isset(self::$keterangan_shift_detail[$kode]) ? self::$keterangan_shift_detail[$kode] : '-';
        }
    }

    public static function statu_approve($kode = '', $default = '--Pilih Status--', $key = '') {
        if (empty($kode)) {
            if (!empty($default)) {
                return self::merge_array(array($key => $default), self::$status_approve);
            } else {
                return self::$status_approve;
            }
        } else {
            return isset(self::$status_approve[$kode]) ? self::$status_approve[$kode] : '-';
        }
    }

    public static function opsi_agama($kode = '', $default = '--Pilih Agama--', $key = '') {
        if (empty($kode)) {
            if (!empty($default)) {
                return self::merge_array(array($key => $default), self::$nama_agama);
            } else {
                return self::$nama_agama;
            }
        } else {
            return isset(self::$nama_agama[$kode]) ? self::$nama_agama[$kode] : '-';
        }
    }

    public static function opsi_status_keluarga($kode = '', $default = '--Pilih Jenis Status--', $key = '') {
        if (empty($kode)) {
            if (!empty($default)) {
                return self::merge_array(array($key => $default), self::$status_keluarga);
            } else {
                return self::$status_keluarga;
            }
        } else {
            return isset(self::$status_keluarga[$kode]) ? self::$status_keluarga[$kode] : '-';
        }
    }

    public static function opsi_kebangsaaan($kode = '', $default = '--Pilih Kebangsaan--', $key = '') {
        if (empty($kode)) {
            if (!empty($default)) {
                return self::merge_array(array($key => $default), self::$nama_bangsa);
            } else {
                return self::$nama_bangsa;
            }
        } else {
            return isset(self::$nama_bangsa[$kode]) ? self::$nama_bangsa[$kode] : '-';
        }
    }

    public static function opsi_kawin($kode = '', $default = '--Pilih Status Pernikahan--', $key = '') {
        if (empty($kode)) {
            if (!empty($default)) {
                return self::merge_array(array($key => $default), self::$nama_kawin);
            } else {
                return self::$nama_kawin;
            }
        } else {
            return isset(self::$nama_kawin[$kode]) ? self::$nama_kawin[$kode] : '-';
        }
    }

    public static function opsi_status_pengajuan($kode = '', $default = '--Pilih--', $key = '') {
        if (empty($kode)) {
            if (!empty($default)) {
                return self::merge_array(array($key => $default), self::$status_apr);
            } else {
                return self::$status_apr;
            }
        } else {
            return isset(self::$status_apr[$kode]) ? self::$status_apr[$kode] : '-';
        }
    }

    public static function opsi_jenis_pengajuan($kode = '', $default = '--Pilih--', $key = '') {
        if (empty($kode)) {
            if (!empty($default)) {
                return self::merge_array(array($key => $default), self::$status_pengajuan);
            } else {
                return self::$status_pengajuan;
            }
        } else {
            return isset(self::$status_pengajuan[$kode]) ? self::$status_pengajuan[$kode] : '-';
        }
    }

    public static function opsi_gender($kode = '', $default = '--Pilih Jenis Kelamin--', $key = '') {
        if (empty($kode)) {
            if (!empty($default)) {
                return self::merge_array(array($key => $default), self::$nama_gender);
            } else {
                return self::$nama_gender;
            }
        } else {
            return isset(self::$nama_gender[$kode]) ? self::$nama_gender[$kode] : '-';
        }
    }

    public static function opsi_gender2($kode = '', $default = '--Pilih Jenis Kelamin--', $key = '') {
        if (empty($kode)) {
            if (!empty($default)) {
                return self::merge_array(array($key => $default), self::$nama_gender2);
            } else {
                return self::$nama_gender2;
            }
        } else {
            return isset(self::$nama_gender2[$kode]) ? self::$nama_gender2[$kode] : '-';
        }
    }

    public static function opsi_cuti($kode = '', $default = '--Pilih Jenis Cuti--', $key = '') {
        if (empty($kode)) {
            if (!empty($default)) {
                return self::merge_array(array($key => $default), self::$nama_cuti);
            } else {
                return self::$nama_cuti;
            }
        } else {
            return isset(self::$nama_cuti[$kode]) ? self::$nama_cuti[$kode] : '-';
        }
    }

    public static function opsi_kategori_penyakit($kode = '', $default = '--Pilih Jenis Kategori--', $key = '') {
        if (empty($kode)) {
            if (!empty($default)) {
                return self::merge_array(array($key => $default), self::$kategori_penyakit);
            } else {
                return self::$kategori_penyakit;
            }
        } else {
            return isset(self::$kategori_penyakit[$kode]) ? self::$kategori_penyakit[$kode] : '-';
        }
    }

    public static function opsi_publish($kode = '', $default = '--Pilih--', $key = '') {
        if (empty($kode)) {
            if (!empty($default)) {
                return self::merge_array(array($key => $default), self::$publish);
            } else {
                return self::$publish;
            }
        } else {
            return isset(self::$publish[$kode]) ? self::$publish[$kode] : '-';
        }
    }

    public function merge_array($array1 = array(), $array2 = array()) {
        $array = array();
        foreach ($array1 as $key => $value) {
            $array[$key] = $value;
        }
        foreach ($array2 as $key => $value) {
            $array[$key] = $value;
        }
        return $array;
    }

    public function merge_array_raw($array1 = array(), $array2 = array()) {
        $array = $array1;
        foreach ($array2 as $value) {
            $array[] = $value;
        }
        return $array;
    }

    public static function rupiah($number) {
        return number_format($number, 2, '.', ',');
    }

    public static function diskon($number) {
        return number_format($number, 2, '.', ',').' %';
    }

    public static function persen($number) {
        return number_format($number, 2, '.', ',').' %';
    }

    public static function number($number) {
        return number_format($number, 2, '.', ',');
    }

    public static function number_plaint($number) {
        return number_format($number, 0, '.', ',');
    }


    public static function numberexcel($number) {
        $angka = $number;
        if(floor($number)!=$number) {
            return number_format($angka, 4, '.', ',');
        }else{
            return number_format($angka, 0, '.', ',');
        }
    }

    public static function encode_npp($npp) {
        return str_replace(' ', self::$npp_sparator, $npp);
    }

    public static function decode_npp($npp) {
        return str_replace(self::$npp_sparator, ' ', $npp);
    }

    public static function datetime_diff($str_interval = '', $start = '', $end = '') {

        $start = date('Y-m-d H:i:s', strtotime($start));
        $end = date('Y-m-d H:i:s', strtotime($end));
        $d_start = new DateTime($start);
        $d_end = new DateTime($end);
        $diff = $d_start->diff($d_end);

        $year = $diff->format('%y');
        $month = $diff->format('%m');
        $day = $diff->format('%d');
        $hour = $diff->format('%h');
        $min = $diff->format('%i');
        $sec = $diff->format('%s');

        switch ($str_interval) {
            case "y":
                $month = $month > 0 ? round($month / 12, 0) : 0;
                $day = $day > 0 ? round($day / 365, 0) : 0;
                $total = $year + $month / 12 + $day / 365;
                break;
            case "m":
                $day = $day > 0 ? round($day / 30, 0) : 0;
                $hour = $hour > 0 ? round($hour / 24, 0) : 0;
                $total = $year * 12 + $month + $day + $hour;
                break;
            case "d":
                $hour = $hour > 0 ? round($hour / 24, 0) : 0;
                $min = $min > 0 ? round($min / 60, 0) : 0;
                $total = ($year * 365) + ($month * 30) + $day + $hour + $min;
                break;
            case "h":
                $min = $min > 0 ? round($min / 60, 0) : 0;
                $total = ((($year * 365) + ($month * 30) + $day) * 24) + $hour + $min;
                break;
            case "i":
                $sec = $sec > 0 ? round($sec / 60, 0) : 0;
                $total = ((((($year * 365) + ($month * 30) + $day) * 24) + $hour) * 60) + $min + $sec;
                break;
            case "s":
                $total = ((((($year * 365) + ($month * 30) + $day) * 24 + $hour) * 60 + $min) * 60) + $sec;
                break;
        }

        if (strtotime($start) < strtotime($end))
            $total = $total > 0 ? -1 * $total : 0;

        $temp_total = $total;
        if ((string) $temp_total == 'NAN')
            $total = 0;

        return $total;
    }

    /*
     * A mathematical decimal difference between two informed dates 
     *
     * Author: Sergio Abreu
     * Website: http://sites.sitesbr.net
     *
     * Features: 
     * Automatic conversion on dates informed as string.
     * Possibility of absolute values (always +) or relative (-/+)
     */

    public static function s_datediff($str_interval, $dt_menor, $dt_maior, $relative = false) {

        $dt_maior_temp = $dt_maior;
        $dt_menor_temp = $dt_menor;

        if (is_string($dt_menor))
            $dt_menor = date_create($dt_menor);
        if (is_string($dt_maior))
            $dt_maior = date_create($dt_maior);

        $diff = date_diff($dt_menor, $dt_maior, !$relative);

        switch ($str_interval) {
            case "y":
                $total = $diff->y + $diff->m / 12 + $diff->d / 365.25;
                break;
            case "m":
                $total = $diff->y * 12 + $diff->m + $diff->d / 30 + $diff->h / 24;
                break;
            case "d":
                $h = $diff->h > 0 ? $diff->h / 24 : 0;
                $i = $diff->i > 0 ? $diff->i / 60 : 0;
                $total = $diff->y * 365.25 + $diff->m * 30 + $diff->d + $h + $i;
                break;
            case "h":
                $total = ($diff->y * 365.25 + $diff->m * 30 + $diff->d) * 24 + $diff->h + $diff->i / 60;
                break;
            case "i":
                $total = (($diff->y * 365.25 + $diff->m * 30 + $diff->d) * 24 + $diff->h) * 60 + $diff->i + $diff->s / 60;
                break;
            case "s":
                $total = ((($diff->y * 365.25 + $diff->m * 30 + $diff->d) * 24 + $diff->h) * 60 + $diff->i) * 60 + $diff->s;
                break;
        }


// if ($diff->invert)
// return -1 * round($total, 0);
// else
// return round($total, 0);

        $total = round($total, 0);
        if (strtotime($dt_menor_temp) < strtotime($dt_maior_temp))
            $total = -1 * $total;

        return $total;
    }

    public static function form_dropdown_image($name = '', $options = array(), $selected = array(), $extra = '') {
        $dropdown_print_options = array();
        $dropdown_print_options_icon = array();

        foreach ($options as $key => $value) {
            $dropdown_print_options[$key] = isset($value['title']) ? $value['title'] : '';
            $dropdown_print_options_icon[$key] = isset($value['icon']) ? $value['icon'] : '';
        }

        return form_dropdown($name, $dropdown_print_options, $selected, $extra, 'icon', $dropdown_print_options_icon);
    }

    public static function minute_to_hours($time, $format) {
        settype($time, 'integer');
        if ($time < 1) {
            return;
        }
        $hours = floor($time / 60);
        $minutes = ($time % 60);
        return sprintf($format, $hours, $minutes);
    }

    public static function __switch_number($number) {
        if(!empty($number)||$number != 0){
            $pecah_titik = str_replace('.', '', $number);
            list($uang, $ekor) = explode(',', $pecah_titik);
            return $uang;
        }else{
            return $number;
        }
    }

    public static function switch_number($number) {
        if(!empty($number)||$number != 0){
            $pecah_titik = str_replace('.', '', $number);
            $pecah_titik = str_replace(',', '.', $pecah_titik);
            //list($uang, $ekor) = explode(',', $pecah_titik);
            return $pecah_titik;
        }else{
            return $number;
        }
    }

    public static function switch_number2($number) {
        if(!empty($number)||$number != 0){
            $pecah_titik = str_replace('.', '', $number);
            $pecah_titik = str_replace(',', '.', $pecah_titik);
            //list($uang, $ekor) = explode(',', $pecah_titik);
            return $pecah_titik;
        }else{
            return $number;
        }
    }

    public static function font_awesome($kode = '', $default = '--Pilih Icon--', $key = '') {
        $font_list = array();
        $font_source = FCPATH . 'icon/icon-list.txt';

        if (file_exists($font_source)) {
            $font_content = file_get_contents($font_source);
            $font_list = explode(',', $font_content);
        }

        if (empty($kode)) {
            $data = array();
            foreach ($font_list as $value) {
                $value = trim($value);
                $data[$value] = $value;
            }
            if (!empty($default)) {
                return self::merge_array(array($key => $default), $data);
            } else {
                return $data;
            }
        }
    }

    public static function list_month($default = '--Pilih Bulan--') {
        $options = array(0 => $default, "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
        return $options;
    }

    public static function list_year($default = '--Pilih Tahun--', $start = 2014, $range = 2) {
        $options = array();

        if (!empty($default))
            $options[] = $default;

        $now = date('Y');
        $start_year = $start;
        $end_year = $now + $range;

        for ($i = $end_year; $i >= $start_year; $i--) {
            $options[$i] = $i;
        }
        return $options;
    }

    public static function nama_bulan($key) {
        $month = self::list_month();
        return $month[$key];
    }

    public static function opsi_lulus($kode = '', $default = '--Pilih--', $key = '') {
        if (empty($kode)) {
            if (!empty($default)) {
                return self::merge_array(array($key => $default), self::$lulus);
            } else {
                return self::$lulus;
            }
        } else {
            return isset(self::$lulus[$kode]) ? self::$lulus[$kode] : '-';
        }
    }

    public static function opsi_penghargaan($kode = '', $default = '--Pilih Kelompok--', $key = '') {
        if (empty($kode)) {
            if (!empty($default)) {
                return self::merge_array(array($key => $default), self::$kelompok_penghargaan);
            } else {
                return self::$kelompok_penghargaan;
            }
        } else {
            return isset(self::$kelompok_penghargaan[$kode]) ? self::$kelompok_penghargaan[$kode] : '-';
        }
    }

    public static function opsi_dokumen($kode = '', $default = '--Pilih Jenis Dokumen--', $key = '') {
        if (empty($kode)) {

            if (!empty($default)) {
                return self::merge_array(array($key => $default), self::$jenis_dokumen);
            } else {
                return self::$jenis_dokumen;
            }
        } else {
            return isset(self::$jenis_dokumen[$kode]) ? self::$jenis_dokumen[$kode] : '-';
        }
    }

    public static function opsi_dokumen_jabatan($kode = '', $default = '--Pilih Jenis Dokumen--', $key = '') {
        if (empty($kode)) {

            if (!empty($default)) {
                return self::merge_array(array($key => $default), self::$jenis_dokumen_jabatan);
            } else {
                return self::$jenis_dokumen_jabatan;
            }
        } else {
            return isset(self::$jenis_dokumen_jabatan [$kode]) ? self::$jenis_dokumen_jabatan [$kode] : '-';
        }
    }

    public static function opsi_satuan_waktu($kode = '', $default = '--Pilih--', $key = '') {
        if (empty($kode)) {

            if (!empty($default)) {
                return self::merge_array(array($key => $default), self::$satuan_waktu);
            } else {
                return self::$satuan_waktu;
            }
        } else {
            return isset(self::$satuan_waktu[$kode]) ? self::$satuan_waktu[$kode] : '-';
        }
    }

    public static function opsi_kelas_plafond($kode = '', $default = '--Pilih--', $key = '') {
        if (empty($kode)) {

            if (!empty($default)) {
                return self::merge_array(array($key => $default), self::$kelas_plafond);
            } else {
                return self::$kelas_plafond;
            }
        } else {
            return isset(self::$kelas_plafond[$kode]) ? self::$kelas_plafond[$kode] : '-';
        }
    }

    public static function opsi_satuan_nominal($kode = '', $default = '--Pilih--', $key = '') {
        if (empty($kode)) {

            if (!empty($default)) {
                return self::merge_array(array($key => $default), self::$satuan_nominal);
            } else {
                return self::$satuan_nominal;
            }
        } else {
            return isset(self::$satuan_nominal[$kode]) ? self::$satuan_nominal[$kode] : '-';
        }
    }

    public static function opsi_kategori_perawatan($kode = '', $default = '--Pilih--', $key = '') {
        if (empty($kode)) {
            if (!empty($default)) {
                return self::merge_array(array($key => $default), self::$kategori_perawatan);
            } else {
                return self::$kategori_perawatan;
            }
        } else {
            return isset(self::$kategori_perawatan[$kode]) ? self::$kategori_perawatan[$kode] : '-';
        }
    }

}

/* End of file hgenerator_helper.php */
/* Location: ./application/helpers/hgenerator_helper.php */


function array_multi_to_single($arr){
    $temp = array();
    foreach ($arr as $value) {
        foreach ($value as $v) {
            $temp[] = $v;
        }
    }

    return $temp;
}

function array_values_index($arr, $index){
    $temp = array();
    foreach ($arr as $value) {
        $temp[] = $value[$index];
    }
    return $temp;
}

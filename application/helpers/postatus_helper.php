<?php
class postatus {
	const OPEN = 0;
	const WAITING_RECEIVE = 1;
	const DC_TRANSIT = 2;
	const SHIP = 3;
	const CLOSE = 4;
	const CANCEL = 5;

	
	public static function getStatus($id){
		switch ($id) {
			case 0:
				$stat = 'OPEN';
				break;
			case 1:
				$stat = 'WAITING FOR RECEIVE';
				break;
			case 2:
				$stat = 'DC TRANSIT';
				break;
			case 3:
				$stat = 'SHIP TO STORE';
				break;
			case 4:
				$stat = 'CLOSE';
				break;
			case 5:
				$stat = 'CANCEL';
				break;
			default:
				# code...
				break;
		}
		return $stat;

	}
}


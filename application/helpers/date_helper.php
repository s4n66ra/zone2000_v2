<?php
use Carbon\Carbon;

class date {

	public function __construct() {
		self::init();
	}

	public static function init(){
		//Carbon::setLocale('id');
	}

	public static function now_sql(){
		return Date('Y-m-d H:i:s');
	}

	public static function shortDate($date,$type = 0){
		if($date == NULL)
			return NULL;
		$date = Carbon::parse($date);
		if($type == 0){
			return strtoupper($date->format('j-m-Y'));
		}else{
			return strtoupper($date->format('j M Y'));
		}
	}

	public static function getDate($date,$type = 0){
		$date = Carbon::parse($date);
		return strtoupper($date->format('Y-m-d'));
	}

	public static function longDate($date){
		$date = Carbon::parse($date);
		return strtoupper($date->format('j M Y - H:i'));
	}

	public static function diff_day($past, $future = NULL){

		if(!$future)
			$date2 = Carbon::now();
		else
			$date2 = Carbon::parse($future);	
		$date = Carbon::parse($past);
		
		return $date->diffForHumans($date2);
		$datetime1 = new DateTime($past);

		if(!$future)
			$datetime2 = new DateTime();
		else
			$datetime2 = new DateTime($future);
		
		$interval = $datetime1->diff($datetime2);
		$tanda = $interval->format('%R');
		$hari  = $interval->format('%a');

		if($hari==0)
			return 'today';

		if($tanda=='+')
			return $hari.' days ago';
	}

	public static function getMonthName($month){
		$CI =& get_instance();
		$config = $CI->config->item('date');

		return $config['month_name_en'][$month];
	}


}




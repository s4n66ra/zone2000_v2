<?php
    session_start();

    include APPPATH.'libraries/WebClientPrint.php';

    use Neodynamic\SDK\Web\WebClientPrint;
    use Neodynamic\SDK\Web\Utils;

    //$title = 'WebClientPrint 2.0 for PHP - Print Commands Demo';
    $sid = session_id(); 
    $cacheFileName = (Utils::strEndsWith(WebClientPrint::$wcpCacheFolder, '/')?WebClientPrint::$wcpCacheFolder:WebClientPrint::$wcpCacheFolder.'/').$sid.'.ini';
  
?>



<?php
  $content = ob_get_contents();
  ob_clean();
?>    

<script type="text/javascript" src="../zone2000/assets/js/lib/printer/formToWizard2.js"></script>
<script type="text/javascript" src="../zone2000/assets/js/lib/printer/DemoPrintCommands.js"></script>    

<?php
  $currentFileName = basename($_SERVER['PHP_SELF']);
  $currentFolder = substr($_SERVER['REQUEST_URI'], 0, strlen($_SERVER['REQUEST_URI']) - strlen($currentFileName));
  //Specify the ABSOLUTE URL to the php file that will create the ClientPrintJob object
  //echo WebClientPrint::createScript(Utils::getRoot().$currentFolder.'DemoPrintCommandsProcess.php')
    echo WebClientPrint::createScript(Utils::getRoot().'/PrintZPLSample/WebsiteSampleDemo/DemoPrintCommandsProcess.php');
?>


<?php
  $script = ob_get_contents();
  ob_clean();
  
    //include(APPPATH."libraries/template.php");
?>
<?php echo isset($script)?$script:''; ?>



    

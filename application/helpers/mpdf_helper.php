<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function to_pdf($html, $instansi, $due = '', $cover = '', $watermark = array(), $orientation = '')
{
    $urinya = base_url() . 'application/helpers/mpdf/';
    define('_MPDF_URI', $urinya);     // must be  a relative or absolute URI - not a file system path

    include("mpdf/mpdf.php");

    $mpdf = new mPDF('utf-8', 'A4' . $orientation);

    $mpdf->StartProgressBarOutput(2);
    $stylesheet = file_get_contents(site_url('assets/styles/laporan.css'));

    $mpdf->WriteHTML($stylesheet, 1);

    if (is_array($watermark) and (count($watermark) > 0)) {
        $mpdf->SetWatermarkText($watermark['title'], 0.05);
        $mpdf->showWatermarkText = true;
    }

    if (!empty($cover)) {
        $mpdf->AddPage('', '', '', '', 'on');
        $mpdf->WriteHTML($cover);
    }
    if (!empty($instansi)or !empty($due)) {
        $mpdf->SetHeader($instansi . '||Posisi ' . $due);
    }
    if (!empty($cover)) {
        $mpdf->AddPage('', 'NEXT', '0', '1', 'off');
    }
    $mpdf->SetFooter('Hal {PAGENO} dari {nb} halaman');

    $mpdf->WriteHTML($html);


    $mpdf->Output();
    exit;
}
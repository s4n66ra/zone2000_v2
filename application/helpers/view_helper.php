<?php
use Carbon\Carbon;


class view {

	private static $id_form_input = 'finput';
	private static $CI;

	public static function button_add($option = array()){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return anchor(
			"javascript://", 
			'<i class="fa fa-plus"></i> Add New', 
			array(
				'id' 			=> 'button-add',
				'full-width' 	=> $option['full-width'],
				'class' 		=> 'btn green-turquoise btn-form-modal', 
				'onclick' 		=> ($option['onclick'] ? $option['onclick'] :'btnLoadModal(this)'), 
				'data-source' 	=> base_url($controller . '/add'),
			)
		);
	}

	public static function format_day($number){		
		if(!empty($number)){
			return $number.' Days';
		}else{
			return NULL;
		}
	}
	public static function getStatusMesin($id=null){
		if($id == 1){
			return 'FINE';
		}elseif($id == 0){
			return 'BROKEN';
		}elseif ($id == 2) {
			return 'LIQUIDATE';
		}else{
			return NULL;
		}
	}
	
	public static function button_import($option = array()){
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return anchor(
				"javascript://",
				'<i class="fa fa-plus"></i> Add New',
				array(
						'id' 			=> 'button-add',
						'full-width' 	=> $option['full-width'],
						'class' 		=> 'btn green-turquoise btn-form-modal',
						'onclick' 		=> ($option['onclick'] ? $option['onclick'] :'load_form_modal_2(this.id,\'#ffilter\')'),
						'data-source' 	=> base_url($controller . '/add'),
				)
		);
	}
	
	public static function button_export($option = array()){
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return anchor(
				"javascript://",
				'<i class="fa fa-print"></i> Print and Export',
				array(
						'id' 			=> 'button-add',
						'full-width' 	=> $option['full-width'],
						'class' 		=> 'btn',
						'onclick' 		=> ($option['onclick'] ? $option['onclick'] :'load_form_modal_2(this.id,\'#ffilter\')'),
						'data-source' 	=> base_url($controller . '/add'),
				)
		);
	}

	public static function button_export_laporan_payout(){
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return anchor(
				"javascript://",
				'<i class="fa fa-print"></i> Print and Export',
				array(
						'id' 			=> 'button-add',
						'full-width' 	=> $option['full-width'],
						'class' 		=> 'btn',
						'onclick' 		=> 'printExcelLaporanPayout()',
						'data-source' 	=> base_url($controller . '/add'),
				)
		);
	}

	public static function button_export_laporan_penjualan(){
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return anchor(
				"javascript://",
				'<i class="fa fa-print"></i> Print and Export',
				array(
						'id' 			=> 'button-add',
						'full-width' 	=> $option['full-width'],
						'class' 		=> 'btn',
						'onclick' 		=> 'printExcelLaporanPenjualan()',
						'data-source' 	=> base_url($controller . '/add'),
				)
		);
	}

	public static function button_export_laporan(){
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		$url = base_url($controller . '/excel');
		return anchor(
				"javascript://",
				'<i class="fa fa-print"></i> Print and Export',
				array(
						'id' 			=> 'button-add',
						'full-width' 	=> $option['full-width'],
						'class' 		=> 'btn',
						//'onclick' 		=> 'printExcelLap('.base_url($controller . '/excel').')',
						'onclick' 		=> "printExcelLap('".$url."')",
						'data-source' 	=> base_url($controller . '/add'),
				)
		);
	}

	public static function button_export2($url){
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return anchor(
				"javascript://",
				'<i class="fa fa-print"></i> Print and Export',
				array(
						'id' 			=> 'button-add',
						'full-width' 	=> $option['full-width'],
						'class' 		=> 'btn',
						'onclick' 		=> "printExcelJs('$url')",
						'data-source' 	=> base_url($controller . '/add'),
				)
		);
	}

	//BUTTON BARCODE ALL MEWAKILI SEMUA BUTTON
	//YANG ADA DI BEBERAPA HALAMAN PRINT BARCODE ALL
	public static function button_all_barcode($id = '', $option = array(), $message=''){
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return anchor(
			"javascript://", 
			'<i class="fa fa-barcode"></i> Print All Barcode '.$message, 
			array(
				'id'			=> 'button-barcode-'.$id,
				'full-width' 	=> $option['full-width'],
				'class' 		=> 'btn',
				'onclick' 		=> ($option['onclick'] ? $option['onclick'] : 'load_form_modal_2(this.id)'), 
				'data-source' 	=> base_url($controller . '/barcodeall/'.$id),
			)
		);
	}


	public static function button_edit($id = '', $option = array()){	
		
		$ci =& get_instance();
		if($ci->access_right->otoritas('edit')){
			$controller = $ci->router->fetch_class();
			return anchor(
				"javascript://", 
				'<i class="fa fa-edit"></i> Edit', 
				array(
					'id'			=> 'button-edit-'.$id,
					'primary'		=> $id,
					'full-width' 	=> $option['full-width'],
					'class' 		=> 'btn blue contain-primary', 
					'onclick' 		=> ($option['onclick'] ? $option['onclick'] : 'load_form_modal_2(this.id)'), 
					'data-source' 	=> base_url($controller . '/edit/'.$id),
				)
			);
		}
	}

	public static function button_read($id = '', $option = array()){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return anchor(
			"javascript://", 
			'<i class="fa fa-edit"></i> Read', 
			array(
				'id'			=> 'button-edit-'.$id,
				'primary'		=> $id,
				'full-width' 	=> $option['full-width'],
				'class' 		=> 'btn blue contain-primary', 
				'onclick' 		=> ($option['onclick'] ? $option['onclick'] : 'read_notif('.base64_decode($id).')'), 
				'data-source' 	=> base_url($controller . '/edit/'.$id),
			)
		);
	}

	
	public static function button_read_pr_notification($id = '', $option = array()){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return anchor(
			"javascript://", 
			'<i class="fa fa-edit"></i> Read', 
			array(
				'id'			=> 'button-edit-'.$id,
				'primary'		=> $id,
				'full-width' 	=> $option['full-width'],
				'class' 		=> 'btn blue contain-primary', 
				'onclick' 		=> ($option['onclick'] ? $option['onclick'] : 'read_notif_pr('.base64_decode($id).')'), 
				'data-source' 	=> base_url($controller . '/edit/'.$id),
			)
		);
	}



	public static function button_view($id = '', $option = array()){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return anchor(
			"javascript://", 
			'<i class="glyphicon glyphicon-eye-open"></i> View', 
			array(
				'id'			=> 'button-view-'.$id,
				'full-width' 	=> $option['full-width'],
				'class' 		=> 'btn btn-xs btn-default', 
				'onclick' 		=> 'load_form_modal_2(this.id)', 
				'data-source' 	=> base_url($controller . '/view/'.$id),
			)
		);
	}

	public static function button_act($id = '', $option = array()){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return anchor(
			"javascript://", 
			'<i class="fa fa-flash"></i> Assign', 
			array(
				'id'			=> 'button-act-'.$id,
				'full-width' 	=> $option['full-width'],
				'class' 		=> 'btn btn-xs btn-default', 
				'onclick' 		=> 'load_form_modal_2(this.id)', 
				'data-source' 	=> base_url($controller . '/assign/'.$id),
			)
		);
	}

	public static function button_service_detail($id = '', $option = array()){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return anchor($controller . '/detail/'.$id,
			'<i class="fa fa-wrench"></i> Detail',
			array(
				'class' 		=> 'btn btn-xs btn-default', 
			));
		/*return anchor(
			"javascript://", 
			'<i class="fa fa-wrench"></i> Detail', 
			array(
				'id'			=> 'button-detail-'.$id,
				'full-width' 	=> $option['full-width'],
				'class' 		=> 'btn btn-xs btn-default', 
				'onclick' 		=> 'load_form_modal_2(this.id)', 
				'data-source' 	=> base_url($controller . '/repair/'.$id),
			)
		);*/
	}

	public static function button_repair($id = '', $option = array()){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return anchor(
			"javascript://", 
			'<i class="fa fa-wrench"></i> Repair', 
			array(
				'id'			=> 'button-repair-'.$id,
				'full-width' 	=> $option['full-width'],
				'class' 		=> 'btn btn-xs btn-default', 
				'onclick' 		=> 'load_form_modal_2(this.id)', 
				'data-source' 	=> base_url($controller . '/repair/'.$id),
			)
		);
	}

	public static function button_sparepart($id = '', $option = array()){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return anchor(
			"javascript://", 
			'<i class="fa fa-wrench"></i> Sparepart', 
			array(
				'id'			=> 'button-sparepart-'.$id,
				'full-width' 	=> $option['full-width'],
				'class' 		=> 'btn btn-xs btn-default', 
				'onclick' 		=> 'load_form_modal_2(this.id)', 
				'data-source' 	=> base_url($controller . '/sparepart/'.$id),
			)
		);
	}

	public static function button_complete($id = '', $option = array()){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return anchor(
			"javascript://", 
			'<i class="glyphicon glyphicon-eye-open"></i> View', 
			array(
				'id'			=> 'button-complete-'.$id,
				'full-width' 	=> $option['full-width'],
				'class' 		=> 'btn btn-xs btn-default', 
				'onclick' 		=> 'load_form_modal_2(this.id)', 
				'data-source' 	=> base_url($controller . '/complete/'.$id),
			)
		);
	}

	public static function button_detail($id = '', $option = array()){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		$action[] = anchor(
						'javascript://', 
						'<i class="fa fa-flash"></i> Supply', 
						array(
							'class' 		=> 'btn btn-supply btn-xs btn-default',
							'bundle-id'		=> $id,
							'onclick'		=> 'btnLoadNextPage(this)',
							'data-source'	=> site_url($controller.'/detail/'.$id),
						)
					);


		return anchor(
			($option['onclick'] ? 'javascript://' : base_url($controller.'/detail/'.$id) ), 
			'<i class="fa fa-cog"></i>'.($option['caption'] ? $option['caption'] : "Detail"), 
			array(
				'id'			=> 'button-detail-'.$id,
				//'full-width' 	=> $option['full-width'],
				'bundle-id'		=> $id,
				'class' 		=> 'btn btn-sm yellow-saffron', 
				'onclick' 		=> ($option['onclick'] ? $option['onclick'] : ''), 
				'data-source' 	=> base_url($controller . '/detail/'.$id),
			)
		);
	}

	public static function button_delete($id = '', $option = array()){		
		$ci =& get_instance();
		if($ci->access_right->otoritas('delete')){
			$controller = $ci->router->fetch_class();
			return anchor(
				"javascript://", 
				'<i class="fa fa-trash-o"></i> Delete', 
				array(
					'id'			=> 'button-delete-'.$id,
					'full-width' 	=> $option['full-width'],
					'class' 		=> 'btn red-pink', 
					'onclick' 		=> ($option['onclick'] ? $option['onclick'] : 'load_form_modal_2(this.id)'), 
					'data-source' 	=> base_url($controller . '/delete/'.$id),
				)
			);
		}
	}

	public static function button_goto_po(){
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return anchor(
			"javascript://", 
			'<i class="fa fa-trash-o"></i> Go to PO', 
			array(
				'id'			=> 'button-delete-'.$id,
				'full-width' 	=> $option['full-width'],
				'class' 		=> 'btn red-pink', 
				'onclick' 		=> 'gotopo()', 
				'data-source' 	=> base_url($controller . '/delete/'.$id),
			)
		);		
	}


	public static function button_barcode($id = '', $option = array()){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return anchor(
			"javascript://", 
			'<i class="fa fa-barcode"></i> Barcode', 
			array(
				'id'			=> 'button-barcode-'.$id,
				'full-width' 	=> $option['full-width'],
				'class' 		=> 'btn green', 
				'onclick' 		=> ($option['onclick'] ? $option['onclick'] : 'load_form_modal_2(this.id)'), 
				'data-source' 	=> base_url($controller . '/barcode/'.$id),
			)
		);
	}

	public static function button_barcode_service($id = '', $option = array()){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return anchor(
			"javascript://", 
			'<i class="fa fa-barcode"></i> Barcode', 
			array(
				'id'			=> 'button-barcode-'.$id,
				'full-width' 	=> $option['full-width'],
				'class' 		=> 'btn green', 
				'onclick' 		=> ($option['onclick'] ? $option['onclick'] : 'load_form_modal_2(this.id)'), 
				'data-source' 	=> base_url($controller . '/barcode/'.$id),
			)
		);
	}



	//BUTTON PRINT BARCODE DI RECEIVING_WAREHOUSE
	public static function button_barcode_koli($id = '', $option = array()){		
		$ci =& get_instance();
		$id_enc = url_base64_encode($id);
		$controller = $ci->router->fetch_class();
		return anchor(
			"javascript://", 
			'<i class="fa fa-barcode"></i> Barcode', 
			array(
				'id'			=> 'button-barcode-'.$id,
				'full-width' 	=> $option['full-width'],
				'class' 		=> 'btn red-pink', 
				'onclick' 		=> ($option['onclick'] ? $option['onclick'] : 'load_form_modal_2(this.id)'), 
				'data-source' 	=> base_url($controller . '/barcode/'.$id_enc),
			)
		);
	}

	//BUTTON PRINT BARCODE DI SPAREPART
	public static function button_barcode_sparepart($id = '', $option = array()){		
		$ci =& get_instance();
		$id_enc = url_base64_encode($id);
		$controller = $ci->router->fetch_class();
		return anchor(
			"javascript://", 
			'<i class="fa fa-barcode"></i> Barcode', 
			array(
				'id'			=> 'button-barcode-'.$id,
				'full-width' 	=> $option['full-width'],
				'class' 		=> 'btn red-pink', 
				'onclick' 		=> ($option['onclick'] ? $option['onclick'] : 'load_form_modal_2(this.id)'), 
				'data-source' 	=> base_url($controller . '/barcode/'.$id),
			)
		);
	}

	//BUTTON PRINT BARCODE DI MESIN
	public static function button_barcode_mesin($id = '', $option = array()){		
		$ci =& get_instance();
		$id_enc = url_base64_encode($id);
		$controller = $ci->router->fetch_class();
		return anchor(
			"javascript://", 
			'<i class="fa fa-barcode"></i> Barcode', 
			array(
				'id'			=> 'button-barcode-'.$id,
				'full-width' 	=> $option['full-width'],
				'class' 		=> 'btn red-pink', 
				'onclick' 		=> ($option['onclick'] ? $option['onclick'] : 'load_form_modal_2(this.id)'), 
				'data-source' 	=> base_url($controller . '/barcode/'.$id),
			)
		);
	}


	public static function button_delete_confirm($id = ''){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return anchor(
			"javascript://", 
			'<i class="fa fa-trash-o"></i> Delete', 
			array(
				'id'			=> 'button-delete',
				'class' 		=> 'btn red', 
				'onclick' 		=> "simpan_data(this.id, '#".self::$id_form_input."', '#button-back')", 
				'data-source' 	=> base_url($controller . '/delete/'.$id),
				'data-confirm'	=> 'Do you sure to delete?'
			)
		);
	}


	public static function button_delete_confirm_form($id = ''){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return anchor(
			"javascript://", 
			'<i class="fa fa-trash-o"></i> Delete', 
			array(
				'id'			=> 'button-delete',
				'class' 		=> 'btn red component-form-save',
			)
		);
	}

	public static function button_print(){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();

		
		return anchor(
			"javascript://", 
			'<i class="fa fa-print"></i> Print Barcode', 
			array(
				'id' => 'load-print',
				'class' => 'btn btn-primary', 
				'onclick' => "printZPL()",
			)
		);
	}

	public static function button_print_stok(){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();

		
		return anchor(
			"javascript://", 
			'<i class="fa fa-print"></i> Print Barcode', 
			array(
				'id' => 'load-print',
				'class' => 'btn btn-primary', 
				'onclick' => "printZPLStok()",
			)
		);
	}


	public static function button_print_all_merchandise(){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();

		
		return anchor(
			"javascript://", 
			'<i class="fa fa-print"></i> Print Barcode', 
			array(
				'id' => 'load-print',
				'class' => 'btn btn-primary', 
				'onclick' => "printZPLAllHadiah()",
			)
		);
	}

	public static function button_print_all_stok(){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();

		
		return anchor(
			"javascript://", 
			'<i class="fa fa-print"></i> Print Barcode', 
			array(
				'id' => 'load-print',
				'class' => 'btn btn-primary', 
				'onclick' => "printZPLAllStok()",
			)
		);
	}


	public static function button_print_all_sparepart(){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();

		
		return anchor(
			"javascript://", 
			'<i class="fa fa-print"></i> Print Barcode', 
			array(
				'id' => 'load-print',
				'class' => 'btn btn-primary', 
				'onclick' => "printZPLAllSparepart()",
			)
		);
	}

	public static function button_print_all_mesin(){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();

		
		return anchor(
			"javascript://", 
			'<i class="fa fa-print"></i> Print Barcode', 
			array(
				'id' => 'load-print',
				'class' => 'btn btn-primary', 
				'onclick' => "printZPLAllMesin()",
			)
		);
	}


	public static function button_print_koli(){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();

		
		return anchor(
			"javascript://", 
			'<i class="fa fa-print"></i> Print Barcode', 
			array(
				'id' => 'load-print',
				'class' => 'btn btn-primary', 
				'onclick' => "printZPLKoli()",
			)
		);
	}


	public static function button_print_sparepart(){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();

		
		return anchor(
			"javascript://", 
			'<i class="fa fa-print"></i> Print Barcode', 
			array(
				'id' => 'load-print',
				'class' => 'btn btn-primary', 
				'onclick' => "printZPLSparepart()",
			)
		);
	}

	public static function button_print_mesin(){
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();

		
		return anchor(
			"javascript://", 
			'<i class="fa fa-print"></i> Print Barcode', 
			array(
				'id' => 'load-print',
				'class' => 'btn btn-primary', 
				'onclick' => "printZPLMesin()",
			)
		);
	}

	public static function button_print_stok_sc(){
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();

		
		return anchor(
			"javascript://", 
			'<i class="fa fa-print"></i> Print Barcode', 
			array(
				'id' => 'load-print',
				'class' => 'btn btn-primary', 
				'onclick' => "printZPLStokSC()",
			)
		);
	}


	public static function button_print_service(){
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();

		
		return anchor(
			"javascript://", 
			'<i class="fa fa-print"></i> Print Barcode', 
			array(
				'id' => 'load-print',
				'class' => 'btn btn-primary', 
				'onclick' => "printZPLService()",
			)
		);
	}


	public static function button_save(){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return anchor(
			"javascript://", 
			'<i class="fa fa-save"></i> Save', 
			array(
				'id' => 'button-save',
				'class' => 'btn green-jungle', 
				'onclick' => "simpan_data(this.id, '#finput', '#button-back')", 
			)
		);
	}

	public static function button_save_notification(){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return anchor(
			"javascript://", 
			'<i class="fa fa-save"></i> Send', 
			array(
				'id' => 'button-save-notification',
				'class' => 'btn green-jungle save_notification', 
				'onclick' => "updateNotification()",
				'data-dismiss' => "modal", 
				'disabled' => 'disabled'
			)
		);
	}


	public static function button_save_form(){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return anchor(
			"javascript://", 
			'<i class="fa fa-save"></i> Save', 
			array(
				'id' => 'button-save',
				'class' => 'btn green-jungle component-form-save',
			)
		);
	}

	public static function button_save_transaction(){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return anchor(
			"javascript://", 
			'<i class="fa fa-save"></i> Save', 
			array(
				'data-source' => site_url('api/transaction/multi/save'),
				'class' => 'btn green-jungle table-data-save',
			)
		);
	}

	public static function button_confirm(){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return anchor(
			"javascript://", 
			'<i class="fa fa-check"></i> Confirm', 
			array(
				'id' => 'button-save',
				'class' => 'btn yellow-casablanca', 
				'onclick' => "simpan_data(this.id, '#finput', '#button-back')", 
			)
		);
	}

	public static function button_confirm_cancel_po(){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return anchor(
			"javascript://", 
			'<i class="fa fa-check"></i> Confirm', 
			array(
				'id' => 'button-cancel-po',
				'class' => 'btn yellow-casablanca', 
				'onclick' => 'cancel_purchase_order()', 
			)
		);
	}

	public static function button_add_detail($option = array()){
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return anchor(
			"javascript://", 
			'<i class="fa fa-plus"></i> Add Item', 
			array(
				'id' 			=> 'button-add-detail',
				'full-width' 	=> $option['full-width'],
				'class' 		=> 'btn green-turquoise btn-form-modal', 
				'onclick' 		=> 'load_form_modal_2(this.id,\'#ffilter\')', 
				'data-source' 	=> base_url($controller . '/add_detail/'.$option['id']),
			)
		);
	}

	public static function button_edit_detail($id = '', $option = array()){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return anchor(
			"javascript://", 
			'<i class="fa fa-edit"></i>', 
			array(
				'id'			=> 'button-edit-detail-'.$id,
				'full-width' 	=> $option['full-width'],
				'class' 		=> 'btn grey-cascade btn-xs', 
				'onclick' 		=> 'load_form_modal_2(this.id)', 
				'data-source' 	=> base_url($controller . '/edit_detail/'.$id),
				'title'			=> 'edit',
			)
		);
	}

	public static function button_delete_detail($id = '', $option = array()){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return anchor(
			"javascript://", 
			'<i class="fa fa-trash-o"></i>', 
			array(
				'id'			=> 'button-delete-'.$id,
				'full-width' 	=> $option['full-width'],
				'delete-id'		=> $id,
				'class' 		=> 'btn grey-cascade btn-xs btn-delete-confirm', 
				'data-source' 	=> base_url($controller . '/proses_delete_detail/'.$id),
			)
		);
	}

	public static function button_back_detail($id = '', $option = array()){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return anchor(
			base_url($controller), 
			'<i class="fa fa-mail-reply"></i> Back', 
			array(
				'id'			=> 'button-cancel',
				'class' 		=> 'btn btn-default', 
			)
		);
	}

	public static function button_send_request($id = '', $option = array()){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return anchor(
			'javascript://', 
			'<i class="fa fa-send"></i> Send Request', 
			array(
				'id'			=> 'button-send',
				'class' 		=> 'btn blue', 
				'confirm-body'  => 'Are you sure Raise Purchase Request?',
				'onclick' 		=> 'btnPageSave(this)', 
				'data-source' 	=> base_url($controller . '/proses_request/'.$id),
			)
		);
	}

	public static function button_cancel_request($id = '', $option = array()){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return anchor(
			'javascript://', 
			'<i class="fa fa-arrows-alt"></i> Cancel Request', 
			array(
				'id'			=> 'button-cancel-request',
				'class' 		=> 'btn grey-cascade', 
				'onclick' 		=> 'load_form_modal_2(this.id)', 
				'data-source' 	=> base_url($controller . '/confirm/cancel/'.$id),
			)
		);
	}


	public static function button_receiving($id = '', $option = array()){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return anchor(
			'javascript://', 
			'<i class="glyphicon glyphicon-download-alt"></i> Receive', 
			array(
				'id'			=> 'button-receive-request',
				'class' 		=> 'btn blue-madison btn-receiving', 
				'url-action'	=> ($option['url-action'] ? $option['url-action'] : ''),
				'onclick' 		=> ($option['onclick'] ? $option['onclick'] : ''), 
				'data-source' 	=> base_url($controller . '/proses_receiving/'.$id),

			)
		);
	}

	public static function button_approve_request($id = '', $option = array()){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return anchor(
			'javascript://', 
			'<i class="fa fa-check"></i> Approve', 
			array(
				'id'			=> 'button-approve'.$id,
				'class' 		=> 'btn green-turquoise', 
				'onclick' 		=> ($option['onclick'] || $option['onclick']=='' ? $option['onclick'] : 'load_form_modal_2(this.id)'), 
				'data-source' 	=> base_url($controller . '/confirm/approve/'.$id),
			)
		);
	}

	

	public static function button_shipping($id = '', $option = array()){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return anchor(
			'javascript://', 
			'<i class="fa fa-truck"></i> Shipping', 
			array(
				'id'			=> 'button-shipping',
				'class' 		=> 'btn blue-madison btn-shipping', 
				//'onclick' 		=> 'formSave("#div-form-shipping")', 	
				'data-source' 	=> base_url($controller . '/proses_shipping/'.$id),

			)
		);
	}

	public static function button_print_pdf($id = '', $option = array()){
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return anchor(
				"javascript://",
				'<i class="fa fa-print"></i> Print and Export',
				array(
						'id' 			=> 'button-print-pdf',
						'class' 		=> 'btn blue-madison btn-print-pdf',
						//'onclick' 		=> 'do_print(this.id)',
						'data-source' 	=> base_url($controller . '/print_pdf/'.$id),
				)
		);
	}

	public static function button_revisi_request($id = '', $option = array()){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return anchor(
			'javascript://', 
			'<i class="glyphicon glyphicon-pencil"></i> Revisi', 
			array(
				'id'			=> 'button-revisi',
				'class' 		=> 'btn yellow',
				'onclick' 		=> 'load_form_modal_2(this.id)', 
				'data-source' 	=> base_url($controller . '/confirm/revisi/'.$id), 
			)
		);
	}

	public static function button_add_table($option = array()){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return anchor(
			"javascript://", 
			'<i class="fa fa-plus"></i> Add New', 
			array(
				'class' => 'btn green-turquoise btn-table-add', 
			)
		);
	}

	public static function button_back(){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return anchor(
			"javascript://", 
			'<i class="fa fa-arrow-left"></i> Back', 
			array(
				'id' => 'button-back',
				'class' => 'btn btn-default', 
				'data-dismiss' => "modal",
				//'onclick' => 'close_form_modal(this.id)', 
			)
		);
	}

	public static function form_input($id = '', $options = array()){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();

		return '<form action="'.base_url( ($options['action'] ? $options['action'] : $controller.'/proses')).'" method="post" id="'.( $options['id'] ? $options['id'] : self::$id_form_input).'" class="" novalidate="novalidate"><input name = "id" type="hidden" value="'.$id.'">';
	}

	public static function form_approve($id = '', $options = array()){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();

		return '<form action="'.base_url( ($options['action'] ? $options['action'] : $controller.'/proses_approve')).'" method="post" id="'.( $options['id'] ? $options['id'] : self::$id_form_input).'" class="" novalidate="novalidate"><input name = "id" type="hidden" value="'.$id.'">';
	}

	public static function form_reject($id = '', $options = array()){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();

		return '<form action="'.base_url( ($options['action'] ? $options['action'] : $controller.'/proses_reject')).'" method="post" id="'.( $options['id'] ? $options['id'] : self::$id_form_input).'" class="" novalidate="novalidate"><input name = "id" type="hidden" value="'.$id.'">';
	}

	public static function form($id = '' , $action){
		return '<form action="'.base_url($action).'" method="post" id="'.self::$id_form_input.'" class="" novalidate="novalidate"><input name = "id" type="hidden" value="'.$id.'">';
	}

	public static function form_input_detail($id = ''){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();

		return '<form action="'.base_url($controller.'/proses_detail').'" method="post" id="'.self::$id_form_input.'" class="" novalidate="novalidate"><input name = "id" type="hidden" value="'.$id.'">';
	}

	public static function form_delete($id = ''){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();

		return '<form action="'.base_url($controller.'/proses_delete').'" method="post" id="'.self::$id_form_input.'" class="" novalidate="novalidate"><input name = "id" type="hidden" value="'.$id.'">';
	}

	public static function form_delete_detail($id = ''){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();

		return '<form action="'.base_url($controller.'/proses_delete_detail').'" method="post" id="'.self::$id_form_input.'" class="" novalidate="novalidate"><input name = "id" type="hidden" value="'.$id.'">';
	}

	public static function form_filter($id='', $options = array()){
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return '<form method="post" id="'.( $options['id'] ? $options['id'] : self::$id_form_input).'" class="" novalidate="novalidate"><input id = "filter" placeholder="Filter..."'.' style="position: relative; vertical-align: top; background-color: transparent;" class="form-control tt-input"> </form>';
	}

	public static function form_inventory_summary($id='', $options = array()){
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return '
		<div class="form-group">
			<form method="post" id="'.( $options['id'] ? $options['id'] : self::$id_form_input).'" class="" novalidate="novalidate">
				<label>Filter By Type :</label>
				<input id = "filter" placeholder="Filter..."'.' style="position: relative; vertical-align: top; background-color: transparent;" class="form-control tt-input hidden"> 
			</form>
		</div>'
		;
	}

	public static function render_button_group($list = array(),$option = array(), $wrapped = false){			
		if(sizeof($list)<=0)
			return;
		else {
			
			if ($wrapped) {
				//<button type="button" class="btn btn-default btn-delete-confirm">Tools</button>

				$bg = '<div class="btn-group pull-right ">
					<button type="button" class="btn btn-default">Tools</button>
					<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-angle-down"></i>
					</button>
					<ul class="dropdown-menu" role="menu">';
				 
				foreach ($list as $value)
					$bg .= $value;
				'<li><a href="#"> Action </a></li>
						<li><a href="#"> Another action </a></li>
						<li><a href="#"> Something else here </a></li>
						<li class="divider"></li>
						<li><a href="#"> Separated link </a></li>';
				 
				$bg .= '</ul>
				</div>';
			} else {
				$bg = '<div class="btn-group ' . $option ['class'] . '" style="text-align:center;" >';
				foreach ($list as $value)
		        	$bg .= $value;
							'<li><a href="#"> Action </a></li>
							<li><a href="#"> Another action </a></li>
							<li><a href="#"> Something else here </a></li>
							<li class="divider"></li>
							<li><a href="#"> Separated link </a></li>';
				$bg .= '</div>';
			}
		}
        return $bg;
	}

	public static function render_button_group_laporan_payout($list = array(),$option = array(), $wrapped = false){			
		if(sizeof($list)<=0)
			return;
		else {
			
			if ($wrapped) {
				//<button type="button" class="btn btn-default btn-delete-confirm">Tools</button>

				$bg = '<div class="btn-group pull-right ">
					<button type="button" class="btn btn-default">Tools</button>
					<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-angle-down"></i>
					</button>
					<ul class="dropdown-menu" role="menu">';
				 
				foreach ($list as $value)
					$bg .= $value;
				'<li><a href="#"> Action </a></li>
						<li><a href="#"> Another action </a></li>
						<li><a href="#"> Something else here </a></li>
						<li class="divider"></li>
						<li><a href="#"> Separated link </a></li>';
				 
				$bg .= '</ul>
				</div>';
			} else {
				$bg = '<div class="btn-group ' . $option ['class'] . '" style="text-align:center;" >';
				foreach ($list as $value)
		        	$bg .= $value;
							'<li><a href="#"> Action </a></li>
							<li><a href="#"> Another action </a></li>
							<li><a href="#"> Something else here </a></li>
							<li class="divider"></li>
							<li><a href="#"> Separated link </a></li>';
				$bg .= '</div>';
			}
		}
        return $bg;
	}

	public static function render_button_group_laporan_penjualan($list = array(),$option = array(), $wrapped = false){			
		if(sizeof($list)<=0)
			return;
		else {
			
			if ($wrapped) {
				//<button type="button" class="btn btn-default btn-delete-confirm">Tools</button>

				$bg = '<div class="btn-group pull-right ">
					<button type="button" class="btn btn-default">Tools</button>
					<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-angle-down"></i>
					</button>
					<ul class="dropdown-menu" role="menu">';
				 
				foreach ($list as $value)
					$bg .= $value;
				'<li><a href="#"> Action </a></li>
						<li><a href="#"> Another action </a></li>
						<li><a href="#"> Something else here </a></li>
						<li class="divider"></li>
						<li><a href="#"> Separated link </a></li>';
				 
				$bg .= '</ul>
				</div>';
			} else {
				$bg = '<div class="btn-group ' . $option ['class'] . '" style="text-align:center;" >';
				foreach ($list as $value)
		        	$bg .= $value;
							'<li><a href="#"> Action </a></li>
							<li><a href="#"> Another action </a></li>
							<li><a href="#"> Something else here </a></li>
							<li class="divider"></li>
							<li><a href="#"> Separated link </a></li>';
				$bg .= '</div>';
			}
		}
        return $bg;
	}



	public static function render_button_group_portlet($button){
		$content = '';
		foreach ((array)$button as $key => $value) {
			$content.= '
				<div style="display:inline;'.($key==0 ? 'margin-left:5px;' : '').'">
				'.$value.'
				</div>
			';
		}
		return $content;
	}

	public static function render_button_group_raw($source){
		return self::render_button_group(array($source));
	}

	public static function render_button_group_dropdown($buttons = NULL){
		//if(!$buttons) return;

		$bg = '<div class="btn-group">

				<button class="btn grey-steel btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
				<i class="glyphicon glyphicon-flash"></i> Action <i class="fa fa-angle-down"></i>
				</button>
				<ul class="dropdown-menu" role="menu">';

		foreach ($buttons as $key => $button) {
			// menghilangkan class btn
			$button = str_replace('btn ','',$button);
			$bg.= '<li>'.$button.'</li>';
		}

		$bg.= 	
				'</ul>
			</div>';
		return $bg;
	}

	public static function render_status_pr($stat){
		$ci =& get_instance();
		$status = $ci->config->item('status_pr');
		return 
			'<div class="alert alert-success">
				<strong>'.$status[$stat].'</strong>
			</div>';
	}

	public static function render_status_service($stat){
		return 
			'<div class="alert alert-success">
				<strong>'.$stat.'</strong>
			</div>';
	}

	public static function render_revisi_pr($rev){
		return 
			'<div class="alert alert-success">
				<strong>Revision #'.$rev.'</strong>
			</div>';
	}


	public static function button_view_po($id = ''){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return anchor(
			'javascript://', 
			'<i class="fa fa-flash"></i> View', 
			array(
				'class' 		=> 'btn btn-bundle btn-xs btn-default',
				'bundle-id'		=> $id,
				'onclick'		=> 'btnLoadNextPage(this)',
				'data-source'	=> site_url($controller.'/draft/'.$id),
			)
		);
	}

	public static function button_view_bundle($id = ''){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return anchor(
			'javascript://', 
			'<i class="glyphicon glyphicon-search"></i> View', 
			array(
				'class' 		=> 'btn btn-bundle btn-sm btn-default',
				'bundle-id'		=> $id,
				'onclick'		=> 'btnLoadNextPage(this)',
				'data-source'	=> site_url($controller.'/detail/'.$id),
				'callback'		=> 'refreshTotalPo()',
			)
		);
	}

		public static function button_view_bundle_sc($id = ''){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return anchor(
			'javascript://', 
			'<i class="glyphicon glyphicon-search"></i> View', 
			array(
				'class' 		=> 'btn btn-bundle btn-sm btn-default',
				'bundle-id'		=> $id,
				'onclick'		=> 'btnLoadNextPage(this)',
				'data-source'	=> site_url($controller.'/detail/'.$id),
				'callback'		=> 'refreshTotalPoSc()',
			)
		);
	}

	public static function button_view_stok($id = ''){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return anchor(
			'javascript://', 
			'<i class="glyphicon glyphicon-search"></i> View', 
			array(

				'onclick'		=> 'btnLoadNextPage(this)',
				'class' 		=> 'btn btn-bundle btn-sm btn-default',
				'bundle-id'		=> $id,
				'callback'		=> 'refreshTotalPo()',
                'data-source'   => site_url($ci->class_name.'/getForm/table-stok-detail/'.$id),

			)
		);
	}


	public static function btn_group_pr($id){
		$id_enc = url_base64_encode($id);
		$action = array();
		$action[] = self::button_detail($id, array('onclick' => 'btnLoadNextPage(this)'));
        //$action[] = self::button_edit($id_enc);
        $action[] = self::button_delete($id_enc);
        return self::render_button_group_dropdown($action);
	}


	public static function btn_print_barcode_koli($id){
		$id_enc = url_base64_encode($id);
		$action = array();
		$action[] = self::button_barcode_koli($id);
        $action[] = self::button_edit($id_enc);
        //$action[] = self::button_delete($id_enc);
        return self::render_button_group_dropdown($action);
	}


	public static function btn_group_shipping($shipping_code='', $koli_code=''){
		$ci 	=& get_instance();
		$id_enc = url_base64_encode($id);
		$action = array();
		$action[] = anchor(
	            site_url($ci->class_name.'/print_pdf_pengantar/'.$shipping_code.'/'.$koli_code), 
	            '<i class="glyphicon glyphicon-ok"></i> Print Surat Pengantar', 
	            array(
					'class' 		=> 'btn btn-supply btn-xs btn-default',
					'id'			=> $shipping_code.'-'.$koli_code,
					'target'		=> '_blank',
	            )
	        );
		$action[] = anchor(
	            site_url($ci->class_name.'/print_pdf_suratjalan/'.$shipping_code), 
	            '<i class="glyphicon glyphicon-ok"></i> Print Surat Jalan', 
	            array(
					'class' 		=> 'btn btn-supply btn-xs btn-default',
					'id'			=> $shipping_code,
					'target'		=> '_blank',
	            )
	        );
		$action[] = anchor(
	            site_url($ci->class_name.'/print_pdf_all/'.$shipping_code), 
	            '<i class="glyphicon glyphicon-ok"></i> Print All', 
	            array(
					'class' 		=> 'btn btn-supply btn-xs btn-default',
					'id'			=> $shipping_code,
					'target'		=> '_blank',
	            )
	        );
        
        return self::render_button_group_dropdown($action);
	}

	public static function btn_group_receiving_warehouse($id){
		$id_enc = url_base64_encode($id);
		$action = array();
		$action[] = self::button_detail($id, array('onclick' => 'btnLoadNextPage(this)'));
        // $action[] = self::button_edit($id_enc);
        // $action[] = self::button_delete($id_enc);
        return self::render_button_group_dropdown($action);
	}

	public static function btn_group_edit_delete($id,$opt = array()){
		$id_enc = url_base64_encode($id);
		$action = array();
        $action[] = self::button_edit($id_enc, $opt);
        $action[] = self::button_delete($id_enc,$opt);
        return self::render_button_group_dropdown($action);

	}

	public static function btn_group_edit_delete_su_only($id,$opt = array()){
		$ci =& get_instance();
		$group_id = $ci->session->userdata('kd_roles');
		$id_enc = url_base64_encode($id);
		$action = array();
		if($group_id == '15'){ // role su
	        $action[] = self::button_edit($id_enc, $opt);
	        //$action[] = self::button_delete($id_enc,$opt);
	    }
        return self::render_button_group_dropdown($action);

	}

	public static function btn_group_edit($id,$opt = array()){
		$id_enc = url_base64_encode($id);
		$action = array();
        $action[] = self::button_edit($id_enc, $opt);
       //$action[] = self::button_delete($id_enc,$opt);
        return self::render_button_group_dropdown($action);

	}

	public static function btn_group_edit_delete_full_width($id){
		$id_enc = url_base64_encode($id);
		$action = array();
        $action[] = self::button_edit($id_enc,array('full-width'=>1));
        $action[] = self::button_delete($id_enc,array('full-width'=>1));
        return self::render_button_group_dropdown($action);

	}


	public static function btn_group_edit_delete_merchandise($id){
		$id_enc = url_base64_encode($id);
		$action = array();
        $action[] = self::button_edit($id_enc);
        $action[] = self::button_barcode($id_enc);
        $action[] = self::button_delete($id_enc);
        return self::render_button_group_dropdown($action);
	}

	public static function button_barcode_stok($id){
		$id_enc = url_base64_encode($id);
		$action = array();
        $action[] = self::button_barcode($id_enc);
        return self::render_button_group_dropdown($action);
	}


	public static function btn_group_edit_delete_stok_sc($id){
		$id_enc = url_base64_encode($id);
		$action = array();
        $action[] = self::button_barcode($id_enc);
        return self::render_button_group_dropdown($action);
	}

	//BAGIAN NOTIFIKASI NOTIFICATION
	public static function btn_group_edit_delete_notification($id){
		$id_enc = url_base64_encode($id);
		$action = array();
        $action[] = self::button_read($id_enc, array("full-width" => 1));
        return self::render_button_group_dropdown($action);
	}

	public static function btn_group_edit_delete_pr_notification($id){
		$id_enc = url_base64_encode($id);
		$action = array();
        $action[] = self::button_read_pr_notification($id_enc, array("full-width" => 1));
        $action[] = self::button_goto_po();
        return self::render_button_group_dropdown($action);
	}


	public static function btn_group_edit_delete_sparepart($id){
		$id_enc = url_base64_encode($id);
		$action = array();
        $action[] = self::button_edit($id_enc);
        $action[] = self::button_barcode_sparepart($id_enc);
        $action[] = self::button_delete($id_enc);
        return self::render_button_group_dropdown($action);
	}

	public static function btn_group_edit_delete_mesin($id){
		$id_enc = url_base64_encode($id);
		$action = array();
        $action[] = self::button_edit($id_enc, array("full-width" => 1));
        $action[] = self::button_barcode_mesin($id_enc);
        $action[] = self::button_delete($id_enc, array("full-width" => 1));
        return self::render_button_group_dropdown($action);
	}


	public static function button_reject_request($id = '', $option = array()){		
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return anchor(
			'javascript://', 
			'<i class="glyphicon glyphicon-remove"></i> Reject', 
			array(
				'id'			=> 'button-reject'.$id,
				'class' 		=> 'btn red', 
				'onclick' 		=> 'load_form_modal_2(this.id)', 
				'data-source' 	=> base_url($controller . '/confirm/reject/'.$id),
			)
		);
	}

	public static function btn_group_mutasi($id){
		$id_enc = url_base64_encode($id);
		$option['onclick'] = 'load_form_modal_2(this.id)';
		$action = array();
        $action[] = self::button_edit($id_enc);
        $action[] = self::button_delete($id_enc);
        $action[] = self::button_reject_request($id_enc);
        $action[] = self::button_approve_request($id_enc,$option);
        
        //button_reject_request
        return self::render_button_group_dropdown($action);

	}


	public static function btn_group_tools($id){
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		$id_enc = url_base64_encode($id);
		$action = array();
        $action[] = self::button_edit($id_enc);
        $action[] = self::button_delete($id_enc);
        $action[] = anchor(
						'javascript://', 
						'<i class="fa fa-flash"></i> Supply', 
						array(
							'class' 		=> 'btn btn-supply btn-xs btn-default',
							'bundle-id'		=> $id,
							'onclick'		=> 'btnLoadNextPage(this)',
							'data-source'	=> site_url($controller.'/detail/'.$id),
						)
					);
        return self::render_button_group_dropdown($action);
	}


	public static function btn_group_supplier($id){
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		$id_enc = url_base64_encode($id);
		$action = array();
        //$action[] = self::button_barcode_service($id_enc, array("full-width" => 1));
        $action[] = self::button_edit($id_enc,array("full-width" => 1));
        $action[] = self::button_delete($id_enc,array("full-width" => 1));
        $action[] = anchor(
						'javascript://', 
						'<i class="fa fa-flash"></i> Supply', 
						array(
							'class' 		=> 'btn btn-supply btn-xs btn-default',
							'bundle-id'		=> $id,
							'onclick'		=> 'btnLoadNextPage(this)',
							'data-source'	=> site_url($controller.'/detail/'.$id),
						)
					);
        return self::render_button_group_dropdown($action);
	}

	public static function btn_group_service($id){
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		$id_enc = $id;
		$action = array();
        $action[] = self::button_edit($id_enc);
        $action[] = self::button_sparepart($id_enc);
        $action[] = self::button_detail($id_enc,array('onclick' => 'btnLoadNextPage(this)'));
        $action[] = self::button_barcode_service($id_enc, array("full-width" => 1));
        /*$action[] = anchor(
						'javascript://', 
						'<i class="fa fa-flash"></i> Supply', 
						array(
							'class' 		=> 'btn btn-supply btn-xs btn-default',
							'bundle-id'		=> $id,
							'onclick'		=> 'btnLoadNextPage(this)',
							'data-source'	=> site_url($controller.'/detail/'.$id),
						)
					);*/
        return self::render_button_group_dropdown($action);
	}

	public static function btn_group_supplier_detail($id){
		$ci 		=& get_instance();
		$controller = $ci->router->fetch_class();
		$urlFormEdit	= site_url($controller.'/edit_detail/'.$id);
		$urlFormDelete	= site_url($controller.'/proses_delete_detail');
		$btn[] = '<a class="btn grey btn-sm edit-detail-supplier" def-id="'.$id.'" onclick="btnLoadContentSupplier(this)" target="#wrapper-form-supplier-detail" data-source="'.$urlFormEdit.'"><i class="fa fa-edit"></i></a>';
        $btn[] = '<a class="btn btn-delete-confirm grey btn-sm" delete-id="'.$id.'" data-source="'.$urlFormDelete.'" ><i class="fa fa-remove"></i></a>';
 		return self::render_button_group($btn);
	}

	public static function btn_group_po($id=NULL,$status=NULL){
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		//return 


		$ci 		=& get_instance();
		$btn[]		= anchor(
						'javascript://', 
						'<i class="glyphicon glyphicon-search"></i> View', 
						array(
							'class' 		=> 'btn btn-bundle btn-sm btn-default',
							'bundle-id'		=> $id,
							'onclick'		=> 'btnLoadNextPage(this)',
							'data-source'	=> site_url($controller.'/view/'.$id),
							'callback'		=> 'refreshTotalPo()',
						)
					);
		//self::button_view($id);

		$btn[]	= anchor(
            site_url($ci->class_name.'/print_pdf_po/'.$id), 
            '<i class="glyphicon glyphicon-ok"></i> Print PO', 
            array(
				'class' 		=> 'btn btn-supply btn-xs btn-default',
				'id'			=> $id,
				'target'		=> '_blank',
            )
        );
		if($status=='1'){
			$btn[]		= anchor(
							'javascript://', 
							'<i class="glyphicon glyphicon-copy"></i> Copy', 
							array(
								'class' 		=> 'btn btn-bundle btn-sm btn-default',
								'bundle-id'		=> $id,
								'onclick'		=> 'btnLoadNextPage(this)',
								'data-source'	=> site_url($controller.'/copy/'.$id),
								'callback'		=> 'refreshTotalPo()',
							)
						);

		}

		$btn[]		= anchor(
			'javascript://', 
			'<i class="fa fa-arrows-alt"></i> Cancel PO', 
			array(
				'id'			=> 'button-cancel-purchase-order',
				'class' 		=> 'btn grey-cascade', 
				'onclick' 		=> 'load_form_modal_2(this.id);setpoid('.$id.')', 
				'data-source' 	=> base_url($controller . '/confirm/cancel/'.$id),
			)
		);


		return self::render_button_group_dropdown($btn);
	}



	public static function btn_group_po_history($id){
		$ci 		=& get_instance();
		$btn[]		= anchor(
                            "javascript://", 
                            '<i class="glyphicon glyphicon-eye-open"></i> View', 
                            array(
                                'id'            => 'btn-view-history-'.$id,
                                'primary'       => $id,
                                'onclick'       => 'customTableDrilldown(this)', 
                                'data-source'   => site_url($ci->class_name.'/table_history_detail/'.$id),
                            )
                        );
/*		$btn[]		= anchor(
                            "javascript://", 
                            '<i class="glyphicon glyphicon-print"></i> Print Draft PO', 
                            array(
                                'id'            => 'btn-print-history-'.$id,
                                'primary'       => $id,
                            )
                        );*/
		$btn[]		= anchor(
                            "javascript://", 
                            '<i class="glyphicon glyphicon-ok"></i> Generate PO', 
                            array(
								'class' 		=> 'btn btn-supply btn-xs btn-default',
								'id'			=> $id,
								'onclick'		=> 'saveUpdateBundle(this)',
								'name'			=> site_url($controller.'/purchase_order/update_pr_from_historybundle/'.$id),
								//'data-source'	=> site_url($controller.'/detail/'.$id),
                                //'data-source'   => site_url($controller.'/purchase_order/update_pr_from_historybundle/'.$id),
                            )
                        );
		return self::render_button_group_dropdown($btn);
	}

	public static function btn_group_po_history_sc($id){
		$ci 		=& get_instance();
		$btn[]		= anchor(
                            "javascript://", 
                            '<i class="glyphicon glyphicon-eye-open"></i> View', 
                            array(
                                'id'            => 'btn-view-history-'.$id,
                                'primary'       => $id,
                                'onclick'       => 'customTableDrilldown(this)', 
                                'data-source'   => site_url($ci->class_name.'/table_history_detail/'.$id),
                            )
                        );
		$btn[]		= anchor(
                            "javascript://", 
                            '<i class="glyphicon glyphicon-ok"></i> Confirm', 
                            array(
								'class' 		=> 'btn btn-supply btn-xs btn-default',
								'id'			=> $id,
								'onclick'		=> 'saveUpdateBundle(this)',
								'name'			=> site_url($controller.'/purchase_order/update_pr_from_historybundle/'.$id),
								//'data-source'	=> site_url($controller.'/detail/'.$id),
                                //'data-source'   => site_url($controller.'/purchase_order/update_pr_from_historybundle/'.$id),
                            )
                        );
		return self::render_button_group_dropdown($btn);
	}

	public static function btn_group_close_po_history($id){
		$ci 		=& get_instance();
		$btn[]		= anchor(
                            "javascript://", 
                            '<i class="glyphicon glyphicon-eye-open"></i> View', 
                            array(
                                'id'            => 'btn-view-history-'.$id,
                                'primary'       => $id,
     	                        'onclick'       => 'customTableDrilldown(this)', 
                                'data-source'   => site_url($ci->class_name.'/table_history_detail/'.$id),
                            )
                        );


		return self::render_button_group_dropdown($btn);
	}

	public static function btn_group_close_po_history_generated($id){
		$ci 		=& get_instance();
		$btn[]		= anchor(
                            "javascript://", 
                            '<i class="glyphicon glyphicon-eye-open"></i> View', 
                            array(
                                'id'            => 'btn-view-history-'.$id,
                                'primary'       => $id,
     	                        'onclick'       => 'customTableDrilldown(this)', 
                                'data-source'   => site_url($ci->class_name.'/table_history_detail/'.$id),
                            )

                        );
		$btn[]		= anchor(
                            site_url($ci->class_name.'/print_pdf/'.$id), 
                            '<i class="glyphicon glyphicon-ok"></i> Print PDF', 
                            array(
								'class' 		=> 'btn btn-supply btn-xs btn-default',
								'id'			=> $id,
								'target'		=> '_blank',
                            )
                        );

		return self::render_button_group_dropdown($btn);
	}

	public static function btn_group_edit_delete_page($id){
		$id_enc = url_base64_encode($id);
		$action = array();
        $action[] = self::button_edit($id_enc, array('onclick'=>'btnLoadNextPage(this)'));
        $action[] = self::button_delete($id_enc, array('onclick'=>'btnLoadNextPage(this)'));
        return self::render_button_group_dropdown($action);
	}
	

	public static function btn_datatable_detail($id, $option = array()){
		$id_enc = url_base64_encode($id);
		$option = array_merge($option, array('onclick' => 'btnLoadNextPage(this)'));
		return self::button_detail($id, $option);
	}

	public static function statusPrCreateType($type){
		$ci =& get_instance();
		$conf = $ci->config->item('status_prCreateType');
		
		return $conf[$type];
	}

	public static function statusNotification($angka=0){
		$conf_status = "";
		if($angka==0){
			$conf_status='<font color="red">BELUM DIBACA</font>';
		}else{
			$conf_status='<font color="green">SUDAH DIBACA</font>';
		}
		return $conf_status;
	}

	public static function button_icon_select($icon){
		$ci =& get_instance();
		$controller = $ci->router->fetch_class();
		return anchor(
			'javascript://', 
			'<i class="'.$icon.'"></i> Change Icon', 
			array(
				'class' 		=> 'btn btn-icon',
				'full-width'	=> 1,
				'onclick'		=> 'btnLoadModal(this)',
				'data-source'	=> site_url('source/icon'),
			)
		);
	}

	public static function dropdown_month($month = 0){
		$ci =& get_instance();
		$config = $ci->config->item('date');

		$htmlOptions            =  'class="form-control" data-placeholder="Select Month"';
        return form_dropdown('month', $config['month_name_id'], $month, $htmlOptions);
	}

	public static function dropdown_year($year = NULL, $range = 2, $options = array()){
		$ci =& get_instance();
		$now = Date('Y');
		if(!$year) $year = $now;
		$date = array();
		for($i=$range;$i>=0;$i--){
			$temp = ''.($now-$i).'';
			$data[$temp] = $temp;
		}
		$data[$now] = $now;
		for($i=1;$i<=$range;$i++){
			$temp = ''.($now+$i).'';
			$data[$temp] = $temp;
		}

		if($options['htmlOptions'])
			$htmlOptions = $options['htmlOptions'];
		else
			$htmlOptions            =  'id="list-year" class="form-control" data-placeholder="Select Year"';
		
        return form_dropdown('year', $data, $year, $htmlOptions);
	}

	public static function statusPoExpired($date, $itemtype){
		$ci =& get_instance();
		$status = $ci->config->item('status_po_expired');

		$now = Carbon::now();
		$date= Carbon::parse($date);

		$lama = $date->diffInDays($now);
		$expired = $status[$itemtype];

		$text = $date->diffForHumans($now);

		$beda = $expired-$lama;
		if($beda<=0)
			$return = '<span class="label label-sm label-danger">'.$text.'</span>';
		else
			$return = '<span class="label label-sm label-info">'.$text.'</span>';


		return $return;
	}

	public static function icon($icon){
		return '<i class="'.$icon.'"></i>';
	}

	public static function format_number($num){
		return '<span style="display:block;width:100%;text-align:right;" class="format-number">'.number_format($num, 2, '.', ',').'</span>';
	}

	public static function format_number2($num){
		return '<span style="display:block;width:100%;text-align:right;" >'.number_format($num, 2, '.', ',').'</span>';
	}

	public static function format_number_plaint($num){
		return '<span style="display:block;width:100%;text-align:right;" >'.number_format($num, 0, '.', ',').'</span>';
	}

	function getUnitBusiness($id){
    if($id == 1){
        return 'RMS';
    }elseif($id == 2){
        return 'IFS';
    }
    return '';
}



}


function btn_page_back(){
	return '<div class="div-page-action-left btn-page-back tooltips" data-original-title="Back">
				<a class="btn btn-xs"><i class="glyphicon glyphicon-chevron-left"></i> </a>
			</div>';
}

function getMonthName($month){
	return date::getMonthName($month);
}

function viewBreadcumb(){
	$ci =& get_instance();
	$ci->load->model('menu_model');
	$url = $ci->access_right->getMenu();
	$menu = $ci->menu_model->with('group_menu')->where('this.url', $url)->get()->row();

	return '
	<ul class="page-breadcrumb breadcrumb">
		<li>
			<a href="'.base_url().'">Home</a><i class="fa fa-circle"></i>
		</li>
		<li>
			<a href="#">'.$menu->nama_grup_menu.'</a>
			<i class="fa fa-circle"></i>
		</li>
		<li class="active">
			 '.$menu->nama_menu.'
		</li>
	</ul>';
}



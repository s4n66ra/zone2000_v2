<?php


class transaksi {

	public static $config;

	public function __construct() {        
		$ci =& get_instance();
		self::$config = $ci->config->item('transaksi');
    }

	public static function prNextStatus($stat){
		switch ($stat) {
			case 0:
				$stat = 1;
				break;
			
			default:
				# code...
				break;
		}
		return $stat;

	}

	public static function getServiceStatus($stat){
		$ci =& get_instance();
		$status = $ci->config->item('status_confirm');
		return $status[$stat];
	}

	public static function getPrStatus($stat){
		$ci =& get_instance();
		$status = $ci->config->item('status_pr');
		return $status[$stat];	
	}

	public static function getTransStatus($stat){
		$ci =& get_instance();
		$status = $ci->config->item('status_transaction');
		return $status[$stat];
	}
	
	public static function getPrType($stat){
		$ci =& get_instance();
		$status = $ci->config->item('type_item');
		return $status[$stat];	
	}

	public static function getItemType($stat){
		$ci =& get_instance();
		$status = $ci->config->item('type_item');
		return $status[$stat];	
	}

	public static function getBundleStatus($stat){
		$ci =& get_instance();
		$status = $ci->config->item('status_bundle');
		return $status[$stat];
	}

	public static function getPoStatus($stat){
		$ci =& get_instance();
		$status = $ci->config->item('status_po');
		return $status[$stat];	
	}	

	public static function getTransactionType($stat){
		$ci =& get_instance();
		return $status[$stat];	
	}	

	public static function getMerchandiseType(){
		$ci =& get_instance();
		$status = $ci->config->item('status_po');
		return $status[$stat];	
	}


}



function writeDebug($content) {
	//file_put_contents('holahola.txt', $content);
}
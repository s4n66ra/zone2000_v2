<?php
/*HELPER
 * Filename : buds_helper.php
 * Created on Sep 19, 2012
 */
function CurrDate($format)
{
    $CD = date($format);
    
    return $CD;
}

function DateFormat($str)
{
    if($str != null)
    {
        $date = new DateTime($str);
        return $date->format('d-M-Y H:i:s');
    }else{
        return "Unavaliable";
    }
}

function DateFormatNoTime($str)
{
    if($str != null)
    {
        $date = new DateTime($str);
        return $date->format('d-M-Y');
    }else{
        return "Unavaliable";
    }
}

function space_in_ajax_request($val)
{
    $request = str_replace("%20", " ", $val);
    
    return $request;;
}

function url_base64_encode(&$str="")
{
    return strtr(
            base64_encode($str),
            array(
                '+' => '.',
                '=' => '-',
                '/' => '~'
            )
        );
}

function url_base64_decode(&$str="")
{
    return base64_decode(strtr(
            $str, 
            array(
                '.' => '+',
                '-' => '=',
                '~' => '/'
            )
        ));
}

function URICheckNumeric($var)
{
    //Validations URI
    if(is_numeric($var))
    {
        return TRUE;
    }else{
        return FALSE;
    }
}

function AttributePopup($class="",$title="")
{
    $atts = array(
      'width'      => '800',
      'height'     => '600',
      'scrollbars' => 'yes',
      'status'     => 'yes',
      'resizable'  => 'no',
      'screenx'    => '0',
      'screeny'    => '0',
      'title' => $title,
      'class' => $class
    );
    
    return $atts;
}

function LabelSyariah($st_syariah)
{
    $string = "";
    if(($st_syariah!='')OR($st_syariah!=null)OR($st_syariah!=0))
    {
        $string = "<sup><blink>syariah<blink><sup>";
    }
    return $string;
}

function switch_tgl($res_date) 
{
    $duar   = explode('-',$res_date);
    if(is_numeric($duar[0]))
        $date   = $duar[2].'-'.$duar[1].'-'.$duar[0];
    else
        $date   = '-';

    return $date;
}

function getPosisi($pos)
{
    $waktu	= explode("-", $pos);
    $b 		= $waktu[0];
    $thn 	= $waktu[1];
    $m 		= array(
            'Jan'=>'1','Feb'=>'2','Mar'=>'3','Apr'=>'4','May'=>'5','Jun'=>'6','Jul'=>'7','Aug'=>'8','Sep'=>'9','Oct'=>'10','Nov'=>'11','Dec'=>'12'
        );
    $bulan	= $m[$b];

    $posisi[1]	= $thn;
    $posisi[2]	= $bulan;

    return $posisi;
}

function CheckPosisi($due)
{
     $waktu	= explode("-", $due);
     $b 		= $waktu[0];
     $m 		= array(
            'Jan'=>'1','Feb'=>'2','Mar'=>'3','Apr'=>'4','May'=>'5','Jun'=>'6','Jul'=>'7','Aug'=>'8','Sep'=>'9','Oct'=>'10','Nov'=>'11','Dec'=>'12'
        );
     
     if(array_key_exists($b,$m))
     {
         return TRUE;
     }else{
         return FALSE;
     }
}

function GetRangeDirections($var='')
{
    $data = array(
        0 => 'Up',
        1 => 'Down'
    );
    if($var != '')
    {
        return $data[$var];
    }else{
        return $data;
    }
}

function GetFx($id='')
{
    $func= array(
        '0' => '',
        '1' => '[Bulan Lalu]',
        '2' => '[Max]',
        '3' => '[Min]'
    );
    
    if($id=='')
    {
        return $func;
    }else{
        return $func[$id];
    }
}

function GetOperasi($id='')
{
    $operasi= array(
        '0' => null,
        '1' => '+',
        '2' => '-',
        '3' => '*',
        '4' => '/'
    );
    if($id=='')
    {
         return $operasi;
    }else{
        return $operasi[$id];
    }
}

function GetOperasiLabel($id)
{
    $operasi= array(
        '0' => null,
        '1' => ' + ',
        '2' => ' - ',
        '3' => ' X ',
        '4' => '<hr width=250px>'
    );
    
    return $operasi[$id];
}

function CheckRangeKonsolidasi($var)
{
    $range = array(1,2);
    if(array_key_exists($var,$range))
    {
        return TRUE;
    }else{
        return FALSE;
    }
}

function ajax_pagination($pagi,$table,$js,$nr='')
{
    $data   = '';
    $data   .= '<p id="pagination">' . $pagi . '</p>';
    $data   .= $table;
    if($nr!='')
        $data   .= '<span id="num_row">Total Rows: ' . $nr . '</span>';
    $data   .= '<p id="pagination">' . $pagi . '</p>';
    $data   .= '<script type="text/javascript" src="'.$js.'"></script>';

    return $data;
}

function FormatInteger($val)
{
    return number_format($val,0,'.','');
}

function FormatIntegerDecimal($val,$digit = 0)
{
    return number_format($val,$digit,'.','');
}

function FormatNoDecimal($val)
{
    return number_format($val,0,',','.');
}

function FormatPercentage($val)
{
    if(is_numeric($val))
    {
        return number_format($val,2,',','.');
    }else{
        return null;
    }
}

function GetAdjustmentWho($val)
{
    $data = array(
        1 => 'User',
        2 =>'Pinca',
        3 => 'SKMR',
        4 => 'SKKMR'
    );
    return $data[$val];
}

function GetWarnaPredikat($val,$warna, $class='')
{
    if(is_numeric($val))
    {
         if($val != 3)
        {
            $warna = "style='background: $warna' class='white title-tooltip $class'";
        }else{
            $warna = "style='background: $warna' class='title-tooltip $class'";
        }
    }else{
        $warna ="class='title-tooltip $class'";
    }
    return $warna;
}

function BlinkPredikat($val,$string)
{
    if($val >4)
    {
        $str = "<blink>$string</blink>";
    }else{
        $str = $string;
    }
    
    return $str;
}

function IsPusat($loker_id)
{
    if($loker_id == '77')
    {
        return TRUE;
    }else{
        return FALSE;
    }
}
function NowDue($now)
    {
        $pos = getPosisi($now);
        return $date = date('M-Y',mktime(0,0,0,($pos[2]-1)));
    }
    
    function getPusat()
{
    return '77';
}

function print_link($tipe,$uri, $title = 'Print', $id = '')
{
	$options	= array();
	if(is_array($tipe)){
		if (in_array('pdf', $tipe))
			$options['pdf']	= 'PDF';
		if (in_array('doc', $tipe))
			$options['doc']	= 'Word';
		if (in_array('xls', $tipe))
			$options['xls']	= 'Excel';
	}
	
	$js = 'id="print_tipe' . $id . '" style="width:70px;"';
	$form	= "<span class='print_option noPrint'>";
	$form	.=  form_dropdown('print_tipe', $options, '', $js);
	$form	.= "<a style='cursor:pointer;' onclick='multi_print(\"".base_url()."\",$(\"#print_tipe{$id}\").val(),\"{$uri}\")'>{$title}</a></span>";
	
	return $form;
}

function to_xls($page,$file_name)
{
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename={$file_name}.xls");
	header("Pragma: no-cache");
	header("Expires: 0");
	echo $page;
}
?>
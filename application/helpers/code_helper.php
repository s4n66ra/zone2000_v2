<?php 

class code {

	public static function getCode($type, $param1 = NULL, $param2=NULL, $offset=0) {
		$ci =& get_instance();
		$ci->load->model(array('sequence_model','store_model'));
		
		$sequenceModel 	= $ci->sequence_model;
		$storeModel 	= $ci->store_model;

		switch ($type) {
			case 'machine_id_kategori' :
				$ci->load->model('kategori_mesin_model');
				$kategoriMesin = $ci->kategori_mesin_model;
				$code = $kategoriMesin->getById($param1)->row()->current_id;
				$code++;
				return $code;

			case 'machine_id_outlet':
				//param1 = jenis_mesin;
				$ci->load->model('jenis_mesin_model');
				$jenisMesin = $ci->jenis_mesin_model;
				$code = $jenisMesin->getById($param1)->row()->kd_jenis_mesin;
				return $code;
			case 'pr':
				// $param1 = store_id
				$store 	= $storeModel->with('owner')->getById($param1)->row();
				$seq 	= $sequenceModel->getSequence($type);
				$code 	= 	$store->kd_cabang .'.'.
							date('Y') .'.'.
							$store->kd_owner .'.'.
							self::fillSequence($seq);
				return $code;
				break;
			
			case 'po':
				// $param1 = store_id - $param2 = supplier_id
				$ci->load->model('supplier_model');
				$store 	= $storeModel->with('owner')->getById($param1)->row();
				$seq 	= $sequenceModel->getSequence($type)+$offset;
				$sup 	= $ci->supplier_model->getById($param2)->row();
				$code 	= 	$store->kd_cabang .'.'.
							$sup->kd_supplier .'.'.
							date('Y') .'.'.
							date('m') .'.'.
							self::fillSequence($seq);
				return $code;
				break;
			
			case 'koli':
				// $param1 = postore_id
				$ci->load->model('po_store_model');
				$store 	= $ci->po_store_model->with(array('store','owner'))->getById($param1)->row();
				
				$seq 	= $sequenceModel->getSequence($type);
				$code 	= 	$store->kd_cabang .'.'.
							date('Y') .'.'.
							$store->kd_owner .'.'.
							self::fillSequence($seq);
				return $code;
				break;

			case 'koli_ticket':
				// $param1 = postore_id
				$ci->load->model('po_store_model');
				$store 	= $ci->po_store_model->with(array('store','owner'))->getById($param1)->row();
				
				$seq 	= $sequenceModel->getSequence($type);
				$code 	= 	'GD01.'.
							date('Y') .'.'.
							$store->kd_owner .'.'.
							self::fillSequence($seq);
				return $code;
				break;

			case 'do':
				// $param1 = postore_id
				$ci->load->model('store_model');
				$store 	= $ci->store_model->with(array('owner'))->getById($param1)->row();
				
				$seq 	= $sequenceModel->getSequence($type);
				$code 	= 	$store->kd_cabang .'.'.
							date('Y') .'.'.
							$store->kd_owner .'.'.
							self::fillSequence($seq);
				return $code;
				break;

			case 'sp':
				// $param1 = postore_id
				$ci->load->model('po_store_model');
				$store 	= $ci->po_store_model->with(array('store','owner'))->getById($param1)->row();
				
				$seq 	= $sequenceModel->getSequence($type);
				$code 	= 	$store->kd_cabang .'.'.
							date('Y') .'.'.
							$store->kd_owner .'.'.
							self::fillSequence($seq);
				return $code;
				break;

			case 'sr': //--------- service request---------
				// $param1 = store_id
				$store 	= $storeModel->with('owner')->getById($param1)->row();
				$seq 	= $sequenceModel->getSequence($type);
				$code 	= 	'SR.'.$store->kd_cabang .'.'.
							date('Y') .'.'.
							$store->kd_owner .'.'.
							self::fillSequence($seq);
				return $code;
				break;

			case 'sku_md':
				$seq 	= $sequenceModel->getSequence($type);
				$lenght = strlen($seq);
				$add_zero = '0';
				$code = '';
				if($lenght<8){
					$min_lenght = 8-$lenght;
					for($i=1;$i<=$min_lenght;$i++){
						$code .= $add_zero;
					}
				}
				$code .= $seq;
				return $code;

			default:
				# code...
				break;
		}
	}

	public static function getCodeAndRaise($type, $param1 = NULL, $param2=NULL, $offset=0){
		$ci =& get_instance();
		$ci->load->model(array('sequence_model','store_model','kategori_mesin_model'));

		$code = self::getCode($type, $param1, $param2, $offset);
		if($type == 'machine_id_kategori'){
			$ci->kategori_mesin_model->raiseSequence($param1);
		}else{
			$ci->sequence_model->raiseSequence($type);
		}

		return $code;
	}

	public static function fillSequence($seq){
		return str_pad($seq, 6, "0", STR_PAD_LEFT);
	}

}
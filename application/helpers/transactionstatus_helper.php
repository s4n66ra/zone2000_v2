<?php
	
class transactionstatus {
	const DRAFT = 0;
	const WAITING_REVIEW = 1;
	const WAITING_CONFIRM = 2;
	const REVISI = 3;
	const REJECT = 4;
	const APPROVE = 5;
	const WAREHOUSE = 6;
	const SHIP = 7;
	const RECEIVE = 8;
	const CANCEL = 9;
	const SC = 10;
}
<?php
class mutasistatus {
	const OPEN = 0;
	const REQUEST = 1;
	const APPROVE = 2;
	const SHIP = 3;
	const CLOSE = 4;
	const CANCEL = 5;
	const REJECT = 6;

	
	public static function getStatus($id){
		switch ($id) {
			case 1:
				$stat = 'Waiting For Review';
				break;
			case 2:
				$stat = 'APPROVE';
				break;
			case 3:
				$stat = 'SHIP';
				break;
			case 4:
				$stat = 'CLOSE';
				break;
			case 5:
				$stat = 'CANCEL';
				break;
			case 6:
				$stat = 'REJECT';
				break;
			default:
				# code...
				break;
		}
		return $stat;

	}
}


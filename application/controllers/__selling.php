<?php

class selling extends My_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array(
            'shipping_model','shipping_koli_model','koli_model', 'item_stok_model', 'item_model','itemtype_model', 'transaction_model', 'machine_transaction_model', 'mesin_model'
        ));
        $this->load->library('Datatable');
    }

    public function index($hist_option='', $getitemonly=false) {
        if($getitemonly==true){
            $this->getItemsByIdAndKeywordJSON(2);
        }else{
            if($hist_option=='history'){
                $this->getForm('hist-form-mc-redemp');
            }else{
                $data['title'] = 'Penjualaan Langsung dan Promosi';
                $data['sidebar'] = $this->access_right->menu();
                $data['content'] = 'base/page_content.tpl';
                //$data['javascript'] = array('selling.js');           
                $data['table']['main'] = $this->getForm('main_redeem');        
                $this->twig->display('index.tpl', $data);
            }            
        }
    }

    public function kuras_koin($hist_option='') {
        if($hist_option=='history'){
            $this->getForm('hist-form-coin-kuras');
        }else{            

            $data['title'] = 'Kuras Koin';
            $data['sidebar'] = $this->access_right->menu();
            $data['content'] = 'base/page_content.tpl';
            $data['javascript'] = array('machine-transaction.js');            
            $data['table']['main'] = $this->getForm('main_kuras_koin');
            $this->twig->display('index.tpl', $data);
        }
    }

    public function redeem($hist_option='', $getitemonly=false) {
        if($getitemonly==true){
            $this->getItemsByIdAndKeywordJSON(2);
        }else{
            if($hist_option=='history'){
                $this->getForm('hist-form-mc-redemp');
            }else{
                $data['title'] = 'Ticket Redeem Member';
                $data['sidebar'] = $this->access_right->menu();
                $data['content'] = 'base/page_content.tpl';
                $data['javascript'] = array('machine-transaction-redemp.js');           
                $data['table']['main'] = $this->getForm('main_redeem');        
                $this->twig->display('index.tpl', $data);
            }            
        }

    }

    public function redeem_regular($hist_option='', $getitemonly=false) {
        if($getitemonly==true){
            $this->getItemsByIdAndKeywordJSON(2);
        }else{
            if($hist_option=='history'){
                $this->getForm('hist-form-mc-redemp-regular');
            }else{
                $data['title'] = 'Ticket Redeem Regular';
                $data['sidebar'] = $this->access_right->menu();
                $data['content'] = 'base/page_content.tpl';
                $data['javascript'] = array('machine-transaction-redemp.js');           
                $data['table']['main'] = $this->getForm('main_redeem_regular');        
                $this->twig->display('index.tpl', $data);
            }            
        }

    }

    public function vending($hist_option='') {
        if($hist_option=='history'){
            $this->getForm('hist-form-mc-refill');        
        }else{
            $data['title'] = 'Machine Transactions Reports';
            $data['sidebar'] = $this->access_right->menu();
            $data['content'] = 'base/page_content.tpl';
            $data['javascript'] = array('machine-transaction-redemp.js');
            $data['table']['main'] = $this->getForm('main_vending');
            $this->twig->display('index.tpl', $data);            

        }
    }

    public function ticket_refill($hist_option='') {
        if($hist_option=='history'){
            $this->getForm('hist-form-ticket-refill');
        }else{
            $data['title'] = 'Machine Transactions Reports';
            $data['sidebar'] = $this->access_right->menu();
            $data['content'] = 'base/page_content.tpl';
            $data['javascript'] = array('machine-transaction.js');        
            $data['table']['main'] = $this->getForm('main_ticket_refill');
            $this->twig->display('index.tpl', $data);
        }
    }

    public function cutter($hist_option='') {
        if($hist_option=='history'){
            $this->getForm('hist-form-cutter');
        }else{
            $data['title'] = 'Machine Transactions Reports';
            $data['sidebar'] = $this->access_right->menu();
            $data['content'] = 'base/page_content.tpl';
            $data['javascript'] = array('machine-transaction.js');        
            $data['table']['main'] = $this->getForm('main_cutter');
            $this->twig->display('index.tpl', $data);
        }
    }

    public function getItemsByIdAndKeywordJSON($itemtype_id=''){
        $this->load->model(array('item_model'));

        $tmp = $this->item_model->getItemsByIdAndKeyword($itemtype_id);
        
        //$tmp = $this->item_model->getItemsByIdAndKeyword(2,'');
        $arr = array();
        foreach ($tmp as $k => $v) {
            if($k != ''){
                $obj = new StdClass();
                $obj->id        = $k;
                $obj->text      = $tmp[$k];
                array_push($arr, $obj);
            }
        }
        echo json_encode($arr);
    }

    

    public function getForm($type = 'add', $id=NULL, $param = array()){
        //echo $type;
        $this->load->library('custom_form');
        $this->load->library('custom_table');
        $this->load->library('table_data');
        $this->load->library('table_data_refill');
        $this->load->library('table_data_cutter');
        $this->load->model(array('store_data_model'));

        $storedata = $this->store_data_model;
        $mesinModel = $this->mesin_model;

        $form       = new custom_form();
        $table      = new custom_table();
        $tableData  = new table_data();
        $tableDataCutter = new table_data_cutter();
        $tableDataRefill = new table_data_refill();

        switch ($type) {
            case 'main' :
                $param['page']['coin_kuras'] = $this->getForm('page-coin-kuras');
                $param['page']['hist_coin_kuras'] = $this->getForm('hist-page-coin-kuras');
                
                $param['page']['ticket_refill']  = $this->getForm('page-ticket-refill');
                //$param['page']['coin_meter']  = $this->getForm('page-coin-meter');
                
                $param['page']['ticket_meter'] = $this->getForm('page-ticket-meter');
                $param['page']['ticket_refill'] = $this->getForm('page-ticket-refill');
                $param['page']['mc_refill'] = $this->getForm('page-mc-refill');
                $param['page']['mc_redemp'] = $this->getForm('page-mc-redemp');
                return $this->twig->render($this->class_name.'/page_main.tpl', $param);
                break;

            case 'main_kuras_koin' :
                $param['page']['coin_kuras'] = $this->getForm('page-coin-kuras');
                $param['page']['hist_coin_kuras'] = $this->getForm('hist-page-coin-kuras');
                return $this->twig->render($this->class_name.'/page_main.tpl', $param);
                break;

            case 'main_redeem' :
                $param['page']['mc_redemp'] = $this->getForm('page-mc-redemp');
                $param['page']['hist_mc_redemp'] = $this->getForm('hist-page-mc-redemp');
                return $this->twig->render($this->class_name.'/page_main.tpl', $param);
                break;

            case 'main_redeem_regular' :
                $param['page']['mc_redemp'] = $this->getForm('page-mc-redemp-regular');
                $param['page']['hist_mc_redemp'] = $this->getForm('hist-page-mc-redemp-regular');
                return $this->twig->render($this->class_name.'/page_main.tpl', $param);
                break;

            case 'main_vending' :
                $param['page']['mc_refill'] = $this->getForm('page-mc-refill');
                $param['page']['hist_mc_refill'] = $this->getForm('hist-page-mc-refill');
                return $this->twig->render($this->class_name.'/page_main.tpl', $param);
                break;

            case 'main_ticket_refill' :
                $param['page']['ticket_refill']  = $this->getForm('page-ticket-refill');
                $param['page']['hist_ticket_refill'] = $this->getForm('hist-page-ticket-refill');
                return $this->twig->render($this->class_name.'/page_main.tpl', $param);
                break;

            case 'main_cutter' :
                $param['page']['cutter']  = $this->getForm('page-cutter');
                $param['page']['hist_cutter'] = $this->getForm('hist-page-cutter');
                return $this->twig->render($this->class_name.'/page_main.tpl', $param);
                break;

            case 'page-coin-meter' :
                $param['portlet'] = false;
                $param['table']['main']     = $this->getForm('form-coin-meter');
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'page-cutter' :
                $param['portlet'] = false;
                $param['table']['main']     = $this->getForm('form-cutter');
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'hist-page-cutter' :
                $param['portlet'] = false;
                $param['table']['main']  = $this->getForm('hist-form-cutter', '', array('wrapper'));
                $param['javascript'] = array('receiving.js');
                
                return $this->twig->render('base/page_content.tpl', $param);
                break;


            case 'form-cutter' :
                $table->columnWidth = array(30,30,30,10);
                $table->columns     = array('TICKET METER', '');
                $table->id          = 'table-ticket-cutter';
                $table->numbering   = false;
                $tableDataCutter->table   = $table;
                $tableDataCutter->param   = array(
                                        'type_id' => transactiontype::TCUT,
                                    );
                return $tableDataCutter->generate();
                break;

            case 'hist-form-cutter' :
                $table = new Datatable();

                $data = $this->mesin_model->options_filter();                
                if (in_array('wrapper', $param)) {
                    $table->numbering  = true;
                    $table->id         = 'table-hist-ticket_cutter';
                    $table->isScrollable= false;


                    $filter = array(
                        array('id_mesin', 'Machine ID', 'list', $data),
                        array('nama_jenis_mesin', 'Machine Type', 'text'),
                        array('num_meter', 'Number Meter', 'text'),
                        array('rec_created', 'Date', 'date'),
                        array('nama_pegawai', 'Employee', 'text'),
                        array('nama_cabang', 'Store Name', 'text'),

                    );


                    $table->dataFilter = $filter;


                    $table->header     = array('SUM', 'DATE', 'EMPLOYEE' ,'STORE');
                    $table->source      = site_url($this->class_name.'/cutter/history');
                    return $table->generateWrapper();

                } else {
                    $table
                        ->setModel($this->transaction_model)
                        ->setNumbering()
                        ->with(array('transaction_machine','transaction_store','machine','cabang','pegawai', 'jenis_mesin'))
                        ->where(array('transactiontype_id' => 7))
                        ->select('num_meter, DATE(rec_created) as rec_created, nama_pegawai ,nama_cabang')
                        ->order_by('rec_created desc');
                        //->edit_column('rec_created', '$1', 'date::longDate(rec_created)');
                        //->edit_column('shipping_id', '$1', 'view::btn_group_receiving_warehouse(shipping_id)');
                    echo $table->generate();
                }                
                break;

            case 'page-ticket-refill' :
                $param['portlet'] = false;
                $param['table']['main']     = $this->getForm('form-ticket-refill');
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'hist-page-ticket-refill' :
                $param['portlet'] = false;
                //$param['table']['main']     = $this->getForm('hist-form-coin-kuras','',array('wrapper'));
                $param['table']['main']  = $this->getForm('hist-form-ticket-refill', '', array('wrapper'));
                $param['javascript'] = array('receiving.js');
                
                return $this->twig->render('base/page_content.tpl', $param);
                break;


            case 'form-ticket-refill' :
                $table->columnWidth = array(30,30,10,10,10,10);
                $table->columns     = array('MACHINE', 'INFO', 'MACHINE LAST TIKET NUMBER', 'START NUMBER','END NUMBER','');
                $table->id          = 'table-ticket-refill';
                $table->numbering   = false;
                $tableDataRefill->table   = $table;
                $tableDataRefill->param   = array(
                                        'type_id' => transactiontype::TFIL,
                                    );
                return $tableDataRefill->generate();
                break;

            case 'hist-form-ticket-refill' :
                $table = new Datatable();

                $data = $this->mesin_model->options_filter();                
                if (in_array('wrapper', $param)) {
                    $table->numbering  = true;
                    $table->id         = 'table-hist-ticket-refill';
                    $table->isScrollable= false;


                    $filter = array(
                        array('id_mesin', 'Machine ID', 'list', $data),
                        array('nama_jenis_mesin', 'Machine Type', 'text'),
                        array('num_meter', 'Number Meter', 'text'),
                        array('rec_created', 'Date', 'date'),
                        array('nama_pegawai', 'Employee', 'text'),
                        array('nama_cabang', 'Store Name', 'text'),

                    );


                    $table->dataFilter = $filter;


                    $table->header     = array('MACHINE CODE','MACHINE TYPE','SUM','LAST METER','NUM START','NUM END', 'DATE', 'EMPLOYEE' ,'STORE');
                    $table->source      = site_url($this->class_name.'/ticket_refill/history');
                    return $table->generateWrapper();

                } else {
                    $table
                        ->setModel($this->transaction_model)
                        ->setNumbering()
                        ->with(array('transaction_machine','transaction_store','machine','cabang','pegawai', 'jenis_mesin'))
                        ->where(array('transactiontype_id' => transactiontype::TFIL))
                        ->select('kd_mesin, nama_jenis_mesin, num_meter,num_ticket,num_start,num_end, DATE(rec_created) as rec_created, nama_pegawai ,nama_cabang')
                        ->order_by('zn_transaction.transaction_id desc');
                        //->edit_column('rec_created', '$1', 'date::longDate(rec_created)');
                        //->edit_column('shipping_id', '$1', 'view::btn_group_receiving_warehouse(shipping_id)');
                    echo $table->generate();
                }                
                break;

            

            case 'form-coin-meter' :
                $table->columnWidth = array(30,30,30,10);
                $table->columns     = array('MACHINE', 'INFO', 'COIN METER', '');
                $table->id          = 'table-coin-meter';
                $table->numbering   = false;
                $tableData->table   = $table;
                $tableData->param   = array(
                                        'type_id' => transactiontype::COIN,
                                    );
                return $tableData->generate();
                break;

            case 'page-coin-kuras' :
                $param['portlet'] = false;
                $param['table']['main']     = $this->getForm('form-coin-kuras');
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'hist-page-coin-kuras' :
                $param['portlet'] = false;
                //$param['table']['main']     = $this->getForm('hist-form-coin-kuras','',array('wrapper'));
                $param['table']['main']  = $this->getForm('hist-form-coin-kuras', '', array('wrapper'));
                $param['javascript'] = array('receiving.js');
                
                return $this->twig->render('base/page_content.tpl', $param);
                break;


            case 'form-coin-kuras' :
                $table->columnWidth = array(30,30,30,10);
                $table->columns     = array('MACHINE', 'INFO', 'COIN COUNT', '');
                $table->id          = 'table-coin-kuras';
                $table->numbering   = false;
                $tableData->table   = $table;
                $tableData->param   = array(
                                        'type_id' => transactiontype::COUT,
                                    );
                return $tableData->generate();
                break;

            case 'hist-form-coin-kuras' :
                $table = new Datatable();

                $data = $this->mesin_model->options_filter();                
                if (in_array('wrapper', $param)) {
                    $table->numbering  = true;
                    $table->id         = 'hist-table-coin-kuras';
                    $table->isScrollable= false;


                    $filter = array(
                        array('id_mesin', 'Machine Code', 'list', $data),
                        array('num_coin', 'Number Coin', 'text'),
                        array('rec_created', 'Date', 'date'),
                        array('nama_pegawai', 'Employee', 'text'),
                        array('nama_cabang', 'Store Name', 'text'),

                    );


                    $table->dataFilter = $filter;

                    $table->header     = array('MACHINE CODE','MACHINE TYPE','SUM', 'DATE', 'EMPLOYEE' ,'STORE');

                    $table->source      = site_url($this->class_name.'/kuras_koin/history');
                    return $table->generateWrapper();

                } else {
                    $table
                        ->setModel($this->transaction_model)
                        ->setNumbering()
                        ->with(array('transaction_machine','transaction_store','machine','cabang','pegawai', 'jenis_mesin'))
                        ->where(array('transactiontype_id' => 2))
                        ->select('kd_mesin, nama_jenis_mesin, num_coin, DATE(rec_created) as rec_created, nama_pegawai ,nama_cabang')
                        ->order_by('rec_created desc');
                        //->edit_column('rec_created', '$1', 'date::longDate(rec_created)');
                        //->edit_column('shipping_id', '$1', 'view::btn_group_receiving_warehouse(shipping_id)');
                    echo $table->generate();
                }
                break;


            case 'page-ticket-meter' :
                $param['portlet'] = false;
                $param['table']['main']     = $this->getForm('form-ticket-meter');
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'form-ticket-meter' :
                $table->columnWidth = array(30,30,30,10);
                $table->columns     = array('MACHINE', 'INFO', 'TICKET METER', '');
                $table->id          = 'table-ticket-meter';
                $table->numbering   = false;
                $tableData->table   = $table;
                $tableData->param   = array(
                                        'type_id' => transactiontype::TOUT,
                                    );
                return $tableData->generate();
                break;

            case 'page-ticket-refill' :
                $param['portlet'] = false;
                $param['table']['main']     = $this->getForm('form-ticket-refill');
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'form-ticket-refill' :
                $table->columnWidth = array(20,30,20,20,10);
                $table->columns     = array('MACHINE', 'INFO', 'START', 'END', '');
                $table->id          = 'table-ticket-refill';
                $table->numbering       = false;
                $tableData->num_input   = 2;
                $tableData->table       = $table;
                $tableData->param       = array(
                                            'type_id' => transactiontype::TFIL,
                                        );
                return $tableData->generate();
                break;

            case 'page-mc-refill' :
                $param['portlet'] = false;
                $param['table']['main']     = $this->getForm('form-mc-refill');
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'hist-page-mc-refill' :
                $param['portlet'] = false;
                //$param['table']['main']     = $this->getForm('hist-form-coin-kuras','',array('wrapper'));
                $param['table']['main']  = $this->getForm('hist-form-mc-refill', '', array('wrapper'));
                $param['javascript'] = array('receiving.js');
                
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'form-mc-refill' :
                $this->load->model(array('item_model','mesin_model'));
                $table->columnWidth = array(30,30,30,10);
                $table->columns     = array('MACHINE', 'INFO', 'ITEM QTY', '');
                $table->id          = 'table-mc-refill';
                $table->numbering       = false;
                $tableData->list_item   = $this->item_model->options(itemtype::ITEM_MERCHANDISE);
                $tableData->list_bundle = $this->mesin_model->options_vending();
                $tableData->table       = $table;
                $tableData->param       = array(
                                            'type_id' => transactiontype::MFIL,
                                        );
                return $tableData->generateRedemp();
                break;

            case 'hist-form-mc-refill' :
                $table = new Datatable();

                $data = $this->mesin_model->options_filter();                
                if (in_array('wrapper', $param)) {
                    $table->numbering  = true;
                    $table->id         = 'hist-table-mc-refill';
                    $table->isScrollable= false;


                    $filter = array(
                        array('id_mesin', 'Machine Code', 'list', $data),
                        array('nama_jenis_mesin', 'Machine Type', 'text'),
                        array('item_name', 'Item Name', 'text'),
                        array('item_qty', 'Item Quantity', 'text'),
                        array('rec_created', 'Date', 'date'),
                        array('nama_pegawai', 'Employee', 'text'),
                        array('nama_cabang', 'Store Name', 'text'),

                    );


                    $table->dataFilter = $filter;


                    $table->header      = array('MACHINE CODE','MACHINE TYPE','ITEM NAME', 'ITEM QTY','DATE', 'EMPLOYEE' ,'STORE');
                    $table->source      = site_url($this->class_name.'/vending/history');
                    return $table->generateWrapper();

                } else {
                    $table
                        ->setModel($this->transaction_model)
                        ->setNumbering()
                        ->with(array('transaction_machine','transaction_store','machine','cabang','pegawai', 'jenis_mesin', 'refill', 'item'))
                        ->where(array('transactiontype_id' => 6))
                        ->select('kd_mesin, nama_jenis_mesin, item_name, item_qty,DATE(zn_transaction.rec_created) as rec_created, nama_pegawai ,nama_cabang')
                        ->order_by('rec_created desc');
                        //->edit_column('rec_created', '$1', 'date::longDate(rec_created)');
                        //->edit_column('shipping_id', '$1', 'view::btn_group_receiving_warehouse(shipping_id)');
                    echo $table->generate();
                }
                break;

            case 'page-mc-redemp' :
                $param['portlet'] = false;
                $param['table']['main']     = $this->getForm('form-mc-redemp');
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'hist-page-mc-redemp' :
                $param['portlet'] = false;
                $param['table']['main']  = $this->getForm('hist-form-mc-redemp', '', array('wrapper'));
                $param['javascript'] = array('receiving.js');
                
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'page-mc-redemp-regular' :
                $param['portlet'] = false;
                $param['table']['main']     = $this->getForm('form-mc-redemp-regular');
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'hist-page-mc-redemp-regular' :
                $param['portlet'] = false;
                $param['table']['main']  = $this->getForm('hist-form-mc-redemp-regular', '', array('wrapper'));
                $param['javascript'] = array('receiving.js');
                
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'form-mc-redemp' :
                $this->load->model(array('item_model','mesin_model'));
                $table->columnWidth = array(30,30,30,10);
                $table->columns     = array('MERCHANDISE', 'INFO', 'ITEM QTY', '');
                $table->id          = 'table-mc-redemp';
                $table->numbering       = false;
                $tableData->list_item   = $this->item_model->options(itemtype::ITEM_MERCHANDISE);
                //$tableData->list_bundle = $this->mesin_model->options2();
                $tableData->table       = $table;
                $tableData->param       = array(
                                            'type_id' => transactiontype::MRDM,
                                        );
                return $tableData->generateRedemp();
                break;

            case 'hist-form-mc-redemp' :
                $table = new Datatable();

                $data = $this->mesin_model->options_filter();                
                if (in_array('wrapper', $param)) {
                    $table->numbering  = true;
                    $table->id         = 'table-ticket-redemp';
                    $table->isScrollable= false;


                    $filter = array(
                        array('num_coin', 'Number Coin', 'text'),
                        array('rec_created', 'Date', 'date'),
                        array('nama_pegawai', 'Employee', 'text'),
                        array('nama_cabang', 'Store Name', 'text'),

                    );


                    $table->dataFilter = $filter;


                    $table->header      = array('ITEM NAME', 'ITEM QTY','TICKET COUNT','DATE', 'EMPLOYEE' ,'STORE');
                    $table->source      = site_url($this->class_name.'/redeem/history');
                    return $table->generateWrapper();

                } else {
                    $table
                        ->setModel($this->transaction_model)
                        ->setNumbering()
                        ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem'))
                        ->where(array('transactiontype_id' => transactiontype::MRDM))
                        ->select('item_name, item_qty,ticket_count,DATE(zn_transaction.rec_created) as rec_created, nama_pegawai ,nama_cabang')
                        ->order_by('rec_created desc');
                        //->edit_column('rec_created', '$1', 'date::longDate(rec_created)');
                        //->edit_column('shipping_id', '$1', 'view::btn_group_receiving_warehouse(shipping_id)');
                    echo $table->generate();
                }

                break;

            case 'form-mc-redemp-regular' :
                $this->load->model(array('item_model','mesin_model'));
                $table->columnWidth = array(30,30,30,10);
                $table->columns     = array('MERCHANDISE', 'INFO', 'ITEM QTY', '');
                $table->id          = 'table-mc-redemp';
                $table->numbering       = false;
                $tableData->list_item   = $this->item_model->options(itemtype::ITEM_MERCHANDISE);
                //$tableData->list_bundle = $this->mesin_model->options2();
                $tableData->table       = $table;
                $tableData->param       = array(
                                            'type_id' => transactiontype::MRDM_REG,
                                        );
                return $tableData->generateRedemp();
                break;

            case 'hist-form-mc-redemp-regular' :
                $table = new Datatable();

                $data = $this->mesin_model->options_filter();                
                if (in_array('wrapper', $param)) {
                    $table->numbering  = true;
                    $table->id         = 'table-ticket-redemp';
                    $table->isScrollable= false;


                    $filter = array(
                        array('num_coin', 'Number Coin', 'text'),
                        array('rec_created', 'Date', 'date'),
                        array('nama_pegawai', 'Employee', 'text'),
                        array('nama_cabang', 'Store Name', 'text'),

                    );


                    $table->dataFilter = $filter;


                    $table->header      = array('ITEM NAME', 'ITEM QTY','TICKET COUNT','DATE', 'EMPLOYEE' ,'STORE');
                    $table->source      = site_url($this->class_name.'/redeem_regular/history');
                    return $table->generateWrapper();

                } else {
                    $table
                        ->setModel($this->transaction_model)
                        ->setNumbering()
                        ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem'))
                        ->where(array('transactiontype_id' => transactiontype::MRDM_REG))
                        ->select('item_name, item_qty,ticket_count,DATE(zn_transaction.rec_created) as rec_created, nama_pegawai ,nama_cabang')
                        ->order_by('rec_created desc');
                        //->edit_column('rec_created', '$1', 'date::longDate(rec_created)');
                        //->edit_column('shipping_id', '$1', 'view::btn_group_receiving_warehouse(shipping_id)');
                    echo $table->generate();
                }

                break;



            case 'target' :
                $this->twig->add_function('getMonthName');
                $param['form_action'] = form_open(
                                            site_url($this->class_name.'/proses_target/'.$id),
                                            '',
                                            array(
                                                'id' => $id,
                                            ));
                // options untuk list year
                $options['htmlOptions'] = $htmlOptions            =  'id="list-year" target="#form-target-part" data-source="'.site_url($this->class_name.'/form_target/'.$id).'" onchange="refreshList(this)" class="form-control"';

                $param['form_part']     = $this->getForm('target-part', $id);
                $param['list']['year']  = view::dropdown_year($temp['year_month'], 2, $options);
                $form->param = $param;
                $form->url_form     = $this->class_name.'/form_target.tpl';
                return $form->generate();
                break;

            default:
                return '';
                break;
        }
    }


}

/* End of file bbm.php */
/* Location: ./application/controllers/bbm.php */

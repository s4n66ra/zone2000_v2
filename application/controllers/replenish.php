<?php

class replenish extends My_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array(
            'cabang_model','shipping_model','shipping_koli_model','koli_model', 'item_stok_model', 'item_model','itemtype_model', 'transaction_model', 'replenish_model', 'mesin_model'
        ));
    }

    public function index() {
        if($hist_option=='history'){
            $this->getForm('form-replenish');        
        }else{
            $data['title'] = 'Replenish Merchandise';
            $data['sidebar'] = $this->access_right->menu();
            $data['content'] = 'base/page_content.tpl';
            $data['st_date'] = 1;
            $data['target'] = base_url('replenish/insertToSession');
            $sistem_date = $this->session->userdata['sistem_date'];
            if(!empty($sistem_date)){
                $data['hid_date'] = $sistem_date;
                $data['style'] = "style=padding-top:6px;color:rgb(255,0,0);";
            }else{
                $data['hid_date'] = date('Y-m-d');
                $data['style'] = 'style="padding-top:6px; color: rgb(0, 0, 0);"';
            }
            
            //PAKAI JS YANG DI PAKAI JUGA DI REDEMP
            $data['javascript'] = array('machine-transaction-redemp.js');
            $data['table']['main'] = $this->getForm('main_replenish');
            $this->twig->display('index.tpl', $data);            

        }
    }

    public function history_data($hist_option = '') {
        if($hist_option=='history'){
            $this->getForm('hist-form-replenish');        
        }else{
            $data['title'] = 'Replenish Merchandise';
            $data['sidebar'] = $this->access_right->menu();
            $data['content'] = 'base/page_content.tpl';
            $data['javascript'] = array('machine-transaction.js');
            $data['table']['main'] = $this->getForm('main_replenish');
            $this->twig->display('index.tpl', $data);            

        }
    }

    public function getForm($type = 'add', $id=NULL, $param = array()){
        //echo $type;
        $this->load->library('custom_form');
        $this->load->library('custom_table');
        $this->load->library('table_data_replenish');
        $this->load->model(array('store_data_model'));

        $storedata = $this->store_data_model;
        $mesinModel = $this->mesin_model;

        $form       = new custom_form();
        $table      = new custom_table();
        $tableData  = new table_data_replenish();

        switch ($type) {

            case 'main_replenish' :
                $param['page']['mc_refill'] = $this->getForm('page-replenish');
                $param['page']['hist_mc_refill'] = $this->getForm('hist-page-replenish');
                return $this->twig->render($this->class_name.'/page_main.tpl', $param);
 
             case 'page-replenish' :
                $param['portlet'] = false;
                $param['table']['main']     = $this->getForm('form-replenish');
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'hist-page-replenish' :
                $param['portlet'] = false;
                //$param['table']['main']     = $this->getForm('hist-form-coin-kuras','',array('wrapper'));
                $param['table']['main']  = $this->getForm('hist-form-replenish', '', array('wrapper'));
                $param['javascript'] = array('receiving.js');
                
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'form-replenish' :
                $this->load->model(array('item_model','mesin_model'));
                $table->columnWidth = array(30,30,30,10);
                $table->columns     = array('MERCHANDISE', 'ITEM QTY', '');
                $table->id          = 'table-mc-refill';
                $table->numbering       = false;
                $tableData->list_item   = $this->item_model->options(itemtype::ITEM_MERCHANDISE);
                $tableData->list_bundle = $this->cabang_model->options();
                $tableData->table       = $table;
                $tableData->param       = array(
                                            'type_id' => transactiontype::MRPL,
                                        );
                return $tableData->generate();
                break;

            case 'hist-form-replenish' :
                $this->load->library('Datatable');
                $table = new Datatable();

                $data = $this->mesin_model->options_filter();                
                if (in_array('wrapper', $param)) {
                    $table->numbering  = true;
                    $table->id         = 'hist-table-mc-refill';
                    $table->isScrollable= false;


                    $filter = array(
                        array('id_mesin', 'Machine ID', 'list', $data),
                        //array('nama_jenis_mesin', 'Machine Type', 'text'),
                        array('item_name', 'Item Name', 'text'),
                        array('item_qty', 'Item Quantity', 'text'),
                        array('rec_created', 'Date', 'date'),
                        array('nama_pegawai', 'Employee', 'text'),
                        array('nama_cabang', 'Store Name', 'text'),

                    );


                    $table->dataFilter = $filter;


                    $table->header      = array('SKU','ITEM NAME', 'ITEM QTY','DATE', 'EMPLOYEE' ,'STORE');
                    $table->source      = site_url($this->class_name.'/history_data/history');
                    return $table->generateWrapper();

                } else {
                    $table
                        ->setModel($this->transaction_model)
                        ->setNumbering()
                        ->with(array('transaction_store','cabang','pegawai', 'replenish', 'item_replenish','hadiah'))
                        ->where(array('transactiontype_id' => transactiontype::MRPL))
                        ->select('m_hadiah.item_key,item_name, item_qty,DATE(zn_transaction.rec_created) as rec_created, nama_pegawai ,nama_cabang')
                        ->order_by('rec_created desc')
                        ->edit_column('rec_created', '$1', 'date::shortDate(rec_created)');
                        //->edit_column('shipping_id', '$1', 'view::btn_group_receiving_warehouse(shipping_id)');
                    echo $table->generate();
                }
                break;

            case 'target' :
                $this->twig->add_function('getMonthName');
                $param['form_action'] = form_open(
                                            site_url($this->class_name.'/proses_target/'.$id),
                                            '',
                                            array(
                                                'id' => $id,
                                            ));
                // options untuk list year
                $options['htmlOptions'] = $htmlOptions            =  'id="list-year" target="#form-target-part" data-source="'.site_url($this->class_name.'/form_target/'.$id).'" onchange="refreshList(this)" class="form-control"';

                $param['form_part']     = $this->getForm('target-part', $id);
                $param['list']['year']  = view::dropdown_year($temp['year_month'], 2, $options);
                $form->param = $param;
                $form->url_form     = $this->class_name.'/form_target.tpl';
                return $form->generate();
                break;

            default:
                return '';
                break;
        }
    }

    public function getItemsByIdAndKeywordJSON($itemtype_id=''){
        $this->load->model(array('item_model'));

        $tmp = $this->item_model->getItemsByIdAndKeyword($itemtype_id);
        
        //$tmp = $this->item_model->getItemsByIdAndKeyword(2,'');
        $arr = array();
        foreach ($tmp as $k => $v) {
            if($k != ''){
                $obj = new StdClass();
                $obj->id        = $k;
                $obj->text      = $tmp[$k];
                array_push($arr, $obj);
            }
        }
        echo json_encode($arr);
    }

    public function insertToSession($date=""){
        //$user_data = array_merge($temp_user_data,array("hari" => $date));
        $this->session->set_userdata('sistem_date',$date);
        $temp_user_data = $this->session->all_userdata();
        if($date != date('Y-m-d'))
        echo json_encode(1);
    }

}

/* End of file bbm.php */
/* Location: ./application/controllers/bbm.php */

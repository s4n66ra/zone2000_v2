<?php

/**
 * Description of user
 *
 * @author Warman Suganda
 */
class user extends CI_Controller {

    private $class_name;
    private $limit = 10;

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

        // Protection
        hprotection::login();
		
        if ($this->uri->segment(2) != 'change_password') {
            $this->access_right->check();
            $this->access_right->otoritas('view', true);
        }

        // Global Model
        $this->load->model($this->class_name . '_model');
    }

    public function index() {
        $data['title'] = 'Daftar User';
        $data['page'] = $this->class_name . '/index';
        $data['button_group'] = array();
        if ($this->access_right->otoritas('add')) {
            $data['button_group'][] = anchor(null, '<i class="icon-plus"></i> Tambah Data', array('id' => 'button-add', 'class' => 'btn yellow', 'onclick' => 'load_form(this.id)', 'data-source' => base_url($this->class_name . '/add')));
        }

        $base_url = base_url() . $this->class_name;
        $base_url_images = base_url() . 'images';

        $data['print_group'] = array();

        $this->access_right->activity_logs('view','Daftar User');

        $data['assets_url'] = $this->config->item('assets_url');
        $data['sidebar'] = $this->access_right->menu();
        $data['nama_pegawai'] = $this->currentUsername;
        $data['content'] = $this->class_name.'/table.tpl';
        $data['data_table'] = $this->load();        
        $data['add_button'] = '<a id="sample_editable_1_new" class="btn green" href="'.base_url($this->class_name.'/add').'">Add New <i class="fa fa-plus"></i></a>';
        $this->twig->display('index.tpl', $data);
    }

    public function load($page = 0) {
        $this->load->library("custom_table");

        $table = new stdClass();
        $header[0] = array("NPP", 1, 1, "Nama", 1, 1,  "Username", 1, 1, "Ket. Password", 1, 1, "Status Password", 1, 1, "Role", 1, 1, "Status", 1, 1, "Aksi", 1, 1);
        $table->header = $header;
        $table->align = array('nik' => 'center', 'username' => 'center', 'ket_password' => 'center', 'status_password' => 'center', 'status' => 'center', 'action' => 'center');
        $table->style = "table table-striped table-bordered table-hover datatable dataTable";
        $table->numbering = false;

        $table->model = $this->class_name . '_model->data_table';
        $table->limit = $this->limit;
        $table->page = $page;
        $data = $this->custom_table->generate($table);

        return $data;
    }

        public function add($id = '',$status_delete = 0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->config->load('twig');
            $this->load->model('roles_model');

            $title = 'Tambah Data Group Menu';
            $data['id'] = $id;
            if ($id != '') {
                $title = 'Edit Data Group Menu';

                $db = $this->menu_group_model->get_by_id($id);
                if ($db->num_rows() > 0) {
                    $row = $db->row();
                    $data['default'] = $row;
                    $data['def_nama_grup_menu'] = $row->nama_grup_menu;
                }
            }
            //echo $id;
            $data['title'] = $title;
            $data['assets_url'] = $this->config->item('assets_url');
            $data['sidebar'] = $this->access_right->menu();
            if($status_delete == 0){
                $data['form_action'] = '<form action="'.base_url($this->class_name.'/proses').'" method="post" id="form_sample_3" class="form-horizontal" novalidate="novalidate"><input name = "id" type="hidden" value="'.$id.'">';
                $data['button_submit'] = '<button type="submit" class="btn green">Submit</button>';
            }else{
                $data['title'] = 'Delete Data Pulau';
                $data['form_action'] = '<form action="'.base_url($this->class_name.'/proses_delete').'" method="post" id="form_sample_3" class="form-horizontal" novalidate="novalidate"><input name = "id" type="hidden" value="'.$id.'">';
                $data['button_submit'] = '<button type="submit" class="btn red">Delete</button>';
            }
            $data['button_submit'] .= '<a href = "'.base_url($this->class_name).'" class="btn default">Cancel</a>';
            $data['form'] = $this->class_name.'/form.tpl';
            $this->twig->display('form.tpl', $data);

        } else {
            $this->access_right->redirect();
        }
    }

    public function add_old($id = '') {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->load->model('roles_model');

            $title = 'Tambah Data';
            $data['form_action'] = $this->class_name . '/proses';
            $data['pegawai_source'] = base_url($this->class_name . '/search_pegawai');
            $data['role_source'] = base_url($this->class_name . '/roles_setting');

            $data['role_options'] = $this->roles_model->options();

            $data['id'] = $id;
            if ($id != '') {
                $title = 'Edit Data';

                $db = $this->user_model->get_by_id($id);
                if ($db->num_rows() > 0) {
                    $row = $db->row();
                    $data['default'] = $row;
                }
            }

            $data['title'] = '<i class="icon-edit"></i> ' . $title;

            $this->load->view($this->class_name . '/form', $data);
        } else {
            $this->access_right->redirect();
        }
    }

    public function edit($id) {
        $this->add($id);
    }

    public function proses() {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_message('matches', '%s tidak benar.');

            $this->form_validation->set_rules('nama', 'Nama', 'trim|required|matches[temp_nama]');
            $this->form_validation->set_rules('user_name', 'Username', 'required');
            $this->form_validation->set_rules('roles', 'Role', 'required');
            $this->form_validation->set_rules('status', 'Status', 'required');

            if ($this->form_validation->run()) {
                $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');

                $id = $this->input->post('id');

                $data = array(
                    'grup_id' => $this->input->post('roles'),
                    'nik' => $this->input->post('nik'),
                    'user_name' => $this->input->post('user_name'),
                    'status' => $this->input->post('status'),
                    'otoritas_menu_view' => $this->input->post('otoritas_menu_view'),
                    'otoritas_menu_add' => $this->input->post('otoritas_menu_add'),
                    'otoritas_menu_edit' => $this->input->post('otoritas_menu_edit'),
                    'otoritas_menu_delete' => $this->input->post('otoritas_menu_delete'),
                    'otoritas_menu_approve' => $this->input->post('otoritas_menu_approve'),
                    'otoritas_menu_export' => $this->input->post('otoritas_menu_export'),
                    'otoritas_menu_print' => $this->input->post('otoritas_menu_print')
                );

                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */

                if ($id == '') {
                    if ($this->user_model->create($data)) {
                        $insert_id = $this->session->userdata('insert_id');
						$this->access_right->activity_logs('add','Daftar User');
                        $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', '#content_table');
                    }
                } else {
                    if ($this->user_model->update($data, $id)) {
                        $insert_id = $id;
						$this->access_right->activity_logs('edit','Daftar User');
                        $message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', '#content_table');
                    }
                }
            } else {
                $message = array(false, 'Terjadi Kesalahan', validation_errors(), '');
            }
            echo json_encode($message);
        } else {
            $this->access_right->redirect();
        }
    }

    public function delete($id) {
        if ($this->access_right->otoritas('delete', true)) {
            $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');
            if ($this->user_model->delete($id)) {
				$this->access_right->activity_logs('delete','Daftar User');
                $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', '#content_table');
            }
            echo json_encode($message);
        }
    }

    public function search_pegawai() {
        $this->load->model('master_pegawai_model');

        $query = $this->input->get('query');

        $condition = array();
        $condition['OR']["LOWER(a.nik) LIKE " . " '%" . strtolower($query) . "%'"] = null;
        $condition['OR']["LOWER(a.nama) LIKE " . " '%" . strtolower($query) . "%'"] = null;

        $data = array();
        $source = $this->master_pegawai_model->get_data($condition);

        foreach ($source->result() as $value) {
            $data[] = array(
                'nik' => $value->nik,
                'nama' => $value->nik . ' - ' . $value->nama
            );
        }

        echo json_encode($data);
    }

    public function roles_setting() {
        $id = $this->input->post('id');
        $user_id = $this->input->post('user_id');

        $this->load->model('menu_group_model');
        $this->load->model('menu_model');
        $this->load->model('roles_model');

        $data['menu_group'] = $this->menu_group_model->get_data();
        $data['menu'] = $this->menu_model->parsing_data();

        $data['roles'] = $this->roles_model->parsing_roles(array('grup_id' => $id));
        $data['user_akses'] = $this->user_model->parsing_akses(array('userid' => $user_id));

        echo json_encode($this->load->view($this->class_name . '/roles', $data, true));
    }

    public function reset_password($id) {
        if ($this->access_right->otoritas('edit', true)) {
            $message = array(false, 'Proses gagal', 'Proses reset password gagal.', '');

            if ($this->user_model->reset_password($id)) {
                $message = array(true, 'Proses Berhasil', 'Proses reset password berhasil.', '#content_table');
            }
            echo json_encode($message);
        }
    }

    public function pdf() {
        if ($this->access_right->otoritas('print', true)) {
            $this->load->library("custom_table");
            $this->load->helper('hpdf');

            $pdf = new hpdf('A4-L');

            $pdf->judul('DAFTAR USER');

            $table = new stdClass();
            $header[0] = array("NPP", 1, 1, "Nama", 1, 1, "Lokasi Kerja", 1, 1, "Username", 1, 1, "Ket. Password", 1, 1, "Status Password", 1, 1, "Role", 1, 1, "Status", 1, 1);
            $table->header = $header;
            $table->align = array('nik' => 'center', 'username' => 'center', 'ket_password' => 'center', 'status_password' => 'center', 'status' => 'center');
            $table->style = "table";
            $table->numbering = false;
            $table->kolom = 9;

            $table->model = $this->class_name . '_model->data_table_report';
            $data = $this->custom_table->generate($table);

            $pdf->html($data['table']);
            $pdf->cetak();
        }
    }

    public function excel() {
        if ($this->access_right->otoritas('print', true)) {
            $this->load->helper('hexcel');
            $excel = new hexcel();

            // Defult Border
            $defaultBorder = array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => array('rgb' => '000000')
            );

            $excel->getDefaultStyle()->getFont()->setName('Arial');
            $excel->getDefaultStyle()->getFont()->setSize(8);
            $excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);

            // Set Active Sheet 1
            $excel->setActiveSheetIndex(0);
            $sheet = $excel->getActiveSheet();

            // Seting Halaman
            $excel->page_setup($sheet, PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4, PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
            $excel->margin($sheet, 0.3, 0.3, 0.5, 0.5, 0.5, 0.5);

            // Judul
            $sheet->setCellValue('A1', 'DAFTAR USER');
            $sheet->mergeCells('A1:H1');
            $sheet->getRowDimension(1)->setRowHeight(27);
            $sheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('A1')->applyFromArray(array('font' => array('bold' => true, 'size' => 11)));

            // Set Lebar Kolom
            $sheet->getColumnDimension('A')->setWidth(17);
            $sheet->getColumnDimension('B')->setWidth(35);
            $sheet->getColumnDimension('C')->setWidth(25);
            $sheet->getColumnDimension('D')->setWidth(17);
            $sheet->getColumnDimension('E')->setWidth(17);
            $sheet->getColumnDimension('F')->setWidth(17);
            $sheet->getColumnDimension('G')->setWidth(33);
            $sheet->getColumnDimension('H')->setWidth(14);

            // Set Header Table
            $sheet->setCellValue('A2', 'NPP');
            $sheet->setCellValue('B2', 'Nama');
            $sheet->setCellValue('C2', 'Lokasi Kerja');
            $sheet->setCellValue('D2', 'Username');
            $sheet->setCellValue('E2', 'Ket. Password');
            $sheet->setCellValue('F2', 'Status Password');
            $sheet->setCellValue('G2', 'Role');
            $sheet->setCellValue('H2', 'Status');
            $sheet->getStyle('A2:H2')->applyFromArray(array('font' => array('bold' => true), 'borders' => array('inside' => $defaultBorder, 'outline' => $defaultBorder)));
            $sheet->getStyle('A2:H2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(2, 2);

            // Set Content Table
            $data = $this->user_model->data_table_report();

            $start_row = 3;
            $idx = $start_row - 1;
            if ($data['total'] > 0) {
                foreach ($data['rows'] as $value) {
                    $idx++;
                    $sheet->setCellValueExplicit('A' . $idx, $value['nik'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValue('B' . $idx, $value['nama']);
                    $sheet->setCellValue('C' . $idx, $value['nama_cabang']);
                    $sheet->setCellValueExplicit('D' . $idx, $value['username'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValue('E' . $idx, $value['ket_password']);
                    $sheet->setCellValue('F' . $idx, $value['status_password']);
                    $sheet->setCellValue('G' . $idx, $value['grup_nama']);
                    $sheet->setCellValue('H' . $idx, $value['status']);
                }

                $sheet->getStyle('A' . $start_row . ':H' . $idx)->applyFromArray(array('borders' => array('inside' => $defaultBorder, 'outline' => $defaultBorder)));
                $sheet->getStyle('A' . $start_row . ':A' . $idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('D' . $start_row . ':F' . $idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('H' . $start_row . ':H' . $idx)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            } else {
                $sheet->setCellValue('A' . $start_row, 'Data tidak ditemukan');
                $sheet->mergeCells('A' . $start_row . ':H' . $start_row);
                $sheet->getStyle('A' . $start_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('A' . $start_row . ':H' . $start_row)->applyFromArray(array('borders' => array('inside' => $defaultBorder, 'outline' => $defaultBorder)));
            }

            // Proses Generate Excel
            hexcel::simpan('daftar_user', $excel, 'Excel2007');
        }
    }

    public function change_password($data='') {
        $this->load->model('user_model');
        $kd_roles = $this->session->userdata('kd_roles');
        $nik = $this->session->userdata('nik_session');
        $q = $this->db->query("select user_id from m_user where nik='$nik'");
        foreach ($q->result_array() as $row) {
            $userid = $row['user_id'];
        }
		
        $data['data_user'] = $this->user_model->data_user_mdl($userid);
		$data['title']="Ganti Username / Password";
        $data['page'] = 'user/edit_user';
        $this->load->view('template', $data);
    }


}

/* End of file user.php */
    /* Location: ./application/controllers/user.php */
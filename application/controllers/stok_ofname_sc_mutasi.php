<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class stok_ofname_sc_mutasi extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array(
        	'item_model','item_stok_model', 'stok_ofname_sc_mutasi_model','store_model',
        ));

        //$this->button['add'] = view::button_add(array('onclick'=>"btnLoadNextPage(this)"));

    }

    public function index() {

        $data['title'] = 'Store Master';

        /* INSERT LOG */
        $this->access_right->activity_logs('view',$data['title']);
        /* END OF INSERT LOG */
        $data['button_group'] = $this->getAvailableButtons();


        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'base/page_content.tpl';

        //$data['table']['main']  = $this->table_main(array('wrapper', 'filter'));
        $data['table']['main']  = $this->getForm('table-main', '', array('wrapper'));
            
        $this->twig->display('index.tpl', $data);
    }

    public function add($id='',$status_delete = 0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->config->load('twig');
            
            $title = 'Tambah Data';
            $data['form_action'] = $this->class_name . '/proses';

            $data['id'] = $id;
            if($id){
                $title = 'Edit Merchandise';
                $row            = $this->hadiah_model->getById($id)->row();
                $data['data']  = $row;
            }

            $data['title']      = $title;
            $data['assets_url'] = $this->config->item('assets_url');
            $data['sidebar']    = $this->access_right->menu();
            $data['form']       = $this->class_name.'/form.tpl';

            $button_group   = array();
            $button_group[] = view::button_back();
            
            if($status_delete == 0){
                $data['form_action']    = view::form_input($id);
                $button_group[]         = view::button_save();
            }else{
                $data['title']          = 'Delete Merchandise';
                $data['form_action']    = view::form_delete($id);;
                $data['readonly']       = 'readonly=""';
                $button_group[]         = view::button_delete_confirm();
            }

            $this->load->model(array(
                        'item_model',
                        'store_model',
                        'stok_ofname_sc_mutasi_model',
                    ));
            
            $row = $this->stok_ofname_sc_mutasi_model
                    ->with(array('store', 'item'))
                    ->get()
                    ->result_array();

            $data['data'] = $row[0];


            $htmlOptions            =  'id="list-store" class="form-control select2" data-placeholder="Select Store" disabled="disabled"';
            $data['list']['store']     = form_dropdown('id_cabang', $this->store_model->options_empty(), hprotection::getSC(), $htmlOptions);

            $data['id_cabang'] = hprotection::getSC();
            $data['button_group'] = view::render_button_group($button_group);
            
            $this->twig->display('base/page_form.tpl', $data);

        } else {
            $this->access_right->redirect();
        }
    }


    public function proses() {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('data[item_id]', 'Item Id', 'required|trim');
            $this->form_validation->set_rules('data[store_id]', 'Cabang', 'required|trim');

            if ($this->form_validation->run()) {

                $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');

                $id = $this->input->post('id');
                $data = $this->input->post('data');
                $cek_eksist = $this->item_stok_model
                    ->select('item_id')
                    ->where(array('store_id'=>$data['store_id'],'item_id'=>$data['item_id']))
                    ->get()->row();



                //print_r($data);
                $temp_stok_total=0;
                if(!empty($cek_eksist->item_id)){
                    $this->item_stok_model
                        ->set('stok_gudang_mutasi', $data['stok_gudang_mutasi'])
                        ->where(array('item_id'=>$data['item_id'], 'store_id'=>$data['store_id']))
                        ->update();
                }else{                                        
                    $this->item_stok_model->create(array('item_id'=>$data['item_id'], 'store_id' => $data['store_id'], 'stok_gudang' => $data['stok_gudang'], 'stok_mesin' => $data['stok_mesin'], 'stok_store' => $data['stok_store']));
                }
                $data['date'] = date('Y-m-d');
                $data['st_gudang_mutasi'] = 1;

                $this->stok_ofname_sc_mutasi_model->create($data);

                
                if($this->db->trans_status()){
                    $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh_filter()');
                }else{
                    $message = array(false, 'Database Error', $this->db->_error_message());
                }
                $this->db->trans_complete();
            } else {
                $message = array('success'=>false, 'message'=>validation_errors());
            }
            echo json_encode($message);
        } else {
            $this->access_right->redirect();
        }
    }



    public function warehouse() {

        $data['title']      = 'DAFTAR STOK WAREHOUSE';
        $data['sidebar']    = $this->access_right->menu();
        $data['content']    = 'base/page_content.tpl';     
        //$data['button_group']     = $this->getAvailableButtons();
        $data['table']['main']  = $this->getForm('table-main-warehouse', '', array('wrapper'));
        $data['javascript'] = array('receiving.js');
        
        $this->twig->display('index.tpl', $data);
    }

    public function edit($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('edit', true);
        $this->form('edit', $id);
    }

    public function form($type='add',  $id = NULL){
        $data['title'] = 'Stok Ofname Update';
        $data['content']['body'] = $this->getForm($type, $id);
        $data['isBtnPageBack']      = true;

        $this->twig->display('base/page_content.tpl', $data);
    }



    public function getForm($type = '', $id = NULL, $param = array()){
        $this->load->library(array('custom_form','custom_table'));
        $this->load->model(array('sequence_model','po_store_model','koli_model'));
        $form = $this->custom_form;

        switch ($type) {
            case 'add' :
                $data['act'] = 'add';
                $data['form']['main'] = $this->getForm('main', $id);
                //return $this->twig->render($this->class_name.'/page_form.tpl', $data);
                break;

            case 'edit':
                $data['button']['back'] = true;
                $data['form']['main']   = $this->getForm('main', $id);
                return $this->twig->render($this->class_name.'/page_form.tpl', $data);
                break;
            case 'page-detail':
                $param['title']     = 'RECEIVING DETAIL';
                $param['portlet']   = true;
                $param['button']['back'] = true;
                $param['form']['info']  = $this->getForm('form-info', $id);
                $param['table']['main'] = $this->getForm('page-koli', $id);
                return $this->twig->render('base/page_content.tpl', $param);
                break;
            
            case 'table-main':
                $this->load->library('Datatable');
                $table = new Datatable();

                if (in_array('wrapper', $param)) {
                    $this->load->model('itemtype_model');
                    $data = $this->itemtype_model->options_filter();
                    $table->innerFilter = array(
                        array('itemtype_id', 'Item Type', 'list', $data),
                        array('item_name', 'Item Name', 'text'),
                    );
                    $table->dataFilter = $table->innerFilter;

                    $table->id          = 'table-stok-ofname';
                    $table->isScrollable= false;
                    $table->numbering   = true;
                    $table->header      = array('ITEM', 'TYPE', 'STORE', 'STOK GUDANG MUTASI LAMA','STOK GUDANG MUTASI BARU');
                    $table->source      = site_url($this->class_name.'/getform/table-main/');
                    return $table->generateWrapper();
                } else {
                    $table
                        ->setModel($this->stok_ofname_sc_mutasi_model)
                        ->setNumbering()
                        ->with(array('store','item'))
                        ->select('item_name, itemtype_id, nama_cabang, old_stok_gudang_mutasi,stok_gudang_mutasi')
                        ->where('store_id',hprotection::getSC())
                        ->where('st_gudang_mutasi',1)
                        ->order_by('nama_cabang')
                        ->order_by('itemtype_id')
                        ->order_by('item_name')
                        ->edit_column('so_id', '$1', 'view::view::btn_group_edit_delete_page(so_id)')
                        ->edit_column('itemtype_id','$1','transaksi::getItemType(itemtype_id)');
                    echo $table->generate();
                }
                break;

            case 'table-main-warehouse':
                $this->load->library('Datatable');
                $table = new Datatable();

                if (in_array('wrapper', $param)) {
                    $this->load->model('itemtype_model');
                    $data = $this->itemtype_model->options_filter();
                    $innerFilter = array(
                        array('nama_cabang', 'Store', 'text'),
                        array('koli_code', 'Koli Code', 'text'),
                        array('note','Note','text'),
                        array('nama_supplier','Supplier','text')
                    );
                    $table->dataFilter = $innerFilter;

                    $table->id          = 'table-stok-warehouse';
                    $table->isScrollable= false;
                    $table->numbering   = true;
                    $table->header      = array('KOLI CODE','STORE', 'QTY', 'NOTE','SUPPLIER', 'STATUS', 'DATE RECEIVED', 'DATE SHIP', 'ACTION');
                    $table->source      = site_url($this->class_name.'/getform/table-main-warehouse/');
                    return $table->generateWrapper();
                } else {
                    $table
                        ->setModel($this->koli_model)
                        ->setNumbering()
                        ->with(array('store','supplier','shipping','sp','postore_koli','receiving'))
                        ->select('koli_code, nama_cabang, sum(qty) as qty, note,nama_supplier,koli_status,  receiving.date_created as da,sp.date_created as dc, koli_code as kc')
                        ->order_by('koli_code')                        
                        //->where('koli_status',prstatus::WAREHOUSE)
                        /*->order_by('itemtype_id')
                        ->order_by('item_name')*/
                        ->group_by('koli_code')
                        ->edit_column('koli_status','$1','prstatus::getStatus(koli_status)')
                        ->edit_column('kc', '$1', "view::button_view_stok(kc)");
                   echo $table->generate();
                }
                break;
            
            case 'table-stok-detail':

                $this->load->model('receiving_postore_model');
                $model = $this->receiving_postore_model;
                $code = !empty($code) ? $code : 0;
                
                $result = $model->with(array('receiving_po_koli','po_detail','item','koli','shipping_koli','shipping','supplier','store'))
                    //nama_cabang = destination
                    //shipping_code dijadikan global
                    ->select('koli_code, item_name, po_detail.qty_approved as qty, price, (po_detail.qty_approved*price) as sub_total, nama_cabang')
                    //->select('*')
                    ->where(array('koli_code' => $id))
                    ->get()
                    ->result_array();

                $table = new custom_table();

                $table->columns = array('DATE', 'STATUS','QTY APPROVED','PRICE','SUB TOTAL','STORE NAME');
                $table->id      = 'table-history';
                $table->data_source = site_url($this->class_name.'/getform/table-stok-detail/'.$id);
                $table->data    = $result;

                $content = $table->generateWithWrapper();

                $param['list']['content']=$content;
                $param['list']['koli_code']=$id;
                $data = $param;
                $this->twig->display($this->class_name.'/page_detail.tpl', $data);
                break;

            case 'main' :
            
                $this->load->model(array(
                            'item_model',
                            'store_model',
                            'stok_ofname_sc_mutasi_model',
                        ));
                
                $row = $this->stok_ofname_sc_mutasi_model
                        ->with(array('store', 'item'))
                        ->get()
                        ->result_array();

                $htmlOptions            =  'id="list-store" class="form-control select2" data-placeholder="Select Store" disabled="disabled"';
                $data['list']['store']     = form_dropdown('data[id_cabang]', $this->store_model->options_empty(), $this->session->userdata("store_id"), $htmlOptions);


                if($st_delete == 1){
                    $data['form_action']    = view::form_delete($id);
                    $button_group[]         = view::button_delete_confirm();
                    $data['button_group'] = view::render_button_group($button_group);
                }else{
                    $data['form_action']    = view::form_input($id);
                }
                $data['data']       = $row[0];
                $form->param        = $data;
                $form->url_form     = $this->class_name.'/form.tpl';
    
                $data['button_group'] = view::render_button_group($button_group);

                $this->twig->display('base/page_form.tpl', $data);

                //return $form->generate();
                break;


            default:
                # code...
                break;


        }
    }

    public function getDataByItemIdAndStoreId($item_id=NULL, $store_id=NULL){
        $row = $this->item_stok_model
            ->select('stok_total, stok_gudang, stok_store, stok_mesin')
            ->where(array('item_id'=>$item_id, 'store_id'=>$store_id))
            ->get()
            ->row();

        echo json_encode($row);
    }


}
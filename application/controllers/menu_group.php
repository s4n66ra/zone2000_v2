<?php

/**
 * Description of menu_group
 *
 * @author Warman Suganda
 */
class menu_group extends MY_Controller {

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);
    }

    public function test(){
        $this->menu_group_model->delete(0);
    }

    public function index() {
        $data['title'] = 'Grup Menu';
        $data['page'] = $this->class_name . '/index';

        $button_group    = array(
                                view::button_add()
                            );

        $data['button_group'] = view::render_button_group($button_group);
        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'base/page_content.tpl';
        $data['table']['main'] = $this->table_main(array('wrapper'));
        $data['nama_pegawai'] = $this->currentUsername;  
        $this->twig->display('index.tpl', $data);
    }

    public function table_main($option = array()){
        $this->load->library('Datatable');
        $table = $this->datatable;

        if (in_array('wrapper', $option)) {
            $table->numbering  = false;
            $table->rowReordering = true;
            $table->isScrollable = false;
            $table->url_action_reordering = site_url($this->class_name.'/proses_reordering');
            $table->id         = 'table-grup-menu';
            $table->header     = array('ICON', 'MENU GROUP NAME', 'ACTION');
            $table->source     = site_url($this->class_name.'/table_main');
            return $table->generateWrapper();
        } else {
            $table
                ->setModel($this->menu_group_model)
                ->select('icon, nama_grup_menu, kd_grup_menu')
                ->order_by('urutan')
                ->edit_column('icon', '$1', 'view::icon(icon)')
                ->edit_column('kd_grup_menu', '$1', 'view::btn_group_edit_delete(kd_grup_menu)');
            echo $table->generate();
        }

    }

    public function add($id = '',$status_delete = 0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->load->library('custom_form');
            $form = $this->custom_form;

            $this->load->model('menu_group_model');

            $title = 'Tambah Data Group Menu';
            $data['id'] = $id;
            if ($id != '') {
                $title = 'Edit Data Group Menu';

                $db = $this->menu_group_model->get_by_id($id);
                if ($db->num_rows() > 0) {
                    $row = $db->row();
                    $data['data'] = $row;
                }
            }
            //echo $id;
            $data['title'] = $title;

            if($status_delete == 0){
                $data['form_action'] = view::form_input($id);
            }else{
                $data['form_action'] = '<form action="'.base_url($this->class_name.'/proses_delete').'" method="post" id="form_sample_3" class="form-horizontal" novalidate="novalidate"><input name = "id" type="hidden" value="'.$id.'">';
                $form->type     = 'delete';
            }

            $form->portlet          = true;
            $form->button_back      = true;
            $form->id               = 'form-menu-group';
            $form->param            = $data;
            $form->url_form         = $this->class_name.'/form.tpl';
            echo $form->generate();
        
        } else {
            $this->access_right->redirect();
        }
    }


    public function edit($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->add($id);
    }

    public function proses() {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('nama', 'Nama Grup', 'required');

            if ($this->form_validation->run()) {
                $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');

                $id = $this->input->post('id');

                $data = array(
                    'nama_grup_menu' => $this->input->post('nama'),
                    'icon' => $this->input->post('icon')
                );

                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */

                $this->db->trans_start();
                if ($id == '') {
                    $this->menu_group_model->create($data);
                } else {
                    $this->menu_group_model->update($data, $id);
                }

                if($this->db->trans_status()){
                    $message = array('success'=>true, 'message'=>'Save Menu Success');
                }else{
                    $message = array('success'=>false, 'message'=>'Database Error');
                }
                $this->db->trans_complete();

            } else {
                $message = array('success'=>false, 'message'=>validation_errors());
            }
            echo json_encode($message);
        } else {
            $this->access_right->redirect();
        }
    }

    public function proses_reordering(){
        $data = $this->input->post('data');
        //$data = array(0,1,2,10,11);
        $this->db->trans_start();
        $message = array('success'=>false, 'message'=> 'Update Failed');

        if($data) {
            $this->load->model('menu_group_model');

            foreach ($data as $urutan => $id) {
                $id = url_base64_decode($id);
                $this->menu_group_model->set('urutan', $urutan)->findByPk($id)->update();

            }

            if($this->db->trans_status()){
                $message = array('success'=>true, 'message'=> 'Update Success');
            }
        }

        $this->db->trans_complete();
        echo json_encode($message);
    }

    public function delete($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->add($id,$status_delete = 1);
    }

    public function proses_delete() {
        $id = $this->input->post('id');
        $this->access_right->otoritas('delete', true);
        if ($this->access_right->otoritas('delete', true)) {
            $this->db->trans_start();
            $this->menu_group_model->delete($id);
            if($this->db->trans_status()){
                $message = array('success'=>true, 'message'=>'Delete Success', 'callback'=>'refreshNewCustomTable()');
            }else{
                $message = array('success'=>false, 'message'=>'Database Error');
            }
            $this->db->trans_complete();
            echo json_encode($message);
        }
    }

}

/* End of file menu_group.php */
/* Location: ./application/controllers/menu_group.php */
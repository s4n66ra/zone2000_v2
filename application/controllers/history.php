<?php

class history extends My_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model(array(
            'shipping_model','shipping_koli_model','koli_model', 'item_stok_model', 'item_model','itemtype_model', 'transaction_model', 'machine_transaction_model', 'mesin_model'
        ));


        $this->button['add'] = view::button_add(array('onclick'=>"btnLoadNextPage(this)"));

    }

    public function index() {
        $data['title'] = 'Machine Transactions History';

        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'base/page_content.tpl';
        
        //$data['button_group']   = $this->getAvailableButtons();
        $data['javascript'] = array('history.js');
        
        $data['table']['main'] = $this->getForm('main');
        $this->twig->display('index.tpl', $data);
    }

    public function main(){
        $data['content']    = 'base/page_content.tpl';     
        $data['table']['main']  = $this->history_kuras(array('wrapper'));
        return $this->twig->display('index.tpl', $data);
    }

    public function history_kuras($option = array()){

        $this->load->library('Datatable');
        $table = $this->datatable;

        $data = '';
        if (in_array('wrapper', $option)) {
            $table->numbering  = true;
            $table->id         = 'table-coin-kuras';
            $table->isScrollable= false;
        } else {
            $table
                ->setModel($this->transaction_model)
                ->setNumbering()
                ->with(array('transaction_machine','transaction_store','machine','cabang','pegawai', 'jenis_mesin'))
                ->where(array('transactiontype_id' => 2))
                ->select('kd_mesin, num_coin, DATE(rec_created) as rec_created, nama_pegawai ,nama_cabang')
                ->order_by('rec_created desc');
                //->edit_column('rec_created', '$1', 'date::longDate(rec_created)');
                //->edit_column('shipping_id', '$1', 'view::btn_group_receiving_warehouse(shipping_id)');
            echo $table->generate();
        }

    }

    public function history_cutter($option = array()){

        $this->load->library('Datatable');
        $table = $this->datatable;

        $data = '';
        if (in_array('wrapper', $option)) {
            $table->numbering  = true;
            $table->id         = 'table-cutter';
            $table->isScrollable= false;
        } else {
            $table
                ->setModel($this->transaction_model)
                ->setNumbering()
                ->with(array('transaction_machine','transaction_store','machine','cabang','pegawai', 'jenis_mesin'))
                ->where(array('transactiontype_id' => 2))
                ->select('kd_mesin, num_coin, DATE(rec_created) as rec_created, nama_pegawai ,nama_cabang')
                ->order_by('rec_created desc');
                //->edit_column('rec_created', '$1', 'date::longDate(rec_created)');
                //->edit_column('shipping_id', '$1', 'view::btn_group_receiving_warehouse(shipping_id)');
            //$data = $table;
            echo $table->generate();
        }
        print_r($data);

    }
    

    public function getForm($type = 'add', $id=NULL, $param = array()){
        //echo $type;
        $this->load->library('custom_form');
        $this->load->library('custom_table');
        $this->load->library('table_data');
        $this->load->model(array('store_data_model'));

        $storedata = $this->store_data_model;
        $mesinModel = $this->mesin_model;

        $form       = new custom_form();
        $table      = new custom_table();
        $tableData  = new table_data();

        $this->load->library('Datatable');

        switch ($type) {
            case 'main' :
                $param['page']['coin_kuras'] = $this->getForm('page-coin-kuras');
                
                $param['page']['cutter']  = $this->getForm('page-cutter');
                //$param['page']['coin_meter']  = $this->getForm('page-coin-meter');
                
                $param['page']['ticket_meter'] = $this->getForm('page-ticket-meter');
                $param['page']['ticket_refill'] = $this->getForm('page-ticket-refill');
                $param['page']['mc_refill'] = $this->getForm('page-mc-refill');
                $param['page']['mc_redemp'] = $this->getForm('page-mc-redemp');
                return $this->twig->render($this->class_name.'/page_main.tpl', $param);
                break;

            case 'page-coin-meter' :
                $param['portlet'] = false;
                $param['table']['main']     = $this->getForm('form-coin-meter');
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'page-cutter' :
                $param['portlet'] = false;
                $param['table']['main']     = $this->getForm('form-cutter');
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'form-cutter' :

                $data = $this->mesin_model->options_filter();                

                $form_cutter       = new custom_form();
                $table_cutter      = new custom_table();
                $tableData_cutter  = new table_data();

                $table_cutter = new Datatable();


                $filter = array(
                    array('id_mesin', 'Machine ID', 'list', $data),
                    array('num_coin', 'Number Coin', 'text'),
                    array('rec_created', 'Date', 'date'),
                    array('nama_pegawai', 'Employee', 'text'),
                    array('nama_cabang', 'Store Name', 'text'),

                );



                $table_cutter->dataFilter = $filter;

                $table_cutter->numbering  = true;

                //$tableData->list_item   = $this->item_model->options(itemtype::ITEM_MERCHANDISE);
                $tableData_cutter->list_bundle = $this->mesin_model->options2();

                $table_cutter->id         = 'table-cutter';
                $table_cutter->isScrollable= false;
                $table_cutter->header     = array('MACHINE CODE','SUM', 'DATE', 'EMPLOYEE' ,'STORE');
                $table_cutter->source     = site_url($this->class_name.'/history_cutter');
                

                //return $table_cutter->generateWrapper();
                break;

            

            case 'form-coin-meter' :
                $table->columnWidth = array(30,30,30,10);
                $table->columns     = array('MACHINE', 'INFO', 'COIN METER', '');
                $table->id          = 'table-coin-meter';
                $table->numbering   = false;
                $tableData->table   = $table;
                $tableData->param   = array(
                                        'type_id' => transactiontype::COIN,
                                    );
                return $tableData->generate();
                break;

            case 'page-coin-kuras' :
                $param['portlet'] = false;
                $param['table']['main']     = $this->getForm('form-coin-kuras');
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'form-coin-kuras' :
                $data = $this->mesin_model->options_filter();                

                $form_kuras       = new custom_form();
                $table_kuras      = new custom_table();
                $tableData_kuras  = new table_data();
                $table_kuras = new Datatable;


                $filter = array(
                    array('id_mesin', 'Machine ID', 'list', $data),
                    array('num_coin', 'Number Coin', 'text'),
                    array('rec_created', 'Date', 'date'),
                    array('nama_pegawai', 'Employee', 'text'),
                    array('nama_cabang', 'Store Name', 'text'),

                );


                $table_kuras->dataFilter = $filter;

                $table_kuras->numbering  = true;

                //$tableData->list_item   = $this->item_model->options(itemtype::ITEM_MERCHANDISE);
                $tableData_kuras->list_bundle = $this->mesin_model->options2();

                $table_kuras->id         = 'table-coin-kuras';
                $table_kuras->isScrollable= false;
                $table_kuras->header     = array('MACHINE CODE','SUM', 'DATE', 'EMPLOYEE' ,'STORE');
                $table_kuras->source     = site_url($this->class_name.'/history_kuras');
                

                return $table_kuras->generateWrapper();
                break;
            
            
            case 'page-ticket-meter' :
                $param['portlet'] = false;
                $param['table']['main']     = $this->getForm('form-ticket-meter');
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'form-ticket-meter' :
                $table->columnWidth = array(30,30,30,10);
                $table->columns     = array('MACHINE', 'INFO', 'TICKET METER', '');
                $table->id          = 'table-ticket-meter';
                $table->numbering   = false;
                $tableData->table   = $table;
                $tableData->param   = array(
                                        'type_id' => transactiontype::TOUT,
                                    );
                return $tableData->generate();
                break;

            case 'page-ticket-refill' :
                $param['portlet'] = false;
                $param['table']['main']     = $this->getForm('form-ticket-refill');
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'form-ticket-refill' :
                $table->columnWidth = array(20,30,20,20,10);
                $table->columns     = array('MACHINE', 'INFO', 'START', 'END', '');
                $table->id          = 'table-ticket-refill';
                $table->numbering       = false;
                $tableData->num_input   = 2;
                $tableData->table       = $table;
                $tableData->param       = array(
                                            'type_id' => transactiontype::TFIL,
                                        );
                return $tableData->generate();
                break;

            case 'page-mc-refill' :
                $param['portlet'] = false;
                $param['table']['main']     = $this->getForm('form-mc-refill');
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'form-mc-refill' :
                $this->load->model(array('item_model','mesin_model'));
                $table->columnWidth = array(30,30,30,10);
                $table->columns     = array('MACHINE', 'INFO', 'ITEM QTY', '');
                $table->id          = 'table-mc-refill';
                $table->numbering       = false;
                $tableData->list_item   = $this->item_model->options(itemtype::ITEM_MERCHANDISE);
                $tableData->list_bundle = $this->mesin_model->options2();
                $tableData->table       = $table;
                $tableData->param       = array(
                                            'type_id' => transactiontype::MFIL,
                                        );
                return $tableData->generate();
                break;

            case 'page-mc-redemp' :
                $param['portlet'] = false;
                $param['table']['main']     = $this->getForm('form-mc-redemp');
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'form-mc-redemp' :
                $this->load->model(array('item_model','mesin_model'));
                $table->columnWidth = array(30,30,30,10);
                $table->columns     = array('MACHINE', 'INFO', 'ITEM QTY', '');
                $table->id          = 'table-mc-redemp';
                $table->numbering       = false;
                $tableData->list_item   = $this->item_model->options(itemtype::ITEM_MERCHANDISE);
                $tableData->list_bundle = $this->mesin_model->options2();
                $tableData->table       = $table;
                $tableData->param       = array(
                                            'type_id' => transactiontype::MFIL,
                                        );
                return $tableData->generate();
                break;

            case 'target' :
                $this->twig->add_function('getMonthName');
                $param['form_action'] = form_open(
                                            site_url($this->class_name.'/proses_target/'.$id),
                                            '',
                                            array(
                                                'id' => $id,
                                            ));
                // options untuk list year
                $options['htmlOptions'] = $htmlOptions            =  'id="list-year" target="#form-target-part" data-source="'.site_url($this->class_name.'/form_target/'.$id).'" onchange="refreshList(this)" class="form-control"';

                $param['form_part']     = $this->getForm('target-part', $id);
                $param['list']['year']  = view::dropdown_year($temp['year_month'], 2, $options);
                $form->param = $param;
                $form->url_form     = $this->class_name.'/form_target.tpl';
                return $form->generate();
                break;

            default:
                return '';
                break;
        }
    }

    public function testquery(){
        $this->load->model('transaction_model');

        $data = $this->transaction_model
                ->with(array('transaction_machine','transaction_store','cabang','pegawai_cabang','pegawai'))
                ->where('transactiontype_id',2)
                ->get()
                ->result();
        print_r($data);
    }

}

/* End of file bbm.php */
/* Location: ./application/controllers/bbm.php */

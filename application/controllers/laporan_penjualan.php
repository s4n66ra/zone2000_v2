<?php

/**
 * Description of laporan penjualan
 *
 * @author SANGGRA HANDI
 */
class laporan_penjualan extends My_Controller {


    public function __construct() {
        parent::__construct();
        //$this->load->model('selling_model');
    }

    
    public function index() {
        $data['title'] = 'Laporan CH';

        /* INSERT LOG */
        //$this->access_right->activity_logs('view',$data['title']);
        /* END OF INSERT LOG */

        //$data['button_group'] = $this->getAvailableButtons();
        //$data['button_right'] = $this->getTools();
        $data['button_right'] = $this->getPrintTools();

        //========================== sistem date ========================
        $data['st_date'] = 1;
        $data['target'] = base_url('replenish/insertToSession');
        $sistem_date = $this->session->userdata['sistem_date'];
        if(!empty($sistem_date)){
            $data['hid_date'] = $sistem_date;
            $data['style'] = "style=padding-top:6px;color:rgb(255,0,0);";
        }else{
            $data['hid_date'] = date('Y-m-d');
            $data['style'] = 'style="padding-top:6px; color: rgb(0, 0, 0);"';
        }
        //========================== end sistem date ========================

        $data['table']['main']  = $this->table_main(array('wrapper', 'filter'));
        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'base/page_content.tpl';
    
        $this->twig->display('index.tpl', $data);
    }

    public function table_main($option = array()){
        //$this->load->model('selling_model');
        $this->load->library('Datatable');
        /*$table = $this->datatable;
        $is_head_office = hprotection::isHeadOffice();*/

        $table = new Datatable();
        $table->reset();

        /*if (in_array('filter', $option)) {
            $table->dataFilter = array(
                                array('nama_selling', 'Merchandise Name', 'text'),
                                array('this.sellingtype_id', 'Type', 'list', $this->selling_type_model->options()),
                            );
        }*/
        $sistem_date = $this->session->userdata['sistem_date'];
        if(!empty($sistem_date)){
            $time=strtotime($sistem_date);
            $month=date("m",$time);
            $year=date("Y",$time);
        }else{
            $month = date("m");
            $year = date("Y");
        }

        if (in_array('wrapper', $option)) {
            $table->dropdowncabangpenjualan = true;
            $table->pagination=false;
            $table->numbering  = true;
            $table->isScrollable= true;
            $table->btn_refresh = false;
            $table->id         = 'table-laporan-penjualan';
            $header     = array('TIKET','SKU','NAME','STOK');
            $table->width       = array(20,200);
            for($i=1;$i<=31;$i++){
                $header     = array_merge($header,array($i,'TX'));
            }
            $header = array_merge($header,array('total','total tiket'));
            $table->header = $header;
            $table->source     = site_url($this->class_name.'/table_main');
            return $table->generateWrapper();
        } else {
            //store id 4 ini berarti blok m store...
            $table
                ->setModel($this->laporan_penjualan_model)
                ->setNumbering()
                ->with(array('item','stok','store'))
                ->select('this.rec_created,item.item_key, item_name,stok_total , sum(item_qty) as qty, day(this.rec_created) as tgl,ticket_count,this.item_id,')
                ->where('this.rec_created <= ',$year.'-'.$month.'-31')
                ->where('this.rec_created >= ',$year.'-'.$month.'-01')
                ->where(array("stok.store_id" => 4))
                ->group_by( array('item.item_key','day(this.rec_created)'))
                ->order_by('item.item_key')
                ->edit_column('qty', '$1', 'hgenerator::number(qty)');
                //->edit_column('item_id', '$1', 'view::btn_group_edit_delete(item_id)') ;
            $row = $table->getData();

            $new_key = 0;
            $temp_key = '';
            $last_key = '';
            $data = array();
            for($i=1;$i<=31;$i++){
                $v[$i] = 0;
                $t[$i] = 0;
            }
            $sum_v = 0;
            $sum_t = 0;
            foreach ($row as $key => $value) {
                $temp_key = $value['item_key'];
                $data[$temp_key]['no'] = 1;
                $data[$temp_key]['ticket_count'] = $value['ticket_count'];
                $data[$temp_key]['item_key'] = $value['item_key'];
                $data[$temp_key]['item_name'] = $value['item_name'];
                $data[$temp_key]['stok_total'] = $value['stok_total'];
                $data[$temp_key]['v'.$value['tgl']] = $value['qty'];
                $data[$temp_key]['t'.$value['tgl']] = $value['qty'] * $value['ticket_count'];
                $data[$temp_key]['total'] += $value['qty'];
                $data[$temp_key]['total_ticket'] += $value['qty'] * $value['ticket_count'];
            }
            $no = 0;
            foreach ($data as $key => $value) {
                $temp_data[$no]['no'] = 1;
                $temp_data[$no]['ticket_count'] = $value['ticket_count'];
                $temp_data[$no]['item_key'] = $value['item_key'];
                $temp_data[$no]['item_name'] = $value['item_name'];
                $temp_data[$no]['stok_total'] = $value['stok_total'];
                for($i=1;$i<=31;$i++){
                    if(!empty($value['v'.$i])){
                        $temp_data[$no]['v'.$i] = $value['v'.$i];
                        $temp_data[$no]['t'.$i] = $value['t'.$i];
                        $v[$i] += $value['v'.$i];
                        $t[$i] += $value['t'.$i];
                    }else{
                        $temp_data[$no]['v'.$i] = 0;
                        $temp_data[$no]['t'.$i] = 0;
                    }
                }
                $temp_data[$no]['total'] = $value['total'];
                $temp_data[$no]['total_ticket'] = $value['total_ticket'];
                $sum_v += $value['total'];
                $sum_t += $value['total_ticket'];
                $no++;
                # code...
            }
            $temp_data[$no]['no'] = 1;
            $temp_data[$no]['ticket_count'] = '';
            $temp_data[$no]['item_key'] = '';
            $temp_data[$no]['item_name'] = 'Total';
            $temp_data[$no]['stok_total'] = '';
            for($i=1;$i<=31;$i++){
                $temp_data[$no]['v'.$i] = $v[$i];
                $temp_data[$no]['t'.$i] = $t[$i];
            }
            $temp_data[$no]['total'] = $sum_v;
            $temp_data[$no]['total_ticket'] = $sum_t;
            $table->setData($temp_data);
            echo $table->generate();
        }

    }

    public function update_table_main($store_id=4, $option = array()){
        //$this->load->model('selling_model');
        $this->load->library('Datatable');
        /*$table = $this->datatable;
        $is_head_office = hprotection::isHeadOffice();*/

        $table = new Datatable();
        $table->reset();

        /*if (in_array('filter', $option)) {
            $table->dataFilter = array(
                                array('nama_selling', 'Merchandise Name', 'text'),
                                array('this.sellingtype_id', 'Type', 'list', $this->selling_type_model->options()),
                            );
        }*/
        $sistem_date = $this->session->userdata['sistem_date'];
        if(!empty($sistem_date)){
            $time=strtotime($sistem_date);
            $month=date("m",$time);
            $year=date("Y",$time);
        }else{
            $month = date("m");
            $year = date("Y");
        }

        if (in_array('wrapper', $option)) {
            $table->dropdowncabangpenjualan = true;
            $table->pagination =false;
            $table->numbering  = true;
            $table->isScrollable= true;
            $table->btn_refresh = false;
            $table->id         = 'table-laporan-penjualan';
            $header     = array('TIKET','SKU','NAME','STOK');
            $table->width       = array(20,200);
            for($i=1;$i<=31;$i++){
                $header     = array_merge($header,array($i,'TX'));
            }
            $header = array_merge($header,array('total','total tiket'));
            $table->header = $header;
            $table->source     = site_url($this->class_name.'/table_main');
            return $table->generateWrapper();
        } else {
            $table
                ->setModel($this->laporan_penjualan_model)
                ->setNumbering()
                ->with(array('item','stok','store'))
                ->select('rec_created,item.item_key, item_name,stok_total , sum(item_qty) as qty, day(rec_created) as tgl,ticket_count,this.item_id,')
                ->where('rec_created <= ',$year.'-'.$month.'-31')
                ->where('rec_created >= ',$year.'-'.$month.'-01')
                ->where(array("stok.store_id" => $store_id))
                ->group_by( array('item.item_key','day(rec_created)'))
                ->order_by('item.item_key')
                ->edit_column('qty', '$1', 'hgenerator::number(qty)');
                //->edit_column('item_id', '$1', 'view::btn_group_edit_delete(item_id)') ;
            $row = $table->getData();

            $new_key = 0;
            $temp_key = '';
            $last_key = '';
            $data = array();
            for($i=1;$i<=31;$i++){
                $v[$i] = 0;
                $t[$i] = 0;
            }
            $sum_v = 0;
            $sum_t = 0;
            foreach ($row as $key => $value) {
                $temp_key = $value['item_key'];
                $data[$temp_key]['no'] = 1;
                $data[$temp_key]['ticket_count'] = $value['ticket_count'];
                $data[$temp_key]['item_key'] = $value['item_key'];
                $data[$temp_key]['item_name'] = $value['item_name'];
                $data[$temp_key]['stok_total'] = $value['stok_total'];
                $data[$temp_key]['v'.$value['tgl']] = $value['qty'];
                $data[$temp_key]['t'.$value['tgl']] = $value['qty'] * $value['ticket_count'];
                $data[$temp_key]['total'] += $value['qty'];
                $data[$temp_key]['total_ticket'] += $value['qty'] * $value['ticket_count'];
            }
            $no = 0;
            foreach ($data as $key => $value) {
                $temp_data[$no]['no'] = 1;
                $temp_data[$no]['ticket_count'] = $value['ticket_count'];
                $temp_data[$no]['item_key'] = $value['item_key'];
                $temp_data[$no]['item_name'] = $value['item_name'];
                $temp_data[$no]['stok_total'] = $value['stok_total'];
                for($i=1;$i<=31;$i++){
                    if(!empty($value['v'.$i])){
                        $temp_data[$no]['v'.$i] = $value['v'.$i];
                        $temp_data[$no]['t'.$i] = $value['t'.$i];
                        $v[$i] += $value['v'.$i];
                        $t[$i] += $value['t'.$i];
                    }else{
                        $temp_data[$no]['v'.$i] = 0;
                        $temp_data[$no]['t'.$i] = 0;
                    }
                }
                $temp_data[$no]['total'] = $value['total'];
                $temp_data[$no]['total_ticket'] = $value['total_ticket'];
                $sum_v += $value['total'];
                $sum_t += $value['total_ticket'];
                $no++;
                # code...
            }
            $temp_data[$no]['no'] = 1;
            $temp_data[$no]['ticket_count'] = '';
            $temp_data[$no]['item_key'] = '';
            $temp_data[$no]['item_name'] = 'Total';
            $temp_data[$no]['stok_total'] = '';
            for($i=1;$i<=31;$i++){
                $temp_data[$no]['v'.$i] = $v[$i];
                $temp_data[$no]['t'.$i] = $t[$i];
            }
            $temp_data[$no]['total'] = $sum_v;
            $temp_data[$no]['total_ticket'] = $sum_t;
            $table->setData($temp_data);
            echo $table->generate();
        }

    }


    public function get_data_update_table_main($store_id=4, $option = array()){
        //$this->load->model('selling_model');
        $this->load->library('Datatable');
        /*$table = $this->datatable;
        $is_head_office = hprotection::isHeadOffice();*/

        $table = new Datatable();
        $table->reset();

        /*if (in_array('filter', $option)) {
            $table->dataFilter = array(
                                array('nama_selling', 'Merchandise Name', 'text'),
                                array('this.sellingtype_id', 'Type', 'list', $this->selling_type_model->options()),
                            );
        }*/
        $sistem_date = $this->session->userdata['sistem_date'];
        if(!empty($sistem_date)){
            $time=strtotime($sistem_date);
            $month=date("m",$time);
            $year=date("Y",$time);
        }else{
            $month = date("m");
            $year = date("Y");
        }

        if (in_array('wrapper', $option)) {
            $table->dropdowncabangpenjualan = true;
            $table->pagination =false;
            $table->numbering  = true;
            $table->isScrollable= true;
            $table->btn_refresh = false;
            $table->id         = 'table-laporan-penjualan';
            $header     = array('TIKET','SKU','NAME','STOK');
            $table->width       = array(20,200);
            for($i=1;$i<=31;$i++){
                $header     = array_merge($header,array($i,'TX'));
            }
            $header = array_merge($header,array('total','total tiket'));
            $table->header = $header;
            $table->source     = site_url($this->class_name.'/table_main');
            return $table->generateWrapper();
        } else {
            $table
                ->setModel($this->laporan_penjualan_model)
                ->setNumbering()
                ->with(array('item','stok','store'))
                ->select('rec_created,item.item_key, item_name,stok_total , sum(item_qty) as qty, day(rec_created) as tgl,ticket_count,this.item_id,')
                ->where('rec_created <= ',$year.'-'.$month.'-31')
                ->where('rec_created >= ',$year.'-'.$month.'-01')
                ->where(array("stok.store_id" => $store_id))
                ->group_by( array('item.item_key','day(rec_created)'))
                ->order_by('item.item_key')
                ->edit_column('qty', '$1', 'hgenerator::number(qty)');
                //->edit_column('item_id', '$1', 'view::btn_group_edit_delete(item_id)') ;
            $row = $table->getData();

            $new_key = 0;
            $temp_key = '';
            $last_key = '';
            $data = array();
            for($i=1;$i<=31;$i++){
                $v[$i] = 0;
                $t[$i] = 0;
            }
            $sum_v = 0;
            $sum_t = 0;
            foreach ($row as $key => $value) {
                $temp_key = $value['item_key'];
                //$data[$temp_key]['no'] = 1;
                $data[$temp_key]['ticket_count'] = $value['ticket_count'];
                $data[$temp_key]['item_key'] = $value['item_key'];
                $data[$temp_key]['item_name'] = $value['item_name'];
                $data[$temp_key]['stok_total'] = $value['stok_total'];
                $data[$temp_key]['v'.$value['tgl']] = $value['qty'];
                $data[$temp_key]['t'.$value['tgl']] = $value['qty'] * $value['ticket_count'];
                $data[$temp_key]['total'] += $value['qty'];
                $data[$temp_key]['total_ticket'] += $value['qty'] * $value['ticket_count'];
            }
            $no = 0;
            foreach ($data as $key => $value) {
                //$temp_data[$no]['no'] = 1;
                $temp_data[$no]['ticket_count'] = $value['ticket_count'];
                $temp_data[$no]['item_key'] = $value['item_key'];
                $temp_data[$no]['item_name'] = $value['item_name'];
                $temp_data[$no]['stok_total'] = $value['stok_total'];
                for($i=1;$i<=31;$i++){
                    if(!empty($value['v'.$i])){
                        $temp_data[$no]['v'.$i] = $value['v'.$i];
                        $temp_data[$no]['t'.$i] = $value['t'.$i];
                        $v[$i] += $value['v'.$i];
                        $t[$i] += $value['t'.$i];
                    }else{
                        $temp_data[$no]['v'.$i] = 0;
                        $temp_data[$no]['t'.$i] = 0;
                    }
                }
                $temp_data[$no]['total'] = $value['total'];
                $temp_data[$no]['total_ticket'] = $value['total_ticket'];
                $sum_v += $value['total'];
                $sum_t += $value['total_ticket'];
                $no++;
                # code...
            }
            //$temp_data[$no]['no'] = 1;
            $temp_data[$no]['ticket_count'] = '';
            $temp_data[$no]['item_key'] = '';
            $temp_data[$no]['item_name'] = 'Total';
            $temp_data[$no]['stok_total'] = '';
            for($i=1;$i<=31;$i++){
                $temp_data[$no]['v'.$i] = $v[$i];
                $temp_data[$no]['t'.$i] = $t[$i];
            }
            $temp_data[$no]['total'] = $sum_v;
            $temp_data[$no]['total_ticket'] = $sum_t;
            return $temp_data;
        }

    }


    public function add($id='',$status_delete = 0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->config->load('twig');
            $this->load->model('item_model');
            $title = 'Tambah Data Penjualan Langsung';
            $data['form_action'] = $this->class_name . '/proses';

            $data['id'] = $id;
            if($id){
                $title = 'Edit Data Penjualan Langsung';
                $row            = $this->selling_model->getById($id)->row();
                $data['data']  = $row;
            }

            $data['title']      = $title;
            $data['assets_url'] = $this->config->item('assets_url');
            $data['sidebar']    = $this->access_right->menu();
            $data['form']       = $this->class_name.'/form_selling.tpl';

            $button_group   = array();
            $button_group[] = view::button_back();
            
            if($status_delete == 0){
                $data['form_action']    = view::form_input($id);
                $button_group[]         = view::button_save();
            }else{
                $data['title']          = 'Delete Selling';
                $data['form_action']    = view::form_delete($id);;
                $data['readonly']       = 'readonly=""';
                $button_group[]         = view::button_delete_confirm();
            }

            $data['button_group'] = view::render_button_group($button_group);

            $is_head_office = hprotection::isHeadOffice();
            $this->load->model('store_model');
            if($is_head_office){
                $htmlOptions            =  'id="list-store" class="form-control select2" data-placeholder="Select Store"';
                $data['list_store']     = form_dropdown('data[store_id]', $this->store_model->options_empty(), $row->store_id,$htmlOptions);
            }else{
                $htmlOptions            =  'id="list-store" class="form-control select2" data-placeholder="Select Store" disabled="disabled"';
                $data['list_store']     = form_dropdown('data[store_id]', $this->store_model->options_empty(), $this->session->userdata('store_id'),$htmlOptions ).form_hidden('data[store_id]',$this->session->userdata('store_id'));
            }
            if(empty($row->selling_date)){
                $data['data']['selling_date'] = date('Y-m-d');
            }
/*            $htmlOptions        = 'id="list-item" placeholder="Select Item" class="select2 form-control" data-live-search="true"';

            $data['list_item']  = form_dropdown('data[item_id]', $this->item_model->options_empty(2), $row->item_id, $htmlOptions);
*/
            $data['list_item'] = '<input type="hidden" id="sell_list_item_id" name="data[item_id]" class="form-control" value="'.$row->item_id.'"/>';


            //$htmlOptions        = 'id="list-item" placeholder="Select Item" class="bs-select form-control" data-live-search="true"';

            //$data['list_jenis']  = form_dropdown('data[jenis_selling_id]', $this->selling_model->options_jenis(), $row->jenis_selling_id, $htmlOptions);
            $data['list_jenis'] = '<input type="hidden" name="data[jenis_selling_id]"  value="1"/>';
            
            
            $this->twig->display('base/page_form.tpl', $data);

        } else {
            $this->access_right->redirect();
        }
    }

    

    public function edit($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('edit', true);
        $this->add($id);
    }

    public function delete($id_enc = '') {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('delete', true);
        $this->add($id,$status_delete = 1);
    }

    public function proses() {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('data[store_id]', 'Outlet', 'required|trim');
            $this->form_validation->set_rules('data[jenis_selling_id]', 'Type selling', 'required|trim');
            $this->form_validation->set_rules('data[item_id]', 'Item', 'required|trim');
            $this->form_validation->set_rules('data[qty]', 'Qty', 'required|trim');

            $this->load->model('item_stok_model');
            $modelItemStok = $this->item_stok_model;


            if ($this->form_validation->run()) {
                $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');

                $id = $this->input->post('id');
                $data = $this->input->post('data');
                $data['qty'] = hgenerator::switch_number($data['qty']);
                $this->db->trans_start();
                $qty = $data['qty'];
                if ($id){
                    $row            = $this->selling_model->getById($id)->row();
                    $dataold  = $row;
                    $this->selling_model->update($data, $id);

                    $whereCek = array('store_id'=>$data['store_id'],'item_id'=>$data['item_id']);
                    $cekEksisStok = $modelItemStok->select('itemstok_id')->where($whereCek)->get()->num_rows();
                    if($cekEksisStok>0){
                        $modelItemStok
                            ->set("stok_gudang","stok_gudang-$qty",FALSE)
                            ->set("stok_total","stok_total-$qty",FALSE)
                            ->where($whereCek)->update();
                    }else{
                        $modelItemStok
                            ->set("stok_gudang","stok_gudang-$qty",FALSE)
                            ->set("stok_total","stok_total-$qty",FALSE)
                            ->set($whereCek)->insert();
                    }

                    $whereCek2 = array('store_id'=>$dataold->store_id,'item_id'=>$dataold->item_id);
                    $qty2 = $dataold->qty;
                    $cekEksisStok = $modelItemStok->select('itemstok_id')->where($whereCek2)->get()->num_rows();
                    if($cekEksisStok>0){
                        $modelItemStok
                            ->set("stok_gudang","stok_gudang+$qty2",FALSE)
                            ->set("stok_total","stok_total+$qty2",FALSE)
                            ->where($whereCek2)->update();
                    }else{
                        $data = array(
                           'stok_gudang' => "stok_gudang+$qty2" ,
                           'stok_total' => "stok_total+$qty2" 
                        );
                        $this->db->insert('m_item_stok',$data);
                    }
                }else{
                    $this->selling_model->create($data);
                    $whereCek = array('store_id'=>$data['store_id'],'item_id'=>$data['item_id']);
                    $cekEksisStok = $modelItemStok->select('itemstok_id')->where($whereCek)->get()->num_rows();
                    if($cekEksisStok>0){
                        $modelItemStok
                            ->set("stok_gudang","stok_gudang-$qty",FALSE)
                            ->set("stok_total","stok_total-$qty",FALSE)
                            ->where($whereCek)->update();
                    }else{
                        $data = array(
                           'stok_gudang' => 0 ,
                           'stok_total' => 0 
                        );
                        $this->db->insert('m_item_stok',$data);
                    }
                }

                
                
                if($this->db->trans_status()){
                    $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh_filter()');
                }else{
                    $message = array(false, 'Terjadi Kesalahan',$this->db->_error_message(), '');
                }
                $this->db->trans_complete();
            } else {
                $message = array(false, 'Terjadi Kesalahan', validation_errors());
            }
            echo json_encode($message);
        } else {
            $this->access_right->redirect();
        }
    }

    public function proses_delete() {
        $id=$this->input->post("id");
		$this->access_right->otoritas('delete', true);
        if ($this->access_right->otoritas('delete', true)) {
            $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');
            if ($this->selling_model->delete($id)) {
                $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', 'refresh_filter()');
				/* INSERT LOG */
				$this->access_right->activity_logs('delete','Delete Merchandise');
				/* END OF INSERT LOG */
            }
            echo json_encode($message);
        }
    }

    public function getPrintTools(){
        $button_group = array();
        if($this->access_right->otoritas('print')){
            $button_group[] = view::button_export_laporan_penjualan();
        }
        
        if(true){//export
            //$button_group[] = view::button_export();
        }
        return view::render_button_group_laporan_penjualan($button_group, array(), true);

    }


    public function getItemsByIdAndKeywordJSON($itemtype_id=''){
        $this->load->model(array('item_model'));

        $tmp = $this->item_model->getItemsByIdAndKeyword($itemtype_id);
        
        //$tmp = $this->item_model->getItemsByIdAndKeyword(2,'');
        $arr = array();
        foreach ($tmp as $k => $v) {
            if($k != ''){
                $obj = new StdClass();
                $obj->id        = $k;
                $obj->text      = $tmp[$k];
                array_push($arr, $obj);
            }
        }
        echo json_encode($arr);
    }


    public function excel($store_id=''){

        $this->load->model(array('store_model'));        
        $filter = array(
            'TIKET'         =>'1',
            'SKU'           =>'1',
            'NAME'          =>'1',
            'STOK'          =>'1',
            't1'             =>'TX',
            
            't2'             =>'TX',
            
            't3'             =>'TX',
            
            't4'             =>'TX',
            
            't5'             =>'TX',
            
            't6'             =>'TX',
            
            't7'             =>'TX',
            
            't8'             =>'TX',
            
            't9'             =>'TX',
            
            't10'            =>'TX',
            
            't11'            =>'TX',
            
            't12'            =>'TX',
            
            't13'            =>'TX',
            
            't14'            =>'TX',
            
            't15'            =>'TX',
            
            't16'            =>'TX',
            
            't17'            =>'TX',
            
            't18'            =>'TX',
            
            't19'            =>'TX',
            
            't20'            =>'TX',
            
            't21'            =>'TX',
            
            't22'            =>'TX',
            
            't23'            =>'TX',
            
            't24'            =>'TX',
            
            't25'            =>'TX',
            
            't26'            =>'TX',
            
            't27'            =>'TX',
            
            't28'            =>'TX',
            
            't29'            =>'TX',
            
            't30'            =>'TX',
            
            't31'            =>'TX',
            
            'Total'         =>'1',
            'Total Tiket'   =>'1',
            );

        $data['filter'] = $filter;
        //----------------------------------------------------
        $date = date('Ymd His');
        $data['judul_kecil']    = 'Laporan Penjualan';
        $data['filename']       = 'Laporan Penjualan('.$date.')';
        $data['content']        = $this->get_data_update_table_main($store_id);
        $data['selected_store'] = $this->store_model
                                 ->select('nama_cabang')
                                 ->where(array("id_cabang"=>$store_id))
                                 ->get()
                                 ->row();
        //print_r($data['content']);
        //print_r($data['selected_store']);
        $this->load->view('cetak_excel_penjualan',$data);
    
    }




}

/* End of file bbm.php */
/* Location: ./application/controllers/bbm.php */

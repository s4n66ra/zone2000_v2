<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class history_coin_kuras extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array(
            'shipping_model','shipping_koli_model','koli_model', 'item_stok_model', 'item_model','itemtype_model', 'transaction_model', 'machine_transaction_model', 'mesin_model'
        ));
    }

    public function index() {

        $data['title'] 		= 'HISTORY COIN KURAS';
        $data['sidebar'] 	= $this->access_right->menu();
        $data['content'] 	= 'base/page_content.tpl';     
        //$data['button_group'] 	= $this->getAvailableButtons();
        $data['table']['main'] 	= $this->getForm('other', '', array('wrapper'));
        $data['javascript'] = array('receiving.js');
        
        $this->twig->display('index.tpl', $data);
    }

    public function getForm($type = '', $id = NULL, $param = array()){
        $this->load->library(array('custom_form','custom_table'));
        $this->load->model(array('sequence_model','po_store_model'));
        $form = $this->custom_form;

        switch ($type) {
            case 'page-detail':
                $param['title']     = 'RECEIVING DETAIL';
                $param['portlet']   = true;
                $param['button']['back'] = true;
                $param['form']['info']  = $this->getForm('form-info', $id);
                $param['table']['main'] = $this->getForm('page-koli', $id);
                return $this->twig->render('base/page_content.tpl', $param);
                break;
            
            case 'table-main':
                $this->load->library('Datatable');
                $table = new Datatable();

                if (in_array('wrapper', $param)) {
                    $this->load->model('itemtype_model');
                    $data = $this->itemtype_model->options_filter();
                    $table->innerFilter = array(
                        array('itemtype_id', 'Item Type', 'list', $data),
                        array('item_name', 'Item Name', 'text'),
                    );
                    $table->dataFilter = $table->innerFilter;

                    $table->id          = 'table-stok';
                    $table->isScrollable= false;
                    $table->numbering   = true;
                    $table->header      = array('ITEM', 'TYPE', 'STORE', 'STOK');
                    $table->source      = site_url($this->class_name.'/getform/table-main/');
                    return $table->generateWrapper();
                } else {
                    $table
                        ->setModel($this->item_stok_model)
                        ->setNumbering()
                        ->with(array('store','item'))
                        ->select('item_name, itemtype_id, nama_cabang, stok_total')
                        ->order_by('nama_cabang')
                        ->order_by('itemtype_id')
                        ->order_by('item_name')
                        ->edit_column('itemtype_id','$1','transaksi::getItemType(itemtype_id)');
                    echo $table->generate();
                }
                break;
            case 'other':
                $this->load->library('Datatable');
                $table = new Datatable();

                $data = $this->mesin_model->options_filter();                
                if (in_array('wrapper', $param)) {
                    $table->numbering  = true;
                    $table->id         = 'table-coin-kuras';
                    $table->isScrollable= false;


                    $filter = array(
                        array('id_mesin', 'Machine ID', 'list', $data),
                        array('num_coin', 'Number Coin', 'text'),
                        array('rec_created', 'Date', 'date'),
                        array('nama_pegawai', 'Employee', 'text'),
                        array('nama_cabang', 'Store Name', 'text'),

                    );


                    $table->dataFilter = $filter;


                    $table->header     = array('MACHINE CODE','MACHINE TYPE','SUM', 'DATE', 'EMPLOYEE' ,'STORE');
                    $table->source      = site_url($this->class_name.'/getform/other/');
                    return $table->generateWrapper();

                } else {
                    $table
                        ->setModel($this->transaction_model)
                        ->setNumbering()
                        ->with(array('transaction_machine','transaction_store','machine','cabang','pegawai', 'jenis_mesin'))
                        ->where(array('transactiontype_id' => 2))
                        ->select('kd_mesin, nama_jenis_mesin, num_coin, DATE(rec_created) as rec_created, nama_pegawai ,nama_cabang')
                        ->order_by('rec_created desc');
                        //->edit_column('rec_created', '$1', 'date::longDate(rec_created)');
                        //->edit_column('shipping_id', '$1', 'view::btn_group_receiving_warehouse(shipping_id)');
                    echo $table->generate();
                }

                break; 
            
            default:
                # code...
                break;
        }
    }


}
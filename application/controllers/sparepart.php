<?php

/**
 * Description of bbm
 *
 * @author SANGGRA HANDI
 */
class sparepart extends My_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array('sparepart_dua_model'));
    }

    public function index() {
        $data['title'] = 'Master Sparepart';

        /* INSERT LOG */
        //$this->access_right->activity_logs('view',$data['title']);
        /* END OF INSERT LOG */

        $data['button_group'] = $this->getAvailableButtons();
        //$data['button_right'] = $this->getTools();
        $data['button_right'] = $this->getPrintTools();  
        $data['table']['main']  = $this->table_main(array('wrapper', 'filter'));
        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'base/page_content.tpl';
    
        $this->twig->display('index.tpl', $data);
    }


    public function index__() {
        $data['title'] = 'Master Sparepart';
        $data['data_source'] = base_url($this->class_name . '/load');

        /* INSERT LOG */
        $this->access_right->activity_logs('view',$data['title']);
        /* END OF INSERT LOG */
        $data['button_group'] = $this->getAvailableButtons();
        $data['button_right'] = $this->getTools();

        
        $data['assets_url'] = $this->config->item('assets_url');
        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'base/page_content.tpl';
        $data['nama_pegawai'] = $this->currentUsername;

        $this->twig->display('index.tpl', $data);
    }

    public function load($page = 0) {
        $this->load->library("custom_table");

        $table = new stdClass();
        $header[0] = array(
                            "SPAREPART CODE",1,1,
                            "CODE EXIST",1,1,
                            "SPAREPART NAME", 1, 1,
                            "PRICE", 1, 1
                        );

        if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
            $header[0] = array(
                            "SPAREPART CODE",1,1,
                            "CODE EXIST",1,1,
                            "SPAREPART NAME", 1, 1,
                            "PRICE", 1, 1,
                            "ACTION",1,1
                            );
        }

        $table->header = $header;
        $table->id     = 't_bbm';
        $table->align = array('tanggal_bbm' => 'center', 'kd_bbm' => 'center','kd_purchase_order'=>'center','supplier'=>'center', 'aksi' => 'center');
        $table->style = "table table-striped table-bordered table-hover datatable dataTable";
        $table->model = "sparepart_model->data_table";
        $table->limit = $this->limit;
        $table->page = $page;
        $data = $this->custom_table->generate_ajax($table);
        echo json_encode($data);
    }

    public function add($id = '', $status_delete=0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->load->model('sparepart_model');

            $title = 'Tambah Data';
            $data['form_action'] = $this->class_name . '/proses';

            $data['id'] = $id;
            if ($id != '') {
                $title = 'Edit Data';
                $row = $this->sparepart_model->get_by_id($id)->row();
                $data['data'] = $row;
            }else{
                $data['data']['item_key'] = code::getCode('sku_md');
                $data['data']['kd_sparepart'] = code::getCode('sku_md');
            }

            $data['title']      = $title;
            $data['assets_url'] = $this->config->item('assets_url');
            $data['sidebar']    = $this->access_right->menu();
            $data['form']       = $this->class_name.'/form_sparepart.tpl';

            $button_group   = array();
            $button_group[] = view::button_back();

            if($status_delete == 0){
                $data['form_action']    = view::form_input($id);
                $button_group[]         = view::button_save();
            }else{
                $data['title']          = 'Delete Store';
                $data['form_action']    = view::form_delete($id);;
                $data['readonly']       = 'readonly=""';
                $button_group[]         = view::button_delete_confirm();
            }

            $data['button_group'] = view::render_button_group($button_group, array('class'=>'pull-right'));            
            $this->twig->display('base/page_form.tpl', $data);

        } else {
            $this->access_right->redirect();
        }
    }

    public function edit($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('edit', true);
        $this->add($id);
    }

    public function delete($id_enc = '') {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('delete', true);
        $this->add($id,$status_delete = 1);
    }
    public function proses() {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('kd_sparepart', 'Code Sparepart', 'required|trim');
            $this->form_validation->set_rules('nama_sparepart', 'Nama sparepart', 'required|trim');
            $id = $this->input->post('id');
             if ($id == '') {
                    $this->form_validation->set_rules('kd_sparepart', 'Code Sparepart', 'is_unique[m_sparepart.item_key]');
                    $this->form_validation->set_rules('nama_sparepart', 'Sparepart Name', 'is_unique[m_sparepart.nama_sparepart]');
                } else {
                    $this->form_validation->set_rules('kd_sparepart', 'Code Sparepart', 'edit_unique[m_sparepart.item_key.'.$id.'.id_sparepart]');
                    $this->form_validation->set_rules('nama_sparepart', 'Sparepart Name', 'edit_unique[m_sparepart.nama_sparepart.'.$id.'.id_sparepart]');
                }

            if ($this->form_validation->run()) {
                $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');

                $id = $this->input->post('id');

                $data = array(
                    'kd_sparepart' => $this->input->post('kd_sparepart'),
                    'nama_sparepart' => $this->input->post('nama_sparepart'),
                    'harga' => $this->input->post('harga'),
                    'item_key' => $this->input->post('kd_sparepart'),
                    'code_exist' => $this->input->post('code_exist'),
                );

                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */

                if ($id == '') {
                    if ($this->sparepart_model->create($data)) {
                        $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh_filter()');
						/* INSERT LOG */
						$this->access_right->activity_logs('add','Tambah Sparepart');
						/* END OF INSERT LOG */
                        $riseCode = code::getCodeAndRaise('sku_md');
                    }
                } else {
                    if ($this->sparepart_model->update($data, $id)) {
                        $message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', 'refresh_filter()');
						/* INSERT LOG */
						$this->access_right->activity_logs('edit','Edit Sparepart');
						/* END OF INSERT LOG */
                    }
                }
            } else {
                $message = array(false, 'Terjadi Kesalahan', validation_errors(), '');
            }
            echo json_encode($message);
        } else {
            $this->access_right->redirect();
        }
    }

    public function proses_delete() {
		$id = $this->input->post("id");
        $this->access_right->otoritas('delete', true);
        if ($this->access_right->otoritas('delete', true)) {
            $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');
            if ($this->sparepart_model->delete($id)) {
                $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', 'refresh_filter()');
				/* INSERT LOG */
				$this->access_right->activity_logs('delete','Delete BBM');
				/* END OF INSERT LOG */
            }
            echo json_encode($message);
        }
    }


    public function table_main($option = array()){
        $this->load->library('Datatable');
        $table = $this->datatable;
        if (in_array('filter', $option)) {
            $table->dataFilter = array(
                                array('nama_sparepart', 'SPAREPART NAME', 'text'),
                                array('this.harga', 'PRICE', 'text'),
                                array('this.item_key','SPAREPART CODE','text'),
                                array('this.code_exist','Code Exist','text'),
                                
                            );
        }   

        if (in_array('wrapper', $option)) {
            $table->numbering  = true;
            $table->id         = 'table-shipping';
            $table->isScrollable= false;
            $table->header     = array('SPAREPART CODE','CODE EXIST', 'SPAREPART NAME', 'PRICE', 'ACTION');
            $table->source     = site_url($this->class_name.'/table_main');
            return $table->generateWrapper();
        } else {
            $table
                ->setModel($this->sparepart_dua_model)
                ->setNumbering()
                ->select('item_key,code_exist, nama_sparepart , harga, id_sparepart')
                ->order_by('id_sparepart')
//                ->edit_column('id_sparepart', '$1', 'view::btn_group_receiving_warehouse(shipping_id)')
                ->edit_column('id_sparepart', '$1', 'view::btn_group_edit_delete_sparepart(id_sparepart)');
            echo $table->generate();
        }

    }


    public function barcode($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('edit', true);
        $this->showbarcode($id);
    }

    public function barcodeall($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('edit', true);
        $this->showbarcodeall($id);
    }

    public function getPrintTools(){
        $button_group = array();
        if($this->access_right->otoritas('print')){
            $button_group[] = view::button_all_barcode(1,1,"Sparepart");
        }
        
        if(true){//export
            //$button_group[] = view::button_export();
        }
        return view::render_button_group_laporan_payout($button_group, array(), true);

    }


    public function showbarcode($id='',$status_delete = 0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->config->load('twig');
            
            $title = 'Tambah Data';
            $data['form_action'] = $this->class_name . '/proses';

            $data['id'] = $id;
            if($id){
                $title = 'Sparepart Barcode';
                //$row            = $this->hadiah_model->getById($id)->row();
                $row        =   $this->sparepart_dua_model
                                ->where('id_sparepart', $id)
                                ->get()
                                ->row();
                $data['data']  = $row;
            }

            $data['title']      = $title;
            $data['assets_url'] = $this->config->item('assets_url');
            $data['sidebar']    = $this->access_right->menu();
            $data['form']       = $this->class_name.'/form_barcode.tpl';

            $button_group   = array();
            $button_group[] = view::button_back();
            
            if($status_delete == 0){
                $data['form_action']    = view::form_input($id);
                //INI BUTTON UNTUK PRINT BARCODE SPAREPART
                $button_group[]         = view::button_print_sparepart();
            }else{
                $data['title']          = 'Delete Merchandise';
                $data['form_action']    = view::form_delete($id);;
                $data['readonly']       = 'readonly=""';
                $button_group[]         = view::button_delete_confirm();
            }

            $data['button_group'] = view::render_button_group($button_group);
            //$data['options_hadiah'] = form_dropdown('data[hadiahtype_id]', $this->hadiah_type_model->options(), $row->hadiahtype_id, 'class="bs-select" disabled="disabled"');
            
            //$options_pulau = $this->pulau_model->options();
            //$data['options_pulau'] = form_dropdown('id_pulau',$options_pulau, !empty($def_id_pulau) ? $def_id_pulau : '', 'class = "form-control"'); 
            
            $this->twig->display('base/page_form.tpl', $data);

        } else {
            $this->access_right->redirect();
        }
    }

    public function showbarcodeall($id='',$status_delete = 0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->config->load('twig');
            
            $title = 'Print All Sparepart Barcode';
            $data['form_action'] = $this->class_name . '/proses';

            $data['id'] = $id;
            if($id){
                $title = 'Sparepart Barcode';
                //$row            = $this->hadiah_model->getById($id)->row();
                $row        =   $this->sparepart_dua_model
                                ->where('id_sparepart', $id)
                                ->get()
                                ->row();
                $data['data']  = $row;
            }

            $data['title']      = $title;
            $data['assets_url'] = $this->config->item('assets_url');
            $data['sidebar']    = $this->access_right->menu();
            $data['form']       = $this->class_name.'/form_barcode_all.tpl';

            $button_group   = array();
            $button_group[] = view::button_back();
            
            if($status_delete == 0){
                $data['form_action']    = view::form_input($id);
                //INI BUTTON UNTUK PRINT BARCODE SPAREPART
                $button_group[]         = view::button_print_all_sparepart();
            }else{
                $data['title']          = 'Delete Merchandise';
                $data['form_action']    = view::form_delete($id);;
                $data['readonly']       = 'readonly=""';
                $button_group[]         = view::button_delete_confirm();
            }

            $data['button_group'] = view::render_button_group($button_group);
            //$data['options_hadiah'] = form_dropdown('data[hadiahtype_id]', $this->hadiah_type_model->options(), $row->hadiahtype_id, 'class="bs-select" disabled="disabled"');
            
            //$options_pulau = $this->pulau_model->options();
            //$data['options_pulau'] = form_dropdown('id_pulau',$options_pulau, !empty($def_id_pulau) ? $def_id_pulau : '', 'class = "form-control"'); 
            
            $this->twig->display('base/page_form.tpl', $data);

        } else {
            $this->access_right->redirect();
        }
    }


    public function initPrinting(){
        $this->load->helper('printingdua');

        $ci =& get_instance();

        $nc = $ci->input->post('nc');

        $kd_sparepart = $ci->input->post('kd_sparepart');
        $nama_sparepart = $ci->input->post('nama_sparepart');

        $arrname = $this->cutName($nama_sparepart, 20);

        $cmd="";
        $cmd .= "^XA
                ^MMA
                ^PW320
                ^LL0240
                ^LS0
                ^BY3,3,71^FT38,104^BCN,,Y,N
                ^FD>;".$kd_sparepart."^FS
                ^FT39,162^A0N,23,24^FH\^FD".$arrname[0]."^FS";
        
        switch (count($arrname)) {
            case 2:
                $cmd .= "^FT39,196^A0N,23,24^FH\^FD".$arrname[1]."^FS";
                break;
            
            case 3:
                $cmd .= "^FT39,196^A0N,23,24^FH\^FD".$arrname[1]."^FS";
                $cmd .="^FT39,229^A0N,23,24^FH\^FD".$arrname[2]."^FS";
                break;

            default:
                # code...
                break;
        }
        $cmd .= "~JSB
                ^PQ".$nc.",0,1,Y^XZ";


/*            $cmd .= "^XA
                ^MMA
                ^PW609
                ^LL0609
                ^LS0
                ^BY4,3,160^FT135,284^BCN,,Y,N
                ^FD>;".$kd_sparepart."^FS
                ^FT219,386^A0N,28,28^FH\^FD".$nama_sparepart."^FS
                ^PQ1,0,1,Y^XZ";
*/        


        
        $form = '<form id="myForm" action="">

    <input type="hidden" id="sid" name="sid" value="'.session_id().'" />
        <fieldset hidden>
            <legend>Client Printer Settings</legend>
            
            <div hidden>
                I want to:&nbsp;&nbsp;
                <select id="pid" name="pid">
                  <option value="0">Use Default Printer</option>
                  <option value="1" selected="selected">Display a Printer dialog</option>
                  <option value="2">Use an installed Printer</option>
                  <option value="3">Use an IP/Etherner Printer</option>
                  <option value="4">Use a LPT port</option>
                  <option value="5">Use a RS232 (COM) port</option>
                </select>
                <br />
                <br />
                <div id="info" class="alert alert-info" style="font-size:11px;"></div>                
                <br />
            </div>
            
            <div id="installedPrinter" hidden>
                <div id="loadPrinters" name="loadPrinters">
                WebClientPrint can detect the installed printers in your machine. <a onclick="javascript:jsWebClientPrint.getPrinters();" class="btn btn-success">Load installed printers...</a>
                </div>
                <label for="installedPrinterName">Select an installed Printer:</label>
                <select name="installedPrinterName" id="installedPrinterName"></select>

            

            </div>           
                        
        </fieldset>
        <fieldset hidden>
            <legend>Printer Commands</legend>
            
            <p>
                Enter the printers commands you want to send and is supported by the specified printer (ESC/P, PCL, ZPL, EPL, DPL, IPL, EZPL, etc). 
                <br /><br />
                <b>NOTE:</b> You can use the <b>hex notation for non-printable characters</b> e.g. for Carriage Return (ASCII Hex 0D) you can specify 0x0D
                
            </p>
            <br /><br />
            <div class="alert alert-info" style="font-size:11px;">
            <b>Upload Files</b><br />
            This online demo does not allow you to upload files. So, if you have a file containing the printer commands like a PRN file, Postscript, PCL, ZPL, etc, then we recommend you to <a href="http://www.neodynamic.com/products/printing/raw-data/php/download/" target="_blank">download WebClientPrint</a> and test it by using the sample source code included in the package. Feel free to <a href="http://www.neodynamic.com/support" target="_blank">contact our Tech Support</a> for further assistance, help, doubts or questions.             
            </div>            
        </fieldset>        
        <fieldset hidden>
            <legend hidden>Ready to print!</legend>
            <h3>Your settings were saved! Now its time to <a href="#" onclick="javascript:doClientPrint();" class="btn btn-large btn-success">Print</a></h3>           
        </fieldset>

        <textarea id="printerCommands" name="printerCommands" rows="10" cols="80" class="span9" hidden>'.$cmd.'</textarea>
    </form>';
    echo $form;

    }


    public function printAllBarcode(){
        $this->load->helper('printingdua');

        $ci =& get_instance();

        $dataall = $this->sparepart_model
                    ->select('kd_sparepart, nama_sparepart')
                    ->get()
                    ->result_array();
        $cmd = "";
        foreach ($dataall as $key => $value) {
            $arrname = $this->cutName($dataall[$key]["nama_sparepart"], 20);
            $cmd .= "^XA
                    ^MMA
                    ^PW320
                    ^LL0240
                    ^LS0
                    ^BY3,3,71^FT38,104^BCN,,Y,N
                    ^FD>;".$dataall[$key]["kd_sparepart"]."^FS
                    ^FT39,162^A0N,23,24^FH\^FD".$arrname[0]."^FS";
            
            switch (count($arrname)) {
                case 2:
                    $cmd .= "^FT39,196^A0N,23,24^FH\^FD".$arrname[1]."^FS";
                    break;
                
                case 3:
                    $cmd .= "^FT39,196^A0N,23,24^FH\^FD".$arrname[1]."^FS";
                    $cmd .="^FT39,229^A0N,23,24^FH\^FD".$arrname[2]."^FS";
                    break;

                default:
                    # code...
                    break;
            }
            $cmd .= "^PQ1,0,1,Y^XZ";
            
        }



/*            $cmd .= "^XA
                ^MMA
                ^PW609
                ^LL0609
                ^LS0
                ^BY4,3,160^FT135,284^BCN,,Y,N
                ^FD>;".$kd_sparepart."^FS
                ^FT219,386^A0N,28,28^FH\^FD".$nama_sparepart."^FS
                ^PQ1,0,1,Y^XZ";
*/        
        


        
        $form = '<form id="myForm" action="">

    <input type="hidden" id="sid" name="sid" value="'.session_id().'" />
        <fieldset hidden>
            <legend>Client Printer Settings</legend>
            
            <div hidden>
                I want to:&nbsp;&nbsp;
                <select id="pid" name="pid">
                  <option value="0">Use Default Printer</option>
                  <option value="1" selected="selected">Display a Printer dialog</option>
                  <option value="2">Use an installed Printer</option>
                  <option value="3">Use an IP/Etherner Printer</option>
                  <option value="4">Use a LPT port</option>
                  <option value="5">Use a RS232 (COM) port</option>
                </select>
                <br />
                <br />
                <div id="info" class="alert alert-info" style="font-size:11px;"></div>                
                <br />
            </div>
            
            <div id="installedPrinter" hidden>
                <div id="loadPrinters" name="loadPrinters">
                WebClientPrint can detect the installed printers in your machine. <a onclick="javascript:jsWebClientPrint.getPrinters();" class="btn btn-success">Load installed printers...</a>
                </div>
                <label for="installedPrinterName">Select an installed Printer:</label>
                <select name="installedPrinterName" id="installedPrinterName"></select>

            

            </div>           
                        
        </fieldset>
        <fieldset hidden>
            <legend>Printer Commands</legend>
            
            <p>
                Enter the printers commands you want to send and is supported by the specified printer (ESC/P, PCL, ZPL, EPL, DPL, IPL, EZPL, etc). 
                <br /><br />
                <b>NOTE:</b> You can use the <b>hex notation for non-printable characters</b> e.g. for Carriage Return (ASCII Hex 0D) you can specify 0x0D
                
            </p>
            <br /><br />
            <div class="alert alert-info" style="font-size:11px;">
            <b>Upload Files</b><br />
            This online demo does not allow you to upload files. So, if you have a file containing the printer commands like a PRN file, Postscript, PCL, ZPL, etc, then we recommend you to <a href="http://www.neodynamic.com/products/printing/raw-data/php/download/" target="_blank">download WebClientPrint</a> and test it by using the sample source code included in the package. Feel free to <a href="http://www.neodynamic.com/support" target="_blank">contact our Tech Support</a> for further assistance, help, doubts or questions.             
            </div>            
        </fieldset>        
        <fieldset hidden>
            <legend hidden>Ready to print!</legend>
            <h3>Your settings were saved! Now its time to <a href="#" onclick="javascript:doClientPrint();" class="btn btn-large btn-success">Print</a></h3>           
        </fieldset>

        <textarea id="printerCommands" name="printerCommands" rows="10" cols="80" class="span9" hidden>'.$cmd.'</textarea>
    </form>';
    echo $form;

    }



    function cutName($name='', $maxlength=0){
        //$name = 'CHOCO MANIA BISKUIT 21GR72PC';
        $arrname=explode(' ',$name);
        
        $arrresult = array();
        $i=0;
        $j=0;
        $temp = '';
        while($i<count($arrname)){
            $temp .= $arrname[$i];            
            if(strlen($arrresult[$j])<$maxlength && strlen($temp)<$maxlength){
                if(strlen($arrresult[$j])==0){
                    $arrresult[$j] .= $arrname[$i];                    
                }else{
                    $arrresult[$j] .= ' '.$arrname[$i];
                }
            }else{
                $temp = '';
                $j++;
                $i--;
            }
            $i++;
        }
        return $arrresult;
    }


}

/* End of file bbm.php */
/* Location: ./application/controllers/bbm.php */

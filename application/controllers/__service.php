<?php

class service extends MY_Controller {

    public function __construct() {
        // Declaration
        parent::__construct();
    }


    
    public function index() {

        $data['title']       = 'Service Request';

        $button_group[] = view::button_add();
        $data['button_group'] = view::render_button_group($button_group);
        $data['button_right'] = $this->getTools();
        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'base/page_content.tpl';

        $this->load->library('Datatable');
        $table = $this->datatable;
        $table->numbering  = true;
        $table->id         = 'table-service';
        $table->header     = array('MACHINE', 'STORE', 'REQUEST DATE', 'STATUS', 'PROBLEM', 'ACTION');
        $table->source     = site_url($this->class_name.'/table_main');
        $data['table']['main'] = $table->generateWrapper();
    
        $this->twig->display('index.tpl', $data);
    }

    public function table_main(){
        $this->load->library('Datatable');
        $table = $this->datatable;

        $table  
                ->setModel($this->service_model)
                ->setNumbering()
                ->selectFromIndex()
                ->select('*, this.transactionservice_id as service_id')
                ->with(array('machine', 'store', 'transaction', 'repair'));

        $data = $table->getData();
        //print_r($this->db->last_query());print_r($data);die;
        // edit cara pertama. column yang dipilih dari avalibility index di array dengan 
        $rows = array();
        foreach ($data as $key => $value){

            // button
            $btn    = array();
            $btn[]  = view::button_edit($value['service_id']);
            if($value['service_type']==2)
                $btn[] = view::button_act($value['service_id']);
            $btn[]  = view::button_repair($value['service_id']);

            if($value['transactionrepair_id'])
                $btn[]  = view::button_sparepart($value['service_id']);

            // data
            $temp = array();
            $temp['serial_number']      = $value['serial_number'];
            $temp['nama_cabang']        = $value['nama_cabang'];
            $temp['rec_created']        = $value['rec_created'];
            $temp['service_status']     = transaksi::getServiceStatus($value['service_status']);
            $temp['issue_description']  = $value['issue_description'];
            $temp['action']             = view::render_button_group($btn);

            $rows[] = $temp;
        }

        $table->setData($rows);

        echo  $table->generate();
    }

    public function table_sparepart($id){
        echo $this->getForm('table-sparepart', $id, array('no_wrapper'=>true));
    }

    public function form($act = 'new', $id = '', $param = array()){
        $data['form'] = $this->getForm($act, $id, $param);
        $this->twig->display('base/page_form_part.tpl', $data);
    }

    public function add() {
        echo $this->getForm('new');
    }

    public function edit($id) {
        $this->access_right->otoritas('edit', true);
        echo $this->getForm('edit',$id); 
    }

    public function edit_sparepart($id){
        echo $this->getForm('form-sparepart',NULL, array('id'=>$id));
    }

    public function view($id_enc){
        $id = url_base64_decode($id_enc);
        $this->form('view',$id);
    }

    public function delete($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('delete', true);
        $this->form('delete',$id);
    }

    public function assign($id){
        echo $this->getForm('assign',$id);
    }

    public function repair($id){
        echo $this->getForm('repair',$id);
    }

    public function sparepart($id){
        echo $this->getForm('sparepart',$id);
    }

    public function proses() {
        
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
                
            $this->load->model(array(
                'transaction_model','transaction_document_model',
                'transaction_store_model', 'transaction_machine_model',
                'service_model'
            ));

            // inisialisasi
            $this->db->trans_start();
            $user_id = $this->session->userdata('user_id');
            $store_id= $this->session->userdata('store_id');
            $id             = $this->input->post('id');
            $data           = $this->input->post('data');
            $doc_number     = $this->input->post('doc_number');

            if($id){
                // edit
                $this->service_model->update($data, $id);

            }else{
                // insert to transaction table
                $trans['transactiontype_id']    = transactiontype::SREQ;
                $trans['rec_created']           = date::now_sql();
                $trans['rec_user']              = $user_id;
                $this->transaction_model->create($trans);//---------insert 1
                $transaction_id = $this->db->insert_id();

                // insert to transaction_store table
                $store['store_id']          = $store_id;
                $store['transaction_id']    = $transaction_id;
                $this->transaction_store_model->create($store);//----------------insert 2

                // insert to transaction_document table
                $doc['doc_number']      = $doc_number;
                $doc['transaction_id']  = $transaction_id;
                $this->transaction_document_model->create($doc);//-------------------insert 3

                // insert to transaction_machine table
                $mcn['machine_id']      = $data['machine_id'];
                $mcn['transaction_id']  = $transaction_id;
                $this->transaction_machine_model->create($mcn);//--------------------insert 4

                // insert to transaction_service
                $data['transaction_id']  = $transaction_id;
                $data['store_id']        = $store_id;
                $this->service_model->create($data);//-------------------------insert 5

            }

            if($this->db->trans_status()){
                $message = array(
                            'success'=>true,
                            'message'=>'Save Success'
                            );
            }else{
                $message = array(
                            'success'=>false,
                            'message'=>'Database Error'
                            );
            }
            $this->db->trans_complete();

            
            echo json_encode($message);
        } else {
            $this->access_right->redirect();
        }
    }

    public function proses_repair(){
        $this->load->model(array(
            'transaction_model','transaction_document_model',
            'transaction_store_model', 'transaction_machine_model',
            'service_model', 'repair_model'
        ));

        // inisialisasi
        $this->db->trans_start();
        $user_id        = $this->session->userdata('user_id');
        $store_id       = $this->session->userdata('store_id');
        $service_id     = $this->input->post('transactionservice_id');
        $repair_id      = $this->input->post('transactionrepair_id');
        $data           = $this->input->post('data');
        $doc_number     = $this->input->post('doc_number');

        if($repair_id){
            // edit
            $this->repair_model->update($data, $transactionrepair_id);

        }else{

            $service = $this->service_model->getById($service_id)->row();

            // insert to transaction table
            $trans['transactiontype_id']    = transactiontype::SREP;
            $trans['rec_created']           = date::now_sql();
            $trans['rec_user']              = $user_id;
            $this->transaction_model->create($trans);
            $transaction_id = $this->db->insert_id();

            // insert to transaction_store table
            $store['store_id']          = $store_id;
            $store['transaction_id']    = $transaction_id;
            $this->transaction_store_model->create($store);

            // insert to transaction_document table
            $doc['doc_number']      = $doc_number;
            $doc['transaction_id']  = $transaction_id;
            $this->transaction_document_model->create($doc);

            // insert to transaction_machine table
            $mcn['machine_id']      = $service->machinde_id;
            $mcn['transaction_id']  = $transaction_id;
            $this->transaction_machine_model->create($mcn);

            // insert to transaction_repair
            $data['transaction_id']         = $transaction_id;
            $data['transactionservice_id']  = $service_id;
            $data['store_id']               = $store_id;
            $this->repair_model->create($data);

        }

        if($this->db->trans_status()){
            $message = array(
                        'success'=>true,
                        'message'=>'Save Success'
                        );
        }else{
            $message = array(
                        'success'=>false,
                        'message'=>'Database Error'
                        );
        }
        $this->db->trans_complete();
        
        echo json_encode($message);
    }

    public function proses_sparepart(){
        $this->db->trans_start();
        $this->load->model(array('repair_sparepart_model'));

        $id         = $this->input->post('id');
        $service_id = $this->input->post('transactionservice_id');
        $repair_id  = $this->input->post('transactionrepair_id');
        $data       = $this->input->post('data');

        if($id){
            // edit
            $this->repair_sparepart_model->update($data, $id);
        }else{
            // new
            $data['transactionservice_id'] = $service_id;
            $data['transactionrepair_id']  = $repair_id;
            $this->repair_sparepart_model->create($data);
        }

        if($this->db->trans_status()){
            $message = array(
                        'success'=>true,
                        'message'=>'Save Success',
                        //'callback' => 'loadNewCustomTable("#div-table-sparepart")',
                        );
        }else{
            $message = array(
                        'success'=>false,
                        'message'=>'Database Error'
                        );
        }
        $this->db->trans_complete();
        echo json_encode($message);
    }

    public function proses_delete_sparepart($id){
        if(!$id)
            $id = $this->input->post('id');
        if(!$id)
            return;

        $this->db->trans_start();
        $this->load->model('repair_sparepart_model');
        $this->repair_sparepart_model->delete($id);

        if($this->db->trans_status()){
            $message = array(
                        'success'=>true,
                        'message'=>'Save Success',
                        'callback' => 'loadNewCustomTable("#div-table-sparepart")',
                        );
        }else{
            $message = array(
                        'success'=>false,
                        'message'=>'Database Error'
                        );
        }
        $this->db->trans_complete();
        echo json_encode($message);
    }

    public function proses_delete() {
        $id = $this->input->post('id');
        $this->access_right->otoritas('delete', true);
        if ($this->access_right->otoritas('delete', true)) {
            $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');
            if ($this->service_model->delete($id)) {
                $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', 'refresh_filter()');
                /* INSERT LOG */
                $this->access_right->activity_logs('delete','Delete BBM');
                /* END OF INSERT LOG */
            }
            echo json_encode($message);
        }
    }

    public function getForm($act='new', $id='', $param = array()){
        $this->load->model(array('mesin_model', 'pegawai_model'));
        $this->load->library('custom_form');
        $form = new custom_form();

        $data = array();
        $data['act']    = $act;
        $data['id']     = $id;
        $data['form']       = $this->class_name.'/form.tpl';

        if ($id) {
            $row    = $this->service_model
                        ->with(array('transaction', 'document', 'store', 'machine', 'machine_type', 'repair'))
                        ->getById($id)->row();
        }

        switch ($act) {
            case $act == 'new' || $act=='edit' :
                $param['title']         = 'New Service Request';
                $param['form_action']   = view::form_input($id);
                $param['data']          = $row;
                $param['list']['service_type'] = form_dropdown('data[service_type]', array(1=>'Internal Repair', 2=>'Request to HO'), $row->service_type, 'class="form-control bs-select"');
                $param['list']['machine'] = form_dropdown(
                                            'data[machine_id]', 
                                            $this->mesin_model->options_serial(), 
                                            $row->machine_id, 
                                            'class="form-control select2" data-placeholder="Select Machine"'
                                        );

                $form->portlet      = true;
                $form->button_back  = true;
                $form->param        = $param;
                $form->url_form     = $this->class_name.'/form_request.tpl';
                return $form->generate();
                break;

            case 'repair':
                $this->load->model('status_repair_model');
                $param['title']             = 'REPARATION';
                $param['form_action']       = form_open(
                                                site_url($this->class_name.'/proses_repair'),
                                                '',
                                                array(
                                                    'transactionservice_id' => $id,
                                                    'transactionrepair_id' => $row->transactionrepair_id,
                                                )
                                            );
                $param['data']              = $row;
                $param['list']['repair_status'] = form_dropdown('data[repair_status]', $this->status_repair_model->options(), $row->repair_status, 'class="bs-select form-control"');

                $form->portlet      = true;
                $form->button_back  = true;
                $form->param        = $param;
                $form->url_form     = $this->class_name.'/form_repair.tpl';
                return $form->generate();
                break;

            case 'sparepart' :
                $paramForm['transactionservice_id'] = $row->transactionservice_id; 
                $paramForm['transactionrepair_id']  = $row->transactionrepair_id;

                $param['table']['sparepart']= $this->getForm('table-sparepart', $id);
                $param['form']['sparepart'] = $this->getForm('form-sparepart', NULL, $paramForm);

                $param['title']             = 'SPAREPART';
                $param['btn_close']         = true;
                $param['content_body']      = $this->class_name.'/page_sparepart.tpl';
                
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'form-sparepart':
                $this->load->model('item_model');

                if ($param['id']){
                    // id repair_sparepart
                    $this->load->model(array('repair_sparepart_model'));
                    $row = $this->repair_sparepart_model
                                    ->getById($param['id'])->row();
                }

                $hiddenValue = array(
                                        'id' => $param['id'],
                                        'transactionservice_id'=> $param['transactionservice_id'],
                                        'transactionrepair_id' => $param['transactionrepair_id'],
                                    );

                $form->form_open    = form_open(
                                            site_url($this->class_name.'/proses_sparepart'),
                                            '',
                                            $hiddenValue
                                        );
                $param['list']['sparepart'] = form_dropdown('data[item_id]', $this->item_model->options_sparepart(), $row->item_id,'class="form-control select2"');
                
                $form->data         = $row;
                $form->buttonInForm = true;
                $form->close_modal  = false;
                $form->param        = $param;
                $form->url_form     = $this->class_name.'/form_sparepart.tpl';
                return $form->generate();
                break;

            case 'table-sparepart':
                $this->load->model('repair_sparepart_model');
                $data = $this->repair_sparepart_model
                            ->with('item')
                            ->where('transactionservice_id', $id)
                            ->where('transactionrepair_id', $row->transactionrepair_id)
                            ->get()->result_array();
                

                foreach ($data as $key => $val){
                    $temp= array();
                    $btn = array();
                    $urlFormEdit    = site_url($this->class_name.'/edit_sparepart/'.$val['repairsparepart_id']);
                    $urlFormDelete  = site_url($this->class_name.'/proses_delete_sparepart/'.$val['repairsparepart_id']);
                    $btn[] = '<a class="btn grey btn-sm" onclick="btnLoadContent(this)" target="#wrapper-form-sparepart" data-source="'.$urlFormEdit.'" ><i class="fa fa-edit"></i></a>';
                    $btn[] = '<a class="btn btn-delete-confirm grey btn-sm" delete-id="'.$val['repairsparepart_id'].'" data-source="'.$urlFormDelete.'" ><i class="fa fa-remove"></i></a>';

                    $temp['item_name']  = $val['item_name'];
                    $temp['qty']        = $val['qty'];
                    $temp['action']     = view::render_button_group($btn);
                    $rows[] = $temp;
                }

                $this->load->library("custom_table");
                $table = new custom_table();
                $table->columns = array('SPAREPART', 'QTY', 'ACTION');
                $table->id      = 'table-sparepart';
                $table->data    = $rows;
                $table->data_source = site_url($this->class_name.'/table_sparepart/'.$id);
                
                if($param['no_wrapper'])
                    return $table->generate();
                else
                    return $table->generateWithWrapper();
                break;
            

            case 'delete':
                $data['title']          = 'Delete REPARATION DATA';
                $data['form_action']    = view::form_delete($id);;
                $data['readonly']       = ' readonly="" ';
                $button_group[]         = view::button_delete_confirm();
                break;

            case 'view':
                $data['title']          = 'VIEW REPARATION DATA';
                $data['form_action']    = view::form_delete($id);
                $data['readonly']       = ' readonly="" ';
                unset($button_group);
                break;

            case 'complete':
                $title['title'] = 'EDIT REPARATION DATA';
                $data['act']    = 'edit';
                $data['form']   = $this->getForm('edit',$id);
                $data['form_service'] = $this->getForm('view',$id);
                //$button_group[]     = view::button_save();
                return $this->twig->render($this->class_name.'/form_complete.tpl', $data);
                break;

            case 'assign':
                //$button_group[] = view::button_approve_request('', array('onclick' => ""));
                $param['title']             = 'ASSIGN REPAIR JOB';
                $param['data']              = $row;
                $param['list']['teknisi']   = form_dropdown('data[technician_id]', $this->pegawai_model->getOptions('teknisi'), $row->technician_id, 'class="form-control select2"');
                $param['form_action']       = view::form_input($id, array('id'=>'form-service'));

                $form->portlet      = true;
                $form->button_back  = true;
                $form->param        = $param;
                $form->url_form     = $this->class_name.'/form.tpl';
                return $form->generate();

                break;

            default:
                # code...
                break;
        }

        $data['button_group'] = view::render_button_group($button_group);
        return $this->twig->render($this->class_name.'/form.tpl', $data);
    }


}

/* End of file bbm.php */
/* Location: ./application/controllers/bbm.php */

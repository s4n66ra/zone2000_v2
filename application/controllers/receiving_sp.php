<?php

/**
 * Description of receiving_sp
 * modul receiving sparepart di store
 * @author SANGGRA HANDI
 */
class receiving_sp extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->button['add'] = view::button_add(array('onclick'=>"btnLoadNextPage(this)"));
    }

    public function testing(){
        echo code::getCode('pr',11);
    }

    public function index() {

        $data['title'] = 'OUTLET RECEIVING SPAREPART';
        /* INSERT LOG */
        $this->access_right->activity_logs('view','receiving outlet sparepart');

        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'base/page_content.tpl'; 
        
        $data['table']['main'] = $this->getForm('page-receiving', $id);

        //========================== sistem date -> Cancel ========================
            /*$data['st_date'] = 1;
            $data['target'] = base_url('replenish/insertToSession');
            $sistem_date = $this->session->userdata['sistem_date'];
            if(!empty($sistem_date)){
                $data['hid_date'] = $sistem_date;
                $data['style'] = "style=padding-top:6px;color:rgb(255,0,0);";
            }else{
                $data['hid_date'] = date('Y-m-d');
                $data['style'] = 'style="padding-top:6px; color: rgb(0, 0, 0);"';
            }*/
        //========================== end sistem date ========================

        $this->twig->display('index.tpl', $data);
    }

    public function table_main($options = array()){
        $this->load->library('Datatable');
        $table = $this->datatable;

        if (in_array('wrapper', $option)) {
            $table->numbering  = true;
            $table->id         = 'table-shipping';
            $table->isScrollable= false;
            $table->header     = array('DESTINATION', 'USER', 'DATE', 'ACTION');
            $table->source     = site_url($this->class_name.'/table_main');
            return $table->generateWrapper();
        } else {
            $table
                ->setModel($this->shipping_model)
                ->setNumbering()
                ->with(array('store','employee'))
                ->where('type', storetype::WAREHOUSE)
                ->select('nama_cabang, nama_pegawai , date_created, shipping_id')
                ->edit_column('date_created', '$1', 'date::longDate(date_created)')
                ->edit_column('shipping_id', '$1', 'view::btn_group_receiving_warehouse(shipping_id)');
            echo $table->generate();
        }
    }

    public function add() {
        $this->load->model('receiving_warehouse_model');
        /*$this->db->trans_start();
        $data['type'] = storetype::WAREHOUSE;
        $this->receiving_warehouse_model->create($data);*/
        //$id = $this->receiving_warehouse_model->insert_id;

        $this->detail($id);
    }

    public function detail($id = '') {
        echo $this->getForm('page-detail', $id);
    }

    public function add_do($id = 0){
        $ids = $this->input->post('ids');
        echo $this->getForm('form-koli', $ids[0], array('ids'=>$ids));
    }

    public function getForm($type = '', $id = NULL, $param = array()){
        $this->load->library(array('custom_form','custom_table'));
        $this->load->model(array('sequence_model','po_store_model','purchase_request_model','purchase_request_detail_model'));
        $form = $this->custom_form;

        switch ($type) {
            case 'page-receiving':
                $param['page']['create_koli']   = $this->getForm('table-do', $id, array('wrapper'));
                $param['page']['list_koli']     = $this->getForm('table-koli', $id, array('wrapper'));
                return $this->twig->render('receiving_sp/page_receiving.tpl', $param);
                break;

            case 'form-info' :
                $param['data']   = '';//$this->receiving_warehouse_model->with('store')->getById($id)->row();
                return $this->twig->render('receiving_sp/warehouse/form_info.tpl', $param);
                break;

            case 'page-koli':
                $param['page']['create_koli']   = $this->getForm('table-do', $id, array('wrapper'));
                $param['page']['list_koli']     = $this->getForm('table-koli', $id, array('wrapper'));
                return $this->twig->render('receiving_sp/page_receiving.tpl', $param);
                break;

            case 'form-koli' :
                $this->load->model('shipping_model');

                $shipping = $this->shipping_model->with(array('purchase_request_sparepart','store_sparepart'))->where('this.pr_id',$id)->get()->result();
                $shipping_id = $shipping[0]->shipping_id;             
                $shipping = $shipping[0];

                $param['title']         = 'Create Receiving Sparepart Outlet';
                $param['form_action']   = form_open(
                                                site_url($this->class_name.'/proses_receiving_store'),
                                                '', array(
                                                    'id' => $id,
                                                    'shipping_id'=>json_encode($shipping_id),
                                                    'pr_id' => json_encode($id)
                                                )
                                            );
                $param['data']          = $shipping;
                $param['date_arrived'] = date('Y-m-d');
                $modelShipping = $this->shipping_model;
                $dataShipping = $modelShipping
                    ->select('this.shipping_id,pr_code,item_name, qty_approved,prdetail_id')
                    ->with(array('purchase_request_sparepart','pr_detail_sparepart','item_sparepart'))
                    ->where('this.pr_id',$id)
                    ->get();

                $param['table']         = $dataShipping->result_array();
                //$param['table']         = array('koli_code'=>1,'id'=>2);
                $form->portlet          = true;
                $form->button_back      = true;
                $form->id               = 'form-receiving';
                $form->param            = $param;
                $form->url_form         = 'receiving_sp/store/form_receiving.tpl';
                $form_receiving = $form->generate();
                
                return $form_receiving;
                break;

            case 'table-koli':
                $this->load->library('Datatable');
                $table = $this->datatable;

                if (in_array('wrapper', $param)) {
                    $table->id          = 'table-koli-detail';
                    $table->isScrollable= false;
                    $table->checklist   = false;
                    $table->pagination  = true;
                    $table->header      = array('DATE RECEIVED','PP CODE','SKU', 'ITEM','APPROVED', 'RECEIVED');
                    $table->source      = site_url($this->class_name.'/getform/table-koli/'.$id);
                    $table->caption_btn_group   = NULL;//'RECEIVE';
                    $table->url_action_group    = NULL;//site_url($this->class_name.'/proses_receiving');
                    $table->callback_group_submit = NULL;
                    //$table->add_javascript =  '#table-koli-detail.submit()';
                    return $table->generateWrapper();
                } else {
                    $this->load->model('shipping_model');
                    $table
                        ->setModel($this->shipping_model)
                        ->with(array('purchase_request_sparepart','pr_detail_sparepart','item_sparepart'))
                        ->where('date_arrived IS NOT NULL',NULL)
                        ->where('pr_type',itemtype::ITEM_SPAREPART)
                        //->where('this.shipping_id',$id)
                        ->select('date_arrived,pr_code,item_key,item_name, qty_approved,qty_received,prdetail_id')
                        ->edit_column('date_arrived','$1','date::longDate(date_arrived)');
                        //->edit_column('podetail_id', '<input type="text" name="data[$1]" >','podetail_id');
                        //->edit_column('shipping_id', '<input type="checkbox" name="id[]" value="$1">','shipping_id');
                    echo $table->generate();
                }
                break;

            case 'table-do':
                $this->load->library('Datatable');
                $table = $this->datatable;

                if (in_array('wrapper', $param)) {
                    $table->id          = 'table-receiving-detail';
                    $table->isScrollable= false;
                    $table->checklist   = true;
                    $table->pagination  = false;
                    $table->header      = array('PP CODE','DATE CREATED','STORE NAME' ,'NOTE');
                    $table->source      = site_url($this->class_name.'/getform/table-do/'.$id);
                    $table->caption_btn_group   = 'Create Receiving';
                    $table->url_action_group    = site_url($this->class_name.'/add_do');
                    $table->callback_group_submit = '
                        var param = {
                            ids : grid.getSelectedRows()
                        };
                        console.log(param);
                        var url = "'.site_url($this->class_name.'/add_do/'.$id).'";
                        loadModal(url, param);

                    ';
                    return $table->generateWrapper();
                } else {
                    $this->load->model('shipping_model');
                    $table
                        ->setModel($this->purchase_request_model)
                        ->with(array('store'))
                        ->where('pr_status', prstatus::SHIP) // approve
                        ->where('pr_type',itemtype::ITEM_SPAREPART)
                        // harusnya tambah satu lagi receive_id <> null
                        ->select('pr_id,pr_code,date_created, nama_cabang, pr_note')
                        ->edit_column('pr_id', '<input type="checkbox" name="id[]" value="$1">','pr_id')
                        ;
                    echo $table->generate();
                }
                break;

            

            
            default:
                # code...
                break;
        }
    }
//=====
    public function proses_receiving_store(){
        $this->load->model(array('shipping_model','purchase_order_detail_model','purchase_request_detail_model','item_stok_model','purchase_order_model','purchase_request_model'));
        $modelShipping = $this->shipping_model;
        $modelPODetail = $this->purchase_order_detail_model;
        $modelPRDetail = $this->purchase_request_detail_model;
        $modelItemStok = $this->item_stok_model;
        $modelPO       = $this->purchase_order_model;
        $modelPR       = $this->purchase_request_model;


        $data = $this->input->post('data');
        $shipping_id = json_decode($this->input->post('shipping_id'));
        $date_arrived = $this->input->post('date_arrived');
        $pr_id = json_decode($this->input->post('pr_id'));

        $this->db->trans_start();

        $modelShipping 
            ->set('date_arrived',$date_arrived)
            ->where('shipping_id',$shipping_id)
            ->update();

        foreach ($data as $key => $value) {
            /*$modelPODetail
                ->set('qty_received',$value)
                ->set('date_received',$date_arrived)
                ->where('podetail_id',$key)
                ->update();*/
            $dataPR = $modelPRDetail ->select('prdetail_id,store_id,item_id')->with('pr')->where('prdetail_id',$key)->get()->row();
            $modelPRDetail 
                ->set('qty_received',$value)
                ->where('prdetail_id',$dataPR->prdetail_id)
                ->update();
            $whereCek = array('store_id'=>$dataPR->store_id,'item_id'=>$dataPR->item_id);
            $cekEksisStok = $modelItemStok->select('itemstok_id')->where($whereCek)->get()->num_rows();
            if($cekEksisStok>0){
                $modelItemStok
                    ->set("stok_gudang","stok_gudang+$value",FALSE)
                    ->set("stok_total","stok_total+$value",FALSE)
                    ->where($whereCek)->update();
            }else{
                $modelItemStok
                    ->set("stok_gudang","stok_gudang+$value",FALSE)
                    ->set("stok_total","stok_total+$value",FALSE)
                    ->set($whereCek)->insert();
            }

        }

        $modelPR
            ->set('pr_status',prstatus::RECEIVE)
            ->where('this.pr_id',$pr_id)
            ->update();
       // echo $this->db->last_query();

        $this->db->trans_complete();
        if($this->db->trans_status()){
            $message = array('success'=>true, 'message'=>'Save Success');
        }else{
            $message = array('success'=>false, 'message'=>$this->db->_error_message());
        }
        echo json_encode($message);
    }

    //===================================== end new===========================

    

    public function edit($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('edit', true);
        $this->add($id);
    }

    public function delete($id_enc) {        
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('delete', true);
        $this->add($id,1);
    }

    public function proses() {        
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('data[pr_type]', 'Purchase Request Type', 'required|trim');

            if ($this->form_validation->run()) {
                $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');
                
                $id = $this->input->post('id');
                $data = $this->input->post('data');

                if ($id == '') {
                    $this->db->trans_start();
                    $user_id = $this->session->userdata('user_id');
                    $store_id= $this->session->userdata('store_id');
                    $now = date::now_sql();

                    $this->load->model(array(
                        'transaction_model','transaction_store_model','transaction_document_model','sequence_model'
                    ));
                    // insert to transaction
                    $trans['transactiontype_id'] = transactiontype::PR;
                    $trans['rec_user'] = $user_id;
                    $trans['rec_created'] = $now;
                    $this->transaction_model->create($trans);
                    $transaction_id = $this->db->insert_id();

                    // insert to transaction store
                    $trans_store['transaction_id'] = $transaction_id;
                    $trans_store['store_id'] = $store_id;
                    $this->transaction_store_model->create($trans_store);

                    // insert to transaction document
                    // $trans_doc['transaction_id'] = $transaction_id;
                    // $trans_doc['doc_number'] = $this->input->post('doc_number');
                    // $this->transaction_document_model->create($trans_doc);

                    // insert to receiving
                    
                    // cek jika dibuat oleh HO
                    if(hprotection::isHeadOffice()){
                        $data['create_type']    = 1;
                        $data['store_id']       = $data['store_id'];
                        $data['created_by']     = $store_id;
                    }else{
                        $data['store_id']       = $store_id;
                        $data['created_by']     = $store_id;
                    }

                    $data['transaction_id'] = $transaction_id;
                    $data['user_id'] = $user_id;

                    // set doc.number / pr_code
                    $data['pr_code'] = $data['pr_code'];
                    $this->sequence_model->raiseSequence('pr');

                    $this->receiving_model->create($data);
                    $pr_id = $this->db->insert_id();

                    if($this->db->trans_status() === TRUE){
                        $message = array(
                            true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.',"loadNextPage(1,'".site_url($this->class_name."/detail/".$pr_id)."')"  
                        );
                    }else{
                        // Database Error
                        $message = array(false, 'Database Error Occured', $this->db->_error_message(), '');
                    }
                    $this->db->trans_complete();

                } else {
                    // EDIT
                    $this->load->model(array(
                        'transaction_model','transaction_store_model','transaction_document_model'
                    ));

                    $pr = $this->receiving_model->getById($id)->row();
                    if($pr){
                        $this->db->trans_start();
                        // Edit purchase Request
                        $this->receiving_model->update($data, $id);

                        // Edit Transaction Document Number
                        $trans_doc['doc_number'] = $this->input->post('doc_number');
                        $this->transaction_document_model->where('transaction_id', $pr->transaction_id)->update($trans_doc);
                        file_put_contents('parameter.txt', $this->db->last_query());

                        if($this->db->trans_status() === TRUE){
                            $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', 'refresh_filter()');
                        }else{
                            // Database Error
                            $message = array(false, 'Database Error Occured', $this->db->_error_message(), '');
                        }                            
                        $this->db->trans_complete();
                    }
                }
            } else {
                $message = array(false, 'Terjadi Kesalahan', validation_errors(), '');
            }
            echo json_encode($message);
        } else {
            $this->access_right->redirect();
        }
    }

    public function proses_delete() {
        $id = $this->input->post('id');
        $this->access_right->otoritas('delete', true);
        if ($this->access_right->otoritas('delete', true)) {
            $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '','id : '.$id);
            if ($this->receiving_model->delete($id)) {
                $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', 'refresh_filter()');
                /* INSERT LOG */
                $this->access_right->activity_logs('delete','Delete pr_hadiah');
                /* END OF INSERT LOG */
            }
            echo json_encode($message);
        }
    }

    public function confirm($act, $id){
        $button_group   = array();
        $button_group[] = view::button_back();
        $button_group[] = view::button_confirm();

        switch ($act) {
            

            case 'send':
                $data['title'] = 'SEND REQUEST';
                $data['status'] = 1;
                break;
            
            case 'revisi':
                $data['title'] = 'REVISI REQUEST';
                $data['status'] = 2;
                break;

            case 'reject':
                $data['title'] = 'REJECT REQUEST';
                $data['status'] = 4;
                break;

            case 'approve':
                $data['title'] = 'APPROVE REQUEST';
                $data['status'] = 5;
                break;

            case 'cancel':
                $data['title'] = 'CANCEL REQUEST';
                $data['status'] = 9;
                break;

            default:
                # code...
                break;
        }
        $data['hide_kd']        = true;
        $data['button_group']   = view::render_button_group($button_group);
        $data['form_action']    = view::form($id,$this->class_name.'/confirm_proses');
        $data['form'] = $this->class_name.'/form.tpl';
        $this->twig->display('base/page_form.tpl', $data);

    }

    public function proses_request($id){
        $message = array('success'=>false, 'message'=>'No Purchase Request Selected');

        if (!$id) {
            echo json_encode($message);return;
        }

        $this->db->trans_start();
        $this->receiving_model
            ->set('pr_status', transactionstatus::WAITING_REVIEW)
            ->set('date_request', date::now_sql())
            ->update();

        if($this->db->trans_status()){
            $message = array(
                'success'=>true, 'message'=>'Save Success',
                'callback'=>'reloadPageLoading()',
            );
        }else{
            $message['message'] = $this->db->_error_message();
        }
        $this->db->trans_complete();
        echo json_encode($message);
    }

    public function confirm_proses($prid = NULL){
        $id     = $this->input->post('id');
        $status = $this->input->post('status');
        $data   = $this->input->post('data');

        if(!$id) $id = $prid;

        //$this->receiving_model->update($data, $id);
        $result = $this->receiving_model->proceed($id, $status);
        
        // if($result['success']){        
        //     // refresh page content berguna untuk me-refresh page content dengan parameter url data-source, id_element, 
        //     // id-element default = page-content
        //     // data-source default= diambil dari attr data-source pada id element
        //     $message = array(true, 'Proses Berhasil', 'Proses Berhasil.', 'reloadPageLoading()');
        // }else{
        //     $message = array(false, 'Proses gagal', $result['message'], '','id : '.$id);    
        // }
        echo json_encode($result);
    }

    

    public function __detail($id){
        // id : id_pr_detail

        $data['title'] = 'PURCHASE REQEUST';
        $data['subtitle']= transaksi::getItemType($pr->pr_type);
        
        $data['data_source_page']   = base_url($this->class_name . '/detail/'.$id);
        $pr                         = $this->receiving_model
                                        ->with(array('document','store'))
                                        ->getById($id)->row();
        $data['data']               = $pr;
        

        // button bagian atas
        $btnGroup = array();
        if($pr->pr_status==prstatus::DRAFT) 
            $btnGroup[] = view::button_add_detail(array('id'=>$id));
        $data['button_group'] = view::render_button_group($btnGroup); 

        // button bagian bawah
        if ($pr->pr_status==prstatus::REVISI || $pr->pr_status==prstatus::DRAFT){
            $btngroup2[] = view::button_cancel_request($id);
            $btngroup2[] = view::button_send_request($id);
        }
        $data['button_group_2'] =view::render_button_group($btngroup2);

        // view keterangan status pr dan revisi pr
        $data['status_pr']  = view::render_status_pr($data['data']->pr_status);
        $data['revisi_pr']  = view::render_revisi_pr($data['data']->count_revisi);
        $data['page']['id'] = 'pr-detail';

        $data['table']['main']  = $this->table_detail($id, array('wrapper'));
        $data['readonly']       = "readonly";
        $data['isBtnPageBack']  = true;
        $data['form_info']      = 'receiving/form_info.tpl';

        if($this->input->is_ajax_request()){
            $this->twig->display('base/page_content.tpl',$data);
        }else{
            $data['sidebar'] = $this->access_right->menu();
            $data['content'] = 'base/page_content.tpl'; 
            $this->twig->display('index.tpl', $data);    
        }
        
    }

    public function table_detail($id_pr = '', $options = array()){
        $pr = $this->receiving_model->getById($id_pr)->row();
        $this->load->library("custom_table");
        $table = new custom_table();
        $table->headerAlign = array('left','left','left','right', 'right','right','right','right', 'center');
        $table->columns = array('SKU', 'ITEM', 'SUPPLIER', 'LAST STOK', 'REQUEST', 'APPROVED', 'PRICE', 'TOTAL');

        if ($pr->pr_status==prstatus::DRAFT)
            $table->columns[] = 'ACTION';

        $table->id      = 'table-pr-detail';
        $table->data_source = site_url($this->class_name.'/table_detail/'.$id_pr);
        $table->data    = $this->receiving_detail_model->getDataCustomTable($id_pr);
        $param['total'] = $this->receiving_detail_model->total;
        $table->footer  = $this->twig->render('base/table/footer_total.tpl', $param);
        
        if(in_array('wrapper', $options))
            return $table->generateWithWrapper();
        else{
            echo $table->generate();
        }

    }

    public function add_detail($pr_id ='', $id='', $status_delete = 0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            
            // load custom form
            $this->load->library('custom_form');
            $form = new custom_form();

            // load model
            $this->load->model('receiving_detail_model');
            $this->load->model(array('item_model', 'supplier_model', 'item_supplier_model'));
            $data['id'] = $id;

            if ($id) {
                
                $row    = $this->receiving_detail_model->with('supplier')->getById($id)->row();
                $pr_id  = $row->pr_id;
                $param['title'] = 'EDIT ITEM';
                $param['data']  = $row;
            } else{
                // input new . set pr_id
                $param['title']         = 'NEW ITEM';
                $param['data']['pr_id'] = $pr_id;
            }

            if($pr_id){
                $pr             = $this->receiving_model->getById($pr_id)->row();
                $itemtype_id    = $pr->pr_type;
            }

            // DROPDOWN ITEM BERDASARKAN ITEMTYPE ID (mesin, merchandise, sparepart, dll)
            //$htmlOptions        = 'onChange="refresh_dropdown_options(this, \'#supplier_select\', \''.site_url("receiving/getSupplier").'\')" class="form-control select2" data-placeholder="Select '.transaksi::getPrType($itemtype_id).'" '. ($status_delete==1 ? 'disabled' : '');
            $htmlOptions        = 'onChange="refreshList(this)" target="#supplier_select" data-source="'.
                                site_url("receiving/getSupplier").'" class="form-control select2" data-placeholder="Select '.transaksi::getPrType($itemtype_id).'" '. ($status_delete==1 ? 'disabled' : '');

            $param['list_item']  = form_dropdown('data[item_id]', $this->item_model->options_empty($itemtype_id), $row->item_id, $htmlOptions);

            $htmlOptions        = 'id="supplier_select" class="form-control select2" data-placeholder="Select Supplier" '. ($status_delete==1 ? 'disabled' : '');
            $param['list_supplier']  = form_dropdown('data[supplier_id]', $this->item_supplier_model->options_empty($row->item_id), $row->supplier_id, $htmlOptions);

            if($status_delete == 0){
                $param['form_action']    = view::form_input_detail($id);
            }else{
                $param['title']          = 'DELETE ITEM';
                $param['form_action']    = view::form_delete_detail($id);
                $form->type = 'delete';
            }

            $form->portlet          = true;
            $form->button_back      = true;
            $form->id               = 'form-pr-detail-add';
            $form->param            = $param;
            $form->data_source      = site_url($this->class_name.'/table_detail/'.$pr_id);
            $form->url_form         = $this->class_name.'/form_detail.tpl';
            
            echo $form->generate();
        } else {
            $this->access_right->redirect();
        }
    }
    
    public function getSupplier($itemId){
    	$this->load->model("item_supplier_model");
    	$options = $this->item_supplier_model->options_empty($itemId);
    	
    	$result = "";
    	foreach ($options as $key => $value) {
    		$result .= "<option value='".$key."'>".$value."</option>";
    	}
    	echo $result;
    }

    public function edit_detail($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('edit', true);
        $this->add_detail('', $id);
    }

    public function delete_detail($pr_id, $id_enc) {        
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('delete', true);
        $this->add_detail($pr_id, $id, 1);
    }

    public function proses_detail() {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {

            $this->form_validation->set_rules('data[pr_id]', 'Purchase Request Doc', 'required|trim');
            $this->form_validation->set_rules('data[item_id]', 'Item', 'required|trim');
            $this->form_validation->set_rules('data[qty_temp]', 'Qty', 'required|trim');
            $this->form_validation->set_rules('data[supplier_id]', 'Supplier', 'required|trim');

            if ($this->form_validation->run()) {
                $message = array('success'=>false, 'message'=>'Proses penyimpanan data gagal.');

                $id     = $this->input->post('id');
                $data   = $this->input->post('data');
                $this->db->trans_start();
                $this->load->model(array('item_supplier_model', 'receiving_detail_model'));

                if (!$id) {
                    $item_id    = $data['item_id'];

                    $pr     = $this->receiving_model->getById($pr_id)->row();
                    $item   = $this->item_supplier_model
                            	->where('item_id', $item_id)
                            	->where('supplier_id', $data['supplier_id'])
                            	->get()->row();
                   
                    // insert to table transaction_detail
                    $detail['pr_id']            = $data['pr_id'];
                    $detail['transaction_id']   = $pr->transaction_id;
                    $detail['price']            = $item->price;
                    $detail['qty_request']      = $data['qty_temp'];
                    $detail['qty_temp']         = $data['qty_temp'];
                    $detail['item_id']          = $data['item_id'];
                    $detail['supplier_id']      = $data['supplier_id'];

                    $this->receiving_detail_model->create($detail);
                    
                    
                } else {
                    $detail['qty_request']  = $data['qty_temp'];
                    $detail['qty_temp']     = $data['qty_temp'];
                    $this->receiving_detail_model->update($detail, $id);
                }
                
                if($this->db->trans_status()){
                    $message = array('success'=>true, 'message'=>'Save Success', 'callback'=>'loadNewCustomTable("#div-table-pr-detail")');
                }else{
                    $message = array('success'=>false, 'message'=>'Database Error');
                }
                $this->db->trans_complete();
            } else {
                $message = array('success'=>false, 'message'=>validation_errors());
            }
            echo json_encode($message);
        } else {
            $this->access_right->redirect();
        }
    }

    public function proses_delete_detail() {
        $id = $this->input->post('id');

        if(!$id) {
            $message = array('success'=>false, 'message'=>'Id Required');
            echo json_encode($message);
            return;
        }

        $this->access_right->otoritas('delete', true);
        if ($this->access_right->otoritas('delete', true)) {

            $this->db->trans_start();
            $this->receiving_detail_model->delete($id);
            if($this->db->trans_status()){
                $message = array('success'=>true, 'message'=>'Save Success', 'callback'=>'loadNewCustomTable("#div-table-pr-detail")');
            }else{
                $message = array('success'=>false, 'message'=>'Database Error');
            }
            $this->db->trans_complete();
        }
        echo json_encode($message);
    }

    public function proceed($id = 0, $status = 0){
        /*
            memproses PR.
            dari draft - pengajuan
            dari revisi - pengajuan

        */
        //print_r($this->session->userdata('user'));
        //$this->output->enable_profiler(TRUE);

        // should be form validation

        // id_pr yang akan dirubah statusnya
        $return = $this->receiving_model->proceed($id, $status);
        return $return;
    }

    public function getCode($storeId = NULL){
        if (!$storeId)
            return;
        echo code::getCode('pr', $storeId);
    }

}
/* End of file bbm.php */
/* Location: ./application/controllers/bbm.php */




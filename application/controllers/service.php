<?php

class service extends MY_Controller {

    public function __construct() {
        // Declaration
        parent::__construct();
    }


    
    public function index() {

/*        $data['title'] = 'Master Supplier';        

        $data['button_group'] = $this->getAvailableButtons();
        $data['button_right'] = $this->getTools();

        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'base/page_content.tpl'; 
        $data['nama_pegawai'] = $this->currentUsername;    
        
        $data['table']['main'] = $this->table_main(array('wrapper', 'filter'));
        $this->twig->display('index.tpl', $data);*/


        //====================================

        $data['title']       = 'Service Request';

        $button_group[] = view::button_add();
        $data['button_group'] = view::render_button_group($button_group);
        //$data['button_right'] = $this->getTools();
        
        //$data['button_right'] = $this->getPrintTools();
        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'base/page_content.tpl';
        $data['nama_pegawai'] = $this->currentUsername;    
        $data['table']['main'] = $this->table_main(array('wrapper', 'filter'));
    
        $this->twig->display('index.tpl', $data);
    }

    public function table_main($option = array()){
        $this->load->library('Datatable');
        $table = $this->datatable;
        $table->startdate = true;
        $table->enddate = true;

        if (in_array('wrapper', $option)) {
            $table->id         = 'table-servcie';
            $table->numbering  = true;
            $table->isScrollable = false;
            $table->header     = array('REG MACHINE','SERVICE NO','MACHINE', 'STORE', 'REQUEST DATE','DUE DATE','LOCATION', 'STATUS', 'PROBLEM', 'ACTION');
            $table->source     = site_url($this->class_name.'/table_main');
            return $table->generateWrapper();
        } else 
            $table
                ->setModel($this->service_model)
                ->setNumbering()
                ->select('serial_number,doc_number,nama_jenis_mesin,nama_cabang,rec_created,due_date,loc_name,nama,issue_description, this.transactionservice_id as service_id')
                ->with(array('machine', 'store', 'transaction', 'repair','machine_type','document','v_detail','status_service'))
                ->edit_column('rec_created','$1','date::longDate(rec_created)')
                ->edit_column('due_date','$1','date::longDate(due_date)')
                ->edit_column('service_id', '$1', 'view::btn_group_service(service_id)');
            echo $table->generate();
    }

    public function update_table_main($startdate ='', $enddate = ''){
        $this->load->library('Datatable');
        $table = $this->datatable;
        $table->startdate = true;
        $table->enddate = true;

        $table
            ->setModel($this->service_model)
            ->setNumbering()
            ->select('serial_number,doc_number,nama_jenis_mesin,nama_cabang,rec_created,due_date,loc_name,nama,issue_description, this.transactionservice_id as service_id')
            ->with(array('machine', 'store', 'transaction', 'repair','machine_type','document','v_detail','status_service'))
            ->where('rec_created >=', $startdate)
            ->where('rec_created <=', $enddate)
            ->edit_column('rec_created','$1','date::longDate(rec_created)')
            ->edit_column('due_date','$1','date::longDate(due_date)')
            ->edit_column('service_id', '$1', 'view::btn_group_service(service_id)');
        echo $table->generate();

    }

    public function excel($startdate='', $enddate = ''){

        $this->load->model(array('store_model'));        
        $filter = array(
            'REG_MACHINE'           =>$bbm,
            'SERVICE_NO'          =>$po,
            'MACHINE'          =>$supplier,
            'STORE'      =>$tanggal_awal,
            'REQUEST_DATE'     =>$tanggal_akhir,
            'DUE_DATE'       =>$tahun_aktif,
            'LOCATION'    =>$tahun_aktif,
            'STATUS'    =>$tahun_aktif,
            'PROBLEM'    =>$tahun_aktif,
            );

        $data['filter'] = $filter;
        //----------------------------------------------------
        $date = date('Ymd His');
        $data['judul_kecil']    = 'Laporan Service Request';
        $data['filename']       = 'Laporan Payout('.$date.')';
        $data['content']        = $this->laporan_payout_model->data_detail(array("id_cabang" => $store_id));
        $data['selected_store'] = $this->store_model
                                 ->select('nama_cabang')
                                 ->where(array("id_cabang"=>$store_id))
                                 ->get()
                                 ->row();
        //print_r($data['selected_store']);
        $this->load->view('cetak_excel_payout',$data);
    
    }

    public function getPrintTools(){
        $button_group = array();
        if($this->access_right->otoritas('print')){
            $button_group[] = view::button_export_laporan_payout();
        }
        
        if(true){//export
            //$button_group[] = view::button_export();
        }
        return view::render_button_group_laporan_payout($button_group, array(), true);

    }




    public function detail($id){
        // id : id_pr_detail
        $this->load->model('purchase_request_model');
        $data['title'] = 'Detail Service';
        $data['subtitle']= transaksi::getItemType($pr->pr_type);
        
        $data['data_source_page']   = base_url($this->class_name . '/detail/'.$id);
        $service                         = $this->service_model
                                        ->with(array('machine', 'store', 'transaction', 'repair','machine_type','document'))
                                        ->getById($id)->row();
        $data['data']               = $service;
        //$data['data']['status'] = transaksi::getServiceStatus($service['service_status']);

        // button bagian atas
        $btnGroup = array();
        if($pr->pr_status==prstatus::DRAFT) 
            $btnGroup[] = view::button_add_detail(array('id'=>$id));
        $data['button_group'] = view::render_button_group($btnGroup); 

        // button bagian bawah
        if ($pr->pr_status==prstatus::REVISI || $pr->pr_status==prstatus::DRAFT){
            $btngroup2[] = view::button_cancel_request($id);
            $btngroup2[] = view::button_send_request($id);
        }
        $data['button_group_2'] =view::render_button_group($btngroup2);

        // view keterangan status pr dan revisi pr
        $status_service = transaksi::getServiceStatus($data['data']->service_status);
        $data['status_service']  = view::render_status_service($status_service);
        $data['revisi_pr']  = view::render_revisi_pr($data['data']->count_revisi);
        $data['page']['id'] = 'pr-detail';

        $data['table']['main']  = $this->table_detail($id, array('wrapper'));
        $data['readonly']       = "readonly";
        $data['isBtnPageBack']  = true;
        $data['form_info']      = 'service/form_info.tpl';

        if($this->input->is_ajax_request()){
            $this->twig->display('base/page_content.tpl',$data);
        }else{
            $data['sidebar'] = $this->access_right->menu();
            $data['content'] = 'base/page_content.tpl'; 
            $this->twig->display('index.tpl', $data);    
        }
        
    }

    public function table_detail($id_sv = '', $options = array()){
        $this->load->model('selling_model');
        $this->load->library('Datatable');
        /*$table = $this->datatable;
        $is_head_office = hprotection::isHeadOffice();*/

        $table = new Datatable();
        $table->reset();

        if (in_array('wrapper', $options)) {
            $table->numbering  = true;
            $table->isScrollable= false;
            $table->id         = 'table-detail_service';
            $table->header     = array('DATE','DUE DATE','LOCATION', 'STATUS', 'NOTE', 'USER');
            $table->source     = site_url($this->class_name.'/table_detail/'.$id_sv);
            $table->limitStore = TRUE;
            return $table->generateWrapper();
        } else {
            $table
                ->setModel($this->service_model)
                ->setNumbering()
                ->with(array('detail','status_detail','store_detail','pegawai_detail','location'))
                ->select('date_created,date_created + interval exp_time day as due_date,loc_name,nama, note,nama_pegawai')
                ->where('this.transactionservice_id',$id_sv);
/*                ->edit_column('selling_id', '$1', 'view::btn_group_edit_delete(selling_id)')
                ->edit_column('qty', '$1', 'hgenerator::number(qty)'); */    
            echo $table->generate();
        }

    }

    public function add_detail($id='', $status_delete = 0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->load->model(array('mesin_model','m_service_model','m_loc_service_model'));
            $this->load->library('custom_form');
            $form = new custom_form();
            $param['title']         = 'Add Detail Service Request';
            $param['form_action']   = view::form_input_detail($id);
            //$param['data']          = $row;

            $param['list']['status'] = form_dropdown(
                                        'data[id_status]', 
                                        $this->m_service_model->options(), 
                                        '', 
                                        'class="form-control select2" data-placeholder="Select Status"'
                                    );
            $param['list']['location'] = form_dropdown(
                                        'data[id_lokasi]', 
                                        $this->m_loc_service_model->options(), 
                                        '', 
                                        'class="form-control select2" data-placeholder="Select Location" id="22"'
                                    );


            $form->portlet      = true;
            $form->button_back  = true;
            $form->param        = $param;
            $form->url_form     = $this->class_name.'/form_detail.tpl';
            echo $form->generate();
        } else {
            $this->access_right->redirect();
        }
    }

    public function proses_detail() {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {

            $this->form_validation->set_rules('data[id_status]', 'Status Service', 'required|trim');
            $this->form_validation->set_rules('data[id_lokasi]', 'Service Location', 'required|trim');

            if ($this->form_validation->run()) {
                $message = array('success'=>false, 'message'=>'Proses penyimpanan data gagal.');

                
                $data   = $this->input->post('data');
                $data['id_trans_service']     = $this->input->post('id');
                $data['user_id'] = $this->session->userdata('user_id');
                $data['date_created'] = date::now_sql();
                $this->db->trans_start();
                $this->load->model(array('service_model'));
                $this->service_model->insert_detail($data);

                $updateStat['service_status'] = $data['id_status'];
                $this->service_model->update($updateStat,$data['id_trans_service']);

               
                
                if($this->db->trans_status()){
                    $message = array('success'=>true, 'message'=>'Save Success', 'callback'=>'loadNewCustomTable("#table-detail_service")');
                }else{
                    $message = array('success'=>false, 'message'=>'Database Error');
                }
                $this->db->trans_complete();
            } else {
                $message = array('success'=>false, 'message'=>validation_errors());
            }
            echo json_encode($message);
        } else {
            $this->access_right->redirect();
        }
    }

    public function table_sparepart($id){
        echo $this->getForm('table-sparepart', $id, array('no_wrapper'=>true));
    }

    public function form($act = 'new', $id = '', $param = array()){
        $data['form'] = $this->getForm($act, $id, $param);
        $this->twig->display('base/page_form_part.tpl', $data);
    }

    public function add() {
        echo $this->getForm('new');
    }

    public function edit($id) {
        $this->access_right->otoritas('edit', true);
        echo $this->getForm('edit',$id); 
    }

    public function edit_sparepart($id){
        echo $this->getForm('form-sparepart',NULL, array('id'=>$id));
    }

    public function view($id_enc){
        $id = url_base64_decode($id_enc);
        $this->form('view',$id);
    }

    public function delete($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('delete', true);
        $this->form('delete',$id);
    }

    public function assign($id){
        echo $this->getForm('assign',$id);
    }

    public function repair($id){
        echo $this->getForm('repair',$id);
    }

    public function sparepart($id){
        echo $this->getForm('sparepart',$id);
    }

    public function proses() {
        
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
                
            $this->load->model(array(
                'transaction_model','transaction_document_model',
                'transaction_store_model', 'transaction_machine_model',
                'service_model'
            ));

            // inisialisasi
            $this->db->trans_start();
            $user_id = $this->session->userdata('user_id');
            $store_id= $this->session->userdata('store_id');
            $id             = $this->input->post('id');
            $data           = $this->input->post('data');
            $doc_number     = $this->input->post('doc_number');

            if($id){
                // edit
                $transaction_id = $this->service_model->getTransactionId($id);
                 // insert to transaction_document table
                $doc['doc_number']      = $doc_number;
                //$doc['transaction_id']  = $transaction_id;
                $this->transaction_document_model->update($doc,NULL,array('transaction_id'=>$transaction_id));//---------------insert 3

                $this->service_model->update($data, $id);//----------------- update 5

            }else{
                // insert to transaction table
                $trans['transactiontype_id']    = transactiontype::SREQ;
                $trans['rec_created']           = date::now_sql();
                $trans['rec_user']              = $user_id;
                $this->transaction_model->create($trans);//---------insert 1
                $transaction_id = $this->db->insert_id();

                // insert to transaction_store table
                $store['store_id']          = $store_id;
                $store['transaction_id']    = $transaction_id;
                $this->transaction_store_model->create($store);//----------------insert 2

                // insert to transaction_document table
                $doc['doc_number']      = $doc_number;
                $doc['transaction_id']  = $transaction_id;
                $this->transaction_document_model->create($doc);//-------------------insert 3

                // insert to transaction_machine table
                $mcn['machine_id']      = $data['machine_id'];
                $mcn['transaction_id']  = $transaction_id;
                $this->transaction_machine_model->create($mcn);//--------------------insert 4

                // insert to transaction_service
                $data['transaction_id']  = $transaction_id;
                $data['store_id']        = $store_id;
                $data['service_status'] = 2;//===== cons untuk menampilkan tombol repair.
                $this->service_model->create($data);//-------------------------insert 5
                $transactionservice_id = $this->db->insert_id();
                //===================================
                // inisialisasi
                //$this->db->trans_start();
                //$user_id        = $this->session->userdata('user_id');
                //$store_id       = $this->session->userdata('store_id');
                $service_id     = $transactionservice_id;
                //$repair_id      = $this->input->post('transactionrepair_id');
                //$data           = $this->input->post('data');
                //$doc_number     = $this->input->post('doc_number');

                

                $service = $this->service_model->getById($service_id)->row();

                // insert to transaction table
                $trans['transactiontype_id']    = transactiontype::SREP;
                $trans['rec_created']           = date::now_sql();
                $trans['rec_user']              = $user_id;
                $this->transaction_model->create($trans);
                $transaction_id = $this->db->insert_id();

                // insert to transaction_store table
                $store['store_id']          = $store_id;
                $store['transaction_id']    = $transaction_id;
                $this->transaction_store_model->create($store);

                // insert to transaction_document table
                $doc['doc_number']      = $doc_number;
                $doc['transaction_id']  = $transaction_id;
                $this->transaction_document_model->create($doc);

                // insert to transaction_machine table
                $mcn['machine_id']      = $service->machinde_id;
                $mcn['transaction_id']  = $transaction_id;
                $this->transaction_machine_model->create($mcn);

                // insert to transaction_repair
                $this->load->model('repair_model');
                $data2['transaction_id']         = $transaction_id;
                $data2['transactionservice_id']  = $service_id;
                $data2['store_id']               = $store_id;
                $data2['repair_status'] = 2;
                $this->repair_model->create($data2);

                

            }

            if($this->db->trans_status()){
                $message = array(
                            'success'=>true,
                            'message'=>'Save Success'
                            );
            }else{
                $message = array(
                            'success'=>false,
                            'message'=>'Database Error'
                            );
            }
            $this->db->trans_complete();

            
            echo json_encode($message);
        } else {
            $this->access_right->redirect();
        }
    }

    public function proses_repair(){
        $this->load->model(array(
            'transaction_model','transaction_document_model',
            'transaction_store_model', 'transaction_machine_model',
            'service_model', 'repair_model'
        ));

        // inisialisasi
        $this->db->trans_start();
        $user_id        = $this->session->userdata('user_id');
        $store_id       = $this->session->userdata('store_id');
        $service_id     = $this->input->post('transactionservice_id');
        $repair_id      = $this->input->post('transactionrepair_id');
        $data           = $this->input->post('data');
        $doc_number     = $this->input->post('doc_number');

        if($repair_id){
            // edit
            $this->repair_model->update($data, $transactionrepair_id);

        }else{

            $service = $this->service_model->getById($service_id)->row();

            // insert to transaction table
            $trans['transactiontype_id']    = transactiontype::SREP;
            $trans['rec_created']           = date::now_sql();
            $trans['rec_user']              = $user_id;
            $this->transaction_model->create($trans);
            $transaction_id = $this->db->insert_id();

            // insert to transaction_store table
            $store['store_id']          = $store_id;
            $store['transaction_id']    = $transaction_id;
            $this->transaction_store_model->create($store);

            // insert to transaction_document table
            $doc['doc_number']      = $doc_number;
            $doc['transaction_id']  = $transaction_id;
            $this->transaction_document_model->create($doc);

            // insert to transaction_machine table
            $mcn['machine_id']      = $service->machinde_id;
            $mcn['transaction_id']  = $transaction_id;
            $this->transaction_machine_model->create($mcn);

            // insert to transaction_repair
            $data['transaction_id']         = $transaction_id;
            $data['transactionservice_id']  = $service_id;
            $data['store_id']               = $store_id;
            $this->repair_model->create($data);

        }

        if($this->db->trans_status()){
            $message = array(
                        'success'=>true,
                        'message'=>'Save Success'
                        );
        }else{
            $message = array(
                        'success'=>false,
                        'message'=>'Database Error'
                        );
        }
        $this->db->trans_complete();
        
        echo json_encode($message);
    }

    public function proses_sparepart(){
        $this->db->trans_start();
        $this->load->model(array('repair_sparepart_model','item_stok_model'));

        $id         = $this->input->post('id');
        $service_id = $this->input->post('transactionservice_id');
        $repair_id  = $this->input->post('transactionrepair_id');
        $data       = $this->input->post('data');

        if($id){
            // edit
            $this->repair_sparepart_model->update($data, $id);
        }else{
            // new
            $data['transactionservice_id'] = $service_id;
            $data['transactionrepair_id']  = $repair_id;
            $this->repair_sparepart_model->create($data);

            //========= update stok ===================
            $currentStock = $this->item_stok_model
                ->with('item')
                ->where("store_id", hprotection::getSC())
                ->where('m_item_stok.item_id',$data['item_id'])
                ->get()->row();

            $stokUpdateRow = array("itemstok_id"=> $currentStock->itemstok_id);
            $stokUpdateRow['stok_gudang'] = $currentStock->stok_gudang - $data['qty'];
            $stokUpdateRow['stok_gudang_mutasi'] = $currentStock->stok_gudang_mutasi - $data['qty_mutasi'];
            $stokUpdateRow['stok_total'] = $currentStock->stok_total - $data['qty'];
            $stok_update_data[] = $stokUpdateRow;
            $this->db->update_batch ( 'm_item_stok', $stok_update_data, 'itemstok_id' );
            //-------- end update stok ------------------

        }

        if($this->db->trans_status()){
            $message = array(
                        'success'=>true,
                        'message'=>'Save Success',
                        //'callback' => 'loadNewCustomTable("#div-table-sparepart")',
                        );
        }else{
            $message = array(
                        'success'=>false,
                        'message'=>'Database Error'
                        );
        }
        $this->db->trans_complete();
        echo json_encode($message);
    }

    public function proses_delete_sparepart($id){
        if(!$id)
            $id = $this->input->post('id');
        if(!$id)
            return;

        $this->db->trans_start();
        $this->load->model('repair_sparepart_model');
        $this->repair_sparepart_model->delete($id);

        if($this->db->trans_status()){
            $message = array(
                        'success'=>true,
                        'message'=>'Save Success',
                        'callback' => 'loadNewCustomTable("#div-table-sparepart")',
                        );
        }else{
            $message = array(
                        'success'=>false,
                        'message'=>'Database Error'
                        );
        }
        $this->db->trans_complete();
        echo json_encode($message);
    }

    public function proses_delete() {
        $id = $this->input->post('id');
        $this->access_right->otoritas('delete', true);
        if ($this->access_right->otoritas('delete', true)) {
            $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');
            if ($this->service_model->delete($id)) {
                $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', 'refresh_filter()');
                /* INSERT LOG */
                $this->access_right->activity_logs('delete','Delete BBM');
                /* END OF INSERT LOG */
            }
            echo json_encode($message);
        }
    }

    public function getForm($act='new', $id='', $param = array()){
        $this->load->model(array('mesin_model', 'pegawai_model','store_model'));
        $this->load->library('custom_form');
        $form = new custom_form();

        $data = array();
        $data['act']    = $act;
        $data['id']     = $id;
        $data['form']       = $this->class_name.'/form.tpl';

        if ($id) {
            $row    = $this->service_model
                        ->with(array('transaction', 'document', 'store', 'machine', 'machine_type', 'repair'))
                        ->getById($id)->row();
        }

        switch ($act) {
            case $act == 'new' || $act=='edit' :
                $param['title']         = 'New Service Request';
                $param['form_action']   = view::form_input($id);
                $param['data']          = $row;
                $param['list']['service_type'] = form_dropdown('data[service_type]', array(1=>'Internal Repair', 2=>'Request to HO'), $row->service_type, 'class="form-control bs-select"');

                $htmlOptions            =  'onchange="refreshList(this)" target="#pr_code" target-type="text" 
                                            data-source="'.site_url($this->class_name.'/getcode').'"
                                            class="form-control select2" data-live-search="true" data-placeholder="Select Store"';
                //$data['list_store']     = form_dropdown('data[store_id]', $this->store_model->options_empty(), $row->store_id, $htmlOptions);
                $param['list']['machine'] = form_dropdown(
                                            'data[machine_id]', 
                                            $this->mesin_model->options_serial(), 
                                            $row->machine_id, 
                                            'class="form-control select2" data-placeholder="Select Machine"'
                                        );
                $param['list']['machine'] = form_dropdown(
                                            'data[machine_id]', 
                                            $this->mesin_model->options_serial(), 
                                            $row->machine_id, 
                                            $htmlOptions
                                        );

                $form->portlet      = true;
                $form->button_back  = true;
                $form->param        = $param;
                $form->url_form     = $this->class_name.'/form_request.tpl';
                return $form->generate();
                break;

            case 'repair':
                $this->load->model('status_repair_model');
                $param['title']             = 'REPARATION';
                $param['form_action']       = form_open(
                                                site_url($this->class_name.'/proses_repair'),
                                                '',
                                                array(
                                                    'transactionservice_id' => $id,
                                                    'transactionrepair_id' => $row->transactionrepair_id,
                                                )
                                            );
                $param['data']              = $row;
                $param['list']['repair_status'] = form_dropdown('data[repair_status]', $this->status_repair_model->options(), $row->repair_status, 'class="bs-select form-control"');

                $form->portlet      = true;
                $form->button_back  = true;
                $form->param        = $param;
                $form->url_form     = $this->class_name.'/form_repair.tpl';
                return $form->generate();
                break;

            case 'sparepart' :
                $paramForm['transactionservice_id'] = $row->transactionservice_id; 
                $paramForm['transactionrepair_id']  = $row->transactionrepair_id;

                $param['table']['sparepart']= $this->getForm('table-sparepart', $id);
                $param['form']['sparepart'] = $this->getForm('form-sparepart', NULL, $paramForm);

                $param['title']             = 'SPAREPART';
                $param['btn_close_service']         = true;
                $param['content_body']      = $this->class_name.'/page_sparepart.tpl';
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'form-sparepart':
                $this->load->model('item_model');

                if ($param['id']){
                    // id repair_sparepart
                    $this->load->model(array('repair_sparepart_model'));
                    $row = $this->repair_sparepart_model
                                    ->getById($param['id'])->row();
                }

                $hiddenValue = array(
                                        'id' => $param['id'],
                                        'transactionservice_id'=> $param['transactionservice_id'],
                                        'transactionrepair_id' => $param['transactionrepair_id'],
                                    );

                $form->form_open    = form_open(
                                            site_url($this->class_name.'/proses_sparepart'),
                                            '',
                                            $hiddenValue
                                        );
                $param['list']['sparepart'] = form_dropdown('data[item_id]', $this->item_model->options_sparepart(), $row->item_id,'class="form-control select2"');
                
                $form->data         = $row;
                $form->buttonInForm = true;
                $form->close_modal  = false;
                $form->param        = $param;
                $form->url_form     = $this->class_name.'/form_sparepart.tpl';
                return $form->generate();
                break;

            case 'table-sparepart':
                $this->load->model('repair_sparepart_model');
                $data = $this->repair_sparepart_model
                            ->with('item')
                            ->where('transactionservice_id', $id)
                            ->where('transactionrepair_id', $row->transactionrepair_id)
                            ->get()->result_array();
                

                foreach ($data as $key => $val){
                    $temp= array();
                    $btn = array();
                    $urlFormEdit    = site_url($this->class_name.'/edit_sparepart/'.$val['repairsparepart_id']);
                    $urlFormDelete  = site_url($this->class_name.'/proses_delete_sparepart/'.$val['repairsparepart_id']);
                    $btn[] = '<a class="btn grey btn-sm" onclick="btnLoadContent(this)" target="#wrapper-form-sparepart" data-source="'.$urlFormEdit.'" ><i class="fa fa-edit"></i></a>';
                    $btn[] = '<a class="btn btn-delete-confirm grey btn-sm" delete-id="'.$val['repairsparepart_id'].'" data-source="'.$urlFormDelete.'" ><i class="fa fa-remove"></i></a>';

                    $temp['item_name']  = $val['item_name'];
                    $temp['qty']        = $val['qty'];
                    $temp['qty_mutasi']        = $val['qty_mutasi'];
                    $temp['action']     = view::render_button_group($btn);
                    $rows[] = $temp;
                }

                $this->load->library("custom_table");
                $table = new custom_table();
                $table->columns = array('SPAREPART', 'QTY','QTY MUTASI', 'ACTION');
                $table->id      = 'table-sparepart';
                $table->data    = $rows;
                $table->data_source = site_url($this->class_name.'/table_sparepart/'.$id);
                
                if($param['no_wrapper'])
                    return $table->generate();
                else
                    return $table->generateWithWrapper();
                break;
            

            case 'delete':
                $data['title']          = 'Delete REPARATION DATA';
                $data['form_action']    = view::form_delete($id);;
                $data['readonly']       = ' readonly="" ';
                $button_group[]         = view::button_delete_confirm();
                break;

            case 'view':
                $data['title']          = 'VIEW REPARATION DATA';
                $data['form_action']    = view::form_delete($id);
                $data['readonly']       = ' readonly="" ';
                unset($button_group);
                break;

            case 'complete':
                $title['title'] = 'EDIT REPARATION DATA';
                $data['act']    = 'edit';
                $data['form']   = $this->getForm('edit',$id);
                $data['form_service'] = $this->getForm('view',$id);
                //$button_group[]     = view::button_save();
                return $this->twig->render($this->class_name.'/form_complete.tpl', $data);
                break;

            case 'assign':
                //$button_group[] = view::button_approve_request('', array('onclick' => ""));
                $param['title']             = 'ASSIGN REPAIR JOB';
                $param['data']              = $row;
                $param['list']['teknisi']   = form_dropdown('data[technician_id]', $this->pegawai_model->getOptions('teknisi'), $row->technician_id, 'class="form-control select2"');
                $param['form_action']       = view::form_input($id, array('id'=>'form-service'));

                $form->portlet      = true;
                $form->button_back  = true;
                $form->param        = $param;
                $form->url_form     = $this->class_name.'/form.tpl';
                return $form->generate();

                break;

            default:
                # code...
                break;
        }

        $data['button_group'] = view::render_button_group($button_group);
        return $this->twig->render($this->class_name.'/form.tpl', $data);
    }

    public function getCode($machine_id = NULL){
        if (!$machine_id)
            return;
        $storeId = $this->service_model->getStoreId($machine_id);
        echo code::getCodeAndRaise('sr', $storeId);
    }


    public function barcode($id) {
        //$id = url_base64_decode($id_enc);
        $this->access_right->otoritas('edit', true);
        $this->showbarcode($id);
    }


    public function showbarcode($id='',$status_delete = 0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->config->load('twig');
            
            $title = 'Tambah Data';
            $data['form_action'] = $this->class_name . '/proses';

            $data['id'] = $id;
            if($id){
                $title = 'Service Request Barcode';

                $row = $this->service_model
                ->with(array('machine', 'store', 'transaction', 'repair','machine_type','document','v_detail','status_service'))
                ->where(array('this.transactionservice_id' => $id))
                ->get()->row();

                $data['data']  = $row;
            }

            $data['title']      = $title;
            $data['assets_url'] = $this->config->item('assets_url');
            $data['sidebar']    = $this->access_right->menu();
            $data['form']       = $this->class_name.'/form_barcode.tpl';

            $button_group   = array();

            //BUTTON BACK
            $button_group[] = view::button_back();            
            //BUTTON PRINT
            $data['form_action']    = view::form_input($id);
            $button_group[]         = view::button_print_service();

            $data['button_group'] = view::render_button_group($button_group);
            //$data['options_hadiah'] = form_dropdown('data[hadiahtype_id]', $this->hadiah_type_model->options(), $row->hadiahtype_id, 'class="bs-select" disabled="disabled"');
            
            //$options_pulau = $this->pulau_model->options();
            //$data['options_pulau'] = form_dropdown('id_pulau',$options_pulau, !empty($def_id_pulau) ? $def_id_pulau : '', 'class = "form-control"'); 
            
            $this->twig->display('base/page_form.tpl', $data);

        } else {
            $this->access_right->redirect();
        }
    }


    public function initPrinting(){
        $this->load->helper('printingdua');

        $ci =& get_instance();

        $doc_number = $ci->input->post('doc_number');

        $nc = $ci->input->post('nc');

        $arrname = $this->cutName($nama, 20);

        $cmd .= "^XA
                ^MMA
                ^PW799
                ^LL0240
                ^LS0
                ^BY2,3,71^FT129,131^BCN,,Y,N
                ^FD>:".$doc_number."^FS
                ^FT220,197^A0N,28,28^FH\^FD".$doc_number."^FS";

        if(count($arrname)==2){
            $cmd .= "^FT39,196^A0N,23,24^FH\^FD".$arrname[1]."^FS";
        }

        if(count($arrname)==3){
            $cmd .= "^FT39,196^A0N,23,24^FH\^FD".$arrname[1]."^FS";
            $cmd .="^FT39,229^A0N,23,24^FH\^FD".$arrname[2]."^FS";
        }
                
        $cmd .= "^PQ".$nc.",0,1,Y^XZ";

/*        $cmd .= "^XA";
        $cmd .= "^MMA";
        $cmd .= "^PW320";
        $cmd .= "^LL0240";
        $cmd .= "^LS0";
        $cmd .= "^BY3,3,106^FT36,134^BCN,,Y,N";
        $cmd .= "^FD>;".$angka."^FS";
        for($i=0;$i<count($arrname);$i++){
            $cmd .= "^FT36,185^A0N,20,19^FH\^FD".$arrname[$i]."^FS";
        }
        $cmd .= "^PQ".$nc.",0,1,Y^XZ";  
*/
        $form = '<form id="myForm" action="">

    <input type="hidden" id="sid" name="sid" value="'.session_id().'" />
        <fieldset hidden>
            <legend>Client Printer Settings</legend>
            
            <div hidden>
                I want to:&nbsp;&nbsp;
                <select id="pid" name="pid">
                  <option value="0">Use Default Printer</option>
                  <option value="1" selected="selected">Display a Printer dialog</option>
                  <option value="2">Use an installed Printer</option>
                  <option value="3">Use an IP/Etherner Printer</option>
                  <option value="4">Use a LPT port</option>
                  <option value="5">Use a RS232 (COM) port</option>
                </select>
                <br />
                <br />
                <div id="info" class="alert alert-info" style="font-size:11px;"></div>                
                <br />
            </div>
            
            <div id="installedPrinter" hidden>
                <div id="loadPrinters" name="loadPrinters">
                WebClientPrint can detect the installed printers in your machine. <a onclick="javascript:jsWebClientPrint.getPrinters();" class="btn btn-success">Load installed printers...</a>
                </div>
                <label for="installedPrinterName">Select an installed Printer:</label>
                <select name="installedPrinterName" id="installedPrinterName"></select>

            

            </div>           
                        
        </fieldset>
        <fieldset hidden>
            <legend>Printer Commands</legend>
            
            <p>
                Enter the printers commands you want to send and is supported by the specified printer (ESC/P, PCL, ZPL, EPL, DPL, IPL, EZPL, etc). 
                <br /><br />
                <b>NOTE:</b> You can use the <b>hex notation for non-printable characters</b> e.g. for Carriage Return (ASCII Hex 0D) you can specify 0x0D
                
            </p>
            <br /><br />
            <div class="alert alert-info" style="font-size:11px;">
            <b>Upload Files</b><br />
            This online demo does not allow you to upload files. So, if you have a file containing the printer commands like a PRN file, Postscript, PCL, ZPL, etc, then we recommend you to <a href="http://www.neodynamic.com/products/printing/raw-data/php/download/" target="_blank">download WebClientPrint</a> and test it by using the sample source code included in the package. Feel free to <a href="http://www.neodynamic.com/support" target="_blank">contact our Tech Support</a> for further assistance, help, doubts or questions.             
            </div>            
        </fieldset>        
        <fieldset hidden>
            <legend hidden>Ready to print!</legend>
            <h3>Your settings were saved! Now its time to <a href="#" onclick="javascript:doClientPrint();" class="btn btn-large btn-success">Print</a></h3>           
        </fieldset>

        <textarea id="printerCommands" name="printerCommands" rows="10" cols="80" class="span9" hidden>'.$cmd.'</textarea>
    </form>';
    echo $form;

    }


    function cutName($name='', $maxlength=0){
        //$name = 'CHOCO MANIA BISKUIT 21GR72PC';
        $arrname=explode(' ',$name);
        
        $arrresult = array();
        $i=0;
        $j=0;
        $temp = '';
        while($i<count($arrname)){
            $temp .= $arrname[$i];            
            if(strlen($arrresult[$j])<$maxlength && strlen($temp)<$maxlength){
                if(strlen($arrresult[$j])==0){
                    $arrresult[$j] .= $arrname[$i];                    
                }else{
                    $arrresult[$j] .= ' '.$arrname[$i];
                }
            }else{
                $temp = '';
                $j++;
                $i--;
            }
            $i++;
        }
        return $arrresult;
    }

}

/* End of file bbm.php */
/* Location: ./application/controllers/bbm.php */

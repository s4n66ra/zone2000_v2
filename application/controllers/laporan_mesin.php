<?php

/**
 * Description of laporan penjualan
 *
 * @author SANGGRA HANDI
 */
class laporan_mesin extends My_Controller {


    public function __construct() {
        parent::__construct();
        $this->load->model('laporan_payout_model');
    }

    
    public function index() {
        $data['title'] = 'Laporan Machine';

        /* INSERT LOG */
        //$this->access_right->activity_logs('view',$data['title']);
        /* END OF INSERT LOG */

        //$data['button_group'] = $this->getAvailableButtons();
        //$data['button_right'] = $this->getTools();

        $data['table']['main']  = $this->table_main(array('wrapper', 'filter'));
        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'base/page_content.tpl';
    
        $this->twig->display('index.tpl', $data);
    }

    public function table_main($option = array()){
        $this->load->model('laporan_mesin_model');
        $this->load->model('range_tiket_model');
        $this->load->model('likuidasi_model');
        $this->load->library('Datatable');
        $table = $this->datatable;
        //$is_head_office = hprotection::isHeadOffice();*/

        $table = new Datatable();
        $table->reset();

        /*if (in_array('filter', $option)) {
            $table->dataFilter = array(
                                array('nama_selling', 'Merchandise Name', 'text'),
                                array('this.sellingtype_id', 'Type', 'list', $this->selling_type_model->options()),
                            );
        }*/

        if (in_array('wrapper', $option)) {
           $table->numbering  = true;
            $table->isScrollable= true;
            $table->id         = 'table-laporan-payout';
            $table->header = array('MACHINE','REGISTER','HARGA','OMSET','SWIPE');
            $table->source     = site_url($this->class_name.'/table_main');
            return $table->generateWrapper();
        } else {
            $table
                ->setModel($this->laporan_mesin_model)
                ->setNumbering()
                ->with(array('zn_mesin'))
                ->select('machine_id')
                ->group_by( array('machine_id'))
                ->where('rec_created <= ','2015-'.date("m").'-31')
                ->where('rec_created >= ','2015-'.date("m").'-01')
                ->order_by('machine_id');
            $row = $table->getData();
            $data = $this->laporan_mesin_model->data_detail();
            $table->setData($data);

            echo $table->generate();

/*            $table
                ->setModel($this->likuidasi_model)
                ->setNumbering()
                ->with(array('item','outlet'))
                ->select('date,item_key, item_name,qty ,outlet.nama_cabang,note,status,likuidasi_id')
                ->edit_column('status', '$1', 'mutasistatus::getStatus(status)')
                ->edit_column('likuidasi_id', '$1', 'view::btn_group_mutasi(likuidasi_id)');
            $data = $table->getData();
            foreach ($data as $key => $value) {
                $temp = $this->range_tiket_model->get_tiket_by_id($value['item_id']);
                $data[$key]['item_id'] = $temp;
            }
            $table->setData($data);

            echo $table->generate();*/
        }

        /*if (in_array('wrapper', $option)) {
            $table->numbering  = true;
            $table->isScrollable= true;
            $table->id         = 'table-laporan-payout';
            $table->header = array('MACHINE','REGISTER','TIKET','VALUE','OMSET','PAYOUT');
            $table->source     = site_url($this->class_name.'/table_main');
            return $table->generateWrapper();
        } else {
            $table
                ->setModel($this->laporan_payout_model)
                ->setNumbering()
                ->with(array('zn_mesin'))
                ->select('machine_id')
                ->group_by( array('machine_id'))
                ->order_by('machine_id');
            $row = $table->getData();
            $data = $this->laporan_payout_model->data_detail();
            $table->setData($data);

            echo $table->generate();
        }*/

    }

    public function add($id='',$status_delete = 0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->config->load('twig');
            $this->load->model('item_model');
            $title = 'Tambah Data Penjualan Langsung';
            $data['form_action'] = $this->class_name . '/proses';

            $data['id'] = $id;
            if($id){
                $title = 'Edit Data Penjualan Langsung';
                $row            = $this->selling_model->getById($id)->row();
                $data['data']  = $row;
            }

            $data['title']      = $title;
            $data['assets_url'] = $this->config->item('assets_url');
            $data['sidebar']    = $this->access_right->menu();
            $data['form']       = $this->class_name.'/form_selling.tpl';

            $button_group   = array();
            $button_group[] = view::button_back();
            
            if($status_delete == 0){
                $data['form_action']    = view::form_input($id);
                $button_group[]         = view::button_save();
            }else{
                $data['title']          = 'Delete Selling';
                $data['form_action']    = view::form_delete($id);;
                $data['readonly']       = 'readonly=""';
                $button_group[]         = view::button_delete_confirm();
            }

            $data['button_group'] = view::render_button_group($button_group);

            $is_head_office = hprotection::isHeadOffice();
            $this->load->model('store_model');
            if($is_head_office){
                $htmlOptions            =  'id="list-store" class="form-control select2" data-placeholder="Select Store"';
                $data['list_store']     = form_dropdown('data[store_id]', $this->store_model->options_empty(), $row->store_id,$htmlOptions);
            }else{
                $htmlOptions            =  'id="list-store" class="form-control select2" data-placeholder="Select Store" disabled="disabled"';
                $data['list_store']     = form_dropdown('data[store_id]', $this->store_model->options_empty(), $this->session->userdata('store_id'),$htmlOptions ).form_hidden('data[store_id]',$this->session->userdata('store_id'));
            }
            if(empty($row->selling_date)){
                $data['data']['selling_date'] = date('Y-m-d');
            }
/*            $htmlOptions        = 'id="list-item" placeholder="Select Item" class="select2 form-control" data-live-search="true"';

            $data['list_item']  = form_dropdown('data[item_id]', $this->item_model->options_empty(2), $row->item_id, $htmlOptions);
*/
            $data['list_item'] = '<input type="hidden" id="sell_list_item_id" name="data[item_id]" class="form-control" value="'.$row->item_id.'"/>';


            //$htmlOptions        = 'id="list-item" placeholder="Select Item" class="bs-select form-control" data-live-search="true"';

            //$data['list_jenis']  = form_dropdown('data[jenis_selling_id]', $this->selling_model->options_jenis(), $row->jenis_selling_id, $htmlOptions);
            $data['list_jenis'] = '<input type="hidden" name="data[jenis_selling_id]"  value="1"/>';
            
            
            $this->twig->display('base/page_form.tpl', $data);

        } else {
            $this->access_right->redirect();
        }
    }

    

    public function edit($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('edit', true);
        $this->add($id);
    }

    public function delete($id_enc = '') {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('delete', true);
        $this->add($id,$status_delete = 1);
    }

    public function proses() {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('data[store_id]', 'Outlet', 'required|trim');
            $this->form_validation->set_rules('data[jenis_selling_id]', 'Type selling', 'required|trim');
            $this->form_validation->set_rules('data[item_id]', 'Item', 'required|trim');
            $this->form_validation->set_rules('data[qty]', 'Qty', 'required|trim');

            $this->load->model('item_stok_model');
            $modelItemStok = $this->item_stok_model;


            if ($this->form_validation->run()) {
                $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');

                $id = $this->input->post('id');
                $data = $this->input->post('data');
                $data['qty'] = hgenerator::switch_number($data['qty']);
                $this->db->trans_start();
                $qty = $data['qty'];
                if ($id){
                    $row            = $this->selling_model->getById($id)->row();
                    $dataold  = $row;
                    $this->selling_model->update($data, $id);

                    $whereCek = array('store_id'=>$data['store_id'],'item_id'=>$data['item_id']);
                    $cekEksisStok = $modelItemStok->select('itemstok_id')->where($whereCek)->get()->num_rows();
                    if($cekEksisStok>0){
                        $modelItemStok
                            ->set("stok_gudang","stok_gudang-$qty",FALSE)
                            ->set("stok_total","stok_total-$qty",FALSE)
                            ->where($whereCek)->update();
                    }else{
                        $modelItemStok
                            ->set("stok_gudang","stok_gudang-$qty",FALSE)
                            ->set("stok_total","stok_total-$qty",FALSE)
                            ->set($whereCek)->insert();
                    }

                    $whereCek2 = array('store_id'=>$dataold->store_id,'item_id'=>$dataold->item_id);
                    $qty2 = $dataold->qty;
                    $cekEksisStok = $modelItemStok->select('itemstok_id')->where($whereCek2)->get()->num_rows();
                    if($cekEksisStok>0){
                        $modelItemStok
                            ->set("stok_gudang","stok_gudang+$qty2",FALSE)
                            ->set("stok_total","stok_total+$qty2",FALSE)
                            ->where($whereCek2)->update();
                    }else{
                        $data = array(
                           'stok_gudang' => "stok_gudang+$qty2" ,
                           'stok_total' => "stok_total+$qty2" 
                        );
                        $this->db->insert('m_item_stok',$data);
                    }
                }else{
                    $this->selling_model->create($data);
                    $whereCek = array('store_id'=>$data['store_id'],'item_id'=>$data['item_id']);
                    $cekEksisStok = $modelItemStok->select('itemstok_id')->where($whereCek)->get()->num_rows();
                    if($cekEksisStok>0){
                        $modelItemStok
                            ->set("stok_gudang","stok_gudang-$qty",FALSE)
                            ->set("stok_total","stok_total-$qty",FALSE)
                            ->where($whereCek)->update();
                    }else{
                        $data = array(
                           'stok_gudang' => 0 ,
                           'stok_total' => 0 
                        );
                        $this->db->insert('m_item_stok',$data);
                    }
                }

                
                
                if($this->db->trans_status()){
                    $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh_filter()');
                }else{
                    $message = array(false, 'Terjadi Kesalahan',$this->db->_error_message(), '');
                }
                $this->db->trans_complete();
            } else {
                $message = array(false, 'Terjadi Kesalahan', validation_errors());
            }
            echo json_encode($message);
        } else {
            $this->access_right->redirect();
        }
    }

    public function proses_delete() {
        $id=$this->input->post("id");
		$this->access_right->otoritas('delete', true);
        if ($this->access_right->otoritas('delete', true)) {
            $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');
            if ($this->selling_model->delete($id)) {
                $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', 'refresh_filter()');
				/* INSERT LOG */
				$this->access_right->activity_logs('delete','Delete Merchandise');
				/* END OF INSERT LOG */
            }
            echo json_encode($message);
        }
    }

    public function getItemsByIdAndKeywordJSON($itemtype_id=''){
        $this->load->model(array('item_model'));

        $tmp = $this->item_model->getItemsByIdAndKeyword($itemtype_id);
        
        //$tmp = $this->item_model->getItemsByIdAndKeyword(2,'');
        $arr = array();
        foreach ($tmp as $k => $v) {
            if($k != ''){
                $obj = new StdClass();
                $obj->id        = $k;
                $obj->text      = $tmp[$k];
                array_push($arr, $obj);
            }
        }
        echo json_encode($arr);
    }





}

/* End of file bbm.php */
/* Location: ./application/controllers/bbm.php */

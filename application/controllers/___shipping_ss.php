<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class shipping_ss extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array(
        	'shipping_model','shipping_koli_model','koli_model'
        ));
        $this->button['add'] = view::button_add(array('onclick'=>"btnLoadNextPage(this)"));
    }

    public function test(){
        $this->load->model(array('po_store_model','purchase_order_detail_model'));
        $model = $this->purchase_order_detail_model;
        $this->db->trans_start();

        $this->koli_model
            ->findByPk(1)
            ->set('koli_status', 90)
            ->update();
        vdebug('asd');
        // $this->koli_model
        //     ->findByPk(1)
        //     //->where('this.koli_id',)
        //     ->where('koli_status > 5')
        //     ->set('koli_status', 10)
        //     ->update();
        // $this->po_store_model
        //     ->findByPk(1)
        //     ->set('postore_status', 12)
        //     ->update();
        // $this->purchase_order_detail_model
        //     ->findByPk(1)
        //     ->set('detail_status', 20)
        //     ->update();

        $this->db->trans_complete();
    }

    public function index() {

        $data['title'] 		= 'SHIPPING WAREHOUSE';
        $data['sidebar'] 	= $this->access_right->menu();
        $data['content'] 	= 'base/page_content.tpl';     
        $data['button_group'] 	= $this->getAvailableButtons();
        $data['table']['main'] 	= $this->table_main(array('wrapper'));
        $data['javascript'] = array('shipping.js');
        
        $this->twig->display('index.tpl', $data);
    }

    public function table_main($option = array()){
        $this->load->library('Datatable');


        $table = $this->datatable;

        if (in_array('wrapper', $option)) {
            $table->numbering  = true;
            $table->id         = 'table-shipping';
            $table->isScrollable= false;
            $table->header     = array('SERVICE CENTER', 'USER', 'DATE', 'ACTION');
            $table->source     = site_url($this->class_name.'/table_main');
            return $table->generateWrapper();
        } else {
        	$table
	            ->setModel($this->shipping_model)
	            ->setNumbering()
	            ->with(array('store','employee'))
	            ->where('type', storetype::SC)
	            ->select('nama_cabang, nama_pegawai , date_created, shipping_id')
	            ->edit_column('date_created', '$1', 'date::longDate(date_created)')
	            ->edit_column('shipping_id', '$1', 'view::btn_group_receiving_warehouse(shipping_id)');
            echo $table->generate();
        }

    }

    public function add() {
    	$this->db->trans_start();
    	$data['type'] = storetype::SC;
    	
        
        $this->shipping_model->create($data);
    	$id = $this->shipping_model->insert_id;

    	if ($this->db->trans_status()){
    		
    	} else {
    		
    	}

    	$this->db->trans_complete();

    	$this->detail($id);
        
    }

    public function detail($id = '') {
    	echo $this->getForm('page-detail', $id);
    }

    public function add_detail($id = 0){
    	$ids = $this->input->post('ids');

        $modelShipKoli = $this->shipping_koli_model;
        
        foreach ($ids as $key => $value) {
            $dataInsert[] = array(
                'shipping_id' => $id,
                'koli_id' => $value,
            );
        }

        $this->db->trans_start();
        $modelShipKoli->insert_batch($dataInsert);
        if($this->db->trans_status()){
            $message = array(
                'success'=>true, 
                'message'=>'Save Success',
                'callback'=> 'refreshDataTable(true);'
            );
        }else{
            $message = array('success'=>false, 'message'=>$this->db->_error_message());
        }
        $this->db->trans_complete();
        echo json_encode($message);

    }

    public function getForm($type = '', $id = NULL, $param = array()){
    	$this->load->library(array('custom_form','custom_table'));
    	$this->load->model(array('sequence_model','po_store_model'));
    	$form = $this->custom_form;

    	switch ($type) {
    		case 'page-detail':
    			$param['title'] 	= 'SHIPPING DETAIL';
    			$param['portlet']	= true;
                $param['button']['portlet'] = view::render_button_group_portlet(array(
                            //view::button_save_form(),
                            view::button_shipping($id)
                    ));
    			$param['button']['back'] = true;
    			$param['form']['info'] 	= $this->getForm('form-info', $id);
    			$param['table']['main']	= $this->getForm('page-shipping', $id);
    			return $this->twig->render('base/page_content.tpl', $param);
    			break;

    		case 'form-info' :
                $param['form_action']   = form_open(site_url($this->class_name.'/proses_shipping/'.$id));
    			$param['data']	        = $this->shipping_model->with('store')->getById($id)->row();

                $form->button_save      = false
;                $form->id               = 'form-shipping';
                $form->param            = $param;
                $form->url_form         = 'shipping/form_info.tpl';
                return $form->generate();
    			break;

    		case 'page-shipping':
                $this->load->library('custom_tab');
                $tab = new Custom_tab();

                $tab->tab = array(
                    'list_koli' => array(
                                    'title' => 'List Koli',
                                    'body'  => $this->getForm('table-koli', $id, array('wrapper'))
                                ),
                    'list_shipping' => array(
                                    'title' => 'List Shipping',
                                    'body'  => $this->getForm('table-shipping', $id, array('wrapper'))
                                ),
                );
				
                return $tab->generate();
    			break;

    		case 'table-koli':
    			$this->load->library('Datatable');
		        $table = new Datatable();

		        if (in_array('wrapper', $param)) {
		            $table->id         	= 'table-koli';
		            $table->isScrollable= false;
		            $table->checklist   = true;
		            $table->pagination 	= false;
		            $table->header     	= array('KOLI CODE', 'SUPPLIER', 'STORE NAME', 'QTY');
		            $table->source     	= site_url($this->class_name.'/getform/table-koli/'.$id);
		            $table->caption_btn_group 	= 'Add to Shipping';
		            $table->url_action_group 	= site_url($this->class_name.'/add_detail');
		            $table->callback_group_submit = '
		            	var param = {
		            		ids : grid.getSelectedRows()
		            	};
		            	var url = "'.site_url($this->class_name.'/add_detail/'.$id).'";
                        var options = {
                            //confirm_message : "Sure to Add ?",
                        };

		            	pageSave(url, param, options);

		            ';
		            return $table->generateWrapper();
		        } else {
		        	$table
			            ->setModel($this->koli_model)
			            ->with(array('store', 'supplier', 'shipping'))
			            ->where('shipping_id is NULL')
                        ->where('koli_status', transactionstatus::SC)
                        // harusnya tambah satu lagi receive_id <> null
			            ->select('this.koli_id, koli_code, nama_supplier, nama_cabang, qty')
                        ->edit_column('koli_id', '<input type="checkbox" name="id[]" value="$1">','koli_id');
		            echo $table->generate();
		        }
    			break;
    		
    		case 'table-shipping':
    			$this->load->library('Datatable');
		        $table = new Datatable();

		        if (in_array('wrapper', $param)) {
		            $table->id         	= 'table-shipping-koli';
		            $table->isScrollable= false;
		            $table->numbering 	= true;
		            $table->pagination 	= false;
		            $table->header     	= array('KOLI CODE', 'SUPPLIER', 'DESTINATION', 'STATUS', 'QTY');
		            $table->source     	= site_url($this->class_name.'/getform/table-shipping/'.$id);
		            return $table->generateWrapper();
		        } else {
		        	$model = $this->shipping_koli_model;
		        	$table
			            ->setModel($model)
			            ->setNumbering()
			            ->with(array('koli','storekoli','supplier'))
			            ->where('shipping_id', $id)
			            ->select('koli_code, nama_supplier, nama_cabang, koli_status as status, qty')
                        ->edit_column('status', '$1', 'transaksi::getTransStatus(status)');
		            echo $table->generate();
		        }
    			break;
    		

    		case 'form-koli' :
    			$this->load->model('store_model');
    			if($id){

    			}else{
    				
    			}

    			$poStores = $this->po_store_model->with(array('po','store'))->where_in('postore_id',$param['ids'])->get()->result();
    			$postore_id = $param['ids'][0];    			
    			$postore = $poStores[0];

    			$param['postores'] 			= $poStores;
    			$param['data']['koli_code'] = code::getCode('koli',$postore_id);

    			$param['title']			= 'Create Koli';
    			$param['form_action'] 	= form_open(
                                                site_url($this->class_name.'/proses_koli'),
                                                '', array(
                                                	'id' => $id,
                                                	'postore_ids'=>json_encode($param['ids'])
                                                )
                                            );
    			$option = array($postore->store_id => $postore->nama_cabang);
    			$param['list']['store'] = form_dropdown('data[store_id]', $option, $postore->store_id, 'class="bs-select" disabled');

    			$form->portlet          = true;
	            $form->button_back      = true;
	            $form->id               = 'form-koli';
	            $form->param            = $param;
	            $form->url_form         = 'receiving/warehouse/form_koli.tpl';
	            return $form->generate();
    			break;
    		default:
    			# code...
    			break;
    	}
    }

    public function proses_shipping($id = NULL){
        
        $this->db->trans_start();

        // [POINT] update table shipping . truck number
        $data = $this->input->post('data');
        $data['shipping_status'] = transactionstatus::SHIP;
        $this->shipping_model->update($data, $id);

        // [POINT] update status di koli. postore. podetail.prdetail
        $koliIds = $this->koli_model->select('this.koli_id')->with('shipping')->where('shipping_id', $id)->get()->result_array();
        $ids = array_values_index($koliIds, 'koli_id');
        $this->koli_model
            ->where_in('koli_id', $ids)
            ->set('koli_status',transactionstatus::SHIP)
            ->update();

        if($this->db->trans_status()){
            $message = array(
                        'success' => true,     
                        'message' => 'Save Success',
                        //'callback'=> "reloadPageLoading();",        
                    );
        }else{
            $message = array(
                        'success' => false,                        
                        'message' => 'Database Error Occured',
                    );
        }
        $this->db->trans_complete();
        echo json_encode($message);
    }

    public function proses($postore_id){
        

        if(!$postore_id) return;

        $this->load->model(array('purchase_request_detail_model','po_store_model','item_in_model','item_stok_model'));
        $prdetailModel = $this->purchase_request_detail_model;
            $iteminModel = $this->item_in_model;
            $itemStokModel = $this->item_stok_model;

        $this->db->trans_start();
        /*
            data = array of (prdetail_id, qty_received)
        */
        $postore = $this->po_store_model->getById($postore_id)->row_array();
        
        if(!$postore) return;

        $data = $this->input->post('data');
        if(!$data) return;

        foreach ($data as $value) {

            $prdetail = $prdetailModel->getById($value['prdetail_id'])->row_array();

            // UPDATE PURCHASE_REQUEST_DETAIL
            $prdetail_data = array();
            $prdetail_data['qty_received']   = $value['qty_received'];
            $prdetail_data['qty_temp']       = $value['qty_received'];
            $prdetail_data['detail_status']  = prstatus::RECEIVE;
            $prdetailModel->update($prdetail_data, $value['prdetail_id']);

            // INSERT TO ITEM_IN
            $itemin = array();
            $itemin['store_id'] = $postore['store_id'];
            $itemin['item_id']  = $prdetail['item_id'];
            $itemin['price']    = $prdetail['price'];
            $itemin['qty']      = $value['qty_received'];
            $itemin['postore_id'] = $postore_id;
            $this->item_in_model->create($itemin);

            // UPDATE TO ITEM_STOK : cek item_stok ada atau tidak
            $itemstok = $itemStokModel
                        ->where('store_id', $postore['store_id'])
                        ->where('item_id', $prdetail['item_id'])
                        ->get()->row();
            if($itemstok) {
                // menghitung harga item rata-rata. update harga rata-rata per item per store
                $rata = 0;
                if($value['qty_received'] > 0)
                $rata = (($itemstok->stok_store *  $itemstok->price_avg) +
                        ($prdetail['price'] * $value['qty_received'])) / 
                        ($itemstok->stok_store + $value['qty_received']);

                $itemStokModel->set('stok_total','stok_total+'.$value['qty_received'], FALSE);
                $itemStokModel->set('stok_gudang','stok_gudang+'.$value['qty_received'], FALSE);
                $itemStokModel->set('stok_store','stok_store+'.$value['qty_received'], FALSE);
                $itemStokModel->set('price_avg', $rata);
                $itemStokModel->where('store_id', $postore['store_id']);
                $itemStokModel->where('item_id', $prdetail['item_id']);
                $itemStokModel->update();
            }else{
                $itemStok_data['stok_total'] = $value['qty_received'];
                $itemStok_data['stok_gudang'] = $value['qty_received'];
                $itemStok_data['stok_store'] = $value['qty_received'];
                $itemStok_data['store_id'] = $postore['store_id'];
                $itemStok_data['item_id'] = $prdetail['item_id'];
                $itemStok_data['price_avg'] = $prdetail['price'];
                $itemStokModel->create($itemStok_data);
            }

        }

        // UPDATE PO_STORE
        $postore_data['postore_status'] = 1; //CLOSED
        $postore_data['date_received']  = date::now_sql();
        $this->po_store_model->update($postore_data, $postore_id);

        if($this->db->trans_status()){
            $message = array(
                        'success' => true,     
                        'message' => 'Save Success',
                        'callback'=> "reloadPageLoading()",        
                    );
        }else{
            $message = array(
                        'success' => false,                        
                        'message' => 'Database Error Occured',
                    );
        }

        
        $this->db->trans_complete();

        echo json_encode($message);

    }



}


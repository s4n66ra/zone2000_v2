<?php

/**
 * Description of bbm
 *
 * @author SANGGRA HANDI
 */
class pulau extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }
    
    public function index() {
        
        $data['title'] = 'Master Island';
        $data['data_source'] = base_url($this->class_name . '/load');

		$data['button_group'] = $this->getAvailableButtons();
        $data['button_right'] = $this->getTools();

 		/* INSERT LOG */
		$this->access_right->activity_logs('view','Master Island');
        /* END OF INSERT LOG */
        $data['assets_url'] = $this->config->item('assets_url');
        $data['sidebar'] = $this->access_right->menu();

        $data['content'] = 'base/page_content.tpl'; 
        $data['nama_pegawai'] = $this->currentUsername;      
        
        $data['input_kd_pulau'] = form_input('kd_pulau', '', 'class="form-control input-inline input-medium"');
        $data['input_nama_pulau'] =  form_input('nama_pulau', '', 'class="form-control input-inline input-medium"');     
        $this->twig->display('index.tpl', $data);
    }

    public function load($page = 0) {
        $this->load->library("custom_table");

        $table = new stdClass();
        $header[0] = array(
                            "ISLAND CODE",1,1,
                            "ISLAND NAME", 1, 1, 
                        );

        if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
            $header[0] = array(
                            "ISLAND CODE",1,1,
                            "ISLAND NAME", 1, 1, 
                            "ACTION",1,1,
                            );
        }

        $table->header = $header;
        $table->id     = 't_bbm';
        $table->align = array('tanggal_bbm' => 'center', 'kd_bbm' => 'center','kd_purchase_order'=>'center','supplier'=>'center', 'aksi' => 'left');
        $table->style = "table table-striped table-bordered table-hover datatable dataTable";
        $table->model = "pulau_model->data_table";
        //$table->limit = $this->limit;
        $table->page = $page;
        $data = $this->custom_table->generate_ajax($table);

        echo json_encode($data);
    }

    public function proses() {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('nama_pulau', 'Nama Pulau', 'required|trim');
            $this->form_validation->set_rules('kd_pulau', 'Kode Pulau', 'required|trim');
            $this->form_validation->set_rules('id_negara', 'Nama Negara', 'required|trim');


            if ($this->form_validation->run()) {
                $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');

                $id = $this->input->post('id');

                $data = array(
                    'nama_pulau' => $this->input->post('nama_pulau'),
                    'kd_pulau'   => $this->input->post('kd_pulau'),
                    'id_negara'  => $this->input->post('id_negara')
                );

                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */

                if ($id == '') {
                    if ($this->pulau_model->create($data)) {
                        $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh_filter()');
                        /* INSERT LOG */
                        $this->access_right->activity_logs('add','Tambah BBM');
                        /* END OF INSERT LOG */
                    }
                } else {
                    if ($this->pulau_model->update($data, $id)) {
                        $message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', 'refresh_filter()');
                        /* INSERT LOG */
                        $this->access_right->activity_logs('edit','Edit BBM');
                        /* END OF INSERT LOG */
                    }
                }
            } else {
                $message = array(false, 'Terjadi Kesalahan', validation_errors(), '');
            }
            echo json_encode($message);
        } else {
            $this->access_right->redirect();
        }
    }

    public function proses_delete() {
        $id = $this->input->post('id');
        $this->access_right->otoritas('delete', true);
        if ($this->access_right->otoritas('delete', true)) {
            $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');
            if ($this->pulau_model->delete($id)) {
                $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', 'refresh_filter()');
                /* INSERT LOG */
                $this->access_right->activity_logs('delete','Delete BBM');
                /* END OF INSERT LOG */
            }
            echo json_encode($message);
        }
    }

    public function add($id = '',$status_delete = 0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->config->load('twig');
            $this->load->model('negara_model');
            $this->load->model('pulau_model');

            $title = 'Tambah Data Pulau';
             $data['form_action'] = $this->class_name . '/proses';
            $data['id'] = $id;
            if ($id != '') {
                $title = 'Edit Data Pulau';

                $db = $this->pulau_model->get_by_id($id);
                if ($db->num_rows() > 0) {
                    $row = $db->row();
                    $data['default'] = $row;
                    $data['def_kd_pulau'] = $row->kd_pulau;
                    $data['def_nama_pulau'] = $row->nama_pulau;
                    $data['def_id_negara'] = $row->id_negara;
                    $def_id_negara = $row->id_negara;
                }
            }

            //echo $id;
            $data['title'] = $title;

            if($status_delete == 0){
                $data['form_action']    = view::form_input($id);
                $button_group[]         = view::button_save();
            }else{

                $data['title'] = 'Delete Data Pulau';
                //$data['form_action'] = '<form action="'.base_url('pulau/proses_delete').'" method="post" id="form_sample_3" class="form-horizontal" novalidate="novalidate"><input name = "id" type="hidden" value="'.$id.'">';
                $data['form_action']    = view::form_delete($id);;
                $data['readonly']       = 'readonly=""';
                $button_group[] = view::button_delete_confirm();
            }
            
            $data['button_group'] = view::render_button_group($button_group);
            $data['form'] = $this->class_name.'/form_pulau.tpl';
            $options_negara = $this->negara_model->options();
            $data['options_negara'] = form_dropdown('id_negara',$options_negara, !empty($def_id_negara) ? $def_id_negara : '', 'class = "form-control"'); 

            $this->twig->display('base/page_form.tpl', $data);


        } else {
            $this->access_right->redirect();
        }
    }

    public function edit($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('edit', true);
        $this->add($id);
    }

    public function delete($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('delete', true);
        $this->add($id,1);
    }

    
}

/* End of file bbm.php */
/* Location: ./application/controllers/bbm.php */


<?php
class menu extends MY_Controller {

    private $menus;    
    private $title = 'Menu Aplikasi';

    public function __construct() {
        parent::__construct();
    }

    public function test(){
        echo 'yes gendes';
    }

    public function index() {
        
        $data['title'] = $this->title;

        $data['button_group'] = array();
        $data['javascript']     = array('menu.js');
        $data['javascript_var'] = array(
                                        'url_action_reordering' => site_url($this->class_name.'/proses_reordering_new'),
                                    );
        $data['sidebar']        = $this->access_right->menu();
        $data['content']        = 'base/page_content.tpl';
        $data['button_group']   = view::render_button_group(array(view::button_add()));
        $data['button_right']   = '<a class="btn blue btn-save-order"><i class="fa fa-check"></i> Save Order</a>';
        $data['table']['main']  = $this->table_main(array('wrapper'));
        $this->twig->display('index.tpl', $data);
    }

    public function table_test(){
        return $this->twig->render('menu/table_menu.tpl');
    }

    private function recursiveRow($layoutMenu, $kd_parent = NULL, $depth = 0, $classInherit = ''){
        $body = '';
        $menu = $this->menus[$layoutMenu['id']];

        if(!$menu && !$layoutMenu['child'])
            return '';

        $kd_menu        = $menu['kd_menu'];
        $kd_grup_menu   = $menu['kd_grup_menu'];
        $group_id       = $kd_grup_menu;
        
        if($depth>0) {
            $parent_id  = ' parent-id='.$kd_parent.' ';
            $classChild = ' child ';
            $classGroup = 'group-'.$kd_parent;
            $classInherit.= 'menu-child-'.$kd_parent.' ';
        }else{
            $classGroup = 'group-'.$kd_parent;
            $classInherit.= 'menu-child-group-'.$kd_parent.' ';
        }

        if ($layoutMenu['child']){
            $classParent = ' parent ';
            // tombol untuk hide child
            $btnSlide    = '<a class="" style="margin-right:10px;"><i class="icon-slide fa fa-angle-down"></i></a>';
        }

        unset($menu['kd_menu']);
        unset($menu['kd_grup_menu']);
        unset($menu['kd_parent']);

        // set class inherit 
        $a = 60;
        $b = 40;
        $indent = ' style="padding-left:'.(($depth*$a)+$b).'px;" ';
        $firstCol = ' first-column ';
        $body.='<tr id="menu-'.$kd_menu.'" menu-id="'.$kd_menu.'" menu-name="'.$menu['url'].'" depth='.$depth.' indent-offset="'.$b.'" indent="'.$a.'" group="'.$classGroup.'" group-id="'.$group_id.'" menu-id="'.$kd_menu.'" '.$parent_id.' class="'.$classGroup.' '.$classParent.' sortable item-menu '.$classChild.' '.$classInherit.'">';            

        $body.='<td></td>';                    
        foreach ( (array)$menu as $j => $td) {
            if($j!='child')
                $body.= '<td class="text-'.$align[$j].$firstCol.'" '.$indent.'>'.$btnSlide.$td.'</td>';
            $indent = '';
            $btnSlide = '';
            $firstCol = '';
        }
        $body.='</tr>';

        if($layoutMenu['child']){
            foreach ($layoutMenu['child'] as $key => $value) {
                $body.= $this->recursiveRow($value, $layoutMenu['id'], $depth+1, $classInherit);
            }
            return $body;
        }
        else
            return $body;
    }

    public function checkMenu($layout, $menus){
        $tmp = array();
        foreach ($layout as $key => $value) {
            if($value['child'])
                $tmp[] = $value['child'];
        }

        $tmp = json_encode($tmp);

        foreach ($menus as $key => $value) {
            $find1 = strpos($tmp, '"id":"'.$value['kd_menu'].'"') ; 
            $find2 = strpos($tmp, '"id":'.$value['kd_menu']) ; 
            if( $find1==false && $find2==false ) {
                $layout[0]['child'][]= array('id'=>$value['kd_menu']);
            }
        }
        
        return $layout;

    }

    public function table_main($options = array()){
        $this->load->model('config_model');
        $menuModel = $this->menu_model;

        // pengelompokan menu
        $tmp = $menuModel->order_by('menu_urutan')->get()->result_array();
        $menus = array();
        foreach($tmp as $value){
            $tmp = array();
            $tmp['name']        = view::icon($value['icon']).' ' .$value['nama_menu'];
            $tmp['deskripsi']   = $value['deskripsi'];
            $tmp['url']         = $value['url'];
            $tmp['action']      = view::btn_group_edit_delete($value['kd_menu']);
            $tmp['kd_menu']     = $value['kd_menu'];
            $tmp['kd_grup_menu']= $value['kd_grup_menu'];
            $tmp['kd_parent']   = $value['kd_parent'];
            $menus[$value['kd_menu']] = $tmp;
        }
        $this->menus = $menus;

        // mendapatkan layout menu smentara
        // mengganti index layout dengan id grup menu
        $tmp = $this->config_model->get()->row()->menu;
        $tmp = json_decode($tmp, true);
        $tmp = $this->checkMenu($tmp, $menus);
        foreach ($tmp as $value) {
            $tmpLayout[$value['id']] = $value;
        }

        
        // pengelompokan group
        // menambahkan group menu yang tidak ada di layout
        // dan memprioritaskan urutan di group menu daripada di layout
        $layout = array();
        $tmp = $this->menu_group_model->order_by('urutan', 'asc')->get()->result_array();
        $groups = array();
        foreach ($tmp as $key => $value) {
            $kd_grup_menu           = $value['kd_grup_menu'];
            $groups[$kd_grup_menu]  = $value;
            $layout[]               = ($tmpLayout[$kd_grup_menu] ? $tmpLayout[$kd_grup_menu] : array(
                'id'=> $kd_grup_menu));
        }

        // save ke database dulu
        $this->config_model->set('menu', json_encode($layout))->update();
        
        $body.= '
            <style>
                .group-head {
                    background-color:#f5f5f5;
                }
                .item-menu-highlight {
                    background-color:#F8FFD5;
                    height: 40px; 
                }
            </style>
        ';

        $body.='<tbody>';
        foreach ($layout as $head) {
            $group = $groups[$head['id']];
            $kd_grup_menu = $group['kd_grup_menu'];

            $body.= '<tr class="unsortable group group-head" depth="-1" menu-id="'.$kd_grup_menu.'" 
                    group-id="'.$kd_grup_menu.'" group="group" group-name="'.$group['nama_grup_menu'].'"><td colspan="5">';
            $body.= '<a class="" style="margin-right:10px;"><i class="icon-slide fa fa-angle-down"></i></a>';
            $body.= view::icon($group['icon']).' '.$group['nama_grup_menu'];
            $body.= '</td></tr>';
            
            if($head['child'])
                foreach ($head['child'] as $menu) {
                    $body.= $this->recursiveRow($menu, $head['id']);
                }  
        }
        $body.='</tbody>';

        $this->load->library('custom_table');
        $table = $this->custom_table;
        $table->columnWidth = array(40,30,20,10);
        $align = array('action'=>'center');
        $table->id = 'table-menu';
        $table->headerAlign = array( 3=> 'center');
        $table->columns = array('MENU', 'DESCRIPTION', 'URL', 'ACTION');
        $table->data_source = site_url($this->class_name.'/table_main');
        $table->body = $body;
        
        if (in_array('wrapper', $options))
            return $table->generateWithWrapper();
        else
            echo $table->generate();
    }
    
    public function add($id = '',$status_delete = 0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->load->library('custom_form');
            $form = $this->custom_form;

            $this->load->model('menu_group_model');
            $menu_group_options = $this->menu_group_model->options();
            $title = 'Tambah Data Menu';
            $data['id'] = $id;
            if ($id != '') {
                $title = 'Edit Data Menu';

                $row = $this->menu_model->getById($id)->row();
                if($row){
                    $data['default'] = $row;
                    $data['def_nama_menu'] = $row->nama_menu;
                    $data['def_url'] = $row->url;
                    $data['def_deskripsi'] = $row->deskripsi;
                    $def_kd_grup_menu = $row->kd_grup_menu;
                    $def_kd_parent = $row->kd_parent;
                }
                $data['data'] = $row;
            }
            
            $data['title'] = $title;
            
            if($status_delete == 0){
                $data['form_action'] = '<form action="'.base_url($this->class_name.'/proses').'" method="post" id="finput" class="form-horizontal" novalidate="novalidate"><input name = "id" type="hidden" value="'.$id.'">';
            }else{
                $data['title']  = 'Delete Menu';
                $data['form_action'] = '<form action="'.base_url($this->class_name.'/proses_delete').'" method="post" id="finput" class="form-horizontal" novalidate="novalidate"><input name = "id" type="hidden" value="'.$id.'">';
                $form->type     = 'delete';
            }
            
            $data['base_url'] = base_url();
            $data['options_group_menu'] = form_dropdown('group_menu', $menu_group_options, !empty($def_kd_grup_menu) ? $def_kd_grup_menu : '', 'id="group_menu" class="form-control bs-select" data-live-search="true"');
            //$data['options_parent']     = form_dropdown('parent', $this->menu_model->options('Select Parent'), $def_kd_parent, 'id="parent" class="form-control select2" placeholder="Select Parent"');
            $data['options_parent']     = form_dropdown('parent', $this->menu_model->options('Select Parent'), $def_kd_parent, 'id="select-parent" class="select2 form-control" placeholder="Select Parent"');


            $form->portlet          = true;
            $form->button_back      = true;
            $form->id               = 'form-hadiah';
            $form->param            = $data;
            $form->url_form         = $this->class_name.'/form.tpl';
            echo $form->generate();


        } else {
            $this->access_right->redirect();
        }
    }

    public function edit($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->add($id);
    }

    public function proses() {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('nama_menu', 'Nama Menu', 'trim|required');
            //$this->form_validation->set_rules('url', 'URL', 'trim|required');
            $this->form_validation->set_rules('group_menu', 'Group Menu', 'required');

            if ($this->form_validation->run()) {

                $id = $this->input->post('id');
                $parent = $this->input->post('parent');

                // menset parent
                $menuLevel = 0;
                if($parent) {
                    $rowParent = $this->menu_model->getById($parent)->row();
                    $menuLevel = $rowParent->menu_level + 1;
                }

                $data = array(
                    'kd_grup_menu' => $this->input->post('group_menu'),
                    'nama_menu' => $this->input->post('nama_menu'),
                    'deskripsi' => $this->input->post('deskripsi'),
                    'url' => $this->input->post('url'),
                    'icon' => $this->input->post('icon'),
                    'kd_parent' => !empty($parent) ? $parent : null,
                    'menu_level' => $menuLevel,
                );

                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */
                $this->db->trans_start();
                if ($id == '') {
                    $this->menu_model->create($data);
                    $id = $this->menu_model->insert_id;
                } else {
                    $this->menu_model->update($data, $id);
                }
                

                if($parent)
                    $this->menu_model->changeParent($id, $parent);
                else
                    $this->menu_model->changeGroup($id, $this->input->post('group_menu'));

                if($this->db->trans_status()){
                    $message = array('success'=>true, 'message'=>'Save Menu Success');
                }else{
                    $message = array('success'=>false, 'message'=>'Database Error');
                }
                $this->db->trans_complete();

            } else {
                $message = array('success'=>false, 'message'=>validation_errors());
            }
            
            echo json_encode($message);
        } else {
            $this->access_right->redirect();
        }
    }

    public function proses_reordering(){
        $data           = $this->input->post('data');
        $kd_grup_menu   = $this->input->post('group_id');
        $kd_parent      = $this->input->post('parent_id');
        //file_put_contents('holahola.txt', json_encode($_POST));

        $this->db->trans_start();
        $message = array('success'=>false, 'message'=> 'Update Failed');

        if($data) {
            $this->load->model('menu_model');
            $menuLevel = 0;
            if($kd_parent){
                $menuLevel = $this->menu_model->getById($kd_parent)->row()->menu_level;
                $menuLevel++;
            }

            foreach ($data as $urutan => $id) {
                $this->menu_model
                    ->set('menu_urutan', $urutan)
                    ->set('kd_grup_menu', $kd_grup_menu)
                    ->set('kd_parent', $kd_parent)
                    ->set('menu_level', $menuLevel)
                    ->findByPk($id)->update();

                // $this->menu_model
                //     ->set('menu_level', 'menu_level+1', FALSE)
                //     ->where_in('kd_parent', $id)->update();

            }

            $this->menu_model
                ->set('kd_grup_menu', $kd_grup_menu)
                ->where_in('kd_parent', $data)->update();

            if($this->db->trans_status()){
                $message = array('success'=>true, 'message'=> 'Update Success', 'callback'=>'refreshNewCustomTable()');
            }
        }

        $this->db->trans_complete();
        echo json_encode($message);
    }

    public function proses_reordering_new(){
        $this->load->model('config_model');
        $data = $this->input->post('data');
        $message = array('success'=>false, 'message'=> 'sadsad');
        // echo json_encode($message);return;

        $this->db->trans_start();
        $this->config_model->set('menu', $data)->update();
        if($this->db->trans_status()){
            $message = array('success'=>true, 'message'=> 'Update Success', 'callback'=>'refreshNewCustomTable()');
        }
        $this->db->trans_complete();
        echo json_encode($message);

    }

    public function delete($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->add($id,$status_delete = 1);
    }

    public function proses_delete() {
        $id = $this->input->post('id');
        $this->access_right->otoritas('delete', true);
        if ($this->access_right->otoritas('delete', true)) {
            $this->db->trans_start();
            $this->menu_model->delete($id);
            if($this->db->trans_status()){
                $message = array('success'=>true, 'message'=>'Delete Success', 'callback'=>'refreshNewCustomTable()');
            }else{
                $message = array('success'=>false, 'message'=>'Database Error');
            }
            $this->db->trans_complete();
            echo json_encode($message);
        }
    }

    public function get_parent() {
        $kode = $this->input->post('kode');
        $selected = $this->input->post('selected');

        $options = array(null => 'NULL');

        if (!empty($kode)) {
            //$data = $this->menu_model->get_data(array('kd_grup_menu' => $kode, 'kd_parent' => null));
            $data = $this->menu_model->get_data(array('kd_grup_menu' => $kode));
            foreach ($data->result() as $value) {
                $options[$value->kd_menu] = $value->nama_menu;
            }
        }

        echo json_encode(hgenerator::array_to_option($options, $selected));
    }

}

?>
<?php

/**
 * Description of bbm
 *
 * @author SANGGRA HANDI
 */
class jenis_mesin extends MY_Controller {

    public function __construct() {
        // Declaration
        parent::__construct();

        $this->load->model(array('kategori_mesin_model','kelas_model','koin_model'));
    }

    public function index() {
        $data['title'] = 'Machine Type Master';

        /* INSERT LOG */
        //$this->access_right->activity_logs('view',$data['title']);
        /* END OF INSERT LOG */

        $data['button_group'] = $this->getAvailableButtonsFullWidth();
        //$data['button_right'] = $this->getTools();
       // $data['button_right'] = $this->getPrintTools();

        $data['table']['main']  = $this->table_main(array('wrapper', 'filter'));
        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'base/page_content.tpl';
    
        $this->twig->display('index.tpl', $data);
    }

    
    public function __index() {
        //echo json_encode($this->jenis_mesin_model->data_table());die;
        $data['title'] = 'Machine Type Master';
        $data['data_source'] = base_url($this->class_name . '/load');
        //print_r($data['data_source']);
        /* INSERT LOG */
        $this->access_right->activity_logs('view',$data['title']);
        /* END OF INSERT LOG */

        $data['button_group'] = $this->getAvailableButtons();
        $data['button_right'] = $this->getTools();

        $data['assets_url'] = $this->config->item('assets_url');
        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'base/page_content.tpl';
        $data['nama_pegawai'] = $this->currentUsername;
    
        $this->twig->display('index.tpl', $data);
    }

    public function table_main($option = array()){
        $this->load->library('Datatable');
        $table = $this->datatable;
        if (in_array('filter', $option)) {
            $table->dataFilter = array(
                                array('kd_jenis_mesin', 'MACHINE ID', 'text'),
                                array('nama_jenis_mesin','MACHINE TYPE NAME','text'),
                                array('nama_kategori_mesin','CATEGORY','text'),
                                array('deskripsi','DESCRIPTION','text'),
                            );
        }
        if (in_array('wrapper', $option)) {
            $table->numbering  = true;
            $table->id         = 'table-shipping';
            $table->isScrollable= false;
            $table->header     = array('MACHINE ID', 'MACHINE TYPE NAME', 'CATEGORY', 'DESCRIPTION', 'ACTION');
            $table->source     = site_url($this->class_name.'/table_main');
            return $table->generateWrapper();
        } else {
            $table
                ->setModel($this->jenis_mesin_model)
                ->with(array('category'))
                ->setNumbering()
                ->select('kd_jenis_mesin, nama_jenis_mesin, nama_kategori_mesin, deskripsi,  id_jenis_mesin')
//                ->edit_column('id_sparepart', '$1', 'view::btn_group_receiving_warehouse(shipping_id)')
                ->order_by('id_jenis_mesin','DESC')
                ->edit_column('id_jenis_mesin', '$1', 'view::btn_group_edit_delete_full_width(id_jenis_mesin)');
            echo $table->generate();
        }

    }

    public function __load($page = 0) {
        $this->load->library("custom_table");

        $table = new stdClass();
        $header[0] = array(
                            //"IMAGE",1,1,
                            "ID",1,1,
                            "MACHINE TYPE NAME", 1, 1,
                            "CATEGORY", 1, 1,
//                             "COIN TYPE", 1, 1,
//                             "CLASS", 1, 1,
                            "DESCRIPTION", 1, 1,
                        );

        if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
            $header[0] = hgenerator::merge_array_raw(
                            $header[0],
                            array("ACTION",1,1)
                        );
        }

        $table->header = $header;
        $table->id     = 't_mesin';
        $table->align = array('tanggal_bbm' => 'center', 'kd_bbm' => 'center','kd_purchase_order'=>'center','supplier'=>'center', 'aksi' => 'center');
        $table->style = "table table-striped table-bordered table-hover datatable dataTable";
        $table->model = "jenis_mesin_model->data_table";
        $table->limit = $this->limit;
        $table->page = $page;
        $data = $this->custom_table->generate_ajax($table);
        echo json_encode($data);
    }

    public function add($id = '', $status_delete=0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $title = 'Tambah Data';
            $data['form_action'] = $this->class_name . '/proses';

            $data['id'] = $id;
            if ($id != '') {
                $title = 'Edit Data';
                $row = $this->jenis_mesin_model->get_by_id($id)->row();
                $data['data'] = $row;
            }

            $data['title']      = $title;
            $data['assets_url'] = $this->config->item('assets_url');
            $data['sidebar']    = $this->access_right->menu();
            $data['form']       = $this->class_name.'/form.tpl';

            $this->load->model('itemtype_model');

            // dropdown kelas, jenis koin , kategori mesin         
            /* $data['list_kelas']     =   form_dropdown(
                                            'data[id_kelas]', 
                                            $this->kelas_model->options_empty(), 
                                            $row->id_kelas, 
                                            'class="form-control select2" data-placeholder="Select Machine Class"'
                                        ); June 29th wha*/
$htmlOptions            =  'onchange="refreshList(this)" target="#machine-id" target-type="text" 
                                            data-source="'.site_url($this->class_name.'/getcode').'"
                                            class="form-control bs-select" data-live-search="true" data-placeholder="Select Machine Type"';
            $data['list_kategori']  =   form_dropdown(
                                            'data[id_kategori_mesin]', 
                                            $this->kategori_mesin_model->options_empty(), 
                                            $row->id_kategori_mesin, $htmlOptions
                                        );
            /* $data['list_coin']  =   form_dropdown(
                                        'data[jenis_koin]', 
                                        $this->koin_model->options_empty(), 
                                        $row->jenis_koin, 
                                        'class="form-control select2" data-placeholder="Select Coin Type"'
                                    ); June 29th wha*/

            $opt_itemtype = $this->itemtype_model->options();

            $data['list_typein']  =   form_dropdown(
                                        'data[type_in]', 
                                        $opt_itemtype, 
                                        $row->type_in, 
                                        'class="bs-select form-control" data-placeholder="Select Input Type"'
                                    );

            $data['list_typeout']  =   form_dropdown(
                                        'data[type_out]', 
                                        $opt_itemtype, 
                                        $row->type_out, 
                                        'class="bs-select form-control" data-placeholder="Select Output Type"'
                                    );

            $button_group   = array();
            $button_group[] = view::button_back();

            if($status_delete == 0){
                $data['form_action']    = view::form_input($id);
                $button_group[]         = view::button_save();
            }else{
                $data['title']          = 'Delete Store';
                $data['form_action']    = view::form_delete($id);;
                $data['readonly']       = ' readonly="" ';
                $button_group[]         = view::button_delete_confirm();
            }

            $data['button_group'] = view::render_button_group($button_group, array('class'=>'pull-right'));
            $this->twig->display('base/page_form.tpl', $data);
        } else {
            $this->access_right->redirect();
        }
    }

    public function getCode($jenis_mesin = NULL){
        if (!$jenis_mesin)
            return;
        echo code::getCode('machine_id_kategori', $jenis_mesin);
    }

    public function edit($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('edit', true);
        $this->add($id);
    }

    public function delete($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('delete', true);
        $this->add($id,$status_delete = 1);
    }

    public function proses() {
        $data = $this->input->post('data');

        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {

            $this->form_validation->set_rules('data[kd_jenis_mesin]', 'Machine Type Code', 'required|trim');
            $this->form_validation->set_rules('data[nama_jenis_mesin]', 'Machine Type Name', 'required|trim');
            $this->form_validation->set_rules('data[id_kategori_mesin]', 'Category', 'required|trim');
//             $this->form_validation->set_rules('data[id_kelas]', 'Machine Class', 'required|trim');June 29th wha
//             $this->form_validation->set_rules('data[jenis_koin]', 'Coin Type', 'required|trim');June 29th wha
            

            if ($this->form_validation->run()) {
                $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');

                $id = $this->input->post('id');
                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */
                
                if ($id == '') {
                    //--------- raise code kategori mesin----------------
                    $jenis_mesin = $data['id_kategori_mesin'];
                    code::getCodeAndRaise('machine_id_kategori', $jenis_mesin);
                    if ($this->jenis_mesin_model->create($data)) {
                        $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh_filter()');
                        /* INSERT LOG */
                        $this->access_right->activity_logs('add','Tambah Cabang');
                        /* END OF INSERT LOG */
                    }
                } else {

                    if ($this->jenis_mesin_model->update($data, $id)) {
                        $message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', 'refresh_filter()');
                        /* INSERT LOG */
                        $this->access_right->activity_logs('edit','Edit BBM');
                        /* END OF INSERT LOG */
                    }
                }
            } else {
               $message = array(false, 'Terjadi Kesalahan', validation_errors(), '');
            }
            echo json_encode($message);
        } else {
            $this->access_right->redirect();
        }
    }

    public function proses_delete() {
        $id = $this->input->post('id');
        $this->access_right->otoritas('delete', true);
        if ($this->access_right->otoritas('delete', true)) {
            $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');
            if ($this->jenis_mesin_model->delete($id)) {
                $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', 'refresh_filter()');
                /* INSERT LOG */
                $this->access_right->activity_logs('delete','Delete BBM');
                /* END OF INSERT LOG */
            }
            echo json_encode($message);
        }
    }



}

/* End of file bbm.php */
/* Location: ./application/controllers/bbm.php */

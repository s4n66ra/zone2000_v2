<?php

/**
 * Description of Divisi
 *
 * @author SANGGRA HANDI
 */
class lap_pegawai extends My_Controller {


    public function __construct() {
        parent::__construct();
        $this->load->model('lap_pegawai_model');
    }

    
    public function index() {
        $data['title'] = 'Laporan Pegawai Pusat ';

        /* INSERT LOG */
        //$this->access_right->activity_logs('view',$data['title']);
        /* END OF INSERT LOG */

        //$data['button_group'] = $this->getAvailableButtons();
        $data['button_right'] = $this->getPrintTools();

        $data['table']['main']  = $this->table_main(array('wrapper', 'filter'));
        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'base/page_content.tpl';
    
        $this->twig->display('index.tpl', $data);
    }

    public function table_main($option = array(),$st_print = FALSE){
        $this->load->model('lap_pegawai_model');
        $this->load->library('DatatablelapSDM');
        $table = $this->datatablelapsdm;

        /*$this->load->library('Datatable_lapSDM');
        $table = $this->datatable_lapSDM;*/

        if (in_array('filter', $option)) {
            $table->dataFilter = array(
                                array('kd_divisi','NAME','text'),
                                array('nama_divisi', 'DIVISI', 'text'),
                                array('nik', 'ID', 'text'),
                                array('id', 'NIK', 'text'),
                                array('kd_level', 'LEVEL', 'text'),
                            );
        }

        if (in_array('wrapper', $option)) {
            $table->numbering  = true;
            $table->isScrollable= false;
            $table->id         = 'table-divisi';
            $table->header     = array('NAME','ID','NIK','LEVEL','DIVISI',);
            $table->source     = site_url($this->class_name.'/table_main');
            return $table->generateWrapper();
        } else {
            $table
                ->setModel($this->lap_pegawai_model)
                ->with(array('divisi','level'))
                ->setNumbering()
               // ->order_by('no_urut','ASC')
                ->order_by('this.id_divisi','DESC')
                ->where('default_store_id',hprotection::getHO())
                ->select('nama_pegawai,nik,id_karyawan,kd_level,nama_divisi');
            if($st_print == TRUE)
                return $table->getData();
            else
                echo $table->generate();
        }

    }

    public function add($id='',$status_delete = 0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->config->load('twig');
            $this->load->model('item_model');
            $title = 'Form Tambah Divisi';
            $data['form_action'] = $this->class_name . '/proses';

            $data['id'] = $id;
            if($id){
                $title = 'Form Edit divisi';
                $row            = $this->lap_pegawai_model->getById($id)->row();
                $data['data']  = $row;
            }

            $data['title']      = $title;
            $data['assets_url'] = $this->config->item('assets_url');
            $data['sidebar']    = $this->access_right->menu();
            $data['form']       = $this->class_name.'/form.tpl';

            $button_group   = array();
            $button_group[] = view::button_back();
            
            if($status_delete == 0){
                $data['form_action']    = view::form_input($id);
                $button_group[]         = view::button_save();
            }else{
                $data['title']          = 'Delete Divisi';
                $data['form_action']    = view::form_delete($id);;
                $data['readonly']       = 'readonly=""';
                $button_group[]         = view::button_delete_confirm();
            }

            $data['button_group'] = view::render_button_group($button_group);            
            $this->twig->display('base/page_form.tpl', $data);

        } else {
            $this->access_right->redirect();
        }
    }

    

    public function edit($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('edit', true);
        $this->add($id);
    }

    public function delete($id_enc = '') {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('delete', true);
        $this->add($id,$status_delete = 1);
    }

    public function proses() {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('data[nama_divisi]', 'Nama divisi', 'required|trim');
            $this->form_validation->set_rules('data[kd_divisi]', 'Kode divisi', 'required|trim');

            if ($this->form_validation->run()) {
                $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');

                $id = $this->input->post('id');
                $data = $this->input->post('data');
                $this->db->trans_start();
                if ($id){
                    $row            = $this->lap_pegawai_model->getById($id)->row();
                    $dataold  = $row;
                    $this->lap_pegawai_model->update($data, $id);
                }else{
                    $this->lap_pegawai_model->create($data);
                }
                if($this->db->trans_status()){
                    $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh_filter()');
                }else{
                    $message = array(false, 'Terjadi Kesalahan',$this->db->_error_message(), '');
                }
                $this->db->trans_complete();
            } else {
                $message = array(false, 'Terjadi Kesalahan', validation_errors());
            }
            echo json_encode($message);
        } else {
            $this->access_right->redirect();
        }
    }

    public function proses_delete() {
        $id=$this->input->post("id");
        $this->access_right->otoritas('delete', true);
        if ($this->access_right->otoritas('delete', true)) {
            $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');
            if ($this->lap_pegawai_model->delete($id)) {
                $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', 'refresh_filter()');
                /* INSERT LOG */
                $this->access_right->activity_logs('delete','Delete Divisi');
                /* END OF INSERT LOG */
            }
            echo json_encode($message);
        }
    }

    public function getPrintTools(){
        $button_group = array();
        if($this->access_right->otoritas('print')){
            $button_group[] = view::button_export_laporan();
        }
        
        if(true){//export
            //$button_group[] = view::button_export();
        }
        return view::render_button_group_laporan_penjualan($button_group, array(), true);

    }

    public function excel($store_id=''){

        $this->load->model(array('store_model','laporan_payout_model'));  
        /*$this->load->library('DatatablelapSDM');
        $table = $this->datatablelapsdm;
        $table  ->setModel($this->lap_pegawai_model)
                ->with(array('divisi','level'))
                ->setNumbering()
               // ->order_by('no_urut','ASC')
                ->order_by('this.id_divisi','DESC')
                ->select('nama_pegawai,nik,id_karyawan,kd_level,nama_divisi');
        $dataTable = $table->getData();    */  
        $filter = array();

        $data['filter'] = $filter;
        //----------------------------------------------------
        $date = date('Ymd His');
        $header =array('NAME','ID','NIK','LEVEL');

        $data['header'] = $header;
        $data['judul_kecil']    = 'Laporan Pegawai Pusat';
        $data['filename']       = 'Laporan Pegawai Pusat('.$date.')';
        $data['content']        = $this->table_main(array(),TRUE);// $this->laporan_payout_model->data_detail(array("id_cabang" => $store_id));
        $data['selected_store'] = $this->store_model
                                 ->select('nama_cabang')
                                 ->where(array("id_cabang"=>$store_id))
                                 ->get()
                                 ->row();
        //print_r($data['selected_store']);
        $this->load->view('cetak_excel_lap_pegawai',$data);
    
    }  

/*    public function getItemsByIdAndKeywordJSON($itemtype_id=''){
        $this->load->model(array('item_model'));

        $tmp = $this->item_model->getItemsByIdAndKeyword($itemtype_id);
        
        //$tmp = $this->item_model->getItemsByIdAndKeyword(2,'');
        $arr = array();
        foreach ($tmp as $k => $v) {
            if($k != ''){
                $obj = new StdClass();
                $obj->id        = $k;
                $obj->text      = $tmp[$k];
                array_push($arr, $obj);
            }
        }
        echo json_encode($arr);
    }*/





}

/* End of file divisi.php */
/* Location: ./application/controllers/bbm.php */

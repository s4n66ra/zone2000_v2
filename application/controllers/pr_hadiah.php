<?php

/**
 * Description of bbm
 *
 * @author SANGGRA HANDI
 */
class pr_hadiah extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {

        $data['title'] = 'PURCHASE REQEUST MERCHANDISE';
        $data['data_source'] = base_url($this->class_name . '/load');

        /* BUTTON GROUP */
        $button_group    = array(
                                view::button_add()
                            );

        $data['button_group'] = view::render_button_group($button_group);

        /* INSERT LOG */
        $this->access_right->activity_logs('view','PR Hadiah');

        $data['assets_url'] = $this->config->item('assets_url');
        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'base/page_content.tpl';  
        $data['nama_pegawai'] = $this->currentUsername;   
        
        $this->twig->display('index.tpl', $data);
    }

    public function load($page = 0) {
        $this->load->library ( "custom_table" );
        
        $table = new stdClass ();
        $header [0] = array (
                "PR REF",1,1,
                "DATE",1,1,
                "STATUS",1,1,
                "NOTE",1,1,
        );
        
        if ($this->access_right->otoritas ( 'edit' ) || $this->access_right->otoritas ( 'delete' )) {
            $header [0] = hgenerator::merge_array_raw ( $header [0], array (
                    "ACTION",1,1 
            ) );
        }
        
        $table->header = $header;
        $table->id = 't_pr_hadiah';
        $table->align = array (
                'tanggal_bbm' => 'center',
                'kd_bbm' => 'center',
                'kd_purchase_order' => 'center',
                'supplier' => 'center',
                'aksi' => 'center' 
        );
        $table->style = "table table-striped table-bordered table-hover datatable dataTable";
        $table->model = "pr_hadiah_model->data_table";
        $table->limit = $this->limit;
        $table->page = $page;
        $data = $this->custom_table->generate_ajax ( $table );
        
        echo json_encode ( $data );
    }

    public function add($id='',$status_delete = 0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->config->load('twig');

            $title = 'NEW PURCHASE REQUEST MERCHANDISE';
            $data['form_action'] = $this->class_name . '/proses';

            $data['id'] = $id;
            if ($id != '') {
                $title = 'Edit Data';

                $row = $this->pr_hadiah_model->get_by_id($id)->row();
                $data['data'] = $row;
            }

            $data['title']      = $title;
            $data['assets_url'] = $this->config->item('assets_url');
            $data['sidebar']    = $this->access_right->menu();
            $data['form']       = 'pr_hadiah/form.tpl';

            $button_group   = array();
            $button_group[] = view::button_back();

            if($status_delete == 0){
                $data['form_action']    = view::form_input($id);
                $button_group[]         = view::button_save();
            }else{
                $data['title']          = 'Delete PR Hadiah';
                $data['form_action']    = view::form_delete($id);;
                $data['readonly']       = 'readonly=""';
                $button_group[]         = view::button_delete_confirm();
            }

            $data['button_group'] = view::render_button_group($button_group);
            $this->twig->display('base/page_form.tpl', $data);
        } else {
            $this->access_right->redirect();
        }
    }

    public function edit($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('edit', true);
        $this->add($id);
    }

    public function delete($id_enc) {        
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('delete', true);
        $this->add($id,1);
    }

    public function proses() {        
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('data[kd_pr]', 'PR Code', 'required|trim');

            if ($this->form_validation->run()) {
                $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');

                $id = $this->input->post('id');
                $data = $this->input->post('data');

                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */

                if ($id == '') {
                    $data['id_pengirim']    = $this->session->userdata('id_cabang');
                    $data['id_tujuan']      = $this->session->userdata('id_pusat');
                    if ($this->pr_hadiah_model->create($data)) {
                        $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh_filter()');
                        /* INSERT LOG */
                        $this->access_right->activity_logs('add','Tambah Kategori Mesin');
                        /* END OF INSERT LOG */
                    }
                } else {
                    if ($this->pr_hadiah_model->update($data, $id)) {
                        $message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', 'refresh_filter()');
                        /* INSERT LOG */
                        $this->access_right->activity_logs('edit','Edit Kategori Mesin');
                        /* END OF INSERT LOG */
                    }
                }
            } else {
                $message = array(false, 'Terjadi Kesalahan', validation_errors(), '');
            }
            echo json_encode($message);
        } else {
            $this->access_right->redirect();
        }
    }

    public function proses_delete() {
        $id = $this->input->post('id');
        $this->access_right->otoritas('delete', true);
        if ($this->access_right->otoritas('delete', true)) {
            $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '','id : '.$id);
            if ($this->pr_hadiah_model->delete($id)) {
                $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', 'refresh_filter()');
                /* INSERT LOG */
                $this->access_right->activity_logs('delete','Delete pr_hadiah');
                /* END OF INSERT LOG */
            }
            echo json_encode($message);
        }
    }

    public function confirm($act, $id){
        $button_group   = array();
        $button_group[] = view::button_back();
        $button_group[] = view::button_confirm();

        switch ($act) {
            

            case 'send':
                $data['title'] = 'SEND REQUEST';
                $data['status'] = 1;
                break;
            
            case 'revisi':
                $data['title'] = 'REVISI REQUEST';
                $data['status'] = 2;
                break;

            case 'reject':
                $data['title'] = 'REJECT REQUEST';
                $data['status'] = 3;
                break;

            case 'approve':
                $data['title'] = 'APPROVE REQUEST';
                $data['status'] = 4;
                break;

            case 'cancel':
                $data['title'] = 'CANCEL REQUEST';
                $data['status'] = 7;
                break;

            default:
                # code...
                break;
        }
        $data['hide_kd']        = true;
        $data['button_group']   = view::render_button_group($button_group);
        $data['form_action']    = view::form($id,'pr_hadiah/confirm_proses');
        $data['form'] = 'pr_hadiah/form.tpl';
        $this->twig->display('base/page_form.tpl', $data);

    }

    public function confirm_proses(){
        $id     = $this->input->post('id');
        $status = $this->input->post('status');
        $data   = $this->input->post('data');
        $this->pr_hadiah_model->update($data, $id);

        $message = array(false, 'Proses gagal', 'Proses Gagal', '','id : '.$id);
        if($this->pr_hadiah_model->proceed($id, $status)){        
            // refresh page content berguna untuk me-refresh page content dengan parameter url data-source, id_element, 
            // id-element default = page-content
            // data-source default= diambil dari attr data-source pada id element
            $message = array(true, 'Proses Berhasil', 'Proses Berhasil.', 'load_page()');
        }
        echo json_encode($message);
    }

    

    public function detail($id){
        // id : id_pr_detail

        $data['title'] = 'PURCHASE REQEUST MERCHANDISE';
        
        $data['data_source']        = base_url($this->class_name . '/load_detail/'.$id);
        $data['data_source_page']   = base_url($this->class_name . '/detail/'.$id);
        $pr                     = $this->pr_hadiah_model->get_by_id($id)->row();
        $data['data']           = $pr;

        /* BUTTON GROUP */
        $button_group    = array(
                                view::button_add_detail(array('id'=>$id))
                            );

        $data['button_group'] = view::render_button_group(array(
                                    view::button_add_detail(array('id'=>$id))
                                ));
        $data['button_group_2'] =view::render_button_group(array(
                                    view::button_back_detail($id),
                                    view::button_deliver_request($id),
                                    view::button_receive_request($id),                                    
                                    view::button_reject_request($id),
                                    view::button_revisi_request($id),
                                    view::button_approve_request($id),
                                    view::button_cancel_request($id),
                                    view::button_send_request($id),
                                ));

        // view keterangan status pr dan revisi pr
        $data['status_pr']  = view::render_status_pr($data['data']->status_pr);
        $data['revisi_pr']  = view::render_revisi_pr($data['data']->count_revisi);


        /* INSERT LOG */
        $this->access_right->activity_logs('view','PR Hadiah');

        $data['readonly'] = "readonly";
        $data['form_info'] = 'pr_hadiah/form_info.tpl';

        if($this->input->is_ajax_request()){
            $this->twig->display('base/page_content_detail.tpl',$data);
        }else{
            $data['assets_url'] = $this->config->item('assets_url');
            $data['sidebar'] = $this->access_right->menu();
            $data['content'] = 'base/page_content_detail.tpl'; 
            $this->twig->display('index.tpl', $data);    
        }
        
    }

    public function load_detail($id_pr = '' ,$page = 0){
        $this->load->library ( "custom_table" );
        
        $table = new stdClass ();
        $header [0] = array (
                "PR REF",1,1,
                "DESCRIPTION",1,1,
                "QTY",1,1,
                "PRICE",1,1,
                "TOTAL",1,1,
        );
        
        if ($this->access_right->otoritas ( 'edit' ) || $this->access_right->otoritas ( 'delete' )) {
            $header [0] = hgenerator::merge_array_raw ( $header [0], array (
                    "ACTION",1,1 
            ) );
        }
        
        $table->header  = $header;
        $table->id      = 't_pr_hadiah_detail';
        $table->style   = "table table-striped table-bordered table-hover datatable dataTable";
        $table->model   = "pr_hadiah_model->data_table_detail/$id_pr";
        $table->limit   = $this->limit;
        $table->page    = $page;
        $data = $this->custom_table->generate_ajax ( $table );
        
        echo json_encode ( $data );
    }

    public function add_detail($id_pr='', $id='', $status_delete = 0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->config->load('twig');

            $title = 'NEW PURCHASE REQUEST MERCHANDISE';
            $data['form_action'] = $this->class_name . '/proses_detail';

            $data['id'] = $id;
            if ($id != '') {
                $title = 'Edit Data';
                $row = $this->pr_hadiah_model->get_detail_by_id($id)->row();
                $data['data'] = $row;
            } else{
                // input new . set id_pr
                $data['data']['id_pr'] = $id_pr;
                //print_r($data);die;
            }

            $data['title']      = $title;
            $data['assets_url'] = $this->config->item('assets_url');
            $data['sidebar']    = $this->access_right->menu();
            $data['form']       = 'pr_hadiah/form_detail.tpl';

            // dropdown 
            $this->load->model(array('hadiah_model'));
            $htmlOptions        = 'class="form-control select2me" data-placeholder="Select Merchandise" '. ($status_delete==1 ? 'disabled' : '');
            $data['list_item']  = form_dropdown('data[id_hadiah]', $this->hadiah_model->options_empty(), $row->id_hadiah, $htmlOptions);

            $button_group   = array();
            $button_group[] = view::button_back();

            if($status_delete == 0){
                $data['form_action']    = view::form_input_detail($id);
                $button_group[]         = view::button_save();
            }else{
                $data['title']          = 'Delete Detail';
                $data['form_action']    = view::form_delete_detail($id);;
                $data['readonly']       = 'readonly=""';
                $button_group[]         = view::button_delete_confirm();
            }

            $data['button_group'] = view::render_button_group($button_group);
            $this->twig->display('base/page_form.tpl', $data);
        } else {
            $this->access_right->redirect();
        }
    }

    public function edit_detail($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('edit', true);
        $this->add_detail('', $id);
    }

    public function delete_detail($id_enc) {        
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('delete', true);
        $this->add_detail('',$id,1);
    }

    public function proses_detail() {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('data[id_pr]', 'PR Code', 'required|trim');

            if ($this->form_validation->run()) {
                $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');

                $id     = $this->input->post('id');
                $data   = $this->input->post('data');

                if ($id == '') {
                    // setting from session
                    $this->load->model('hadiah_model');
                    $data['jml_diajukan']   = $data['jumlah'];
                    $data['harga']          = $this->hadiah_model->get_by_id($data['id_hadiah'])->row()->harga;
                    if ($this->pr_hadiah_model->create_detail($data)) {
                        $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh_filter()');
                        /* INSERT LOG */
                        $this->access_right->activity_logs('add','Tambah Kategori Mesin');
                        /* END OF INSERT LOG */
                    }
                } else {
                    if ($this->pr_hadiah_model->update_detail($data, $id)) {
                        $message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', 'refresh_filter()');
                        /* INSERT LOG */
                        $this->access_right->activity_logs('edit','Edit Kategori Mesin');
                        /* END OF INSERT LOG */
                    }
                }
            } else {
                $message = array(false, 'Terjadi Kesalahan', validation_errors(), '');
            }
            echo json_encode($message);
        } else {
            $this->access_right->redirect();
        }
    }

    public function proses_delete_detail() {
        $id = $this->input->post('id');
        $this->access_right->otoritas('delete', true);
        if ($this->access_right->otoritas('delete', true)) {
            $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '','id : '.$id);
            if ($this->pr_hadiah_model->delete_detail($id)) {
                $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', 'refresh_filter()');
                /* INSERT LOG */
                $this->access_right->activity_logs('delete','Delete pr_hadiah');
                /* END OF INSERT LOG */
            }
            echo json_encode($message);
        }
    }

    public function proceed($id = 0, $status = 0){
        /*
            memproses PR.
            dari draft - pengajuan
            dari revisi - pengajuan

        */
        //print_r($this->session->userdata('user'));
        //$this->output->enable_profiler(TRUE);

        // should be form validation

        // id_pr yang akan dirubah statusnya
        if($this->pr_hadiah_model->proceed($id, $status)){
            //$this->detail($id);
            return true;
        }else{
            return false;
        }
    }



}
/* End of file bbm.php */
/* Location: ./application/controllers/bbm.php */



<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class setting extends MY_Controller {

	public function index()
	{
		$this->main();
	}

	public function main(){
		$data['title'] = 'SETTING';
        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'base/page_content.tpl';     
        $data['content_body'] = $this->class_name.'/page_main.tpl';

        $data['page']['expired'] = $this->getForm('expired');

        $this->twig->display('index.tpl', $data);
	}

	public function getForm($type=NULL){
		$this->load->library('custom_form');
		$form = new custom_form();
		$this->load->model(array(
								'status_model','status_pr_model','status_po_model', 'status_service_model', 'status_repair_model'
							));

		switch ($type) {
			case 'expired':
				$data['pr']['form'] = $this->getForm('expired-pr');
				$data['po']['form'] = $this->getForm('expired-po');
				$data['service']['form'] = $this->getForm('expired-service');
				$data['repair']['form'] = $this->getForm('expired-repair');
				return $this->twig->render($this->class_name.'/page_expired.tpl', $data);
				break;

			case 'expired-pr' :

				// mendapatkan expired : waiting for review(1)
				$temp = $this->status_pr_model->with(array('expired','status'))
										->get()->result_array();
				foreach ($temp as $key => $value) {
					$data[$value['statuspr_id']] = $value['expired'];
				}

				$param['data'] = $data;
				$param['form_action'] = form_open(site_url($this->class_name.'/proses_expired_pr'));

				$form->param 	= $param;
                $form->url_form = $this->class_name.'/expired/form_pr.tpl';
                return $form->generate();
				break;

			case 'expired-po' :

				// mendapatkan expired : waiting for review(1)
				$temp = $this->status_po_model->with(array('expired','status'))
										->where('statuspo_id', 0) // purchase order = 0 / open
										->get()->result_array();
				foreach ($temp as $key => $value) {
					$data[$value['itemtype_id']] = $value['expired'];
				}

				$param['data'] = $data;
				$param['form_action'] = form_open(site_url($this->class_name.'/proses_expired_po'));

				$form->param 	= $param;
                $form->url_form = $this->class_name.'/expired/form_po.tpl';
                return $form->generate();
				break;
			
			case 'expired-service' :

				// mendapatkan expired : waiting for review(1)
				$temp = $this->status_service_model->with(array('expired','status')) // type service request
										->get()->result_array();

				$param['data'] = $temp;
				$param['form_action'] = form_open(site_url($this->class_name.'/proses_expired_service'));

				$form->param 	= $param;
                $form->url_form = $this->class_name.'/expired/form_service.tpl';
                return $form->generate();
				break;

			case 'expired-repair' :

				// mendapatkan expired : waiting for review(1)
				$temp = $this->status_repair_model->with(array('expired','status')) // type repair request
										->get()->result_array();

				$param['data'] = $temp;
				$param['form_action'] = form_open(site_url($this->class_name.'/proses_expired_repair'));

				$form->param 	= $param;
                $form->url_form = $this->class_name.'/expired/form_repair.tpl';
                return $form->generate();
				break;

			default:
				# code...
				break;
		}
	}

	public function proses_expired_pr(){
		$post = $this->input->post();
		$data = $post['data'];


		$this->load->model(array('status_pr_model','status_expired_model'));

		$this->db->trans_start();
		$statusPr = $this->config->item('status_pr');
		foreach ($statusPr as $key => $status) {
			if($data[$key]){
				$statuspr_id = $key;
				$expired = $data[$key];

				$statusExpired = $this->status_expired_model
								->with('status_pr')
								->where('statuspr_id', $statuspr_id)
								->get()->row();


				if($statusExpired){
					// jika data di table status_expired sudah ada. edit
					$this->status_expired_model
						->where('statusexpired_id', $statusExpired->statusexpired_id)
						->set('expired', $expired)
						->update();
				}else{
					// jika belum ada. create baru
					$tmp = $this->status_pr_model->where('statuspr_id', $statuspr_id)->get()->row();
					$temp['status_id'] = $tmp->status_id;
					$temp['expired'] = $expired;
					$this->status_expired_model->create($temp);

				}

			}
		}

		if($this->db->trans_status()){
			$message = array(
						'success'=>true,
						'message'=>'Save Success'
						);
		}else{
			$message = array(
						'success'=>false,
						'message'=>'Database Error'
						);
		}

		$this->db->trans_complete();
		echo json_encode($message);
	}

	public function proses_expired_po(){
		$post = $this->input->post();
		$data = $post['data'];
		//$data = array(7=>90, 2=>5,3=>4);
		/* data : array of(itemtype_id, expired_days)
			(1, 90), (2, 100), (3, 400), dst
		*/

		$this->load->model(array('status_po_model','status_expired_model'));

		// get status_id untuk po_status = 0 (open)
		$status_id = $this->status_po_model->getById(0)->row()->status_id;
		

		$this->db->trans_start();
		$listItemType = $this->config->item('type_item');
		foreach ($listItemType as $key => $name) {
			// key - name ===== 1 - Machine , 2 - Merchandise, dst
			if($data[$key]){
				$itemtype_id = $key;
				$expired = $data[$key];

				$statusExpired = $this->status_expired_model
								->with(array('status_po','itemtype'))
								->where('statuspo_id', 0) // draft. po open
								->where('this.itemtype_id', $itemtype_id)
								->get()->row();

				
				if($statusExpired){
					// jika data di table status_expired sudah ada. edit
					$this->status_expired_model
						->where('statusexpired_id', $statusExpired->statusexpired_id)
						->set('expired', $expired)
						->update();
				}else{
					// jika belum ada. create baru
					$temp['status_id'] = $status_id;
					$temp['expired'] = $expired;
					$temp['itemtype_id'] = $itemtype_id;
					$this->status_expired_model->create($temp);

				}

			}
		}

		if($this->db->trans_status()){
			$message = array(
						'success'=>true,
						'message'=>'Save Success'
						);
		}else{
			$message = array(
						'success'=>false,
						'message'=>'Database Error'
						);
		}

		$this->db->trans_complete();
		echo json_encode($message);
	}

	public function proses_expired_service(){
		$post = $this->input->post();
		$data = $post['data'];


		$this->load->model(array('status_service_model','status_expired_model'));
		$this->db->trans_start();
		
		foreach ($data as $statusservice_id => $expired) {
			
			$statusExpired = $this->status_expired_model
							->with('status_service')
							->where('statusservice_id', $statusservice_id)
							->get()->row();


			if($statusExpired){
				// jika data di table status_expired sudah ada. edit
				$this->status_expired_model
					->where('statusexpired_id', $statusExpired->statusexpired_id)
					->set('expired', $expired)
					->update();
			}else{
				// jika belum ada. create baru
				$tmp = $this->status_service_model->where('statusservice_id', $statusservice_id)->get()->row();
				$temp['status_id'] = $tmp->status_id;
				$temp['expired'] = $expired;
				$this->status_expired_model->create($temp);

			}
		}

		if($this->db->trans_status()){
			$message = array(
						'success'=>true,
						'message'=>'Save Success'
						);
		}else{
			$message = array(
						'success'=>false,
						'message'=>'Database Error'
						);
		}

		$this->db->trans_complete();
		echo json_encode($message);
	}

	public function proses_expired_repair(){
		$post = $this->input->post();
		$data = $post['data'];

		$this->load->model(array('status_repair_model','status_expired_model'));
		$this->db->trans_start();
		
		foreach ($data as $statusrepair_id => $expired) {
			
			$statusExpired = $this->status_expired_model
							->with('status_repair')
							->where('statusrepair_id', $statusrepair_id)
							->get()->row();


			if($statusExpired){
				// jika data di table status_expired sudah ada. edit
				$this->status_expired_model
					->where('statusexpired_id', $statusExpired->statusexpired_id)
					->set('expired', $expired)
					->update();
			}else{
				// jika belum ada. create baru
				$tmp = $this->status_repair_model->where('statusrepair_id', $statusrepair_id)->get()->row();
				$temp['status_id'] = $tmp->status_id;
				$temp['expired'] = $expired;
				$this->status_expired_model->create($temp);

			}
		}

		if($this->db->trans_status()){
			$message = array(
						'success'=>true,
						'message'=>'Save Success'
						);
		}else{
			$message = array(
						'success'=>false,
						'message'=>'Database Error'
						);
		}

		$this->db->trans_complete();
		echo json_encode($message);
	}

}

/* End of file setting.php */
/* Location: ./application/controllers/setting.php */
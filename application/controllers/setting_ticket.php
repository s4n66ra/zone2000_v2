<?php

/**
 *
 *
 * @author ROFID RAHMADI
 */
class setting_ticket extends MY_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model(array('setting_ticket_model'));
    }

    public function index() {

        /*$data['button_group'] = $this->getAvailableButtons();
        $data['button_right'] = $this->getTools();*/
        $data['title'] = 'STATUS';
        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'base/page_content.tpl'; 
        
        $this->load->library('Datatable');
        $table = $this->datatable;
        //$data['st_extend2'] = TRUE;
        $data['table']['main'] = $this->table_main(array('wrapper','filter'));

        $this->twig->display('index.tpl', $data);

    }

    public function table_main($option = array()){
        $this->load->library('Datatable');
        $table = $this->datatable;
        if (in_array('filter', $option)) {
            $this->load->model('transactiontype_model');
            $listOptions = $this->transactiontype_model->options_empty_name();
            $table->dataFilter = array(
                                    array('transactiontype_name', 'Transaction Type', 'list', $listOptions),
                                );
        }

        if (in_array('wrapper', $option)) {
            $table->numbering  = true;
            $table->id         = 'setting-ticket';
            $table->isScrollable= false;
            $table->header     = array('KATEGORI', 'VALUE', 'ACTION');
            $table->source     = site_url($this->class_name.'/table_main');
            return $table->generateWrapper();
        } else {
            $table
                ->setModel($this->setting_ticket_model)
                ->setNumbering()
           //     ->with(array('type'))
             //   ->order_by('status_type', 'asc')
                //->order_by('status_id', 'asc')
                ->select('setting_name,setting_qty,setting_id')
                ->edit_column('setting_qty', '$1', 'hgenerator::number_plaint(setting_qty)')
                ->edit_column('setting_id', '$1', "view::btn_group_edit_delete(setting_id)");
            echo $table->generate();
        }
    }

    public function form($type='add', $id=NULL) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            
            if ($id) {
                $data['title'] = 'Edit Status';
                $row = $this->setting_ticket_model
                            ->getById($id)->row();
                $data['data'] = $row;
                
                switch ($row->status_type) {
                    case 15:
                        // PR
                        $data['status_number'] = $row->statuspr_id;
                        break;
                    case 17:
                        // PO
                        $data['status_number'] = $row->statuspo_id;
                        break;
                    case 10:
                        // SERVICE REQUEST
                        $data['status_number'] = $row->statusservice_id;
                        break;
                    case 11:
                        // REPAIR JOB
                        $data['status_number'] = $row->statusrepair_id;
                        break;
                    default:
                        # code...
                        break;
                }
            }

            
            $button_group   = array();
            $button_group[] = view::button_back();

            switch ($type) {

                case 'add' :
                    $data['title'] = 'New Status';
                    $data['form_action']    = view::form_input($id);
                    $button_group[]         = view::button_save();
                    break;

                case 'edit' :
                    $data['title'] = 'Edit Setting';
                    $data['form_action']    = view::form_input($id);
                    $button_group[]         = view::button_save();
                    break;

                case 'delete':
                    $data['title']          = 'Delete Status';
                    $data['form_action']    = view::form_delete($id);
                    $data['readonly']       = 'readonly=""';
                    $button_group[]         = view::button_delete_confirm();
                    break;
                
                default:
                    # code...
                    break;
            }

            $this->load->model('transactiontype_model');
            $data['list']['status_type'] = form_dropdown(
                                            'data[status_type]', 
                                            $this->transactiontype_model->options_empty(), 
                                            $row->status_type, 
                                            'class="form-control select2" data-placeholder="Select Type"'
                                        );
            $data['form']       = $this->class_name.'/form.tpl';
            $data['button_group'] = view::render_button_group($button_group);
            $this->twig->display('base/page_form.tpl', $data);

        } else {
            $this->access_right->redirect();
        }
    }

    public function add(){
        $this->form('add');
    }

    public function edit($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('edit', true);
        $this->form('edit', $id);
    }

    public function delete($id_enc) {        
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('delete', true);
        $this->form('delete', $id);
    }

    public function proses() {        
        
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('data[setting_qty]', 'Value', 'required|trim');
            $this->form_validation->set_rules('data[setting_name]', 'Kategori', 'required|trim');

            if ($this->form_validation->run()) {

                $this->db->trans_start();

                $id = $this->input->post('id'); 
                $data = $this->input->post('data');
                // status number adalah statuspr_id / statuspo_id / statusservice_id / dll
                $status_number = $this->input->post('status_number');

                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */

                if ($id == '') {
                    // insert to status_model
                    $this->setting_ticket_model->create($data);
                    $setting_id = $this->setting_ticket_model->insert_id;
                    
                } else {
                    $this->setting_ticket_model->update($data, $id);
                }

                if($this->db->trans_status()){
                    $message = array(true, 'Success', 'Save Success', 'refreshTable()');
                }else{
                    $message = array(false, 'Error', 'Database Error', 'refreshTable()');
                }

                $this->db->trans_complete();

            } else {
                $message = array(false, 'Terjadi Kesalahan', validation_errors(), '');
            }
            echo json_encode($message);
        } else {
            $this->access_right->redirect();
        }
    }

    public function proses_delete() {
        $id = $this->input->post('id');
		$this->access_right->otoritas('delete', true);
        if ($this->access_right->otoritas('delete', true)) {
            $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');
            if ($this->status_model->delete($id)) {
                $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', 'refreshTable()');
            }
            echo json_encode($message);
        }
    }

    
}


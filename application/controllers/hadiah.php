<?php

/**
 * Description of bbm
 *
 * @author SANGGRA HANDI
 */


class hadiah extends My_Controller {


    public function __construct() {
        parent::__construct();
        $this->load->model('hadiah_type_model');
    }

    
    public function index() {
        $data['title'] = 'Master Merchandise';

        /* INSERT LOG */
        //$this->access_right->activity_logs('view',$data['title']);
        /* END OF INSERT LOG */

        $data['button_group'] = $this->getAvailableButtons();
        //$data['button_right'] = $this->getTools();
        $data['button_right'] = $this->getPrintTools();
        
        $data['table']['main']  = $this->table_main(array('wrapper', 'filter'));
        $data['st_extend2'] = TRUE;
        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'base/page_content.tpl';
    
        $this->twig->display('index.tpl', $data);
    }

    public function getPrintTools(){
        $button_group = array();
        if($this->access_right->otoritas('print')){
            $button_group[] = view::button_all_barcode(1,1,"Merchandise");
        }
        
        if(true){//export
            //$button_group[] = view::button_export();
        }
        return view::render_button_group_laporan_payout($button_group, array(), true);

    }



    public function table_main($option = array()){
        $this->load->model('hadiah_model');
        $this->load->library('Datatable');
        $table = $this->datatable;

        if (in_array('filter', $option)) {
            $table->dataFilter = array(
                                array('nama_hadiah', 'Merchandise Name', 'text'),
                                array('this.hadiahtype_id', 'Type', 'list', $this->hadiah_type_model->options()),
                                array('this.item_key','Code SKU','text'),
                                array('this.code_exist','Code Exist','text'),
                                
                            );
        }

        if (in_array('wrapper', $option)) {
            $table->numbering  = true;
            $table->isScrollable= false;
            $table->id         = 'table-hadiah';
            $table->header     = array('CODE EXIST','SKU CODE', 'NAME', 'TYPE','DIVISI','HARGA','SATUAN','HARGA/PAX', 'ACTION');
            $table->source     = site_url($this->class_name.'/table_main');
            return $table->generateWrapper();
        } else {
            $table
                ->setModel($this->hadiah_model)
                ->setNumbering()

                ->with(array('type','item','satuan','divisi'))
                ->select('this.code_exist, this.item_key, nama_hadiah , type_name, nama_bagian_md, item.harga,nama_satuan,item.price_pax, id_hadiah')
                ->order_by('this.id_hadiah')
                ->edit_column('id_hadiah', '$1', 'view::btn_group_edit_delete_merchandise(id_hadiah)')
                ->edit_column('price_pax', '$1', 'hgenerator::number(price_pax)')
                ->edit_column('harga', '$1', 'hgenerator::number(harga)');
            echo $table->generate();
        }

    }

    public function add($id='',$status_delete = 0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->config->load('twig');
            $this->load->model('satuan_model');
            $this->load->model('bagian_md_model');
            
            $title = 'Tambah Data';
            $data['form_action'] = $this->class_name . '/proses';

            $data['id'] = $id;
            if($id){
                $title = 'Edit Merchandise';
                $row            = $this->hadiah_model->getById($id)->row();
                $data['data']  = $row;
            }else{
                $data['data']['item_key'] = code::getCode('sku_md');
            }

            $data['title']      = $title;
            $data['assets_url'] = $this->config->item('assets_url');
            $data['sidebar']    = $this->access_right->menu();
            $data['form']       = $this->class_name.'/form_hadiah.tpl';

            $button_group   = array();
            $button_group[] = view::button_back();
            
            if($status_delete == 0){
                $data['form_action']    = view::form_input($id);
                $button_group[]         = view::button_save();
            }else{
                $data['title']          = 'Delete Merchandise';
                $data['form_action']    = view::form_delete($id);;
                $data['readonly']       = 'readonly=""';
                $button_group[]         = view::button_delete_confirm();
            }

            $data['button_group'] = view::render_button_group($button_group);
            $data['options_hadiah'] = form_dropdown('data[hadiahtype_id]', $this->hadiah_type_model->options(), $row->hadiahtype_id, 'class="bs-select"');
            $data['options_satuan'] = form_dropdown('data[id_satuan]', $this->satuan_model->options(), $row->id_satuan, 'class="bs-select"');
            $htmlOptions            =  'class="form-control select2me" data-placeholder="Select Divisi"'.$disabled;
            $data['options_divisi']  = form_dropdown('data[id_bagian_md]', $this->bagian_md_model->options(), $row->id_bagian_md, $htmlOptions);
            
            //$options_pulau = $this->pulau_model->options();
            //$data['options_pulau'] = form_dropdown('id_pulau',$options_pulau, !empty($def_id_pulau) ? $def_id_pulau : '', 'class = "form-control"'); 
            
            $this->twig->display('base/page_form.tpl', $data);

        } else {
            $this->access_right->redirect();
        }
    }

    public function showbarcode($id='',$status_delete = 0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->config->load('twig');
            
            $title = 'Tambah Data';
            $data['form_action'] = $this->class_name . '/proses';

            $data['id'] = $id;
            if($id){
                $title = 'Merchandise Barcode';
                $row            = $this->hadiah_model->getById($id)->row();
                $data['data']  = $row;
            }

            $data['title']      = $title;
            $data['assets_url'] = $this->config->item('assets_url');
            $data['sidebar']    = $this->access_right->menu();
            $data['form']       = $this->class_name.'/form_barcode.tpl';

            $button_group   = array();
            $button_group[] = view::button_back();
            
            if($status_delete == 0){
                $data['form_action']    = view::form_input($id);
                $button_group[]         = view::button_print();
            }else{
                $data['title']          = 'Delete Merchandise';
                $data['form_action']    = view::form_delete($id);;
                $data['readonly']       = 'readonly=""';
                $button_group[]         = view::button_delete_confirm();
            }

            $data['button_group'] = view::render_button_group($button_group);
            $data['options_hadiah'] = form_dropdown('data[hadiahtype_id]', $this->hadiah_type_model->options(), $row->hadiahtype_id, 'class="bs-select" disabled="disabled"');
            
            //$options_pulau = $this->pulau_model->options();
            //$data['options_pulau'] = form_dropdown('id_pulau',$options_pulau, !empty($def_id_pulau) ? $def_id_pulau : '', 'class = "form-control"'); 
            
            $this->twig->display('base/page_form.tpl', $data);

        } else {
            $this->access_right->redirect();
        }
    }

    public function showbarcodeall($id='',$status_delete = 0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->config->load('twig');
            
            $title = 'Print All Merchandise Barcode';
            $data['form_action'] = $this->class_name . '/proses';

            $data['id'] = $id;
            if($id){
                $title = 'Merchandise Barcode';
                $row            = $this->hadiah_model->getById($id)->row();
                $data['data']  = $row;
            }

            $data['title']      = $title;
            $data['assets_url'] = $this->config->item('assets_url');
            $data['sidebar']    = $this->access_right->menu();
            $data['form']       = $this->class_name.'/form_barcode_all.tpl';

            $button_group   = array();
            $button_group[] = view::button_back();
            
            if($status_delete == 0){
                $data['form_action']    = view::form_input($id);
                $button_group[]         = view::button_print_all_merchandise();
            }else{
                $data['title']          = 'Delete Merchandise';
                $data['form_action']    = view::form_delete($id);;
                $data['readonly']       = 'readonly=""';
                $button_group[]         = view::button_delete_confirm();
            }

            $data['button_group'] = view::render_button_group($button_group);
            $data['options_hadiah'] = form_dropdown('data[hadiahtype_id]', $this->hadiah_type_model->options(), $row->hadiahtype_id, 'class="bs-select" disabled="disabled"');
            
            //$options_pulau = $this->pulau_model->options();
            //$data['options_pulau'] = form_dropdown('id_pulau',$options_pulau, !empty($def_id_pulau) ? $def_id_pulau : '', 'class = "form-control"'); 
            
            $this->twig->display('base/page_form.tpl', $data);

        } else {
            $this->access_right->redirect();
        }
    }

    public function __add($id = '', $status_delete=0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->load->library('custom_form');
            $form = new custom_form();

            $param['title'] = 'New Merchandise';
            if($id){
                $param['title'] = 'Edit Merchandise';
                $row            = $this->hadiah_model->getById($id)->row();
                $param['data']  = $row;
            }

            if($status_delete == 0){
                $param['form_action']   = view::form_input($id);
            }else{
                $param['title']          = 'Delete Merchandise';
                $param['form_action']    = view::form_delete($id);
                $form->type = 'delete';
                //$button_group[]         = view::button_delete_confirm();
            }

            $param['list']['type'] = form_dropdown('data[hadiahtype_id]', $this->hadiah_type_model->options(), $row->hadiahtype_id, 'class="bs-select"');

            $form->portlet          = true;
            $form->button_back      = true;
            $form->id               = 'form-hadiah';
            $form->param            = $param;
            $form->url_form         = $this->class_name.'/form_hadiah.tpl';
            echo $form->generate();

        } else {
            $this->access_right->redirect();
        }
    }

    public function edit($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('edit', true);
        $this->add($id);
    }

    public function delete($id_enc = '') {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('delete', true);
        $this->add($id,$status_delete = 1);
    }

    public function barcode($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('edit', true);
        $this->showbarcode($id);
    }

    public function barcodeall($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('edit', true);
        $this->showbarcodeall($id);
    }


    public function proses() {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            //$this->form_validation->set_rules('data[kd_hadiah]', 'Kode hadiah', 'required|trim');
            $this->form_validation->set_rules('data[nama_hadiah]', 'Nama hadiah', 'required|trim');
            $this->form_validation->set_rules('data[item_key]', 'Barcode / Item Key', 'required|trim');
            $this->form_validation->set_rules('data[harga]', 'Harga', 'required|trim');


            if ($this->form_validation->run()) {
                $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');

                $id = $this->input->post('id');
                $data = $this->input->post('data');
                $data['kd_hadiah'] = $data['item_key'];
                $data['item_key'] = $data['item_key'];

                $this->db->trans_start();
                if ($id){
                    $this->hadiah_model->update($data, $id);
                }else{
                    $this->hadiah_model->create($data);
                }
                
                if($this->db->trans_status()){
                    $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh_filter()');
                    if(empty($id)){
                        $riseCode = code::getCodeAndRaise('sku_md');
                    }
                }else{
                    $message = array(false, 'Proses penyimpanan data gagal','Database Error');
                }
                $this->db->trans_complete();
            } else {
                $message = array(false, 'Proses penyimpanan data gagal',validation_errors());
            }
            echo json_encode($message);
        } else {
            $this->access_right->redirect();
        }
    }

    public function proses_delete() {
        $id=$this->input->post("id");
		$this->access_right->otoritas('delete', true);
        if ($this->access_right->otoritas('delete', true)) {
            $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');
            if ($this->hadiah_model->delete($id)) {
                $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', 'refresh_filter()');
				/* INSERT LOG */
				$this->access_right->activity_logs('delete','Delete Merchandise');
				/* END OF INSERT LOG */
            }
            echo json_encode($message);
        }
    }

    public function initPrinting(){
        $this->load->helper('printingdua');

        $ci =& get_instance();
        $angka = $ci->input->post('angka');
        $nama = $ci->input->post('nama');
        $nc = $ci->input->post('nc');
        $kc = $ci->input->post('koli_code');

        $arrname = $this->cutName($nama, 20);

        $cmd .= "^XA
                ^MMA
                ^PW320
                ^LL0240
                ^LS0
                ^BY3,3,71^FT38,104^BCN,,Y,N
                ^FD>;".$angka."^FS
                ^FT39,162^A0N,23,24^FH\^FD".$arrname[0]."^FS";

        if(count($arrname)==2){
            $cmd .= "^FT39,196^A0N,23,24^FH\^FD".$arrname[1]."^FS";
        }

        if(count($arrname)==3){
            $cmd .= "^FT39,196^A0N,23,24^FH\^FD".$arrname[1]."^FS";
            $cmd .="^FT39,229^A0N,23,24^FH\^FD".$arrname[2]."^FS";
        }
                
        $cmd .= "^PQ".$nc.",0,1,Y^XZ";

/*        $cmd .= "^XA";
        $cmd .= "^MMA";
        $cmd .= "^PW320";
        $cmd .= "^LL0240";
        $cmd .= "^LS0";
        $cmd .= "^BY3,3,106^FT36,134^BCN,,Y,N";
        $cmd .= "^FD>;".$angka."^FS";
        for($i=0;$i<count($arrname);$i++){
            $cmd .= "^FT36,185^A0N,20,19^FH\^FD".$arrname[$i]."^FS";
        }
        $cmd .= "^PQ".$nc.",0,1,Y^XZ";  
*/
        $form = '<form id="myForm" action="">

    <input type="hidden" id="sid" name="sid" value="'.session_id().'" />
        <fieldset hidden>
            <legend>Client Printer Settings</legend>
            
            <div hidden>
                I want to:&nbsp;&nbsp;
                <select id="pid" name="pid">
                  <option value="0">Use Default Printer</option>
                  <option value="1" selected="selected">Display a Printer dialog</option>
                  <option value="2">Use an installed Printer</option>
                  <option value="3">Use an IP/Etherner Printer</option>
                  <option value="4">Use a LPT port</option>
                  <option value="5">Use a RS232 (COM) port</option>
                </select>
                <br />
                <br />
                <div id="info" class="alert alert-info" style="font-size:11px;"></div>                
                <br />
            </div>
            
            <div id="installedPrinter" hidden>
                <div id="loadPrinters" name="loadPrinters">
                WebClientPrint can detect the installed printers in your machine. <a onclick="javascript:jsWebClientPrint.getPrinters();" class="btn btn-success">Load installed printers...</a>
                </div>
                <label for="installedPrinterName">Select an installed Printer:</label>
                <select name="installedPrinterName" id="installedPrinterName"></select>

            

            </div>           
                        
        </fieldset>
        <fieldset hidden>
            <legend>Printer Commands</legend>
            
            <p>
                Enter the printers commands you want to send and is supported by the specified printer (ESC/P, PCL, ZPL, EPL, DPL, IPL, EZPL, etc). 
                <br /><br />
                <b>NOTE:</b> You can use the <b>hex notation for non-printable characters</b> e.g. for Carriage Return (ASCII Hex 0D) you can specify 0x0D
                
            </p>
            <br /><br />
            <div class="alert alert-info" style="font-size:11px;">
            <b>Upload Files</b><br />
            This online demo does not allow you to upload files. So, if you have a file containing the printer commands like a PRN file, Postscript, PCL, ZPL, etc, then we recommend you to <a href="http://www.neodynamic.com/products/printing/raw-data/php/download/" target="_blank">download WebClientPrint</a> and test it by using the sample source code included in the package. Feel free to <a href="http://www.neodynamic.com/support" target="_blank">contact our Tech Support</a> for further assistance, help, doubts or questions.             
            </div>            
        </fieldset>        
        <fieldset hidden>
            <legend hidden>Ready to print!</legend>
            <h3>Your settings were saved! Now its time to <a href="#" onclick="javascript:doClientPrint();" class="btn btn-large btn-success">Print</a></h3>           
        </fieldset>

        <textarea id="printerCommands" name="printerCommands" rows="10" cols="80" class="span9" hidden>'.$cmd.'</textarea>
    </form>';
    echo $form;

    }

    function cutName($name='', $maxlength=0){
        //$name = 'CHOCO MANIA BISKUIT 21GR72PC';
        $arrname=explode(' ',$name);
        
        $arrresult = array();
        $i=0;
        $j=0;
        $temp = '';
        while($i<count($arrname)){
            $temp .= $arrname[$i];            
            if(strlen($arrresult[$j])<$maxlength && strlen($temp)<$maxlength){
                if(strlen($arrresult[$j])==0){
                    $arrresult[$j] .= $arrname[$i];                    
                }else{
                    $arrresult[$j] .= ' '.$arrname[$i];
                }
            }else{
                $temp = '';
                $j++;
                $i--;
            }
            $i++;
        }
        return $arrresult;
    }


    public function printAllMerBarcode(){    
        $this->load->helper('printingdua');

        $ci =& get_instance();
        $this->load->model(array('hadiah_model'));

        $dataall = $this->hadiah_model
        ->select('kd_hadiah, nama_hadiah')
        ->get()
        ->result_array();
        $cmd ="";
        foreach ($dataall as $key => $value) {
            $arrname = $this->cutName($dataall[$key]["nama_hadiah"], 20);
            $cmd .= "^XA
                    ^MMA
                    ^PW320
                    ^LL0240
                    ^LS0
                    ^BY3,3,71^FT38,104^BCN,,Y,N
                    ^FD>;".$dataall[$key]["kd_hadiah"]."^FS
                    ^FT39,162^A0N,23,24^FH\^FD".$arrname[0]."^FS";

            if(count($arrname)==2){
                $cmd .= "^FT39,196^A0N,23,24^FH\^FD".$arrname[1]."^FS";
            }

            if(count($arrname)==3){
                $cmd .= "^FT39,196^A0N,23,24^FH\^FD".$arrname[1]."^FS";
                $cmd .="^FT39,229^A0N,23,24^FH\^FD".$arrname[2]."^FS";
            }
                    
            $cmd .= "^PQ1,0,1,Y^XZ";
            
        }

/*        $cmd .= "^XA";
        $cmd .= "^MMA";
        $cmd .= "^PW320";
        $cmd .= "^LL0240";
        $cmd .= "^LS0";
        $cmd .= "^BY3,3,106^FT36,134^BCN,,Y,N";
        $cmd .= "^FD>;".$angka."^FS";
        for($i=0;$i<count($arrname);$i++){
            $cmd .= "^FT36,185^A0N,20,19^FH\^FD".$arrname[$i]."^FS";
        }
        $cmd .= "^PQ".$nc.",0,1,Y^XZ";  
*/
        $form = '<form id="myForm" action="">

    <input type="hidden" id="sid" name="sid" value="'.session_id().'" />
        <fieldset hidden>
            <legend>Client Printer Settings</legend>
            
            <div hidden>
                I want to:&nbsp;&nbsp;
                <select id="pid" name="pid">
                  <option value="0">Use Default Printer</option>
                  <option value="1" selected="selected">Display a Printer dialog</option>
                  <option value="2">Use an installed Printer</option>
                  <option value="3">Use an IP/Etherner Printer</option>
                  <option value="4">Use a LPT port</option>
                  <option value="5">Use a RS232 (COM) port</option>
                </select>
                <br />
                <br />
                <div id="info" class="alert alert-info" style="font-size:11px;"></div>                
                <br />
            </div>
            
            <div id="installedPrinter" hidden>
                <div id="loadPrinters" name="loadPrinters">
                WebClientPrint can detect the installed printers in your machine. <a onclick="javascript:jsWebClientPrint.getPrinters();" class="btn btn-success">Load installed printers...</a>
                </div>
                <label for="installedPrinterName">Select an installed Printer:</label>
                <select name="installedPrinterName" id="installedPrinterName"></select>

            

            </div>           
                        
        </fieldset>
        <fieldset hidden>
            <legend>Printer Commands</legend>
            
            <p>
                Enter the printers commands you want to send and is supported by the specified printer (ESC/P, PCL, ZPL, EPL, DPL, IPL, EZPL, etc). 
                <br /><br />
                <b>NOTE:</b> You can use the <b>hex notation for non-printable characters</b> e.g. for Carriage Return (ASCII Hex 0D) you can specify 0x0D
                
            </p>
            <br /><br />
            <div class="alert alert-info" style="font-size:11px;">
            <b>Upload Files</b><br />
            This online demo does not allow you to upload files. So, if you have a file containing the printer commands like a PRN file, Postscript, PCL, ZPL, etc, then we recommend you to <a href="http://www.neodynamic.com/products/printing/raw-data/php/download/" target="_blank">download WebClientPrint</a> and test it by using the sample source code included in the package. Feel free to <a href="http://www.neodynamic.com/support" target="_blank">contact our Tech Support</a> for further assistance, help, doubts or questions.             
            </div>            
        </fieldset>        
        <fieldset hidden>
            <legend hidden>Ready to print!</legend>
            <h3>Your settings were saved! Now its time to <a href="#" onclick="javascript:doClientPrint();" class="btn btn-large btn-success">Print</a></h3>           
        </fieldset>

        <textarea id="printerCommands" name="printerCommands" rows="10" cols="80" class="span9" hidden>'.$cmd.'</textarea>
    </form>';
    echo $form;

    }


}

/* End of file bbm.php */
/* Location: ./application/controllers/bbm.php */

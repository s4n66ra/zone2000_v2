<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Git extends MY_Controller {

	public function index()
	{
		
	}

	public function shell($exe){
		$output = shell_exec($exe);
        echo "<pre>$output</pre>";
	}

	public function command($cm){	
		$output = shell_exec('git '.$cm);
        echo "<pre>$output</pre>";
	}

	public function status(){
		$output = shell_exec('git status');
        echo "<pre>$output</pre>";
	}

	public function pull(){
		$output = shell_exec('git pull');
        echo "<pre>$output</pre>";
	}

}

/* End of file git.php */
/* Location: ./application/controllers/git.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class dashboard extends MY_Controller {
    function __construct(){
		parent::__construct();
		$this->load->model(array("store_model","area_model","provinsi_model","user_model"));
	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		if($this->session->userdata('kd_roles') == hprotection::getSupllierGroupId()){
			redirect('/web_supplier');
		}
        $data['sidebar'] = $this->access_right->menu();
        $htmlOptions            =  'id="list_store_id" class="form-control select2" data-placeholder="Select Store"';
        $data['list_store']     = form_dropdown('data[store_id]', $this->store_model->options_empty(), $row->store_id,$htmlOptions);

        $data['dashboard_filter'] = $this->class_name.'/page_filter.tpl';

        //MENAMPILKAN NOTIFIKASI
        //SEMENTARA TIDAK PAKE INI NOTIFNYA
		if($this->access_right->real_otoritas('view')){
			$data['content'] = $this->class_name.'/page_main.tpl';
			$this->twig->display('dashboard.tpl', $data);
		}else{
			$data['content'] = $this->class_name.'/page_main_empty.tpl';
			$this->twig->display('index.tpl',$data);
		}

	    $this->store_model->update_tiket();
		
		
	}


	public function getMostItemRedemp($kategory='', $det_kategory='', $start='', $end=''){
		$this->load->model(array('transaction_model'));
		$this->load->model(array('transaction_redemption_model'));
		//echo $start;
		$trans ='';
		switch ($kategory) {
			case 'store':
				if($det_kategory=='' || $start=='' || $end==''){
			        $trans = $this->transaction_redemption_model
			            ->with(array( 'item_redeem'))
			           // ->where(array('transactiontype_id' => transactiontype::MRDM))
			            ->select('item_name, sum(item_qty) as jumlah')
			            ->group_by('m_item.item_key')
			            ->order_by('jumlah', desc)
			            ->limit(10)
			            ->get()
			            ->result_array();
			        //$trans = $this->transaction_model->getMostItemRedemp_All();
			        //$trans = $this->transaction_model->top_md();

				}else{
					$trans = $this->zn_transaction_redemption
			            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem'))
			            ->where(array('transactiontype_id' => transactiontype::MRDM, 'm_cabang.id_cabang'=>$det_kategory))
			            ->where('DATE(zn_transaction.rec_created) >=', $start)
			            ->where('DATE(zn_transaction.rec_created) <=', $end)
			            ->select('item_name, sum(item_qty) as jumlah')
			            ->group_by('m_item.item_key')
			            ->order_by('jumlah', desc)
			            ->limit(10)
			            ->get()
			            ->result_array();
				}
				break;

			case 'area':
				if($det_kategory=='' || $start=='' || $end==''){
			        $trans = $this->transaction_model
			            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','area'))
			            ->where(array('transactiontype_id' => transactiontype::MRDM))
			            ->select('item_name, sum(item_qty) as jumlah')
			            ->group_by('m_item.item_key')
			            ->order_by('jumlah', desc)
			            ->limit(10)
			            ->get()
			            ->result_array();

				}else{
					$trans = $this->transaction_model
			            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','area'))
			            ->where(array('transactiontype_id' => transactiontype::MRDM, 'm_area.id_area'=>$det_kategory))
			            ->where('DATE(zn_transaction.rec_created) >=', $start)
			            ->where('DATE(zn_transaction.rec_created) <=', $end)
			            ->select('item_name, sum(item_qty) as jumlah')
			            ->group_by('m_item.item_key')
			            ->order_by('jumlah', desc)
			            ->limit(10)
			            ->get()
			            ->result_array();
				}
				break;

			case 'province':
				if($det_kategory=='' || $start=='' || $end==''){
			        $trans = $this->transaction_model
			            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','kota','province'))
			            ->where(array('transactiontype_id' => transactiontype::MRDM))
			            ->select('item_name, sum(item_qty) as jumlah')
			            ->group_by('m_item.item_key')
			            ->order_by('jumlah', desc)
			            ->limit(10)
			            ->get()
			            ->result_array();

				}else{
					$trans = $this->transaction_model
			            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','area','kota','province'))
			            ->where(array('transactiontype_id' => transactiontype::MRDM, 'm_provinsi.id_provinsi'=>$det_kategory))
			            ->where('DATE(zn_transaction.rec_created) >=', $start)
			            ->where('DATE(zn_transaction.rec_created) <=', $end)
			            ->select('item_name, sum(item_qty) as jumlah')
			            ->group_by('m_item.item_key')
			            ->order_by('jumlah', desc)
			            ->limit(10)
			            ->get()
			            ->result_array();
				}

				break;

			default:
				# code...
				break;
		}

        echo json_encode($trans);
	}

	public function getTopToko($kategory='', $det_kategory='', $start='', $end=''){
		$this->load->model(array('lap_store_model'));
		//echo $start;
		$trans ='';
		switch ($kategory) {
			case 'store':
				if($det_kategory=='' || $start=='' || $end==''){
			        $trans = $this->lap_store_model
			            ->with(array('cabang','bulan'))
			           // ->where(array('transactiontype_id' => transactiontype::MRDM))
			            ->select('CONCAT(nama_cabang,"(",nama_bulan,"-",tahun,")") as nama_cabang, total_omzet as jumlah',FALSE)
			            //->group_by('m_item.item_key')
			            ->order_by('jumlah', desc)
			            ->limit(10)
			            ->get()
			            ->result_array();
			        //$trans = $this->transaction_model->top_md();

				}else{
					$trans = $this->transaction_model
			            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem'))
			            ->where(array('transactiontype_id' => transactiontype::MRDM, 'm_cabang.id_cabang'=>$det_kategory))
			            ->where('DATE(zn_transaction.rec_created) >=', $start)
			            ->where('DATE(zn_transaction.rec_created) <=', $end)
			            ->select('item_name, sum(item_qty) as jumlah')
			            ->group_by('m_item.item_key')
			            ->order_by('jumlah', desc)
			            ->limit(10)
			            ->get()
			            ->result_array();
				}
				break;

			/*case 'area':
				if($det_kategory=='' || $start=='' || $end==''){
			        $trans = $this->transaction_model
			            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','area'))
			            ->where(array('transactiontype_id' => transactiontype::MRDM))
			            ->select('item_name, sum(item_qty) as jumlah')
			            ->group_by('m_item.item_key')
			            ->order_by('jumlah', desc)
			            ->limit(10)
			            ->get()
			            ->result_array();

				}else{
					$trans = $this->transaction_model
			            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','area'))
			            ->where(array('transactiontype_id' => transactiontype::MRDM, 'm_area.id_area'=>$det_kategory))
			            ->where('DATE(zn_transaction.rec_created) >=', $start)
			            ->where('DATE(zn_transaction.rec_created) <=', $end)
			            ->select('item_name, sum(item_qty) as jumlah')
			            ->group_by('m_item.item_key')
			            ->order_by('jumlah', desc)
			            ->limit(10)
			            ->get()
			            ->result_array();
				}
				break;

			case 'province':
				if($det_kategory=='' || $start=='' || $end==''){
			        $trans = $this->transaction_model
			            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','kota','province'))
			            ->where(array('transactiontype_id' => transactiontype::MRDM))
			            ->select('item_name, sum(item_qty) as jumlah')
			            ->group_by('m_item.item_key')
			            ->order_by('jumlah', desc)
			            ->limit(10)
			            ->get()
			            ->result_array();

				}else{
					$trans = $this->transaction_model
			            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','area','kota','province'))
			            ->where(array('transactiontype_id' => transactiontype::MRDM, 'm_provinsi.id_provinsi'=>$det_kategory))
			            ->where('DATE(zn_transaction.rec_created) >=', $start)
			            ->where('DATE(zn_transaction.rec_created) <=', $end)
			            ->select('item_name, sum(item_qty) as jumlah')
			            ->group_by('m_item.item_key')
			            ->order_by('jumlah', desc)
			            ->limit(10)
			            ->get()
			            ->result_array();
				}

				break;*/

			default:
				# code...
				break;
		}

        echo json_encode($trans);
	}

	public function getWorseToko($kategory='', $det_kategory='', $start='', $end=''){
		$this->load->model(array('lap_store_model'));
		//echo $start;
		$trans ='';
		switch ($kategory) {
			case 'store':
				if($det_kategory=='' || $start=='' || $end==''){
			        $trans = $this->lap_store_model
			            ->with(array('cabang','bulan'))
			           // ->where(array('transactiontype_id' => transactiontype::MRDM))
			            ->select('CONCAT(nama_cabang,"(",nama_bulan,"-",tahun,")") as nama_cabang, total_omzet as jumlah',FALSE)
			            //->group_by('m_item.item_key')
			            ->order_by('jumlah', ASC)
			            ->limit(10)
			            ->get()
			            ->result_array();
			        //$trans = $this->transaction_model->top_md();

				}else{
					$trans = $this->transaction_model
			            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem'))
			            ->where(array('transactiontype_id' => transactiontype::MRDM, 'm_cabang.id_cabang'=>$det_kategory))
			            ->where('DATE(zn_transaction.rec_created) >=', $start)
			            ->where('DATE(zn_transaction.rec_created) <=', $end)
			            ->select('item_name, sum(item_qty) as jumlah')
			            ->group_by('m_item.item_key')
			            ->order_by('jumlah', ASC)
			            ->limit(10)
			            ->get()
			            ->result_array();
				}
				break;

			/*case 'area':
				if($det_kategory=='' || $start=='' || $end==''){
			        $trans = $this->transaction_model
			            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','area'))
			            ->where(array('transactiontype_id' => transactiontype::MRDM))
			            ->select('item_name, sum(item_qty) as jumlah')
			            ->group_by('m_item.item_key')
			            ->order_by('jumlah', desc)
			            ->limit(10)
			            ->get()
			            ->result_array();

				}else{
					$trans = $this->transaction_model
			            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','area'))
			            ->where(array('transactiontype_id' => transactiontype::MRDM, 'm_area.id_area'=>$det_kategory))
			            ->where('DATE(zn_transaction.rec_created) >=', $start)
			            ->where('DATE(zn_transaction.rec_created) <=', $end)
			            ->select('item_name, sum(item_qty) as jumlah')
			            ->group_by('m_item.item_key')
			            ->order_by('jumlah', desc)
			            ->limit(10)
			            ->get()
			            ->result_array();
				}
				break;

			case 'province':
				if($det_kategory=='' || $start=='' || $end==''){
			        $trans = $this->transaction_model
			            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','kota','province'))
			            ->where(array('transactiontype_id' => transactiontype::MRDM))
			            ->select('item_name, sum(item_qty) as jumlah')
			            ->group_by('m_item.item_key')
			            ->order_by('jumlah', desc)
			            ->limit(10)
			            ->get()
			            ->result_array();

				}else{
					$trans = $this->transaction_model
			            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','area','kota','province'))
			            ->where(array('transactiontype_id' => transactiontype::MRDM, 'm_provinsi.id_provinsi'=>$det_kategory))
			            ->where('DATE(zn_transaction.rec_created) >=', $start)
			            ->where('DATE(zn_transaction.rec_created) <=', $end)
			            ->select('item_name, sum(item_qty) as jumlah')
			            ->group_by('m_item.item_key')
			            ->order_by('jumlah', desc)
			            ->limit(10)
			            ->get()
			            ->result_array();
				}

				break;*/

			default:
				# code...
				break;
		}

        echo json_encode($trans);
	}

	public function getMostMember(){
		$this->load->model(array('transaction_model'));
        $trans = $this->transaction_model
            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem'))
            ->where(array('transactiontype_id' => transactiontype::MRDM))
            ->select('item_name, sum(item_qty) as jumlah')
            ->group_by('m_item.item_key')
            ->order_by('jumlah', desc)
            ->limit(10)
            ->get()
            ->result_array();
        echo json_encode($trans);
	}



	public function getItemRedempDailyJmlBarang($kategory='', $det_kategory='', $start='', $end='', $mode=''){
		$this->load->model(array('transaction_model'));
		$trans='';
		switch ($kategory) {
			case 'store':
				if($det_kategory=='' || $start=='' || $end=='' || $mode==''){
			        $trans = $this->transaction_model
			            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem'))
			            ->where(array('transactiontype_id' => transactiontype::MRDM))
			            ->select('DATE(zn_transaction.rec_created) as hari, sum(item_qty) as jumlah')
			            ->group_by('DATE(zn_transaction.rec_created)')
			            ->order_by('hari', asc)
			            ->get()
			            ->result_array();
				}else{
					switch ($mode) {
						case 'daily':
						    $trans = $this->transaction_model
					            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem'))
					            ->where(array('transactiontype_id' => transactiontype::MRDM, 'm_cabang.id_cabang'=>$det_kategory))
					            ->where('DATE(zn_transaction.rec_created) >=', $start)
					            ->where('DATE(zn_transaction.rec_created) <=', $end)
					            ->select("DATE(zn_transaction.rec_created) as hari, sum(item_qty) as jumlah")
					            ->group_by('DATE(zn_transaction.rec_created)')
					            ->order_by('hari', asc)
					            ->get()
					            ->result_array();
							break;
						case 'monthly':
						    $trans = $this->transaction_model
					            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem'))
					            ->where(array('transactiontype_id' => transactiontype::MRDM, 'm_cabang.id_cabang'=>$det_kategory))
					            ->where('DATE(zn_transaction.rec_created) >=', $start)
					            ->where('DATE(zn_transaction.rec_created) <=', $end)
					            ->select("concat(YEAR(zn_transaction.rec_created),-MONTH(zn_transaction.rec_created)) as hari, sum(item_qty) as jumlah")
					            ->group_by("concat(YEAR(zn_transaction.rec_created),-MONTH(zn_transaction.rec_created))")
					            ->order_by('hari', asc)
					            ->get()
					            ->result_array();
					        break;
						case 'yearly':
						    $trans = $this->transaction_model
					            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem'))
					            ->where(array('transactiontype_id' => transactiontype::MRDM, 'm_cabang.id_cabang'=>$det_kategory))
					            ->where('DATE(zn_transaction.rec_created) >=', $start)
					            ->where('DATE(zn_transaction.rec_created) <=', $end)
					            ->select("concat(YEAR(zn_transaction.rec_created)) as hari, sum(item_qty) as jumlah")
					            ->group_by("concat(YEAR(zn_transaction.rec_created))")
					            ->order_by('hari', asc)
					            ->get()
					            ->result_array();

						default:
							break;
					}

				}

				break;
			case 'area':
				if($det_kategory=='' || $start=='' || $end=='' || $mode==''){
			        $trans = $this->transaction_model
			            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','area'))
			            ->where(array('transactiontype_id' => transactiontype::MRDM))
			            ->select('DATE(zn_transaction.rec_created) as hari, sum(item_qty) as jumlah')
			            ->group_by('DATE(zn_transaction.rec_created)')
			            ->order_by('hari', asc)
			            ->get()
			            ->result_array();
				}else{
					switch ($mode) {
						case 'daily':
						    $trans = $this->transaction_model
					            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','area'))
					            ->where(array('transactiontype_id' => transactiontype::MRDM))
					            ->where('DATE(zn_transaction.rec_created) >=', $start)
					            ->where('DATE(zn_transaction.rec_created) <=', $end)
					            ->where('m_cabang.id_area', $det_kategory)
					            ->select("DATE(zn_transaction.rec_created) as hari, sum(item_qty) as jumlah")
					            ->group_by('DATE(zn_transaction.rec_created)')
					            ->order_by('hari', asc)
					            ->get()
					            ->result_array();
							break;
						case 'monthly':
						    $trans = $this->transaction_model
					            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','area'))
					            ->where(array('transactiontype_id' => transactiontype::MRDM))
					            ->where('DATE(zn_transaction.rec_created) >=', $start)
					            ->where('DATE(zn_transaction.rec_created) <=', $end)
					            ->where('m_cabang.id_area', $det_kategory)
					            ->select("concat(YEAR(zn_transaction.rec_created),-MONTH(zn_transaction.rec_created)) as hari, sum(item_qty) as jumlah")
					            ->group_by("concat(YEAR(zn_transaction.rec_created),-MONTH(zn_transaction.rec_created))")
					            ->order_by('hari', asc)
					            ->get()
					            ->result_array();
					        break;
						case 'yearly':
						    $trans = $this->transaction_model
					            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','area'))
					            ->where(array('transactiontype_id' => transactiontype::MRDM))
					            ->where('DATE(zn_transaction.rec_created) >=', $start)
					            ->where('DATE(zn_transaction.rec_created) <=', $end)
					            ->where('m_cabang.id_area', $det_kategory)
					            ->select("concat(YEAR(zn_transaction.rec_created)) as hari, sum(item_qty) as jumlah")
					            ->group_by("concat(YEAR(zn_transaction.rec_created))")
					            ->order_by('hari', asc)
					            ->get()
					            ->result_array();
					        break;
						default:
							break;
					}

				}

				break;
			case 'province':
				if($det_kategory=='' || $start=='' || $end=='' || $mode==''){
			        $trans = $this->transaction_model
			            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','kota', 'province'))
			            ->where(array('transactiontype_id' => transactiontype::MRDM))
			            ->select('DATE(zn_transaction.rec_created) as hari, sum(item_qty) as jumlah')
			            ->group_by('DATE(zn_transaction.rec_created)')
			            ->order_by('hari', asc)
			            ->get()
			            ->result_array();
				}else{
					switch ($mode) {
						case 'daily':
						    $trans = $this->transaction_model
					            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','kota', 'province'))
					            ->where(array('transactiontype_id' => transactiontype::MRDM))
					            ->where('DATE(zn_transaction.rec_created) >=', $start)
					            ->where('DATE(zn_transaction.rec_created) <=', $end)
					            ->where('m_provinsi.id_provinsi', $det_kategory)
					            ->select("DATE(zn_transaction.rec_created) as hari, sum(item_qty) as jumlah")
					            ->group_by('DATE(zn_transaction.rec_created)')
					            ->order_by('hari', asc)
					            ->get()
					            ->result_array();
							break;
						case 'monthly':
						    $trans = $this->transaction_model
					            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','kota', 'province'))
					            ->where(array('transactiontype_id' => transactiontype::MRDM))
					            ->where('DATE(zn_transaction.rec_created) >=', $start)
					            ->where('DATE(zn_transaction.rec_created) <=', $end)
					            ->where('m_provinsi.id_provinsi', $det_kategory)
					            ->select("concat(YEAR(zn_transaction.rec_created),-MONTH(zn_transaction.rec_created)) as hari, sum(item_qty) as jumlah")
					            ->group_by("concat(YEAR(zn_transaction.rec_created),-MONTH(zn_transaction.rec_created))")
					            ->order_by('hari', asc)
					            ->get()
					            ->result_array();
					        break;
						case 'yearly':
						    $trans = $this->transaction_model
					            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','kota', 'province'))
					            ->where(array('transactiontype_id' => transactiontype::MRDM))
					            ->where('DATE(zn_transaction.rec_created) >=', $start)
					            ->where('DATE(zn_transaction.rec_created) <=', $end)
					            ->where('m_provinsi.id_provinsi', $det_kategory)
					            ->select("concat(YEAR(zn_transaction.rec_created)) as hari, sum(item_qty) as jumlah")
					            ->group_by("concat(YEAR(zn_transaction.rec_created))")
					            ->order_by('hari', asc)
					            ->get()
					            ->result_array();
					        break;
						default:
							break;
					}

				}
				break;			
			default:
				# code...
				break;
		}

        echo json_encode($trans);

	}

	public function getItemRedempDailyValue($kategory='', $det_kategory='', $start='', $end='', $mode=''){
		$this->load->model(array('transaction_model'));
		switch ($kategory) {
			case 'store':
				if($det_kategory=='' || $start=='' || $end=='' || $mode==''){
			        $trans = $this->transaction_model
			            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','hadiah'))
			            ->where(array('transactiontype_id' => transactiontype::MRDM))
			            ->select('DATE(zn_transaction.rec_created) as hari, sum(item_qty*m_hadiah.harga) as jumlah')
			            ->group_by('DATE(zn_transaction.rec_created)')
			            ->order_by('hari', asc)
			            ->get()
			            ->result_array();			
				}else{
					switch ($mode) {
						case 'daily':
					        $trans = $this->transaction_model
					            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','hadiah'))
					            ->where(array('transactiontype_id' => transactiontype::MRDM, 'm_cabang.id_cabang'=>$det_kategory))
					            ->where('DATE(zn_transaction.rec_created) >=', $start)
					            ->where('DATE(zn_transaction.rec_created) <=', $end)
					            ->select('DATE(zn_transaction.rec_created) as hari, sum(item_qty*m_hadiah.harga) as jumlah')
					            ->group_by('DATE(zn_transaction.rec_created)')
					            ->order_by('hari', asc)
					            ->get()
					            ->result_array();			
							break;
						case 'monthly':
					        $trans = $this->transaction_model
					            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','hadiah'))
					            ->where(array('transactiontype_id' => transactiontype::MRDM, 'm_cabang.id_cabang'=>$det_kategory))
					            ->where('DATE(zn_transaction.rec_created) >=', $start)
					            ->where('DATE(zn_transaction.rec_created) <=', $end)
					            ->select("concat(YEAR(zn_transaction.rec_created),-MONTH(zn_transaction.rec_created)) as hari, sum(item_qty*m_hadiah.harga) as jumlah")
					            ->group_by("concat(YEAR(zn_transaction.rec_created),-MONTH(zn_transaction.rec_created))")
					            ->order_by('hari', asc)
					            ->get()
					            ->result_array();
							break;
						case 'yearly':
						    $trans = $this->transaction_model
					            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','hadiah'))
					            ->where(array('transactiontype_id' => transactiontype::MRDM, 'm_cabang.id_cabang'=>$det_kategory))
					            ->where('DATE(zn_transaction.rec_created) >=', $start)
					            ->where('DATE(zn_transaction.rec_created) <=', $end)
					            ->select("concat(YEAR(zn_transaction.rec_created)) as hari, sum(item_qty*m_hadiah.harga) as jumlah")
					            ->group_by("concat(YEAR(zn_transaction.rec_created))")
					            ->order_by('hari', asc)
					            ->get()
					            ->result_array();
							break;
						default:
							# code...
							break;
					}
				}

				break;
			case 'area':
				if($det_kategory=='' || $start=='' || $end=='' || $mode==''){
			        $trans = $this->transaction_model
			            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','area','hadiah'))
			            ->where(array('transactiontype_id' => transactiontype::MRDM))
			            ->select('DATE(zn_transaction.rec_created) as hari, sum(item_qty*m_hadiah.harga) as jumlah')
			            ->group_by('DATE(zn_transaction.rec_created)')
			            ->order_by('hari', asc)
			            ->get()
			            ->result_array();			
				}else{
					switch ($mode) {
						case 'daily':
					        $trans = $this->transaction_model
					            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','area','hadiah'))
					            ->where(array('transactiontype_id' => transactiontype::MRDM))
					            ->where('DATE(zn_transaction.rec_created) >=', $start)
					            ->where('DATE(zn_transaction.rec_created) <=', $end)
					            ->where('m_cabang.id_area', $det_kategory)
					            ->select('DATE(zn_transaction.rec_created) as hari, sum(item_qty*m_hadiah.harga) as jumlah')
					            ->group_by('DATE(zn_transaction.rec_created)')
					            ->order_by('hari', asc)
					            ->get()
					            ->result_array();			
							break;
						case 'monthly':
					        $trans = $this->transaction_model
					            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','hadiah'))
					            ->where(array('transactiontype_id' => transactiontype::MRDM))
					            ->where('m_cabang.id_area', $det_kategory)
					            ->where('DATE(zn_transaction.rec_created) >=', $start)
					            ->where('DATE(zn_transaction.rec_created) <=', $end)
					            ->select("concat(YEAR(zn_transaction.rec_created),-MONTH(zn_transaction.rec_created)) as hari, sum(item_qty*m_hadiah.harga) as jumlah")
					            ->group_by("concat(YEAR(zn_transaction.rec_created),-MONTH(zn_transaction.rec_created))")
					            ->order_by('hari', asc)
					            ->get()
					            ->result_array();
							break;
						case 'yearly':
						    $trans = $this->transaction_model
					            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','hadiah'))
					            ->where(array('transactiontype_id' => transactiontype::MRDM))
					            ->where('DATE(zn_transaction.rec_created) >=', $start)
					            ->where('DATE(zn_transaction.rec_created) <=', $end)
					            ->where('m_cabang.id_area', $det_kategory)
					            ->select("concat(YEAR(zn_transaction.rec_created)) as hari, sum(item_qty*m_hadiah.harga) as jumlah")
					            ->group_by("concat(YEAR(zn_transaction.rec_created))")
					            ->order_by('hari', asc)
					            ->get()
					            ->result_array();
							break;
						default:
							# code...
							break;
					}
				}

				break;			
			case 'province':
				if($det_kategory=='' || $start=='' || $end=='' || $mode==''){
			        $trans = $this->transaction_model
			            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','kota','province','hadiah'))
			            ->where(array('transactiontype_id' => transactiontype::MRDM))
			            ->select('DATE(zn_transaction.rec_created) as hari, sum(item_qty*m_hadiah.harga) as jumlah')
			            ->group_by('DATE(zn_transaction.rec_created)')
			            ->order_by('hari', asc)
			            ->get()
			            ->result_array();			
				}else{
					switch ($mode) {
						case 'daily':
					        $trans = $this->transaction_model
					            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','kota','province','hadiah'))
					            ->where(array('transactiontype_id' => transactiontype::MRDM))
					            ->where('DATE(zn_transaction.rec_created) >=', $start)
					            ->where('DATE(zn_transaction.rec_created) <=', $end)
					            ->where('m_provinsi.id_provinsi', $det_kategory)
					            ->select('DATE(zn_transaction.rec_created) as hari, sum(item_qty*m_hadiah.harga) as jumlah')
					            ->group_by('DATE(zn_transaction.rec_created)')
					            ->order_by('hari', asc)
					            ->get()
					            ->result_array();			
							break;
						case 'monthly':
					        $trans = $this->transaction_model
					            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','kota','province','hadiah'))
					            ->where(array('transactiontype_id' => transactiontype::MRDM))
					            ->where('m_provinsi.id_provinsi', $det_kategory)
					            ->where('DATE(zn_transaction.rec_created) >=', $start)
					            ->where('DATE(zn_transaction.rec_created) <=', $end)
					            ->select("concat(YEAR(zn_transaction.rec_created),-MONTH(zn_transaction.rec_created)) as hari, sum(item_qty*m_hadiah.harga) as jumlah")
					            ->group_by("concat(YEAR(zn_transaction.rec_created),-MONTH(zn_transaction.rec_created))")
					            ->order_by('hari', asc)
					            ->get()
					            ->result_array();
							break;
						case 'yearly':
						    $trans = $this->transaction_model
					            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','kota','province','hadiah'))
					            ->where(array('transactiontype_id' => transactiontype::MRDM))
					            ->where('DATE(zn_transaction.rec_created) >=', $start)
					            ->where('DATE(zn_transaction.rec_created) <=', $end)
					            ->where('m_provinsi.id_provinsi', $det_kategory)
					            ->select("concat(YEAR(zn_transaction.rec_created)) as hari, sum(item_qty*m_hadiah.harga) as jumlah")
					            ->group_by("concat(YEAR(zn_transaction.rec_created))")
					            ->order_by('hari', asc)
					            ->get()
					            ->result_array();
							break;
						default:
							# code...
							break;
					}
				}

				break;

			default:
				# code...
				break;
		}

        echo json_encode($trans);

	}

	public function getStatusService($kategory='', $det_kategory='', $start='', $end='', $mode=''){
		$this->load->model(array('service_detail_model', 'status_service_new_model'));
		switch ($kategory) {
			case 'store':
				if($det_kategory=='' || $start=='' || $end=='' || $mode==''){
					$statusidname = $this->status_service_new_model
									->select('status_id, nama')
									->get()
									->result_array();

			        $data = $this->service_detail_model
			            ->with(array('service','status_service_new'))
			            ->select('DATE(this.date_created) as hari, this.date_created as jumlah, status_id, nama')
			            ->order_by('hari', asc)
			            ->get()
			            ->result_array();
			        $trans = array("mode" => "daily", "statusidname" => $statusidname, "data" => $data);
				}else{
					switch ($mode) {
						case 'daily':
								$statusidname = $this->status_service_new_model
												->select('status_id, nama')
												->get()
												->result_array();

						        $data = $this->service_detail_model
						            ->with(array('service','status_service_new'))
						            ->select('DATE(this.date_created) as hari, this.date_created as jumlah, status_id, nama')
						            ->order_by('hari', asc)
						            ->get()
						            ->result_array();
						        $trans = array("mode" => "daily","statusidname" => $statusidname, "data" => $data);
							break;
						case 'monthly':
							$statusidname = $this->status_service_new_model
											->select('status_id, nama')
											->get()
											->result_array();

					        $data = $this->service_detail_model
					            ->with(array('service','status_service_new'))
					            ->select('concat(YEAR(this.date_created),-MONTH(this.date_created)) as hari, status_id, nama')
					            ->order_by('hari', asc)
					            ->get()
					            ->result_array();
					        $trans = array("mode"=> "monthly","statusidname" => $statusidname, "data" => $data);

							break;
						case 'yearly':
							$statusidname = $this->status_service_new_model
											->select('status_id, nama')
											->get()
											->result_array();

					        $data = $this->service_detail_model
					            ->with(array('service','status_service_new'))
					            ->select('YEAR(this.date_created) as hari, status_id, nama')
					            ->order_by('hari', asc)
					            ->get()
					            ->result_array();
					        $trans = array("mode"=> "yearly","statusidname" => $statusidname, "data" => $data);
							break;
						default:
							# code...
							break;
					}
				}

				break;
			case 'area':
				if($det_kategory=='' || $start=='' || $end=='' || $mode==''){
			        $trans = $this->transaction_model
			            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','area','hadiah'))
			            ->where(array('transactiontype_id' => transactiontype::MRDM))
			            ->select('DATE(zn_transaction.rec_created) as hari, sum(item_qty*m_hadiah.harga) as jumlah')
			            ->group_by('DATE(zn_transaction.rec_created)')
			            ->order_by('hari', asc)
			            ->get()
			            ->result_array();			
				}else{
					switch ($mode) {
						case 'daily':
					        $trans = $this->transaction_model
					            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','area','hadiah'))
					            ->where(array('transactiontype_id' => transactiontype::MRDM))
					            ->where('DATE(zn_transaction.rec_created) >=', $start)
					            ->where('DATE(zn_transaction.rec_created) <=', $end)
					            ->where('m_cabang.id_area', $det_kategory)
					            ->select('DATE(zn_transaction.rec_created) as hari, sum(item_qty*m_hadiah.harga) as jumlah')
					            ->group_by('DATE(zn_transaction.rec_created)')
					            ->order_by('hari', asc)
					            ->get()
					            ->result_array();			
							break;
						case 'monthly':
					        $trans = $this->transaction_model
					            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','hadiah'))
					            ->where(array('transactiontype_id' => transactiontype::MRDM))
					            ->where('m_cabang.id_area', $det_kategory)
					            ->where('DATE(zn_transaction.rec_created) >=', $start)
					            ->where('DATE(zn_transaction.rec_created) <=', $end)
					            ->select("concat(YEAR(zn_transaction.rec_created),-MONTH(zn_transaction.rec_created)) as hari, sum(item_qty*m_hadiah.harga) as jumlah")
					            ->group_by("concat(YEAR(zn_transaction.rec_created),-MONTH(zn_transaction.rec_created))")
					            ->order_by('hari', asc)
					            ->get()
					            ->result_array();
							break;
						case 'yearly':
						    $trans = $this->transaction_model
					            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','hadiah'))
					            ->where(array('transactiontype_id' => transactiontype::MRDM))
					            ->where('DATE(zn_transaction.rec_created) >=', $start)
					            ->where('DATE(zn_transaction.rec_created) <=', $end)
					            ->where('m_cabang.id_area', $det_kategory)
					            ->select("concat(YEAR(zn_transaction.rec_created)) as hari, sum(item_qty*m_hadiah.harga) as jumlah")
					            ->group_by("concat(YEAR(zn_transaction.rec_created))")
					            ->order_by('hari', asc)
					            ->get()
					            ->result_array();
							break;
						default:
							# code...
							break;
					}
				}

				break;			
			case 'province':
				if($det_kategory=='' || $start=='' || $end=='' || $mode==''){
			        $trans = $this->transaction_model
			            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','kota','province','hadiah'))
			            ->where(array('transactiontype_id' => transactiontype::MRDM))
			            ->select('DATE(zn_transaction.rec_created) as hari, sum(item_qty*m_hadiah.harga) as jumlah')
			            ->group_by('DATE(zn_transaction.rec_created)')
			            ->order_by('hari', asc)
			            ->get()
			            ->result_array();			
				}else{
					switch ($mode) {
						case 'daily':
					        $trans = $this->transaction_model
					            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','kota','province','hadiah'))
					            ->where(array('transactiontype_id' => transactiontype::MRDM))
					            ->where('DATE(zn_transaction.rec_created) >=', $start)
					            ->where('DATE(zn_transaction.rec_created) <=', $end)
					            ->where('m_provinsi.id_provinsi', $det_kategory)
					            ->select('DATE(zn_transaction.rec_created) as hari, sum(item_qty*m_hadiah.harga) as jumlah')
					            ->group_by('DATE(zn_transaction.rec_created)')
					            ->order_by('hari', asc)
					            ->get()
					            ->result_array();			
							break;
						case 'monthly':
					        $trans = $this->transaction_model
					            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','kota','province','hadiah'))
					            ->where(array('transactiontype_id' => transactiontype::MRDM))
					            ->where('m_provinsi.id_provinsi', $det_kategory)
					            ->where('DATE(zn_transaction.rec_created) >=', $start)
					            ->where('DATE(zn_transaction.rec_created) <=', $end)
					            ->select("concat(YEAR(zn_transaction.rec_created),-MONTH(zn_transaction.rec_created)) as hari, sum(item_qty*m_hadiah.harga) as jumlah")
					            ->group_by("concat(YEAR(zn_transaction.rec_created),-MONTH(zn_transaction.rec_created))")
					            ->order_by('hari', asc)
					            ->get()
					            ->result_array();
							break;
						case 'yearly':
						    $trans = $this->transaction_model
					            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','kota','province','hadiah'))
					            ->where(array('transactiontype_id' => transactiontype::MRDM))
					            ->where('DATE(zn_transaction.rec_created) >=', $start)
					            ->where('DATE(zn_transaction.rec_created) <=', $end)
					            ->where('m_provinsi.id_provinsi', $det_kategory)
					            ->select("concat(YEAR(zn_transaction.rec_created)) as hari, sum(item_qty*m_hadiah.harga) as jumlah")
					            ->group_by("concat(YEAR(zn_transaction.rec_created))")
					            ->order_by('hari', asc)
					            ->get()
					            ->result_array();
							break;
						default:
							# code...
							break;
					}
				}

				break;

			default:
				# code...
				break;
		}

        echo json_encode($trans);

	}

	public function getItemRedempDailyJmlTicket($kategory='', $det_kategory='', $start='', $end='', $mode=''){
		$this->load->model(array('transaction_model'));
		switch ($kategory) {
			case 'store':
				if($det_kategory=='' || $start=='' || $end=='' || $mode==''){
			        $trans = $this->transaction_model
			            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem'))
			            ->where(array('transactiontype_id' => transactiontype::MRDM))
			            ->select('DATE(zn_transaction.rec_created) as hari, sum(item_qty*ticket_count) as jumlah')
			            ->group_by('DATE(zn_transaction.rec_created)')
			            ->order_by('hari', asc)
			            ->get()
			            ->result_array();			
				}else{
					switch ($mode) {
						case 'daily':
					        $trans = $this->transaction_model
					            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem'))
					            ->where(array('transactiontype_id' => transactiontype::MRDM, 'm_cabang.id_cabang'=>$det_kategory))
					            ->where('DATE(zn_transaction.rec_created) >=', $start)
					            ->where('DATE(zn_transaction.rec_created) <=', $end)
					            ->select('DATE(zn_transaction.rec_created) as hari, sum(item_qty*ticket_count) as jumlah')
					            ->group_by('DATE(zn_transaction.rec_created)')
					            ->order_by('hari', asc)
					            ->get()
					            ->result_array();			
							break;
						case 'monthly':
					        $trans = $this->transaction_model
					            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem'))
					            ->where(array('transactiontype_id' => transactiontype::MRDM, 'm_cabang.id_cabang'=>$det_kategory))
					            ->where('DATE(zn_transaction.rec_created) >=', $start)
					            ->where('DATE(zn_transaction.rec_created) <=', $end)
					            ->select("concat(YEAR(zn_transaction.rec_created),-MONTH(zn_transaction.rec_created)) as hari, sum(item_qty*ticket_count) as jumlah")
					            ->group_by("concat(YEAR(zn_transaction.rec_created),-MONTH(zn_transaction.rec_created))")
					            ->order_by('hari', asc)
					            ->get()
					            ->result_array();
							break;
						case 'yearly':
						    $trans = $this->transaction_model
					            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','hadiah'))
					            ->where(array('transactiontype_id' => transactiontype::MRDM, 'm_cabang.id_cabang'=>$det_kategory))
					            ->where('DATE(zn_transaction.rec_created) >=', $start)
					            ->where('DATE(zn_transaction.rec_created) <=', $end)
					            ->select("concat(YEAR(zn_transaction.rec_created)) as hari, sum(item_qty*ticket_count) as jumlah")
					            ->group_by("concat(YEAR(zn_transaction.rec_created))")
					            ->order_by('hari', asc)
					            ->get()
					            ->result_array();
							break;
						default:
							# code...
							break;
					}
				}

				break;
			case 'area':
				if($det_kategory=='' || $start=='' || $end=='' || $mode==''){
			        $trans = $this->transaction_model
			            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','area'))
			            ->where(array('transactiontype_id' => transactiontype::MRDM))
			            ->select('DATE(zn_transaction.rec_created) as hari, sum(item_qty*ticket_count) as jumlah')
			            ->group_by('DATE(zn_transaction.rec_created)')
			            ->order_by('hari', asc)
			            ->get()
			            ->result_array();			
				}else{
					switch ($mode) {
						case 'daily':
					        $trans = $this->transaction_model
					            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','area'))
					            ->where(array('transactiontype_id' => transactiontype::MRDM))
					            ->where('DATE(zn_transaction.rec_created) >=', $start)
					            ->where('DATE(zn_transaction.rec_created) <=', $end)
					            ->where('m_cabang.id_area', $det_kategory)
					            ->select('DATE(zn_transaction.rec_created) as hari, sum(item_qty*ticket_count) as jumlah')
					            ->group_by('DATE(zn_transaction.rec_created)')
					            ->order_by('hari', asc)
					            ->get()
					            ->result_array();			
							break;
						case 'monthly':
					        $trans = $this->transaction_model
					            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem'))
					            ->where(array('transactiontype_id' => transactiontype::MRDM))
					            ->where('m_cabang.id_area', $det_kategory)
					            ->where('DATE(zn_transaction.rec_created) >=', $start)
					            ->where('DATE(zn_transaction.rec_created) <=', $end)
					            ->select("concat(YEAR(zn_transaction.rec_created),-MONTH(zn_transaction.rec_created)) as hari, sum(item_qty*ticket_count) as jumlah")
					            ->group_by("concat(YEAR(zn_transaction.rec_created),-MONTH(zn_transaction.rec_created))")
					            ->order_by('hari', asc)
					            ->get()
					            ->result_array();
							break;
						case 'yearly':
						    $trans = $this->transaction_model
					            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','hadiah'))
					            ->where(array('transactiontype_id' => transactiontype::MRDM))
					            ->where('DATE(zn_transaction.rec_created) >=', $start)
					            ->where('DATE(zn_transaction.rec_created) <=', $end)
					            ->where('m_cabang.id_area', $det_kategory)
					            ->select("concat(YEAR(zn_transaction.rec_created)) as hari, sum(item_qty*ticket_count) as jumlah")
					            ->group_by("concat(YEAR(zn_transaction.rec_created))")
					            ->order_by('hari', asc)
					            ->get()
					            ->result_array();
							break;
						default:
							# code...
							break;
					}
				}

				break;			
			case 'province':
				if($det_kategory=='' || $start=='' || $end=='' || $mode==''){
			        $trans = $this->transaction_model
			            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','kota','province'))
			            ->where(array('transactiontype_id' => transactiontype::MRDM))
			            ->select('DATE(zn_transaction.rec_created) as hari, sum(item_qty*ticket_count) as jumlah')
			            ->group_by('DATE(zn_transaction.rec_created)')
			            ->order_by('hari', asc)
			            ->get()
			            ->result_array();			
				}else{
					switch ($mode) {
						case 'daily':
					        $trans = $this->transaction_model
					            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','kota','province'))
					            ->where(array('transactiontype_id' => transactiontype::MRDM))
					            ->where('DATE(zn_transaction.rec_created) >=', $start)
					            ->where('DATE(zn_transaction.rec_created) <=', $end)
					            ->where('m_provinsi.id_provinsi', $det_kategory)
					            ->select('DATE(zn_transaction.rec_created) as hari, sum(item_qty*ticket_count) as jumlah')
					            ->group_by('DATE(zn_transaction.rec_created)')
					            ->order_by('hari', asc)
					            ->get()
					            ->result_array();			
							break;
						case 'monthly':
					        $trans = $this->transaction_model
					            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','kota','province'))
					            ->where(array('transactiontype_id' => transactiontype::MRDM))
					            ->where('m_provinsi.id_provinsi', $det_kategory)
					            ->where('DATE(zn_transaction.rec_created) >=', $start)
					            ->where('DATE(zn_transaction.rec_created) <=', $end)
					            ->select("concat(YEAR(zn_transaction.rec_created),-MONTH(zn_transaction.rec_created)) as hari, sum(item_qty*ticket_count) as jumlah")
					            ->group_by("concat(YEAR(zn_transaction.rec_created),-MONTH(zn_transaction.rec_created))")
					            ->order_by('hari', asc)
					            ->get()
					            ->result_array();
							break;
						case 'yearly':
						    $trans = $this->transaction_model
					            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem','hadiah','kota','province'))
					            ->where(array('transactiontype_id' => transactiontype::MRDM))
					            ->where('DATE(zn_transaction.rec_created) >=', $start)
					            ->where('DATE(zn_transaction.rec_created) <=', $end)
					            ->where('m_provinsi.id_provinsi', $det_kategory)
					            ->select("concat(YEAR(zn_transaction.rec_created)) as hari, sum(item_qty*ticket_count) as jumlah")
					            ->group_by("concat(YEAR(zn_transaction.rec_created))")
					            ->order_by('hari', asc)
					            ->get()
					            ->result_array();
							break;
						default:
							# code...
							break;
					}
				}

				break;

			default:
				# code...
				break;
		}

        echo json_encode($trans);

	}

	public function getTop5Mesin($kategory='', $det_kategory='', $start='', $end='', $mode=''){
		$this->load->model(array('transaction_teknogame_model', 'store_model','v_mesin_model'));
		$trans='';
		switch ($kategory) {
			case 'store':
				if($det_kategory=='' || $start=='' || $end==''){
			        $trans = $this->v_mesin_model
			            ->select('CONCAT(IFNULL(mesin_teknogame.nama_mesin,""),"(", this.kode_mesin,")") as nama_mesin, jumlah',FALSE)
			            ->with(array('mesin_teknogame','cabang'))
			            ->order_by('jumlah', desc)
			            ->limit(5)
			            ->get()
			            ->result_array();

				}else{
			        $trans = $this->transaction_teknogame_model
			            ->select('mesin_teknogame.kd_mesin, mesin_teknogame.nama_mesin, sum(harga) as jumlah')
			            ->with(array('mesin_teknogame','cabang'))
			            ->where('DATE(tanggal) >=', $start)
			            ->where('DATE(tanggal) <=', $end)
			            ->where('cabang.id_cabang', $det_kategory)
			            ->group_by('mesin_teknogame.kd_mesin')
			            ->order_by('jumlah', desc)
			            ->limit(5)
			            ->get()
			            ->result_array();
				}

				break;
			case 'area':
				if($det_kategory=='' || $start=='' || $end==''){
			        $trans = $this->transaction_teknogame_model
			            ->select('mesin_teknogame.kd_mesin, mesin_teknogame.nama_mesin, sum(harga) as jumlah')
			            ->with(array('mesin_teknogame','cabang','area'))
			            ->group_by('mesin_teknogame.kd_mesin')
			            ->order_by('jumlah', desc)
			            ->limit(5)
			            ->get()
			            ->result_array();

				}else{
			        $trans = $this->transaction_teknogame_model
			            ->select('mesin_teknogame.kd_mesin, mesin_teknogame.nama_mesin, sum(harga) as jumlah')
			            ->with(array('mesin_teknogame','cabang','area'))
			            ->where('DATE(tanggal) >=', $start)
			            ->where('DATE(tanggal) <=', $end)
			            ->where('area.id_area', $det_kategory)
			            ->group_by('mesin_teknogame.kd_mesin')
			            ->order_by('jumlah', desc)
			            ->limit(5)
			            ->get()
			            ->result_array();
				}

				break;
			case 'province':
				if($det_kategory=='' || $start=='' || $end==''){
			        $trans = $this->transaction_teknogame_model
			            ->select('mesin_teknogame.kd_mesin, mesin_teknogame.nama_mesin, sum(harga) as jumlah')
			            ->with(array('mesin_teknogame','cabang'))
			            ->group_by('mesin_teknogame.kd_mesin')
			            ->order_by('jumlah', desc)
			            ->limit(5)
			            ->get()
			            ->result_array();

				}else{
			        $trans = $this->transaction_teknogame_model
			            ->select('mesin_teknogame.kd_mesin, mesin_teknogame.nama_mesin, sum(harga) as jumlah')
			            ->with(array('mesin_teknogame','cabang','kota','province'))
			            ->where('DATE(tanggal) >=', $start)
			            ->where('DATE(tanggal) <=', $end)
			            ->where('province.id_provinsi', $det_kategory)
			            ->group_by('mesin_teknogame.kd_mesin')
			            ->order_by('jumlah', desc)
			            ->limit(5)
			            ->get()
			            ->result_array();
				}

				break;
			
			default:
				# code...
				break;
		}

		

        echo json_encode($trans);

	}

	public function getWorse5Mesin($kategory='', $det_kategory='', $start='', $end='', $mode=''){
		$this->load->model(array('transaction_teknogame_model', 'store_model','v_mesin_model'));
		$trans='';
		switch ($kategory) {
			case 'store':
				if($det_kategory=='' || $start=='' || $end==''){
					$notjenismesin = array('387','388');
			        $trans = $this->v_mesin_model
			            ->select('mesin_teknogame.kd_mesin, mesin_teknogame.nama_mesin, jumlah')
			            ->with(array('mesin_teknogame','cabang'))
			            ->where_not_in('id_jenis_mesin',$notjenismesin)
			            ->order_by('jumlah', ASC)
			            ->limit(5)
			            ->get()
			            ->result_array();

				}else{
			        $trans = $this->transaction_teknogame_model
			            ->select('mesin_teknogame.kd_mesin, mesin_teknogame.nama_mesin, sum(harga) as jumlah')
			            ->with(array('mesin_teknogame','cabang'))
			            ->where('DATE(tanggal) >=', $start)
			            ->where('DATE(tanggal) <=', $end)
			            ->where('cabang.id_cabang', $det_kategory)
			            ->group_by('mesin_teknogame.kd_mesin')
			            ->order_by('jumlah', ASC)
			            ->limit(5)
			            ->get()
			            ->result_array();
				}

				break;
			case 'area':
				if($det_kategory=='' || $start=='' || $end==''){
			        $trans = $this->transaction_teknogame_model
			            ->select('mesin_teknogame.kd_mesin, mesin_teknogame.nama_mesin, sum(harga) as jumlah')
			            ->with(array('mesin_teknogame','cabang','area'))
			            ->group_by('mesin_teknogame.kd_mesin')
			            ->order_by('jumlah', ASC)
			            ->limit(5)
			            ->get()
			            ->result_array();

				}else{
			        $trans = $this->transaction_teknogame_model
			            ->select('mesin_teknogame.kd_mesin, mesin_teknogame.nama_mesin, sum(harga) as jumlah')
			            ->with(array('mesin_teknogame','cabang','area'))
			            ->where('DATE(tanggal) >=', $start)
			            ->where('DATE(tanggal) <=', $end)
			            ->where('area.id_area', $det_kategory)
			            ->group_by('mesin_teknogame.kd_mesin')
			            ->order_by('jumlah', ASC)
			            ->limit(5)
			            ->get()
			            ->result_array();
				}

				break;
			case 'province':
				if($det_kategory=='' || $start=='' || $end==''){
			        $trans = $this->transaction_teknogame_model
			            ->select('mesin_teknogame.kd_mesin, mesin_teknogame.nama_mesin, sum(harga) as jumlah')
			            ->with(array('mesin_teknogame','cabang'))
			            ->group_by('mesin_teknogame.kd_mesin')
			            ->order_by('jumlah', ASC)
			            ->limit(5)
			            ->get()
			            ->result_array();

				}else{
			        $trans = $this->transaction_teknogame_model
			            ->select('mesin_teknogame.kd_mesin, mesin_teknogame.nama_mesin, sum(harga) as jumlah')
			            ->with(array('mesin_teknogame','cabang','kota','province'))
			            ->where('DATE(tanggal) >=', $start)
			            ->where('DATE(tanggal) <=', $end)
			            ->where('province.id_provinsi', $det_kategory)
			            ->group_by('mesin_teknogame.kd_mesin')
			            ->order_by('jumlah', ASC)
			            ->limit(5)
			            ->get()
			            ->result_array();
				}

				break;
			
			default:
				# code...
				break;
		}

		

        echo json_encode($trans);

	}

	public function getTop10Sparepart($kategory='', $det_kategory='', $start='', $end='', $mode=''){
		$this->load->model(array('transaction_teknogame_model', 'store_model', 'item_model', 'repair_sparepart_model'));
		$trans='';
		switch ($kategory) {
			case 'store':
				if($det_kategory=='' || $start=='' || $end==''){
			        $trans = $this->repair_sparepart_model
			            ->select('item.item_name, sum(qty) as jumlah')
			            ->with(array('item'))
			            ->group_by('this.item_id')
			            ->order_by('jumlah', desc)
			            ->limit(10)
			            ->get()
			            ->result_array();

				}else{
			        $trans = $this->repair_sparepart_model
			            ->select('item.item_name, sum(qty) as jumlah')
			            ->with(array('item'))
			            ->group_by('this.item_id')
			            ->order_by('jumlah', desc)
			            ->limit(10)
			            ->get()
			            ->result_array();
				}

				break;
			case 'area':
				if($det_kategory=='' || $start=='' || $end==''){
			        $trans = $this->transaction_teknogame_model
			            ->select('mesin_teknogame.kd_mesin, mesin_teknogame.nama_mesin, sum(harga) as jumlah')
			            ->with(array('mesin_teknogame','cabang','area'))
			            ->group_by('mesin_teknogame.kd_mesin')
			            ->order_by('jumlah', desc)
			            ->limit(5)
			            ->get()
			            ->result_array();

				}else{
			        $trans = $this->transaction_teknogame_model
			            ->select('mesin_teknogame.kd_mesin, mesin_teknogame.nama_mesin, sum(harga) as jumlah')
			            ->with(array('mesin_teknogame','cabang','area'))
			            ->where('DATE(tanggal) >=', $start)
			            ->where('DATE(tanggal) <=', $end)
			            ->where('area.id_area', $det_kategory)
			            ->group_by('mesin_teknogame.kd_mesin')
			            ->order_by('jumlah', desc)
			            ->limit(5)
			            ->get()
			            ->result_array();
				}

				break;
			case 'province':
				if($det_kategory=='' || $start=='' || $end==''){
			        $trans = $this->transaction_teknogame_model
			            ->select('mesin_teknogame.kd_mesin, mesin_teknogame.nama_mesin, sum(harga) as jumlah')
			            ->with(array('mesin_teknogame','cabang'))
			            ->group_by('mesin_teknogame.kd_mesin')
			            ->order_by('jumlah', desc)
			            ->limit(5)
			            ->get()
			            ->result_array();

				}else{
			        $trans = $this->transaction_teknogame_model
			            ->select('mesin_teknogame.kd_mesin, mesin_teknogame.nama_mesin, sum(harga) as jumlah')
			            ->with(array('mesin_teknogame','cabang','kota','province'))
			            ->where('DATE(tanggal) >=', $start)
			            ->where('DATE(tanggal) <=', $end)
			            ->where('province.id_provinsi', $det_kategory)
			            ->group_by('mesin_teknogame.kd_mesin')
			            ->order_by('jumlah', desc)
			            ->limit(5)
			            ->get()
			            ->result_array();
				}

				break;
			
			default:
				# code...
				break;
		}

		

        echo json_encode($trans);

	}

	public function getOmzet($kategory='', $det_kategory='', $start='', $end='', $mode=''){
		$this->load->model(array('transaction_teknogame_model', 'store_model','omzet_mesin_model'));
		$trans='';
		switch ($kategory) {
			case 'store':
				if($det_kategory=='' || $start=='' || $end=='' || $mode==''){
			        $trans = $this->omzet_mesin_model
			            ->select('hari, mesin_teknogame.kd_mesin, mesin_teknogame.nama_mesin, jumlah')
			            ->with(array('mesin_teknogame','cabang'))
			            ->order_by('hari', asc)
			            ->get()
			            ->result_array();		
				}else{
					switch ($mode) {
						case 'daily':
					        $trans = $this->omzet_mesin_model
					            ->select('hari, mesin_teknogame.kd_mesin, mesin_teknogame.nama_mesin,jumlah')
					            ->with(array('mesin_teknogame','cabang'))
					            ->where('cabang.id_cabang', $det_kategory)
					            ->where('hari >=', $start)
					            ->where('hari <=', $end)
					            ->order_by('hari', asc)
					            ->get()
					            ->result_array();

							break;

						case 'monthly':
					        $trans = $this->transaction_teknogame_model
					            ->select("concat(YEAR(tanggal),-MONTH(tanggal)) as hari, mesin_teknogame.kd_mesin, mesin_teknogame.nama_mesin, sum(harga) as jumlah")
					            ->with(array('mesin_teknogame','cabang'))
					            ->where('DATE(tanggal) >=', $start)
					            ->where('DATE(tanggal) <=', $end)
					            ->where('cabang.id_cabang', $det_kategory)
					            ->group_by("concat(YEAR(tanggal),-MONTH(tanggal))")
					            ->order_by('hari', asc)
					            ->get()
					            ->result_array();

							break;

						case 'yearly':
					        $trans = $this->transaction_teknogame_model
					            ->select("YEAR(tanggal) as hari, mesin_teknogame.kd_mesin, mesin_teknogame.nama_mesin, sum(harga) as jumlah")
					            ->with(array('mesin_teknogame','cabang'))
					            ->where('DATE(tanggal) >=', $start)
					            ->where('DATE(tanggal) <=', $end)
					            ->where('cabang.id_cabang', $det_kategory)
					            ->group_by("YEAR(tanggal)")
					            ->order_by('hari', asc)
					            ->get()
					            ->result_array();
							break;
						
						default:
							# code...
							break;
					}

				}

				break;

				case 'area':
					if($det_kategory=='' || $start=='' || $end=='' || $mode==''){
				        $trans = $this->transaction_teknogame_model
				            ->select('DATE(tanggal) hari, mesin_teknogame.kd_mesin, mesin_teknogame.nama_mesin, sum(harga) as jumlah')
				            ->with(array('mesin_teknogame','cabang','area'))
							->group_by('DATE(tanggal)')
				            ->order_by('hari', asc)
				            ->get()
				            ->result_array();		
					}else{
						switch ($mode) {
							case 'daily':
						        $trans = $this->transaction_teknogame_model
						            ->select('DATE(tanggal) hari, mesin_teknogame.kd_mesin, mesin_teknogame.nama_mesin, sum(harga) as jumlah')
						            ->with(array('mesin_teknogame','cabang', 'area'))
						            ->where('area.id_area', $det_kategory)
						            ->where('DATE(tanggal) >=', $start)
						            ->where('DATE(tanggal) <=', $end)
									->group_by('DATE(tanggal)')
						            ->order_by('hari', asc)
						            ->get()
						            ->result_array();

								break;

							case 'monthly':
						        $trans = $this->transaction_teknogame_model
						            ->select("concat(YEAR(tanggal),-MONTH(tanggal)) as hari, mesin_teknogame.kd_mesin, mesin_teknogame.nama_mesin, sum(harga) as jumlah")
						            ->with(array('mesin_teknogame','cabang','area'))
						            ->where('DATE(tanggal) >=', $start)
						            ->where('DATE(tanggal) <=', $end)
						            ->where('area.id_area', $det_kategory)
						            ->group_by("concat(YEAR(tanggal),-MONTH(tanggal))")
						            ->order_by('hari', asc)
						            ->get()
						            ->result_array();

								break;

							case 'yearly':
						        $trans = $this->transaction_teknogame_model
						            ->select("YEAR(tanggal) as hari, mesin_teknogame.kd_mesin, mesin_teknogame.nama_mesin, sum(harga) as jumlah")
						            ->with(array('mesin_teknogame','cabang','area'))
						            ->where('DATE(tanggal) >=', $start)
						            ->where('DATE(tanggal) <=', $end)
						            ->where('area.id_area', $det_kategory)
						            ->group_by("YEAR(tanggal)")
						            ->order_by('hari', asc)
						            ->get()
						            ->result_array();
								break;
							
							default:
								# code...
								break;
						}

					}
					break;
				case 'province':
					if($det_kategory=='' || $start=='' || $end=='' || $mode==''){
				        $trans = $this->transaction_teknogame_model
				            ->select('DATE(tanggal) hari, mesin_teknogame.kd_mesin, mesin_teknogame.nama_mesin, sum(harga) as jumlah')
				            ->with(array('mesin_teknogame','cabang','kota','province'))
							->group_by('DATE(tanggal)')
				            ->order_by('hari', asc)
				            ->get()
				            ->result_array();		
					}else{
						switch ($mode) {
							case 'daily':
						        $trans = $this->transaction_teknogame_model
						            ->select('DATE(tanggal) hari, mesin_teknogame.kd_mesin, mesin_teknogame.nama_mesin, sum(harga) as jumlah')
						            ->with(array('mesin_teknogame','cabang', 'kota','province'))
						            ->where('province.id_provinsi', $det_kategory)
						            ->where('DATE(tanggal) >=', $start)
						            ->where('DATE(tanggal) <=', $end)
									->group_by('DATE(tanggal)')
						            ->order_by('hari', asc)
						            ->get()
						            ->result_array();

								break;

							case 'monthly':
						        $trans = $this->transaction_teknogame_model
						            ->select("concat(YEAR(tanggal),-MONTH(tanggal)) as hari, mesin_teknogame.kd_mesin, mesin_teknogame.nama_mesin, sum(harga) as jumlah")
						            ->with(array('mesin_teknogame','cabang','kota','province'))
						            ->where('DATE(tanggal) >=', $start)
						            ->where('DATE(tanggal) <=', $end)
						            ->where('province.id_provinsi', $det_kategory)
						            ->group_by("concat(YEAR(tanggal),-MONTH(tanggal))")
						            ->order_by('hari', asc)
						            ->get()
						            ->result_array();

								break;

							case 'yearly':
						        $trans = $this->transaction_teknogame_model
						            ->select("YEAR(tanggal) as hari, mesin_teknogame.kd_mesin, mesin_teknogame.nama_mesin, sum(harga) as jumlah")
						            ->with(array('mesin_teknogame','cabang','kota','province'))
						            ->where('DATE(tanggal) >=', $start)
						            ->where('DATE(tanggal) <=', $end)
						            ->where('province.id_provinsi', $det_kategory)
						            ->group_by("YEAR(tanggal)")
						            ->order_by('hari', asc)
						            ->get()
						            ->result_array();
								break;
							
							default:
								# code...
								break;
						}

					}

					break;
			
			default:
				# code...
				break;
		}

		
        echo json_encode($trans);

	}


	public function store_empty(){
		$data = $this->store_model->options_empty();
		$arrobj= array();
		foreach ($data as $key => $value) {
			$stdClass = new StdClass();
			$stdClass->id = $key;
			$stdClass->text = $value;
			if($key != ''){
				array_push($arrobj, $stdClass);
			}
		}
		echo json_encode($arrobj);
	}

	public function area_empty(){
		$data = $this->area_model->options_empty();
		$arrobj= array();
		foreach ($data as $key => $value) {
			$stdClass = new StdClass();
			$stdClass->id = $key;
			$stdClass->text = $value;
			if($key != ''){
				array_push($arrobj, $stdClass);
			}
		}
		echo json_encode($arrobj);
	}

	public function province_empty(){
		$data = $this->provinsi_model->options_empty();
		$arrobj= array();
		foreach ($data as $key => $value) {
			$stdClass = new StdClass();
			$stdClass->id = $key;
			$stdClass->text = $value;
			if($key != ''){
				array_push($arrobj, $stdClass);
			}
		}
		echo json_encode($arrobj);		
	}


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
<?php

/**
 * Description of bbm
 *
 * @author SANGGRA HANDI
 */
class kategori_mesin extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {

        $data['title'] = 'Machine Category';
        $data['data_source'] = base_url($this->class_name . '/load');

        $data['button_group'] = $this->getAvailableButtons();
        $data['button_right'] = $this->getTools();

        /* INSERT LOG */
        $this->access_right->activity_logs('view','Machine Category');

        $data['assets_url'] = $this->config->item('assets_url');
        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'base/page_content.tpl'; 
        $data['nama_pegawai'] = $this->currentUsername;    
        
        $this->twig->display('index.tpl', $data);
    }

    public function load($page = 0) {
        $this->load->library ( "custom_table" );
        
        $table = new stdClass ();
        $header [0] = array (
                "CATEGORY CODE",1,1,
                "CATEGORY NAME",1,1,
        );
        
        if ($this->access_right->otoritas ( 'edit' ) || $this->access_right->otoritas ( 'delete' )) {
            $header [0] = hgenerator::merge_array_raw ( $header [0], array (
                    "ACTION",1,1 
            ) );
        }
        
        $table->header = $header;
        $table->id = 't_kategori_mesin';
        $table->align = array (
                'tanggal_bbm' => 'center',
                'kd_bbm' => 'center',
                'kd_purchase_order' => 'center',
                'supplier' => 'center',
                'aksi' => 'center' 
        );
        $table->style = "table table-striped table-bordered table-hover datatable dataTable";
        $table->model = "kategori_mesin_model->data_table";
        $table->limit = $this->limit;
        $table->page = $page;
        $data = $this->custom_table->generate_ajax ( $table );
        
        echo json_encode ( $data );
    }

    public function add($id='',$status_delete = 0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->config->load('twig');

            $title = 'Tambah Data';
            $data['form_action'] = $this->class_name . '/proses';

            $data['id'] = $id;
            if ($id != '') {
                $title = 'Edit Data';

                $row = $this->kategori_mesin_model->getById($id)->row();
                $data['data'] = $row;
            }

            $data['title']      = $title;
            $data['assets_url'] = $this->config->item('assets_url');
            $data['sidebar']    = $this->access_right->menu();
            $data['form']       = 'kategori_mesin/form.tpl';

            $button_group   = array();
            $button_group[] = view::button_back();

            if($status_delete == 0){
                $data['form_action']    = view::form_input($id);
                $button_group[]         = view::button_save();
            }else{
                $data['title']          = 'Delete Machine Category';
                $data['form_action']    = view::form_delete($id);;
                $data['readonly']       = 'readonly=""';
                $button_group[]         = view::button_delete_confirm();
            }

            $data['button_group'] = view::render_button_group($button_group);
            $this->twig->display('base/page_form.tpl', $data);
        } else {
            $this->access_right->redirect();
        }
    }

    public function edit($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('edit', true);
        $this->add($id);
    }

    public function delete($id_enc) {        
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('delete', true);
        $this->add($id,1);
    }

    public function proses() {        
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('data[kd_kategori_mesin]', 'Class Code', 'required|trim');
            $this->form_validation->set_rules('data[nama_kategori_mesin]', 'Class Name', 'required|trim');

            if ($this->form_validation->run()) {
                $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');

                $id = $this->input->post('id');
                $data = $this->input->post('data');

                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */

                if ($id == '') {
                    if ($this->kategori_mesin_model->create($data)) {
                        $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh_filter()');
                        /* INSERT LOG */
                        $this->access_right->activity_logs('add','Tambah Kategori Mesin');
                        /* END OF INSERT LOG */
                    }
                } else {
                    if ($this->kategori_mesin_model->update($data, $id)) {
                        $message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', 'refresh_filter()');
                        /* INSERT LOG */
                        $this->access_right->activity_logs('edit','Edit Kategori Mesin');
                        /* END OF INSERT LOG */
                    }
                }
            } else {
                $message = array(false, 'Terjadi Kesalahan', validation_errors(), '');
            }
            echo json_encode($message);
        } else {
            $this->access_right->redirect();
        }
    }

    public function proses_delete() {
        $id = $this->input->post('id');
        $this->access_right->otoritas('delete', true);
        if ($this->access_right->otoritas('delete', true)) {
            $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '','id : '.$id);
            if ($this->kategori_mesin_model->delete($id)) {
                $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', 'refresh_filter()');
                /* INSERT LOG */
                $this->access_right->activity_logs('delete','Delete kategori_mesin');
                /* END OF INSERT LOG */
            }
            echo json_encode($message);
        }
    }

}
/* End of file bbm.php */
/* Location: ./application/controllers/bbm.php */

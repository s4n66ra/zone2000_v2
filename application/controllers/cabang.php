<?php

/**
 * Description of bbm
 *
 * @author SANGGRA HANDI
 */
class cabang extends CI_Controller {

    private $class_name;
    private $limit = 50;

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->class_name = get_class($this);

        // Protection
        hprotection::login();
        $this->access_right->check();
        $this->access_right->otoritas('view', true);
		
		/* Otoritas */
		//$this->access_right->check();
		//$this->access_right->otoritas('view', true);
		
        // Global Model
        $this->load->model(array($this->class_name . '_model'));
    }

    
    public function index() {
        $data['title'] = 'Branch Master';
        $data['page'] = $this->class_name . '/index';
        $data['data_source'] = base_url($this->class_name . '/load');

        /* INSERT LOG */
        $this->access_right->activity_logs('view',$data['title']);

        /* END OF INSERT LOG */
        $data['assets_url'] = $this->config->item('assets_url');
        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'cabang/table_cabang.tpl';
        $data['data_table'] = $this->load();     
        $data['nama_pegawai'] = $this->currentUsername;   
        $data['add_button'] = '<a id="sample_editable_1_new" class="btn green" href="'.base_url('cabang/add').'">Add New <i class="fa fa-plus"></i></a>';        
        $data['input_kd_cabang'] = form_input('kd_cabang', '', 'class="form-control input-inline input-medium"');
        $data['input_nama_cabang'] =  form_input('nama_cabang', '', 'class="form-control input-inline input-medium"');
        

        $this->twig->display('index.tpl', $data);
    }

    public function load($page = 0) {
        $this->load->library("custom_table");

        $table = new stdClass();
        $header[0] = array(
                            "BRANCH CODE",1,1,
                            "BRANCH NAME", 1, 1,
                            "AREA",1,1,
                            "BRANCH ADDRESS", 1, 1,
                            "TELP",1,1,
                            "SUPERVISOR", 1, 1,
                            "LEADER",1,1,
                            "WIDE", 1, 1,
                            "TAX OF BRANCH", 1, 1

                        );

        if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
            $header[0] = array(
                            "BRANCH CODE",1,1,
                            "BRANCH NAME", 1, 1,
                            "AREA",1,1,
                            "BRANCH ADDRESS", 1, 1,
                            "TELP",1,1,
                            "SUPERVISOR", 1, 1,
                            "LEADER",1,1,
                            "WIDE", 1, 1,
                            "TAX OF BRANCH", 1, 1,
                            "ACTION",1,1
                            );
        }

        $table->header = $header;
        $table->id     = 't_bbm';
        $table->align = array('tanggal_bbm' => 'center', 'kd_bbm' => 'center','kd_purchase_order'=>'center','supplier'=>'center', 'aksi' => 'center');
        $table->style = "table table-striped table-bordered table-hover datatable dataTable";
        $table->model = "cabang_model->data_table";
        $table->limit = $this->limit;
        $table->page = $page;
        $data = $this->custom_table->generate($table);
        return $data;
    }

    public function get_detail_bbm($id_bbm) {
        $data['title'] = '<i class="icon-plus-sign-alt"></i> Master cabang';
        $data['page'] = $this->class_name . '/get_detail_bbm';
        $data['data_source'] = base_url($this->class_name . '/load_detail_bbm');

 
        /* Otoritas Tombol tambah */
     
            $data['button_group'] = array();
            if ($this->access_right->otoritas('add')) {
                $data['button_group'] = array(
                    anchor(null, '<i class="icon-plus"></i> Tambah Data Detail', array('id' => 'button-add-detail-'.$id_bbm, 'class' => 'btn yellow', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url($this->class_name . '/add_detail/'.$id_bbm)))
                );
            }
        //=========tombol cetak excel==========
        $base_url_images = base_url() . 'images';
        $base_url = base_url() . $this->class_name;
        if ($this->access_right->otoritas('print')) {
            $data['print_group'] = array(
                anchor(null, '<img src="' . $base_url_images . '/doc_excel.png" />', array('id' => 'button-print-excel-'.$id_bbm, 'class' => 'btn outline pull-right', 'rel' => 'tooltip', 'title' => 'Cetak Laporan ke Excel', 'onclick' => 'do_print(this.id, \'#ffilter_detail_'.$id_bbm.'\')', 'data-source' => $base_url . '/excel_detail/'.$id_bbm))
            );
        }
        //-------------------------------------
        $this->load->model('referensi_satuan_model');
        $data['options_satuan'] = $this->referensi_satuan_model->options();
         
         $data['id_bbm'] = $id_bbm;
        /* INSERT LOG */
        $this->access_right->activity_logs('view','Bukti Barang Masuk');
        /* END OF INSERT LOG */
        $this->load->view('bbm/detail_bbm', $data);
    }

    public function load_detail_bbm($page = 0) {
        $this->load->library("custom_table_detail");

        $table = new stdClass();
        $header[0] = array(
                            "KODE BARANG",1,1,
                            "NAMA BARANG",1,1,
                            "JUMLAH", 1, 1, 
                            "SATUAN", 1, 1, 
                            "KATEGORI",1,1
                        );

        if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
            $header[0] = array(
                            "KODE BARANG",1,1,
                             "NAMA BARANG",1,1,
                            "JUMLAH", 1, 1, 
                            "SATUAN", 1, 1, 
                            "KATEGORI",1,1,
                            "AKSI",1,1
                            );
        }

        $table->header = $header;
        $table->id     = 't_bbm_detail';
        $table->align = array('tanggal_bbm' => 'center', 'jumlah' => 'center','satuan'=>'center','kategori'=>'center', 'aksi' => 'center');
        $table->style = "table table-striped table-bordered table-hover datatable dataTable";
        $table->model = "cabang_model->data_table_detail";
        $table->limit = $this->limit;
        $table->page = $page;
        $data = $this->custom_table_detail->generate($table);

        echo json_encode($data);
    }

    public function add($id = '', $status_delete=0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->load->model('area_model');
            $this->load->model('cabang_model');
            $this->load->model('pegawai_model');
            $this->load->model('jabatan_model');
            $this->load->model('level_model');

            $title = 'Tambah Data';
            $data['form_action'] = $this->class_name . '/proses';

            $data['id'] = $id;
            if ($id != '') {
                $title = 'Edit Data';

                $db = $this->cabang_model->get_by_id($id);
                if ($db->num_rows() > 0) {
                    $row = $db->row();
                    $data['default'] = $row;
                    $data['def_kd_cabang'] = $row->kd_cabang;
                    $data['def_nama_cabang'] = $row->nama_cabang;
                    $data['def_alamat'] = $row->alamat;
                    $data['def_telp'] = $row->telp;
                    $data['def_luas'] = $row->luas;
                    $data['def_pajak_toko'] = $row->pajak_toko;
                    $def_id_supervisor = $row->id_supervisor;
                    $def_id_kepcabang = $row->id_kepcabang;
                    $def_id_area = $row->id_area;
                }
            }

            $data['title'] = $title;

            $data['assets_url'] = $this->config->item('assets_url');
            $data['sidebar'] = $this->access_right->menu();
            if($status_delete == 0){
                $data['form_action'] = '<form action="'.base_url('cabang/proses').'" method="post" id="form_sample_3" class="form-horizontal" novalidate="novalidate"><input name = "id" type="hidden" value="'.$id.'">';
                $data['button_submit'] = '<button type="submit" class="btn green">Submit</button>';
            }else{
                $data['title'] = 'Delete Data Provinsi';
                $data['form_action'] = '<form action="'.base_url('cabang/proses_delete').'" method="post" id="form_sample_3" class="form-horizontal" novalidate="novalidate"><input name = "id" type="hidden" value="'.$id.'">';
                $data['button_submit'] = '<button type="submit" class="btn red">Delete</button>';
            }

            $data['button_submit'] .= '<a href = "'.base_url('cabang').'" class="btn default">Cancel</a>';
            $data['form'] = 'cabang/form_cabang.tpl';

            $options_pegawai = $this->pegawai_model->options();
            $options_area = $this->area_model->options();

            $data['options_supervisor'] = form_dropdown('id_supervisor',$options_pegawai, !empty($def_id_supervisor) ? $def_id_supervisor : '', 'class = "form-control"'); 
            $data['options_kepcabang'] = form_dropdown('id_kepcabang',$options_pegawai, !empty($def_id_kepcabang) ? $def_id_kepcabang : '', 'class = "form-control"'); 
            $data['options_area'] = form_dropdown('id_area',$options_area, !empty($def_id_area) ? $def_id_area : '', 'class = "form-control"'); 
            

            $this->twig->display('form.tpl', $data);
        } else {
            $this->access_right->redirect();
        }
    }

    public function edit($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('edit', true);
        $this->add($id);
    }

    public function delete($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('delete', true);
        $this->add($id,$status_delete = 1);
    }

    public function proses() {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('kd_cabang', 'Kode cabang', 'required|trim');
            $this->form_validation->set_rules('nama_cabang', 'Nama cabang', 'required|trim');

            if ($this->form_validation->run()) {
                $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');

                $id = $this->input->post('id');

                $data = array(
                    'kd_cabang' => $this->input->post('kd_cabang'),
                    'nama_cabang' => $this->input->post('nama_cabang'),
                    'id_area' => $this->input->post('id_area'),
                    'alamat' => $this->input->post('alamat'),
                    'telp' => $this->input->post('telp'),
                    'id_supervisor' => $this->input->post('id_supervisor'),
                    'id_kepcabang' => $this->input->post('id_kepcabang'),
                    'luas' => $this->input->post('luas'),
                    'pajak_toko' => $this->input->post('pajak_toko')
                );

                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */

                if ($id == '') {
                    if ($this->cabang_model->create($data)) {
                        $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh_filter()');
						/* INSERT LOG */
						$this->access_right->activity_logs('add','Tambah Cabang');
						/* END OF INSERT LOG */
                    }
                } else {
                    if ($this->cabang_model->update($data, $id)) {
                        $message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', 'refresh_filter()');
						/* INSERT LOG */
						$this->access_right->activity_logs('edit','Edit BBM');
						/* END OF INSERT LOG */
                    }
                }
            } else {
                $message = array(false, 'Terjadi Kesalahan', validation_errors(), '');
            }
            redirect(base_url($this->class_name));            
        } else {
            $this->access_right->redirect();
        }
    }

    public function proses_delete($id) {
        $id = $this->input->post('id');
		$this->access_right->otoritas('delete', true);
        if ($this->access_right->otoritas('delete', true)) {
            $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');
            if ($this->cabang_model->delete($id)) {
                $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', 'refresh_filter()');
				/* INSERT LOG */
				$this->access_right->activity_logs('delete','Delete BBM');
				/* END OF INSERT LOG */
            }
            redirect(base_url($this->class_name));
        }
    }

    public function add_detail($id_bbm = '') {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {

            $title = 'Tambah Data';
            $data['form_action'] = $this->class_name . '/proses_detail';

            $data['id'] = '';
            $data['id_bbm'] = $id_bbm;

        $data['title'] = '<i class="icon-edit"></i> ' . $title;
        $this->load->model('referensi_barang_model');
        $this->load->model('referensi_purchase_order_model');
        $data['options_barang'] = $this->referensi_barang_model->options();
        $data['options_purchase_order'] = $this->referensi_purchase_order_model->options();
            

            $this->load->view($this->class_name . '/form_detail', $data);
        } else {
            $this->access_right->redirect();
        }
    }

    public function edit_detail($id_bbm,$id) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->load->model('cabang_model');

            $title = 'Tambah Data';
            $data['form_action'] = $this->class_name . '/proses_detail';

            $data['id'] = $id;
            $data['id_bbm'] = $id_bbm;
            if ($id != '') {
                $title = 'Edit Data Detail';

                $db = $this->cabang_model->get_detail_by_id($id);
                if ($db->num_rows() > 0) {
                    $row = $db->row();
                    $data['default'] = $row;
                }
            }

        $data['title'] = '<i class="icon-edit"></i> ' . $title;
        $this->load->model('referensi_barang_model');
        $this->load->model('referensi_purchase_order_model');
        $data['options_barang'] = $this->referensi_barang_model->options();
        $data['options_purchase_order'] = $this->referensi_purchase_order_model->options();
            

            $this->load->view($this->class_name . '/form_detail', $data);
        } else {
            $this->access_right->redirect();
        }
    }

    public function proses_detail() {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('kd_barang', 'Nama Barang', 'required|trim');
            $this->form_validation->set_rules('jumlah', 'Jumlah', 'required|trim');


            if ($this->form_validation->run()) {
                $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');

                $id = $this->input->post('id');

                $data = array(
                    'id_barang' => $this->input->post('kd_barang'),
                    'jumlah_barang' => $this->input->post('jumlah'),
                );

                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */

                if ($id == '') {
                    $data['id_bbm'] = $this->input->post('id_bbm');
                    if ($this->cabang_model->create_detail($data)) {
                        $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh_filter()');
                        /* INSERT LOG */
                        $this->access_right->activity_logs('add','Tambah BBM');
                        /* END OF INSERT LOG */
                    }
                } else {
                    if ($this->cabang_model->update_detail($data, $id)) {
                        $message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', 'refresh_filter()');
                        /* INSERT LOG */
                        $this->access_right->activity_logs('edit','Edit BBM');
                        /* END OF INSERT LOG */
                    }
                }
            } else {
                $message = array(false, 'Terjadi Kesalahan', validation_errors(), '');
            }
            echo json_encode($message);
        } else {
            $this->access_right->redirect();
        }
    }

    public function delete_detail($id_bbm,$id) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->load->model('cabang_model');

            $title = 'Hapus Data';
            $data['form_action'] = $this->class_name . '/proses_delete_detail/'.$id;

            $data['id'] = $id;
            $data['id_bbm'] = $id_bbm;
            if ($id != '') {
                $title = 'Edit Data Detail';

                $db = $this->cabang_model->get_detail_by_id($id);
                if ($db->num_rows() > 0) {
                    $row = $db->row();
                    $data['default'] = $row;
                }
            }

        $data['title'] = '<i class="icon-edit"></i> ' . $title;
        $this->load->model('referensi_barang_model');
        $this->load->model('referensi_purchase_order_model');
        $data['options_barang'] = $this->referensi_barang_model->options();
        $data['options_purchase_order'] = $this->referensi_purchase_order_model->options();
            

            $this->load->view($this->class_name . '/form_detail_delete', $data);
        } else {
            $this->access_right->redirect();
        }
    }

    public function proses_delete_detail($id) {
        $this->access_right->otoritas('delete', true);
        if ($this->access_right->otoritas('delete', true)) {
            $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');
            if ($this->cabang_model->delete_detail($id)) {
                $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', 'refresh_filter()');
                /* INSERT LOG */
                $this->access_right->activity_logs('delete','Delete BBM');
                /* END OF INSERT LOG */
            }
            echo json_encode($message);
        }
    }

    public function excel($page = 0){

        $this->load->library("custom_table");

        $table = new stdClass();
        $header[0] = array(
                            "TANGGAL BBM",1,1,
                            "KODE BBM", 1, 1, 
                            "KODE PURCHASE ORDER", 1, 1, 
                            "SUPPLIER",1,1
                        );
        $table->header = $header;
        $table->id     = 't_bbm';
        $table->align = array('tanggal_bbm' => 'center', 'kd_bbm' => 'center','kd_purchase_order'=>'center','supplier'=>'center', 'aksi' => 'center');
        $table->style = "table table-striped table-bordered table-hover datatable dataTable";
        $table->model = "cabang_model->data_table_excel";
        $table->limit = $this->limit;
        $table->border = 1;
        $table->page = 1;
        $data = $this->custom_table->generate($table);
        
        //===========filter di excel===================
        $id_supplier= $this->input->post("kd_supplier");
        $id_purchase_order = $this->input->post('kd_po');
        $tanggal_awal = $this->input->post('tanggal_awal_bbm');
        $tanggal_akhir = $this->input->post('tanggal_akhir_bbm');
        $tahun_aktif = $this->input->post('tahun_aktif');
        $kd_bbm = $this->input->post('kd_bbm');
        if(empty($kd_bbm)){
            $bbm = 'Semua';
        }else{
            $bbm = $kd_bbm;
        }
        if(empty($tahun_aktif)){
            $tahun_aktif = date('Y');
        }
        if($id_supplier==''){
            $supplier = 'Semua';
        }else{
            $supplier       = $id_supplier;
        }
        if($id_purchase_order==''){
            $po = 'Semua';
        }else{
            $po = $id_purchase_order;
        }
        if($tanggal_awal==''||$tanggal_akhir==''){
            $tanggal_awal = 'Semua';
            $tanggal_akhir = 'Semua';
        }
        
        $filter = array(
            'Kode BBM'          =>$bbm,
            'Purchase Order'    =>$po,
            'Supplier'          =>$supplier,
            'Periode Awal'      =>$tanggal_awal,
            'Periode Akhir'     =>$tanggal_akhir,
            'Tahun Aktif'       =>$tahun_aktif);
        $data['filter'] = $filter;
        //----------------------------------------------------
        $date = date('Ymd His');
        $data['judul_kecil'] = 'Bukti Barang Masuk';
        $data['filename'] = 'Bukti Barang Masuk ('.$date.')';
        $this->load->view('cetak_excel',$data);
    }

    public function excel_detail($id_bbm) {
        $this->load->library("custom_table_detail");

        $table = new stdClass();
        $header[0] = array(
                            "KODE BARANG",1,1,
                            "NAMA BARANG",1,1,
                            "JUMLAH", 1, 1, 
                            "SATUAN", 1, 1, 
                            "KATEGORI",1,1
                        );

        $table->header = $header;
        $table->id     = 't_bbm_detail';
        $table->align = array('tanggal_bbm' => 'center', 'jumlah' => 'center','satuan'=>'center','kategori'=>'center', 'aksi' => 'center');
        $table->style = "table table-striped table-bordered table-hover datatable dataTable";
        $table->model = "cabang_model->data_table_detail_excel";
        $table->limit = $this->limit;
        $table->page = 1;
        $table->border = 1;
        $data = $this->custom_table_detail->generate($table);
        //============data detail==================
        $data_bbm = $this->cabang_model->get_by_id($id_bbm);
        if($data_bbm->num_rows()>0){
            $bbm = $data_bbm->row();
            $filter = array(
                'Tanggal BBM'       =>$bbm->tanggal_bbm,
                'Kode BBM'          =>$bbm->kd_bbm,
                'Kode PO'           =>$bbm->kd_po,
                'Kode Supplier'     =>$bbm->nama_supplier.' ('.$bbm->kd_supplier.')');
            $data['filter'] = $filter;
        }
        //------------------------------------------------
        
        $date = date('Ymd His');
        $data['judul_kecil'] = 'Bukti Barang Masuk Detail';
        $data['filename'] = 'Bukti Barang Masuk Detail ('.$date.')';
        $this->load->view('cetak_excel',$data);
    }



}

/* End of file bbm.php */
/* Location: ./application/controllers/bbm.php */

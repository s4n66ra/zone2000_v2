<?php

/**
 * Description of bbm
 *
 * @author SANGGRA HANDI
 */
class promosi extends My_Controller {


    public function __construct() {
        parent::__construct();
        $this->load->model('promosi_model');
    }

    
    public function index() {
        $data['title'] = 'Setting Promosi Outlet';

        /* INSERT LOG */
        //$this->access_right->activity_logs('view',$data['title']);
        /* END OF INSERT LOG */

        $data['button_group'] = $this->getAvailableButtons();
        $data['button_right'] = $this->getTools();

        $data['table']['main']  = $this->table_main(array('wrapper', 'filter'));
        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'base/page_content.tpl';
    
        $this->twig->display('index.tpl', $data);
    }

    public function table_main($option = array()){
        $this->load->model('range_tiket_model');
        $this->load->library('Datatable');
        $table = $this->datatable;

        /*if (in_array('filter', $option)) {
            $table->dataFilter = array(
                                array('nama_promosi', 'Merchandise Name', 'text'),
                                array('this.promositype_id', 'Type', 'list', $this->promosi_type_model->options()),
                            );
        }*/

        if (in_array('wrapper', $option)) {
            $table->numbering  = true;
            $table->isScrollable= false;
            $table->id         = 'table-promosi';
            $table->header     = array('DATE BEGIN','DATE END','SKU','NAME','REG TICKET','PROMOSI', 'STORE','NOTE', 'ACTION');
            $table->source     = site_url($this->class_name.'/table_main');
            return $table->generateWrapper();
        } else {
            $table
                ->setModel($this->promosi_model)
                ->setNumbering()
                ->with(array('item','outlet'))
                ->select('date_begin,date_end,item_key, item_name ,this.item_id , qty_tiket, nama_cabang,note,promosi_id,harga,m_type')
                //->edit_column('item_id', '$1', $this->range_tiket_model->get_tiket_by_id('item_id'))
                ->edit_column('promosi_id', '$1', 'view::btn_group_edit_delete(promosi_id)');
            $data = $table->getData();
            foreach ($data as $key => $value) {
                $temp = $this->range_tiket_model->get_tiket_by_id($value['item_id']);
                    
                /*$docs = '';
                foreach ($temp as $tmp) {
                    $docs.= ' <span class="badge badge-success badge-roundless">'.$tmp['pr_code'].'</span> ';
                }*/
                $data[$key]['item_id'] = $temp;
            }
            $table->setData($data);

            echo $table->generate();
            //echo $table->generate();
        }

    }

    public function add($id='',$status_delete = 0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->config->load('twig');
            $this->load->model(array('item_model','range_tiket_model','store_model'));
            $title = 'Tambah Data Promosi';
            $data['form_action'] = $this->class_name . '/proses';

            $data['id'] = $id;
            if($id){
                $title = 'Edit promosi';
                $row            = $this->promosi_model->getById($id)->row();
                $data['data']  = $row;
            }

            $data['title']      = $title;
            $data['assets_url'] = $this->config->item('assets_url');
            $data['sidebar']    = $this->access_right->menu();
            $data['form']       = $this->class_name.'/form_promosi.tpl';

            $button_group   = array();
            $button_group[] = view::button_back();
            
            if($status_delete == 0){
                $data['form_action']    = view::form_input($id);
                $button_group[]         = view::button_save();
            }else{
                $data['title']          = 'Delete promosi';
                $data['form_action']    = view::form_delete($id);;
                $data['readonly']       = 'readonly=""';
                $button_group[]         = view::button_delete_confirm();
            }

            $data['button_group'] = view::render_button_group($button_group);

            $is_head_office = hprotection::isHeadOffice();
            if($is_head_office){
                $data['is_head_office'] = TRUE;
                $data['list_store']     = form_dropdown('data[store_id]', $this->store_model->options_empty(), $row->store_id,'class="form-control bs-select"' );
            }else{
                $data['is_head_office'] = FALSE;
                $data['list_store']     = form_hidden('data[store_id]',$this->session->userdata('store_id') );
            }
            if(empty($row->date_begin)){
                $data['data']['date_begin'] = date('Y-m-d');
                $data['data']['date_end'] = date('Y-m-d');
            }
            /*target="#list-item" data-source="'.site_url($this->class_name.'/getlist/item').'" 
                                    onchange="refreshList(this)" class="bs-select form-control"*/
            $htmlOptions        = 'target="#reg_tiket" data-source="'.site_url($this->class_name.'/getTiket').'" 
                                    onchange="refreshList(this)"  id="list-item" placeholder="Select Item" class="select2 form-control" data-live-search="true" target-type="text"';

            $data['list_item']  = form_dropdown('data[item_id]', $this->item_model->options_empty(2), $row->item_id, $htmlOptions);
            
            $this->twig->display('base/page_form.tpl', $data);

        } else {
            $this->access_right->redirect();
        }
    }

    public function getTiket($item_id=null) {
        if(!empty($item_id)){
            $this->load->model(array('range_tiket_model','item_model'));
            $dataItem = $this->item_model->getById($item_id)->row();
            $data = $this->range_tiket_model->get_tiket($dataItem->harga,$dataItem->m_type);
            echo $data;
        }
        
    }

    

    public function edit($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('edit', true);
        $this->add($id);
    }

    public function delete($id_enc = '') {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('delete', true);
        $this->add($id,$status_delete = 1);
    }

    public function proses() {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('data[store_id]', 'Outlet', 'required|trim');
            $this->form_validation->set_rules('data[date_begin]', 'Date Begin', 'required|trim');
             $this->form_validation->set_rules('data[date_end]', 'Date End', 'required|trim');
            $this->form_validation->set_rules('data[item_id]', 'Item', 'required|trim');
            $this->form_validation->set_rules('data[qty_tiket]', 'Qty Tiket', 'required|trim');

            if ($this->form_validation->run()) {
                $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');

                $id = $this->input->post('id');
                $data = $this->input->post('data');
                $data['qty_tiket'] = hgenerator::switch_number($data['qty_tiket']);
                $this->db->trans_start();
                if ($id){
                    $this->promosi_model->update($data, $id);
                }else{
                    $this->promosi_model->create($data);
                    
                }
                
                if($this->db->trans_status()){
                    $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh_filter()');
                }else{
                    $message = array(false, 'Terjadi Kesalahan',$this->db->_error_message(), '');
                }
                $this->db->trans_complete();
            } else {
                $message = array(false, 'Terjadi Kesalahan', validation_errors());
            }
            echo json_encode($message);
        } else {
            $this->access_right->redirect();
        }
    }

    public function proses_delete() {
        $id=$this->input->post("id");
		$this->access_right->otoritas('delete', true);
        if ($this->access_right->otoritas('delete', true)) {
            $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');
            if ($this->promosi_model->delete($id)) {
                $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', 'refresh_filter()');
				/* INSERT LOG */
				$this->access_right->activity_logs('delete','Delete Merchandise');
				/* END OF INSERT LOG */
            }
            echo json_encode($message);
        }
    }






}

/* End of file bbm.php */
/* Location: ./application/controllers/bbm.php */

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class receiving_ss extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array(
        	'receiving_model','receiving_postore_model',
        	'receiving_postore_koli_model', 'koli_model',
        	'transaction_model'
        ));
        $this->button['add'] = view::button_add(array('onclick'=>"btnLoadNextPage(this)"));
    }

    public function index() {

        $data['title']       = 'SERVICE CENTER RECEIVING';
        $data['sidebar']   = $this->access_right->menu();
        $data['content']    = 'base/page_content.tpl';    
        $data['table']['main'] = $this->getForm('page-index', $id); 
        $this->twig->display('index.tpl', $data);
    }


    public function __index() {

        $data['title'] 		= 'SERVICE CENTER RECEIVING';
        $data['sidebar'] 	= $this->access_right->menu();
        $data['content'] 	= 'base/page_content.tpl';     
        $data['button_group'] 	= $this->getAvailableButtons();
        $data['table']['main'] 	= $this->table_main(array('wrapper'));
        //
        if(empty($id)){
            $param['page']['create_koli']   = $this->getForm('table-postore', $id, array('wrapper','filter'));
        }
        $param['page']['list_koli']     = $this->getForm('table-main-ss', $id, array('wrapper','filter'));
        return $this->twig->render('receiving/ss/page_koli.tpl', $param);
        break;
        //
        $data['javascript'] = array('receiving.js');
        
        $this->twig->display('index.tpl', $data);
    }

    public function table_main($option = array()){
        $this->load->library('Datatable');
        $table = $this->datatable;

        // if (in_array('filter', $option)) {
        //     $table->dataFilter = array(
        //                         array('nama_cabang', 'Store Name', 'text'),
        //                         array('nama_kota', 'City', 'text'),
        //                         array('telp', 'Phone', 'numeric'),
        //                         array('date_start', 'Date Start', 'range-date'),
        //                         array('store_area', 'Area', 'range-numeric')
        //                     );
        // }

        if (in_array('wrapper', $option)) {
            $table->numbering  = true;
            $table->id         = 'table-receiving';
            $table->isScrollable= false;
            $table->header     = array('SERVICE CENTER', 'USER', 'DATE', 'ACTION');
            $table->source     = site_url($this->class_name.'/table_main');
            return $table->generateWrapper();
        } else {
        	$table
	            ->setModel($this->receiving_model)
	            ->setNumbering()
	            ->with(array('store','employee'))
	            ->where('type', storetype::SC)
	            ->select('nama_cabang, nama_pegawai , date_created, receiving_id')
	            ->edit_column('date_created', '$1', 'date::longDate(date_created)')
	            ->edit_column('receiving_id', '$1', 'view::btn_group_receiving_warehouse(receiving_id)');
            echo $table->generate();
        }

    }

    public function add() {
    	$this->db->trans_start();
    	$data['type'] = storetype::SC;
    	$this->receiving_model->create($data);
    	$id = $this->receiving_model->insert_id;

    	if ($this->db->trans_status()){
    		
    	} else {
    		
    	}

    	$this->db->trans_complete();

    	$this->detail($id);
    }

    public function detail($id = '') {
    	echo $this->getForm('page-detail', $id);
    }

    public function add_koli($id = 0){
    	$ids = $this->input->post('ids');
    	echo $this->getForm('form-koli', $id, array('ids'=>$ids));
    }

    public function getForm($type = '', $id = NULL, $param = array()){
    	$this->load->library(array('custom_form','custom_table'));
    	$this->load->model(array('sequence_model','po_store_model'));
    	

    	switch ($type) {
            case 'page-index':
                if(empty($id)){
                    $param['page']['create_koli']   = $this->getForm('table-postore', $id, array('wrapper','filter'));
                }
                $param['page']['list_koli']     = $this->getForm('table-main-ss', $id, array('wrapper','filter'));
                return $this->twig->render('receiving/ss/page_koli.tpl', $param);
                break;

            case 'table-postore':
                $this->load->library('Datatable');
                $table = $this->datatable;
/*                if (in_array('filter', $param)) {
                    $table->dataFilter = array(
                                        array('po.po_code', 'PO Code', 'text'),
                                        array('nama_supplier','Supplier Name','text'),
                                        array('nama_cabang','Store Name','text'),
                                        array('item_name','Item','text'),                                        
                                    );
                }*/

                if (in_array('wrapper', $param)) {
                    $table->id          = 'table-receiving-detail';
                    $table->isScrollable= false;
                    $table->checklist   = true;
                    $table->pagination  = true;
                    $table->header      = array('PO', 'SUPPLIER', 'STORE NAME','JUMLAH','ITEM', 'TYPE','STATUS');
                    $table->source      = site_url($this->class_name.'/getform/table-postore/'.$id);
                    $table->caption_btn_group   = 'Receive';
                    $table->url_action_group    = site_url($this->class_name.'/add_koli');
                    $table->callback_group_submit = '
                        var param = {
                            ids : grid.getSelectedRows()
                        };
                        console.log(param);
                        var url = "'.site_url($this->class_name.'/add_koli/'.$id).'";
                        loadModal(url, param);

                    ';
                    return $table->generateWrapper();
                } else {
                    $this->load->model('po_store_model');
                    $table
                        ->setModel($this->po_store_model)
                        ->with(array('store','po', 'supplier', 'postore_koli','detail_po','item'))
                        ->where('postore_status', postatus::WAITING_RECEIVE)
                        ->where('koli_id is NULL')
                        ->where('qty_for_po > 0')
                        ->where('(this.po_type = '.itemtype::ITEM_SPAREPART.' OR this.po_type = '.itemtype::ITEM_SPAREPART .')', NULL)
                        //->or_where('this.po_type', itemtype::ITEM_MESIN)
                        ->select('this.postore_id, po.po_code, nama_supplier, nama_cabang, qty_for_po,item_name , this.po_type, postore_status as status')
                        ->order_by('this.date_created','DESC')
                        ->edit_column('status', '$1', 'transaksi::getPoStatus(status)')
                        ->edit_column('po_type', '$1', 'transaksi::getItemType(po_type)')
                        ->edit_column('postore_id', '<input type="checkbox" name="id[]" value="$1">','postore_id');
                    echo $table->generate();
                }
                break;

            case 'table-main-ss':
                $this->load->library('Datatable');
                $table = new Datatable();
                 if (in_array('filter', $param)) {
                    $table->dataFilter = array(
                                        array('koli_code', 'Receiving Code', 'text'),                                       
                                    );
                }

                if (in_array('wrapper', $param)) {
                    $this->load->model('itemtype_model');
                    $data = $this->itemtype_model->options_filter();
                    $table->id          = 'table-stok-ss';
                    $table->isScrollable= false;
                    $table->numbering   = true;
                    $table->header      = array('RECEIVING CODE','STORE', 'NOTE','ITEM','QTY','SUPPLIER', 'STATUS');
                    $table->source      = site_url($this->class_name.'/getform/table-main-ss/');
                    return $table->generateWrapper();
                } else {
                    $table
                        ->setModel($this->koli_model)
                        ->setNumbering()
                        ->with(array('store','supplier','postore_koli','po_detail','item'))
                        ->select('koli_code, nama_cabang, note,item_name,dc_qty_received,nama_supplier,koli_status')
                        ->order_by('koli_code')
                        ->where_in('itemtype_id',hprotection::getScItem())
                        ->edit_column('koli_status','$1','prstatus::getStatus(koli_status)');

                    echo $table->generate();
                }
                break;

            

    		case 'page-detail':
    			$param['title'] 	= 'RECEIVING DETAIL';
    			$param['portlet']	= true;
    			$param['button']['back'] = true;
                $param['button']['portlet'] = view::render_button_group_portlet(array(
                            //view::button_save_form(),
                            view::button_receiving($id)
                    ));
    			$param['form']['info'] 	= $this->getForm('form-info', $id);
    			$param['table']['main']	= $this->getForm('page-koli', $id);
    			return $this->twig->render('base/page_content.tpl', $param);
    			break;

    		case 'form-info' :
    			$param['data']	 = $this->receiving_model->with('store')->getById($id)->row();
    			return $this->twig->render('receiving/warehouse/form_info.tpl', $param);
    			break;

    		case 'page-koli':
				$param['page']['create_koli']	= $this->getForm('table-postore', $id, array('wrapper','filter'));
				$param['page']['list_koli'] 	= $this->getForm('table-koli', $id, array('wrapper'));
    			return $this->twig->render('receiving/warehouse/page_koli.tpl', $param);
    			break;

            case 'form-koli' :
                $this->load->model('store_model');
                if($id){

                }else{
                    
                }

                $poStores = $this->po_store_model->with(array('po','store'))->where_in('postore_id',$param['ids'])->get()->result();
                $postore_id = $param['ids'][0];             
                $postore = $poStores[0];

                $param['title']         = 'Receiving Sparepart';
                $param['form_action']   = form_open(
                                            site_url($this->class_name.'/proses_koli'),
                                            '', array(
                                                'id' => $id,
                                                'postore_ids'=>json_encode($param['ids'])
                                            )
                                        );
                $option = array($postore->store_id => $postore->nama_cabang);
                $param['list']['store'] = form_dropdown('data[store_id]', $option, $postore->store_id, 'class="bs-select" disabled');
                $param['postores']          = $poStores;
                $param['data']['koli_code'] = code::getCode('koli',$postore_id);
                $param['data']['date_created'] = date('Y-m-d');

                $receiving = $this->po_store_model->with(array('detail_po','item','po'))->where_in('this.postore_id',$param['ids'])->get()->result();
                $param['table'] = $receiving;
                $form = $this->custom_form;
                $form->portlet          = true;
                $form->button_back      = true;
                $form->id               = 'form-koli';
                $form->param            = $param;
                $form->url_form         = 'receiving/ss/form_koli.tpl';
                return $form->generate();
                break;
    		default:
    			# code...
    			break;
    	}
    }

    public function proses_koli(){
        $this->load->model(array('po_store_model','receiving_warehouse_model', 'koli_model','receiving_postore_model','receiving_postore_koli_model','purchase_order_model','purchase_order_detail_model','item_stok_model'));
        $modelKoli = $this->koli_model;
       


        $modelRePostore = $this->receiving_postore_model;
        $modelRePostoreKoli = $this->receiving_postore_koli_model;
        $modelPostore = $this->po_store_model;

        $postore_ids = json_decode($this->input->post('postore_ids'),TRUE);
        $receiving_id= $this->input->post('id');
        $dataKoli = $this->input->post('data');

        $postore = $modelPostore->getById($postore_ids[0])->row();

        $dataInsertKoli['koli_code'] = code::getCodeAndRaise('koli', $postore_ids[0]);
        $dataInsertKoli['store_id']  = $postore->store_id;
        $dataInsertKoli['supplier_id'] = $postore->supplier_id;
        $dataInsertKoli['qty'] = $dataKoli['qty'];
        $dataInsertKoli['note'] = $dataKoli['note'];
        $dcQty = $this->input->post('dc_qty');
        $dcItem = $this->input->post('dc_item');
        

        $this->db->trans_start();
        // [POINT] create koli
        $modelKoli->create($dataInsertKoli);
        $koliId = $modelKoli->insert_id;

        foreach ($dcQty as $key => $value) {
            $modelItemStok = $this->item_stok_model;
            $this->purchase_order_detail_model
            ->set('dc_qty_received',$value)
            ->set('dc_date_received',$dataKoli['date_created'])
            ->where('podetail_id',$key)
            ->update();

            //=========== update receiving 21/02/2016===================
            $whereCek = array('store_id'=>hprotection::getSC(),'item_id'=>$koliId[$key]);
            $cekEksisStok = $modelItemStok->select('itemstok_id')->where($whereCek)->get()->num_rows();
            if($cekEksisStok>0){
                $modelItemStok
                    ->set("stok_gudang","stok_gudang+$value",FALSE)
                    ->set("stok_total","stok_total+$value",FALSE)
                    ->where($whereCek)->update();
            }else{
                $this->db
                    ->set("stok_gudang","stok_gudang+$value",FALSE)
                    ->set("stok_total","stok_total+$value",FALSE)
                    ->set($whereCek)->insert('m_item_stok');
            }
            //-----------end update----------------------
        }

        // [POINT] create receiving_postore dan [POINT] receiving_postore_koli
        $dataRePostore = array();
        $dataRePostoreKoli = array();
        
        foreach ($postore_ids as $key => $value) {
            $dataRePostore[] = array(
                    'receiving_id' => $receiving_id,
                    'postore_id' => $value,
                );

            $dataRePostoreKoli[] = array(
                    'receiving_id' => $receiving_id,
                    'koli_id' => $koliId,
                    'postore_id' => $value,
                );
            $postore_ids[] = $value;
        }

        $modelRePostore->insert_batch($dataRePostore);
        $modelRePostoreKoli->insert_batch($dataRePostoreKoli);
        //-----------------29092015-------------------------
        $this->purchase_order_model
        ->with('detail','repostore_koli')
        ->where_in('postore_id', $postore_ids)->set('po_status', postatus::DC_TRANSIT)->update();
        $this->po_store_model->update_postore(array('postore_status'=>postatus::DC_TRANSIT),$postore_ids);
        
        
        //==============================================

        //---------------- new 16092015---------------------
        $data['type'] = storetype::SC;
        $data['receiving_status'] = transactionstatus::SC;
        $data['date_created'] = $dataKoli['date_created'];
        $this->receiving_warehouse_model->create($data);
        $id = $this->receiving_warehouse_model->insert_id;

         //update zn_receiving_postore_koli

        $this->db->set('receiving_id',$id)
            ->where('receiving_id',0)
            ->update('zn_receiving_postore_koli');

        // [POINT] update status di koli. postore. podetail.prdetail
        $koliIds = $this->koli_model->select('this.koli_id')->with('postore_koli')->where('receiving_id', $id)->get()->result_array();
        $ids = array_values_index($koliIds, 'koli_id');
        $this->koli_model
            ->where_in('koli_id', $ids)
            ->set('koli_status',transactionstatus::SC)
            ->update();

        //=================end neew====================
        
        if($this->db->trans_status()){
            $message = array('success'=>true, 'message'=>'Save Success');
        }else{
            $message = array('success'=>false, 'message'=>$this->db->_error_message());
        }
        $this->db->trans_complete();
        echo json_encode($message);
    }

    public function __proses_koli(){
    	$this->load->model(array('po_store_model', 'koli_model','receiving_postore_model','receiving_postore_koli_model'));
    	$modelKoli = $this->koli_model;


    	$modelRePostore = $this->receiving_postore_model;
    	$modelRePostoreKoli = $this->receiving_postore_koli_model;
    	$modelPostore = $this->po_store_model;

    	$postore_ids = json_decode($this->input->post('postore_ids'),TRUE);
    	$receiving_id= $this->input->post('id');
    	$dataKoli = $this->input->post('data');

    	$postore = $modelPostore->getById($postore_ids[0])->row();

    	$dataKoli['koli_code'] = code::getCodeAndRaise('koli', $postore_ids[0]);
    	$dataKoli['store_id']  = $postore->store_id;
    	$dataKoli['supplier_id'] = $postore->supplier_id;

    	// [POINT] create koli
    	$modelKoli->create($dataKoli);
    	$koliId = $modelKoli->insert_id;

    	// [POINT] create receiving_postore dan [POINT] receiving_postore_koli
    	$dataRePostore = array();
    	$dataRePostoreKoli = array();
    	$this->db->trans_start();
    	foreach ($postore_ids as $key => $value) {
    		$dataRePostore[] = array(
    				'receiving_id' => $receiving_id,
    				'postore_id' => $value,
    			);

    		$dataRePostoreKoli[] = array(
    				'receiving_id' => $receiving_id,
    				'koli_id' => $koliId,
    				'postore_id' => $value,
    			);
    	}

    	$modelRePostore->insert_batch($dataRePostore);
    	$modelRePostoreKoli->insert_batch($dataRePostoreKoli);
    	
    	if($this->db->trans_status()){
            $message = array('success'=>true, 'message'=>'Save Success');
        }else{
            $message = array('success'=>false, 'message'=>$this->db->_error_message());
        }
        $this->db->trans_complete();
        echo json_encode($message);
    }

    public function proses_receiving(){
        $this->db->trans_start();
        //$data['type'] = storetype::WAREHOUSE;
        $data['receiving_status'] = transactionstatus::SC;
        $this->receiving_model->create($data);
        $id = $this->receiving_model->insert_id;

        //update zn_receiving_postore_koli

        $this->db->set('receiving_id',$id)
            ->where('receiving_id',0)
            ->update('zn_receiving_postore_koli');

        // [POINT] update table receiving set status
        
        //$this->receiving_warehouse_model->update($data, $id_receiving);

        // [POINT] update status di koli. postore. podetail.prdetail
        $koliIds = $this->koli_model->select('this.koli_id')->with('postore_koli')->where('receiving_id', $id)->get()->result_array();
        $ids = array_values_index($koliIds, 'koli_id');
        $this->koli_model
            ->where_in('koli_id', $ids)
            ->set('koli_status',transactionstatus::WAREHOUSE)
            ->update();

        if($this->db->trans_status()){
            $message = array(
                        'success' => true,     
                        'message' => 'Save Success',
                        'callback'=> "refreshDataTable(true)",        
                    );
        }else{
            $message = array(
                        'success' => false,                        
                        'message' => 'Database Error Occured',
                    );
        }
        $this->db->trans_complete();
        echo json_encode($message);

    }


    public function barcode($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('edit', true);
        $this->showbarcode($id);
    }


    public function showbarcode($id='',$status_delete = 0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->load->model(array('hadiah_type_model','koli_model'));
            $this->config->load('twig');
            
            $title = 'Tambah Data';
            $data['form_action'] = $this->class_name . '/proses';

            $data['id'] = $id;
            if($id){
                $title = 'Koli Barcode';
                $row            = $this->koli_model
                                    ->with(array('store','postore_koli', 'po_detail'))
                                    ->where('this.koli_id', $id)
                                    ->get()
                                    ->row();
                $data['data']  = $row;
            }

            $data['title']      = $title;
            $data['assets_url'] = $this->config->item('assets_url');
            $data['sidebar']    = $this->access_right->menu();
            $data['form']       = 'receiving/warehouse/form_barcode.tpl';

            $button_group   = array();
            $button_group[] = view::button_back();
            
            if($status_delete == 0){
                $data['form_action']    = view::form_input($id);
                //INI BUTTON UNTUK PRINT BARCODE KOLI
                $button_group[]         = view::button_print_koli();
            }else{
                $data['title']          = 'Delete Sparepart';
                $data['form_action']    = view::form_delete($id);;
                $data['readonly']       = 'readonly=""';
                $button_group[]         = view::button_delete_confirm();
            }

            $data['button_group'] = view::render_button_group($button_group);
            $data['options_hadiah'] = form_dropdown('data[hadiahtype_id]', $this->hadiah_type_model->options(), $row->hadiahtype_id, 'class="bs-select" disabled="disabled"');
            
            //$options_pulau = $this->pulau_model->options();
            //$data['options_pulau'] = form_dropdown('id_pulau',$options_pulau, !empty($def_id_pulau) ? $def_id_pulau : '', 'class = "form-control"'); 
            
            $this->twig->display('base/page_form.tpl', $data);

        } else {
            $this->access_right->redirect();
        }
    }


    public function __proses_receiving($id){
        $this->db->trans_start();

        // [POINT] update table receiving set status
        $data['receiving_status'] = transactionstatus::SC;
        $this->receiving_model->update($data, $id);

        // [POINT] update status di koli. postore. podetail.prdetail
        $koliIds = $this->koli_model->select('this.koli_id')->with('postore_koli')->where('receiving_id', $id)->get()->result_array();
        $ids = array_values_index($koliIds, 'koli_id');
        $this->koli_model
            ->where_in('koli_id', $ids)
            ->set('koli_status',transactionstatus::SC)
            ->update();

        if($this->db->trans_status()){
            $message = array(
                        'success' => true,     
                        'message' => 'Save Success',
                        'callback'=> "refreshDataTable(true)",        
                    );
        }else{
            $message = array(
                        'success' => false,                        
                        'message' => 'Database Error Occured',
                    );
        }
        $this->db->trans_complete();
        echo json_encode($message);

    }


}


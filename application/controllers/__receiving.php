<?php


class receiving extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array('po_store_model', 'po_store_detail_model'));
    }

    public function test(){
        
        $date2 = Carbon::now();
        $date = Carbon::parse($past);
        
        echo $date->diffForHumans($date2);
    }

    public function index() {
        $this->load->library('Datatablemodel');

        $data['title'] = 'RECEIVING';
        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'base/page_content.tpl';     

        $table = $this->datatablemodel;
        $table->id = 'table-main';
        $table->numbering = true;
        $table->header    = array('STORE', 'SUPPLIER', 'DATE CREATED', 'TYPE', 'STATUS', 'INTERVAL', 'ACTION');
        $table->source = site_url($this->class_name.'/table_main');
        $data['table']['main'] = $table->generate_table();
        $data['javascript'] = array('receiving.js');
        
        $this->twig->display('index.tpl', $data);
    }

    public function table_main(){
        // MENGISI NILAI EXPIRED UNTUK STATUS PO DRAFT (0)
        $this->load->model(array('status_po_model', 'po_store_detail_model'));
        $status_po = $this->status_po_model
                        ->with(array('expired'))
                        ->where('statuspo_id', 0)
                        ->get()->result_array();
        $config = array();
        foreach ($status_po as $key => $value) {
            $config[$value['itemtype_id']] = $value['expired'];
        }
        $this->config->set_item('status_po_expired', $config);

        $this->load->library('Datatablemodel');
        $this->datatablemodel
                ->setModel($this->po_store_detail_model)
                ->setNumbering()
                ->select("nama_cabang, nama_supplier, date_created, po_type, postore_status, date_created as now, this.postore_id")
                ->with(array('postore', 'store','supplier'))
                ->group_by('this.postore_id')
                // ->edit_column('view', '$1', "view::button_view_po(view)")
                ->edit_column('postore_status', '$1', "transaksi::getPoStatus(postore_status)")
                ->edit_column('now', '$1', "view::statusPoExpired(now, po_type)")
                ->edit_column('po_type', '$1', "transaksi::getItemType(po_type)")
                ->edit_column('postore_id', '$1', "view::btn_datatable_detail(postore_id)");


        $data = $this->datatablemodel->generate();
        echo $data;
    }

    public function detail($postore_id = NULL){
        $this->load->library('custom_form');
        $this->load->model(array('purchase_request_model','po_store_model'));

        // FORM INFO
        $postore               = $this->po_store_model
                                        ->with(array('store','supplier'))
                                        ->getById($postore_id)->row();


        // $param['form_action'] = form_open(
        //                                 site_url($this->class_name.'/proses_data/'.$param['store_id']),
        //                                 ''
        //                             );
        // $form = new custom_form();
        // $form->param = $param;
        // $form->url_form = $this->class_name.'/form_info.tpl';

        // BUTTON GROUP 2
        if($postore->postore_status==0){
            $btngroup2[] = view::button_receive_request(
                                    $postore_id, 
                                    array(
                                        'url-action'=> site_url($this->class_name.'/proses_receiving/'.$postore_id),                                        
                                        )
                                    );
            $data['button_group_2'] =view::render_button_group($btngroup2);
        }

        $data['data'] = $postore;
        $data['title'] = 'RECEIVING';
        $data['subtitle']= transaksi::getItemType($pr->pr_type);
        $data['data_source_page_1'] = site_url($this->class_name . '/detail/'.$postore_id);        
        $data['readonly'] = "readonly";
        $data['isBtnPageBack'] = true;
        $data['form_info'] = $this->class_name.'/form_info.tpl';
        $data['table']['main']  = $this->table_detail($postore_id);

        if($this->input->is_ajax_request()){
            $this->twig->display('base/page_content.tpl',$data);
        }else{
            $data['sidebar'] = $this->access_right->menu();
            $data['content'] = 'base/page_content.tpl'; 
            $this->twig->display('index.tpl', $data);    
        }

    }

    public function proses_receiving($postore_id){
        

        if(!$postore_id) return;

        $this->load->model(array('purchase_request_detail_model','po_store_model','item_in_model','item_stok_model'));
        $prdetailModel = $this->purchase_request_detail_model;
            $iteminModel = $this->item_in_model;
            $itemStokModel = $this->item_stok_model;

        $this->db->trans_start();
        /*
            data = array of (prdetail_id, qty_received)
        */
        $postore = $this->po_store_model->getById($postore_id)->row_array();
        
        if(!$postore) return;

        $data = $this->input->post('data');
        if(!$data) return;

        foreach ($data as $value) {

            $prdetail = $prdetailModel->getById($value['prdetail_id'])->row_array();

            // UPDATE PURCHASE_REQUEST_DETAIL
            $prdetail_data = array();
            $prdetail_data['qty_received']   = $value['qty_received'];
            $prdetail_data['qty_temp']       = $value['qty_received'];
            $prdetail_data['detail_status']  = prstatus::RECEIVE;
            $prdetailModel->update($prdetail_data, $value['prdetail_id']);

            // INSERT TO ITEM_IN
            $itemin = array();
            $itemin['store_id'] = $postore['store_id'];
            $itemin['item_id']  = $prdetail['item_id'];
            $itemin['price']    = $prdetail['price'];
            $itemin['qty']      = $value['qty_received'];
            $itemin['postore_id'] = $postore_id;
            $this->item_in_model->create($itemin);

            // UPDATE TO ITEM_STOK : cek item_stok ada atau tidak
            $itemstok = $itemStokModel
                        ->where('store_id', $postore['store_id'])
                        ->where('item_id', $prdetail['item_id'])
                        ->get()->row();
            if($itemstok) {
                // menghitung harga item rata-rata. update harga rata-rata per item per store
                $rata = 0;
                if($value['qty_received'] > 0)
                $rata = (($itemstok->stok_store *  $itemstok->price_avg) +
                        ($prdetail['price'] * $value['qty_received'])) / 
                        ($itemstok->stok_store + $value['qty_received']);

                $itemStokModel->set('stok_total','stok_total+'.$value['qty_received'], FALSE);
                $itemStokModel->set('stok_gudang','stok_gudang+'.$value['qty_received'], FALSE);
                $itemStokModel->set('stok_store','stok_store+'.$value['qty_received'], FALSE);
                $itemStokModel->set('price_avg', $rata);
                $itemStokModel->where('store_id', $postore['store_id']);
                $itemStokModel->where('item_id', $prdetail['item_id']);
                $itemStokModel->update();
            }else{
                $itemStok_data['stok_total'] = $value['qty_received'];
                $itemStok_data['stok_gudang'] = $value['qty_received'];
                $itemStok_data['stok_store'] = $value['qty_received'];
                $itemStok_data['store_id'] = $postore['store_id'];
                $itemStok_data['item_id'] = $prdetail['item_id'];
                $itemStok_data['price_avg'] = $prdetail['price'];
                $itemStokModel->create($itemStok_data);
            }

        }

        // UPDATE PO_STORE
        $postore_data['postore_status'] = 1; //CLOSED
        $postore_data['date_received']  = date::now_sql();
        $this->po_store_model->update($postore_data, $postore_id);

        if($this->db->trans_status()){
            $message = array(
                        'success' => true,     
                        'message' => 'Save Success',
                        'callback'=> "reloadPageLoading()",        
                    );
        }else{
            $message = array(
                        'success' => false,                        
                        'message' => 'Database Error Occured',
                    );
        }

        
        $this->db->trans_complete();

        echo json_encode($message);

    }

    public function table_detail($postore_id = 0, $page = 0){
        $postore = $this->po_store_model->getById($postore_id)->row_array();

        $model = $this->po_store_detail_model;
        $rows = $model->with(array('prdetail','item'))
                ->where('this.postore_id',$postore_id)
                ->get()
                ->result_array();
        $data = array();
        foreach ($rows as $key => $row) {
            $temp = array();
            $temp['item_id'] = $row['item_key'];
            $temp['item_name']= $row['item_name'];
            $temp['price'] = $row['price'];
            $temp['qty_approved']= $row['qty_approved'];
            if($postore['postore_status']==0)
                $temp['qty_received']= '<input type="text" value="'.$row['qty_received'].'" prdetail-id="'.$row['prdetail_id'].'" postoredetail-id="'.$row['postoredetail_id'].'" class="qty-received form-control" size=3>';
            else
                $temp['qty_received']= $row['qty_received'];
            $data[] = $temp;
        }

        $this->load->library ("custom_table" );
        $table = new custom_table();
        $header [0] = array (
                "SKU",1,1,
                "ITEM NAME",1,1,
                "PRICE",1,1,
                "APPROVED",1,1,
                "RECEIVED",1,1,
        );
        $table->header = $header;
        $table->data = $data;
        return $table->generate();
    }

    public function proses_detail(){

        $this->load->model(array('transaction_document_model'));
        $data = $this->input->post('data');

        $this->db->trans_start();
        // update document number di purchase order
        foreach ($data as $key => $value) {
            $po = $this->purchase_order_model->getById($value['po_id'])->row();
            $this->transaction_document_model
                ->where('transaction_id', $po->transaction_id)
                ->update(array('doc_number' => $value['doc_number']));
        }

        if($this->db->trans_status()){
            $message = array(
                        'success' => true,     
                        'message' => 'Save Success',
                        'callback'=> "reloadPageLoading()",        
                    );
        }else{
            $message = array(
                        'success' => false,                        
                        'message' => 'Database Error Occured',
                    );
        }

        $this->db->trans_complete();

        echo json_encode($message);
    }


}



<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class web_supplier extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array(
        	'receiving_warehouse_model','receiving_postore_model',
        	'receiving_postore_koli_model', 'koli_model',
        	'transaction_model', 'hadiah_type_model', 'hadiah_model'
        ));
        //$this->button['add'] = view::button_add(array('onclick'=>"btnLoadNextPage(this)"));
    }

    public function index() {
        $data['title']       = 'Supplier Page';
        $data['sidebar']   = $this->access_right->menu();
        $data['content']    = 'base/page_content.tpl';    
        //$data['form']['info']  = $this->getForm('form-info', $id);
        $data['table']['main'] = $this->getForm('page-index', $id); 
        $data['button_right'] = $this->getPrintTools();
       
        $this->twig->display('index.tpl', $data);
    }


    public function getForm($type = '', $id = NULL, $param = array(),$st_print = FALSE){
    	$this->load->library(array('custom_form','custom_table'));
    	$this->load->model(array('sequence_model','po_store_model','store_model'));
    	$form = $this->custom_form;

    	switch ($type) {
            case 'page-index':
                if(empty($id)){
                    $param['page']['create_koli']   = $this->getForm('table-postore', $id, array('wrapper'));
                }
                $param['page']['list_koli']     = $this->getForm('table-main-warehouse', $id, array('wrapper'));
                return $this->twig->render('receiving/warehouse/page_koli.tpl', $param);
                break;

    		case 'page-detail':
    			$param['title'] 	= 'LIST PO';
    			$param['portlet']	= true;
    			$param['button']['back'] = true;
                /*if(empty($id)){
                    $param['button']['portlet'] = view::render_button_group_portlet(array(
                                //view::button_save_form(),
                                view::button_receiving($id)
                        ));
                }*/
    			$param['form']['info'] 	= $this->getForm('form-info', $id);
    			$param['table']['main']	= $this->getForm('page-koli', $id);
    			return $this->twig->render('base/page_content.tpl', $param);
    			break;

    		case 'form-info' :
    			//$param['data']	 = $this->receiving_warehouse_model->with('store')->getById($id)->row();
                $param['data']   = $this->store_model->where('id_cabang',$this->session->userdata('store_id'))->get()->row();
    			return $this->twig->render('receiving/warehouse/form_info.tpl', $param);
    			break;

    		

    		case 'table-postore':
    			$this->load->library('Datatable');
		        $table = $this->datatable;

		        if (in_array('wrapper', $param)) {
		            $table->id         	= 'table-receiving-detail';
                    $table->numbering  = true;
                    $table->isScrollable= false;
		            $table->header     	= array('PO', 'SUPPLIER', 'STORE NAME','JUMLAH','ITEM', 'TYPE','STATUS');
		            $table->source     	= site_url($this->class_name.'/getform/table-postore/'.$id);
		            return $table->generateWrapper();
		        } else {
		        	$this->load->model('po_store_model');
		        	$table
			            ->setModel($this->po_store_model)
			            ->with(array('store','po', 'supplier', 'postore_koli','detail_po','item'))
			           // ->where('this.po_type', itemtype::ITEM_MERCHANDISE)
                        ->where('supplier.id_supplier',$this->session->userdata('supplier_id'))
                        ->where('postore_status', postatus::WAITING_RECEIVE)
			            ->where('koli_id is NULL')
			            ->select('this.postore_id, po.po_code, nama_supplier, nama_cabang, qty_approved,item_name , this.po_type, postore_status as status')
                        ->edit_column('status', '$1', 'transaksi::getPoStatus(status)')
			            ->edit_column('po_type', '$1', 'transaksi::getItemType(po_type)')
			            ->edit_column('postore_id', '<input type="checkbox" name="id[]" value="$1">','postore_id');

		            echo $table->generate();
		        }
    			break;

            case 'table-main-warehouse':
                $this->load->library('Datatable');
                $table = new Datatable();

                if (in_array('wrapper', $param)) {
                    $this->load->model('itemtype_model');
                    $data = $this->itemtype_model->options_filter();
                    /*$innerFilter = array(
                        array('nama_cabang', 'Store', 'text'),
                        array('koli_code', 'Koli Code', 'text'),
                        array('note','Note','text'),
                        array('nama_supplier','Supplier','text')
                    );
                    $table->dataFilter = $innerFilter;*/

                    $table->id          = 'table-stok-warehouse';
                    $table->isScrollable= false;
                    $table->numbering   = true;
                    $table->header      = array('KOLI CODE','PO','STORE', 'QTY KOLI', 'NOTE','ITEM','QTY','STORE RECEIVE','SUPPLIER', 'STATUS');
                    $table->source      = site_url($this->class_name.'/getform/table-main-warehouse/');
                    return $table->generateWrapper();
                } else {
                    $table
                        ->setModel($this->koli_model)
                        ->setNumbering()
                        ->with(array('store','supplier','postore_koli','po_detail','item','postore','po','pr_status'))
                        ->select('koli_code,po_code, nama_cabang, qty, note,item_name,dc_qty_received,qty_received,nama_supplier,pr_status')
                        ->where('supplier.id_supplier',$this->session->userdata('supplier_id'))
                   //     ->where_in('itemtype_id',hprotection::getDcItem())
                        /*->order_by('itemtype_id')
                        ->order_by('item_name')*/
                        //->edit_column('koli_status','$1','prstatus::getStatus(koli_status)')
                        ;
                        //->edit_column('id_hadiah', '$1', 'view::btn_group_edit_delete_merchandise(id_hadiah)');

                    if($st_print == TRUE)
                        return $table->getData();
                    else
                        echo $table->generate();
                }
                break;

    		default:
    			# code...
    			break;
    	}
    }

    public function getPrintTools(){
        $button_group = array();
        if($this->access_right->otoritas('print')){
            $button_group[] = view::button_export_laporan();
        }
        
        if(true){//export
            //$button_group[] = view::button_export();
        }
        return view::render_button_group_laporan_penjualan($button_group, array(), true);

    }

    public function excel($store_id=''){

        $this->load->model(array('store_model','laporan_payout_model'));  

        $filter = array();

        $data['filter'] = $filter;
        //----------------------------------------------------
        $date = date('Ymd His');
        $header =array('KOLI CODE','PO','STORE', 'QTY KOLI', 'NOTE','ITEM','QTY','STORE RECEIVE','SUPPLIER', 'STATUS');

        $data['header'] = $header;
        $data['judul_kecil']    = 'Supplier Page';
        $data['filename']       = 'Supplier Page('.$date.')';
        $data['content']        = $this->getForm('table-main-warehouse','',array(),TRUE);// $this->laporan_payout_model->data_detail(array("id_cabang" => $store_id));
        $data['selected_store'] = $this->store_model
                                 ->select('nama_cabang')
                                 ->where(array("id_cabang"=>$store_id))
                                 ->get()
                                 ->row();
        //print_r($data['selected_store']);
        $this->load->view('cetak_excel',$data);
    
    }  
   


}


<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class dsb extends MY_Controller {
    function __construct(){
		parent::__construct();
	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		/*$memcache = new Memcache;
		$memcache->connect('localhost', 11211) or die ("Could not connect");
		$ver = $memcache->getVersion();
		echo "Server's version: ". $ver ."<br/>\n";
		$oTmp = new stdClass;
		$oTmp->str_attr = 'testing memcached';
		$oTmp->int_attr = 12345;
		$memcache->set('key', $oTmp, false, 10) or
		die ("Failed to save data at the server");
		echo "Store data in the cache (data will expire in 10 seconds)
		<br/>\n";
		$res = $memcache->get('key');
		echo "Data from the cache:<br/>\n";
		var_dump($res);
		die;*/

		//$this->output->enable_profiler(TRUE);

        $data['sidebar'] = $this->access_right->menu();
        //$data['content'] = 'base/page_content.tpl';     
        //$data['nama_pegawai'] = $this->currentUsername;
		$data['content'] = 'dashboard/page_main.tpl';
		$this->twig->display('dashboard.tpl', $data);
		//$this->twig->display('index.tpl',$data);
	}


	public function getMostItemRedemp(){
		$this->load->model(array('transaction_model'));
        $trans = $this->transaction_model
            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem'))
            ->where(array('transactiontype_id' => transactiontype::MRDM))
            ->select('item_name, sum(item_qty) as jumlah')
            ->group_by('m_item.item_key')
            ->order_by('jumlah', desc)
            ->limit(10)
            ->get()
            ->result_array();
        echo json_encode($trans);
	}

	public function getMostMember(){
		$this->load->model(array('transaction_model'));
        $trans = $this->transaction_model
            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem'))
            ->where(array('transactiontype_id' => transactiontype::MRDM))
            ->select('item_name, sum(item_qty) as jumlah')
            ->group_by('m_item.item_key')
            ->order_by('jumlah', desc)
            ->limit(10)
            ->get()
            ->result_array();
        echo json_encode($trans);
	}



	public function getItemRedempDailyJmlBarang(){
		$this->load->model(array('transaction_model'));
        $trans = $this->transaction_model
            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem'))
            ->where(array('transactiontype_id' => transactiontype::MRDM))
            ->select('DATE(zn_transaction.rec_created) as hari, sum(item_qty) as jumlah')
            ->group_by('DATE(zn_transaction.rec_created)')
            ->order_by('hari', asc)
            ->get()
            ->result_array();
        echo json_encode($trans);

	}

	public function getItemRedempDailyJmlTicket(){
		$this->load->model(array('transaction_model'));
        $trans = $this->transaction_model
            ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem'))
            ->where(array('transactiontype_id' => transactiontype::MRDM))
            ->select('DATE(zn_transaction.rec_created) as hari, sum(item_qty*ticket_count) as jumlah')
            ->group_by('DATE(zn_transaction.rec_created)')
            ->order_by('hari', asc)
            ->get()
            ->result_array();
        echo json_encode($trans);

	}


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
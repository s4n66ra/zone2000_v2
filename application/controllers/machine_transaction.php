<?php

class machine_transaction extends My_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array(
            'shipping_model','shipping_koli_model','koli_model', 'item_stok_model', 'item_model','itemtype_model', 'transaction_model', 'machine_transaction_model', 'mesin_model'
        ));
        $this->load->library('Datatable');
    }

    public function index() {
        if($this->session->userdata('is_head_office'));
            $h = '(H)';
        $data['title'] = 'Machine Transactions Reports';

        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'base/page_content.tpl';
        $data['javascript'] = array('machine-transaction.js');
        
        $data['table']['main'] = $this->getForm('main');
    
        $this->twig->display('index.tpl', $data);
    }

    public function kuras_koin($hist_option='') {
        if($hist_option=='history'){
            $this->getForm('hist-form-coin-kuras');
        }else{            

            $data['title'] = 'Kuras Koin';
            $data['sidebar'] = $this->access_right->menu();
            $data['content'] = 'base/page_content.tpl';
            $data['javascript'] = array('machine-transaction.js');            
            $data['table']['main'] = $this->getForm('main_kuras_koin');
            $this->twig->display('index.tpl', $data);
        }
    }

    public function tambah_koin($hist_option='') {
        if($hist_option=='history'){
            $this->getForm('hist-form-tambah-coin');
        }else{            

            $data['title'] = 'Tambah Koin';
            $data['sidebar'] = $this->access_right->menu();
            $data['content'] = 'base/page_content.tpl';
            $data['javascript'] = array('machine-transaction.js');            
            $data['table']['main'] = $this->getForm('main_tambah_koin');
            $this->twig->display('index.tpl', $data);
        }
    }

    public function buying($hist_option='', $getitemonly=false) {
        if($getitemonly==true){
            $this->getItemsByIdAndKeywordJSON(2);
        }else{
            if($hist_option=='history'){
                $this->getForm('hist-form-buying');
            }else{
                $data['title'] = 'Pembelian Cash (Stok MD)';
                $data['sidebar'] = $this->access_right->menu();
                $data['content'] = 'base/page_content.tpl';
                $data['javascript'] = array('machine-transaction-redemp.js');           
                $data['table']['main'] = $this->getForm('main_buying');        
                $this->twig->display('index.tpl', $data);
            }            
        }

    }

    public function buying_sc($hist_option='', $getitemonly=false) {
        if($getitemonly==true){
            $this->getItemsByIdAndKeywordJSON(3);
        }else{
            if($hist_option=='history'){
                $this->getForm('hist-form-buying-sc');
            }else{
                $data['title'] = 'Pembelian Cash (Stok Sparepart)';
                $data['sidebar'] = $this->access_right->menu();
                $data['content'] = 'base/page_content.tpl';
                $data['javascript'] = array('machine-transaction-redemp.js');           
                $data['table']['main'] = $this->getForm('main_buying_sc');        
                $this->twig->display('index.tpl', $data);
            }            
        }

    }

    public function mutasi_sc_in($hist_option='', $getitemonly=false) {
        if($getitemonly==true){
            $this->getItemsByIdAndKeywordJSON(3);
        }else{
            if($hist_option=='history'){
                $this->getForm('hist-form-mutasi-sc-in');
            }else{
                $data['title'] = 'Mutasi Masuk (SC)';
                $data['sidebar'] = $this->access_right->menu();
                $data['content'] = 'base/page_content.tpl';
                $data['javascript'] = array('machine-transaction-redemp.js');           
                $data['table']['main'] = $this->getForm('main_mutasi_sc_in');        
                $this->twig->display('index.tpl', $data);
            }            
        }

    }

    public function selling($hist_option='', $getitemonly=false) {
        if($getitemonly==true){
            $this->getItemsByIdAndKeywordJSON(2);
        }else{
            if($hist_option=='history'){
                $this->getForm('hist-form-selling');
            }else{
                $data['title'] = 'Penjualan Langsung';
                $data['sidebar'] = $this->access_right->menu();
                $data['content'] = 'base/page_content.tpl';
                $data['javascript'] = array('machine-transaction-redemp.js');           
                $data['table']['main'] = $this->getForm('main_selling');        
                $this->twig->display('index.tpl', $data);
            }            
        }

    }

    public function promosi($hist_option='', $getitemonly=false) {
        if($getitemonly==true){
            $this->getItemsByIdAndKeywordJSON(2);
        }else{
            if($hist_option=='history'){
                $this->getForm('hist-form-promosi');
            }else{
                $data['title'] = 'Penjualan Promosi';
                $data['sidebar'] = $this->access_right->menu();
                $data['content'] = 'base/page_content.tpl';
                $data['javascript'] = array('machine-transaction-redemp.js');           
                $data['table']['main'] = $this->getForm('main_promosi');        
                $this->twig->display('index.tpl', $data);
            }            
        }

    }

    public function redeem($hist_option='', $getitemonly=false) {
        if($getitemonly==true){
            $this->getItemsByIdAndKeywordJSON(2);
        }else{
            if($hist_option=='history'){
                $this->getForm('hist-form-mc-redemp');
            }else{
                $data['title'] = 'Ticket Redeem Member';
                $data['sidebar'] = $this->access_right->menu();
                $data['content'] = 'base/page_content.tpl';
                $data['javascript'] = array('machine-transaction-redemp.js');   
                //========================== sistem date ========================
                $data['st_date'] = 1;
                $data['target'] = base_url('replenish/insertToSession');
                $sistem_date = $this->session->userdata['sistem_date'];
                if(!empty($sistem_date)){
                    $data['hid_date'] = $sistem_date;
                    $data['style'] = "style=padding-top:6px;color:rgb(255,0,0);";
                }else{
                    $data['hid_date'] = date('Y-m-d');
                    $data['style'] = 'style="padding-top:6px; color: rgb(0, 0, 0);"';
                }
                //========================== end sistem date ========================        
                $data['table']['main'] = $this->getForm('main_redeem');        
                $this->twig->display('index.tpl', $data);
            }            
        }

    }

    public function redeem_regular($hist_option='', $getitemonly=false) {
        if($getitemonly==true){
            $this->getItemsByIdAndKeywordJSON(2);
        }else{
            if($hist_option=='history'){
                $this->getForm('hist-form-mc-redemp-regular');
            }else{
                $data['title'] = 'Ticket Redeem Regular';
                $data['sidebar'] = $this->access_right->menu();
                $data['content'] = 'base/page_content.tpl';
                $data['javascript'] = array('machine-transaction-redemp.js');    
                //========================== sistem date ========================
                $data['st_date'] = 1;
                $data['target'] = base_url('replenish/insertToSession');
                $sistem_date = $this->session->userdata['sistem_date'];
                if(!empty($sistem_date)){
                    $data['hid_date'] = $sistem_date;
                    $data['style'] = "style=padding-top:6px;color:rgb(255,0,0);";
                }else{
                    $data['hid_date'] = date('Y-m-d');
                    $data['style'] = 'style="padding-top:6px; color: rgb(0, 0, 0);"';
                }
                //========================== end sistem date ========================       
                $data['table']['main'] = $this->getForm('main_redeem_regular');        
                $this->twig->display('index.tpl', $data);
            }            
        }

    }

    public function vending($hist_option='') {
        if($hist_option=='history'){
            $this->getForm('hist-form-mc-refill');        
        }else{
            $data['title'] = 'Vending (Tambah Merchandise)';
            $data['sidebar'] = $this->access_right->menu();
            $data['content'] = 'base/page_content.tpl';
            $data['javascript'] = array('machine-transaction-redemp.js');
            $data['table']['main'] = $this->getForm('main_vending');
            $this->twig->display('index.tpl', $data);            

        }
    }

    public function vending_out($hist_option='') {
        if($hist_option=='history'){
            $this->getForm('hist-form-mc-refill-out');        
        }else{
            $data['title'] = 'Kuras Vending';
            $data['sidebar'] = $this->access_right->menu();
            $data['content'] = 'base/page_content.tpl';
            $data['javascript'] = array('machine-transaction-redemp.js');
            $data['table']['main'] = $this->getForm('main_vending_out');
            $this->twig->display('index.tpl', $data);            

        }
    }

    public function ticket_refill($hist_option='') {
        if($hist_option=='history'){
            $this->getForm('hist-form-ticket-refill');
        }else{
            $data['title'] = 'Machine Transactions Reports';
            $data['sidebar'] = $this->access_right->menu();
            $data['content'] = 'base/page_content.tpl';
            $data['javascript'] = array('machine-transaction-refill.js');        
            $data['table']['main'] = $this->getForm('main_ticket_refill');
            //========================== sistem date ========================
            $data['st_date'] = 1;
            $data['target'] = base_url('replenish/insertToSession');
            $sistem_date = $this->session->userdata['sistem_date'];
            if(!empty($sistem_date)){
                $data['hid_date'] = $sistem_date;
                $data['style'] = "style=padding-top:6px;color:rgb(255,0,0);";
            }else{
                $data['hid_date'] = date('Y-m-d');
                $data['style'] = 'style="padding-top:6px; color: rgb(0, 0, 0);"';
            }
            //========================== end sistem date ========================
            $this->twig->display('index.tpl', $data);
        }
    }

    public function ticket_payout($hist_option='') {
        if($hist_option=='history'){
            $this->getForm('hist-form-ticket-payout');
        }else{
            if($this->session->userdata('is_head_office')){
                //$h = '(H)';
            }
            $data['title'] = 'Machine Transactions Reports'.$h;
            $data['sidebar'] = $this->access_right->menu();
            $data['content'] = 'base/page_content.tpl';
            $data['javascript'] = array('machine-transaction-payout.js'); 

            //========================== sistem date ========================
            $data['st_date'] = 1;
            $data['target'] = base_url('replenish/insertToSession');
            $sistem_date = $this->session->userdata['sistem_date'];
            if(!empty($sistem_date)){
                $data['hid_date'] = $sistem_date;
                $data['style'] = "style=padding-top:6px;color:rgb(255,0,0);";
            }else{
                $data['hid_date'] = date('Y-m-d');
                $data['style'] = 'style="padding-top:6px; color: rgb(0, 0, 0);"';
            }
            //========================== end sistem date ========================
            $data['table']['main'] = $this->getForm('main_ticket_payout');
            $this->twig->display('index.tpl', $data);
        }
    }

    public function cutter($hist_option='') {
        if($hist_option=='history'){
            $this->getForm('hist-form-cutter');
        }else{
            $data['title'] = 'Machine Transactions Reports';
            $data['sidebar'] = $this->access_right->menu();
            $data['content'] = 'base/page_content.tpl';
            $data['javascript'] = array('machine-transaction.js');        
            $data['table']['main'] = $this->getForm('main_cutter');
            $this->twig->display('index.tpl', $data);
        }
    }

    public function getItemsByIdAndKeywordJSON($itemtype_id=''){
        $this->load->model(array('item_model'));

        $tmp = $this->item_model->getItemsByIdAndKeyword($itemtype_id);
        
        //$tmp = $this->item_model->getItemsByIdAndKeyword(2,'');
        $arr = array();
        foreach ($tmp as $k => $v) {
            if($k != ''){
                $obj = new StdClass();
                $obj->id        = $k;
                $obj->text      = $tmp[$k];
                array_push($arr, $obj);
            }
        }
        echo json_encode($arr);
    }

    

    public function getForm($type = 'add', $id=NULL, $param = array()){
        //echo $type;
        $this->load->library('custom_form');
        $this->load->library('custom_table');
        $this->load->library('table_data');
        $this->load->library('table_data_refill');
        $this->load->library('table_data_cutter');
        $this->load->library('table_data_payout');
        $this->load->library('table_data_coin_out');
        $this->load->model(array('store_data_model'));

        $storedata = $this->store_data_model;
        $mesinModel = $this->mesin_model;

        $form       = new custom_form();
        $table      = new custom_table();
        $tableData  = new table_data();
        $tableDataCutter = new table_data_cutter();
        $tableDataRefill = new table_data_refill();
        $tableDataPayout = new table_data_payout();
        $tableDataCoinOut = new table_data_coin_out();

        switch ($type) {
            case 'main' :
                $param['page']['coin_kuras'] = $this->getForm('page-coin-kuras');
                $param['page']['hist_coin_kuras'] = $this->getForm('hist-page-coin-kuras');
                
                $param['page']['ticket_refill']  = $this->getForm('page-ticket-refill');
                //$param['page']['coin_meter']  = $this->getForm('page-coin-meter');
                
                $param['page']['ticket_meter'] = $this->getForm('page-ticket-meter');
                $param['page']['ticket_refill'] = $this->getForm('page-ticket-refill');
                $param['page']['mc_refill'] = $this->getForm('page-mc-refill');
                $param['page']['mc_redemp'] = $this->getForm('page-mc-redemp');
                return $this->twig->render($this->class_name.'/page_main.tpl', $param);
                break;

            case 'main_kuras_koin' :
                $param['page']['coin_kuras'] = $this->getForm('page-coin-kuras');
                $param['page']['hist_coin_kuras'] = $this->getForm('hist-page-coin-kuras');
                return $this->twig->render($this->class_name.'/page_main.tpl', $param);
                break;

            case 'main_tambah_koin' :
                $param['page']['tambah_koin'] = $this->getForm('page-tambah-coin');
                $param['page']['hist_tambah_koin'] = $this->getForm('hist-page-tambah-coin');
                return $this->twig->render($this->class_name.'/page_main.tpl', $param);
                break;

            case 'main_redeem' :
                $param['page']['mc_redemp'] = $this->getForm('page-mc-redemp');
                $param['page']['hist_mc_redemp'] = $this->getForm('hist-page-mc-redemp');
                return $this->twig->render($this->class_name.'/page_main.tpl', $param);
                break;

            case 'main_buying' :
                $param['page']['buying'] = $this->getForm('page-buying');
                $param['page']['hist_buying'] = $this->getForm('hist-page-buying');
                return $this->twig->render($this->class_name.'/page_main.tpl', $param);
                break;

            case 'main_buying_sc' :
                $param['page']['buying_sc'] = $this->getForm('page-buying-sc');
                $param['page']['hist_buying_sc'] = $this->getForm('hist-page-buying-sc');
                return $this->twig->render($this->class_name.'/page_main.tpl', $param);
                break;

            case 'main_mutasi_sc_in' :
                $param['page']['mutasi_sc_in'] = $this->getForm('page-mutasi-sc-in');
                $param['page']['hist_mutasi_sc_in'] = $this->getForm('hist-page-mutasi-sc-in');
                return $this->twig->render($this->class_name.'/page_main.tpl', $param);
                break;

            case 'main_selling' :
                $param['page']['selling'] = $this->getForm('page-selling');
                $param['page']['hist_selling'] = $this->getForm('hist-page-selling');
                return $this->twig->render($this->class_name.'/page_main.tpl', $param);
                break;

            case 'main_promosi' :
                $param['page']['promosi'] = $this->getForm('page-promosi');
                $param['page']['hist_promosi'] = $this->getForm('hist-page-promosi');
                return $this->twig->render($this->class_name.'/page_main.tpl', $param);
                break;

            case 'main_redeem_regular' :
                $param['page']['mc_redemp'] = $this->getForm('page-mc-redemp-regular');
                $param['page']['hist_mc_redemp'] = $this->getForm('hist-page-mc-redemp-regular');
                return $this->twig->render($this->class_name.'/page_main.tpl', $param);
                break;

            case 'main_vending' :
                $param['page']['mc_refill'] = $this->getForm('page-mc-refill');
                $param['page']['hist_mc_refill'] = $this->getForm('hist-page-mc-refill');
                return $this->twig->render($this->class_name.'/page_main.tpl', $param);
                break;

            case 'main_vending_out' :
                $param['page']['mc_refill'] = $this->getForm('page-mc-refill-out');
                $param['page']['hist_mc_refill'] = $this->getForm('hist-page-mc-refill-out');
                return $this->twig->render($this->class_name.'/page_main.tpl', $param);
                break;

            case 'main_ticket_refill' :
                $param['page']['ticket_refill']  = $this->getForm('page-ticket-refill');
                $param['page']['hist_ticket_refill'] = $this->getForm('hist-page-ticket-refill');
                return $this->twig->render($this->class_name.'/page_main.tpl', $param);
                break;

            case 'main_ticket_payout' :
                $param['page']['ticket_payout']  = $this->getForm('page-ticket-payout');
                $param['page']['hist_ticket_payout'] = $this->getForm('hist-page-ticket-payout');
                return $this->twig->render($this->class_name.'/page_main.tpl', $param);
                break;

            case 'main_cutter' :
                $param['page']['cutter']  = $this->getForm('page-cutter');
                $param['page']['hist_cutter'] = $this->getForm('hist-page-cutter');
                return $this->twig->render($this->class_name.'/page_main.tpl', $param);
                break;

            case 'page-coin-meter' :
                $param['portlet'] = false;
                $param['table']['main']     = $this->getForm('form-coin-meter');
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'page-cutter' :
                $param['portlet'] = false;
                $param['table']['main']     = $this->getForm('form-cutter');
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'hist-page-cutter' :
                $param['portlet'] = false;
                $param['table']['main']  = $this->getForm('hist-form-cutter', '', array('wrapper'));
                $param['javascript'] = array('receiving.js');
                
                return $this->twig->render('base/page_content.tpl', $param);
                break;


            case 'form-cutter' :
                $table->columnWidth = array(30,30,30,10);
                $table->columns     = array('TICKET METER', '');
                $table->id          = 'table-ticket-cutter';
                $table->numbering   = false;
                $tableDataCutter->table   = $table;
                $tableDataCutter->param   = array(
                                        'type_id' => transactiontype::TCUT,
                                    );
                return $tableDataCutter->generate();
                break;

            case 'hist-form-cutter' :
                $table = new Datatable();

                $data = $this->mesin_model->options_filter();                
                if (in_array('wrapper', $param)) {
                    $table->numbering  = true;
                    $table->id         = 'table-hist-ticket_cutter';
                    $table->isScrollable= false;


                    $filter = array(
                        array('id_mesin', 'Machine ID', 'list', $data),
                        array('nama_jenis_mesin', 'Machine Type', 'text'),
                        array('num_meter', 'Number Meter', 'text'),
                        array('rec_created', 'Date', 'date'),
                        array('nama_pegawai', 'Employee', 'text'),
                        array('nama_cabang', 'Store Name', 'text'),

                    );


                    $table->dataFilter = $filter;


                    $table->header     = array('SUM', 'DATE', 'EMPLOYEE' ,'STORE');
                    $table->source      = site_url($this->class_name.'/cutter/history');
                    return $table->generateWrapper();

                } else {
                    $table
                        ->setModel($this->transaction_model)
                        ->setNumbering()
                        ->with(array('transaction_machine','transaction_store','machine','cabang','pegawai', 'jenis_mesin'))
                        ->where(array('transactiontype_id' => 7))
                        ->select('num_meter, DATE(rec_created) as rec_created, nama_pegawai ,nama_cabang')
                        ->order_by('rec_created desc')
                        ->edit_column('rec_created', '$1', 'hgenerator::switch_tanggal(rec_created)');
                        //->edit_column('shipping_id', '$1', 'view::btn_group_receiving_warehouse(shipping_id)');
                    echo $table->generate();
                }                
                break;

            case 'page-ticket-refill' :
                $param['portlet'] = false;
                $param['table']['main']     = $this->getForm('form-ticket-refill');
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'hist-page-ticket-refill' :
                $param['portlet'] = false;
                //$param['table']['main']     = $this->getForm('hist-form-coin-kuras','',array('wrapper'));
                $param['table']['main']  = $this->getForm('hist-form-ticket-refill', '', array('wrapper'));
                $param['javascript'] = array('receiving.js');
                
                return $this->twig->render('base/page_content.tpl', $param);
                break;


            case 'form-ticket-refill' :
                $table->columnWidth = array(30,30,10,10,10,10);
                $table->columns     = array('MACHINE', 'INFO', 'MACHINE LAST TIKET NUMBER', 'START NUMBER','END NUMBER','');
                $table->id          = 'table-ticket-refill';
                $table->numbering   = false;
                $tableDataRefill->table   = $table;
                $tableDataRefill->param   = array(
                                        'type_id' => transactiontype::TFIL,
                                    );
                return $tableDataRefill->generate();
                break;

            case 'hist-form-ticket-refill' :
                $table = new Datatable();
                $this->load->model('ticket_refill_model');
                $data = $this->mesin_model->options_filter();                
                if (in_array('wrapper', $param)) {
                    $table->numbering  = true;
                    $table->id         = 'table-hist-ticket-refill';
                    $table->isScrollable= false;


                    $filter = array(
                        array('id_mesin', 'Machine ID', 'list', $data),
                        array('nama_jenis_mesin', 'Machine Type', 'text'),
                        array('num_meter', 'Number Meter', 'text'),
                        array('rec_created', 'Date', 'date'),
                        array('nama_pegawai', 'Employee', 'text'),
                        array('nama_cabang', 'Store Name', 'text'),

                    );


                    $table->dataFilter = $filter;


                    $table->header     = array('MACHINE CODE','REGISTER MACHINE','MACHINE TYPE','SUM','LAST METER','NUM START','NUM END', 'DATE', 'EMPLOYEE' ,'STORE');
                    $table->source      = site_url($this->class_name.'/ticket_refill/history');
                    return $table->generateWrapper();

                } else {
                    $table
                        ->setModel($this->ticket_refill_model)
                        ->setNumbering()
                        //->with(array('transaction_machine','transaction_store','machine','cabang','pegawai', 'jenis_mesin'))
                        //->where(array('transactiontype_id' => transactiontype::TFIL))
                        ->select('kd_mesin,serial_number, nama_jenis_mesin, num_meter,num_ticket,num_start,num_end, DATE(rec_created) as rec_created, nama_pegawai ,nama_cabang')
                        //->order_by('zn_transaction.transaction_id desc')
                        ->edit_column('rec_created', '$1', 'hgenerator::switch_tanggal(rec_created)');
                        //->edit_column('shipping_id', '$1', 'view::btn_group_receiving_warehouse(shipping_id)');
                    echo $table->generate();
                }                
                break;


            case 'page-ticket-payout' :
                $param['portlet'] = false;
                $param['table']['main']     = $this->getForm('form-ticket-payout');
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'hist-page-ticket-payout' :
                $param['portlet'] = false;
                //$param['table']['main']     = $this->getForm('hist-form-coin-kuras','',array('wrapper'));
                $param['table']['main']  = $this->getForm('hist-form-ticket-payout', '', array('wrapper'));
                $param['javascript'] = array('receiving.js');
                
                return $this->twig->render('base/page_content.tpl', $param);
                break;


            case 'form-ticket-payout' :
                $table->columnWidth = array(30,30,30,10,10,10);
                $table->columns     = array('MACHINE', 'LAST TIKET NUMBER','CURENT TICKET NUMBER','');
                $table->id          = 'table-ticket-payout';
                $table->numbering   = false;
                $tableDataPayout->table   = $table;
                $tableDataPayout->param   = array(
                                        'type_id' => transactiontype::TPAYO,
                                    );
                return $tableDataPayout->generate();
                break;

            case 'hist-form-ticket-payout' :
                $table = new Datatable();

                $data = $this->mesin_model->options_filter();                
                if (in_array('wrapper', $param)) {
                    $table->numbering  = true;
                    $table->id         = 'table-hist-ticket-payout';
                    $table->isScrollable= false;


                    $filter = array(
                        array('id_mesin', 'Machine ID', 'list', $data),
                        array('nama_jenis_mesin', 'Machine Type', 'text'),
                        array('num_meter', 'Number Meter', 'text'),
                        array('rec_created', 'Date', 'date'),
                        array('nama_pegawai', 'Employee', 'text'),
                        array('nama_cabang', 'Store Name', 'text'),

                    );


                    $table->dataFilter = $filter;


                    $table->header     = array('MACHINE CODE','MACHINE TYPE','REGISTER','NUM TICKET','LAST METER','CURENT METER', 'DATE', 'EMPLOYEE' ,'STORE');
                    $table->source      = site_url($this->class_name.'/ticket_payout/history');
                    return $table->generateWrapper();

                } else {
                    $table
                        ->setModel($this->transaction_model)
                        ->setNumbering()
                        ->with(array('transaction_machine','transaction_store','machine','cabang','pegawai', 'jenis_mesin'))
                        ->where(array('transactiontype_id' => transactiontype::TPAYO))
                        ->select('kd_mesin, nama_jenis_mesin,serial_number, num_ticket,num_start,num_end, DATE(rec_created) as rec_created, nama_pegawai ,nama_cabang')
                        ->order_by('zn_transaction.transaction_id desc')
                        ->edit_column('rec_created', '$1', 'hgenerator::switch_tanggal(rec_created)');
                        //->edit_column('shipping_id', '$1', 'view::btn_group_receiving_warehouse(shipping_id)');
                    echo $table->generate();
                }                
                break;
            

            case 'form-coin-meter' :
                $table->columnWidth = array(30,30,30,10);
                $table->columns     = array('MACHINE', 'INFO', 'COIN METER', '');
                $table->id          = 'table-coin-meter';
                $table->numbering   = false;
                $tableData->table   = $table;
                $tableData->param   = array(
                                        'type_id' => transactiontype::COIN,
                                    );
                return $tableData->generate();
                break;

            case 'page-tambah-coin' :
                $param['portlet'] = false;
                $param['table']['main']     = $this->getForm('form-tambah-coin');
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'hist-page-tambah-coin' :
                $param['portlet'] = false;
                //$param['table']['main']     = $this->getForm('hist-form-coin-kuras','',array('wrapper'));
                $param['table']['main']  = $this->getForm('hist-form-tambah-coin', '', array('wrapper'));
                $param['javascript'] = array('receiving.js');
                
                return $this->twig->render('base/page_content.tpl', $param);
                break;


            case 'form-tambah-coin' :
                $table->columnWidth = array(30,30,30,10);
                $table->columns     = array('MACHINE', 'INFO', 'COIN COUNT', '');
                $table->id          = 'table-tambah-coin';
                $table->numbering   = false;
                $tableData->table   = $table;
                $tableData->param   = array(
                                        'type_id' => transactiontype::CADD,
                                    );
                return $tableData->generate();
                break;

            case 'hist-form-tambah-coin' :
                $table = new Datatable();

                $data = $this->mesin_model->options_filter();                
                if (in_array('wrapper', $param)) {
                    $table->numbering  = true;
                    $table->id         = 'hist-table-tambah-coin';
                    $table->isScrollable= false;


                    $filter = array(
                        array('id_mesin', 'Machine Code', 'list', $data),
                        array('num_coin', 'Number Coin', 'text'),
                        array('rec_created', 'Date', 'date'),
                        array('nama_pegawai', 'Employee', 'text'),
                        array('nama_cabang', 'Store Name', 'text'),

                    );


                    $table->dataFilter = $filter;

                    $table->header     = array('MACHINE CODE','MACHINE TYPE','SUM', 'DATE', 'EMPLOYEE' ,'STORE');

                    $table->source      = site_url($this->class_name.'/tambah_koin/history');
                    return $table->generateWrapper();

                } else {
                    $table
                        ->setModel($this->transaction_model)
                        ->setNumbering()
                        ->with(array('transaction_machine','transaction_store','machine','cabang','pegawai', 'jenis_mesin'))
                        ->where(array('transactiontype_id' => transactiontype::CADD))
                        ->select('kd_mesin, nama_jenis_mesin, num_coin, DATE(rec_created) as rec_created, nama_pegawai ,nama_cabang')
                        ->order_by('rec_created desc')
                        ->edit_column('rec_created', '$1', 'hgenerator::switch_tanggal(rec_created)');
                        //->edit_column('shipping_id', '$1', 'view::btn_group_receiving_warehouse(shipping_id)');
                    echo $table->generate();
                }
                break;

            case 'page-coin-kuras' :
                $param['portlet'] = false;
                $param['table']['main']     = $this->getForm('form-coin-kuras');
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'hist-page-coin-kuras' :
                $param['portlet'] = false;
                //$param['table']['main']     = $this->getForm('hist-form-coin-kuras','',array('wrapper'));
                $param['table']['main']  = $this->getForm('hist-form-coin-kuras', '', array('wrapper'));
                $param['javascript'] = array('receiving.js');
                
                return $this->twig->render('base/page_content.tpl', $param);
                break;


            case 'form-coin-kuras' :
                $table->columnWidth = array(22,22,22,22,12);
                $table->columns     = array('MACHINE' , 'LAST COIN METER', 'CURENT COIN METER','JUMLAH COIN CH','');
                $table->id          = 'table-coin-kuras';
                $table->numbering   = false;
                $tableDataCoinOut->table   = $table;
                $tableDataCoinOut->param   = array(
                                        'type_id' => transactiontype::COUT,
                                    );
                return $tableDataCoinOut->generate();
                break;

            case 'hist-form-coin-kuras' :
                $table = new Datatable();

                $data = $this->mesin_model->options_filter();                
                if (in_array('wrapper', $param)) {
                    $table->numbering  = true;
                    $table->id         = 'hist-table-coin-kuras';
                    $table->isScrollable= false;


                    $filter = array(
                        array('id_mesin', 'Machine Code', 'list', $data),
                        array('num_coin', 'Number Coin', 'text'),
                        array('rec_created', 'Date', 'date'),
                        array('nama_pegawai', 'Employee', 'text'),
                        array('nama_cabang', 'Store Name', 'text'),

                    );


                    $table->dataFilter = $filter;

                    $table->header     = array('REGISTER','MACHINE TYPE','JUMLAH COIN','LAST METER','CURENT METER','COIN NON-CH', 'DATE', 'EMPLOYEE' ,'STORE');

                    $table->source      = site_url($this->class_name.'/kuras_koin/history');
                    return $table->generateWrapper();

                } else {
                    $table
                        ->setModel($this->transaction_model)
                        ->setNumbering()
                        ->with(array('transaction_machine','transaction_store','machine','cabang','pegawai', 'jenis_mesin'))
                        ->where(array('transactiontype_id' => 2))
                        ->select('serial_number, nama_jenis_mesin, num_coin,num_start,num_end,num_ticket as num_non_ch, DATE(rec_created) as rec_created, nama_pegawai ,nama_cabang')
                        ->order_by('rec_created desc')
                        ->edit_column('rec_created', '$1', 'hgenerator::switch_tanggal(rec_created)');
                        //->edit_column('shipping_id', '$1', 'view::btn_group_receiving_warehouse(shipping_id)');
                    echo $table->generate();
                }
                break;


            case 'page-ticket-meter' :
                $param['portlet'] = false;
                $param['table']['main']     = $this->getForm('form-ticket-meter');
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'form-ticket-meter' :
                $table->columnWidth = array(30,30,30,10);
                $table->columns     = array('MACHINE', 'INFO', 'TICKET METER', '');
                $table->id          = 'table-ticket-meter';
                $table->numbering   = false;
                $tableData->table   = $table;
                $tableData->param   = array(
                                        'type_id' => transactiontype::TOUT,
                                    );
                return $tableData->generate();
                break;

            case 'page-ticket-refill' :
                $param['portlet'] = false;
                $param['table']['main']     = $this->getForm('form-ticket-refill');
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'form-ticket-refill' :
                $table->columnWidth = array(20,30,20,20,10);
                $table->columns     = array('MACHINE', 'INFO', 'START', 'END', '');
                $table->id          = 'table-ticket-refill';
                $table->numbering       = false;
                $tableData->num_input   = 2;
                $tableData->table       = $table;
                $tableData->param       = array(
                                            'type_id' => transactiontype::TFIL,
                                        );
                return $tableData->generate();
                break;

            case 'page-mc-refill' :
                $param['portlet'] = false;
                $param['table']['main']     = $this->getForm('form-mc-refill');
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'hist-page-mc-refill' :
                $param['portlet'] = false;
                //$param['table']['main']     = $this->getForm('hist-form-coin-kuras','',array('wrapper'));
                $param['table']['main']  = $this->getForm('hist-form-mc-refill', '', array('wrapper'));
                $param['javascript'] = array('receiving.js');
                
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'form-mc-refill' :
                $this->load->model(array('item_model','mesin_model'));
                $table->columnWidth = array(30,30,30,10);
                $table->columns     = array('MACHINE', 'INFO', 'ITEM QTY', '');
                $table->id          = 'table-mc-refill';
                $table->numbering       = false;
                $tableData->list_item   = $this->item_model->options(itemtype::ITEM_MERCHANDISE);
                $tableData->list_bundle = $this->mesin_model->options_vending();
                $tableData->table       = $table;
                $tableData->param       = array(
                                            'type_id' => transactiontype::MFIL,
                                        );
                return $tableData->generateRedemp();
                break;

            case 'hist-form-mc-refill' :
                $table = new Datatable();

                $data = $this->mesin_model->options_filter();                
                if (in_array('wrapper', $param)) {
                    $table->numbering  = true;
                    $table->id         = 'hist-table-mc-refill';
                    $table->isScrollable= false;


                    $filter = array(
                        array('id_mesin', 'Machine Code', 'list', $data),
                        array('nama_jenis_mesin', 'Machine Type', 'text'),
                        array('item_name', 'Item Name', 'text'),
                        array('item_qty', 'Item Quantity', 'text'),
                        array('rec_created', 'Date', 'date'),
                        array('nama_pegawai', 'Employee', 'text'),
                        array('nama_cabang', 'Store Name', 'text'),

                    );


                    $table->dataFilter = $filter;


                    $table->header      = array('MACHINE CODE','MACHINE TYPE','ITEM NAME', 'ITEM QTY','DATE', 'EMPLOYEE' ,'STORE');
                    $table->source      = site_url($this->class_name.'/vending/history');
                    return $table->generateWrapper();

                } else {
                    $table
                        ->setModel($this->transaction_model)
                        ->setNumbering()
                        ->with(array('transaction_machine','transaction_store','machine','cabang','pegawai', 'jenis_mesin', 'refill', 'item'))
                        ->where(array('transactiontype_id' => 6))
                        ->select('kd_mesin, nama_jenis_mesin, item_name, item_qty,DATE(zn_transaction.rec_created) as rec_created, nama_pegawai ,nama_cabang')
                        ->order_by('rec_created desc')
                        ->edit_column('rec_created', '$1', 'hgenerator::switch_tanggal(rec_created)');
                        //->edit_column('shipping_id', '$1', 'view::btn_group_receiving_warehouse(shipping_id)');
                    echo $table->generate();
                }
                break;

            case 'page-mc-refill-out' :
                $param['portlet'] = false;
                $param['table']['main']     = $this->getForm('form-mc-refill-out');
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'hist-page-mc-refill-out' :
                $param['portlet'] = false;
                //$param['table']['main']     = $this->getForm('hist-form-coin-kuras','',array('wrapper'));
                $param['table']['main']  = $this->getForm('hist-form-mc-refill-out', '', array('wrapper'));
                $param['javascript'] = array('receiving.js');
                
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'form-mc-refill-out' :
                $this->load->model(array('item_model','mesin_model'));
                $table->columnWidth = array(30,30,30,10);
                $table->columns     = array('MACHINE', 'INFO', 'ITEM QTY', '');
                $table->id          = 'table-mc-refill';
                $table->numbering       = false;
                $tableData->list_item   = $this->item_model->options(itemtype::ITEM_MERCHANDISE);
                $tableData->list_bundle = $this->mesin_model->options_vending();
                $tableData->table       = $table;
                $tableData->param       = array(
                                            'type_id' => transactiontype::MFILO,
                                        );
                return $tableData->generateRedemp();
                break;

            case 'hist-form-mc-refill-out' :
                $table = new Datatable();

                $data = $this->mesin_model->options_filter();                
                if (in_array('wrapper', $param)) {
                    $table->numbering  = true;
                    $table->id         = 'hist-table-mc-refill';
                    $table->isScrollable= false;


                    $filter = array(
                        array('id_mesin', 'Machine Code', 'list', $data),
                        array('nama_jenis_mesin', 'Machine Type', 'text'),
                        array('item_name', 'Item Name', 'text'),
                        array('item_qty', 'Item Quantity', 'text'),
                        array('rec_created', 'Date', 'date'),
                        array('nama_pegawai', 'Employee', 'text'),
                        array('nama_cabang', 'Store Name', 'text'),

                    );


                    $table->dataFilter = $filter;


                    $table->header      = array('MACHINE CODE','MACHINE TYPE','ITEM NAME', 'ITEM QTY','DATE', 'EMPLOYEE' ,'STORE');
                    $table->source      = site_url($this->class_name.'/vending_out/history');
                    return $table->generateWrapper();

                } else {
                    $table
                        ->setModel($this->transaction_model)
                        ->setNumbering()
                        ->with(array('transaction_machine','transaction_store','machine','cabang','pegawai', 'jenis_mesin', 'refill_out', 'item_out'))
                        ->where(array('transactiontype_id' => transactiontype::MFILO))
                        ->select('kd_mesin, nama_jenis_mesin, item_name, item_qty,DATE(zn_transaction.rec_created) as rec_created, nama_pegawai ,nama_cabang')
                        ->order_by('rec_created desc')
                        ->edit_column('rec_created', '$1', 'hgenerator::switch_tanggal(rec_created)');
                        //->edit_column('shipping_id', '$1', 'view::btn_group_receiving_warehouse(shipping_id)');
                    echo $table->generate();
                }
                break;

            case 'page-buying' :
                $param['portlet'] = false;
                $param['table']['main']     = $this->getForm('form-buying');
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'hist-page-buying' :
                $param['portlet'] = false;
                $param['table']['main']  = $this->getForm('hist-form-buying', '', array('wrapper'));
                $param['javascript'] = array('receiving.js');
                
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'page-buying-sc' :
                $param['portlet'] = false;
                $param['table']['main']     = $this->getForm('form-buying-sc');
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'hist-page-buying-sc' :
                $param['portlet'] = false;
                $param['table']['main']  = $this->getForm('hist-form-buying-sc', '', array('wrapper'));
                $param['javascript'] = array('receiving.js');
                
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'page-mutasi-sc-in' :
                $param['portlet'] = false;
                $param['table']['main']     = $this->getForm('form-mutasi-sc-in');
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'hist-page-mutasi-sc-in' :
                $param['portlet'] = false;
                $param['table']['main']  = $this->getForm('hist-form-mutasi-sc-in', '', array('wrapper'));
                $param['javascript'] = array('receiving.js');
                
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'page-selling' :
                $param['portlet'] = false;
                $param['table']['main']     = $this->getForm('form-selling');
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'hist-page-selling' :
                $param['portlet'] = false;
                $param['table']['main']  = $this->getForm('hist-form-selling', '', array('wrapper'));
                $param['javascript'] = array('receiving.js');
                
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'page-promosi' :
                $param['portlet'] = false;
                $param['table']['main']     = $this->getForm('form-promosi');
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'hist-page-promosi' :
                $param['portlet'] = false;
                $param['table']['main']  = $this->getForm('hist-form-promosi', '', array('wrapper'));
                $param['javascript'] = array('receiving.js');
                
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'page-mc-redemp' :
                $param['portlet'] = false;
                $param['table']['main']     = $this->getForm('form-mc-redemp');
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'hist-page-mc-redemp' :
                $param['portlet'] = false;
                $param['table']['main']  = $this->getForm('hist-form-mc-redemp', '', array('wrapper'));
                $param['javascript'] = array('receiving.js');
                
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'page-mc-redemp-regular' :
                $param['portlet'] = false;
                $param['table']['main']     = $this->getForm('form-mc-redemp-regular');
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'hist-page-mc-redemp-regular' :
                $param['portlet'] = false;
                $param['table']['main']  = $this->getForm('hist-form-mc-redemp-regular', '', array('wrapper'));
                $param['javascript'] = array('receiving.js');
                
                return $this->twig->render('base/page_content.tpl', $param);
                break;

            case 'form-mc-redemp' :
                $this->load->model(array('item_model','mesin_model'));
                $table->columnWidth = array(30,30,30,10);
                $table->columns     = array('MERCHANDISE', 'INFO', 'ITEM QTY', '');
                $table->id          = 'table-mc-redemp';
                $table->numbering       = false;
                $tableData->list_item   = $this->item_model->options(itemtype::ITEM_MERCHANDISE);
                //$tableData->list_bundle = $this->mesin_model->options2();
                $tableData->table       = $table;
                $tableData->param       = array(
                                            'type_id' => transactiontype::MRDM,
                                        );
                return $tableData->generateRedemp();
                break;

            case 'hist-form-mc-redemp' :
                $table = new Datatable();

                $data = $this->mesin_model->options_filter();                
                if (in_array('wrapper', $param)) {
                    $table->numbering  = true;
                    $table->id         = 'table-ticket-redemp';
                    $table->isScrollable= false;


                    $filter = array(
                        array('num_coin', 'Number Coin', 'text'),
                        array('rec_created', 'Date', 'date'),
                        array('nama_pegawai', 'Employee', 'text'),
                        array('nama_cabang', 'Store Name', 'text'),

                    );


                    $table->dataFilter = $filter;


                    $table->header      = array('ITEM NAME', 'ITEM QTY','TICKET COUNT','DATE', 'EMPLOYEE' ,'STORE');
                    $table->source      = site_url($this->class_name.'/redeem/history');
                    return $table->generateWrapper();

                } else {
                    $firstMonth = date('Y-m-'.'01');
                    $table
                        ->setModel($this->transaction_model)
                        ->setNumbering()
                        ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem'))
                        ->where(array('transactiontype_id' => transactiontype::MRDM))
 //                       ->where('zn_transaction.rec_created >='. $firstMonth,NULL)
                        ->select('item_name, item_qty,ticket_count,DATE(zn_transaction.rec_created) as rec_created, nama_pegawai ,nama_cabang')
                        ->order_by('rec_created desc')
                        ->edit_column('rec_created', '$1', 'hgenerator::switch_tanggal(rec_created)');
                        //->edit_column('shipping_id', '$1', 'view::btn_group_receiving_warehouse(shipping_id)');
                    echo $table->generate();
                }

                break;

            case 'form-mc-redemp-regular' :
                $this->load->model(array('item_model','mesin_model'));
                $table->columnWidth = array(30,30,30,10);
                $table->columns     = array('MERCHANDISE', 'INFO', 'ITEM QTY', '');
                $table->id          = 'table-mc-redemp';
                $table->numbering       = false;
                $tableData->list_item   = $this->item_model->options(itemtype::ITEM_MERCHANDISE);
                //$tableData->list_bundle = $this->mesin_model->options2();
                $tableData->table       = $table;
                $tableData->param       = array(
                                            'type_id' => transactiontype::MRDM_REG,
                                        );
                return $tableData->generateRedemp();
                break;

            case 'hist-form-mc-redemp-regular' :
                $table = new Datatable();

                $data = $this->mesin_model->options_filter();                
                if (in_array('wrapper', $param)) {
                    $table->numbering  = true;
                    $table->id         = 'table-ticket-redemp';
                    $table->isScrollable= false;


                    $filter = array(
                        array('num_coin', 'Number Coin', 'text'),
                        array('rec_created', 'Date', 'date'),
                        array('nama_pegawai', 'Employee', 'text'),
                        array('nama_cabang', 'Store Name', 'text'),

                    );


                    $table->dataFilter = $filter;


                    $table->header      = array('ITEM NAME', 'ITEM QTY','TICKET COUNT','DATE', 'EMPLOYEE' ,'STORE');
                    $table->source      = site_url($this->class_name.'/redeem_regular/history');
                    return $table->generateWrapper();

                } else {
                    $table
                        ->setModel($this->transaction_model)
                        ->setNumbering()
                        ->with(array('transaction_store','cabang','pegawai', 'redeem', 'item_redeem'))
                        ->where(array('transactiontype_id' => transactiontype::MRDM_REG))
                        ->select('item_name, item_qty,ticket_count,DATE(zn_transaction.rec_created) as rec_created, nama_pegawai ,nama_cabang')
                        ->order_by('rec_created desc')
                        ->edit_column('rec_created', '$1', 'hgenerator::switch_tanggal(rec_created)');
                        //->edit_column('shipping_id', '$1', 'view::btn_group_receiving_warehouse(shipping_id)');
                    echo $table->generate();
                }

                break;

            case 'form-buying' :
                $this->load->model(array('item_model','mesin_model'));
                $table->columnWidth = array(30,30,30,10);
                $table->columns     = array('MERCHANDISE', 'INFO', 'ITEM QTY', '');
                $table->id          = 'table-mc-redemp';
                $table->numbering       = false;
                $tableData->list_item   = $this->item_model->options(itemtype::ITEM_MERCHANDISE);
                //$tableData->list_bundle = $this->mesin_model->options2();
                $tableData->table       = $table;
                $tableData->param       = array(
                                            'type_id' => transactiontype::BYS,
                                        );
                return $tableData->generateRedemp();
                break;

            case 'hist-form-buying' :
                $table = new Datatable();
                $this->load->model('beli_langsung_model');
                $data = $this->mesin_model->options_filter();                
                if (in_array('wrapper', $param)) {
                    $table->numbering  = true;
                    $table->id         = 'table-buying';
                    $table->isScrollable= false;


                    $filter = array(
                        array('num_coin', 'Number Coin', 'text'),
                        array('rec_created', 'Date', 'date'),
                        array('nama_pegawai', 'Employee', 'text'),
                        array('nama_cabang', 'Store Name', 'text'),

                    );


                    $table->dataFilter = $filter;


                    $table->header      = array('DATE','SKU','NAME','QTY', 'STORE', 'TYPE');
                    $table->source      = site_url($this->class_name.'/buying/history');
                    return $table->generateWrapper();

                } else {
                    $table
                        ->setModel($this->beli_langsung_model)
                        ->setNumbering()
                        ->with(array('type','item','outlet'))
                        ->where('this.jenis_buying_id',1)
                        ->select('rec_created,item.item_key, item.item_name, item_qty, nama_cabang, nama_jenis')
                        ->order_by('buying_id','desc')
                        ->edit_column('rec_created', '$1', 'date::shortDate(rec_created)');
                        //->edit_column('shipping_id', '$1', 'view::btn_group_receiving_warehouse(shipping_id)');
                    echo $table->generate();
                }

                break;

            case 'form-buying-sc' :
                $this->load->model(array('item_model','mesin_model'));
                $table->columnWidth = array(30,30,30,10);
                $table->columns     = array('SPAREPART', 'INFO', 'ITEM QTY', '');
                $table->id          = 'table-mc-redemp-sc';
                $table->numbering       = false;
                $tableData->list_item   = $this->item_model->options_sparepart(itemtype::ITEM_SPAREPART);
                //$tableData->list_bundle = $this->mesin_model->options2();
                $tableData->table       = $table;
                $tableData->param       = array(
                                            'type_id' => transactiontype::BYSC,
                                        );
                return $tableData->generateRedemp();
                break;

            case 'hist-form-buying-sc' :
                $table = new Datatable();
                $this->load->model('beli_langsung_model');
                $data = $this->mesin_model->options_filter();                
                if (in_array('wrapper', $param)) {
                    $table->numbering  = true;
                    $table->id         = 'table-buying-sc';
                    $table->isScrollable= false;


                    $filter = array(
                        array('num_coin', 'Number Coin', 'text'),
                        array('rec_created', 'Date', 'date'),
                        array('nama_pegawai', 'Employee', 'text'),
                        array('nama_cabang', 'Store Name', 'text'),

                    );


                    $table->dataFilter = $filter;


                    $table->header      = array('DATE','SKU','NAME','QTY', 'STORE', 'TYPE');
                    $table->source      = site_url($this->class_name.'/buying_sc/history');
                    return $table->generateWrapper();

                } else {
                    $table
                        ->setModel($this->beli_langsung_model)
                        ->setNumbering()
                        ->with(array('type','item','outlet'))
                        ->where('this.jenis_buying_id',3)
                        ->select('rec_created,item.item_key, item.item_name, item_qty, nama_cabang, nama_jenis')
                        ->order_by('buying_id','desc')
                        ->edit_column('rec_created', '$1', 'date::shortDate(rec_created)');
                        //->edit_column('shipping_id', '$1', 'view::btn_group_receiving_warehouse(shipping_id)');
                    echo $table->generate();
                }

                break;

            case 'form-mutasi-sc-in' :
                $this->load->model(array('item_model','mesin_model'));
                $table->columnWidth = array(30,30,30,10);
                $table->columns     = array('SPAREPART MUTASI', 'INFO', 'ITEM QTY', '');
                $table->id          = 'table-mc-redemp-sc';
                $table->numbering       = false;
                $tableData->list_item   = $this->item_model->options_sparepart(itemtype::ITEM_SPAREPART);
                //$tableData->list_bundle = $this->mesin_model->options2();
                $tableData->table       = $table;
                $tableData->param       = array(
                                            'type_id' => transactiontype::MTSI,
                                        );
                return $tableData->generateRedemp();
                break;

            case 'hist-form-mutasi-sc-in' :
                $table = new Datatable();
                $this->load->model('beli_langsung_model');
                $data = $this->mesin_model->options_filter();                
                if (in_array('wrapper', $param)) {
                    $table->numbering  = true;
                    $table->id         = 'table-buying-sc';
                    $table->isScrollable= false;


                    $filter = array(
                        array('num_coin', 'Number Coin', 'text'),
                        array('rec_created', 'Date', 'date'),
                        array('nama_pegawai', 'Employee', 'text'),
                        array('nama_cabang', 'Store Name', 'text'),

                    );


                    $table->dataFilter = $filter;


                    $table->header      = array('DATE','SKU','NAME','QTY', 'STORE', 'TYPE');
                    $table->source      = site_url($this->class_name.'/mutasi_sc_in/history');
                    return $table->generateWrapper();

                } else {
                    $table
                        ->setModel($this->beli_langsung_model)
                        ->setNumbering()
                        ->with(array('type','item','outlet'))
                        ->where('this.jenis_buying_id',4)
                        ->select('rec_created,item.item_key, item.item_name, item_qty, nama_cabang, nama_jenis')
                        ->order_by('buying_id','desc')
                        ->edit_column('rec_created', '$1', 'date::shortDate(rec_created)');
                        //->edit_column('shipping_id', '$1', 'view::btn_group_receiving_warehouse(shipping_id)');
                    echo $table->generate();
                }

                break;

            case 'form-selling' :
                $this->load->model(array('item_model','mesin_model'));
                $table->columnWidth = array(30,30,30,10);
                $table->columns     = array('MERCHANDISE', 'INFO', 'ITEM QTY', '');
                $table->id          = 'table-mc-redemp';
                $table->numbering       = false;
                $tableData->list_item   = $this->item_model->options(itemtype::ITEM_MERCHANDISE);
                //$tableData->list_bundle = $this->mesin_model->options2();
                $tableData->table       = $table;
                $tableData->param       = array(
                                            'type_id' => transactiontype::SELL,
                                        );
                return $tableData->generateRedemp();
                break;

            case 'hist-form-selling' :
                $table = new Datatable();
                $this->load->model('selling_model');
                $data = $this->mesin_model->options_filter();                
                if (in_array('wrapper', $param)) {
                    $table->numbering  = true;
                    $table->id         = 'table-selling';
                    $table->isScrollable= false;


                    $filter = array(
                        array('num_coin', 'Number Coin', 'text'),
                        array('rec_created', 'Date', 'date'),
                        array('nama_pegawai', 'Employee', 'text'),
                        array('nama_cabang', 'Store Name', 'text'),

                    );


                    $table->dataFilter = $filter;


                    $table->header      = array('DATE','SKU','NAME','QTY', 'STORE', 'TYPE');
                    $table->source      = site_url($this->class_name.'/selling/history');
                    return $table->generateWrapper();

                } else {
                    $table
                        ->setModel($this->selling_model)
                        ->setNumbering()
                        ->with(array('type','item','outlet'))
                        ->where('this.jenis_selling_id',1) //----------------jenis penjualan langsung
                        ->select('rec_created,item.item_key, item.item_name, item_qty, nama_cabang, nama_jenis')
                        ->edit_column('rec_created', '$1', 'date::shortDate(rec_created)');
                        //->edit_column('shipping_id', '$1', 'view::btn_group_receiving_warehouse(shipping_id)');
                    echo $table->generate();
                }

                break;

            case 'form-promosi' :
                $this->load->model(array('item_model','mesin_model'));
                $table->columnWidth = array(30,30,30,10);
                $table->columns     = array('MERCHANDISE', 'INFO', 'ITEM QTY', '');
                $table->id          = 'table-mc-redemp';
                $table->numbering       = false;
                $tableData->list_item   = $this->item_model->options(itemtype::ITEM_MERCHANDISE);
                //$tableData->list_bundle = $this->mesin_model->options2();
                $tableData->table       = $table;
                $tableData->param       = array(
                                            'type_id' => transactiontype::PRMO,
                                        );
                return $tableData->generateRedemp();
                break;

            case 'hist-form-promosi' :
                $table = new Datatable();
                $this->load->model('selling_model');
                $data = $this->mesin_model->options_filter();                
                if (in_array('wrapper', $param)) {
                    $table->numbering  = true;
                    $table->id         = 'table-promosi';
                    $table->isScrollable= false;


                    $filter = array(
                        array('num_coin', 'Number Coin', 'text'),
                        array('rec_created', 'Date', 'date'),
                        array('nama_pegawai', 'Employee', 'text'),
                        array('nama_cabang', 'Store Name', 'text'),

                    );


                    $table->dataFilter = $filter;


                    $table->header      = array('DATE','SKU','NAME','QTY', 'STORE', 'TYPE');
                    $table->source      = site_url($this->class_name.'/promosi/history');
                    return $table->generateWrapper();

                } else {
                    $table
                        ->setModel($this->selling_model)
                        ->setNumbering()
                        ->with(array('type','item','outlet'))
                        ->where('this.jenis_selling_id',2) //------------- jenis promosi
                        ->select('rec_created,item.item_key, item.item_name, item_qty, nama_cabang, nama_jenis')
                        ->order_by('rec_created','DESC')
                        ->edit_column('rec_created', '$1', 'date::shortDate(rec_created)');
                        //->edit_column('shipping_id', '$1', 'view::btn_group_receiving_warehouse(shipping_id)');
                    echo $table->generate();
                }

                break;

            case 'target' :
                $this->twig->add_function('getMonthName');
                $param['form_action'] = form_open(
                                            site_url($this->class_name.'/proses_target/'.$id),
                                            '',
                                            array(
                                                'id' => $id,
                                            ));
                // options untuk list year
                $options['htmlOptions'] = $htmlOptions            =  'id="list-year" target="#form-target-part" data-source="'.site_url($this->class_name.'/form_target/'.$id).'" onchange="refreshList(this)" class="form-control"';

                $param['form_part']     = $this->getForm('target-part', $id);
                $param['list']['year']  = view::dropdown_year($temp['year_month'], 2, $options);
                $form->param = $param;
                $form->url_form     = $this->class_name.'/form_target.tpl';
                return $form->generate();
                break;

            default:
                return '';
                break;
        }
    }

    public function replenish($def='',$date=""){
        //$user_data = array_merge($temp_user_data,array("hari" => $date));
        $this->session->set_userdata('sistem_date',$date);
        $temp_user_data = $this->session->all_userdata();
        if($date != date('Y-m-d'))
        echo json_encode(1);
    }


}

/* End of file bbm.php */
/* Location: ./application/controllers/bbm.php */

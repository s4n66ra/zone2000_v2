<?php

/**
 * Description of bbm
 *
 * @author SANGGRA HANDI
 */
class bundle extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('transaction_bundle_model');
    }

    public function index() {
        $data['title'] = 'PURCHASE REQUEST LIST';
        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'base/page_content.tpl';     
        $data['content_body'] = $this->class_name.'/data_grid.tpl';
        //$data['content'] = $this->class_name.'/page_draft.tpl';

        $data['data_source_page_2'] = site_url($this->class_name.'/draft');
        $data['url_action_group'] = site_url($this->class_name.'/proses_draft');
        $this->twig->display('index.tpl', $data);
    }

    public function load(){
        
    }


}



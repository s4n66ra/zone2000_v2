<?php

/**
 * Description of bbm
 *
 * @author SANGGRA HANDI
 */
class supplier extends MY_Controller {

    public function __construct() {
        $this->formConfig = array('full-width' => 1);
        parent::__construct();
    }

    public function test(){
        $this->db->trans_start();
        $this->load->model(array('item_model','item_supplier_model','item_supplier_default_model'));
        $item = $this->item_model->get()->result_array();
        $supplier = $this->supplier_model->get()->result_array();

        foreach ($item as $key => $value) {
            // insert to item default
            $tes['item_id'] = $value['item_id'];
            $tes['supplier_id'] = ($value['item_id']%4 )+1;
            $this->item_supplier_default_model->create($tes);

            // insert to item supplier
            foreach ($supplier as $k => $v) {
                // insert to item supplier
                $a['supplier_id'] = $v['id_supplier'];
                $a['item_id'] = $value['item_id'];
                $this->item_supplier_model->create($a);
            }

        }

        $this->db->trans_complete();


    }

    public function index() {

        $data['title'] = 'Master Supplier';        

        $data['button_group'] = $this->getAvailableButtons();
        $data['button_right'] = $this->getTools();

        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'base/page_content.tpl'; 
        $data['nama_pegawai'] = $this->currentUsername;    
        
        $data['table']['main'] = $this->table_main(array('wrapper', 'filter'));
        $data['st_extend2'] = TRUE;
        $this->twig->display('index.tpl', $data);

    }

    public function table_main($option = array()){
        $this->load->library('Datatable');
        $table = $this->datatable;
        $this->load->helper('hgenerator_helper');
       
            //->edit_column('id_supplier', '$1', 'view::btn_group_edit_delete_page(id_supplier)');

        if (in_array('filter', $option)) {
            $table->dataFilter = array(
                                array('kd_supplier', 'Supplier Code', 'text'),
                                array('nama_supplier', 'Supplier Name', 'text'),
                                array('pemilik', 'Owner', 'text'),
                                array('telp', 'Supplier Telp', 'text'),
                                array('alamat_supplier', 'Supplier Address', 'text'),
                                array('username','Username','text'),
                                array('nama_divisi_supplier','Divisi','text'),

                                //array('this.hadiahtype_id', 'Type', 'list', $this->hadiah_type_model->options()),
                            );
        }
        if (in_array('wrapper', $option)) {
            $table->id         = 'table-supplier';
            $table->numbering  = true;
            $table->isScrollable = false;
            $table->header     = array('SUPPLIER CODE', 'SUPPLIER NAME','UNIT BUSINESS','DIVISI','OWNER','TELP', 'ADDRESS','USERNAME', 'ACTION');
            $table->source     = site_url($this->class_name.'/table_main');
            return $table->generateWrapper();
        } else 
            $table
                ->setModel($this->supplier_model)
                ->with(array('user','divisi'))
                ->setNumbering()
                ->select('kd_supplier, nama_supplier,unit_business,nama_divisi_supplier,pemilik,telp,alamat_supplier,user_name, id_supplier')
                ->order_by('urutan','kd_supplier')
                ->edit_column('id_supplier', '$1', 'view::btn_group_supplier(id_supplier)')
                ->edit_column('unit_business', '$1', 'view::getUnitBusiness(unit_business)');
            echo $table->generate();

    }

    public function add($id='',$status_delete = 0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->config->load('twig');
            $this->load->model('supplier_model');
            $this->load->model('divisi_supplier_model');

            $title = 'Tambah Data';
            $data['form_action'] = $this->class_name . '/proses';

            $data['id'] = $id;
            if ($id != '') {
                $title = 'Edit Data';
                $row = $this->supplier_model->get_by_id($id)->row();
                $data['data'] = $row;
                $data['additional_info'] = "<span>Keep it blank if you don't want to change password<span>";
            
            }

            $data['title']      = $title;
            $data['assets_url'] = $this->config->item('assets_url');
            $data['sidebar']    = $this->access_right->menu();
            $data['form']       = 'supplier/form_supplier.tpl';

            $button_group   = array();
            $button_group[] = view::button_back();

            if($status_delete == 0){
                $data['form_action']    = view::form_input($id);
                $button_group[]         = view::button_save();
            }else{
                $data['title']          = 'Delete Data supplier';
                $data['form_action']    = view::form_delete($id);;
                $data['readonly']       = 'readonly=""';
                $button_group[]         = view::button_delete_confirm();
            }
            $htmlOptions            =  'class="form-control select2me" data-placeholder="Select Location"'.$disabled;
            $data['list_divisi']  = form_dropdown('data[id_divisi_supplier]', $this->divisi_supplier_model->options(), $row->id_divisi_supplier, $htmlOptions);

            $data['options_unitBusiness'] = form_dropdown('data[unit_business]', $this->supplier_model->options_unitBusiness(), $row->unit_business, 'class="bs-select"');

            $data['button_group'] = view::render_button_group($button_group);
            $this->twig->display('base/page_form.tpl', $data);
        } else {
            $this->access_right->redirect();
        }
    }

    public function edit($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('edit', true);
        $this->add($id);
    }

    public function delete($id_enc) {        
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('delete', true);
        $this->add($id,1);
    }

    public function proses() {        
        
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('data[kd_supplier]', 'Supplier Code', 'required|trim');
            $this->form_validation->set_rules('data[nama_supplier]', 'Supplier Name', 'required|trim');
            $id = $this->input->post('id');
            $data = $this->input->post('data');
           // $this->form_validation->set_rules('data[alamat_supplier]', 'Supplier Address', 'required|trim');
            if($id==''){
                $this->form_validation->set_rules('data[user_name]', 'Username', 'required|trim|is_unique[m_user.user_name]');
                if(strlen($data['user_password']) > 0){
                    $this->form_validation->set_rules('pass', 'Password', 'trim|matches[confirm]');
                    $this->form_validation->set_rules('confirm', 'Confirm Password', 'trim');
                }
            }else{
                $this->form_validation->set_rules('data[user_name]', 'Username', 'edit_unique[m_user.user_name.'.$data['user_id'].'.user_id]');
                if(strlen($data['user_password']) > 0){
                    $this->form_validation->set_rules('pass', 'Password', 'trim|matches[confirm]');
                    $this->form_validation->set_rules('confirm', 'Confirm Password', 'trim');
                }
            }
            if ($this->form_validation->run()) {
                $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');

                

                
                $data['grup_id'] = hprotection::getSupllierGroupId();

                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */

                if ($id == '') {
                    if ($this->supplier_model->create($data)) {
                        $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh_filter()');
						/* INSERT LOG */
						$this->access_right->activity_logs('add','Tambah Supplier');
						/* END OF INSERT LOG */
                    }
                } else {
                    if ($this->supplier_model->update($data, $id)) {
                        $message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', 'refresh_filter()');
						/* INSERT LOG */
						$this->access_right->activity_logs('edit','Edit Supplier');
						/* END OF INSERT LOG */
                    }
                }
            } else {
                $message = array(false, 'Terjadi Kesalahan', validation_errors(), '');
            }
            echo json_encode($message);
        } else {
            $this->access_right->redirect();
        }
    }

    public function proses_delete() {
        $id = $this->input->post('id');
		$this->access_right->otoritas('delete', true);
        if ($this->access_right->otoritas('delete', true)) {
            $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');
            if ($this->supplier_model->delete($id)) {
                $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', 'refresh_filter()');
				/* INSERT LOG */
				$this->access_right->activity_logs('delete','Delete BBM');
				/* END OF INSERT LOG */
            }
            echo json_encode($message);
        }
    }

    public function proses_delete_detail($id = NULL){
        if(!$id)
            $id = $this->input->post('id');
        if(!$id)
            return;

        $this->db->trans_start();
        $this->load->model('item_supplier_model');
        $this->item_supplier_model->delete($id);

        if($this->db->trans_status()){
            $message = array(
                        'success'=>true,
                        'message'=>'Save Success',
                        'callback'=>'refreshDataTable()',
                        );
        }else{
            $message = array(
                        'success'=>false,
                        'message'=>'Database Error'
                        );
        }
        $this->db->trans_complete();
        echo json_encode($message);
    }

    public function detail($id){

        echo $this->getForm('page-detail', $id);
    }

    public function edit_detail($id) {
        echo $this->getForm('form-detail', $id);
    }

    public function table_detail($id){
        echo $this->getForm('table-detail', $id);
    }

    public function proses_detail(){
        $id = $this->input->post('id');
        $supplier_id = $this->input->post('supplier_id');
        $data = $this->input->post('data');

        $this->load->model('item_supplier_model');

        $this->db->trans_start();
        $message = array('success'=>false, 'message'=> 'Update Failed');

        if (!($data && $supplier_id)){
            echo json_encode($message);
            return;
        }

        $itemSupplier = $this->item_supplier_model
                            ->where('supplier_id', $supplier_id)
                            ->where('item_id', $data['item_id'])
                            ->get()->row_array();

        if ($itemSupplier) {
            // edit
            $this->item_supplier_model->update($data, $itemSupplier['itemsupplier_id']);
        } else {
            // insert New
            $data['supplier_id'] = $supplier_id;
            $this->item_supplier_model->create($data);
        }

        if($this->db->trans_status()){
            $message = array('success'=>true, 'message'=> 'Update Success', 'callback'=>"refreshNewCustomTable('#table-supplier-detail')");
        }else{
            $message = array('success'=>true, 'message'=> 'Database Error');
        }
        
        $this->db->trans_complete();
        echo json_encode($message);

    }

    public function getForm($type = NULL, $id = NULL, $param = array()){
        switch ($type) {
            case 'page-detail':
                $this->load->model(array('supplier_model'));
                $modelSupplier = $this->supplier_model;
                $dataSupplier = $modelSupplier->select('nama_supplier,kd_supplier')->where('id_supplier',$id)->get()->row();
                //print_r($dataSupplier);
                $data['title']         = $dataSupplier->nama_supplier.' ('.$dataSupplier->kd_supplier.') SUPPLIER ITEM ';
                $data['isBtnPageBack'] = true;
                $data['form']['main']  = $this->getForm('form-detail',NULL, array('wrapper', 'supplier_id'=>$id));
                $data['table']['main'] = $this->getForm('table-detail', $id, array('wrapper'));
                $this->twig->display('base/page_content.tpl', $data);
                break;

            case 'table-detail':
                $this->load->model('item_supplier_model');
                $this->load->library('Datatable');
                $table = $this->datatable;
/*                if (in_array('filter', $param)) {
                    $table->dataFilter = array(
                                        array('item_key', 'SKU', 'text'),
                                        array('item_name', 'ITEM NAME', 'text'),
                                        array('price', 'PRICE', 'text'),
                                        array('sup_price_pax', 'PRICE/PAX ', 'text'),
                                        
                                        //array('this.hadiahtype_id', 'Type', 'list', $this->hadiah_type_model->options()),
                                    );
                }*/
                //$table->pagination = false;
                if (in_array('wrapper', $param)) {
                    $table->numbering  = true;
                    $table->btn_refresh= false;
                    $table->id         = 'table-supplier-detail';
                    $table->isScrollable = false;
                    $table->header     = array('SKU', 'TYPE', 'ITEM NAME', 'PRICE','PRICE/PAX','DISKON (%)', 'ACTION');
                    $table->source     = site_url($this->class_name.'/table_detail/'.$id);
                    return $table->generateWrapper();
                } else {
                    $table->setNumbering()
                        ->setModel($this->item_supplier_model)
                        ->with(array('item'))
                        ->select('item_key, itemtype_id, item_name, this.price,sup_price_pax,reg_diskon, itemsupplier_id')
                        ->order_by('itemtype_id')
                        ->where('supplier_id', $id)
                        ->edit_column('itemsupplier_id', '$1', 'view::btn_group_supplier_detail(itemsupplier_id)')
                        ->edit_column('price', '$1', 'hgenerator::number(price)')
                        ->edit_column('sup_price_pax', '$1', 'hgenerator::number(sup_price_pax)')
      //                  ->edit_column('reg_diskon', '$1', 'hgenerator::number(reg_diskon)')
                        ->edit_column('itemtype_id', '$1', 'transaksi::getItemType(itemtype_id)');

                    return $table->generate();
                }
                break;
            
            case 'form-detail' :
                $this->load->model(array('item_model','item_supplier_model'));
                $this->load->library('custom_form_supplier');
                $form = $this->custom_form_supplier;

                if($id){
                    $row = $this->item_supplier_model->with('item')->getById($id)->row();
                    $param['supplier_id'] = $row->supplier_id;
                }
                $param['list']['item'] = '<input type="text" id="supplier_list_item_id" name="data[item_id]" class="bs-select form-control" data-live-search="true" value="'.$row->item_id.'"/>';


                $param['data']      = $row;
                $form->param        = $param;
                $form->form_open    = form_open(
                                            site_url($this->class_name.'/proses_detail'),
                                            '',
                                            array('id'=>$id, 'supplier_id'=>$param['supplier_id'])
                                        );
                $form->id           = 'form-supplier-detail';
                $form->portlet      = false;
                $form->buttonInForm = true;
                $form->url_form     = $this->class_name.'/form_detail.tpl';
                if(in_array('wrapper', $param)){
                    //echo 1212;
                    // generate with wrapper
                    return $form->generate();
                }
                else{
                    //echo 1313;
                    // generate only form
                    //print_r($param);
                    return $form->generateOnlyForm();
                }
                break;

            default:
                # code...
                break;
        }
    }

    public function getlist($type="", $id){
        $this->load->model(array('item_model'));
        switch ($type) {
            case 'item':
                if($id)
                    $data = $this->item_model->where('itemtype_id', $id)->get()->result_array();
                else
                    $data = $this->item_model->get()->result_array();
                $content = '';
                foreach ($data as $key => $value) {
                    $content.= '<option value="'.$value['item_id'].'">'.$value['item_key'].' - '.$value['item_name'].'</option>';
                }
                break;
            default:
                # code...
                break;
        }

        if(!$data)
            return;

        

        echo $content;
    }

    public function getAllItem($def_id = null){
        $this->load->model(array('item_model'));

        $tmp = $this->item_model->getAllItem();
        
        //$tmp = $this->item_model->getItemsByIdAndKeyword(2,'');
        $arr = array();
        foreach ($tmp as $k => $v) {
            if($k != ''){
                $obj = new StdClass();
                $obj->id        = $k;
                $obj->text      = $tmp[$k];
                array_push($arr, $obj);
            }
        }
        echo json_encode($arr);
    }

    public function getNamaItem($id){
        $this->load->model(array('item_supplier_model'));
        $row = $this->item_supplier_model
            ->with(array('item'))
            ->where('itemsupplier_id',$id)
            ->get()->row()->item_name;
        echo json_encode($row);
    }




}

/* End of file bbm.php */
/* Location: ./application/controllers/bbm.php */

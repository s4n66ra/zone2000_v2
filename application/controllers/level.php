<?php

/**
 * Description of bbm
 *
 * @author SANGGRA HANDI
 */
class level extends MY_Controller {

    public function __construct() {
        // Declaration
        parent::__construct();
    }

    public function index() {
        $data['title'] = 'Level';
        $data['data_source'] = base_url($this->class_name . '/load');
        /* INSERT LOG */
        $this->access_right->activity_logs('view',$data['title']);
        /* END OF INSERT LOG */

        $data['button_group'] = $this->getAvailableButtons();
        $data['button_right'] = $this->getTools();

        $data['assets_url'] = $this->config->item('assets_url');
        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'base/page_content.tpl';
        $data['nama_pegawai'] = $this->currentUsername;
    
        $this->twig->display('index.tpl', $data);
    }

    public function load($page = 0) {
    	$this->load->library("custom_table");
    	
    	$table = new stdClass();
		$header [0] = array (
				"KODE LEVEL",1,1,
				"NAMA LEVEL",1,1 ,
                "HEAD SDM ONLY",1,1,
		);
		
    	if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
    		$header[0] = hgenerator::merge_array_raw(
    				$header[0],
    				array("ACTION",1,1)
    		);
    	}
    	
    	$table->header = $header;
    	$table->id     = 't_bbm';
    	$table->style = "table table-striped table-bordered table-hover datatable dataTable";
    	$table->model = "level_model->data_table";
    	$table->limit = $this->limit;
    	$table->page = $page;
    	$data = $this->custom_table->generate_ajax($table);
    	
    	echo json_encode($data);
    }

    public function add($id = '',$status_delete = 0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->config->load('twig');
            $this->load->model('level_model');

            $title = 'Add Level';
            $data['id'] = $id;
            if ($id != '') {
                $title = 'Edit Level';

                $db = $this->level_model->get_by_id($id);
                if ($db->num_rows() > 0) {
                    $row = $db->row();
                    $data['default'] = $row;
                    $data['def_kd_level'] = $row->kd_level;
                    $data['def_nama_level'] = $row->nama_level;
                }
            }
            //echo $id;
            $data['title'] = $title;
            $data['assets_url'] = $this->config->item('assets_url');
            $data['sidebar'] = $this->access_right->menu();
            
            $button_group   = array();
            $button_group[] = view::button_back();
            if($status_delete == 0){
                $data['form_action']    = view::form_input($id);
                $button_group[]         = view::button_save();
            }else{
                $data['title']          = 'Delete Employee';
                $data['form_action']    = view::form_delete($id);;
                $data['readonly']       = 'readonly=""';
                $button_group[]         = view::button_delete_confirm();
            }
            $htmlOptions =  'class="form-control select2me"';
            $options_head_sdm_only = array('0'=>'No','1'=>'Yes');
            $data['head_sdm_only']  = form_dropdown('head_sdm_only', $options_head_sdm_only, $row->head_sdm_only, $htmlOptions.' data-placeholder="Pilih Posisi"');
            $data['button_group'] = view::render_button_group($button_group, array('class'=>'pull-right'));
            $data['form'] = 'level/form_level.tpl';
            $this->twig->display('base/page_form.tpl', $data);
        } else {
            $this->access_right->redirect();
        }
    }

    public function edit($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('edit', true);
        $this->add($id);
    }

    public function delete($id_enc = '') {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('delete', true);
        $this->add($id,$status_delete = 1);
    }

    public function proses() {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('kd_level', 'Kode level', 'required|trim');
            $this->form_validation->set_rules('nama_level', 'Nama level', 'required|trim');


            if ($this->form_validation->run()) {
                $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');

                $id = $this->input->post('id');

                $data = array(
                    'kd_level' => $this->input->post('kd_level'),
                    'nama_level' => $this->input->post('nama_level'),
                    'head_sdm_only' => $this->input->post('head_sdm_only')
                );

                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */

                if ($id == '') {
                    if ($this->level_model->create($data)) {
                        $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh_filter()');
						/* INSERT LOG */
						$this->access_right->activity_logs('add','Tambah BBM');
						/* END OF INSERT LOG */
                    }
                } else {
                    if ($this->level_model->update($data, $id)) {
                        $message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', 'refresh_filter()');
						/* INSERT LOG */
						$this->access_right->activity_logs('edit','Edit BBM');
						/* END OF INSERT LOG */
                    }
                }
            } else {
                $message = array(false, 'Terjadi Kesalahan', validation_errors(), '');
            }
            echo json_encode($message);
        } else {
            $this->access_right->redirect();
        }
    }

    public function proses_delete() {
        $id = $this->input->post('id');
		$this->access_right->otoritas('delete', true);
        if ($this->access_right->otoritas('delete', true)) {
            $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');
            if ($this->level_model->delete($id)) {
                $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', 'refresh_filter()');
				/* INSERT LOG */
				$this->access_right->activity_logs('delete','Delete BBM');
				/* END OF INSERT LOG */
            }
    		echo json_encode($message);
        }
    }
}

/* End of file bbm.php */
/* Location: ./application/controllers/bbm.php */

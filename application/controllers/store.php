<?php

/**
 * Description of bbm
 *
 * @author SANGGRA HANDI
 */
class store extends MY_Controller {

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->load->model('cabang_model');
        $this->load->model('store_data_model');

        $this->button['add'] = view::button_add(array('onclick'=>"btnLoadNextPage(this)"));
    }

    public function test(){
        $this->twig->display('index_dropdown.tpl');
        
    }
    
    public function index() {

        $data['title'] = 'Store Master';

        /* INSERT LOG */
        //$this->access_right->activity_logs('view',$data['title']);
        /* END OF INSERT LOG */

        $data['button_group'] = $this->getAvailableButtons();
        $data['button_right'] = $this->getTools();

        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'base/page_content.tpl';

        $data['table']['main']  = $this->table_main(array('wrapper', 'filter'));
            
        $this->twig->display('index.tpl', $data);
    }

    public function table_main($option = array()){
        $this->load->library('Datatable');
        $table = $this->datatable;
        $table->isScrollable = false;
        
        $table->numbering  = true;
        if (in_array('filter', $option)) {
            $table->dataFilter = array(
                array('nama_cabang', 'Store Name', 'text'),
                array('nama_kota', 'City', 'text'),
                array('telp', 'Phone', 'numeric'),
                array('date_start', 'Date Start', 'range-date'),
                array('store_area', 'Area', 'range-numeric')
            );
        }
        if (in_array('wrapper', $option)) {
            $table->numbering  = true;
            $table->id         = 'table-store';
            $table->header     = array('STORE CODE', 'STORE NAME', 'CITY','AREA', 'PHONE','TARGET (THIS MONTH)', 'ACTION');
            $table->source     = site_url($this->class_name.'/table_main');
            return $table->generateWrapper();
        } else 
        {
            $date = date('Y-m-01');
            //echo $date;
            $table->numbering = true;
            $table
            ->setModel($this->store_model)
            ->setNumbering()
            ->setScrollable(false)
            ->with(array('setting', 'area', 'city','target'))
            ->where('(date = "'.$date.'" or date IS NULL)',NULL)
            ->select('kd_cabang, nama_cabang ,nama_kota, nama_area, telp,target.target, this.id_cabang')
            ->edit_column('target','$1','hgenerator::number(target)')
            ->edit_column('id_cabang', '$1', 'view::btn_group_edit_delete_page(id_cabang)');
            echo $table->generate();
        }
            

    }

    public function load($page = 0) {

        $this->load->library("custom_table");

        $table = new stdClass();
        $header[0] = array(
                            "STORE ID",1,1,
                            "STORE NAME", 1, 1,
                            "COUNTRY",1,1,
                            "ISLAND", 1, 1,
                            "PROVINCE",1,1,
                            "CITY", 1, 1,
                            "TERITORY",1,1,
                            "AREA", 1, 1,
                            "OWNER", 1, 1

                        );

        if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
            $header[0] = hgenerator::merge_array_raw(
                            $header[0],
                            array("ACTION",1,1)
                        );
        }

        $table->header = $header;
        $table->id     = 't_store';
        $table->align = array('tanggal_bbm' => 'center', 'kd_bbm' => 'center','kd_purchase_order'=>'center','supplier'=>'center', 'aksi' => 'center');
        $table->style = "table table-striped table-bordered table-hover datatable dataTable";
        $table->model = "store_model->data_table";
        $table->limit = $this->limit;
        $table->page = $page;
        $data = $this->custom_table->generate_ajax($table);
        echo json_encode($data);
    }

    public function adds($id = '', $status_delete=0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->load->model('area_model');
            $this->load->model('cabang_model');
            $this->load->model('pegawai_model');
            $this->load->model('jabatan_model');
            $this->load->model('level_model');

            // load model
            $this->load->model(array(
                            'pulau_model',
                            'provinsi_model',
                            'area_model',
                            'negara_model',
                            'kota_model',
                            'owner_model'
                        ));

            $title = 'Tambah Data';
            $data['form_action'] = $this->class_name . '/proses';

            $data['id'] = $id;
            if ($id != '') {
                $title = 'Edit Data';
                $row = $this->store_model->get_by_id($id)->row();
                $data['data'] = $row;

                $storedata = $this->store_data_model
                                ->select('storedata_id')->where('store_id', $id)
                                ->order_by('date','desc')->limit(1)
                                ->get()->row();
                $data['form_data'] = $this->getForm('data', array('store_id'=>$id, 'storedata_id' => $storedata->storedata_id));
                $data['table_data']= $this->getTable('data', array('store_id'=>$id));
            }

            $data['title']      = $title;
            $data['assets_url'] = $this->config->item('assets_url');
            $data['sidebar']    = $this->access_right->menu();
            $data['form']       = $this->class_name.'/form.tpl';

            // dropdown island, province, city, area
            

            $button_group   = array();

            if($status_delete == 0){
                $data['form_action']    = view::form_input($id);
                $button_group[]         = view::button_save_form();
            }else{
                $data['title']          = 'Delete Store';
                $data['form_action']    = view::form_delete($id);;
                $data['readonly']       = ' readonly="" ';
                $button_group[]         = view::button_delete_confirm();
            }

            $data['button_group'] = view::render_button_group($button_group, array('class'=>'pull-right'));
            $data['isBtnPageBack']= true;

            

            $this->twig->display('base/page_form.tpl', $data);
        } else {
            $this->access_right->redirect();
        }
    }

    public function form($type='add',  $id = NULL){
        $data['title'] = 'Store';
        $data['content']['body'] = $this->getForm($type, $id);
        $data['isBtnPageBack']      = true;

        $this->twig->display('base/page_content.tpl', $data);
    }

    public function form_target($id, $year){
        $op['year'] = $year;
        echo $this->getForm('target-part', $id, $op);
    }

    public function add(){
        $this->form();
    }

    public function edit($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('edit', true);
        $this->form('edit', $id);
    }

    public function delete($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('delete', true);
        $this->form('delete',$id);
    }

    public function proses() {
        $data = $this->input->post('data');

        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {

            $this->form_validation->set_rules('data[nama_cabang]', 'Store Name', 'required|trim');
            $this->form_validation->set_rules('data[kd_cabang]', 'Store Code', 'required|trim');
            $this->form_validation->set_rules('data[id_kota]', 'City', 'required|trim');
            $this->form_validation->set_rules('data[id_area]', 'Area', 'required|trim');
            $this->form_validation->set_rules('data[id_owner]', 'Owner', 'required|trim');

            if ($this->form_validation->run()) {
                
                $this->db->trans_start();
                $id = $this->input->post('id');

                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */

                if ($id == '') {
                    $this->store_model->create($data);
                } else {
                    $this->store_model->update($data, $id);
                }

                if($this->db->trans_status()){
                    $message = array('success'=>true, 'message'=>'Save Success');
                }else{
                    $message = array('success'=>false, 'message'=>'Database Error');
                }

                $this->db->trans_complete();

            } else {
                $message = array('success'=>false, 'message'=>validation_errors());
            }
            echo json_encode($message);
        } else {
            $this->access_right->redirect();
        }
    }

    public function proses_delete() {
        $id = $this->input->post('id');
		$this->access_right->otoritas('delete', true);
        if ($this->access_right->otoritas('delete', true)) {
            $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');
            if ($this->cabang_model->delete($id)) {
                $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', 'refresh_filter()');
				/* INSERT LOG */
				$this->access_right->activity_logs('delete','Delete BBM');
				/* END OF INSERT LOG */
            }
            echo json_encode($message);
        }
    }

    public function proses_target($store_id){
        $post = $this->input->post();
        $this->load->model('store_data_model');
        $storedata = $this->store_data_model;

        $this->db->trans_start();
        $year  = $post['year'];
        $targets  = $post['data']['month'];
        
        foreach ($targets as $month => $target) {
            $temp = $storedata
                    ->where("month(date) = ".$month)
                    ->where("year(date) = ".$year)
                    ->where("store_id",$store_id)
                    ->get()->row_array();

            if($temp){
                // edit
                $storedata
                    ->set('target', $target)
                    ->where('storedata_id', $temp['storedata_id'])
                    ->update();
            }else{
                // new 
                $data['store_id']   = $store_id;
                $data['date']       = $year.'-'.$month.'-1';
                $data['target']     = $target;
                $storedata->create($data);
                
            }

            //echo $this->db->last_query();
        }

        if($this->db->trans_status()){
            $return['success'] = true;
            $return['message'] = 'Save Data Succes';
            
        }else{
            $return['success'] = false;
            $return['message'] = "Database Error";
        }

        $this->db->trans_complete();
        echo json_encode($return);
    }

    public function proses_area(){
        $post = $this->input->post();
        $datalength = count($post['data']);
        //print_r($post);
        $this->load->model(array(
                            'history_store_area_model',
                            'history_model',
                            'store_setting_model',
                        ));
        $modelHistory = $this->history_model;
        $modelHistoryArea = $this->history_store_area_model;
        $storeSetting = $this->store_setting_model;

        $this->db->trans_start();
        $data       = $post['data'];
        $store_id   = $post['store_id'];

        // harusnya validasi disini

        if($datalength > 2){
            //cek apakah sudah ada di cabang setting atau blm, kalo belum dia insert
            if(count($storeSetting->where('id_cabang',$store_id)->get()->result_array())==0){
                $datanstoreid= array_merge($data,array('id_cabang' => $store_id));
                $storeSetting->create($datanstoreid);
            }else{
                // Update cabang_setting
                $data['percentage_ps']=0;
                $storeSetting->where('id_cabang', $store_id)->update(array_merge($data,array('status_ps'=>0)));                
            }

        }else{
            
            if(count($storeSetting->where('id_cabang',$store_id)->get()->result_array())==0){
                $datanstoreid= array_merge($data,array('id_cabang' => $store_id, 'status_ps' => 1));
                $storeSetting->create($datanstoreid);
            }else{
                // Update cabang_setting
                $data['rent_charge']=NULL;
                $data['service_charge']=NULL;
                $data['rent_date_start']=NULL;
                $data['rent_date_end']=NULL;

                $storeSetting->where('id_cabang', $store_id)->update(array_merge($data,array('status_ps'=>1)));                

            }

        }

        // Create history
        $tmp = array();
        $tmp['store_id'] = $store_id;
        $tmp['rec_user'] = $this->session->userdata('user_id');
        $tmp['historytype_id'] = 10; //history store_area
        $modelHistory->create($tmp);
        $history_id = $modelHistory->insert_id;
        
        // Create history_store_area
        $data['history_id'] = $history_id;
        $modelHistoryArea->create($data);


        if($this->db->trans_status()){
            $return['success'] = true;
            $return['message'] = 'Save Data Succes';
            $return['callback']= "loadNewCustomTable('#div-table-area')";
            
        }else{
            $return['success'] = false;
            $return['message'] = "Database Error";
        }

        $this->db->trans_complete();
        echo json_encode($return);
    }

    public function getForm($type = 'add', $id=NULL, $param = array(),$st_delete=0){
        $this->load->library('custom_form');
        $this->load->library('custom_table');
        $this->load->helper('form');
        $this->load->model(array('store_data_model'));

        $storedata = $this->store_data_model;

        $form       = new custom_form();
        $table      = new custom_table();

        switch ($type) {
            case 'add' :
                $data['act'] = 'add';
                $data['form']['main'] = $this->getForm('main', $id);
                $data['form']['target'] = $this->getForm('target', $id);
                $data['form']['area'] = $this->getForm('area', $id);
                return $this->twig->render($this->class_name.'/page_form.tpl', $data);
                break;

            case 'edit' :
                $data['form']['main']   = $this->getForm('main', $id);
                $data['form']['target'] = $this->getForm('target', $id);
                $data['form']['area']   = $this->getForm('area', $id);
                return $this->twig->render($this->class_name.'/page_form.tpl', $data);
                break;

            case 'delete':
                $data['form']['main']   = $this->getForm('main',$id,array(),1);
                $data['form']['target'] = $this->getForm('target', $id);
                $data['form']['area']   = $this->getForm('area', $id);
                $data['title']          = 'Delete Province Data';
                
                $data['readonly']       = 'readonly=""';
                
                return $this->twig->render($this->class_name.'/page_form.tpl', $data);
                break;

            case 'main' :
                $this->load->model(array(
                            'pulau_model',
                            'provinsi_model',
                            'area_model',
                            'negara_model',
                            'kota_model',
                            'owner_model'
                        ));
                //$row = $this->store_model->getById($id)->row();
                $row = $this->store_model->get_by_id($id)->row();
                $htmlOptions            =  'id="list-country" target="#list-island" data-source="'.site_url($this->class_name.'/getlist/island').'" onchange="refreshList(this)" class="form-control select2" data-placeholder="Select Country"';
                $data['list']['country']   = form_dropdown('id_negara', $this->negara_model->options_empty(), $row->id_negara, $htmlOptions);

                $htmlOptions            =  'id="list-island" target="#list-province" data-source="'.site_url($this->class_name.'/getlist/province').'" onchange="refreshList(this)" class="form-control select2" data-placeholder="Select Island"';
                $data['list']['island']    = form_dropdown('island', $this->pulau_model->options_empty(), $row->id_pulau, $htmlOptions);

                $htmlOptions            =  'id="list-province" target="#list-city" data-source="'.site_url($this->class_name.'/getlist/city').'" onchange="refreshList(this)" class="form-control select2" data-placeholder="Select Province"';
                $data['list']['province']  = form_dropdown('provinsi', $this->provinsi_model->options_empty(), $row->id_provinsi, $htmlOptions);

                $htmlOptions            =  'id="list-city" target="#list-location" data-source="'.site_url($this->class_name.'/getlist/location').'" onchange="refreshList(this)" class="form-control select2" data-placeholder="Select City"';
                $data['list']['city']      = form_dropdown('data[id_kota]', $this->kota_model->options_empty(), $row->id_kota, $htmlOptions);

/*                $htmlOptions            =  'id="list-location" class="form-control select2" data-placeholder="Select Location"';
                $data['list']['location']  = form_dropdown('data[id_lokasi]', $this->lokasi_model->options_empty(), $row->id_kota, $htmlOptions);
*/
                $htmlOptions            =  'id="list-area" class="form-control select2" data-placeholder="Select Area"';
                $data['list']['area']      = form_dropdown('data[id_area]', $this->area_model->options_empty(), $row->id_area, $htmlOptions);
                
                $htmlOptions            =  'id="list-owner" class="form-control select2" data-placeholder="Select Owner"';
                $data['list']['owner']     = form_dropdown('data[id_owner]', $this->owner_model->options_empty(), $row->id_owner, $htmlOptions);
                
                if($st_delete == 1){
                    $data['form_action']    = view::form_delete($id);
                    $button_group[]         = view::button_delete_confirm();
                    $data['button_group'] = view::render_button_group($button_group);
                }else{
                    $data['form_action']    = view::form_input($id);
                }
                $data['data']       = $row;
                $form->param        = $data;
                $form->url_form     = $this->class_name.'/form.tpl';
                return $form->generate();
                break;

            case 'data' :
                // Data Additional
                if($param['storedata_id']){
                    $temp = $storedata->select("this.*, month(date) as date_month, year(date) as date_year")
                                ->getById($param['storedata_id'])->row_array();
                    $param['data'] = $temp;

                }

                $param['form_action'] = form_open(
                                                site_url($this->class_name.'/proses_data/'.$param['store_id']),
                                                '', 
                                                array(
                                                    'store_id' => $param['store_id'],
                                                    'storedata_id' => $param['id'],
                                                )
                                            );
                $param['list_month']  = view::dropdown_month($temp['date_month']);
                $param['list_year']   = view::dropdown_year($temp['year_month']);
                $form->param = $param;
                $form->url_form     = $this->class_name.'/form_data.tpl';
                return $form->generate();
                break;

            case 'area' : 
                $param['form']['area'] = $this->getForm('form-area', $id);
                $param['table']['area'] = $this->getForm('table-area', $id, array('wrapper'));
                return $this->twig->render($this->class_name.'/page_area.tpl', $param);
                break;

            case 'form-area' : 
                $this->load->model('store_setting_model');
                $param['data'] = $this->store_setting_model->where('id_cabang', $id)
                                        ->get()->row();

                $htmlOptions            =  'id="storetype" class="form-control select2" data-placeholder="Select Store Type" onchange=storetypeaction()';

                $storetype = array();
                $storetype[''] = '';
                $storetype[0] = 'Rent Charge';
                $storetype[1] = 'Profit Sharing';               

                $param['list']['storetype']     = form_dropdown('storetype', $storetype, $param['data']->status_ps, $htmlOptions);
                $param['form_action'] = form_open(
                                                site_url($this->class_name.'/proses_area'),
                                                '', array('store_id'=>$id)
                                            );
                $form->param        = $param;
                $form->url_form     = $this->class_name.'/form_area.tpl';
                return $form->generate();
                break;

            case 'table-area' : 
                $this->load->model('history_store_area_model');
                $data = $this->history_store_area_model
                        ->select('rec_created, store_area, service_charge, rent_charge, percentage_ps, rent_date_start, rent_date_end')
                        ->with('history')
                        ->order_by('rec_created', 'desc')
                        ->where('store_id', $id)->get()->result_array();
                
                foreach($data as $key => $val){
                    $data[$key]['rent_period']      = $val['rent_date_start'];
                    $data[$key]['rec_created']      = date::longDate($val['rec_created']);
                    $data[$key]['store_area']       = view::format_number($val['store_area']);
                    $data[$key]['service_charge']   = view::format_number($val['service_charge']);
                    $data[$key]['rent_charge']      = view::format_number($val['rent_charge']);
                    $data[$key]['percentage_ps']    = view::format_number($val['percentage_ps']);
                    unset($data[$key]['rent_date_start']);
                    unset($data[$key]['rent_date_end']);
                }

                $table->columns = array('DATE', 'AREA', 'SERVICE CHARGE', 'RENT CHARGE', 'PROFIT SHARE', 'RENT PERIOD');
                $table->id      = 'table-area';
                $table->data    = $data;
                $table->data_source = site_url($this->class_name.'/getform/table-area/'.$id);

                if(in_array('wrapper', $param)){
                    //return $table->generate();
                    return $table->generateWithWrapper();
                }else{
                    echo $table->generate();
                }
                break;

            case 'target' :
                $this->twig->add_function('getMonthName');
                $param['form_action'] = form_open(
                                            site_url($this->class_name.'/proses_target/'.$id),
                                            '',
                                            array(
                                                'id' => $id,
                                            ));
                // options untuk list year
                $options['htmlOptions'] = $htmlOptions            =  'id="list-year" target="#form-target-part" data-source="'.site_url($this->class_name.'/form_target/'.$id).'" onchange="refreshList(this)" class="form-control"';

                $param['form_part']     = $this->getForm('target-part', $id);
                $param['list']['year']  = view::dropdown_year($temp['year_month'], 2, $options);
                $form->param = $param;
                $form->url_form     = $this->class_name.'/form_target.tpl';
                return $form->generate();
                break;

            case 'target-part' :
                $data = array();
                $yearNow = date('Y');

                if(!$param['year'])
                    $year = date('Y');
                else
                    $year = $param['year'];

                // pengaturan untuk view only
                if($year>$yearNow)
                    $data['month'] = -999;    
                elseif ($year<$yearNow)
                    $data['month'] = 999;
                else
                    $data['month'] = date('m');    
                
                // generate data target per bulan di suatu tahun
                $temp = $this->store_data_model
                                ->select('month(date) as month, target')
                                ->where('year(date) = '.$year)
                                ->where('store_id', $id)
                                ->get()->result();
                foreach ($temp as $value) {
                    $data['data'][$value->month] = $value->target;
                }

                return $this->twig->render($this->class_name.'/form_target_part.tpl', $data);
                break;

            case 'page-report':
                $param['report']['income']      = '';
                $param['report']['membership']  = '';
                $param['report']['employee']    = '';
                return $this->twig->render($this->class_name.'/page_report.tpl', $param);
                break;

            default:
                # code...
                break;
        }
    }

    public function getTable($type = '', $param = array()){
        $this->load->library("custom_table");
        $table = new custom_table();

        switch ($type) {
            case 'data':
                $data = $this->store_data_model
                        ->select("concat(monthname(date), year(date)), store_area, service_charge, target, rent_charge")
                        ->where('store_id', $param['store_id'])
                        ->order_by('date','desc')
                        ->get()->result_array();
                $header [0] = array (
                    "DATE",1,1,
                    "AREA",1,1,
                    "SERVICE",1,1,
                    "TARGET",1,1,
                    "RENT",1,1,
                );
                $table->header = $header;
                $table->id = 'table-store-data';
                $table->data = $data;
                $table->data_source  = site_url($this->class_name.'/table_data/'.$param['store_id']);
                return $table->generateWithWrapper();
                break;
            
            default:
                # code...
                break;
        }
        

    }

    public function table_data($store_id){
        echo $this->getTable('data', array('store_id'=>$store_id));
    }

    public function getlist($type="", $id){
        $this->load->model(array('pulau_model', 'negara_model', 'kota_model','lokasi_model','provinsi_model'));
        switch ($type) {
            case 'island':
                $data = $this->pulau_model->where('id_negara', $id)->get()->result_array();
                $labelId = 'id_pulau';
                $label = 'nama_pulau';
                break;

            case 'province':
                $data = $this->provinsi_model->where('id_pulau', $id)->get()->result_array();
                $labelId = 'id_provinsi';
                $label = 'nama_provinsi';
                break;

            case 'city':
                $data = $this->kota_model->where('id_provinsi', $id)->get()->result_array();
                $labelId = 'id_kota';
                $label = 'nama_kota';
                break;

            case 'location':
                $data = $this->lokasi_model->where('id_kota', $id)->get()->result_array();
                $labelId = 'id_lokasi';
                $label = 'nama_lokasi';
                break;
            
            default:
                # code...
                break;
        }

        if(!$data)
            return;

        $content = '';
        foreach ($data as $key => $value) {
            $content.= '<option value="'.$value[$labelId].'">'.$value[$label].'</option>';
        }

        echo $content;
    }


    //INI DIGUNAKAN UNTUK MENAMBAHKAN DI LIST USER
    public function getAllStores(){
        
        $tmp = $this->store_model
                ->select("id_cabang, kd_cabang, nama_cabang")
                ->get()
                ->result_array();

        //print_r();
        $arr = array();
        foreach ($tmp as $k => $v) {
            $obj = new StdClass();
            $obj->id        = $tmp[$k]["id_cabang"];
            $obj->text      = $tmp[$k]["nama_cabang"];
            array_push($arr, $obj);
        }
        echo json_encode($arr);
    }




}

/* End of file bbm.php */
/* Location: ./application/controllers/bbm.php */

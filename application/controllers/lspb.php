<?php

/**
 * Description of Divisi
 *
 * @author SANGGRA HANDI
 */
class lspb extends My_Controller {


    public function __construct() {
        parent::__construct();
        $this->load->model('lap_pegawai_model');
    }

    
    public function index() {
        $data['title'] = 'Laporan Selisih Penerimaan Barang';

        /* INSERT LOG */
        //$this->access_right->activity_logs('view',$data['title']);
        /* END OF INSERT LOG */

        //$data['button_group'] = $this->getAvailableButtons();
        $data['button_right'] = $this->getPrintTools();

        $data['table']['main']  = $this->table_main(array('wrapper', 'filter'));
        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'base/page_content.tpl';
    
        $this->twig->display('index.tpl', $data);
    }

    public function table_main($option = array(),$st_print = FALSE){
        $this->load->model('lap_pegawai_model');
        $this->load->library('Datatable');
        $table = $this->datatable;

        /*$this->load->library('Datatable_lapSDM');
        $table = $this->datatable_lapSDM;*/

        if (in_array('filter', $option)) {
            $table->dataFilter = array(
                                array('kd_divisi','NO SURAT JALAN','text'),
                                array('kd_cabang','STORE','text'),
                                array('nama_divisi', 'SUPPLIER', 'text'),
                                array('nik', 'KOLI', 'text'),
                                array('id', 'ITEM', 'text'),
                                array('nama_bagian_md', 'ARTIKEL', 'text'),
                                array('dc_qty_received', 'QTY', 'text'),
                                array('qty_received', 'DITERIMA', 'text'),
                                array('price', 'HARGA JUAL', 'text'),
                               
                            );
        }

        if (in_array('wrapper', $option)) {
                    $table->id          = 'table-koli-detail';
                    $table->isScrollable= false;
                    $table->checklist   = false;
                    $table->pagination  = false;
                    $table->header      = array('NO SURAT JALAN','STORE','TANGGAL SURAT JALAN','SUPPLIER','KOLI', 'ITEM','ARTIKEL','QTY', 'DITERIMA','HARGA JUAL','KURANG','LEBIH');
                    $table->source      = site_url($this->class_name.'/table_main/');
                    $table->caption_btn_group   = NULL;//'RECEIVE';
                    $table->url_action_group    = NULL;//site_url($this->class_name.'/proses_receiving');
                    $table->callback_group_submit = NULL;
                    //$table->add_javascript =  '#table-koli-detail.submit()';
                    return $table->generateWrapper();
                } else {
                    $this->load->model('shipping_model');
                    $table
                        ->setModel($this->shipping_model)
                        ->with(array('shipping_koli','koli','receiving','purchase_order','item','bagian_md', 'supplier','item_supplier','store'))
                        ->where('date_arrived IS NOT NULL',NULL)
                        ->where('this.type','1')
                        ->where('dc_qty_received - qty_received <> 0',NULL)
                        //->where('this.shipping_id',$id)
                        ->select('shipping_code, kd_cabang,this.date_created, nama_supplier,koli_code,item_name, nama_bagian_md, dc_qty_received,qty_received, item_supplier.price,dc_qty_received - qty_received as kurang, if(qty_received > dc_qty_received,qty_received - dc_qty_received,0 ) as lebih',false)
                        ->edit_column('date_created','$1','date::longDate(date_created)');
                        //->edit_column('podetail_id', '<input type="text" name="data[$1]" >','podetail_id');
                        //->edit_column('shipping_id', '<input type="checkbox" name="id[]" value="$1">','shipping_id');
                    if($st_print == TRUE)
                        return $table->getData();
                    else
                        echo $table->generate();
                }

    }

    public function add($id='',$status_delete = 0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->config->load('twig');
            $this->load->model('item_model');
            $title = 'Form Tambah Divisi';
            $data['form_action'] = $this->class_name . '/proses';

            $data['id'] = $id;
            if($id){
                $title = 'Form Edit divisi';
                $row            = $this->lap_pegawai_model->getById($id)->row();
                $data['data']  = $row;
            }

            $data['title']      = $title;
            $data['assets_url'] = $this->config->item('assets_url');
            $data['sidebar']    = $this->access_right->menu();
            $data['form']       = $this->class_name.'/form.tpl';

            $button_group   = array();
            $button_group[] = view::button_back();
            
            if($status_delete == 0){
                $data['form_action']    = view::form_input($id);
                $button_group[]         = view::button_save();
            }else{
                $data['title']          = 'Delete Divisi';
                $data['form_action']    = view::form_delete($id);;
                $data['readonly']       = 'readonly=""';
                $button_group[]         = view::button_delete_confirm();
            }

            $data['button_group'] = view::render_button_group($button_group);            
            $this->twig->display('base/page_form.tpl', $data);

        } else {
            $this->access_right->redirect();
        }
    }

    

    public function edit($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('edit', true);
        $this->add($id);
    }

    public function delete($id_enc = '') {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('delete', true);
        $this->add($id,$status_delete = 1);
    }

    public function proses() {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('data[nama_divisi]', 'Nama divisi', 'required|trim');
            $this->form_validation->set_rules('data[kd_divisi]', 'Kode divisi', 'required|trim');

            if ($this->form_validation->run()) {
                $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');

                $id = $this->input->post('id');
                $data = $this->input->post('data');
                $this->db->trans_start();
                if ($id){
                    $row            = $this->lap_pegawai_model->getById($id)->row();
                    $dataold  = $row;
                    $this->lap_pegawai_model->update($data, $id);
                }else{
                    $this->lap_pegawai_model->create($data);
                }
                if($this->db->trans_status()){
                    $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh_filter()');
                }else{
                    $message = array(false, 'Terjadi Kesalahan',$this->db->_error_message(), '');
                }
                $this->db->trans_complete();
            } else {
                $message = array(false, 'Terjadi Kesalahan', validation_errors());
            }
            echo json_encode($message);
        } else {
            $this->access_right->redirect();
        }
    }

    public function proses_delete() {
        $id=$this->input->post("id");
        $this->access_right->otoritas('delete', true);
        if ($this->access_right->otoritas('delete', true)) {
            $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');
            if ($this->lap_pegawai_model->delete($id)) {
                $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', 'refresh_filter()');
                /* INSERT LOG */
                $this->access_right->activity_logs('delete','Delete Divisi');
                /* END OF INSERT LOG */
            }
            echo json_encode($message);
        }
    }

    public function getPrintTools(){
        $button_group = array();
        if($this->access_right->otoritas('print')){
            $button_group[] = view::button_export_laporan();
        }
        
        if(true){//export
            //$button_group[] = view::button_export();
        }
        return view::render_button_group_laporan_penjualan($button_group, array(), true);

    }

    public function excel($store_id=''){

        $this->load->model(array('store_model','laporan_payout_model'));  
        /*$this->load->library('DatatablelapSDM');
        $table = $this->datatablelapsdm;
        $table  ->setModel($this->lap_pegawai_model)
                ->with(array('divisi','level'))
                ->setNumbering()
               // ->order_by('no_urut','ASC')
                ->order_by('this.id_divisi','DESC')
                ->select('nama_pegawai,nik,id_karyawan,kd_level,nama_divisi');
        $dataTable = $table->getData();    */  
        $filter = array();

        $data['filter'] = $filter;
        //----------------------------------------------------
        $date = date('Ymd His');
        $header =array('NO SURAT JALAN','STORE','TANGGAL SURAT JALAN','SUPPLIER','KOLI', 'ITEM','ARTIKEL','QTY', 'DITERIMA','HARGA JUAL','KURANG','LEBIH');

        $data['header'] = $header;
        $data['judul_kecil']    = 'Laporan Selisih Penerimaan Barang';
        $data['filename']       = 'Laporan Selisih Penerimaan Barang('.$date.')';
        $data['content']        = $this->table_main(array(),TRUE);// $this->laporan_payout_model->data_detail(array("id_cabang" => $store_id));
        $data['selected_store'] = $this->store_model
                                 ->select('nama_cabang')
                                 ->where(array("id_cabang"=>$store_id))
                                 ->get()
                                 ->row();
        //print_r($data['selected_store']);
        $this->load->view('cetak_excel',$data);
    
    }  

/*    public function getItemsByIdAndKeywordJSON($itemtype_id=''){
        $this->load->model(array('item_model'));

        $tmp = $this->item_model->getItemsByIdAndKeyword($itemtype_id);
        
        //$tmp = $this->item_model->getItemsByIdAndKeyword(2,'');
        $arr = array();
        foreach ($tmp as $k => $v) {
            if($k != ''){
                $obj = new StdClass();
                $obj->id        = $k;
                $obj->text      = $tmp[$k];
                array_push($arr, $obj);
            }
        }
        echo json_encode($arr);
    }*/





}

/* End of file divisi.php */
/* Location: ./application/controllers/bbm.php */

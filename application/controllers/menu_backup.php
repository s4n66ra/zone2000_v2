
<?php
class menu extends MY_Controller {

    
    private $title = 'Menu Aplikasi';

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        
        $data['title'] = $this->title;

        $data['button_group'] = array();
        $data['javascript']     = array('menu.js');
        $data['javascript_var'] = array(
                                        'url_action_reordering' => site_url($this->class_name.'/proses_reordering_new'),
                                    );
        $data['sidebar']        = $this->access_right->menu();
        $data['content']        = 'base/page_content.tpl';
        $data['button_group']   = view::render_button_group(array(view::button_add()));
        $data['button_right']   = '<a class="btn blue btn-save-order"> Save Order</a>';
        $data['table']['main']  = $this->table_main(array('wrapper'));
        $this->twig->display('index.tpl', $data);
    }

    private function recursiveRow($menu, $depth = 0){
        $body = '';
        $kd_menu        = $menu['kd_menu'];
        $kd_grup_menu   = $menu['kd_grup_menu'];
        $kd_parent      = $menu['kd_parent'];

        $group_id   = $kd_grup_menu;
        $classGroup = 'group-'.$kd_grup_menu;
        if($depth>0) {
            $parent_id  = ' parent-id='.$kd_parent.' ';
            $classChild = ' child ';
            $classGroup = $classGroup.'-'.$kd_parent;
        }

        if ($menu['child']){
            $classParent = ' parent ';
            // tombol untuk hide child
            $btnSlide    = '<a class="" style="margin-right:10px;"><i class="icon-slide fa fa-angle-down"></i></a>';
        }

        $body.='<tr id="menu-'.$kd_menu.'" menu-id="'.$kd_menu.'" depth='.$depth.' group="'.$classGroup.'" group-id="'.$group_id.'" menu-id="'.$kd_menu.'" '.$parent_id.
                ' class="'.$classGroup.' '.$classParent.' sortable '.$classChild.'">';            

        // indent
        $indent = ' style="padding-left:'.($depth*40).'px;" ';

        unset($menu['kd_menu']);
        unset($menu['kd_grup_menu']);
        unset($menu['kd_parent']);

        $firstCol = ' first-column ';

        $body.='<td></td>';                    
        foreach ($menu as $j => $td) {
            if($j!='child')
                $body.= '<td class="text-'.$align[$j].$firstCol.'" '.$indent.'>'.$btnSlide.$td.'</td>';
            $indent = '';
            $btnSlide = '';
            $firstCol = '';
        }
        $body.='</tr>';

        if($menu['child']){
            foreach ($menu['child'] as $key => $value) {
                $body.= $this->recursiveRow($value, $depth+1);
            }
            return $body;
        }
        else
            return $body;
    }

    public function table_main($options = array()){
        $menuModel = $this->menu_model;

        $deepestLevel = $menuModel->select('max(menu_level) as max_level')->get()->row()->max_level;

        // mendapatkan menu di setiap level
        $menuLevel = array();
        for($i=$deepestLevel; $i>=0 ;$i--){
            $temp = $menuModel->where('menu_level', $i)->order_by('menu_urutan')->get()->result_array();
            foreach ($temp as $k => $val) {
                $tmp = array();
                $tmp['name']        = view::icon($val['icon']).' ' .$val['nama_menu'];
                $tmp['deskripsi']   = $val['deskripsi'];
                $tmp['url']         = $val['url'];
                $tmp['action']      = view::btn_group_edit_delete($val['kd_menu']);
                $tmp['kd_menu']     = $val['kd_menu'];
                $tmp['kd_grup_menu']= $val['kd_grup_menu'];
                $tmp['kd_parent']   = $val['kd_parent'];
                $menuLevel[$i][$val['kd_menu']] = $tmp;
            }
        }

        // print_r($menuLevel[1]);die;

        // pengelompokan menu berdasarkan levelnya. 
        for($i=$deepestLevel; $i>=1 ;$i--){
            $temp = array();
            foreach ($menuLevel[$i] as $k => $menu) {
                $menuLevel[$i-1][$menu['kd_parent']]['child'][] = $menu;
                // print_r($menuLevel[$i-1][$menu['kd_parent']]);
                // die;
                // echo $menu['kd_parent'];die;
            }
        }

        $dataMenu = $menuLevel[0];        

        $dataGroup = $this->menu_group_model->order_by('urutan', 'asc')->get()->result_array();
        $groups = array();
        foreach ($dataGroup as $key => $value) {
            $groups[$value['kd_grup_menu']] = $value;
        }

        // pengelompokan berdasarkan grup menu
        foreach ($dataMenu as $key => $val) {
            $kd_grup_menu = $val['kd_grup_menu'];
            $groups[$kd_grup_menu]['menu'][] = $val;
        }

        $body.='<tbody>';
        foreach ($groups as $kd_grup_menu => $group) {
            $body.= '<tr class="unsortable group group-head" menu-id="'.$kd_grup_menu.'" group-id="'.$kd_grup_menu.'" group="group" group-name="'.$group['nama_grup_menu'].'"><td colspan="5">';
            $body.= '<a class="" style="margin-right:10px;"><i class="icon-slide fa fa-angle-down"></i></a>';
            $body.= view::icon($group['icon']).' '.$group['nama_grup_menu'];
            $body.= '</td></tr>';
            
            if($group['menu'])
                foreach ($group['menu'] as $i => $tr) {
                    $body.= $this->recursiveRow($tr);
                }
            
        }
        $body.='</tbody>';

        $this->load->library('custom_table');
        $table = $this->custom_table;
        $align = array('action'=>'center');
        $table->id = 'table-menu';
        $table->headerAlign = array( 3=> 'center');
        $table->columnWidth = array( 3=> 15);
        $table->columns = array('MENU', 'DESCRIPTION', 'URL', 'ACTION');
        $table->data_source = site_url($this->class_name.'/table_main');
        $table->body = $body;
        
        if (in_array('wrapper', $options))
            return $table->generateWithWrapper();
        else
            echo $table->generate();
    }

    public function load() {
        $this->load->model('menu_group_model');
        $this->load->model('menu_model');

        $menu_group = $this->menu_group_model->get_data();
        $menu = $this->menu_model->parsing_data();
        $table = '
<table id="table_list_pegawai" class="table table-bordered table-hover datatable dataTable">
    <thead>
        <tr>
            <th style="width: 50px;"><i class="icon-sitemap"></i></th>
            <th>Nama Menu</th>
            <th>Icon</th>
            <th>Dekripsi</th>
            <th>URL</th>
            <th>Aksi</th>
        </tr>
    </thead>';
    $total_group = $menu_group->num_rows();
    $idx_group = 0;
    foreach ($menu_group->result() as $group) :
        $idx_group++;
        $group_up = '';
        $group_down = '';
        if ($idx_group == 1) {
            $group_up = 'disabled="disabled"';
        } else if ($idx_group == $total_group) {
            $group_down = 'disabled="disabled"';
        }

        $table .= '<tr id="default_row_select">
            <td align="center"><img style="cursor: pointer;" src=" '.base_url().'assets/images/bawah.png" /></td>';
            $table .= '<td colspan="5" style="font-weight: bold;font-size: 12px;"><i class=" '.$group->icon.' "></i> &nbsp; '. $group->nama_grup_menu.'</td>
        </tr>
        <tbody id="menu_group_'.$group->kd_grup_menu.'">';
            
            if (isset($menu[$group->kd_grup_menu])) :
                if (isset($menu[$group->kd_grup_menu][0])) :
                    $total_menu_1 = count($menu[$group->kd_grup_menu][0]);
                    $idx_menu_1 = 0;
                    foreach ($menu[$group->kd_grup_menu][0] as $value) :
                        $idx_menu_1++;
                        $id = $value['kd_menu'];
                        $group_menu_1_up = '';
                        $group_menu_1_down = '';
                        if ($idx_menu_1 == 1) {
                            $group_menu_1_up = 'disabled="disabled"';
                        }
                        if ($idx_menu_1 == $total_menu_1) {
                            $group_menu_1_down = 'disabled="disabled"';
                        }
                        $id_enc = url_base64_encode($id);
                        $table .= '<tr>
                            <td align="center"><i class="icon-menu"></i></td>
                            <td style="color: blue;font-weight: bold;">'.$value['nama_menu'].'</td>
                            <td> <i class="'.$value['icon'].'"></i> </td>
                            <td> '.$value['deskripsi'].' </td>
                            <td> '.$value['url'].'</td>
                            <td align="center">';
                                
                                $action1 = '';
                                if ($this->access_right->otoritas('edit')) {
                                    //$action1 .= anchor(null, '<i class="icon-edit"></i>', array('id' => 'button-edit-' . $id, 'class' => 'btn', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('menu/edit/' . $id)));
                                    $action1 .= view::button_edit($id_enc);//'<a href="'.base_url("menu/edit/$id_enc").'" class="btn default btn-xs purple"><i class="fa fa-edit"></i> Edit </a>';
                                }
                                if ($this->access_right->otoritas('delete')) {
                                    //$action1 .= anchor(null, '<i class="icon-trash"></i>', array('id' => 'button-delete-' . $id, 'class' => 'btn', 'onclick' => 'delete_row(this.id)', 'data-source' => base_url('menu/delete/' . $id), 'style' => 'margin-left:5px;'));
                                    $action1 .= view::button_delete($id_enc);//$action1 .= '<a href="'.base_url("menu/delete/$id_enc").'" class="btn default btn-xs black"><i class="fa fa-edit"></i> Delete </a>';
                                }
                                $table .= view::render_button_group_raw($action1).' </td></tr>';

                        
                        if (isset($menu[$group->kd_grup_menu][$value['kd_menu']])) :
                            $total_menu_2 = count($menu[$group->kd_grup_menu][$value['kd_menu']]);
                            $idx_menu_2 = 0;
                            foreach ($menu[$group->kd_grup_menu][$value['kd_menu']] as $value2) :
                                $idx_menu_2++;
                                $id2 = $value2['kd_menu'];
                                $group_menu_2_up = '';
                                $group_menu_2_down = '';
                                if ($idx_menu_2 == 1) {
                                    $group_menu_2_up = 'disabled="disabled"';
                                }
                                if ($idx_menu_2 == $total_menu_2) {
                                    $group_menu_2_down = 'disabled="disabled"';
                                }
                                $id_enc2 = url_base64_encode($id2);
                                $table .= '<tr>
                                    <td style="padding-right: 16px;" align="right"><i class="icon-menu"></i></td>
                                    <td style="font-weight: bold;"> '.$value2['nama_menu'].'</td>
                                    <td> <i class="'.$value2['icon'].'"></i> </td>
                                    <td> '.$value2['deskripsi'].'</td>
                                    <td> '.$value2['url'].' </td>
                                    <td align="center">';
                                        
                                        $action2 = '';
                                        if ($this->access_right->otoritas('edit')) {
                                            //$action2 .= anchor(null, '<i class="icon-edit"></i>', array('id' => 'button-edit-' . $id2, 'class' => 'btn', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('menu/edit/' . $id2)));
                                            $action1 .= view::button_edit($id_enc);//$action2 .= '<a href="'.base_url("menu/edit/$id_enc2").'" class="btn default btn-xs purple"><i class="fa fa-edit"></i> Edit </a>';
                                        }
                                        if ($this->access_right->otoritas('delete')) {
                                            //$action2 .= anchor(null, '<i class="icon-trash"></i>', array('id' => 'button-delete-' . $id2, 'class' => 'btn', 'onclick' => 'delete_row(this.id)', 'data-source' => base_url('menu/delete/' . $id2), 'style' => 'margin-left:5px;'));
                                            $action1 .= view::button_delete($id_enc);//$action2 .= '<a href="'.base_url("menu/delete/$id_enc2").'" class="btn default btn-xs black"><i class="fa fa-edit"></i> Delete </a>';
                                        }
                                        $table .= view::render_button_group_raw($action2).'</td></tr>';
                                
                                if (isset($menu[$group->kd_grup_menu][$value2['kd_menu']])) :
                                    $total_menu_3 = count($menu[$group->kd_grup_menu][$value2['kd_menu']]);
                                    $idx_menu_3 = 0;
                                    foreach ($menu[$group->kd_grup_menu][$value2['kd_menu']] as $value3) :
                                        $idx_menu_3++;
                                        $id3 = $value3['kd_menu'];
                                        $group_menu_3_up = '';
                                        $group_menu_3_down = '';
                                        if ($idx_menu_3 == 1) {
                                            $group_menu_3_up = 'disabled="disabled"';
                                        }
                                        if ($idx_menu_3 == $total_menu_3) {
                                            $group_menu_3_down = 'disabled="disabled"';
                                        }
                                        $id_enc3 = url_base64_encode($id3);
                                        $table .= '<tr>
                                            <td align="right"><i class="icon-menu"></i></td>
                                            <td> '.$value3['nama_menu'].' </td>
                                            <td><i class="'.$value3['icon'].'"></i> </td>
                                            <td> '.$value3['deskripsi'].' </td>
                                            <td> '.$value3['url'].' </td>
                                            <td align="center">';
                                                $action3 = '';
                                                if ($this->access_right->otoritas('edit')) {
                                                    //$action3 .= anchor(null, '<i class="icon-edit"></i>', array('id' => 'button-edit-' . $id3, 'class' => 'btn', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('menu/edit/' . $id3)));
                                                    $action3 .= '<a href="'.base_url("menu/edit/$id_enc3").'" class="btn default btn-xs purple"><i class="fa fa-edit"></i> Edit </a>';
                                                }
                                                if ($this->access_right->otoritas('delete')) {
                                                    //$action3 .= anchor(null, '<i class="icon-trash"></i>', array('id' => 'button-delete-' . $id3, 'class' => 'btn', 'onclick' => 'delete_row(this.id)', 'data-source' => base_url('menu/delete/' . $id3), 'style' => 'margin-left:5px;'));
                                                    $action3 .= '<a href="'.base_url("menu/delete/$id_enc3").'" class="btn default btn-xs black"><i class="fa fa-edit"></i> Delete </a>';
                                                }
                                                $table .= $action3.'</td></tr>';
                                        
                                    endforeach;
                                endif;
                            endforeach;
                        endif;
                    endforeach;
                endif;
            endif;
            
        $table .= '</tbody>';
     endforeach; 
$table .= '</table>';

        $data = array('table'=>$table, 'total'=>10);
        echo json_encode($data);
    }
    
    public function add($id = '',$status_delete = 0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->config->load('twig');
            $this->load->model('menu_group_model');
            $menu_group_options = $this->menu_group_model->options();
            $title = 'Tambah Data Menu';
            $data['id'] = $id;
            if ($id != '') {
                $title = 'Edit Data Menu';

                $row = $this->menu_model->getById($id)->row();
                if($row){
                    $data['default'] = $row;
                    $data['def_nama_menu'] = $row->nama_menu;
                    $data['def_url'] = $row->url;
                    $data['def_deskripsi'] = $row->deskripsi;
                    $def_kd_grup_menu = $row->kd_grup_menu;
                    $def_kd_parent = $row->kd_parent;
                }
            }
            //echo $id;
            $data['title'] = $title;
            $button_group[] = view::button_back();
            if($status_delete == 0){
                $data['form_action'] = '<form action="'.base_url($this->class_name.'/proses').'" method="post" id="finput" class="form-horizontal" novalidate="novalidate"><input name = "id" type="hidden" value="'.$id.'">';
                $button_group[] = view::button_save();
            }else{
                $data['title'] = 'Delete Data Pulau';
                $data['form_action'] = '<form action="'.base_url($this->class_name.'/proses_delete').'" method="post" id="finput" class="form-horizontal" novalidate="novalidate"><input name = "id" type="hidden" value="'.$id.'">';
                $button_group[] = view::button_delete_confirm();
            }
            
            $data['form'] = $this->class_name.'/form.tpl';
            $data['base_url'] = base_url();
            $data['options_group_menu'] = form_dropdown('group_menu', $menu_group_options, !empty($def_kd_grup_menu) ? $def_kd_grup_menu : '', 'id="group_menu" class="form-control bs-select" data-live-search="true"');
            //$data['options_parent']     = form_dropdown('parent', $this->menu_model->options('Select Parent'), $def_kd_parent, 'id="parent" class="form-control select2" placeholder="Select Parent"');
            $data['options_parent']     = form_dropdown('parent', $this->menu_model->options('Select Parent'), $def_kd_parent, 'id="select-parent" class="select2 form-control" placeholder="Select Parent"');

            
            $data['button_group'] = view::render_button_group($button_group);
            $data['button']['icon'] = view::button_icon_select($row->icon);
            $data['data'] = $row;
            $data['form'] = $this->class_name.'/form.tpl';

            $this->twig->display('base/page_form.tpl', $data);

        } else {
            $this->access_right->redirect();
        }
    }

    public function edit($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->add($id);
    }

    public function proses() {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('nama_menu', 'Nama Menu', 'trim|required');
            //$this->form_validation->set_rules('url', 'URL', 'trim|required');
            $this->form_validation->set_rules('group_menu', 'Group Menu', 'required');

            if ($this->form_validation->run()) {
                $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');

                $id = $this->input->post('id');
                $parent = $this->input->post('parent');

                // menset parent
                $menuLevel = 0;
                if($parent) {
                    $rowParent = $this->menu_model->getById($parent)->row();
                    $menuLevel = $rowParent->menu_level + 1;
                }

                $data = array(
                    'kd_grup_menu' => $this->input->post('group_menu'),
                    'nama_menu' => $this->input->post('nama_menu'),
                    'deskripsi' => $this->input->post('deskripsi'),
                    'url' => $this->input->post('url'),
                    'icon' => $this->input->post('icon'),
                    'kd_parent' => !empty($parent) ? $parent : null,
                    'menu_level' => $menuLevel,
                );

                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */

                if ($id == '') {
                    if ($this->menu_model->create($data)) {
                        $insert_id = $this->session->userdata('insert_id');
                        $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refreshNewCustomTable()');
                    }
                } else {
                    if ($this->menu_model->update($data, $id)) {
                        $insert_id = $id;
                        $message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', 'refreshNewCustomTable()');
                    }
                }
            } else {
                $message = array(false, 'Terjadi Kesalahan', validation_errors(), '');
            }
            
            echo json_encode($message);
        } else {
            $this->access_right->redirect();
        }
    }

    public function proses_reordering(){
        $data           = $this->input->post('data');
        $kd_grup_menu   = $this->input->post('group_id');
        $kd_parent      = $this->input->post('parent_id');
        //file_put_contents('holahola.txt', json_encode($_POST));

        $this->db->trans_start();
        $message = array('success'=>false, 'message'=> 'Update Failed');

        if($data) {
            $this->load->model('menu_model');
            $menuLevel = 0;
            if($kd_parent){
                $menuLevel = $this->menu_model->getById($kd_parent)->row()->menu_level;
                $menuLevel++;
            }

            foreach ($data as $urutan => $id) {
                $this->menu_model
                    ->set('menu_urutan', $urutan)
                    ->set('kd_grup_menu', $kd_grup_menu)
                    ->set('kd_parent', $kd_parent)
                    ->set('menu_level', $menuLevel)
                    ->findByPk($id)->update();

                // $this->menu_model
                //     ->set('menu_level', 'menu_level+1', FALSE)
                //     ->where_in('kd_parent', $id)->update();

            }

            $this->menu_model
                ->set('kd_grup_menu', $kd_grup_menu)
                ->where_in('kd_parent', $data)->update();

            if($this->db->trans_status()){
                $message = array('success'=>true, 'message'=> 'Update Success', 'callback'=>'refreshNewCustomTable()');
            }
        }

        $this->db->trans_complete();
        echo json_encode($message);
    }

    public function proses_reordering_new(){
        $this->load->model('config_model');
        $data = $this->input->post('data');
        $message = array('success'=>false, 'message'=> 'sadsad');
        // echo json_encode($message);return;

        $this->db->trans_start();
        $this->config_model->set('menu', $data)->update();
        if($this->db->trans_status()){
            $message = array('success'=>true, 'message'=> 'Update Success', 'callback'=>'refreshNewCustomTable()');
        }
        $this->db->trans_complete();
        echo json_encode($message);

    }

    public function delete($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->add($id,$status_delete = 1);
    }

    public function proses_delete() {
        $id = $this->input->post('id');
        $this->access_right->otoritas('delete', true);
        if ($this->access_right->otoritas('delete', true)) {
            $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');
            if ($this->menu_model->delete($id)) {
                $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', 'refresh_filter()');
                /* INSERT LOG */
                $this->access_right->activity_logs('delete','Delete Grup Menu');
                /* END OF INSERT LOG */
            }
            redirect(base_url($this->class_name));
        }
    }

    public function get_parent() {
        $kode = $this->input->post('kode');
        $selected = $this->input->post('selected');

        $options = array(null => 'NULL');

        if (!empty($kode)) {
            //$data = $this->menu_model->get_data(array('kd_grup_menu' => $kode, 'kd_parent' => null));
            $data = $this->menu_model->get_data(array('kd_grup_menu' => $kode));
            foreach ($data->result() as $value) {
                $options[$value->kd_menu] = $value->nama_menu;
            }
        }

        echo json_encode(hgenerator::array_to_option($options, $selected));
    }

}

?>
<?php

/**
 * Description of bbm
 *
 * @author SANGGRA HANDI
 */
class mesin_tekno extends My_Controller {

    public function __construct() {
    	$this->formConfig = array('full-width' => 1);
        parent::__construct();
        $this->class_name = get_class($this);
        $this->load->model(array('mesin_tekno_model'));
    }

    public function index() {
        $data['title'] = 'Master Machine Teknogame';

        /* INSERT LOG */
        //$this->access_right->activity_logs('view',$data['title']);
        /* END OF INSERT LOG */

        //$data['button_group'] = $this->getAvailableButtons();
        //$data['button_right'] = $this->getTools();
         /* BUTTON GROUP */
        $button_group    = array(
                                view::button_add()
                            );

        $data['button_group'] = view::render_button_group($button_group);

        $data['button_right'] = $this->getPrintTools();

        $data['table']['main']  = $this->table_main(array('wrapper', 'filter'));
        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'base/page_content.tpl';
    
        $this->twig->display('index.tpl', $data);
    }

    public function table_main($option = array()){
        $this->load->library('Datatable');
        $table = $this->datatable;
        if (in_array('filter', $option)) {
            $table->dataFilter = array(
                                array('kd_mesin', 'SERIAL NUMBER', 'text'),
                                array('serial_number','REGISTRATION NUMBER','text'),
                              
                                array('nama_jenis_mesin','MACHINE TYPE','text'),
                                array('nama_cabang','STORE','text'),
                                array('kd_cabang','STORE CODE','text'),
                                array('nama_mesin','TEKNO NAME','text'),
                                array('kd_mesin_tekno','TEKNO KODE','text'),
                                array('loc_name','LOCATION','text'),
                            );
        }
        if (in_array('wrapper', $option)) {
            $table->numbering  = true;
            $table->id         = 'table-shipping';
            $table->isScrollable= false;
            $table->header     = array('REGISTRATION NUMBER','SERIAL NUMBER', 'MACHINE TYPE','TEKNO NAME','TEKNO KODE','STORE','STORE CODE','LOCATION', 'ACTION');
            $table->source     = site_url($this->class_name.'/table_main');
            return $table->generateWrapper();
        } else {
            $table
                ->setModel($this->mesin_tekno_model)
                ->with(array('mesin','jenis_mesin', 'store','location'))
                ->setNumbering()
                ->select('serial_number, mesin.kd_mesin, nama_jenis_mesin, nama_mesin,this.kd_mesin as kd_mesin_tekno , nama_cabang, kd_cabang, loc_name, id_mesin_teknogame')
//                ->edit_column('id_sparepart', '$1', 'view::btn_group_receiving_warehouse(shipping_id)')
                //->edit_column('harga','$1','hgenerator::rupiah(harga)')
                //->edit_column('tgl_beli','$1','date::shortDate(tgl_beli)')
               // ->edit_column('garansi_toko','$1','date::shortDate(garansi_toko)')
                //->edit_column('status','$1','view::getStatusMesin(status)')
                ->edit_column('id_mesin_teknogame', '$1', 'view::btn_group_edit_delete(id_mesin_teknogame)');
            echo $table->generate();
        }

    }

    
   

    public function add($id = '', $status_delete=0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->load->model('mesin_model');
           // $this->load->model('supplier_model');
            $this->load->model('jenis_mesin_model');
            $this->load->model('store_model');
           /* $this->load->model('owner_model');
            $this->load->model('m_loc_service_model');*/

            $title = 'Tambah Data';
            $data['form_action'] = $this->class_name . '/proses';

            $data['id'] = $id;
            if ($id != '') {
                $title = 'Edit Data';
                $row = $this->mesin_tekno_model->get_by_id($id)->row();
                $data['data'] = $row;
            }
            $disabled = (isset($row) && $row->status == 2) ? 'disabled="disabled"' : "";
            $isLiquidated = (isset($row) && $row->status == 2) ? true : false;

            $data['title'] = $title;
            $data['assets_url'] = $this->config->item('assets_url');
            $data['sidebar'] = $this->access_right->menu();
            $data['form']       = $this->class_name.'/form.tpl';
            
            if($status_delete == 0){
                $data['form_action']    = view::form_input($id);
                $button_group[]         = view::button_save();
            }else{
                $data['title']          = 'Delete Store';
                $data['form_action']    = view::form_delete($id);;
                $data['readonly']       = 'readonly=""';
                $button_group[]         = view::button_delete_confirm();
            }


            // dropdown island, province, city, area
            $htmlOptions            =  'class="form-control select2me" data-placeholder="Select Reg. Number"';
            $data['list_reg_number']  = form_dropdown('data[id_mesin]', $this->mesin_model->options_reg(), $row->id_mesin, $htmlOptions);
            $htmlOptions            =  'class="form-control select2me" data-placeholder="Select Coin"';
          
            $htmlOptions            =  'class="form-control select2me" data-placeholder="Select Machine Type"';
            $data['list_jenis_mesin']  = form_dropdown('data[id_jenis_mesin]', $this->jenis_mesin_model->options(), $row->id_jenis_mesin, $htmlOptions);

            $htmlOptions            =  'class="form-control select2me" data-placeholder="Code Machine Type"';
            $data['kd_jenis_mesin']  = form_dropdown('data[kd_jenis_mesin]', $this->jenis_mesin_model->options_kd(), $row->kd_jenis_mesin, $htmlOptions);
            $htmlOptions            =  'class="form-control select2me" data-placeholder="Select Store"';
            $data['list_cabang']  = form_dropdown('data[store_id]', $this->store_model->options_empty(), $row->store_id, $htmlOptions);
            
            $data['button_group'] = view::render_button_group($button_group, array('class'=>'pull-right'));            
            $this->twig->display('base/page_form.tpl', $data);
        } else {
            $this->access_right->redirect();
        }
    }
    public function getCode($jenis_mesin = NULL){
        if (!$jenis_mesin)
            return;
        echo code::getCode('machine_id_outlet', $jenis_mesin);
    }

    private function getStatusOptions($isLiquidated = false){
    	if($isLiquidated)
    		$options = array('' => '', '0' => "Broken", '1' => "Fine", '2' => "Liquidated");
    	else
    		$options = array('' => '', '0' => "Broken", '1' => "Fine", '2' => "Liquidated");
    	return $options;
    }

    public function edit($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('edit', true);
        $this->add($id);
    }

    public function delete($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('delete', true);
        $this->add($id,$status_delete = 1);
    }

    public function proses() {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('data[kd_mesin]', 'Teknogame Code', 'required|trim');
        
           // $this->form_validation->set_rules('data[serial_number]', 'Registration Number', 'required|trim');
           // $this->form_validation->set_rules('data[id_jenis_mesin]', 'Jenis Mesin', 'required|trim');
           // $this->form_validation->set_rules('data[kd_jenis_mesin]', 'Kode Jenis Mesin', 'required|trim');
            $this->form_validation->set_rules('data[store_id]', 'Store', 'required|trim');
            $this->form_validation->set_rules('data[nama_mesin]', 'Teknogame Name', 'required|trim');
            if ($this->form_validation->run()) {
                $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');

                $id = $this->input->post('id');
				$data = $this->input->post('data');

                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */


                if ($id == '') {
                    if ($this->mesin_tekno_model->create($data)) {
                        $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh_filter()');
						/* INSERT LOG */
						$this->access_right->activity_logs('add','Tambah Mesin');
						/* END OF INSERT LOG */
                    }
                } else {
                    if ($this->mesin_tekno_model->update($data, $id)) {
                        $message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', 'refresh_filter()');
						/* INSERT LOG */
						$this->access_right->activity_logs('edit','Edit Mesin');
						/* END OF INSERT LOG */
                    }
                }
            } else {
                $message = array(false, 'Terjadi Kesalahan', validation_errors(), '');
            }
            echo json_encode($message);
        } else {
            $this->access_right->redirect();
        }
    }

    public function proses_delete() {
        $id = $this->input->post('id');
		$this->access_right->otoritas('delete', true);
        if ($this->access_right->otoritas('delete', true)) {
            $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');
            if ($this->mesin_tekno_model->delete($id)) {
                $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', 'refresh_filter()');
				/* INSERT LOG */
				$this->access_right->activity_logs('delete','Delete BBM');
				/* END OF INSERT LOG */
            }
            echo json_encode($message);
        }
    }

    public function add_detail($id_bbm = '') {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {

            $title = 'Tambah Data';
            $data['form_action'] = $this->class_name . '/proses_detail';

            $data['id'] = '';
            $data['id_bbm'] = $id_bbm;

        $data['title'] = '<i class="icon-edit"></i> ' . $title;
        $this->load->model('referensi_barang_model');
        $this->load->model('referensi_purchase_order_model');
        $data['options_barang'] = $this->referensi_barang_model->options();
        $data['options_purchase_order'] = $this->referensi_purchase_order_model->options();
            

            $this->load->view($this->class_name . '/form_detail', $data);
        } else {
            $this->access_right->redirect();
        }
    }

    public function edit_detail($id_bbm,$id) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->load->model('mesin_tekno_model');

            $title = 'Tambah Data';
            $data['form_action'] = $this->class_name . '/proses_detail';

            $data['id'] = $id;
            $data['id_bbm'] = $id_bbm;
            if ($id != '') {
                $title = 'Edit Data Detail';

                $db = $this->mesin_tekno_model->get_detail_by_id($id);
                if ($db->num_rows() > 0) {
                    $row = $db->row();
                    $data['default'] = $row;
                }
            }

        $data['title'] = '<i class="icon-edit"></i> ' . $title;
        $this->load->model('referensi_barang_model');
        $this->load->model('referensi_purchase_order_model');
        $data['options_barang'] = $this->referensi_barang_model->options();
        $data['options_purchase_order'] = $this->referensi_purchase_order_model->options();
            

            $this->load->view($this->class_name . '/form_detail', $data);
        } else {
            $this->access_right->redirect();
        }
    }

    public function proses_detail() {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('kd_barang', 'Nama Barang', 'required|trim');
            $this->form_validation->set_rules('jumlah', 'Jumlah', 'required|trim');


            if ($this->form_validation->run()) {
                $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');

                $id = $this->input->post('id');

                $data = array(
                    'id_barang' => $this->input->post('kd_barang'),
                    'jumlah_barang' => $this->input->post('jumlah'),
                );

                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */

                if ($id == '') {
                    $data['id_bbm'] = $this->input->post('id_bbm');
                    if ($this->mesin_tekno_model->create_detail($data)) {
                        $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh_filter()');
                        /* INSERT LOG */
                        $this->access_right->activity_logs('add','Tambah BBM');
                        /* END OF INSERT LOG */
                    }
                } else {
                    if ($this->mesin_tekno_model->update_detail($data, $id)) {
                        $message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', 'refresh_filter()');
                        /* INSERT LOG */
                        $this->access_right->activity_logs('edit','Edit BBM');
                        /* END OF INSERT LOG */
                    }
                }
            } else {
                $message = array(false, 'Terjadi Kesalahan', validation_errors(), '');
            }
            echo json_encode($message);
        } else {
            $this->access_right->redirect();
        }
    }

    public function delete_detail($id_bbm,$id) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->load->model('mesin_tekno_model');

            $title = 'Hapus Data';
            $data['form_action'] = $this->class_name . '/proses_delete_detail/'.$id;

            $data['id'] = $id;
            $data['id_bbm'] = $id_bbm;
            if ($id != '') {
                $title = 'Edit Data Detail';

                $db = $this->mesin_tekno_model->get_detail_by_id($id);
                if ($db->num_rows() > 0) {
                    $row = $db->row();
                    $data['default'] = $row;
                }
            }

        $data['title'] = '<i class="icon-edit"></i> ' . $title;
        $this->load->model('referensi_barang_model');
        $this->load->model('referensi_purchase_order_model');
        $data['options_barang'] = $this->referensi_barang_model->options();
        $data['options_purchase_order'] = $this->referensi_purchase_order_model->options();
            

            $this->load->view($this->class_name . '/form_detail_delete', $data);
        } else {
            $this->access_right->redirect();
        }
    }

    public function proses_delete_detail($id) {
        $this->access_right->otoritas('delete', true);
        if ($this->access_right->otoritas('delete', true)) {
            $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');
            if ($this->mesin_tekno_model->delete_detail($id)) {
                $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', 'refresh_filter()');
                /* INSERT LOG */
                $this->access_right->activity_logs('delete','Delete BBM');
                /* END OF INSERT LOG */
            }
            echo json_encode($message);
        }
    }

    public function excel($page = 0){

        $this->load->library("custom_table");

        $table = new stdClass();
        $header[0] = array(
                            "TANGGAL BBM",1,1,
                            "KODE BBM", 1, 1, 
                            "KODE PURCHASE ORDER", 1, 1, 
                            "SUPPLIER",1,1
                        );
        $table->header = $header;
        $table->id     = 't_bbm';
        $table->align = array('tanggal_bbm' => 'center', 'kd_bbm' => 'center','kd_purchase_order'=>'center','supplier'=>'center', 'aksi' => 'center');
        $table->style = "table table-striped table-bordered table-hover datatable dataTable";
        $table->model = "mesin_tekno_model->data_table_excel";
        $table->limit = $this->limit;
        $table->border = 1;
        $table->page = 1;
        $data = $this->custom_table->generate($table);
        
        //===========filter di excel===================
        $id_supplier= $this->input->post("kd_supplier");
        $id_purchase_order = $this->input->post('kd_po');
        $tanggal_awal = $this->input->post('tanggal_awal_bbm');
        $tanggal_akhir = $this->input->post('tanggal_akhir_bbm');
        $tahun_aktif = $this->input->post('tahun_aktif');
        $kd_bbm = $this->input->post('kd_bbm');
        if(empty($kd_bbm)){
            $bbm = 'Semua';
        }else{
            $bbm = $kd_bbm;
        }
        if(empty($tahun_aktif)){
            $tahun_aktif = date('Y');
        }
        if($id_supplier==''){
            $supplier = 'Semua';
        }else{
            $supplier       = $id_supplier;
        }
        if($id_purchase_order==''){
            $po = 'Semua';
        }else{
            $po = $id_purchase_order;
        }
        if($tanggal_awal==''||$tanggal_akhir==''){
            $tanggal_awal = 'Semua';
            $tanggal_akhir = 'Semua';
        }
        
        $filter = array(
            'Kode BBM'          =>$bbm,
            'Purchase Order'    =>$po,
            'Supplier'          =>$supplier,
            'Periode Awal'      =>$tanggal_awal,
            'Periode Akhir'     =>$tanggal_akhir,
            'Tahun Aktif'       =>$tahun_aktif);
        $data['filter'] = $filter;
        //----------------------------------------------------
        $date = date('Ymd His');
        $data['judul_kecil'] = 'Bukti Barang Masuk';
        $data['filename'] = 'Bukti Barang Masuk ('.$date.')';
        $this->load->view('cetak_excel',$data);
    }

    public function excel_detail($id_bbm) {
        $this->load->library("custom_table_detail");

        $table = new stdClass();
        $header[0] = array(
                            "KODE BARANG",1,1,
                            "NAMA BARANG",1,1,
                            "JUMLAH", 1, 1, 
                            "SATUAN", 1, 1, 
                            "KATEGORI",1,1
                        );

        $table->header = $header;
        $table->id     = 't_bbm_detail';
        $table->align = array('tanggal_bbm' => 'center', 'jumlah' => 'center','satuan'=>'center','kategori'=>'center', 'aksi' => 'center');
        $table->style = "table table-striped table-bordered table-hover datatable dataTable";
        $table->model = "mesin_tekno_model->data_table_detail_excel";
        $table->limit = $this->limit;
        $table->page = 1;
        $table->border = 1;
        $data = $this->custom_table_detail->generate($table);
        //============data detail==================
        $data_bbm = $this->mesin_tekno_model->get_by_id($id_bbm);
        if($data_bbm->num_rows()>0){
            $bbm = $data_bbm->row();
            $filter = array(
                'Tanggal BBM'       =>$bbm->tanggal_bbm,
                'Kode BBM'          =>$bbm->kd_bbm,
                'Kode PO'           =>$bbm->kd_po,
                'Kode Supplier'     =>$bbm->nama_supplier.' ('.$bbm->kd_supplier.')');
            $data['filter'] = $filter;
        }
        //------------------------------------------------
        
        $date = date('Ymd His');
        $data['judul_kecil'] = 'Bukti Barang Masuk Detail';
        $data['filename'] = 'Bukti Barang Masuk Detail ('.$date.')';
        $this->load->view('cetak_excel',$data);
    }

    public function getPrintTools(){
        $button_group = array();
        if($this->access_right->otoritas('print')){
            $button_group[] = view::button_all_barcode(1,1,"Machine");
        }
        
        if(true){//export
            //$button_group[] = view::button_export();
        }
        return view::render_button_group_laporan_payout($button_group, array(), true);

    }


    public function barcode($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('edit', true);
        $this->showbarcode($id);
    }

    public function barcodeall($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('edit', true);
        $this->showbarcodeall($id);
    }


    public function showbarcode($id='',$status_delete = 0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->config->load('twig');
            
            $title = 'Tambah Data';
            $data['form_action'] = $this->class_name . '/proses';

            $data['id'] = $id;
            if($id){
                $title = 'Machine Barcode';
                //$row            = $this->hadiah_model->getById($id)->row();
                $row        =   $this->mesin_tekno_model
                                ->with('jenis_mesin')
                                ->where('id_mesin', $id)
                                ->get()
                                ->row();
                $data['data']  = $row;
            }

            $data['title']      = $title;
            $data['assets_url'] = $this->config->item('assets_url');
            $data['sidebar']    = $this->access_right->menu();
            $data['form']       = $this->class_name.'/form_barcode.tpl';

            $button_group   = array();
            $button_group[] = view::button_back();
            
            if($status_delete == 0){
                $data['form_action']    = view::form_input($id);
                //INI BUTTON UNTUK PRINT BARCODE SPAREPART
                $button_group[]         = view::button_print_mesin();
            }else{
                $data['title']          = 'Delete Merchandise';
                $data['form_action']    = view::form_delete($id);;
                $data['readonly']       = 'readonly=""';
                $button_group[]         = view::button_delete_confirm();
            }

            $data['button_group'] = view::render_button_group($button_group);
            //$data['options_hadiah'] = form_dropdown('data[hadiahtype_id]', $this->hadiah_type_model->options(), $row->hadiahtype_id, 'class="bs-select" disabled="disabled"');
            
            //$options_pulau = $this->pulau_model->options();
            //$data['options_pulau'] = form_dropdown('id_pulau',$options_pulau, !empty($def_id_pulau) ? $def_id_pulau : '', 'class = "form-control"'); 
            
            $this->twig->display('base/page_form.tpl', $data);

        } else {
            $this->access_right->redirect();
        }
    }

    public function showbarcodeall($id='',$status_delete = 0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->config->load('twig');
            
            $title = 'Print All Machine Barcode';
            $data['form_action'] = $this->class_name . '/proses';

            $data['id'] = $id;
            if($id){
                $title = 'Machine Barcode';
                //$row            = $this->hadiah_model->getById($id)->row();
                $row        =   $this->mesin_tekno_model
                                ->where('id_mesin', $id)
                                ->get()
                                ->row();
                $data['data']  = $row;
            }

            $data['title']      = $title;
            $data['assets_url'] = $this->config->item('assets_url');
            $data['sidebar']    = $this->access_right->menu();
            $data['form']       = $this->class_name.'/form_barcode_all.tpl';

            $button_group   = array();
            $button_group[] = view::button_back();
            
            if($status_delete == 0){
                $data['form_action']    = view::form_input($id);
                //INI BUTTON UNTUK PRINT BARCODE SPAREPART
                $button_group[]         = view::button_print_all_mesin();
            }else{
                $data['title']          = 'Delete Merchandise';
                $data['form_action']    = view::form_delete($id);;
                $data['readonly']       = 'readonly=""';
                $button_group[]         = view::button_delete_confirm();
            }

            $data['button_group'] = view::render_button_group($button_group);
            //$data['options_hadiah'] = form_dropdown('data[hadiahtype_id]', $this->hadiah_type_model->options(), $row->hadiahtype_id, 'class="bs-select" disabled="disabled"');
            
            //$options_pulau = $this->pulau_model->options();
            //$data['options_pulau'] = form_dropdown('id_pulau',$options_pulau, !empty($def_id_pulau) ? $def_id_pulau : '', 'class = "form-control"'); 
            
            $this->twig->display('base/page_form.tpl', $data);

        } else {
            $this->access_right->redirect();
        }
    }


    public function initPrinting(){
        $this->load->helper('printingdua');

        $ci =& get_instance();

        $nc = $ci->input->post('nc');

        $kd_mesin = $ci->input->post('kd_mesin');
        $serial_number = $ci->input->post('serial_number');
        $nama_jenis_mesin = $ci->input->post('nama_jenis_mesin');

        $cmd="";

/*        $cmd .="^XA
                ^MMA
                ^PW799
                ^LL0200
                ^LS0
                ^BY2,3,93^FT120,138^BCN,,Y,N
                ^FD>:".$serial_number."^FS
                ~JSB
                ^PQ".$nc.",0,1,Y^XZ";*/
        $cmd .="^XA
                ^MMA
                ^PW799
                ^LL0240
                ^LS0
                ^BY2,3,85^FT143,123^BCN,,Y,N
                ^FD>:".$serial_number."^FS
                ^FT238,192^A0N,45,45^FH\^FD".$nama_jenis_mesin."^FS
                ~JSB
                ^PQ".$nc.",0,1,Y^XZ";
/*^BY2,3,85^FT14,107^BCN,,Y,N
^FD>:R 91 . >52009>6 . 05 . 18 . 026 . 04^FS
^FT17,204^A0N,45,45^FH\^FDAIR MINI HOCKEY^FS

^XA
^MMT
^PW799
^LL0240
^LS0
^BY2,3,85^FT143,123^BCN,,Y,N
^FD>:R91.>52009>6.05.18.026.04^FS
^FT238,192^A0N,45,45^FH\^FDAIR MINI HOCKEY^FS
^PQ1,0,1,Y^XZ

^XA
^MMT
^PW799
^LL0240
^LS0
^BY2,3,85^FT145,97^BCN,,Y,N
^FD>:R91.>52009>6.05.18.026.04^FS
^FT240,166^A0N,45,45^FH\^FDAIR MINI HOCKEY^FS
^PQ1,0,1,Y^XZ


^XA
^MMT
^PW799
^LL0240
^LS0
^BY2,3,76^FT14,88^BCN,,Y,N
^FD>:R 20 . >52008>6 . 09 . 18 . 349 . 01^FS
^FT14,157^A0N,45,45^FH\^FDAIR HOCKEY 2000^FS
^PQ1,0,1,Y^XZ


^XA
^MMT
^PW799
^LL0240
^LS0
^BY2,3,85^FT20,99^BCN,,Y,N
^FD>:R 91 . >52009>6 . 05 . 18 . 026 . 04^FS
^FT240,166^A0N,45,45^FH\^FDAIR MINI HOCKEY^FS
^PQ1,0,1,Y^XZ
*/
        
        $form = '<form id="myForm" action="">

    <input type="hidden" id="sid" name="sid" value="'.session_id().'" />
        <fieldset hidden>
            <legend>Client Printer Settings</legend>
            
            <div hidden>
                I want to:&nbsp;&nbsp;
                <select id="pid" name="pid">
                  <option value="0">Use Default Printer</option>
                  <option value="1" selected="selected">Display a Printer dialog</option>
                  <option value="2">Use an installed Printer</option>
                  <option value="3">Use an IP/Etherner Printer</option>
                  <option value="4">Use a LPT port</option>
                  <option value="5">Use a RS232 (COM) port</option>
                </select>
                <br />
                <br />
                <div id="info" class="alert alert-info" style="font-size:11px;"></div>                
                <br />
            </div>
            
            <div id="installedPrinter" hidden>
                <div id="loadPrinters" name="loadPrinters">
                WebClientPrint can detect the installed printers in your machine. <a onclick="javascript:jsWebClientPrint.getPrinters();" class="btn btn-success">Load installed printers...</a>
                </div>
                <label for="installedPrinterName">Select an installed Printer:</label>
                <select name="installedPrinterName" id="installedPrinterName"></select>

            

            </div>           
                        
        </fieldset>
        <fieldset hidden>
            <legend>Printer Commands</legend>
            
            <p>
                Enter the printers commands you want to send and is supported by the specified printer (ESC/P, PCL, ZPL, EPL, DPL, IPL, EZPL, etc). 
                <br /><br />
                <b>NOTE:</b> You can use the <b>hex notation for non-printable characters</b> e.g. for Carriage Return (ASCII Hex 0D) you can specify 0x0D
                
            </p>
            <br /><br />
            <div class="alert alert-info" style="font-size:11px;">
            <b>Upload Files</b><br />
            This online demo does not allow you to upload files. So, if you have a file containing the printer commands like a PRN file, Postscript, PCL, ZPL, etc, then we recommend you to <a href="http://www.neodynamic.com/products/printing/raw-data/php/download/" target="_blank">download WebClientPrint</a> and test it by using the sample source code included in the package. Feel free to <a href="http://www.neodynamic.com/support" target="_blank">contact our Tech Support</a> for further assistance, help, doubts or questions.             
            </div>            
        </fieldset>        
        <fieldset hidden>
            <legend hidden>Ready to print!</legend>
            <h3>Your settings were saved! Now its time to <a href="#" onclick="javascript:doClientPrint();" class="btn btn-large btn-success">Print</a></h3>           
        </fieldset>

        <textarea id="printerCommands" name="printerCommands" rows="10" cols="80" class="span9" hidden>'.$cmd.'</textarea>
    </form>';
    echo $form;

    }


    public function printAllBarcode(){
        $this->load->helper('printingdua');

        $ci =& get_instance();

        $nc = $ci->input->post('nc');

        $kd_mesin = $ci->input->post('kd_mesin');
        $serial_number = $ci->input->post('serial_number');

        $cmd="";
        $dataall = $this->mesin_tekno_model
                    ->select('serial_number,nama_jenis_mesin')
                    ->with(array('jenis_mesin'))
                    ->get()
                    ->result_array();
        foreach ($dataall as $key => $value) {
            $cmd .="^XA
                ^MMA
                ^PW799
                ^LL0240
                ^LS0
                ^BY2,3,85^FT143,123^BCN,,Y,N
                ^FD>:".str_replace(" ","",$dataall[$key]["serial_number"])."^FS
                ^FT238,192^A0N,45,45^FH\^FD".$dataall[$key]["nama_jenis_mesin"]."^FS
                ~JSB
                ^PQ1,0,1,Y^XZ";
            

            /*$cmd .="^XA
                    ^MMA
                    ^PW799
                    ^LL0200
                    ^LS0
                    ^BY2,3,93^FT120,138^BCN,,Y,N
                    ^FD>:".str_replace(" ","",$dataall[$key]["serial_number"])."^FS
                    ^PQ1,0,1,Y^XZ";*/
        }


/*            $cmd .= "^XA
                ^MMA
                ^PW609
                ^LL0609
                ^LS0
                ^BY4,3,160^FT135,284^BCN,,Y,N
                ^FD>;".$kd_mesin."^FS
                ^FT219,386^A0N,28,28^FH\^FD".$serial_number."^FS
                ^PQ1,0,1,Y^XZ
                ";
*/



        
        $form = '<form id="myForm" action="">

    <input type="hidden" id="sid" name="sid" value="'.session_id().'" />
        <fieldset hidden>
            <legend>Client Printer Settings</legend>
            
            <div hidden>
                I want to:&nbsp;&nbsp;
                <select id="pid" name="pid">
                  <option value="0">Use Default Printer</option>
                  <option value="1" selected="selected">Display a Printer dialog</option>
                  <option value="2">Use an installed Printer</option>
                  <option value="3">Use an IP/Etherner Printer</option>
                  <option value="4">Use a LPT port</option>
                  <option value="5">Use a RS232 (COM) port</option>
                </select>
                <br />
                <br />
                <div id="info" class="alert alert-info" style="font-size:11px;"></div>                
                <br />
            </div>
            
            <div id="installedPrinter" hidden>
                <div id="loadPrinters" name="loadPrinters">
                WebClientPrint can detect the installed printers in your machine. <a onclick="javascript:jsWebClientPrint.getPrinters();" class="btn btn-success">Load installed printers...</a>
                </div>
                <label for="installedPrinterName">Select an installed Printer:</label>
                <select name="installedPrinterName" id="installedPrinterName"></select>

            

            </div>           
                        
        </fieldset>
        <fieldset hidden>
            <legend>Printer Commands</legend>
            
            <p>
                Enter the printers commands you want to send and is supported by the specified printer (ESC/P, PCL, ZPL, EPL, DPL, IPL, EZPL, etc). 
                <br /><br />
                <b>NOTE:</b> You can use the <b>hex notation for non-printable characters</b> e.g. for Carriage Return (ASCII Hex 0D) you can specify 0x0D
                
            </p>
            <br /><br />
            <div class="alert alert-info" style="font-size:11px;">
            <b>Upload Files</b><br />
            This online demo does not allow you to upload files. So, if you have a file containing the printer commands like a PRN file, Postscript, PCL, ZPL, etc, then we recommend you to <a href="http://www.neodynamic.com/products/printing/raw-data/php/download/" target="_blank">download WebClientPrint</a> and test it by using the sample source code included in the package. Feel free to <a href="http://www.neodynamic.com/support" target="_blank">contact our Tech Support</a> for further assistance, help, doubts or questions.             
            </div>            
        </fieldset>        
        <fieldset hidden>
            <legend hidden>Ready to print!</legend>
            <h3>Your settings were saved! Now its time to <a href="#" onclick="javascript:doClientPrint();" class="btn btn-large btn-success">Print</a></h3>           
        </fieldset>

        <textarea id="printerCommands" name="printerCommands" rows="10" cols="80" class="span9" hidden>'.$cmd.'</textarea>
    </form>';
    echo $form;

    }



}

/* End of file bbm.php */
/* Location: ./application/controllers/bbm.php */

<?php

/**
 * Description of bbm
 *
 * @author SANGGRA HANDI
 */
class likuidasi extends My_Controller {


    public function __construct() {
        parent::__construct();
        $this->load->model('likuidasi_model');
    }

    
    public function index() {
        $data['title'] = 'Likuidasi Outlet';

        /* INSERT LOG */
        //$this->access_right->activity_logs('view',$data['title']);
        /* END OF INSERT LOG */

        $data['button_group'] = $this->getAvailableButtons();
        $data['button_right'] = $this->getTools();

        $data['table']['main']  = $this->table_main(array('wrapper', 'filter'));
        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'base/page_content.tpl';
    
        $this->twig->display('index.tpl', $data);
    }

    public function table_main($option = array()){
        $this->load->model('range_tiket_model');
        $this->load->library('Datatable');
        $table = $this->datatable;

        if (in_array('wrapper', $option)) {
            $table->numbering  = true;
            $table->isScrollable= false;
            $table->id         = 'table-likuidasi';
            $table->header     = array('DATE','SKU','NAME','QTY','STORE','NOTE', 'STATUS', 'ACTION');
            $table->source     = site_url($this->class_name.'/table_main');
            return $table->generateWrapper();
        } else {
            $table
                ->setModel($this->likuidasi_model)
                ->setNumbering()
                ->with(array('item','outlet'))
                ->select('date,item_key, item_name,qty ,outlet.nama_cabang,note,status,likuidasi_id')
                ->edit_column('status', '$1', 'mutasistatus::getStatus(status)')
                ->edit_column('likuidasi_id', '$1', 'view::btn_group_mutasi(likuidasi_id)');
            $data = $table->getData();
            foreach ($data as $key => $value) {
                $temp = $this->range_tiket_model->get_tiket_by_id($value['item_id']);
                    
                /*$docs = '';
                foreach ($temp as $tmp) {
                    $docs.= ' <span class="badge badge-success badge-roundless">'.$tmp['pr_code'].'</span> ';
                }*/
                $data[$key]['item_id'] = $temp;
            }
            $table->setData($data);

            echo $table->generate();
            //echo $table->generate();
        }

    }

    public function add($id='',$status_delete = 0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->config->load('twig');
            $this->load->model(array('item_model','range_tiket_model','store_model'));
            $title = 'Tambah Data likuidasi';
            $data['form_action'] = $this->class_name . '/proses';

            $data['id'] = $id;
            if($id){
                $title = 'Edit likuidasi';
                $row           = $this->likuidasi_model->getById($id)->row();
                $data['data']  = $row;
            }

            $data['title']      = $title;
            $data['assets_url'] = $this->config->item('assets_url');
            $data['sidebar']    = $this->access_right->menu();
            $data['form']       = $this->class_name.'/form_likuidasi.tpl';

            $button_group   = array();
            $button_group[] = view::button_back();
            
            if($status_delete == 0){
                $data['form_action']    = view::form_input($id);
                $button_group[]         = view::button_save();
            }elseif($status_delete == 1){
                $data['title']          = 'Delete likuidasi';
                $data['form_action']    = view::form_delete($id);;
                $data['readonly']       = 'readonly=""';
                $button_group[]         = view::button_delete_confirm();
            }elseif($status_delete == 2){
                $data['title']          = 'Approve likuidasi';
                $data['form_action']    = view::form_input($id);;
                $data['readonly']       = 'readonly=""';
                $button_group[]         = view::button_save();
            }

            $data['button_group'] = view::render_button_group($button_group);

            $is_head_office = hprotection::isHeadOffice();
            if($is_head_office){
                $data['is_head_office'] = TRUE;
                $data['list_store']     = form_dropdown('data[store_id]', $this->store_model->options_empty(), $row->store_id,'class="form-control bs-select"' );
            }else{
                $data['is_head_office'] = FALSE;
                $data['list_store']     = form_hidden('data[store_id]', $this->session->userdata('store_id'));
            }

            $data['list_destination_store']     = form_dropdown('data[store_destination_id]', $this->store_model->options_empty(), $row->store_destination_id,'class="form-control bs-select"' );
            if(empty($row->date)){
                $data['data']['date'] = date('Y-m-d');
            }
            /*target="#list-item" data-source="'.site_url($this->class_name.'/getlist/item').'" 
                                    onchange="refreshList(this)" class="bs-select form-control"*/

/*            $htmlOptions        = 'target="#reg_tiket" id="list-item" placeholder="Select Item" class="select2 form-control" data-live-search="true" target-type="text"';

            $data['list_item']  = form_dropdown('data[item_id]', $this->item_model->options_empty(), $row->item_id, $htmlOptions);
*/            

            $data['list_item'] = '<input type="hidden" id="sell_list_item_id" name="data[item_id]" class="form-control" value="'.$row->item_id.'"/>';

            $this->twig->display('base/page_form.tpl', $data);

        } else {
            $this->access_right->redirect();
        }
    }

    public function confirm($act, $id){
        $button_group   = array();
        $button_group[] = view::button_back();
        $button_group[] = view::button_confirm();

        switch ($act) {
            

            case 'send':
                $data['title'] = 'SEND REQUEST';
                $data['status'] = 1;
                break;
            
            case 'revisi':
                $data['title'] = 'REVISI REQUEST';
                $data['status'] = 2;
                break;

            case 'reject':
                $data['title'] = 'REJECT REQUEST';
                $data['status'] = 4;
                break;

            case 'approve':
                $data['title'] = 'APPROVE REQUEST';
                $data['subtitle'] = 'approve request';
                $data['status'] = 5;
                break;

            case 'cancel':
                $data['title'] = 'REJECT REQUEST';
                $data['subtitle'] = 'reject Request';
                $data['status'] = 9;
                break;

            default:
                # code...
                break;
        }
        $data['hide_kd']        = true;
        $data['button_group']   = view::render_button_group($button_group);
        $data['form_action']    = view::form($id,$this->class_name.'/confirm_proses');
        $data['form'] = $this->class_name.'/form.tpl';
        $this->twig->display('base/page_form.tpl', $data);

    }

    public function confirm_proses() {
        $id_enc = $this->input->post('id');
        $status = $this->input->post('status');
        $id = url_base64_decode($id_enc);

        $this->db->trans_start();
        switch ($status) {
            

            case 1:
                $data['title'] = 'SEND REQUEST';
                $data['status'] = 1;
                break;
            
            case 2:
                $data['title'] = 'REVISI REQUEST';
                $data['status'] = 2;
                break;

            case 4:
                $data['title'] = 'REJECT REQUEST';
                $data['status'] = 4;
                break;

            case 5:
                $this->likuidasi_model->set('status',mutasistatus::APPROVE)->where('likuidasi_id',$id)->update();
                break;

            case 9:
                $this->likuidasi_model->set('status',mutasistatus::REJECT)->where('likuidasi_id',$id)->update();
                break;

            default:
                # code...
                break;
        }
        
        if($this->db->trans_status()){
            $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh_filter()');
        }else{
            $message = array(false, 'Terjadi Kesalahan',$this->db->_error_message(), '');
        }
        $this->db->trans_complete();
        //$message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh_filter()');
        echo json_encode($message);
    }

    

    public function edit($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('edit', true);
        $this->add($id);
    }

    public function delete($id_enc = '') {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('delete', true);
        $this->add($id,$status_delete = 1);
    }
    public function approve($id_enc = '') {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('edit', true);
        $this->add($id,$status_delete = 2);
    }

    public function proses() {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('data[store_id]', 'Outlet', 'required|trim');
            //$this->form_validation->set_rules('data[store_destination_id]', 'Destination Outlet', 'required|trim');
            $this->form_validation->set_rules('data[date]', 'Date', 'required|trim');
            $this->form_validation->set_rules('data[item_id]', 'Item', 'required|trim');
            $this->form_validation->set_rules('data[qty]', 'Qty', 'required|trim');

            if ($this->form_validation->run()) {
                $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');

                $id = $this->input->post('id');
                $data = $this->input->post('data');
                $data['status'] = mutasistatus::REQUEST;
                $data['qty'] = hgenerator::switch_number($data['qty']);
                $this->db->trans_start();
                if ($id){
                    $this->likuidasi_model->update($data, $id);
                }else{
                    $this->likuidasi_model->create($data);
                    
                }
                
                if($this->db->trans_status()){
                    $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh_filter()');
                }else{
                    $message = array(false, 'Terjadi Kesalahan',$this->db->_error_message(), '');
                }
                $this->db->trans_complete();
            } else {
                $message = array(false, 'Terjadi Kesalahan', validation_errors());
            }
            echo json_encode($message);
        } else {
            $this->access_right->redirect();
        }
    }

    public function proses_delete() {
        $id=$this->input->post("id");
		$this->access_right->otoritas('delete', true);
        if ($this->access_right->otoritas('delete', true)) {
            $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');
            if ($this->likuidasi_model->delete($id)) {
                $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', 'refresh_filter()');
				/* INSERT LOG */
				$this->access_right->activity_logs('delete','Delete Merchandise');
				/* END OF INSERT LOG */
            }
            echo json_encode($message);
        }
    }






}

/* End of file bbm.php */
/* Location: ./application/controllers/bbm.php */

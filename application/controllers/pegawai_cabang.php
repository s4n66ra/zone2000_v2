<?php

/**
 * Description of bbm
 *
 * @author SANGGRA HANDI
 */
class pegawai_cabang extends My_Controller {

    public function __construct() {
        parent::__construct();
        $this->class_name = get_class($this);
    }

    
    public function index() {
        $data['title'] = 'Employees and Their Store';
        $data['data_source'] = base_url($this->class_name . '/load');

        /* INSERT LOG */
        $this->access_right->activity_logs('view',$data['title']);

        $data['button_group'] = $this->getAvailableButtons();
        $data['button_right'] = $this->getTools();

        /* END OF INSERT LOG */
        $data['assets_url'] = $this->config->item('assets_url');
        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'base/page_content.tpl';
        $data['nama_pegawai'] = $this->currentUsername;

        $this->twig->display('index.tpl', $data);
    }

    public function load($page = 0) {
        $this->load->library("custom_table");

        $table = new stdClass();
        $header[0] = array(
                            "EMPLOYEES NAME",1,1,
                            "STORE NAME", 1, 1
                        );

        if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
            $header[0] = array(
                            "EMPLOYEES NAME",1,1,
                            "STORE NAME", 1, 1,
                            "ACTION",1,1
                            );
        }

        $table->header = $header;
        $table->id     = 't_bbm';
        $table->align = array('tanggal_bbm' => 'center', 'kd_bbm' => 'center','kd_purchase_order'=>'center','supplier'=>'center', 'aksi' => 'center');
        $table->style = "table table-striped table-bordered table-hover datatable dataTable";
        $table->model = "pegawai_cabang_model->data_table";
        $table->limit = $this->limit;
        $table->page = $page;
        $data = $this->custom_table->generate_ajax($table);
        echo json_encode($data);
    }


    public function add($id = '', $status_delete=0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->load->model('store_model');
            $this->load->model('pegawai_model');

            $title = 'Tambah Data';
            $data['form_action'] = $this->class_name . '/proses';

            $data['id'] = $id;
            if ($id != '') {
                $title = 'Edit Data';
                $row = $this->pegawai_cabang_model->get_by_id($id)->row();
                $data['data'] = $row;
            }

            $data['title'] = $title;
            $data['assets_url'] = $this->config->item('assets_url');
            $data['sidebar'] = $this->access_right->menu();
            $data['form']       = $this->class_name.'/form_pegawai_cabang.tpl';
            
            if($status_delete == 0){
                $data['form_action']    = view::form_input($id);
                $button_group[]         = view::button_save();
            }else{
                $data['title']          = 'Delete Store';
                $data['form_action']    = view::form_delete($id);;
                $data['readonly']       = 'readonly=""';
                $button_group[]         = view::button_delete_confirm();
            }



            // dropdown island, province, city, area
            $htmlOptions            =  'class="form-control select2me" data-placeholder="--Select Store--"';
            $data['list_cabang']  = form_dropdown('id_cabang', $this->store_model->options_empty(), $row->id_cabang, $htmlOptions);

            $htmlOptions            =  'class="form-control select2me" data-placeholder="--Select Employee--"';
            $data['list_pegawai']  = form_dropdown('id_pegawai', $this->pegawai_model->options_empty(), $row->id_pegawai, $htmlOptions);


            $data['button_group'] = view::render_button_group($button_group, array('class'=>'pull-right'));            
            $this->twig->display('base/page_form.tpl', $data);

        } else {
            $this->access_right->redirect();
        }
    }

    public function edit($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('edit', true);
        $this->add($id);
    }

    public function delete($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('delete', true);
        $this->add($id,$status_delete = 1);
    }

    public function proses() {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('id_pegawai', 'Kode pegawai_cabang', 'required|trim');

            if ($this->form_validation->run()) {
                $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');

                $id = $this->input->post('id');

                $data = array(
                    'id_pegawai' => $this->input->post('id_pegawai'),
                    'id_cabang' => $this->input->post('id_cabang')
                    );

                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */

                if ($id == '') {
                    if ($this->pegawai_cabang_model->create($data)) {
                        $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh_filter()');
						/* INSERT LOG */
						$this->access_right->activity_logs('add','Tambah pegawai_cabang');
						/* END OF INSERT LOG */
                    }
                } else {
                    if ($this->pegawai_cabang_model->update($data, $id)) {
                        $message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', 'refresh_filter()');
						/* INSERT LOG */
						$this->access_right->activity_logs('edit','Edit pegawai_cabang');
						/* END OF INSERT LOG */
                    }
                }
            } else {
                $message = array(false, 'Terjadi Kesalahan', validation_errors(), '');
            }
            echo json_encode($message);
        } else {
            $this->access_right->redirect();
        }
    }


    public function proses_delete() {
        $id = $this->input->post('id');
		$this->access_right->otoritas('delete', true);
        if ($this->access_right->otoritas('delete', true)) {
            $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');
            if ($this->pegawai_cabang_model->delete($id)) {
                $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', 'refresh_filter()');
				/* INSERT LOG */
				$this->access_right->activity_logs('delete','Delete BBM');
				/* END OF INSERT LOG */
            }
            echo json_encode($message);
        }
    }

}

/* End of file bbm.php */
/* Location: ./application/controllers/bbm.php */

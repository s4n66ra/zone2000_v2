<?php

/**
 * Description of bbm
 *
 * @author SANGGRA HANDI
 */
class owner extends My_Controller {


    public function __construct() {
        parent::__construct();
    }

    
    public function index() {
        $data['title'] = 'Master owner';
        $data['data_source'] = base_url($this->class_name . '/load');

        /* INSERT LOG */
        $this->access_right->activity_logs('view',$data['title']);
        /* END OF INSERT LOG */

        $button_group    = array(
                view::button_add(array('full-width' => 1)), // btn add default
            );
        $data['button_group'] = view::render_button_group($button_group);

        $data['assets_url'] = $this->config->item('assets_url');
        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'base/page_content.tpl';
        $data['nama_pegawai'] = $this->currentUsername;
    
        $this->twig->display('index.tpl', $data);
    }

    public function load($page = 0) {
        $this->load->library("custom_table");

        $table = new stdClass();
        $header[0] = array(
                            "OWNER CODE",1,1,
                            "OWNER NAME", 1, 1
                        );

        if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
            $header[0] = hgenerator::merge_array_raw(
                            $header[0],
                            array("ACTION",1,1)
                        );
        }

        $table->header = $header;
        $table->id     = 't_bbm';
        $table->align = array('tanggal_bbm' => 'center', 'kd_bbm' => 'center','kd_purchase_order'=>'center','supplier'=>'center', 'aksi' => 'left');
        $table->style = "table table-striped table-bordered table-hover datatable dataTable";
        $table->model = "owner_model->data_table";
        $table->limit = $this->limit;
        $table->page = $page;
        $data = $this->custom_table->generate_ajax($table);

        echo json_encode($data);

    }

    public function get_detail_bbm($id_bbm) {
        $data['title'] = '<i class="icon-plus-sign-alt"></i> Master owner';
        $data['page'] = $this->class_name . '/get_detail_bbm';
        $data['data_source'] = base_url($this->class_name . '/load_detail_bbm');

 
        /* Otoritas Tombol tambah */
 
        $data['button_group'] = array();
        if ($this->access_right->otoritas('add')) {
            $data['button_group'] = array(
                anchor(null, '<i class="icon-plus"></i> Tambah Data Detail', array('id' => 'button-add-detail-'.$id_bbm, 'class' => 'btn yellow', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url($this->class_name . '/add_detail/'.$id_bbm)))
            );
        }

        //=========tombol cetak excel==========
        $base_url_images = base_url() . 'images';
        $base_url = base_url() . $this->class_name;
        if ($this->access_right->otoritas('print')) {
            $data['print_group'] = array(
                anchor(null, '<img src="' . $base_url_images . '/doc_excel.png" />', array('id' => 'button-print-excel-'.$id_bbm, 'class' => 'btn outline pull-right', 'rel' => 'tooltip', 'title' => 'Cetak Laporan ke Excel', 'onclick' => 'do_print(this.id, \'#ffilter_detail_'.$id_bbm.'\')', 'data-source' => $base_url . '/excel_detail/'.$id_bbm))
            );
        }
            //-------------------------------------
        $this->load->model('referensi_satuan_model');
        $data['options_satuan'] = $this->referensi_satuan_model->options();
         
         $data['id_bbm'] = $id_bbm;
        /* INSERT LOG */
        $this->access_right->activity_logs('view','Bukti Barang Masuk');
        /* END OF INSERT LOG */
        $this->load->view('bbm/detail_bbm', $data);
    }

    public function load_detail_bbm($page = 0) {
        $this->load->library("custom_table_detail");

        $table = new stdClass();
        $header[0] = array(
                            "KODE BARANG",1,1,
                            "NAMA BARANG",1,1,
                            "JUMLAH", 1, 1, 
                            "SATUAN", 1, 1, 
                            "KATEGORI",1,1
                        );

        if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
            $header[0] = array(
                            "KODE BARANG",1,1,
                             "NAMA BARANG",1,1,
                            "JUMLAH", 1, 1, 
                            "SATUAN", 1, 1, 
                            "KATEGORI",1,1,
                            "AKSI",1,1
                            );
        }

        $table->header = $header;
        $table->id     = 't_bbm_detail';
        $table->align = array('tanggal_bbm' => 'center', 'jumlah' => 'center','satuan'=>'center','kategori'=>'center', 'aksi' => 'center');
        $table->style = "table table-striped table-bordered table-hover datatable dataTable";
        $table->model = "owner_model->data_table_detail";
        $table->limit = $this->limit;
        $table->page = $page;
        $data = $this->custom_table_detail->generate($table);

        echo json_encode($data);
    }

    public function add($id='',$status_delete = 0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->load->model('owner_model');
            $this->load->model('pegawai_model');

            $title = 'Add Data';
            $data['form_action'] = $this->class_name . '/proses';

            $data['id'] = $id;
            if ($id != '') {
                $title = 'Edit Data';
                $row = $this->owner_model->get_by_id($id)->row();
                $data['data'] = $row;
            }

            $data['title']      = $title;
            $data['assets_url'] = $this->config->item('assets_url');
            $data['sidebar']    = $this->access_right->menu();
            $data['form']       = $this->class_name.'/form_owner.tpl';


            $button_group   = array();
            $button_group[] = view::button_back();

            if($status_delete == 0){
                $data['form_action']    = view::form_input($id);
                $button_group[]         = view::button_save();
            }else{
                $data['title']          = 'Delete Store';
                $data['form_action']    = view::form_delete($id);;
                $data['readonly']       = 'readonly=""';
                $button_group[]         = view::button_delete_confirm();
            }


            // dropdown island, province, city, owner
            $htmlOptions            =  'class="form-control select2me" data-placeholder="Select Employees"';
            $data['list_employees']  = form_dropdown('id_ka_owner', $this->pegawai_model->options_empty(), $row->id_pegawai, $htmlOptions);

            $data['button_group'] = view::render_button_group($button_group, array('class'=>'pull-right'));            
            $this->twig->display('base/page_form.tpl', $data);

        } else {
            $this->access_right->redirect();
        }
    }

    public function edit($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('edit', true);
        $this->add($id);
    }

    public function delete($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('delete', true);
        $this->add($id,$status_delete = 1);
    }

    public function proses() {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('kd_owner', 'Kode owner', 'required|trim');
            $this->form_validation->set_rules('nama_owner', 'Nama owner', 'required|trim');


            if ($this->form_validation->run()) {
                $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');

                $id = $this->input->post('id');

                $data = array(
                    'kd_owner' => $this->input->post('kd_owner'),
                    'nama_owner' => $this->input->post('nama_owner')
                );

                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */

                if ($id == '') {
                    if ($this->owner_model->create($data)) {
                        $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh_filter()');
						/* INSERT LOG */
						$this->access_right->activity_logs('add','Tambah BBM');
						/* END OF INSERT LOG */
                    }
                } else {
                    if ($this->owner_model->update($data, $id)) {
                        $message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', 'refresh_filter()');
						/* INSERT LOG */
						$this->access_right->activity_logs('edit','Edit BBM');
						/* END OF INSERT LOG */
                    }
                }
            } else {
                $message = array(false, 'Terjadi Kesalahan', validation_errors(), '');
            }
            echo json_encode($message);
        } else {
            $this->access_right->redirect();
        }
    }

    public function proses_delete() {
        $id = $this->input->post('id');
        $this->access_right->otoritas('delete', true);
        if ($this->access_right->otoritas('delete', true)) {
            $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');
            if ($this->owner_model->delete($id)) {
                $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', 'refresh_filter()');
                /* INSERT LOG */
                $this->access_right->activity_logs('delete','Delete BBM');
                /* END OF INSERT LOG */
            }
            echo json_encode($message);
        }
    }

    public function add_detail($id_bbm = '') {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {

            $title = 'Tambah Data';
            $data['form_action'] = $this->class_name . '/proses_detail';

            $data['id'] = '';
            $data['id_bbm'] = $id_bbm;

        $data['title'] = '<i class="icon-edit"></i> ' . $title;
        $this->load->model('referensi_barang_model');
        $this->load->model('referensi_purchase_order_model');
        $data['options_barang'] = $this->referensi_barang_model->options();
        $data['options_purchase_order'] = $this->referensi_purchase_order_model->options();
            

            $this->load->view($this->class_name . '/form_detail', $data);
        } else {
            $this->access_right->redirect();
        }
    }

    public function edit_detail($id_bbm,$id) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->load->model('owner_model');

            $title = 'Tambah Data';
            $data['form_action'] = $this->class_name . '/proses_detail';

            $data['id'] = $id;
            $data['id_bbm'] = $id_bbm;
            if ($id != '') {
                $title = 'Edit Data Detail';

                $db = $this->owner_model->get_detail_by_id($id);
                if ($db->num_rows() > 0) {
                    $row = $db->row();
                    $data['default'] = $row;
                }
            }

        $data['title'] = '<i class="icon-edit"></i> ' . $title;
        $this->load->model('referensi_barang_model');
        $this->load->model('referensi_purchase_order_model');
        $data['options_barang'] = $this->referensi_barang_model->options();
        $data['options_purchase_order'] = $this->referensi_purchase_order_model->options();
            

            $this->load->view($this->class_name . '/form_detail', $data);
        } else {
            $this->access_right->redirect();
        }
    }

    public function proses_detail() {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('kd_barang', 'Nama Barang', 'required|trim');
            $this->form_validation->set_rules('jumlah', 'Jumlah', 'required|trim');


            if ($this->form_validation->run()) {
                $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');

                $id = $this->input->post('id');

                $data = array(
                    'id_barang' => $this->input->post('kd_barang'),
                    'jumlah_barang' => $this->input->post('jumlah'),
                );

                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */

                if ($id == '') {
                    $data['id_bbm'] = $this->input->post('id_bbm');
                    if ($this->owner_model->create_detail($data)) {
                        $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh_filter()');
                        /* INSERT LOG */
                        $this->access_right->activity_logs('add','Tambah BBM');
                        /* END OF INSERT LOG */
                    }
                } else {
                    if ($this->owner_model->update_detail($data, $id)) {
                        $message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', 'refresh_filter()');
                        /* INSERT LOG */
                        $this->access_right->activity_logs('edit','Edit BBM');
                        /* END OF INSERT LOG */
                    }
                }
            } else {
                $message = array(false, 'Terjadi Kesalahan', validation_errors(), '');
            }
            echo json_encode($message);
        } else {
            $this->access_right->redirect();
        }
    }

    public function delete_detail($id_bbm,$id) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->load->model('owner_model');

            $title = 'Hapus Data';
            $data['form_action'] = $this->class_name . '/proses_delete_detail/'.$id;

            $data['id'] = $id;
            $data['id_bbm'] = $id_bbm;
            if ($id != '') {
                $title = 'Edit Data Detail';

                $db = $this->owner_model->get_detail_by_id($id);
                if ($db->num_rows() > 0) {
                    $row = $db->row();
                    $data['default'] = $row;
                }
            }

        $data['title'] = '<i class="icon-edit"></i> ' . $title;
        $this->load->model('referensi_barang_model');
        $this->load->model('referensi_purchase_order_model');
        $data['options_barang'] = $this->referensi_barang_model->options();
        $data['options_purchase_order'] = $this->referensi_purchase_order_model->options();
            

            $this->load->view($this->class_name . '/form_detail_delete', $data);
        } else {
            $this->access_right->redirect();
        }
    }

    public function proses_delete_detail($id) {
        $this->access_right->otoritas('delete', true);
        if ($this->access_right->otoritas('delete', true)) {
            $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');
            if ($this->owner_model->delete_detail($id)) {
                $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', 'refresh_filter()');
                /* INSERT LOG */
                $this->access_right->activity_logs('delete','Delete BBM');
                /* END OF INSERT LOG */
            }
            echo json_encode($message);
        }
    }

    public function excel($page = 0){

        $this->load->library("custom_table");

        $table = new stdClass();
        $header[0] = array(
                            "TANGGAL BBM",1,1,
                            "KODE BBM", 1, 1, 
                            "KODE PURCHASE ORDER", 1, 1, 
                            "SUPPLIER",1,1
                        );
        $table->header = $header;
        $table->id     = 't_bbm';
        $table->align = array('tanggal_bbm' => 'center', 'kd_bbm' => 'center','kd_purchase_order'=>'center','supplier'=>'center', 'aksi' => 'center');
        $table->style = "table table-striped table-bordered table-hover datatable dataTable";
        $table->model = "owner_model->data_table_excel";
        $table->limit = $this->limit;
        $table->border = 1;
        $table->page = 1;
        $data = $this->custom_table->generate($table);
        
        //===========filter di excel===================
        $id_supplier= $this->input->post("kd_supplier");
        $id_purchase_order = $this->input->post('kd_po');
        $tanggal_awal = $this->input->post('tanggal_awal_bbm');
        $tanggal_akhir = $this->input->post('tanggal_akhir_bbm');
        $tahun_aktif = $this->input->post('tahun_aktif');
        $kd_bbm = $this->input->post('kd_bbm');
        if(empty($kd_bbm)){
            $bbm = 'Semua';
        }else{
            $bbm = $kd_bbm;
        }
        if(empty($tahun_aktif)){
            $tahun_aktif = date('Y');
        }
        if($id_supplier==''){
            $supplier = 'Semua';
        }else{
            $supplier       = $id_supplier;
        }
        if($id_purchase_order==''){
            $po = 'Semua';
        }else{
            $po = $id_purchase_order;
        }
        if($tanggal_awal==''||$tanggal_akhir==''){
            $tanggal_awal = 'Semua';
            $tanggal_akhir = 'Semua';
        }
        
        $filter = array(
            'Kode BBM'          =>$bbm,
            'Purchase Order'    =>$po,
            'Supplier'          =>$supplier,
            'Periode Awal'      =>$tanggal_awal,
            'Periode Akhir'     =>$tanggal_akhir,
            'Tahun Aktif'       =>$tahun_aktif);
        $data['filter'] = $filter;
        //----------------------------------------------------
        $date = date('Ymd His');
        $data['judul_kecil'] = 'Bukti Barang Masuk';
        $data['filename'] = 'Bukti Barang Masuk ('.$date.')';
        $this->load->view('cetak_excel',$data);
    }

    public function excel_detail($id_bbm) {
        $this->load->library("custom_table_detail");

        $table = new stdClass();
        $header[0] = array(
                            "KODE BARANG",1,1,
                            "NAMA BARANG",1,1,
                            "JUMLAH", 1, 1, 
                            "SATUAN", 1, 1, 
                            "KATEGORI",1,1
                        );

        $table->header = $header;
        $table->id     = 't_bbm_detail';
        $table->align = array('tanggal_bbm' => 'center', 'jumlah' => 'center','satuan'=>'center','kategori'=>'center', 'aksi' => 'center');
        $table->style = "table table-striped table-bordered table-hover datatable dataTable";
        $table->model = "owner_model->data_table_detail_excel";
        $table->limit = $this->limit;
        $table->page = 1;
        $table->border = 1;
        $data = $this->custom_table_detail->generate($table);
        //============data detail==================
        $data_bbm = $this->owner_model->get_by_id($id_bbm);
        if($data_bbm->num_rows()>0){
            $bbm = $data_bbm->row();
            $filter = array(
                'Tanggal BBM'       =>$bbm->tanggal_bbm,
                'Kode BBM'          =>$bbm->kd_bbm,
                'Kode PO'           =>$bbm->kd_po,
                'Kode Supplier'     =>$bbm->nama_supplier.' ('.$bbm->kd_supplier.')');
            $data['filter'] = $filter;
        }
        //------------------------------------------------
        
        $date = date('Ymd His');
        $data['judul_kecil'] = 'Bukti Barang Masuk Detail';
        $data['filename'] = 'Bukti Barang Masuk Detail ('.$date.')';
        $this->load->view('cetak_excel',$data);
    }



}

/* End of file bbm.php */
/* Location: ./application/controllers/bbm.php */

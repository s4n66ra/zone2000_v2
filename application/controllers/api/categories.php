<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Categories extends REST_Controller{
	function __construct(){
        parent::__construct();
		$this->load->model('admin/categories_model');
    }
	
	function remove_post(){
		$post = array('category_id' => filter_var($this->post('category_id'), FILTER_SANITIZE_NUMBER_INT));
		
		$this->categories_model->removeCategories($post);
		
		$response = array(
			'status' => 'OK'
		);
		
		$this->response($response, 200);
	}
	
	function save_post(){
		$response = array();
	
		$post = array(
			'action' 			 => filter_var($this->post('action'), FILTER_SANITIZE_STRING),
			'base_category_id' 	 => filter_var($this->post('base_category_id'), FILTER_SANITIZE_NUMBER_INT),
			'category_id' 	 => filter_var($this->post('category_id'), FILTER_SANITIZE_NUMBER_INT),
			'category_name' => filter_var($this->post('category_name'), FILTER_SANITIZE_STRING),
			'category_description' => filter_var($this->post('category_description'), FILTER_SANITIZE_STRING)
		);
		
		if($post['action'] == 'modify_category'){
			$this->categories_model->modifyCategories($post);

			$response = array(
				'status' => 'OK',
				'category_id' => $post['category_id']
			);
		}else{
			$lastId = $this->categories_model->createCategories($post);
			
			$response = array(
				'status' => 'OK',
				'category_id' => $lastId
			);
		}
		
		$this->response($response, 200);
	}
    
    function categories_list_post(){
		$list = $this->categories_model->getCategoriesList();
		
		$response = array(
			'status' => 'OK',
			'categories_list' => $list
		);
        $this->response($response, 200);
    }

	public function send_post(){
		var_dump($this->request->body);
	}

	public function send_put(){
		var_dump($this->put('foo'));
	}
}
<?php defined('BASEPATH') OR exit('No direct script access allowed');


require APPPATH.'/libraries/REST_Controller.php';

class Merchandise extends REST_Controller
{
	function __construct()
    {
        // Construct our parent class
        parent::__construct();
        

    }

    public function validate($param){
        $_POST = $param;
        $this->load->library('form_validation');

        $config = array(
           array(
                 'field'   => 'user_id', 
                 'label'   => 'User ID', 
                 'rules'   => 'required'
              ),
           array(
                 'field'   => 'store_id', 
                 'label'   => 'Store ID', 
                 'rules'   => 'required'
              ),
           array(
                 'field'   => 'item_key', 
                 'label'   => 'Item Key', 
                 'rules'   => 'required'
              ),
        );
        $this->form_validation->set_error_delimiters('', '');
        $this->form_validation->set_rules($config);
        return $this->form_validation->run();
    }
 
    public function getDetailMerchandise_post(){
        $this->load->model(array('hadiah_model','transaction_model'));

        $item_key       = $this->post('item_key');

        if($this->validate($this->post())){
            // get machine_id (id_mesin) dulu untuk mencari transaksi terakhir
            $item = $this->hadiah_model
                        ->where('item_key', $item_key)
                        ->get()->row();

            // jika mesin ditemukan
            if($item->item_key){
                $detail['item_id'] = $item->id_hadiah;
                $detail['item_name']=$item->nama_hadiah;
                $detail['ticket_needed']=$item->jumlah_tiket;

                $return['status'] = 'OK';
                $return['message']= 'Item Found';
                $return['detail_item'] = $detail;
            }else{
                $return['status'] = 'ERROR';
                $return['message']= 'Item Not Found';
            }
        }else{
            $return['status'] = 'ERROR';
            $return['message']= validation_errors();
        }

        $this->response($return, 200);
    }
}
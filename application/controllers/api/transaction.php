<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Transaction extends REST_Controller
{	
	function __construct()
    {
        // Construct our parent class
        parent::__construct();
        // Configure limits on our controller methods. Ensure
        // you have created the 'limits' table and enabled 'limits'
        // within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; //500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; //100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; //50 requests per hour per user/key
		$this->load->helpers('itemtype');
		$this->load->helpers('transactiontype');
		$this->load->model("item_stok_model");
		$this->load->model("item_model");
    }

    public function numcheck($in){
        $num_start = $_POST['num_start'];
        $num_end    = $_POST['num_end'];
        $this->form_validation->set_message('numcheck', ' Num End Must Larger than Num Start');
        return $num_end>=$num_start;
    }

    public function validate($param){
        $_POST = $param;
        $this->load->library('form_validation');

        $config = array(
           array(
                 'field'   => 'user_id', 
                 'label'   => 'User ID', 
                 'rules'   => "required"
              ),
           
           array(
                 'field'   => 'store_id', 
                 'label'   => 'Store ID', 
                 'rules'   => 'required'
              ),
           array(
                 'field'   => 'machine_number', 
                 'label'   => 'Machine Number', 
                 'rules'   => 'required'
              ),
           array(
                 'field'   => 'type_id', 
                 'label'   => 'Type ID', 
                 'rules'   => 'required'
              ),
           array(
                 'field'   => 'device_id', 
                 'label'   => 'Device ID', 
                 'rules'   => 'required'
              ),
           array(
                 'field'   => 'machine_id', 
                 'label'   => 'Machine ID', 
                 'rules'   => 'required'
              ),
           array(
                 'field'   => 'num_coin', 
                 'label'   => 'Num Coin', 
                 'rules'   => 'is_natural'
              ),
           array(
                 'field'   => 'num_meter', 
                 'label'   => 'Num Meter', 
                 'rules'   => 'is_natural'
              ),
           array(
                 'field'   => 'num_ticket', 
                 'label'   => 'Num Ticket', 
                 'rules'   => 'is_natural'
              ),
           array(
                 'field'   => 'num_start', 
                 'label'   => 'Num Start', 
                 'rules'   => 'is_natural'
              ),
           array(
                 'field'   => 'num_end', 
                 'label'   => 'Num End', 
                 'rules'   => 'is_natural|callback_numcheck'
              ),
           
        );
        $this->form_validation->set_error_delimiters('', '');
        $this->form_validation->set_rules($config);
        return $this->form_validation->run();
    }

    public function validateItem($param){
        $_POST = $param;
        $config = array(
           array(
                 'field'   => 'item_qty', 
                 'label'   => 'Item Qty', 
                 'rules'   => "required|is_natural"
              ),
           array(
                 'field'   => 'item_id', 
                 'label'   => 'Item Id', 
                 'rules'   => "required"
              )
           );
        $this->form_validation->set_error_delimiters('', '');
        $this->form_validation->set_rules($config);        
        return $this->form_validation->run();
    }

    public function multi_post($type){
        switch ($type) {
            case 'save':
                $post = $this->post();
                
                $param['type_id']   = $post['type_id'];
                $param['store_id']  = $this->session->userdata('store_id');
                $param['user_id']   = $this->session->userdata('user_id');
                $now = date::now_sql();
                $sistem_date = $this->session->userdata('sistem_date');
                $param['rectime']   = !empty($sistem_date)?$this->session->userdata('sistem_date'):$now;
                
                $param['device_id'] = 1; //dari web
                $param['machine_number'] = 'xx';

                //test
                $test;

                $data = $post['data'];
                if(empty($data)){
                  $message = array("success"=>false,"message"=>'Klik tombol ADD NEW terlebih dahulu');
                  echo json_encode($message);
                  break;
                }
                if ($post['type_id']==transactiontype::MFIL || $post['type_id']==transactiontype::MFILO || $post['type_id']==transactiontype::MRPL || $post['type_id']==transactiontype::BYS || $post['type_id']==transactiontype::BYSC || $post['type_id']==transactiontype::MTSI || $post['type_id']==transactiontype::SELL || $post['type_id']==transactiontype::PRMO || $post['type_id']==transactiontype::MRDM || $post['type_id']==transactiontype::MRDM_REG ) {
                    foreach ($data as $key => $value) {
                        $data[$key]['item_qty'] = $data[$key]['qty1'] ;
                        unset($data[$key]['qty1']);
                    }
                    if($post['type_id']==transactiontype::MRPL){
                      $param['store_id']      = !empty($post['bundle_id']) ? $post['bundle_id'] : $this->session->userdata('store_id');
                      $param['machine_id']    = $post['bundle_id'];
                    }elseif($post['type_id']==transactiontype::MRDM || $post['type_id']==transactiontype::MRDM_REG){
                      $param['machine_id']    = 'rdm';
                    }elseif($post['type_id']==transactiontype::TCUT){
                      $param['machine_id']    = 'cut';
                    }elseif($post['type_id']==transactiontype::BYS){
                      $param['machine_id']    = 'bys';
                    }elseif($post['type_id']==transactiontype::BYSC){
                      $param['machine_id']    = 'bysc';
                    }elseif($post['type_id']==transactiontype::MTSI){
                      $param['machine_id']    = 'mtsi';
                    }elseif($post['type_id']==transactiontype::SELL){
                      $param['machine_id']    = 'sell';
                    }elseif($post['type_id']==transactiontype::PRMO){
                      $param['machine_id']    = 'prmo';
                    }else{
                      $param['machine_id']    = $post['bundle_id'];
                    }
                    
                    $param['items']         = json_encode($data);
                    $this->save_post($param);
                    //print_r($param);
                } else {
                    foreach ((array)$post['data'] as $key => $value) {
                        $param['machine_id'] = $value['item_id'];
                        if($post['type_id']==transactiontype::TCUT){
                          $param['machine_id']    = 'cut';
                        }
                        $value['qty1'] = $value['qty1'] ;
                        $value['qty2'] = $value['qty2'] ;
                        $value['qty3'] = $value['qty3'] ;

                        switch ($post['type_id']) {
                            case transactiontype::COUT:
                                //$param['num_coin']   = $value['qty1'];
                                $param['num_start']     = $value['qty1'];
                                $param['num_end']       = $value['qty2'];
                                $param['num_coin']    = $value['qty3'];
                                $param['num_ticket']  = $value['qty2'] - $value['qty1']-$value['qty3'];// as num_non-CH
                                break;

                            case transactiontype::CADD:
                                $param['num_coin']   = $value['qty1'];
                                break;

                            case transactiontype::TFIL:
                                $param['num_start']     = $value['qty2'];
                                $param['num_end']       = $value['qty3'];
                                $param['num_meter']  = $value['qty3'] - $value['qty2'] + 1;
                                $param['num_ticket']  = $value['qty1'];
                                break;

                            case transactiontype::TPAYO:
                                $param['num_start']     = $value['qty1'];
                                $param['num_end']       = $value['qty2'];
                                $param['num_meter']  = $value['qty2'] - $value['qty1'];
                                $param['num_ticket']  = $value['qty2'] - $value['qty1'];
                                break;
                            
                            default:
                                $param['num_meter']  = $value['qty1'];
                                break;
                        }
                        
                        $return = $this->save_post($param);
                    }
                }
                    
                if ($this->db->trans_status()){
                    $message = array("success"=>true,"message"=>'transaksi berhasil');
                }else{
                    $message = array("success"=>false,"message"=>$this->db->_error_message());
                }
                echo json_encode($message);
                break;
            
            default:
                # code...
                break;
        }
    }

    public function save_post($post = array()){
    	$this->load->model('item_stok_model');
        // machine number untuk pengecekan di database
        if(!$post)
            $post = $this->post();

        $type_id = $post['type_id'];
        $this->load->model(array('transaction_model'));
        $validation = $this->validate($post);
        
        $this->db->trans_begin();

        if($validation === true){            
            // insert to transaction
            $data = array();
            $data['transactiontype_id']     = $post['type_id']; // COIN
            $data['rec_user']               = $post['user_id'];        
            $data['rec_created']            = $post['rectime'];

            $data['num_coin']               = $post['num_coin'];
            $data['num_ticket']             = $post['num_ticket'];
            $data['num_meter']              = $post['num_meter'];
            $data['num_start']              = $post['num_start'];
            $data['num_end']                = $post['num_end'];
            //==========================================================================
            if($type_id == transactiontype::TPAYO){
              $num_start = $data['num_start'];
              $num_end = $data['num_end'];
              $machine_id = $post['machine_id'];
              $tot_awal = $this->transaction_model->getTpayoAwal($num_start,$machine_id);
              //echo $tot_awal.'1212';
              $tot_mid = $this->transaction_model->getTpayoMid($num_start,$num_end,$machine_id);
              $tot_akhir = $this->transaction_model->getTpayoAkhir($num_end,$machine_id);
              $data['num_ticket'] = $tot_awal + $tot_mid + $tot_akhir + 1;
              $data['num_meter'] = $data['num_ticket'];
            }

            $this->db->insert('zn_transaction', $data);
            if($type_id == transactiontype::TFIL){
              $data['machine_id'] = $post['machine_id'];
              $data['store_id'] = $post['store_id'];
              $this->db->insert('zn_transaction_ticket_refill', $data);
            }
            
            //echo $this->db->last_query();
            $transaction_id = $this->db->insert_id();

            // insert to transaction machine
            $data = array();
            $data['transaction_id'] = $transaction_id;
            if($type_id != transactiontype::MRPL){
              $data['machine_id']     = $post['machine_id'];
              $this->db->insert('zn_transaction_machine', $data);
            }

            // insert to transaction device table
            if ($post['device_id']) {
                $data = array();
                $data['transaction_id']         = $transaction_id;
                $data['device_id']              = $post['device_id'];
                $data['device_transaction_id']  = $post['transaction_id'];
                $this->db->insert('zn_transaction_device', $data);
            }

            // insert to transaction store table
            $data = array();
            $data['transaction_id']         = $transaction_id;
            $data['store_id']               = $post['store_id'];
            $this->db->insert('zn_transaction_store', $data);
            switch ($type_id) {
                // METER : insert to TRANSACTION_METER
                case ( $type_id == transactiontype::COIN || $type_id == transactiontype::COUT || $type_id == transactiontype::TOUT || $type_id == transactiontype::MOUT || $type_id == transactiontype::TCUT ):

                    if($post['num_meter']>0){
                        $meter = array();
                        $meter_row = $this->transaction_model
                            ->with(array('transaction_machine','transaction_meter'))
                            ->where('machine_id', $post['machine_id'])
                            ->where('transactiontype_id',$post['type_id'])
                            ->order_by('zn_transaction_meter.transaction_id','desc')
                            ->get()->row();
                            
                        $meter['transaction_id'] = $transaction_id;
                        $meter['num_initial']    = ($meter_row->num_current ? $meter_row->num_current : 0);            
                        $meter['num_current']    = $post['num_meter'];
                        $meter['num_diff']       = abs($meter['num_current'] - $meter['num_initial']);
                        $this->db->insert('zn_transaction_meter', $meter);
                        
                        if($type_id == 3){
                        	//TOUT
                        	$this->updateTicketOutStock($post['store_id'], $meter['num_diff']);
                        }
                    }
                    break;
                    
                case transactiontype::TFIL :
                	//TICKET REFILL, we use num_start and num_end
                	$refillAmount = $post['num_end'] - $post['num_start'] + 1;//$post['num_meter'];;//
                	$this->updateTicketRefillStock($post['store_id'], $refillAmount);
                	break;

/*                case transactiontype::TPAYO :
                  //TICKET REFILL, we use num_start and num_end
                  $refillAmount = $post['num_end'] - $post['num_start'];//$post['num_meter'];;//
                  $this->updateTicketRefillStock($post['store_id'], $refillAmount);
                  break;*/

                case ($type_id == transactiontype::SELL || $type_id == transactiontype::PRMO || $type_id == transactiontype::BYS || $type_id == transactiontype::BYSC || $type_id == transactiontype::MTSI || $type_id == transactiontype::MFIL || $type_id == transactiontype::MRDM || $type_id == transactiontype::MRPL || $type_id == transactiontype::MRDM_REG || $type_id == transactiontype::MFILO):
                	$this->processMerchandiseRedemptionAndRefill($post['items'], $transaction_id, $post['store_id'], $type_id);
                	break;
                	
                case transactiontype::SREQ :
                    // SERVICE REQUEST
                    $service['transaction_id'] = $transaction_id;
                    $service['issue_description'] = $post['issue_description'];
                    $this->db->insert('zn_transaction_service', $service);
                    break;

                case transactiontype::SREP :
                    $repair['transaction_id'] = $transaction_id;
                    $repair['repair_status']  = $post['repair_status'];
                    $this->db->insert('zn_transaction_repair', $repair);
                    break;
                    
                case transactiontype::TRDM:
                	$this->processTicketRedemption($post['items'], $numTiket, $storeId, $transactionId);
                	break;
                default:
                    # code...
                    break;
            }
            

            if($this->db->trans_status() === FALSE)
            {
                $return['success']= false;
                $return['status'] = 'ERROR';
                //$return['message']= 'Transaction Failed. Database Error Occurred';
                $return['message'] = $this->db->_error_message();
                $this->db->trans_rollback();
            }else{
                $return['success']= true;
                $return['status'] = 'OK';
                $return['message']= 'Transaction Success';
                $this->db->trans_commit();
            }
            
            //$this->response($return, 200);
            return $return;

        } else{
            // validation error 
            //$this->response(array('success'=>false, 'status'=>'ERROR', 'message'=>validation_errors()),200);
            return array('success'=>false, 'status'=>'ERROR', 'message'=>validation_errors());

        }
    }

    function get_tiket($harga=1,$type=2,$st_member = 1){
       //$this->load->model('range_tiket_model');
        if($st_member = 2){
          if($type == 2){
              $harga = $harga / 25;
          }else{
              $harga = $harga / 15;
          }
        }else{
          if($type == 2){
              $harga = $harga / 30;
          }else{
              $harga = $harga / 20;
          }
        }
        $data = $this->db
            ->select('min,max,kelipatan')
            ->from('m_range_tiket')
            ->where('min <',$harga)
            ->where('max >=',$harga)
            ->get()->row();
        $tiket = !empty($data->min) ? $data->min :0;
        while($harga > $tiket){
            $tiket += $data->kelipatan;
        }
        return $tiket;
    }

    function getTiketRedeem($item_id){
        if(!empty($item_id)){
            $this->load->model('item_model');
            $dataItem = $this->item_model->getById($item_id)->row();
            $st_member = 1;
            $data = $this->get_tiket($dataItem->harga,$dataItem->m_type,$st_member);
            $this->load->model('range_tiket_model');
            $dataTemp = $this->range_tiket_model
                ->getById($item_id)->row();
            return $data;
        }
    }

    function getTiketRedeemReg($item_id){
        if(!empty($item_id)){
            $this->load->model('item_model');
            $dataItem = $this->item_model->getById($item_id)->row();
            $st_member = 2;
            $data = $this->get_tiket($dataItem->harga,$dataItem->m_type,$st_member);
            $this->load->model('range_tiket_model');
            $dataTemp = $this->range_tiket_model
                ->getById($item_id)->row();
            return $data;
        }
    }

    public function __getTiketRedeem($item_id = 0,$store_id){
      $this->load->model(array('promosi_model','item_model','range_tiket_model'));
      $cekPromosi = $this->promosi_model
          ->select('qty_tiket')
          ->where('item_id',$item_id)
          ->where('store_id',$store_id)
          ->where('date_begin <=', date('Y-m-d'))
          ->where('date_end >=',date('Y-m-d'))
          ->get()->row();
      if(!empty($cekPromosi->qty_tiket)){
          $tiket = $cekPromosi->qty_tiket;              
      }else{
          $info = $this->item_model
              ->select('harga,m_type')
              ->where('item_id',$machine_id)
              ->get()->row();
          $price = !empty($info->harga)?$info->harga : 1;
          $m_type = !empty($info->m_type)?$info->m_type : 2;
          $tiket = $this->range_tiket_model->get_tiket($price,$m_type);
      }
      //===============================
      return $tiket;
    }

    
    private function processMerchandiseRedemptionAndRefill($itemsJson, $transaction_id, $store_id, $type_id){
      $this->load->model('transaction_model');
    	$items = json_decode($itemsJson, TRUE);
    	$items_data = array();
    	$stok_update_data = array();
    	$seq = 1;
    	$allValidationPassed = true;
    	foreach ($items as $key => $value) {
    		/* validasi item. jika ada error maka langsung keluar perulangan. */
    		if(!$this->validateItem($value)){
    			$allValidationPassed = false;
    			break;
    		}
    	
    		//build redemption insert data
    		$itemRedemptionRow = array();
    		$itemRedemptionRow['transaction_id'] = $transaction_id;
    		$itemRedemptionRow['item_id'] = $value['item_id'];
    		$itemRedemptionRow['item_key']= $value['item_key'];
    		$itemRedemptionRow['item_qty']= $value['item_qty'];
    		$itemRedemptionRow['seq']     = $seq;
        if($type_id == transactiontype::MRDM){
          $itemRedemptionRow['ticket_count']     = $this->getTiketRedeem($value['item_id'],$store_id);
        }

        if($type_id == transactiontype::MRDM_REG){
          $itemRedemptionRow['ticket_count']     = $this->getTiketRedeemReg($value['item_id'],$store_id);
        }

        if($type_id == transactiontype::BYS){
          $itemRedemptionRow['jenis_buying_id']     = 1; //=========Pembelian langsung MD
          $itemRedemptionRow['store_id']     = $this->session->userdata('store_id');
        }

        if($type_id == transactiontype::BYSC){
          $itemRedemptionRow['jenis_buying_id']     = 3; //========== Pembelian langsung Sparepart
          //$itemRedemptionRow['store_id']     = $this->session->userdata('store_id');
          $itemRedemptionRow['store_id']     = hprotection::getSC();
          $store_id = hprotection::getSC();
        }

        if($type_id == transactiontype::MTSI){
          $itemRedemptionRow['jenis_buying_id']     = 4; //========== Mutasi Masuk SC
          //$itemRedemptionRow['store_id']     = $this->session->userdata('store_id');
          $itemRedemptionRow['store_id']     = hprotection::getSC();
          $store_id = hprotection::getSC();
        }

        if($type_id == transactiontype::SELL){
          $itemRedemptionRow['jenis_selling_id']     = 1;
          $itemRedemptionRow['store_id']     = $this->session->userdata('store_id');
        }

        if($type_id == transactiontype::PRMO){
          $itemRedemptionRow['jenis_selling_id']     = 2;
          $itemRedemptionRow['store_id']     = $this->session->userdata('store_id');
        }

        if($type_id == transactiontype::MRDM){
          $itemRedemptionRow['st_member'] = 1;
        }elseif($type_id == transactiontype::MRDM_REG){
          $itemRedemptionRow['st_member'] = 0;
        }
    		$items_data[] = $itemRedemptionRow;
    		//endof-build redemption insert data
    		
    		//build stok update data
    		$currentStock = $this->item_stok_model
    			->with('item')
    			->where("store_id", $store_id)
    			//->where("itemtype_id", 2)//------filter merchandise
          ->where('m_item_stok.item_id',$value['item_id'])
    			->get()->row();
        if(empty($currentStock->item_id)){
          $dataInsert = array('item_id'=>$value['item_id'], 'store_id' => $store_id );
          $this->db->insert('m_item_stok',$dataInsert);
          //$currentStock = array('stok_mesin' => 0,'stok_gudang_mutasi' => 0, 'stok_gudang' => 0,'stok_store' => 0,'stok_total' => 0,'itemstok_id' => $this->db->insert_id());
          $currentStock = $this->item_stok_model
            ->with('item')
            ->where("store_id", $store_id)
            //->where("itemtype_id", 2)//------filter merchandise
            ->where('m_item_stok.item_id',$value['item_id'])
            ->get()->row();
        }
    		//echo $this->db->last_query();
    		$stokUpdateRow = array(
    				"itemstok_id"=> $currentStock->itemstok_id
    		);
    		
    		switch ($type_id){
          case (transactiontype::SELL) :
            //MERCHANDISE SELL AND PROMO
            $stokUpdateRow['stok_mesin'] = $currentStock->stok_mesin ;
            $stokUpdateRow['stok_store'] = $currentStock->stok_store - $itemRedemptionRow['item_qty'];
            $stokUpdateRow['stok_total'] = $currentStock->stok_total - $itemRedemptionRow['item_qty'];
            break;
          case (transactiontype::PRMO) :
            //MERCHANDISE SELL AND PROMO
            $stokUpdateRow['stok_mesin'] = $currentStock->stok_mesin ;
            $stokUpdateRow['stok_store'] = $currentStock->stok_store - $itemRedemptionRow['item_qty'];
            $stokUpdateRow['stok_total'] = $currentStock->stok_total - $itemRedemptionRow['item_qty'];
            break;
          case transactiontype::BYS :
            //MERCHANDISE BUYING MD
            $stokUpdateRow['stok_mesin'] = $currentStock->stok_mesin ;
            $stokUpdateRow['stok_gudang'] = $currentStock->stok_gudang + $itemRedemptionRow['item_qty'];
            $stokUpdateRow['stok_total'] = $currentStock->stok_total + $itemRedemptionRow['item_qty'];
            break;

          case transactiontype::BYSC :
            //MERCHANDISE BUYING SC
            $stokUpdateRow['stok_mesin'] = $currentStock->stok_mesin ;
            $stokUpdateRow['stok_gudang'] = $currentStock->stok_gudang + $itemRedemptionRow['item_qty'];
            $stokUpdateRow['stok_total'] = $currentStock->stok_total + $itemRedemptionRow['item_qty'];
            break;

          case transactiontype::MTSI :
            //MERCHANDISE BUYING SC
            $stokUpdateRow['stok_mesin']          = $currentStock->stok_mesin ;
            $stokUpdateRow['stok_gudang_mutasi']  = $currentStock->stok_gudang_mutasi + $itemRedemptionRow['item_qty'];
            $stokUpdateRow['stok_total']          = $currentStock->stok_total + $itemRedemptionRow['item_qty'];
            break;

    			case transactiontype::MFIL :
    				//MERCHANDISE REFILL TO MACHINE
    				$stokUpdateRow['stok_mesin'] = $currentStock->stok_mesin + $itemRedemptionRow['item_qty'];
    				$stokUpdateRow['stok_gudang'] = $currentStock->stok_gudang - $itemRedemptionRow['item_qty'];
            $stokUpdateRow['stok_total'] = $currentStock->stok_total - $itemRedemptionRow['item_qty'];
    				break;

          case transactiontype::MFILO :
            //MERCHANDISE REFILL OUT FROM MACHINE
            $stokUpdateRow['stok_mesin'] = $currentStock->stok_mesin;
            $stokUpdateRow['stok_gudang'] = $currentStock->stok_gudang;
            break;
    			case transactiontype::MRDM :
    				//MERCHANDISE REDEMPTION
    				$stokUpdateRow['stok_total'] = $currentStock->stok_total - $itemRedemptionRow['item_qty'];
    				$stokUpdateRow['stok_store'] = $currentStock->stok_store - $itemRedemptionRow['item_qty'];
    				break;
          case transactiontype::MRDM_REG :
            //MERCHANDISE REDEMPTION REGULER
            $stokUpdateRow['stok_total'] = $currentStock->stok_total - $itemRedemptionRow['item_qty'];
            $stokUpdateRow['stok_store'] = $currentStock->stok_store - $itemRedemptionRow['item_qty'];
            break;
          case transactiontype::MRPL :
            //MERCHANDISE Replenish
            $stokUpdateRow['stok_gudang'] = $currentStock->stok_gudang - $itemRedemptionRow['item_qty'];
            $stokUpdateRow['stok_store'] = $currentStock->stok_store + $itemRedemptionRow['item_qty'];
            break;
    		}
    		
    		$stok_update_data[] = $stokUpdateRow;
       // var_dump($stok_update_data);
    		//endof-build stok update data
    	
    		$seq++;
    	}
    	//var_dump($stok_update_data);
    	if ($allValidationPassed) {
			// INSERT TO TABLE REFILL
			$this->db->db_debug = FALSE;
            if ($type_id==transactiontype::MFIL)
                $this->db->insert_batch ( 'zn_transaction_refill', $items_data );
            elseif($type_id==transactiontype::BYS)
                $this->db->insert_batch ( 'zn_buying_store', $items_data );
            elseif($type_id==transactiontype::BYSC || $type_id==transactiontype::MTSI)
                $this->db->insert_batch ( 'zn_buying_store', $items_data );
            elseif($type_id==transactiontype::SELL || $type_id==transactiontype::PRMO)
                $this->db->insert_batch ( 'zn_selling', $items_data );
            elseif($type_id==transactiontype::MFILO)
                $this->db->insert_batch ( 'zn_transaction_refill_out', $items_data );
            elseif($type_id==transactiontype::MRDM || $type_id==transactiontype::MRDM_REG)
                $this->db->insert_batch ( 'zn_transaction_redemption', $items_data );
            else
                $this->db->insert_batch ( 'zn_transaction_replenish', $items_data );
			$this->db->update_batch ( 'm_item_stok', $stok_update_data, 'itemstok_id' );
      //echo $this->db->last_query();
		} else {
			// VALIDASI ITEM ERROR . ROLLBACK DB
			$return ['status'] = 'ERROR';
			$return ['message'] = validation_errors ();
			$this->db->trans_rollback ();
			$this->response ( $return, 200 );
		}
    }

    /**
     * 
     * updating stock on refilling ticket
     * 
     * created by : wahid
     * on April 23th, 2015
     * @param unknown $store_id
     * @param unknown $amount
     */
	private function updateTicketRefillStock($store_id, $amount){
		$currentStock = $this->item_stok_model
		->where('store_id', $store_id)
		->where('item_id', 4)
		->get()->row_array();
		
		if($currentStock){
			$updateData = array();
			$updateData['stok_mesin'] = $currentStock['stok_mesin'] + $amount;
			$updateData['stok_gudang'] = $currentStock['stok_gudang'] - $amount;
			$this->item_stok_model->update($updateData, $currentStock['itemstok_id']);
		}
	}

	/**
	 * update stock item after update ticket meter
	 * 
	 * @param unknown $store_id
	 * @param unknown $amount
	 */
	private function updateTicketOutStock($store_id, $amount){
		$currentStock = $this->item_stok_model
			->where ( "store_id", $store_id )
			->where ( "item_id", 4 ) // 4 means it is a ticket
			->get()
			->row();
		 
		if($currentStock){
			$updateData = array(
					"stok_mesin" => $currentStock->stok_mesin - $amount,
					"stok_store" => $currentStock->stok_store - $amount
			);
			$this->item_stok_model->update($updateData, $currentStock->itemstok_id);
		}
	}

	private function processTicketRedemption($itemJson, $storeId, $transactionId){
		$itemTypeId = itemtype::ITEM_TICKET;
		$currentItem = $this->item_model
			->where('itemtype_id', $itemTypeId)
			->get()->row_array();
		
		$merchandisesToUpdate = json_decode($itemJson, true);
		$updateData = array();
		$totalQty = 0;
		foreach ($merchandisesToUpdate as $key => $value){
			$currentStock = $this->item_stok_model
				->with('item')
				->where('itemtype_id', $itemTypeId)
				->where('store_id', $storeId)
				->get()->row_array();
			
			$updateDataRow = array(
					'itemstok_id' => $currentStock['itemstok_id'],
					'stok_total' => $currentStock['stok_total'] - $value['item_qty'],
					'stok_store'  => $currentStock['stok_store'] - $value['item_qty']
			);
			$updateData[] = $updateDataRow;
			$totalQty += $value['item_qty'];
		}
		
		$this->db->update_batch('m_item_stok', $updateData, 'itemstok_id');
		
		//build transaction redemption data
		$itemRedemptionRow = array(
				'transaction_id' => $transactionId,
				'item_id' => $currentItem['m_item.item_id'],
				'item_key' => $currentItem['item_key'],
				'item_qty' => $totalQty
		);
		$this->db->insert('zn_transaction_redemption', $itemRedemptionRow);
		//endof-build transaction redemption data
	}

	public function test_get(){
		return $this->response(array("message"=>"oke"), 200);
	}
}


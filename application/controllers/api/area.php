<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Area extends REST_Controller{

	function __construct(){
        parent::__construct();
		$this->load->model('hadiah_masuk_model');
    }
    
    function get_data_post(){
        $response = array();
        
        $post = array(
			'tenant_id'           => filter_var($this->post('name'), FILTER_SANITIZE_NUMBER_INT),
			'company_id'          => filter_var($this->post('email'), FILTER_SANITIZE_STRING),
			'user_id'  => filter_var($this->post('default_email'), FILTER_SANITIZE_STRING),
            'phone'          => filter_var($this->post('phone'), FILTER_SANITIZE_NUMBER_INT)
		);
        
        $data = $this->hadiah_masuk_model->options($post);

        //$return = false;

        
/*        if($return){
            $response = array(
				'status' => 'OK'
			);
        }else{
            $response = array(
				'status' => 'ERROR'
			);
        }
*/        
        $response = array('status' => 'OK',
        				'data' => $data 
        	);

        $this->response($response);
    }


	public function send_post(){
		var_dump($this->request->body);
	}

	public function send_put(){
		var_dump($this->put('foo'));
	}
}
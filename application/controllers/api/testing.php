<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Testing extends REST_Controller
{
	function __construct()
    {
        // Construct our parent class
        parent::__construct();
        
        // Configure limits on our controller methods. Ensure
        // you have created the 'limits' table and enabled 'limits'
        // within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; //500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; //100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; //50 requests per hour per user/key
    

    }

    public function numcheck($in){
        $num_start = $_POST['num_start'];
        $num_end    = $_POST['num_end'];
        $this->form_validation->set_message('numcheck', ' Num End Must Larger than Num Start');
        return $num_end>$num_start;
    }

    public function validate($param){
        $_POST = $param;
        print_r($_POST);

        $this->load->library('form_validation');
        $config = array(
           array(
                 'field'   => 'user_id', 
                 'label'   => 'User ID', 
                 'rules'   => "required"
              ),
           array(
                 'field'   => 'store_id', 
                 'label'   => 'Store ID', 
                 'rules'   => 'required'
              ),
           array(
                 'field'   => 'machine_number', 
                 'label'   => 'Machine Number', 
                 'rules'   => 'required'
              ),
           array(
                 'field'   => 'type_id', 
                 'label'   => 'Type ID', 
                 'rules'   => 'required'
              ),
           array(
                 'field'   => 'device_id', 
                 'label'   => 'Device ID', 
                 'rules'   => 'required'
              ),
           array(
                 'field'   => 'machine_id', 
                 'label'   => 'Machine ID', 
                 'rules'   => 'required'
              ),
           array(
                 'field'   => 'num_coin', 
                 'label'   => 'Num Coin', 
                 'rules'   => 'is_natural'
              ),
           array(
                 'field'   => 'num_meter', 
                 'label'   => 'Num Meter', 
                 'rules'   => 'is_natural'
              ),
           array(
                 'field'   => 'num_ticket', 
                 'label'   => 'Num Ticket', 
                 'rules'   => 'is_natural'
              ),
           array(
                 'field'   => 'num_start', 
                 'label'   => 'Num Start', 
                 'rules'   => 'is_natural'
              ),
            array(
                 'field'   => 'num_end', 
                 'label'   => 'Num End', 
                 'rules'   => 'is_natural|callback_numcheck'
              ),
           
        );
        $this->form_validation->set_error_delimiters('', '');
        $this->form_validation->set_rules($config);
        return $this->form_validation->run();
    }

    public function save_post(){
        /*machine_id
        
        $params = array();
        $params['user_id'] = 1;
        $params['store_id'] = 1;
        $params['type_id'] = 1;
        $params['device_id'] = 1;
        $params['transaction_id'] = 1;
        $params['machine_id'] = 1;
        $params['machine_number'] = '0987609876';
        $params['num_coin'] = 0;
        $params['num_meter'] = 100;
        $params['num_ticket'] = 0;
        $params['num_start'] = 0;
        $params['num_end'] = 0;
        $params['rectime'] = '4/1/2015 12:00:00 AM';
        */

        // machine number untuk pengecekan di database
        $post = $this->post();
        $type_id = $post['type_id'];
        $this->load->model(array('transaction_model'));
        $validation = $this->validate($post);
        echo 'error : ';
        echo validation_errors();   
        die;
        if($validation === true){
            $this->db->trans_begin();
            // insert to transaction
            $data = array();
            $data['transactiontype_id']     = $post['type_id']; // COIN
            $data['rec_user']               = $post['user_id'];        
            $data['rec_created']            = $post['rectime'];

            $data['num_coin']               = $post['num_coin'];
            $data['num_ticket']             = $post['num_ticket'];
            $data['num_meter']              = $post['num_meter'];
            $data['num_start']              = $post['num_start'];
            $data['num_end']                = $post['num_end'];
            
            $this->db->insert('zn_transaction', $data);
            $transaction_id = $this->db->insert_id();

            // insert to transaction machine
            $data = array();
            $data['transaction_id'] = $transaction_id;
            $data['machine_id']     = $post['machine_id'];
            $this->db->insert('zn_transaction_machine', $data);

            // insert to transaction device table
            $data = array();
            $data['transaction_id']         = $transaction_id;
            $data['device_id']              = $post['device_id'];
            $data['device_transaction_id']  = $post['transaction_id'];
            $this->db->insert('zn_transaction_device', $data);

            // insert to transaction store table
            $data = array();
            $data['transaction_id']         = $transaction_id;
            $data['store_id']               = $post['store_id'];
            $this->db->insert('zn_transaction_store', $data);

            switch ($type_id) {
                // METER : insert to TRANSACTION_METER
                case ($type_id == 1 || $type_id == 3 || $type_id == 4 || $type_id == 5 || $type_id == 7 ):

                    if($post['num_meter']>0){
                        $meter = array();
                        $meter_row = $this->transaction_model
                            ->with(array('transaction_machine','transaction_meter'))
                            ->where('machine_id', $post['machine_id'])
                            ->where('transactiontype_id',$post['type_id'])
                            ->order_by('zn_transaction_meter.transaction_id','desc')
                            ->get()->row();
                            
                        $meter['transaction_id'] = $transaction_id;
                        $meter['num_initial']    = ($meter_row->num_current ? $meter_row->num_current : 0);            
                        $meter['num_current']    = $post['num_meter'];
                        $meter['num_diff']       = abs($meter['num_current'] - $meter['num_initial']);
                        $this->db->insert('zn_transaction_meter', $meter);
                    }
                    break;

                // REFILL : insert to TRANSACTION_REFILL
                case 5 :

                    break;
                
                default:
                    # code...
                    break;
            }
            

            if($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                $return['status'] = 'ERROR';
                $return['message']= 'Transaction Failed. Database Error Occurred';
            }else{
                $this->db->trans_commit();
                $return['status'] = 'OK';
                $return['message']= 'Transaction Success';
            }

            $this->response($return, 200);
        } else{
            // validation error 
            $this->response(array('status'=>'ERROR', 'message'=>validation_errors()),200);
        }
    }

    public function test_post(){
        $data = array(
                    array('tes'=>'1', 'nama'=>'jono'),
                    array('tes'=>'1', 'nama'=>'jono'),
                    array('tes'=>'1', 'nama'=>'jono'),
                    array('tes'=>'1', 'nama'=>'jono'),
                );
        echo json_encode($data);
    }

    public function decode_post(){
        $test = '[{"tes":"1","nama":"jono"},{"tes":"1","nama":"jono"},{"tes":"1","nama":"jono"},{"tes":"1","nama":"jono"}]';
        //$test = '[1,2,3,4,5,6,7,8]';
        $var = json_decode($test, FALSE);
        print_r($var);
    }

}
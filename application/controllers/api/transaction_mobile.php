<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Transaction_mobile extends REST_Controller
{	
	function __construct()
    {
        // Construct our parent class
        parent::__construct();
        // Configure limits on our controller methods. Ensure
        // you have created the 'limits' table and enabled 'limits'
        // within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; //500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; //100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; //50 requests per hour per user/key
		$this->load->helpers('itemtype');
		$this->load->helpers('transactiontype');
		$this->load->model("item_stok_model");
		$this->load->model("item_model");
    }

    public function numcheck($in){
        $num_start = $_POST['num_start'];
        $num_end    = $_POST['num_end'];
        $this->form_validation->set_message('numcheck', ' Num End Must Larger than Num Start');
        return $num_end>=$num_start;
    }

    public function validate($param){
        $_POST = $param;
        $this->load->library('form_validation');

        $config = array(
           array(
                 'field'   => 'user_id', 
                 'label'   => 'User ID', 
                 'rules'   => "required"
              ),
           
           array(
                 'field'   => 'store_id', 
                 'label'   => 'Store ID', 
                 'rules'   => 'required'
              ),
           array(
                 'field'   => 'machine_number', 
                 'label'   => 'Machine Number', 
                 'rules'   => 'required'
              ),
           array(
                 'field'   => 'type_id', 
                 'label'   => 'Type ID', 
                 'rules'   => 'required'
              ),
           array(
                 'field'   => 'device_id', 
                 'label'   => 'Device ID', 
                 'rules'   => 'required'
              ),
           array(
                 'field'   => 'machine_id', 
                 'label'   => 'Machine ID', 
                 'rules'   => 'required'
              ),
           array(
                 'field'   => 'num_coin', 
                 'label'   => 'Num Coin', 
                 'rules'   => 'is_natural'
              ),
           array(
                 'field'   => 'num_meter', 
                 'label'   => 'Num Meter', 
                 'rules'   => 'is_natural'
              ),
           array(
                 'field'   => 'num_ticket', 
                 'label'   => 'Num Ticket', 
                 'rules'   => 'is_natural'
              ),
           array(
                 'field'   => 'num_start', 
                 'label'   => 'Num Start', 
                 'rules'   => 'is_natural'
              ),
           array(
                 'field'   => 'num_end', 
                 'label'   => 'Num End', 
                 'rules'   => 'is_natural|callback_numcheck'
              ),
           
        );
        $this->form_validation->set_error_delimiters('', '');
        $this->form_validation->set_rules($config);
        return $this->form_validation->run();
    }

    public function validateItem($param){
        $_POST = $param;
        $config = array(
           array(
                 'field'   => 'item_qty', 
                 'label'   => 'Item Qty', 
                 'rules'   => "required|is_natural"
              ),
           array(
                 'field'   => 'item_id', 
                 'label'   => 'Item Id', 
                 'rules'   => "required"
              )
           );
        $this->form_validation->set_error_delimiters('', '');
        $this->form_validation->set_rules($config);        
        return $this->form_validation->run();
    }

    public function save_post(){
    	$this->load->model('item_stok_model');
        // machine number untuk pengecekan di database
        $post = $this->post();
        $type_id = $post['type_id'];
        $this->load->model(array('transaction_model'));
        $validation = $this->validate($post);
        
        $this->db->trans_begin();

        if($validation === true){            
            // insert to transaction
            $data = array();
            $data['transactiontype_id']     = $post['type_id']; // COIN
            $data['rec_user']               = $post['user_id'];        
            $data['rec_created']            = $post['rectime'];

            $data['num_coin']               = $post['num_coin'];
            $data['num_ticket']             = $post['num_ticket'];
            $data['num_meter']              = $post['num_meter'];
            $data['num_start']              = $post['num_start'];
            $data['num_end']                = $post['num_end'];
            
            $this->db->insert('zn_transaction', $data);
            $transaction_id = $this->db->insert_id();

            // insert to transaction machine
            $data = array();
            $data['transaction_id'] = $transaction_id;
            $data['machine_id']     = $post['machine_id'];
            $this->db->insert('zn_transaction_machine', $data);

            // insert to transaction device table
            $data = array();
            $data['transaction_id']         = $transaction_id;
            $data['device_id']              = $post['device_id'];
            $data['device_transaction_id']  = $post['transaction_id'];
            $this->db->insert('zn_transaction_device', $data);

            // insert to transaction store table
            $data = array();
            $data['transaction_id']         = $transaction_id;
            $data['store_id']               = $post['store_id'];
            $this->db->insert('zn_transaction_store', $data);
            switch ($type_id) {
                // METER : insert to TRANSACTION_METER
                case ( $type_id == transactiontype::COIN || $type_id == transactiontype::TOUT || $type_id == transactiontype::MOUT || $type_id == transactiontype::TCUT ):

                    if($post['num_meter']>0){
                        $meter = array();
                        $meter_row = $this->transaction_model
                            ->with(array('transaction_machine','transaction_meter'))
                            ->where('machine_id', $post['machine_id'])
                            ->where('transactiontype_id',$post['type_id'])
                            ->order_by('zn_transaction_meter.transaction_id','desc')
                            ->get()->row();
                            
                        $meter['transaction_id'] = $transaction_id;
                        $meter['num_initial']    = ($meter_row->num_current ? $meter_row->num_current : 0);            
                        $meter['num_current']    = $post['num_meter'];
                        $meter['num_diff']       = abs($meter['num_current'] - $meter['num_initial']);
                        $this->db->insert('zn_transaction_meter', $meter);
                        
                        if($type_id == 3){
                        	//TOUT
                        	$this->updateTicketOutStock($post['store_id'], $meter['num_diff']);
                        }
                    }
                    break;
                    
                case transactiontype::TFIL :
                	//TICKET REFILL, we use num_start and num_end
                	$refillAmount = $post['num_end'] - $post['num_start'];
                	$this->updateTicketRefillStock($post['store_id'], $refillAmount);
                	break;

                case ( $type_id == transactiontype::MFIL || $type_id == transactiontype::MRDM):
                	$this->processMerchandiseRedemptionAndRefill($post['items'], $transaction_id, $post['store_id'], $type_id);
                	break;
                	
                case transactiontype::SREQ :
                    // SERVICE REQUEST
                    $service['transaction_id'] = $transaction_id;
                    $service['issue_description'] = $post['issue_description'];
                    $this->db->insert('zn_transaction_service', $service);
                    break;

                case transactiontype::SREP :
                    $repair['transaction_id'] = $transaction_id;
                    $repair['repair_status']  = $post['repair_status'];
                    $this->db->insert('zn_transaction_repair', $repair);
                    break;
                    
                case transactiontype::TRDM:
                	$this->processTicketRedemption($post['items'], $numTiket, $storeId, $transactionId);
                	break;
                default:
                    # code...
                    break;
            }
            

            if($this->db->trans_status() === FALSE)
            {
                $return['status'] = 'ERROR';
                //$return['message']= 'Transaction Failed. Database Error Occurred';
                $return['message'] = $this->db->_error_message();
                $this->db->trans_rollback();
            }else{
                $return['status'] = 'OK';
                $return['message']= 'Transaction Success';
                $this->db->trans_commit();
            }
            
            $this->response($return, 200);
        } else{
            // validation error 
            $this->response(array('status'=>'ERROR', 'message'=>validation_errors()),200);
        }
    }
    
    private function processMerchandiseRedemptionAndRefill($itemsJson, $transaction_id, $store_id, $type_id){
    	$items = json_decode($itemsJson, TRUE);
    	$items_data = array();
    	$stok_update_data = array();
    	$seq = 1;
    	$allValidationPassed = true;
    	foreach ($items as $key => $value) {
    		/* validasi item. jika ada error maka langsung keluar perulangan. */
    		if(!$this->validateItem($value)){
    			$allValidationPassed = false;
    			break;
    		}
    	
    		//build redemption insert data
    		$itemRedemptionRow = array();
    		$itemRedemptionRow['transaction_id'] = $transaction_id;
    		$itemRedemptionRow['item_id'] = $value['item_id'];
    		$itemRedemptionRow['item_key']= $value['item_key'];
    		$itemRedemptionRow['item_qty']= $value['item_qty'];
    		$itemRedemptionRow['seq']     = $seq;
    		$items_data[] = $itemRedemptionRow;
    		//endof-build redemption insert data
    		
    		//build stok update data
    		$currentStock = $this->item_stok_model
    			->with('item')
    			->where("store_id", $store_id)
    			->where("itemtype_id", 2)
    			->get()->row();
    		
    		$stokUpdateRow = array(
    				"itemstok_id"=> $currentStock->itemstok_id
    		);
    		
    		switch ($type_id){
    			case transactiontype::MFIL :
    				//MERCHANDISE REFILL TO MACHINE
    				$stokUpdateRow['stok_mesin'] = $currentStock->stok_mesin + $itemRedemptionRow['item_qty'];
    				$stokUpdateRow['stok_gudang'] = $currentStock->stok_gudang - $itemRedemptionRow['item_qty'];
    				break;
    			case transactiontype::MRDM :
    				//MERCHANDISE REDEMPTION
    				$stokUpdateRow['stok_mesin'] = $currentStock->stok_mesin - $itemRedemptionRow['item_qty'];
    				$stokUpdateRow['stok_store'] = $currentStock->stok_store - $itemRedemptionRow['item_qty'];
    				break;
    		}
    		
    		$stok_update_data[] = $stokUpdateRow;
    		//endof-build stok update data
    	
    		$seq++;
    	}
    	
    	if ($allValidationPassed) {
			// INSERT TO TABLE REFILL
			$this->db->db_debug = FALSE;
			$this->db->insert_batch ( 'zn_transaction_redemption', $items_data );
			$this->db->update_batch ( 'm_item_stok', $stok_update_data, 'itemstok_id' );
		} else {
			// VALIDASI ITEM ERROR . ROLLBACK DB
			$return ['status'] = 'ERROR';
			$return ['message'] = validation_errors ();
			$this->db->trans_rollback ();
			$this->response ( $return, 200 );
		}
    }

    /**
     * 
     * updating stock on refilling ticket
     * 
     * created by : wahid
     * on April 23th, 2015
     * @param unknown $store_id
     * @param unknown $amount
     */
	private function updateTicketRefillStock($store_id, $amount){
		$currentStock = $this->item_stok_model
		->where('store_id', $store_id)
		->where('item_id', 4)
		->get()->row_array();
		
		if($currentStock){
			$updateData = array();
			$updateData['stok_mesin'] = $currentStock['stok_mesin'] + $amount;
			$updateData['stok_gudang'] = $currentStock['stok_gudang'] - $amount;
			$this->item_stok_model->update($updateData, $currentStock['itemstok_id']);
		}
	}

	/**
	 * update stock item after update ticket meter
	 * 
	 * @param unknown $store_id
	 * @param unknown $amount
	 */
	private function updateTicketOutStock($store_id, $amount){
		$currentStock = $this->item_stok_model
			->where ( "store_id", $store_id )
			->where ( "item_id", 4 ) // 4 means it is a ticket
			->get()
			->row();
		 
		if($currentStock){
			$updateData = array(
					"stok_mesin" => $currentStock->stok_mesin - $amount,
					"stok_store" => $currentStock->stok_store - $amount
			);
			$this->item_stok_model->update($updateData, $currentStock->itemstok_id);
		}
	}

	private function processTicketRedemption($itemJson, $storeId, $transactionId){
		$itemTypeId = itemtype::ITEM_TICKET;
		$currentItem = $this->item_model
			->where('itemtype_id', $itemTypeId)
			->get()->row_array();
		
		$merchandisesToUpdate = json_decode($itemJson, true);
		$updateData = array();
		$totalTicket = 0;
		foreach ($merchandisesToUpdate as $key => $value){
			$currentStock = $this->item_stok_model
				->with('item')
				->where('itemtype_id', $itemTypeId)
				->where('store_id', $storeId)
				->get()->row_array();
			
			$updateDataRow = array(
					'itemstok_id' => $currentStock['itemstok_id'],
					'stok_gudang' => $currentStock['stok_gudang'] - $value['item_qty'],
					'stok_store'  => $currentStock['stok_store'] - $value['item_qty']
			);
			$updateData[] = $updateDataRow;
			$totalTicket += $value['item_qty'];
		}
		
		$this->db->update_batch('m_item_stok', $updateData, 'itemstok_id');
		
		//build transaction redemption data
		$itemRedemptionRow = array(
				'transaction_id' => $transactionId,
				'item_id' => $currentItem['m_item.item_id'],
				'item_key' => $currentItem['item_key'],
				'item_qty' => $totalTicket
		);
		$this->db->insert('zn_transaction_redemption', $itemRedemptionRow);
		//endof-build transaction redemption data
	}

	public function test_get(){
		return $this->response(array("message"=>"oke"), 200);
	}
}

<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Machine extends REST_Controller
{
	function __construct()
    {
        // Construct our parent class
        parent::__construct();
        
        // Configure limits on our controller methods. Ensure
        // you have created the 'limits' table and enabled 'limits'
        // within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; //500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; //100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; //50 requests per hour per user/key
    

    }

    public function validate($param){
        $_POST = $param;
        $this->load->library('form_validation');

        $config = array(
           array(
                 'field'   => 'user_id', 
                 'label'   => 'User ID', 
                 'rules'   => 'required'
              ),
           array(
                 'field'   => 'store_id', 
                 'label'   => 'Store ID', 
                 'rules'   => 'required'
              ),
           array(
                 'field'   => 'machine_number', 
                 'label'   => 'Machine Number', 
                 'rules'   => 'required'
              ),
        );

        $this->form_validation->set_error_delimiters('', '');
        $this->form_validation->set_rules($config);
        return $this->form_validation->run();
    }
    
    public function detail_post($type= ''){
        $param['user_id']       = $this->session->userdata('user_id');
        $param['store_id']      = $this->session->userdata('store_id');
        $param['machine_number']= 'xx';

        switch ($type) {
            case 'last-transaction':
                $param['machine_id']    = $this->post('item_id');
                $param['type_id']       = $this->post('type_id');

                $this->getDetailMachine_post($param);
                break;
            
            default:
                # code...
                break;
        }
    }



    public function getDetailMachine_post($post = array()){

        $this->load->model(array('mesin_model','transaction_model','purchase_request_model','range_tiket_model','item_model','promosi_model'));

        if (!$post)
            $post = $this->post();

        $user_id        = $post['user_id'];
        $store_id       = $post['store_id'];
        $machine_number = $post['machine_number'];
        $machine_id     = $post['machine_id'];
        $type_id        = $post['type_id'];

        if($type_id == transactiontype::MRDM){
            $cekPromosi = $this->promosi_model
                ->select('qty_tiket')
                ->where('item_id',$machine_id)
                ->where('store_id',$this->session->userdata('store_id'))
                ->where('date_begin <=', date('Y-m-d'))
                ->where('date_end >=',date('Y-m-d'))
                ->get()->row();
            if(!empty($cekPromosi->qty_tiket)){
                $detail['last_reading'] = $cekPromosi->qty_tiket.' (Promosi)';
                $detail['promosi'] = ' (Promosi)';
                $return['detail_machine'] = $detail;
                //$return['promosi'] = ' (Promosi)';
                    
            }else{
                $info = $this->item_model
                    ->select('harga,m_type')
                    ->where('item_id',$machine_id)
                    ->get()->row();
                $price = !empty($info->harga)?$info->harga : 1;
                $m_type = !empty($info->m_type)?$info->m_type : 2;
                $st_member = 1;
                $tiket = $this->range_tiket_model->get_tiket($price,$m_type,$st_member);
                $detail['last_reading'] = $tiket;// $info->price.'-'.$info->m_type;//($trans->num_meter) ? $trans->num_meter : 0;

                $return['detail_machine'] = $detail;
                # code...
            }
            
        }elseif($type_id == transactiontype::MRDM_REG){
            $cekPromosi = $this->promosi_model
                ->select('qty_tiket')
                ->where('item_id',$machine_id)
                ->where('store_id',$this->session->userdata('store_id'))
                ->where('date_begin <=', date('Y-m-d'))
                ->where('date_end >=',date('Y-m-d'))
                ->get()->row();
            if(!empty($cekPromosi->qty_tiket)){
                $detail['last_reading'] = $cekPromosi->qty_tiket.' (Promosi)';
                $detail['promosi'] = ' (Promosi)';
                $return['detail_machine'] = $detail;
                //$return['promosi'] = ' (Promosi)';
                    
            }else{
                $info = $this->item_model
                    ->select('harga,m_type')
                    ->where('item_id',$machine_id)
                    ->get()->row();
                $price = !empty($info->harga)?$info->harga : 1;
                $m_type = !empty($info->m_type)?$info->m_type : 2;
                $st_member = 0;
                $tiket = $this->range_tiket_model->get_tiket($price,$m_type,$st_member);
                $detail['last_reading'] = $tiket;// $info->price.'-'.$info->m_type;//($trans->num_meter) ? $trans->num_meter : 0;

                $return['detail_machine'] = $detail;
                # code...
            }
            
        }else{
            if($this->validate($post)){
                // get machine_id (id_mesin) dulu untuk mencari transaksi terakhir
                
                $mesin = $this->mesin_model
                            ->with('jenis_mesin')
                            //->where('serial_number', $machine_number)
                            ->where('id_mesin', $machine_id)
                            ->get()->row();

                // jika mesin ditemukan
                
                if($mesin->serial_number){
                    switch ($type_id) {
                        case ($type_id == 1 || $type_id == 3 || $type_id == 4 || $type_id == 7 ):
                            // 1: COIN OUT METER
                            // 3: TICKET OUT METER
                            // 4: MERCHANDISE OUT METER
                            // 7: TICKET CUTTER METER
                            //
                            $trans = $this->transaction_model
                                ->with(array('transaction_machine','transaction_meter'))
                                ->where('machine_id', $mesin->id_mesin)
                                ->where('zn_transaction.transactiontype_id',$type_id)
                                ->order_by('zn_transaction_meter.transaction_id','desc')
                                ->get()->row();

                            $detail['last_reading'] = ($trans->num_current) ? $trans->num_current : 0;
                            # code...
                            break;

                        case 2:
                            // 2: COIN COUNT
                           /* $trans = $this->transaction_model
                                        ->with(array('transaction_machine'))
                                        ->where('machine_id', $mesin->id_mesin)
                                        ->where('zn_transaction.transactiontype_id',$type_id)
                                        ->order_by('zn_transaction.transaction_id','desc')
                                        ->get()->row();
                            $detail['last_reading'] = ($trans->num_coin) ? $trans->num_coin : 0;*/
                            $trans = $this->transaction_model
                                ->with(array('transaction_machine'))
                                ->where('machine_id',$mesin->id_mesin)
                                ->where('zn_transaction.transactiontype_id',$type_id)
                                ->order_by('zn_transaction.transaction_id','desc')
                                ->get()->row();
                            $detail['last_reading'] = ($trans->num_end) ? $trans->num_end : 0;
                            # code...
                            break;

                        case 5:
                            // 5: TICKET REFILL
                            $trans = $this->transaction_model
                                ->with(array('transaction_machine'))
                                ->where('machine_id',$mesin->id_mesin)
                                ->where('zn_transaction.transactiontype_id',$type_id)
                                ->order_by('zn_transaction.transaction_id','desc')
                                ->get()->row();

                            $detail['last_reading'] = ($trans->num_meter) ? $trans->num_meter : 0;
                            # code...
                            break;

                        case 6:
                            // 6: MERCHANDISE REFILL
                            // lsat reading di non-aktifkan, tidak di butuhkan
                            /*$trans = $this->transaction_model
                                ->with(array('transaction_machine'))
                                ->where('machine_id',$mesin->id_mesin)
                                ->where('zn_transaction.transactiontype_id',$type_id)
                                ->order_by('zn_transaction.transaction_id','desc')
                                ->get()->row();

                            $detail['last_reading'] = ($trans->num_meter) ? $trans->num_meter : 0;*/

                            # code...
                            break;  

                        case 26:
                            // 26: Ticket Payout
                            // lsat reading di non-aktifkan, tidak di butuhkan
                            $this->transaction_model->set_limitStore(FALSE);
                            $trans = $this->transaction_model
                                ->with(array('transaction_machine'))
                                ->where('machine_id',$mesin->id_mesin)
                                ->where('zn_transaction.transactiontype_id',$type_id)
                                ->order_by('zn_transaction.transaction_id','desc')
                                ->get()->row();
                            $detail['last_reading'] = ($trans->num_end) ? $trans->num_end : 0;

                            # code...
                            break;                   

                        default:
                            # code...
                            break;
                    }

                    $detail['machine_id']   = $mesin->id_mesin;
                    $detail['machine_name'] = $mesin->nama_jenis_mesin;
                    $detail['register']     = $mesin->kd_mesin;
                    $detail['last_date']    = ($trans->rec_created ? date::longDate($trans->rec_created) : '-');
                    

                    $return['status'] = true;
                    $return['message']= 'Machine Found';
                    $return['detail_machine'] = $detail;
                
                }else{
                    $return['status'] = false;
                    $return['message']= 'Machine Not Found';

                }
            }else{
                $return['status'] = 'ERROR';
                $return['message']= validation_errors();
            }
        }
        
        if($type_id==6 || $type_id==transactiontype::BYS || $type_id==transactiontype::SELL || $type_id==transactiontype::PRMO || $type_id==transactiontype::BYSC || $type_id==transactiontype::MTSI || $type_id==transactiontype::MFILO){
            echo $this->twig->render('machine_transaction/info/null_info.tpl', (array)$return['detail_machine']);
            
        }elseif($type_id==transactiontype::MRDM){
            echo $this->twig->render('machine_transaction/info/info_redeem.tpl', (array)$return['detail_machine']);
        }elseif($type_id==transactiontype::TPAYO||$type_id==transactiontype::COUT){
            echo $detail['last_reading'];
        }else{
            echo $this->twig->render('machine_transaction/info/machine.tpl', (array)$return['detail_machine']);
        }

    }

    public function testing_get(){
        echo 'sdads';

    }

}
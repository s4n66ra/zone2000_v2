<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Machine_Backup extends REST_Controller
{
	function __construct()
    {
        // Construct our parent class
        parent::__construct();
        
        // Configure limits on our controller methods. Ensure
        // you have created the 'limits' table and enabled 'limits'
        // within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; //500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; //100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; //50 requests per hour per user/key
    

    }

    public function validate($param){
        $_POST = $param;
        $this->load->library('form_validation');

        $config = array(
           // array(
           //       'field'   => 'user_id', 
           //       'label'   => 'User ID', 
           //       'rules'   => 'required'
           //    ),
           array(
                 'field'   => 'store_id', 
                 'label'   => 'Store ID', 
                 'rules'   => 'required'
              ),
           array(
                 'field'   => 'machine_number', 
                 'label'   => 'Machine Number', 
                 'rules'   => 'required'
              ),
        );
        $this->form_validation->set_error_delimiters('', '');
        $this->form_validation->set_rules($config);
        return $this->form_validation->run();
    }
 
    public function getDetailMachine_post(){
        $this->load->model(array('mesin_model','transaction_model'));

        //$this->db->insert('testing',array('value'=>$param));

        $user_id        = $this->post('user_id');
        $store_id       = $this->post('store_id');
        $machine_number = $this->post('machine_number');
        $type_id        = $this->post('type_id');

        if($this->validate($this->post())){
            // get machine_id (id_mesin) dulu untuk mencari transaksi terakhir
            
            $mesin = $this->mesin_model
                        ->with('jenis_mesin')
                        ->where('serial_number', $machine_number)
                        ->get()->row();

            // jika mesin ditemukan

            if($mesin->serial_number){
                switch ($type_id) {
                    case ($type_id == 1 || $type_id == 3 || $type_id == 4 || $type_id == 7 ):
                        // 1: COIN OUT METER
                        // 3: TICKET OUT METER
                        // 4: MERCHANDISE OUT METER
                        // 7: TICKET CUTTER METER
                        //
                        $trans = $this->transaction_model
                            ->with(array('transaction_machine','transaction_meter'))
                            ->where('machine_id', $mesin->id_mesin)
                            ->where('zn_transaction.transactiontype_id',$type_id)
                            ->order_by('zn_transaction_meter.transaction_id','desc')
                            ->get()->row();

                        $detail['last_reading'] = ($trans->num_current) ? $trans->num_current : 0;
                        # code...
                        break;

                    case 2:
                        // 2: COIN COUNT
                        $trans = $this->transaction_model
                                    ->with(array('transaction_machine'))
                                    ->where('machine_id', $mesin->id_mesin)
                                    ->where('zn_transaction.transactiontype_id',$type_id)
                                    ->order_by('zn_transaction.transaction_id','desc')
                                    ->get()->row();
                        $detail['last_reading'] = ($trans->num_coin) ? $trans->num_coin : 0;
                        # code...
                        break;

                    case 5:
                        // 5: TICKET REFILL
                        $trans = $this->transaction_model
                            ->with(array('transaction_machine'))
                            ->where('machine_id',$mesin->id_mesin)
                            ->where('zn_transaction.transactiontype_id',$type_id)
                            ->order_by('zn_transaction.transaction_id','desc')
                            ->get()->row();

                        $detail['last_reading'] = ($trans->num_meter) ? $trans->num_meter : 0;
                        # code...
                        break;

                    case 6:
                        // 6: MERCHANDISE REFILL
                        $trans = $this->transaction_model
                            ->with(array('transaction_machine'))
                            ->where('machine_id',$mesin->id_mesin)
                            ->where('zn_transaction.transactiontype_id',$type_id)
                            ->order_by('zn_transaction.transaction_id','desc')
                            ->get()->row();

                        $detail['last_reading'] = ($trans->num_meter) ? $trans->num_meter : 0;
                        # code...
                        break;

                    default:
                        # code...
                        break;
                }

                $detail['machine_id']   = $mesin->id_mesin;
                $detail['machine_name'] = $mesin->nama_jenis_mesin;
                $detail['register']     = $mesin->kd_mesin;
                $detail['last_date']    = ($trans->rec_created ? $trans->rec_created : '-');
                

                $return['status'] = 'OK';
                $return['message']= 'Machine Found';
                $return['detail_machine'] = $detail;
            
            }else{
                $return['status'] = 'ERROR';
                $return['message']= 'Machine Not Found';
            }
        }else{

            $return['status'] = 'ERROR';
            $return['message']= validation_errors();
        }

        $this->response($return, 200);

    }

    public function testing_post(){
        

    }

}
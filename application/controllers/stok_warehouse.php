<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class stok extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array(
        	'item_model','item_stok_model'
        ));
    }

    public function index() {

        $data['title'] 		= 'DAFTAR STOK';
        $data['sidebar'] 	= $this->access_right->menu();
        $data['content'] 	= 'base/page_content.tpl';     
        //$data['button_group'] 	= $this->getAvailableButtons();
        $data['table']['main'] 	= $this->getForm('table-main', '', array('wrapper'));
        $data['javascript'] = array('receiving.js');
        
        $this->twig->display('index.tpl', $data);
    }

    public function getForm($type = '', $id = NULL, $param = array()){
        $this->load->library(array('custom_form','custom_table'));
        $this->load->model(array('sequence_model','po_store_model'));
        $form = $this->custom_form;

        switch ($type) {
            case 'page-detail':
                $param['title']     = 'RECEIVING DETAIL';
                $param['portlet']   = true;
                $param['button']['back'] = true;
                $param['form']['info']  = $this->getForm('form-info', $id);
                $param['table']['main'] = $this->getForm('page-koli', $id);
                return $this->twig->render('base/page_content.tpl', $param);
                break;
            
            case 'table-main':
                $this->load->library('Datatable');
                $table = new Datatable();

                if (in_array('wrapper', $param)) {
                    $this->load->model('itemtype_model');
                    $data = $this->itemtype_model->options_filter();
                    $table->innerFilter = array(
                        array('itemtype_id', 'Item Type', 'list', $data),
                        array('item_name', 'Item Name', 'text'),
                    );
                    $table->dataFilter = $table->innerFilter;

                    $table->id          = 'table-stok';
                    $table->isScrollable= false;
                    $table->numbering   = true;
                    $table->header      = array('ITEM', 'TYPE', 'STORE', 'STOK');
                    $table->source      = site_url($this->class_name.'/getform/table-main/');
                    return $table->generateWrapper();
                } else {
                    $table
                        ->setModel($this->item_stok_model)
                        ->setNumbering()
                        ->with(array('store','item'))
                        ->select('item_name, itemtype_id, nama_cabang, stok_total')
                        ->order_by('nama_cabang')
                        ->order_by('itemtype_id')
                        ->order_by('item_name')
                        ->edit_column('itemtype_id','$1','transaksi::getItemType(itemtype_id)');
                    echo $table->generate();
                }
                break;
            
            default:
                # code...
                break;
        }
    }


}
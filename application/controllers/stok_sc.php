<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class stok_sc extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array(
        	'item_model','item_stok_model',
        ));
    }

    public function index() {

        $data['title'] 		= 'DAFTAR STOK SPAREPART';
        $data['sidebar'] 	= $this->access_right->menu();
        $data['content'] 	= 'base/page_content.tpl';     
        //$data['button_group'] 	= $this->getAvailableButtons();
        $data['table']['main'] 	= $this->getForm('table-main', '', array('wrapper'));
        $data['javascript'] = array('receiving.js');
        
        $this->twig->display('index.tpl', $data);
    }

    public function warehouse() {

        $data['title']      = 'DAFTAR STOK SPAREPART';
        $data['sidebar']    = $this->access_right->menu();
        $data['content']    = 'base/page_content.tpl';     
        //$data['button_group']     = $this->getAvailableButtons();
        $data['table']['main']  = $this->getForm('table-main-warehouse', '', array('wrapper'));
        $data['javascript'] = array('receiving.js');
        
        $this->twig->display('index.tpl', $data);
    }

    public function edit($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('edit', true);
        $this->form('edit', $id);
    }

    public function form($type='add',  $id = NULL){
        $data['title'] = 'Stok';
        $data['content']['body'] = $this->getForm($type, $id);
        $data['isBtnPageBack']      = true;

        $this->twig->display('base/page_content.tpl', $data);
    }

    public function barcode($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('edit', true);
        $this->showbarcode($id);
    }

    public function showbarcode($id='',$status_delete = 0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->config->load('twig');
            
            $title = 'Tambah Data';
            $data['form_action'] = $this->class_name . '/proses';

            $data['id'] = $id;
            if($id){
                $title = 'Stock Sparepart Barcode';
                $row            = $this->item_stok_model
                                ->with(array('item'))
                                ->where(array("item_key" => $id))
                                ->get()->row();
                $data['data']  = $row;
            }

            $data['title']      = $title;
            $data['assets_url'] = $this->config->item('assets_url');
            $data['sidebar']    = $this->access_right->menu();
            $data['form']       = 'stok_ofname_sc/form_barcode.tpl';

            $button_group   = array();

            //BUTTON BACK
            $button_group[] = view::button_back();            
            //BUTTON PRINT
            $data['form_action']    = view::form_input($id);
            $button_group[]         = view::button_print_stok_sc();

            $data['button_group'] = view::render_button_group($button_group);
            //$data['options_hadiah'] = form_dropdown('data[hadiahtype_id]', $this->hadiah_type_model->options(), $row->hadiahtype_id, 'class="bs-select" disabled="disabled"');
            
            //$options_pulau = $this->pulau_model->options();
            //$data['options_pulau'] = form_dropdown('id_pulau',$options_pulau, !empty($def_id_pulau) ? $def_id_pulau : '', 'class = "form-control"'); 
            
            $this->twig->display('base/page_form.tpl', $data);

        } else {
            $this->access_right->redirect();
        }
    }


    public function getForm($type = '', $id = NULL, $param = array()){
        $this->load->library(array('custom_form','custom_table'));
        $this->load->model(array('sequence_model','po_store_model','koli_model'));
        $form = $this->custom_form;

        switch ($type) {
            case 'edit':
                $data['form']['main']   = $this->getForm('main', $id);
                $data['form']['target'] = $this->getForm('target', $id);
                $data['form']['area']   = $this->getForm('area', $id);
                return $this->twig->render($this->class_name.'/page_form.tpl', $data);
                break;
            case 'page-detail':
                $param['title']     = 'RECEIVING DETAIL';
                $param['portlet']   = true;
                $param['button']['back'] = true;
                $param['form']['info']  = $this->getForm('form-info', $id);
                $param['table']['main'] = $this->getForm('page-koli', $id);
                return $this->twig->render('base/page_content.tpl', $param);
                break;
            
            case 'table-main':
                $this->load->library('Datatable');
                $table = new Datatable();

                if (in_array('wrapper', $param)) {
                    $this->load->model('itemtype_model');
                    $data = $this->itemtype_model->options_filter();
                    $table->innerFilter = array(
                        array('itemtype_id', 'Item Type', 'list', $data),
                        array('item_name', 'Item Name', 'text'),
                    );
                    $table->dataFilter = $table->innerFilter;

                    $table->id          = 'table-stok';
                    $table->isScrollable= false;
                    $table->numbering   = true;
                    $table->header      = array('ITEM', 'TYPE', 'STORE','HARGA','STOK GUDANG','STOK GUDANG MUTASI', 'STOK TOTAL','NILAI NOMINAL','ACTION');
                    $table->source      = site_url($this->class_name.'/getform/table-main/');
                    return $table->generateWrapper();
                } else {
                    $table
                        ->setModel($this->item_stok_model)
                        ->setNumbering()
                        ->with(array('store','item'))
                        ->select('item_name, itemtype_id, nama_cabang,harga,stok_gudang,stok_gudang_mutasi, (stok_gudang + stok_gudang_mutasi) as total,(stok_total*harga) as nominal, item_key')
                        ->where('store_id',hprotection::getSC())
                        ->order_by('nama_cabang')
                        ->order_by('itemtype_id')
                        ->order_by('item_name')
                        ->edit_column('harga','$1','hgenerator::number(harga)')
                        ->edit_column('itemtype_id','$1','transaksi::getItemType(itemtype_id)')
                        ->edit_column('nominal','$1','hgenerator::number(nominal)')
                        ->edit_column('item_key', '$1', 'view::btn_group_edit_delete_stok_sc(item_key)');
                    echo $table->generate();
                }
                break;

            case 'table-main-warehouse':
                $this->load->library('Datatable');
                $table = new Datatable();

                if (in_array('wrapper', $param)) {
                    $this->load->model('itemtype_model');
                    $data = $this->itemtype_model->options_filter();
                    $innerFilter = array(
                        array('nama_cabang', 'Store', 'text'),
                        array('koli_code', 'Koli Code', 'text'),
                        array('note','Note','text'),
                        array('nama_supplier','Supplier','text')
                    );
                    $table->dataFilter = $innerFilter;

                    $table->id          = 'table-stok-warehouse';
                    $table->isScrollable= false;
                    $table->numbering   = true;
                    $table->header      = array('KOLI CODE','STORE', 'QTY', 'NOTE','SUPPLIER', 'STATUS', 'DATE RECEIVED', 'DATE SHIP', 'ACTION');
                    $table->source      = site_url($this->class_name.'/getform/table-main-warehouse/');
                    return $table->generateWrapper();
                } else {
                    $table
                        ->setModel($this->koli_model)
                        ->setNumbering()
                        ->with(array('store','supplier','shipping','sp','postore_koli','receiving'))
                        ->select('koli_code, nama_cabang, sum(qty) as qty, note,nama_supplier,koli_status,  receiving.date_created as da,sp.date_created as dc, koli_code as kc')
                        ->order_by('koli_code')                        
                        //->where('koli_status',prstatus::WAREHOUSE)
                        /*->order_by('itemtype_id')
                        ->order_by('item_name')*/
                        ->group_by('koli_code')
                        ->edit_column('koli_status','$1','prstatus::getStatus(koli_status)')
                        ->edit_column('kc', '$1', "view::button_view_stok(kc)");
                   echo $table->generate();
                }
                break;
            
            case 'table-stok-detail':

                $this->load->model('receiving_postore_model');
                $model = $this->receiving_postore_model;
                $code = !empty($code) ? $code : 0;
                
                $result = $model->with(array('receiving_po_koli','po_detail','item','koli','shipping_koli','shipping','supplier','store'))
                    //nama_cabang = destination
                    //shipping_code dijadikan global
                    ->select('koli_code, item_name, po_detail.qty_approved as qty, price, (po_detail.qty_approved*price) as sub_total, nama_cabang')
                    //->select('*')
                    ->where(array('koli_code' => $id))
                    ->get()
                    ->result_array();

                $table = new custom_table();

                $table->columns = array('DATE', 'STATUS','QTY APPROVED','PRICE','SUB TOTAL','STORE NAME');
                $table->id      = 'table-history';
                $table->data_source = site_url($this->class_name.'/getform/table-stok-detail/'.$id);
                $table->data    = $result;

                $content = $table->generateWithWrapper();

                $param['list']['content']=$content;
                $param['list']['koli_code']=$id;
                $data = $param;
                $this->twig->display($this->class_name.'/page_detail.tpl', $data);
                break;


            default:
                # code...
                break; 
        }
    }


    public function initPrinting(){
        $this->load->helper('printingdua');

        $ci =& get_instance();

        $nc = $ci->input->post('nc');

        $item_key = $ci->input->post('item_key');
        $item_name = $ci->input->post('item_name');

        $arrname = $this->cutName($item_name, 20);

        $cmd="";
        for($i=0;$i<$nc;$i++){
            $koli_ke = $i+1;
            $cmd .= "^XA
                    ^MMA
                    ^PW320
                    ^LL0240
                    ^LS0
                    ^BY3,3,71^FT38,104^BCN,,Y,N
                    ^FD>;".$item_key."^FS
                    ^FT39,162^A0N,23,24^FH\^FD".$arrname[0]."^FS";
            
            switch (count($arrname)) {
                case 2:
                    $cmd .= "^FT39,196^A0N,23,24^FH\^FD".$arrname[1]."^FS";
                    break;
                
                case 3:
                    $cmd .= "^FT39,196^A0N,23,24^FH\^FD".$arrname[1]."^FS";
                    $cmd .="^FT39,229^A0N,23,24^FH\^FD".$arrname[2]."^FS";
                    break;

                default:
                    # code...
                    break;
            }
            $cmd .= "^PQ".$nc.",0,1,Y^XZ";


/*            $cmd .= "^XA
                    ^MMA
                    ^PW609
                    ^LL0609
                    ^LS0
                    ^BY4,3,160^FT135,284^BCN,,Y,N
                    ^FD>;".$kd_sparepart."^FS
                    ^FT219,386^A0N,28,28^FH\^FD".$nama_sparepart."^FS
                    ^PQ1,0,1,Y^XZ";
*/        
        }


        
        $form = '<form id="myForm" action="">

    <input type="hidden" id="sid" name="sid" value="'.session_id().'" />
        <fieldset hidden>
            <legend>Client Printer Settings</legend>
            
            <div hidden>
                I want to:&nbsp;&nbsp;
                <select id="pid" name="pid">
                  <option value="0">Use Default Printer</option>
                  <option value="1" selected="selected">Display a Printer dialog</option>
                  <option value="2">Use an installed Printer</option>
                  <option value="3">Use an IP/Etherner Printer</option>
                  <option value="4">Use a LPT port</option>
                  <option value="5">Use a RS232 (COM) port</option>
                </select>
                <br />
                <br />
                <div id="info" class="alert alert-info" style="font-size:11px;"></div>                
                <br />
            </div>
            
            <div id="installedPrinter" hidden>
                <div id="loadPrinters" name="loadPrinters">
                WebClientPrint can detect the installed printers in your machine. <a onclick="javascript:jsWebClientPrint.getPrinters();" class="btn btn-success">Load installed printers...</a>
                </div>
                <label for="installedPrinterName">Select an installed Printer:</label>
                <select name="installedPrinterName" id="installedPrinterName"></select>

            

            </div>           
                        
        </fieldset>
        <fieldset hidden>
            <legend>Printer Commands</legend>
            
            <p>
                Enter the printers commands you want to send and is supported by the specified printer (ESC/P, PCL, ZPL, EPL, DPL, IPL, EZPL, etc). 
                <br /><br />
                <b>NOTE:</b> You can use the <b>hex notation for non-printable characters</b> e.g. for Carriage Return (ASCII Hex 0D) you can specify 0x0D
                
            </p>
            <br /><br />
            <div class="alert alert-info" style="font-size:11px;">
            <b>Upload Files</b><br />
            This online demo does not allow you to upload files. So, if you have a file containing the printer commands like a PRN file, Postscript, PCL, ZPL, etc, then we recommend you to <a href="http://www.neodynamic.com/products/printing/raw-data/php/download/" target="_blank">download WebClientPrint</a> and test it by using the sample source code included in the package. Feel free to <a href="http://www.neodynamic.com/support" target="_blank">contact our Tech Support</a> for further assistance, help, doubts or questions.             
            </div>            
        </fieldset>        
        <fieldset hidden>
            <legend hidden>Ready to print!</legend>
            <h3>Your settings were saved! Now its time to <a href="#" onclick="javascript:doClientPrint();" class="btn btn-large btn-success">Print</a></h3>           
        </fieldset>

        <textarea id="printerCommands" name="printerCommands" rows="10" cols="80" class="span9" hidden>'.$cmd.'</textarea>
    </form>';
    echo $form;

    }

    function cutName($name='', $maxlength=0){
        //$name = 'CHOCO MANIA BISKUIT 21GR72PC';
        $arrname=explode(' ',$name);
        
        $arrresult = array();
        $i=0;
        $j=0;
        $temp = '';
        while($i<count($arrname)){
            $temp .= $arrname[$i];            
            if(strlen($arrresult[$j])<$maxlength && strlen($temp)<$maxlength){
                if(strlen($arrresult[$j])==0){
                    $arrresult[$j] .= $arrname[$i];                    
                }else{
                    $arrresult[$j] .= ' '.$arrname[$i];
                }
            }else{
                $temp = '';
                $j++;
                $i--;
            }
            $i++;
        }
        return $arrresult;
    }



}
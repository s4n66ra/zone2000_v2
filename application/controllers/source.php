<?php

class source extends MY_Controller {

	public $loadModel = false;

    public function __construct() {
        parent::__construct();
    }

    public function icon(){
    	$this->load->library('custom_form');
    	$form = $this->custom_form;
    	$form->button_save	= false;
    	$form->portlet		= true;
    	$form->action_fullscreen = false;
    	$form->param 		= array('title'=>'SELECT ICON');
    	$form->url_form     = 'icon/page_main.tpl';
    	echo $form->generate();
    }

}
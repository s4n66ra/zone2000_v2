<?php

class mesin_kerusakan extends MY_Controller {

    public function __construct() {
        // Declaration
        parent::__construct();

        $this->load->model(array('kategori_mesin_model','kelas_model','koin_model'));
    }

    
    public function index() {
        //print_r($this->mesin_kerusakan_model->data_table());die;
        $data['title'] = 'Repair Jobs';
        $data['data_source'] = base_url($this->class_name . '/load');

        /* INSERT LOG */
        $this->access_right->activity_logs('view',$data['title']);
        /* END OF INSERT LOG */

        $data['button_group'] = $this->getAvailableButtons();
        $data['button_right'] = $this->getTools();

        $data['assets_url'] = $this->config->item('assets_url');
        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'base/page_content.tpl';
        $data['nama_pegawai'] = $this->currentUsername;
    
        $this->twig->display('index.tpl', $data);
    }

    public function load($page = 0) {
        $this->load->library("custom_table");

        $table = new stdClass();
        $header[0] = array(
                            "REF",1,1,
                            "MACHINE CODE",1,1,
                            "MACHINE TYPE", 1, 1,
                            "PROBLEM", 1, 1,
                            "STATUS",1,1,
                            "DATE", 1, 1,
                            "FINISH TARGET", 1, 1,
                        );

        if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
            $header[0] = hgenerator::merge_array_raw(
                            $header[0],
                            array("ACTION",1,1)
                        );
        }

        $table->header = $header;
        $table->id     = 't_mesin';
        $table->align = array('tanggal_bbm' => 'center', 'kd_bbm' => 'center','kd_purchase_order'=>'center','supplier'=>'center', 'aksi' => 'center');
        $table->style = "table table-striped table-bordered table-hover datatable dataTable";
        $table->model = "mesin_kerusakan_model->data_table";
        $table->limit = $this->limit;
        $table->page = $page;
        $data = $this->custom_table->generate_ajax($table);
        echo json_encode($data);
    }

    public function form($act = 'new', $id = ''){
        $data['form'] = $this->getForm($act, $id);
        $this->twig->display('base/page_form_part.tpl', $data);
    }

    public function add($act = 'new', $id = '') {
        $this->form();
    }

    public function edit($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('edit', true);
        $this->form('edit',$id); 
    }

    public function view($id_enc){
        $id = url_base64_decode($id_enc);
        $this->form('view',$id);
    }

    public function delete($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('delete', true);
        $this->form('delete',$id);
    }

    public function service($id = 0){
        echo $this->getForm('service', $id);

    }

    public function proses() {
        $data = $this->input->post('data');
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {

            $this->form_validation->set_rules('data[id_mesin]', 'Machine Type Code', 'required|trim');

            if ($this->form_validation->run()) {
                $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');

                $id = $this->input->post('id');
                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */

                if ($id == '') {
                    $data['id_cabang'] = $this->session->userdata('id_cabang');
                    if ($this->mesin_kerusakan_model->create($data)) {
                        $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh_filter()');
                        /* INSERT LOG */
                        $this->access_right->activity_logs('add','Tambah Cabang');
                        /* END OF INSERT LOG */
                    }
                } else {

                    // edit mesin_kerusakan
                    $status = $this->mesin_kerusakan_model->update($data, $id);

                    

                    if ($status) {
                        $message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', 'refresh_filter()');
                        /* INSERT LOG */
                        $this->access_right->activity_logs('edit','Edit BBM');
                        /* END OF INSERT LOG */
                    }
                }
            } else {
               $message = array(false, 'Terjadi Kesalahan', validation_errors(), '');
            }
            echo json_encode($message);
        } else {
            $this->access_right->redirect();
        }
    }

    public function proses_delete() {
        $id = $this->input->post('id');
        $this->access_right->otoritas('delete', true);
        if ($this->access_right->otoritas('delete', true)) {
            $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');
            if ($this->mesin_kerusakan_model->delete($id)) {
                $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', 'refresh_filter()');
                /* INSERT LOG */
                $this->access_right->activity_logs('delete','Delete BBM');
                /* END OF INSERT LOG */
            }
            echo json_encode($message);
        }
    }

    public function getForm($act='new', $id=''){
        $data = array();
        $data['act']    = $act;
        $data['id']     = $id;
        $data['form']       = $this->class_name.'/form.tpl';
        
        $button_group   = array();
        $button_group[] = view::button_back();

        if($id != ''){
            $row            = $this->mesin_kerusakan_model->getById($id)->row();
            $data['data']   = $row;
        }

        $this->load->model('mesin_model');
        $data['list_mesin']     =   form_dropdown(
                                        'data[id_mesin]', 
                                        $this->mesin_model->options_empty('m_mesin.id_cabang',$this->session->userdata('id_cabang')), 
                                        $row->id_mesin, 
                                        'class="form-control select2-mesin-kerusakan" data-placeholder="Select Machine"'
                                    );

        switch ($act) {
            case 'new':
                $title['title']         = 'REPARATION';
                $data['form_action']    = view::form_input($id);
                $button_group[]         = view::button_save();
                break;
            
            case 'edit':
                $title['title'] = 'EDIT REPARATION DATA';
                $data['act']    = 'edit';
                $data['form_action']    = view::form_input($id);
                $button_group[]         = view::button_save();
                break;

            case 'delete':
                $data['title']          = 'Delete REPARATION DATA';
                $data['form_action']    = view::form_delete($id);;
                $data['readonly']       = ' readonly="" ';
                $button_group[]         = view::button_delete_confirm();
                break;

            case 'view':
                $data['title']          = 'VIEW REPARATION DATA';
                $data['form_action']    = view::form_delete($id);;
                $data['readonly']       = ' readonly="" ';
                unset($button_group);
                break;

            case 'complete':
                $title['title'] = 'EDIT REPARATION DATA';
                $data['act']    = 'edit';
                $data['form']   = $this->getForm('edit',$id);
                $data['form_service'] = $this->getForm('service', $id);
                $data['data_source_service'] = base_url($this->class_name . '/service/'.$row->id_mesin_rusak);
                
                //$button_group[]         = view::button_save();
                return $this->twig->render($this->class_name.'/form_complete.tpl', $data);
                break;
            
            case 'service' :
                if($row->request_service==0){

                    $temp['button_group'] = view::render_button_group(array(
                        anchor(
                            'javascript://', 
                            '<i class="fa fa-send"></i> Send Request', 
                            array(
                                'id'            => 'button-send-service-request',
                                'class'         => 'btn blue', 
                                'onclick'       => "load_form_modal_2(this.id,'','#form-content-2')", 
                                'data-source'   => base_url('service/add?id_mesin_rusak='.$row->id_mesin_rusak),
                            )
                        )      
                    ));
                    return $this->twig->render($this->class_name.'/form_request.tpl', $temp);
                }else{
                    $this->load->model('service_model');
                    $service = $this->service_model->where('id_mesin_rusak',$row->id_mesin_rusak)->get()->row();
                    switch ($service->status_request) {
                        case 0:
                            // waiting
                            return 'waiting';
                            break;

                        case 1:
                            // accepted
                            $this->loadController('service');
                            return $this->service->getForm('confirmation', $service->id_service);
                            break;

                        case 2:
                            // waiting
                            $this->loadController('service');
                            return $this->service->getForm('confirmation', $service->id_service);
                            break;
                        
                        default:
                            # code...
                            break;
                    }
                }
                break;

            case 'view2':
                $data['title']          = 'REPARATION';
                $data['form']           = $this->class_name.'/form_view.tpl';

                $this->load->model('service_model');
                $service = $this->service_model->where('id_mesin_rusak', $row->id_mesin_rusak)->db()->get()->row();
                $data['service'] = $service;
                $data['form_service']   = $this->twig->render($this->class_name.'/form_service.tpl',$data);
                break;

            default:
                # code...
                break;
        }

        $data['button_group'] = view::render_button_group($button_group, array('class'=>'pull-right'));
        return $this->twig->render($this->class_name.'/form.tpl', $data);
    }


}

/* End of file bbm.php */
/* Location: ./application/controllers/bbm.php */

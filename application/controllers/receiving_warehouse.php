<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class receiving_warehouse extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array(
        	'receiving_warehouse_model','receiving_postore_model',
        	'receiving_postore_koli_model', 'koli_model',
        	'transaction_model', 'hadiah_type_model', 'hadiah_model'
        ));
        //$this->button['add'] = view::button_add(array('onclick'=>"btnLoadNextPage(this)"));
    }

    public function index() {

       /* $
        $data['sidebar'] 	= $this->access_right->menu();
        $data['content'] 	= 'base/page_content.tpl';     
        $data['button_group'] 	= $this->getAvailableButtons();
        $data['table']['main'] 	= $this->table_main(array('wrapper'));
        $data['javascript'] = array('receiving.js');
        
        $this->twig->display('index.tpl', $data);*/
        $data['title']       = 'WAREHOUSE RECEIVING';
        $data['sidebar']   = $this->access_right->menu();
        $data['content']    = 'base/page_content.tpl';    
        //$data['form']['info']  = $this->getForm('form-info', $id);
        $data['table']['main'] = $this->getForm('page-index', $id); 
        //$data['button_group']   = $this->getAvailableButtons();
        //$data['table']['main']  = $this->table_main(array('wrapper'));
        //$data['table']['main']  = $this->getForm('page-detail', $id);

        /*$param['title']     = 'LIST PO';
                $param['portlet']   = true;
                $param['button']['back'] = true;
                if(empty($id)){
                    $param['button']['portlet'] = view::render_button_group_portlet(array(
                                //view::button_save_form(),
                                view::button_receiving($id)
                        ));
                }
                $param['form']['info']  = $this->getForm('form-info', $id);
                $param['table']['main'] = $this->getForm('page-koli', $id);
                return $this->twig->render('base/page_content.tpl', $param);
*/
        $this->twig->display('index.tpl', $data);
    }

    public function table_main($option = array()){
        $this->load->library('Datatable');
        $table = $this->datatable;

        // if (in_array('filter', $option)) {
        //     $table->dataFilter = array(
        //                         array('nama_cabang', 'Store Name', 'text'),
        //                         array('nama_kota', 'City', 'text'),
        //                         array('telp', 'Phone', 'numeric'),
        //                         array('date_start', 'Date Start', 'range-date'),
        //                         array('store_area', 'Area', 'range-numeric')
        //                     );
        // }

        if (in_array('wrapper', $option)) {
            $table->numbering  = true;
            $table->id         = 'table-receiving';
            $table->isScrollable= false;
            $table->header     = array('WAREHOUSE', 'USER', 'DATE', 'ACTION');
            $table->source     = site_url($this->class_name.'/table_main');
            return $table->generateWrapper();
        } else {
        	$table
	            ->setModel($this->receiving_warehouse_model)
	            ->setNumbering()
	            ->with(array('store','employee'))
	            ->where('type', storetype::WAREHOUSE)
	            ->select('nama_cabang, nama_pegawai , date_created, receiving_id')
	            ->edit_column('date_created', '$1', 'date::longDate(date_created)')
	            ->edit_column('receiving_id', '$1', 'view::btn_group_receiving_warehouse(receiving_id)');
            echo $table->generate();
        }

    }

    public function add() {
    	/*$this->db->trans_start();
    	$data['type'] = storetype::WAREHOUSE;
    	$this->receiving_warehouse_model->create($data);
    	$id = $this->receiving_warehouse_model->insert_id;

    	$this->detail($id);*/
        $this->detail();
    }

    public function detail($id = '') {
    	echo $this->getForm('page-detail', $id);
    }

    public function add_koli($id = 0){
    	$ids = $this->input->post('ids');
    	echo $this->getForm('form-koli', $id, array('ids'=>$ids));
    }

     public function edit($id_enc){
        $id = 0;
        $ids = url_base64_decode($id_enc);
        //$ids = $this->input->post('ids');
        echo $this->getForm('edit-koli', $id, array('ids'=>$ids));
    }

    public function getForm($type = '', $id = NULL, $param = array()){
    	$this->load->library(array('custom_form','custom_table'));
    	$this->load->model(array('sequence_model','po_store_model','store_model'));
    	$form = $this->custom_form;

    	switch ($type) {
            case 'page-index':
                if(empty($id)){
                    $param['page']['create_koli']   = $this->getForm('table-postore', $id, array('wrapper'));
                }
                $param['page']['list_koli']     = $this->getForm('table-main-warehouse', $id, array('wrapper'));
                return $this->twig->render('receiving/warehouse/page_koli.tpl', $param);
                break;

    		case 'page-detail':
    			$param['title'] 	= 'LIST PO';
    			$param['portlet']	= true;
    			$param['button']['back'] = true;
                /*if(empty($id)){
                    $param['button']['portlet'] = view::render_button_group_portlet(array(
                                //view::button_save_form(),
                                view::button_receiving($id)
                        ));
                }*/
    			$param['form']['info'] 	= $this->getForm('form-info', $id);
    			$param['table']['main']	= $this->getForm('page-koli', $id);
    			return $this->twig->render('base/page_content.tpl', $param);
    			break;

    		case 'form-info' :
    			//$param['data']	 = $this->receiving_warehouse_model->with('store')->getById($id)->row();
                $param['data']   = $this->store_model->where('id_cabang',$this->session->userdata('store_id'))->get()->row();
    			return $this->twig->render('receiving/warehouse/form_info.tpl', $param);
    			break;

    		

    		case 'table-postore':
    			$this->load->library('Datatable');
		        $table = $this->datatable;

		        if (in_array('wrapper', $param)) {
		            $table->id         	= 'table-receiving-detail';
		            $table->isScrollable= false;
		            $table->checklist   = true;
		            $table->pagination 	= true;
		            $table->header     	= array('PO', 'SUPPLIER', 'STORE NAME','JUMLAH','ITEM', 'TYPE','STATUS');
		            $table->source     	= site_url($this->class_name.'/getform/table-postore/'.$id);
		            $table->caption_btn_group 	= 'Receive';
		            $table->url_action_group 	= site_url($this->class_name.'/add_koli');
		            $table->callback_group_submit = '
		            	var param = {
		            		ids : grid.getSelectedRows()
		            	};
		            	console.log(param);
		            	var url = "'.site_url($this->class_name.'/add_koli/'.$id).'";
		            	loadModal(url, param);

		            ';
		            return $table->generateWrapper();
		        } else {
		        	$this->load->model('po_store_model');
		        	$table
			            ->setModel($this->po_store_model)
			            ->with(array('store','po', 'supplier', 'postore_koli','detail_po','item'))
			            ->where('this.po_type', itemtype::ITEM_MERCHANDISE)
                        ->where('postore_status', postatus::WAITING_RECEIVE)
			            ->where('koli_id is NULL')
			            ->select('this.postore_id, po.po_code, nama_supplier, nama_cabang, qty_approved,item_name , this.po_type, postore_status as status')
                        ->edit_column('status', '$1', 'transaksi::getPoStatus(status)')
			            ->edit_column('po_type', '$1', 'transaksi::getItemType(po_type)')
			            ->edit_column('postore_id', '<input type="checkbox" name="id[]" value="$1">','postore_id');

		            echo $table->generate();
		        }
    			break;
    		
    		case 'table-koli':
    			$this->load->library('Datatable');
		        $table = new Datatable();

		        if (in_array('wrapper', $param)) {
		            $table->id         	= 'table-receiving-koli';
		            $table->isScrollable= false;
		            $table->numbering 	= true;
		            $table->pagination 	= false;
		            $table->header     	= array('KOLI CODE', 'STORE', 'SUPPLIER', 'QTY','STATUS', 'ACTION');
		            $table->source     	= site_url($this->class_name.'/getform/table-koli/'.$id);
		            return $table->generateWrapper();
		        } else {
		        	$this->load->model('receiving_postore_koli_model');
		        	$model = $this->receiving_postore_koli_model;
                    $id = !empty($id)?$id:0;
		        	$table
			            ->setModel($model)
			            ->setNumbering()
			            ->with(array('koli','store','supplier','po_detail','item'))
			            ->where('receiving_id', $id)
			            ->group_by('this.koli_id')
			            ->select('koli_code, nama_cabang, nama_supplier, qty, koli_status as status, this.koli_id as act')
                        ->edit_column('status', '$1', 'transaksi::getTransStatus(status)');
		            echo $table->generate();
		        }
    			break;
    		

    		case 'form-koli' :
    			$this->load->model('store_model');
    			if($id){

    			}else{
    				
    			}

    			$poStores = $this->po_store_model->with(array('po','store'))->where_in('postore_id',$param['ids'])->get()->result();
    			$postore_id = $param['ids'][0];    			
    			$postore = $poStores[0];

    			$param['title']			= 'Create Koli';
    			$param['form_action'] 	= form_open(
                                            site_url($this->class_name.'/proses_koli'),
                                            '', array(
                                            	'id' => $id,
                                            	'postore_ids'=>json_encode($param['ids'])
                                            )
                                        );
    			$option = array($postore->store_id => $postore->nama_cabang);
    			$param['list']['store'] = form_dropdown('data[store_id]', $option, $postore->store_id, 'class="bs-select" disabled');
                $param['postores']          = $poStores;
                $param['data']['koli_code'] = code::getCode('koli',$postore_id);
                $param['data']['date_created'] = date('Y-m-d');

                $receiving = $this->po_store_model->with(array('detail_po','item','po'))->where_in('this.postore_id',$param['ids'])->get()->result();
                $param['table'] = $receiving;

    			$form->portlet          = true;
	            $form->button_back      = true;
	            $form->id               = 'form-koli';
	            $form->param            = $param;
	            $form->url_form         = 'receiving/warehouse/form_koli.tpl';
	            return $form->generate();
    			break;

            case 'edit-koli' :
                $this->load->model('store_model');
                if($id){

                }else{
                    
                }
                $data_koli = $this->receiving_warehouse_model->getpostore_id($postore_id = $param['ids'][0]);
                $postore_id = $data_koli->postore_id;
                $poStores = $this->po_store_model->with(array('po','store'))->where('postore_id',$postore_id)->get()->result();
                //$postore_id = $param['ids'][0];             
                $postore = $poStores[0];

                $param['title']         = 'Edit Koli';
                $param['form_action']   = form_open(
                                            site_url($this->class_name.'/proses_edit_koli'),
                                            '', array(
                                                'id' => $id,
                                                'receiving_id'=>json_encode($param['ids'])
                                            )
                                        );
                $option = array($postore->store_id => $postore->nama_cabang);
                $param['list']['store'] = form_dropdown('data[store_id]', $option, $postore->store_id, 'class="bs-select" disabled');
                $param['postores']          = $poStores;
                $param['data']['koli_code'] = $data_koli->koli_code;
                $param['data']['date_created'] = date::getDate($data_koli->date_created);
                $param['data']['qty'] = $data_koli->qty;
                $param['data']['note'] = $data_koli->note;
                $param['data']['koli_id'] = $data_koli->koli_id;

                $receiving = $this->po_store_model->with(array('detail_po','item','po'))->where('this.postore_id',$postore_id)->get()->result();
                $param['table'] = $receiving;

                $form->portlet          = true;
                $form->button_back      = true;
                $form->id               = 'form-koli';
                $form->param            = $param;
                $form->url_form         = 'receiving/warehouse/form_koli.tpl';
                return $form->generate();
                break;

            case 'table-main-warehouse':
                $this->load->library('Datatable');
                $table = new Datatable();

                if (in_array('wrapper', $param)) {
                    $this->load->model('itemtype_model');
                    $data = $this->itemtype_model->options_filter();
                    /*$innerFilter = array(
                        array('nama_cabang', 'Store', 'text'),
                        array('koli_code', 'Koli Code', 'text'),
                        array('note','Note','text'),
                        array('nama_supplier','Supplier','text')
                    );
                    $table->dataFilter = $innerFilter;*/

                    $table->id          = 'table-stok-warehouse';
                    $table->isScrollable= false;
                    $table->numbering   = true;
                    $table->header      = array('KOLI CODE','STORE', 'QTY KOLI', 'NOTE','ITEM','QTY','SUPPLIER', 'STATUS', 'ACTION');
                    $table->source      = site_url($this->class_name.'/getform/table-main-warehouse/');
                    return $table->generateWrapper();
                } else {
                    $table
                        ->setModel($this->koli_model)
                        ->setNumbering()
                        ->with(array('store','supplier','postore_koli','po_detail','item'))
                        ->select('koli_code, nama_cabang, qty, note,item_name,dc_qty_received,nama_supplier,koli_status, this.koli_id as ki')
                        ->order_by('koli_code')
                        ->where_in('itemtype_id',hprotection::getDcItem())
                        /*->order_by('itemtype_id')
                        ->order_by('item_name')*/
                        ->edit_column('koli_status','$1','prstatus::getStatus(koli_status)')
                        ->edit_column('ki', '$1', "view::btn_print_barcode_koli(ki)");
                        //->edit_column('id_hadiah', '$1', 'view::btn_group_edit_delete_merchandise(id_hadiah)');

                    echo $table->generate();
                }
                break;

    		default:
    			# code...
    			break;
    	}
    }

    public function proses_koli(){
    	$this->load->model(array('po_store_model', 'koli_model','receiving_postore_model','receiving_postore_koli_model','purchase_order_model','purchase_order_detail_model'));
    	$modelKoli = $this->koli_model;



    	$modelRePostore = $this->receiving_postore_model;
    	$modelRePostoreKoli = $this->receiving_postore_koli_model;
    	$modelPostore = $this->po_store_model;

    	$postore_ids = json_decode($this->input->post('postore_ids'),TRUE);
    	$receiving_id= $this->input->post('id');
    	$dataKoli = $this->input->post('data');

    	$postore = $modelPostore->getById($postore_ids[0])->row();

    	$dataInsertKoli['koli_code'] = code::getCodeAndRaise('koli', $postore_ids[0]);
    	$dataInsertKoli['store_id']  = $postore->store_id;
    	$dataInsertKoli['supplier_id'] = $postore->supplier_id;
        $dataInsertKoli['qty'] = $dataKoli['qty'];
        $dataInsertKoli['note'] = $dataKoli['note'];
        $dcQty = $this->input->post('dc_qty');
        

        $this->db->trans_start();
    	// [POINT] create koli
    	$modelKoli->create($dataInsertKoli);
    	$koliId = $modelKoli->insert_id;

        foreach ($dcQty as $key => $value) {
            $this->purchase_order_detail_model
            ->set('dc_qty_received',$value)
            ->set('dc_date_received',$dataKoli['date_created'])
            ->where('podetail_id',$key)
            ->update();
        }

    	// [POINT] create receiving_postore dan [POINT] receiving_postore_koli
    	$dataRePostore = array();
    	$dataRePostoreKoli = array();
    	
    	foreach ($postore_ids as $key => $value) {
    		$dataRePostore[] = array(
    				'receiving_id' => $receiving_id,
    				'postore_id' => $value,
    			);

    		$dataRePostoreKoli[] = array(
    				'receiving_id' => $receiving_id,
    				'koli_id' => $koliId,
    				'postore_id' => $value,
    			);
            $postore_ids[] = $value;
    	}

    	$modelRePostore->insert_batch($dataRePostore);
    	$modelRePostoreKoli->insert_batch($dataRePostoreKoli);
        //-----------------29092015-------------------------
        $this->purchase_order_model
        ->with('detail','repostore_koli')
        ->where_in('postore_id', $postore_ids)->set('po_status', postatus::DC_TRANSIT)->update();
        $this->po_store_model->update_postore(array('postore_status'=>postatus::DC_TRANSIT),$postore_ids);
        
        
        //==============================================

        //---------------- new 16092015---------------------
        $data['type'] = storetype::WAREHOUSE;
        $data['receiving_status'] = transactionstatus::WAREHOUSE;
        $data['date_created'] = $dataKoli['date_created'];
        $this->receiving_warehouse_model->create($data);
        $id = $this->receiving_warehouse_model->insert_id;

         //update zn_receiving_postore_koli

        $this->db->set('receiving_id',$id)
            ->where('receiving_id',0)
            ->update('zn_receiving_postore_koli');

        // [POINT] update status di koli. postore. podetail.prdetail
        $koliIds = $this->koli_model->select('this.koli_id')->with('postore_koli')->where('receiving_id', $id)->get()->result_array();
        $ids = array_values_index($koliIds, 'koli_id');
        $this->koli_model
            ->where_in('koli_id', $ids)
            ->set('koli_status',transactionstatus::WAREHOUSE)
            ->update();

        //=================end neew====================
    	
    	if($this->db->trans_status()){
            $message = array('success'=>true, 'message'=>'Save Success');
        }else{
            $message = array('success'=>false, 'message'=>$this->db->_error_message());
        }
        $this->db->trans_complete();
        echo json_encode($message);
    }

    public function proses_edit_koli(){
        $this->load->model(array('po_store_model', 'koli_model','receiving_postore_model','receiving_postore_koli_model','purchase_order_model','purchase_order_detail_model'));
        $modelKoli = $this->koli_model;

        $receiving_id = json_decode($this->input->post('receiving_id'),TRUE);
        $dataKoli = $this->input->post('data');

        $dataInsertKoli['qty'] = $dataKoli['qty'];
        $dataInsertKoli['note'] = $dataKoli['note'];
        $dcQty = $this->input->post('dc_qty');
        

        $this->db->trans_start();
        $modelKoli->update_koli($dataInsertKoli,$dataKoli['koli_id']);

        foreach ($dcQty as $key => $value) {
            $this->purchase_order_detail_model
            ->set('dc_qty_received',$value)
            ->set('dc_date_received',$dataKoli['date_created'])
            ->where('podetail_id',$key)
            ->update();
        }

        //============ beres proses update koli======================
        
        if($this->db->trans_status()){
            $message = array('success'=>true, 'message'=>'Save Success');
        }else{
            $message = array('success'=>false, 'message'=>$this->db->_error_message());
        }
        $this->db->trans_complete();
        echo json_encode($message);
    }


    public function barcode($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('edit', true);
        $this->showbarcode($id);
    }


    public function showbarcode($id='',$status_delete = 0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->config->load('twig');
            
            $title = 'Tambah Data';
            $data['form_action'] = $this->class_name . '/proses';

            $data['id'] = $id;
            if($id){
                $title = 'Koli Barcode';
                $row            = $this->koli_model
                                    ->with(array('store','postore_koli', 'po_detail'))
                                    ->where('this.koli_id', $id)
                                    ->get()
                                    ->row();
                $data['data']  = $row;
            }

            $data['title']      = $title;
            $data['assets_url'] = $this->config->item('assets_url');
            $data['sidebar']    = $this->access_right->menu();
            $data['form']       = 'receiving/warehouse/form_barcode.tpl';

            $button_group   = array();
            $button_group[] = view::button_back();
            
            if($status_delete == 0){
                $data['form_action']    = view::form_input($id);
                //INI BUTTON UNTUK PRINT BARCODE KOLI
                $button_group[]         = view::button_print_koli();
            }else{
                $data['title']          = 'Delete Merchandise';
                $data['form_action']    = view::form_delete($id);;
                $data['readonly']       = 'readonly=""';
                $button_group[]         = view::button_delete_confirm();
            }

            $data['button_group'] = view::render_button_group($button_group);
            $data['options_hadiah'] = form_dropdown('data[hadiahtype_id]', $this->hadiah_type_model->options(), $row->hadiahtype_id, 'class="bs-select" disabled="disabled"');
            
            //$options_pulau = $this->pulau_model->options();
            //$data['options_pulau'] = form_dropdown('id_pulau',$options_pulau, !empty($def_id_pulau) ? $def_id_pulau : '', 'class = "form-control"'); 
            
            $this->twig->display('base/page_form.tpl', $data);

        } else {
            $this->access_right->redirect();
        }
    }




    public function proses_receiving(){
        $this->db->trans_start();
        $data['type'] = storetype::WAREHOUSE;
        $data['receiving_status'] = transactionstatus::WAREHOUSE;
        $this->receiving_warehouse_model->create($data);
        $id = $this->receiving_warehouse_model->insert_id;

        //update zn_receiving_postore_koli

        $this->db->set('receiving_id',$id)
            ->where('receiving_id',0)
            ->update('zn_receiving_postore_koli');

        // [POINT] update table receiving set status
        
        //$this->receiving_warehouse_model->update($data, $id_receiving);

        // [POINT] update status di koli. postore. podetail.prdetail
        $koliIds = $this->koli_model->select('this.koli_id')->with('postore_koli')->where('receiving_id', $id)->get()->result_array();
        $ids = array_values_index($koliIds, 'koli_id');
        $this->koli_model
            ->where_in('koli_id', $ids)
            ->set('koli_status',transactionstatus::WAREHOUSE)
            ->update();

        if($this->db->trans_status()){
            $message = array(
                        'success' => true,     
                        'message' => 'Save Success',
                        'callback'=> "refreshDataTable(true)",        
                    );
        }else{
            $message = array(
                        'success' => false,                        
                        'message' => 'Database Error Occured',
                    );
        }
        $this->db->trans_complete();
        echo json_encode($message);

    }


    public function initPrinting(){
        $this->load->helper('printingdua');

        $ci =& get_instance();

        $nc = $ci->input->post('nc');
        $koli_id = $ci->input->post('koli_id');
        $koli_code = $ci->input->post('koli_code');
        $qty = $ci->input->post('qty');
        $note = $ci->input->post('note');
        $koli_status = $ci->input->post('koli_status');
        $store_id = $ci->input->post('store_id');
        $supplier_id = $ci->input->post('supplier_id');
        $sp_code = $ci->input->post('sp_code');
        $dc_qty_received = $ci->input->post('dc_qty_received');
        $kd_cabang = $ci->input->post('kd_cabang');

        $arrname = $this->cutName($nama, 20);
        $cmd="
                        
            ";
        for($h=0;$h<$nc;$h++){
            for($i=0;$i<$qty;$i++){
                $koli_ke = $i+1;

                $cmd .= "^XA
                        ^MMA
                        ^PW812
                        ^LL0609
                        ^LS0
                        ^BY2,3,109^FT56,190^BCN,,Y,N
                        ^FD>:".$koli_code."/".$koli_ke."/".$qty."^FS
                        ^FT61,281^A0N,28,33^FH\^FDToko                 : ".$kd_cabang."^FS
                        ^FT61,314^A0N,28,33^FH\^FDJml Karung        : ".$qty."^FS
                        ^FT61,348^A0N,28,33^FH\^FDNo. Karung        : ".$koli_code."/".$koli_ke."/".$qty."^FS
                        ^FT61,382^A0N,28,33^FH\^FDKeterangan        :^FS
                        ^FT61,416^A0N,28,33^FH\^FD".$note."^FS
                        ^FT61,450^A0N,28,33^FH\^FDTotal Qty           : ".$dc_qty_received."^FS
                        ^FT61,484^A0N,28,33^FH\^FD".date("d-m-Y H:i:s")."^FS
                        
                        ^PQ1,0,1,Y
                        ~JSB
                        ^XZ
                        ";

    /*            $cmd .= "^XA
                        ^MMA
                        ^PW609
                        ^LL0609
                        ^LS0
                        ^BY1,3,109^FT163,180^BCN,,Y,N
                        ^FD>:".$koli_code."/".$koli_ke."/".$qty."^FS
                        ^FT58,235^A0N,28,28^FH\^FDToko                 : ".$kd_cabang."^FS
                        ^FT58,269^A0N,28,28^FH\^FDJml Karung        : ".$qty."^FS
                        ^FT58,303^A0N,28,28^FH\^FDNo. Karung        : ".$koli_code."/".$koli_ke."/".$qty."^FS
                        ^FT58,337^A0N,28,28^FH\^FDKeterangan        :^FS
                        ^FT58,371^A0N,28,28^FH\^FD".$note."^FS
                        ^FT58,405^A0N,28,28^FH\^FDTotal Qty           : ".$dc_qty_received."^FS
                        ^FT58,439^A0N,28,28^FH\^FD".date("d-m-Y H:i:s")."^FS
                        ^PQ1,0,1,Y^XZ";
    */        
            }

        }
        $cmd .= "";

        
        $form = '<form id="myForm" action="">

    <input type="hidden" id="sid" name="sid" value="'.session_id().'" />
        <fieldset hidden>
            <legend>Client Printer Settings</legend>
            
            <div hidden>
                I want to:&nbsp;&nbsp;
                <select id="pid" name="pid">
                  <option value="0">Use Default Printer</option>
                  <option value="1" selected="selected">Display a Printer dialog</option>
                  <option value="2">Use an installed Printer</option>
                  <option value="3">Use an IP/Etherner Printer</option>
                  <option value="4">Use a LPT port</option>
                  <option value="5">Use a RS232 (COM) port</option>
                </select>
                <br />
                <br />
                <div id="info" class="alert alert-info" style="font-size:11px;"></div>                
                <br />
            </div>
            
            <div id="installedPrinter" hidden>
                <div id="loadPrinters" name="loadPrinters">
                WebClientPrint can detect the installed printers in your machine. <a onclick="javascript:jsWebClientPrint.getPrinters();" class="btn btn-success">Load installed printers...</a>
                </div>
                <label for="installedPrinterName">Select an installed Printer:</label>
                <select name="installedPrinterName" id="installedPrinterName"></select>

            

            </div>           
                        
        </fieldset>
        <fieldset hidden>
            <legend>Printer Commands</legend>
            
            <p>
                Enter the printers commands you want to send and is supported by the specified printer (ESC/P, PCL, ZPL, EPL, DPL, IPL, EZPL, etc). 
                <br /><br />
                <b>NOTE:</b> You can use the <b>hex notation for non-printable characters</b> e.g. for Carriage Return (ASCII Hex 0D) you can specify 0x0D
                
            </p>
            <br /><br />
            <div class="alert alert-info" style="font-size:11px;">
            <b>Upload Files</b><br />
            This online demo does not allow you to upload files. So, if you have a file containing the printer commands like a PRN file, Postscript, PCL, ZPL, etc, then we recommend you to <a href="http://www.neodynamic.com/products/printing/raw-data/php/download/" target="_blank">download WebClientPrint</a> and test it by using the sample source code included in the package. Feel free to <a href="http://www.neodynamic.com/support" target="_blank">contact our Tech Support</a> for further assistance, help, doubts or questions.             
            </div>            
        </fieldset>        
        <fieldset hidden>
            <legend hidden>Ready to print!</legend>
            <h3>Your settings were saved! Now its time to <a href="#" onclick="javascript:doClientPrint();" class="btn btn-large btn-success">Print</a></h3>           
        </fieldset>

        <textarea id="printerCommands" name="printerCommands" rows="10" cols="80" class="span9" hidden>'.$cmd.'</textarea>
    </form>';
    echo $form;

    }


function cutName($name='', $maxlength=0){
        //$name = 'CHOCO MANIA BISKUIT 21GR72PC';
        $arrname=explode(' ',$name);
        
        $arrresult = array();
        $i=0;
        $j=0;
        $temp = '';
        while($i<count($arrname)){
            $temp .= $arrname[$i];            
            if(strlen($arrresult[$j])<$maxlength && strlen($temp)<$maxlength){
                if(strlen($arrresult[$j])==0){
                    $arrresult[$j] .= $arrname[$i];                    
                }else{
                    $arrresult[$j] .= ' '.$arrname[$i];
                }
            }else{
                $temp = '';
                $j++;
                $i--;
            }
            $i++;
        }
        return $arrresult;
    }


}


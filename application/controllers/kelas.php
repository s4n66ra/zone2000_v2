<?php

/**
 * Description of bbm
 *
 * @author SANGGRA HANDI
 */
class kelas extends MY_Controller {
    
    public function __construct() {
        parent::__construct();
    }

    public function index() {

        $data['title'] = 'Machine Class';
        $data['data_source'] = base_url($this->class_name . '/load');

		$data['button_group'] = $this->getAvailableButtons();
        $data['button_right'] = $this->getTools();

        /* INSERT LOG */
        $this->access_right->activity_logs('view','Machine Class');

        $data['assets_url'] = $this->config->item('assets_url');
        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'base/page_content.tpl'; 
        $data['nama_pegawai'] = $this->currentUsername;    
        
        $this->twig->display('index.tpl', $data);
    }

	public function load($page = 0) {
		$this->load->library ( "custom_table" );
		
		$table = new stdClass ();
		$header [0] = array (
				"MACHINE CLASS CODE",1,1,
				"CLASS NAME",1,1,
				"MIN VALUE",1,1,
				"MAX VALUE",1,1 
		);
		
		if ($this->access_right->otoritas ( 'edit' ) || $this->access_right->otoritas ( 'delete' )) {
			$header [0] = hgenerator::merge_array_raw ( $header [0], array (
					"ACTION",1,1 
			) );
		}
		
		$table->header = $header;
		$table->id = 't_kelas';
		$table->align = array (
				'tanggal_bbm' => 'center',
				'kd_bbm' => 'center',
				'kd_purchase_order' => 'center',
				'supplier' => 'center',
				'aksi' => 'center' 
		);
		$table->style = "table table-striped table-bordered table-hover datatable dataTable";
		$table->model = "kelas_model->data_table";
		$table->limit = $this->limit;
		$table->page = $page;
		$data = $this->custom_table->generate_ajax ( $table );
		
		echo json_encode ( $data );
	}

    public function add($id='',$status_delete = 0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->config->load('twig');
            $this->load->model('kelas_model');

            $title = 'Tambah Data';
            $data['form_action'] = $this->class_name . '/proses';

            $data['id'] = $id;
            if ($id != '') {
                $title = 'Edit Data';

                $row = $this->kelas_model->get_by_id($id)->row();
				$data['data'] = $row;
            }

            $data['title']      = $title;
            $data['assets_url'] = $this->config->item('assets_url');
            $data['sidebar']    = $this->access_right->menu();
            $data['form']       = 'kelas/form_kelas.tpl';

            $button_group   = array();
            $button_group[] = view::button_back();

            if($status_delete == 0){
                $data['form_action']    = view::form_input($id);
                $button_group[]         = view::button_save();
            }else{
                $data['title']          = 'Delete Data supplier';
                $data['form_action']    = view::form_delete($id);;
                $data['readonly']       = 'readonly=""';
                $button_group[]         = view::button_delete_confirm();
            }

            $data['button_group'] = view::render_button_group($button_group);
            $this->twig->display('base/page_form.tpl', $data);
        } else {
            $this->access_right->redirect();
        }
    }

    public function edit($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('edit', true);
        $this->add($id);
    }

    public function delete($id_enc) {        
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('delete', true);
        $this->add($id,1);
    }

    public function proses() {        
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('data[kd_kelas]', 'Class Code', 'required|trim');
            $this->form_validation->set_rules('data[nama_kelas]', 'Class Name', 'required|trim');
            $this->form_validation->set_rules('data[min_value]', 'Min Value', 'required|trim');
            $this->form_validation->set_rules('data[max_value]', 'Min Value', 'required|trim');

            if ($this->form_validation->run()) {
                $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');

                $id = $this->input->post('id');
                $data = $this->input->post('data');

                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */

                if ($id == '') {
                    if ($this->kelas_model->create($data)) {
                        $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh_filter()');
						/* INSERT LOG */
						$this->access_right->activity_logs('add','Tambah Kelas');
						/* END OF INSERT LOG */
                    }
                } else {
                    if ($this->kelas_model->update($data, $id)) {
                        $message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', 'refresh_filter()');
						/* INSERT LOG */
						$this->access_right->activity_logs('edit','Edit Kelas');
						/* END OF INSERT LOG */
                    }
                }
            } else {
                $message = array(false, 'Terjadi Kesalahan', validation_errors(), '');
            }
            echo json_encode($message);
        } else {
            $this->access_right->redirect();
        }
    }

    public function proses_delete() {
        $id = $this->input->post('id');
		$this->access_right->otoritas('delete', true);
        if ($this->access_right->otoritas('delete', true)) {
            $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '','id : '.$id);
            if ($this->kelas_model->delete($id)) {
                $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', 'refresh_filter()');
				/* INSERT LOG */
				$this->access_right->activity_logs('delete','Delete Kelas');
				/* END OF INSERT LOG */
            }
            echo json_encode($message);
        }
    }

    public function add_detail($id_bbm = '') {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {

            $title = 'Tambah Data';
            $data['form_action'] = $this->class_name . '/proses_detail';

            $data['id'] = '';
            $data['id_bbm'] = $id_bbm;

        $data['title'] = '<i class="icon-edit"></i> ' . $title;
        $this->load->model('referensi_barang_model');
        $this->load->model('referensi_purchase_order_model');
        $data['options_barang'] = $this->referensi_barang_model->options();
        $data['options_purchase_order'] = $this->referensi_purchase_order_model->options();
            

            $this->load->view($this->class_name . '/form_detail', $data);
        } else {
            $this->access_right->redirect();
        }
    }

    public function edit_detail($id_bbm,$id) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->load->model('supplier_model');

            $title = 'Tambah Data';
            $data['form_action'] = $this->class_name . '/proses_detail';

            $data['id'] = $id;
            $data['id_bbm'] = $id_bbm;
            if ($id != '') {
                $title = 'Edit Data Detail';

                $db = $this->supplier_model->get_detail_by_id($id);
                if ($db->num_rows() > 0) {
                    $row = $db->row();
                    $data['default'] = $row;
                }
            }

        $data['title'] = '<i class="icon-edit"></i> ' . $title;
        $this->load->model('referensi_barang_model');
        $this->load->model('referensi_purchase_order_model');
        $data['options_barang'] = $this->referensi_barang_model->options();
        $data['options_purchase_order'] = $this->referensi_purchase_order_model->options();
            

            $this->load->view($this->class_name . '/form_detail', $data);
        } else {
            $this->access_right->redirect();
        }
    }

    public function proses_detail() {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('kd_barang', 'Nama Barang', 'required|trim');
            $this->form_validation->set_rules('jumlah', 'Jumlah', 'required|trim');


            if ($this->form_validation->run()) {
                $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');

                $id = $this->input->post('id');

                $data = array(
                    'id_barang' => $this->input->post('kd_barang'),
                    'jumlah_barang' => $this->input->post('jumlah'),
                );

                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */

                if ($id == '') {
                    $data['id_bbm'] = $this->input->post('id_bbm');
                    if ($this->supplier_model->create_detail($data)) {
                        $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh_filter()');
                        /* INSERT LOG */
                        $this->access_right->activity_logs('add','Tambah BBM');
                        /* END OF INSERT LOG */
                    }
                } else {
                    if ($this->supplier_model->update_detail($data, $id)) {
                        $message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', 'refresh_filter()');
                        /* INSERT LOG */
                        $this->access_right->activity_logs('edit','Edit BBM');
                        /* END OF INSERT LOG */
                    }
                }
            } else {
                $message = array(false, 'Terjadi Kesalahan', validation_errors(), '');
            }
            echo json_encode($message);
        } else {
            $this->access_right->redirect();
        }
    }

    public function delete_detail($id_bbm,$id) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->load->model('supplier_model');

            $title = 'Hapus Data';
            $data['form_action'] = $this->class_name . '/proses_delete_detail/'.$id;

            $data['id'] = $id;
            $data['id_bbm'] = $id_bbm;
            if ($id != '') {
                $title = 'Edit Data Detail';

                $db = $this->supplier_model->get_detail_by_id($id);
                if ($db->num_rows() > 0) {
                    $row = $db->row();
                    $data['default'] = $row;
                }
            }

        $data['title'] = '<i class="icon-edit"></i> ' . $title;
        $this->load->model('referensi_barang_model');
        $this->load->model('referensi_purchase_order_model');
        $data['options_barang'] = $this->referensi_barang_model->options();
        $data['options_purchase_order'] = $this->referensi_purchase_order_model->options();
            

            $this->load->view($this->class_name . '/form_detail_delete', $data);
        } else {
            $this->access_right->redirect();
        }
    }

    public function proses_delete_detail($id) {
        $this->access_right->otoritas('delete', true);
        if ($this->access_right->otoritas('delete', true)) {
            $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');
            if ($this->supplier_model->delete_detail($id)) {
                $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', 'refresh_filter()');
                /* INSERT LOG */
                $this->access_right->activity_logs('delete','Delete BBM');
                /* END OF INSERT LOG */
            }
            echo json_encode($message);
        }
    }

    public function excel($page = 0){

        $this->load->library("custom_table");

        $table = new stdClass();
        $header[0] = array(
                            "TANGGAL BBM",1,1,
                            "KODE BBM", 1, 1, 
                            "KODE PURCHASE ORDER", 1, 1, 
                            "SUPPLIER",1,1
                        );
        $table->header = $header;
        $table->id     = 't_bbm';
        $table->align = array('tanggal_bbm' => 'center', 'kd_bbm' => 'center','kd_purchase_order'=>'center','supplier'=>'center', 'aksi' => 'center');
        $table->style = "table table-striped table-bordered table-hover datatable dataTable";
        $table->model = "supplier_model->data_table_excel";
        $table->limit = $this->limit;
        $table->border = 1;
        $table->page = 1;
        $data = $this->custom_table->generate($table);
        
        //===========filter di excel===================
        $id_supplier= $this->input->post("kd_supplier");
        $id_purchase_order = $this->input->post('kd_po');
        $tanggal_awal = $this->input->post('tanggal_awal_bbm');
        $tanggal_akhir = $this->input->post('tanggal_akhir_bbm');
        $tahun_aktif = $this->input->post('tahun_aktif');
        $kd_bbm = $this->input->post('kd_bbm');
        if(empty($kd_bbm)){
            $bbm = 'Semua';
        }else{
            $bbm = $kd_bbm;
        }
        if(empty($tahun_aktif)){
            $tahun_aktif = date('Y');
        }
        if($id_supplier==''){
            $supplier = 'Semua';
        }else{
            $supplier       = $id_supplier;
        }
        if($id_purchase_order==''){
            $po = 'Semua';
        }else{
            $po = $id_purchase_order;
        }
        if($tanggal_awal==''||$tanggal_akhir==''){
            $tanggal_awal = 'Semua';
            $tanggal_akhir = 'Semua';
        }
        
        $filter = array(
            'Kode BBM'          =>$bbm,
            'Purchase Order'    =>$po,
            'Supplier'          =>$supplier,
            'Periode Awal'      =>$tanggal_awal,
            'Periode Akhir'     =>$tanggal_akhir,
            'Tahun Aktif'       =>$tahun_aktif);
        $data['filter'] = $filter;
        //----------------------------------------------------
        $date = date('Ymd His');
        $data['judul_kecil'] = 'Bukti Barang Masuk';
        $data['filename'] = 'Bukti Barang Masuk ('.$date.')';
        $this->load->view('cetak_excel',$data);
    }

    public function excel_detail($id_bbm) {
        $this->load->library("custom_table_detail");

        $table = new stdClass();
        $header[0] = array(
                            "KODE BARANG",1,1,
                            "NAMA BARANG",1,1,
                            "JUMLAH", 1, 1, 
                            "SATUAN", 1, 1, 
                            "KATEGORI",1,1
                        );

        $table->header = $header;
        $table->id     = 't_bbm_detail';
        $table->align = array('tanggal_bbm' => 'center', 'jumlah' => 'center','satuan'=>'center','kategori'=>'center', 'aksi' => 'center');
        $table->style = "table table-striped table-bordered table-hover datatable dataTable";
        $table->model = "supplier_model->data_table_detail_excel";
        $table->limit = $this->limit;
        $table->page = 1;
        $table->border = 1;
        $data = $this->custom_table_detail->generate($table);
        //============data detail==================
        $data_bbm = $this->supplier_model->get_by_id($id_bbm);
        if($data_bbm->num_rows()>0){
            $bbm = $data_bbm->row();
            $filter = array(
                'Tanggal BBM'       =>$bbm->tanggal_bbm,
                'Kode BBM'          =>$bbm->kd_bbm,
                'Kode PO'           =>$bbm->kd_po,
                'Kode Supplier'     =>$bbm->nama_supplier.' ('.$bbm->kd_supplier.')');
            $data['filter'] = $filter;
        }
        //------------------------------------------------
        
        $date = date('Ymd His');
        $data['judul_kecil'] = 'Bukti Barang Masuk Detail';
        $data['filename'] = 'Bukti Barang Masuk Detail ('.$date.')';
        $this->load->view('cetak_excel',$data);
    }



}

/* End of file bbm.php */
/* Location: ./application/controllers/bbm.php */

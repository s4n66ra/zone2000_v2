<?php

/**
 * Description of bbm
 *
 * @author SANGGRA HANDI
 */
class tiket extends My_Controller {

    private $id_hasil;

    public function __construct() {
        parent::__construct();
    }


    
    public function index() {

        $data['title'] = 'Ticket Master';
        $data['data_source'] = base_url($this->class_name . '/load');

        /* BUTTON GROUP */
        $data['button_group'] = $this->getAvailableButtons();
        $data['button_right'] = $this->getTools();

        /* INSERT LOG */
        $this->access_right->activity_logs('view','Province Master');


        /* END OF INSERT LOG */
        $data['assets_url'] = $this->config->item('assets_url');
        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'base/page_content.tpl';
        $data['nama_pegawai'] = $this->currentUsername;

        $data['input_kd_tiket'] = form_input('kd_tiket', '', 'class="form-control input-inline input-medium"');
        $data['input_nama_tiket'] =  form_input('nama_tiket', '', 'class="form-control input-inline input-medium"');     
        
        $this->twig->display('index.tpl', $data);
    }

    public function load($page = 0) {
        $this->load->library("custom_table");

        $table = new stdClass();
        $header[0] = array(
                            "TICKET CODE",1,1,
                            "TICKET NAME", 1, 1,
                            "ITEM NAME", 1, 1

                        );

        if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
            $header[0] = array(
                            "TICKET CODE",1,1,
                            "TICKET NAME", 1, 1,
                            "ITEM NAME", 1, 1,
                            "ACTION",1,1
                            );
        }

        $table->header = $header;
        $table->id     = 't_bbm';
        $table->align = array('tanggal_bbm' => 'center', 'kd_bbm' => 'center','kd_purchase_order'=>'center','tiket'=>'center', 'aksi' => 'left');
        $table->style = "table table-striped table-bordered table-hover datatable dataTable";
        $table->model = "tiket_model->data_table";
        $table->limit = $this->limit;
        $table->page = $page;
        $data = $this->custom_table->generate_ajax($table);

        echo json_encode($data);
    }


    public function add($id='',$status_delete = 0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->config->load('twig');
            $this->load->model('tiket_model');
            $this->load->model('item_model');

            $title = 'Tambah Data';
            $data['form_action'] = $this->class_name . '/proses';

            $data['id'] = $id;
            if ($id != '') {
                $title = 'Edit Data';

                $db = $this->tiket_model->get_by_id($id);
                if ($db->num_rows() > 0) {
                    $row = $db->row();
                    $data['default'] = $row;
                    $data['def_kd_tiket'] = $row->kd_tiket;
                    $data['def_nama_tiket'] = $row->nama_tiket;
                    $data['def_price'] = $row->price;

                }
            }

            $data['title']      = $title;
            $data['assets_url'] = $this->config->item('assets_url');
            $data['sidebar']    = $this->access_right->menu();
            $data['form']       = 'tiket/form_tiket.tpl';

            $htmlOptions =  'class="form-control select2me"';
            $data['list_item']  = form_dropdown('item_id', $this->item_model->options_empty(), $row->item_id, $htmlOptions.' data-placeholder="---Select Item Name---"');


            $button_group   = array();
            $button_group[] = view::button_back();
            
            if($status_delete == 0){
                $data['form_action']    = view::form_input($id);
                $button_group[]         = view::button_save();
            }else{
                $data['title']          = 'Delete Province Data';
                $data['form_action']    = view::form_delete($id);;
                $data['readonly']       = 'readonly=""';
                $button_group[]         = view::button_delete_confirm();
            }

            $data['button_group'] = view::render_button_group($button_group);            
            $this->twig->display('base/page_form.tpl', $data);

        } else {
            $this->access_right->redirect();
        }
    }

    public function edit($id_enc) {
        $id = url_base64_decode($id_enc);
		$this->access_right->otoritas('edit', true);
        $this->add($id);
    }

    public function delete($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('delete', true);
        $this->add($id,1);
    }

    public function proses() {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('kd_tiket', 'Kode Tiket', 'required|trim');
            $this->form_validation->set_rules('nama_tiket', 'Nama Tiket', 'required|trim');

            if ($this->form_validation->run()) {
                $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');

                $id = $this->input->post('id');
                $this->load->model("item_model");

                $data0 = array(
                    'item_key' => $this->input->post('kd_tiket'),
                    'item_name' => $this->input->post('nama_tiket'),
                    'itemtype_id' => 5,
                    'price' => $this->input->post('price')
                );

                $this->setIdHasil();

                $data = array(
                    'kd_tiket' => $this->input->post('kd_tiket'),
                    'nama_tiket' => $this->input->post('nama_tiket'),
                    'item_key' => $this->input->post('kd_tiket'),
                    'item_id' => $this->id_hasil
                );

                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */

                if ($id == '') {
                    if ($this->item_model->create($data0)) {
                        $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh_filter()');
						/* INSERT LOG */
						$this->access_right->activity_logs('add','Tambah Item');
						/* END OF INSERT LOG */
        
                        $this->load->model('item_model');

                        empty($data);
                        //$this->setIdHasil();
                        $this->id_hasil = $this->db->insert_id();
                        $data = array(
                            'kd_tiket' => $this->input->post('kd_tiket'),
                            'nama_tiket' => $this->input->post('nama_tiket'),
                            'item_key' => $this->input->post('kd_tiket'),
                            'item_id' => $this->id_hasil
                        );


                        if ($this->tiket_model->create($data)) {
                            $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh_filter()');
                            /* INSERT LOG */
                            $this->access_right->activity_logs('add','Tambah Tiket');
                            /* END OF INSERT LOG */
                        }



                    }
                    


                } else {
                    if ($this->item_model->update($data0, $this->id_hasil)) {

                        $message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', 'refresh_filter()');
						/* INSERT LOG */
						$this->access_right->activity_logs('edit','Edit Item');
						/* END OF INSERT LOG */
                        if ($this->tiket_model->update($data, $id)) {
                            $message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', 'refresh_filter()');
                            $this->access_right->activity_logs('edit','Edit Tiket');
                        }
                    }

                }
            } else {
                $message = array(false, 'Terjadi Kesalahan', validation_errors(), '');
            }
            echo json_encode($message);
        } else {
            $this->access_right->redirect();
        }
    }

    public function setIdHasil(){
        $this->load->model('item_model');
        $temp_id = $this->input->post('id');

        if($temp_id==null){
            $datatemp = $this->item_model->get_by_key($this->input->post('kd_tiket'))->result();                        
        }else{
            $datatemp = $this->item_model->get_by_id($temp_id)->result();            
        }

        foreach ($datatemp as $key => $value) {
            $this->id_hasil = $value->item_id;
        }

    }

    public function proses_delete() {
        $id = $this->input->post('id');
        $this->access_right->otoritas('delete', true);
        if ($this->access_right->otoritas('delete', true)) {
            $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');
            if ($this->tiket_model->delete($id)) {
                $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', 'refresh_filter()');
                /* INSERT LOG */
                $this->access_right->activity_logs('delete','Delete Tiket');
                /* END OF INSERT LOG */
            }
            echo json_encode($message);
        }
    }




}

/* End of file bbm.php */
/* Location: ./application/controllers/bbm.php */

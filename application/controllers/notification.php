<?php

/**
 * Description of bbm
 *
 * @author SANGGRA HANDI
 */


class notification extends My_Controller {


    public function __construct() {
        parent::__construct();
        $this->load->model(array('hadiah_type_model', 'hadiah_model', 'notification_model', 'user_model', 'notification_user_model', 'notification_store_model'));
    }

    
    public function index() {
        $data['title'] = 'Notification';

        /* INSERT LOG */
        //$this->access_right->activity_logs('view',$data['title']);
        /* END OF INSERT LOG */

        //print_r($this->session->all_userdata());
        $all_ud = $this->session->all_userdata();

        /*//ANGKA 15 ADALAH ANGKA YANG MENANDAKAN SUPER ADMIN
        if($all_ud["kd_roles"]==15){
            $data['button_group'] = $this->getAvailableButtons();
            $data['form']['user_status']  = $this->table_main(array('wrapper', 'filter'));
            $data['form']['pr_status']  = $this->table_pr_status(array('wrapper', 'filter'));
            $data['kd_roles'] ='15';
        }else{*/
            $data['form']['user_status']  = $this->table_main(array('wrapper', 'filter'));   
            if($this->session->userdata('store_id') == hprotection::getHO()) {
                $data['form']['pr_status']  = $this->table_pr_status(array('wrapper', 'filter'));        
                $data['is_head_office'] = TRUE;
            }
            $data['kd_roles'] = '0';
       // }
     

        //$data['button_right'] = $this->getTools();

        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'base/page_content.tpl';
    
        $data['form']['main']=$this->twig->render($this->class_name."/page_form.tpl", $data);

        $this->twig->display('index.tpl', $data);
    }

    public function table_main($option = array()){
        $this->load->library('Datatable');
        $table = $this->datatable;
       // $tabledua = $this->datatable;

/*        if (in_array('filter', $option)) {
            $table->dataFilter = array(
                                array('nama_hadiah', 'Merchandise Name', 'text'),
                                array('this.hadiahtype_id', 'Type', 'list', $this->hadiah_type_model->options()),
                            );
        }
*/


        if (in_array('wrapper', $option)) {
            $table->numbering  = true;
            $table->isScrollable= false;
            $table->id         = 'table-hadiah';
            $table->header     = array('REC CREATED','MESSAGE', 'FROM');
            $table->source     = site_url($this->class_name.'/table_main');
            return $table->generateWrapper();
        } else {

            $table
                ->setModel($this->notification_store_model)
                ->setNumbering()

                ->select('rec_created, custom_message, nama_pegawai')
                ->with(array("notification", "store", "user","user_detail"))
                ->where('this.store_id', $this->session->userdata('store_id'))
                ->edit_column('status', '$1', 'view::statusNotification(status)')
                ->order_by('rec_created', desc);

            echo $table->generate();

        }

    }

    public function table_pr_status($option = array()){
        $this->load->library('Datatable');
        $table = $this->datatable;
        $tabledua = $this->datatable;

/*        if (in_array('filter', $option)) {
            $table->dataFilter = array(
                                array('nama_hadiah', 'Merchandise Name', 'text'),
                                array('this.hadiahtype_id', 'Type', 'list', $this->hadiah_type_model->options()),
                            );
        }
*/


            if (in_array('wrapper', $option)) {
                $table->numbering  = true;
                $table->isScrollable= false;
                $table->id         = 'table-pr-status';
                $table->header     = array('REC CREATED','STATUS','PP CODE','STORE', 'ACTION');
                $table->source     = site_url($this->class_name.'/table_pr_status');
                return $table->generateWrapper();
            } else {

                $table
                    ->setModel($this->notification_model)
                    ->setNumbering()
                    ->with(array("notif", "notif_pr", "purchase_request",'store'))
                    ->select('rec_created, this.status, pr_code,nama_cabang, this.notif_id')
                    //->select('rec_created, telp, custom_message, nama_cabang, this.status')
                    ->edit_column('status', '$1', 'view::statusNotification(status)')
                    ->edit_column('notif_id', '$1', 'view::btn_group_edit_delete_pr_notification(notif_id)')
                    ->where("this.kd_notif", "001")
                    ->order_by('status','asc')
                    ->order_by('notif_id', 'desc')
                    ;

                echo $table->generate();

            }
    }



    public function add($id='',$status_delete = 0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->config->load('twig');
            
            $title = 'Create New Message';
            $data['form_action'] = $this->class_name . '/proses';

            $data['id'] = $id;
            if($id){
                $title = 'View Notification';
                $row            = $this->notification_model
                                    ->with(array("notif", "notif_user"))
                                    ->where("this.notif_id",$id)
                                    ->get()
                                    ->row();
                $data['data']  = $row;
            }

            $data['title']      = $title;
            $data['assets_url'] = $this->config->item('assets_url');
            $data['sidebar']    = $this->access_right->menu();
            $data['form']       = $this->class_name.'/form_notification.tpl';

            $button_group   = array();
            $button_group[] = view::button_back();
            
            if($status_delete == 0){
                $data['form_action']    = view::form_input($id);
                $button_group[]         = view::button_save_notification();
            }else{
                $data['title']          = 'Delete Merchandise';
                $data['form_action']    = view::form_delete($id);;
                $data['readonly']       = 'readonly=""';
                $button_group[]         = view::button_delete_confirm();
            }

            $data['button_group'] = view::render_button_group($button_group);
            $data['options_hadiah'] = form_dropdown('data[hadiahtype_id]', $this->hadiah_type_model->options(), $row->hadiahtype_id, 'class="bs-select"');

            $htmlOptions            =  'id="list-user" class="form-control select2" data-placeholder="Select User"';
            $data['list']['user']     = form_dropdown('data[id_user]', $this->user_model->options_empty(), $row->id_user, $htmlOptions);
            
            //$options_pulau = $this->pulau_model->options();
            //$data['options_pulau'] = form_dropdown('id_pulau',$options_pulau, !empty($def_id_pulau) ? $def_id_pulau : '', 'class = "form-control"'); 
            
            $this->twig->display('base/page_form.tpl', $data);

        } else {
            $this->access_right->redirect();
        }
    }

    public function showbarcode($id='',$status_delete = 0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->config->load('twig');
            
            $title = 'Tambah Data';
            $data['form_action'] = $this->class_name . '/proses';

            $data['id'] = $id;
            if($id){
                $title = 'Merchandise Barcode';
                $row            = $this->hadiah_model->getById($id)->row();
                $data['data']  = $row;
            }

            $data['title']      = $title;
            $data['assets_url'] = $this->config->item('assets_url');
            $data['sidebar']    = $this->access_right->menu();
            $data['form']       = $this->class_name.'/form_barcode.tpl';

            $button_group   = array();
            $button_group[] = view::button_back();
            
            if($status_delete == 0){
                $data['form_action']    = view::form_input($id);
                $button_group[]         = view::button_print();
            }else{
                $data['title']          = 'Delete Merchandise';
                $data['form_action']    = view::form_delete($id);;
                $data['readonly']       = 'readonly=""';
                $button_group[]         = view::button_delete_confirm();
            }

            $data['button_group'] = view::render_button_group($button_group);
            $data['options_hadiah'] = form_dropdown('data[hadiahtype_id]', $this->hadiah_type_model->options(), $row->hadiahtype_id, 'class="bs-select" disabled="disabled"');
            
            //$options_pulau = $this->pulau_model->options();
            //$data['options_pulau'] = form_dropdown('id_pulau',$options_pulau, !empty($def_id_pulau) ? $def_id_pulau : '', 'class = "form-control"'); 
            
            $this->twig->display('base/page_form.tpl', $data);

        } else {
            $this->access_right->redirect();
        }
    }


    public function __add($id = '', $status_delete=0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->load->library('custom_form');
            $form = new custom_form();

            $param['title'] = 'New Merchandise';
            if($id){
                $param['title'] = 'Edit Merchandise';
                $row            = $this->hadiah_model->getById($id)->row();
                $param['data']  = $row;
            }

            if($status_delete == 0){
                $param['form_action']   = view::form_input($id);
            }else{
                $param['title']          = 'Delete Merchandise';
                $param['form_action']    = view::form_delete($id);
                $form->type = 'delete';
                //$button_group[]         = view::button_delete_confirm();
            }

            $param['list']['type'] = form_dropdown('data[hadiahtype_id]', $this->hadiah_type_model->options(), $row->hadiahtype_id, 'class="bs-select"');

            $form->portlet          = true;
            $form->button_back      = true;
            $form->id               = 'form-hadiah';
            $form->param            = $param;
            $form->url_form         = $this->class_name.'/form_notification.tpl';
            echo $form->generate();

        } else {
            $this->access_right->redirect();
        }
    }

    public function edit($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('edit', true);
        $this->add($id);
    }

    public function delete($id_enc = '') {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('delete', true);
        $this->add($id,$status_delete = 1);
    }

    public function barcode($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('edit', true);
        $this->showbarcode($id);
    }


    public function proses() {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('data[kd_hadiah]', 'Kode hadiah', 'required|trim');
            $this->form_validation->set_rules('data[nama_hadiah]', 'Nama hadiah', 'required|trim');
            $this->form_validation->set_rules('data[item_key]', 'Barcode / Item Key', 'required|trim');
            $this->form_validation->set_rules('data[harga]', 'Harga', 'required|trim');


            if ($this->form_validation->run()) {
                $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');

                $id = $this->input->post('id');
                $data = $this->input->post('data');

                $this->db->trans_start();
                if ($id){
                    $this->hadiah_model->update($data, $id);
                }else{
                    $this->hadiah_model->create($data);
                }
                
                if($this->db->trans_status()){
                    $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh_filter()');
                }else{
                    $message = array('success'=>false, 'message'=>'Database Error');
                }
                $this->db->trans_complete();
            } else {
                $message = array('success'=>false, 'message'=>validation_errors());
            }
            echo json_encode($message);
        } else {
            $this->access_right->redirect();
        }
    }

    public function proses_delete() {
        $id=$this->input->post("id");
		$this->access_right->otoritas('delete', true);
        if ($this->access_right->otoritas('delete', true)) {
            $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');
            if ($this->hadiah_model->delete($id)) {
                $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', 'refresh_filter()');
				/* INSERT LOG */
				$this->access_right->activity_logs('delete','Delete Merchandise');
				/* END OF INSERT LOG */
            }
            echo json_encode($message);
        }
    }


    public function initPrinting(){
        $this->load->helper('printingdua');

        $ci =& get_instance();
        $angka = $ci->input->post('angka');
        $nama = $ci->input->post('nama');
        $nc = $ci->input->post('nc');
        $kc = $ci->input->post('koli_code');

        $arrname = $this->cutName($nama, 20);

        $cmd .= "^XA";
        $cmd .= "^MMA";
        $cmd .= "^PW320";
        $cmd .= "^LL0240";
        $cmd .= "^LS0";
        $cmd .= "^BY3,3,106^FT36,134^BCN,,Y,N";
        $cmd .= "^FD>;".$angka."^FS";
        for($i=0;$i<count($arrname);$i++){
            $cmd .= "^FT36,185^A0N,20,19^FH\^FD".$arrname[$i]."^FS";
        }
        $cmd .= "^PQ".$nc.",0,1,Y^XZ";  

        $form = '<form id="myForm" action="">

    <input type="hidden" id="sid" name="sid" value="'.session_id().'" />
        <fieldset hidden>
            <legend>Client Printer Settings</legend>
            
            <div hidden>
                I want to:&nbsp;&nbsp;
                <select id="pid" name="pid">
                  <option value="0">Use Default Printer</option>
                  <option value="1" selected="selected">Display a Printer dialog</option>
                  <option value="2">Use an installed Printer</option>
                  <option value="3">Use an IP/Etherner Printer</option>
                  <option value="4">Use a LPT port</option>
                  <option value="5">Use a RS232 (COM) port</option>
                </select>
                <br />
                <br />
                <div id="info" class="alert alert-info" style="font-size:11px;"></div>                
                <br />
            </div>
            
            <div id="installedPrinter" hidden>
                <div id="loadPrinters" name="loadPrinters">
                WebClientPrint can detect the installed printers in your machine. <a onclick="javascript:jsWebClientPrint.getPrinters();" class="btn btn-success">Load installed printers...</a>
                </div>
                <label for="installedPrinterName">Select an installed Printer:</label>
                <select name="installedPrinterName" id="installedPrinterName"></select>

            

            </div>           
                        
        </fieldset>
        <fieldset hidden>
            <legend>Printer Commands</legend>
            
            <p>
                Enter the printers commands you want to send and is supported by the specified printer (ESC/P, PCL, ZPL, EPL, DPL, IPL, EZPL, etc). 
                <br /><br />
                <b>NOTE:</b> You can use the <b>hex notation for non-printable characters</b> e.g. for Carriage Return (ASCII Hex 0D) you can specify 0x0D
                
            </p>
            <br /><br />
            <div class="alert alert-info" style="font-size:11px;">
            <b>Upload Files</b><br />
            This online demo does not allow you to upload files. So, if you have a file containing the printer commands like a PRN file, Postscript, PCL, ZPL, etc, then we recommend you to <a href="http://www.neodynamic.com/products/printing/raw-data/php/download/" target="_blank">download WebClientPrint</a> and test it by using the sample source code included in the package. Feel free to <a href="http://www.neodynamic.com/support" target="_blank">contact our Tech Support</a> for further assistance, help, doubts or questions.             
            </div>            
        </fieldset>        
        <fieldset hidden>
            <legend hidden>Ready to print!</legend>
            <h3>Your settings were saved! Now its time to <a href="#" onclick="javascript:doClientPrint();" class="btn btn-large btn-success">Print</a></h3>           
        </fieldset>

        <textarea id="printerCommands" name="printerCommands" rows="10" cols="80" class="span9" hidden>'.$cmd.'</textarea>
    </form>';
    echo $form;

    }

    function cutName($name='', $maxlength=0){
        //$name = 'CHOCO MANIA BISKUIT 21GR72PC';
        $arrname=explode(' ',$name);
        
        $arrresult = array();
        $i=0;
        $j=0;
        $temp = '';
        while($i<count($arrname)){
            $temp .= $arrname[$i];            
            if(strlen($arrresult[$j])<$maxlength && strlen($temp)<$maxlength){
                if(strlen($arrresult[$j])==0){
                    $arrresult[$j] .= $arrname[$i];                    
                }else{
                    $arrresult[$j] .= ' '.$arrname[$i];
                }
            }else{
                $temp = '';
                $j++;
                $i--;
            }
            $i++;
        }
        return $arrresult;
    }


    public function savenotif(){
        $arrlength = count($_POST);
        $user_id = $this->session->userdata('user_id');
        if($arrlength<=1){
            $status="gagal";
            echo json_encode(array("message"=>"failed"));
        }else{
            $dt = date("d-m-Y H:i:s");
            $custom_message = $_POST["custom_message"];
            $arr_fix = array("rec_created" => date("Y-m-d H:i:s"), "kd_notif" => "003", "status" => 0, "custom_message" => $custom_message,"user_id" => $user_id);



            //INSERT INTO DB
            $obj = new StdClass();
            if($this->notification_model->create($arr_fix)){
                $obj->message = "success";
                echo json_encode($obj);
            }else{
                $obj->message = "failed";
                echo json_encode($obj);
            }

            $notif_id_inserted = $this->db->insert_id();

            foreach ($_POST["destination"] as $key => $value) {
                $this->notification_store_model->create(array("notif_id"=> $notif_id_inserted, "store_id" => $_POST["destination"][$key], "status" => 0,"user_id"=>$user_id));                
            }


        }
    }

    public function read_notif($notif_store_id=''){
        $this->db->trans_start();

        $this->notification_store_model
            ->set("status",1)
            ->where("id", $notif_store_id)
            ->update();

        $obj = new StdClass();
        if($this->db->trans_status()){
            $temp_jml_notif = $this->session->userdata('jml_notif');
            if($temp_jml_notif >0)
                $this->session->set_userdata('jml_notif', $temp_jml_notif - 1);
            else
                $this->session->set_userdata('jml_notif',0);

            $obj->message = "success";
            echo json_encode($obj);

        }else{
            $obj->message = "failed";
            echo json_encode($obj);

        }
        $this->db->trans_complete();

    }

    public function read_notif_pr($notif_id=''){
        $this->db->trans_start();

        $this->notification_model
            ->set("status",1)
            ->where("notif_id", $notif_id)
            ->update();

        $obj = new StdClass();
        if($this->db->trans_status()){
            $temp_jml_notif = $this->session->userdata('jml_notif');
            if($temp_jml_notif >0)
                $this->session->set_userdata('jml_notif', $temp_jml_notif - 1);
            else
                $this->session->set_userdata('jml_notif',0);

            $obj->message = "success";
            echo json_encode($obj);

        }else{
            $obj->message = "failed";
            echo json_encode($obj);

        }
        $this->db->trans_complete();

    }


    public function get_jml_notif(){
        echo json_encode(array("jml_notif" => $this->session->userdata("jml_notif")));
    }



}




/* End of file bbm.php */
/* Location: ./application/controllers/bbm.php */

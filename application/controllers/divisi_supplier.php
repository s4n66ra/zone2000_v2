<?php

/**
 * Description of divisi_supplier
 *
 * @author SANGGRA HANDI
 */
class divisi_supplier extends My_Controller {


    public function __construct() {
        parent::__construct();
        $this->load->model('divisi_supplier_model');
    }

    
    public function index() {
        $data['title'] = 'Master Divisi Supplier';

        /* INSERT LOG */
        //$this->access_right->activity_logs('view',$data['title']);
        /* END OF INSERT LOG */

        $data['button_group'] = $this->getAvailableButtons();
        $data['button_right'] = $this->getTools();

        $data['table']['main']  = $this->table_main(array('wrapper', 'filter'));
        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'base/page_content.tpl';
    
        $this->twig->display('index.tpl', $data);
    }

    public function table_main($option = array()){
        $this->load->model('divisi_supplier_model');
        $this->load->library('Datatable');
        $table = $this->datatable;

        if (in_array('filter', $option)) {
            $table->dataFilter = array(
                                array('kd_divisi_supplier','divisi_supplier Code','text'),
                                array('nama_divisi_supplier', 'divisi_supplier Name', 'text'),
                                array('keterangan', 'Keterangan', 'text'),
                            );
        }

        if (in_array('wrapper', $option)) {
            $table->numbering  = true;
            $table->isScrollable= false;
            $table->id         = 'table-divisi_supplier';
            $table->header     = array('KODE DIVISI SUPPLIER','NAME','KETERANGAN','NO URUT','ACTION');
            $table->source     = site_url($this->class_name.'/table_main');
            return $table->generateWrapper();
        } else {
            $table
                ->setModel($this->divisi_supplier_model)
                ->setNumbering()
                ->select('kd_divisi_supplier, nama_divisi_supplier,keterangan,no_urut,id_divisi_supplier')
                ->edit_column('id_divisi_supplier', '$1', 'view::btn_group_edit_delete(id_divisi_supplier)');
            echo $table->generate();
        }

    }

    public function add($id='',$status_delete = 0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->config->load('twig');
            $this->load->model('item_model');
            $title = 'Form Tambah Divisi Supplier';
            $data['form_action'] = $this->class_name . '/proses';

            $data['id'] = $id;
            if($id){
                $title = 'Form Edit divisi_supplier';
                $row            = $this->divisi_supplier_model->getById($id)->row();
                $data['data']  = $row;
            }

            $data['title']      = $title;
            $data['assets_url'] = $this->config->item('assets_url');
            $data['sidebar']    = $this->access_right->menu();
            $data['form']       = $this->class_name.'/form.tpl';

            $button_group   = array();
            $button_group[] = view::button_back();
            
            if($status_delete == 0){
                $data['form_action']    = view::form_input($id);
                $button_group[]         = view::button_save();
            }else{
                $data['title']          = 'Delete Divisi Supplier';
                $data['form_action']    = view::form_delete($id);;
                $data['readonly']       = 'readonly=""';
                $button_group[]         = view::button_delete_confirm();
            }

            $data['button_group'] = view::render_button_group($button_group);            
            $this->twig->display('base/page_form.tpl', $data);

        } else {
            $this->access_right->redirect();
        }
    }

    

    public function edit($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('edit', true);
        $this->add($id);
    }

    public function delete($id_enc = '') {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('delete', true);
        $this->add($id,$status_delete = 1);
    }

    public function proses() {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('data[nama_divisi_supplier]', 'Nama Divisi Supplier', 'required|trim');
            $this->form_validation->set_rules('data[kd_divisi_supplier]', 'Kode Divisi Supplier', 'required|trim');

            if ($this->form_validation->run()) {
                $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');

                $id = $this->input->post('id');
                $data = $this->input->post('data');
                $this->db->trans_start();
                if ($id){
                    $row            = $this->divisi_supplier_model->getById($id)->row();
                    $dataold  = $row;
                    $this->divisi_supplier_model->update($data, $id);
                }else{
                    $this->divisi_supplier_model->create($data);
                }
                if($this->db->trans_status()){
                    $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh_filter()');
                }else{
                    $message = array(false, 'Terjadi Kesalahan',$this->db->_error_message(), '');
                }
                $this->db->trans_complete();
            } else {
                $message = array(false, 'Terjadi Kesalahan', validation_errors());
            }
            echo json_encode($message);
        } else {
            $this->access_right->redirect();
        }
    }

    public function proses_delete() {
        $id=$this->input->post("id");
        $this->access_right->otoritas('delete', true);
        if ($this->access_right->otoritas('delete', true)) {
            $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');
            if ($this->divisi_supplier_model->delete($id)) {
                $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', 'refresh_filter()');
                /* INSERT LOG */
                $this->access_right->activity_logs('delete','Delete Divisi Supplier');
                /* END OF INSERT LOG */
            }
            echo json_encode($message);
        }
    }

/*    public function getItemsByIdAndKeywordJSON($itemtype_id=''){
        $this->load->model(array('item_model'));

        $tmp = $this->item_model->getItemsByIdAndKeyword($itemtype_id);
        
        //$tmp = $this->item_model->getItemsByIdAndKeyword(2,'');
        $arr = array();
        foreach ($tmp as $k => $v) {
            if($k != ''){
                $obj = new StdClass();
                $obj->id        = $k;
                $obj->text      = $tmp[$k];
                array_push($arr, $obj);
            }
        }
        echo json_encode($arr);
    }*/





}

/* End of file divisi_supplier.php */
/* Location: ./application/controllers/bbm.php */

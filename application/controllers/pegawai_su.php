<?php

/**
 * Description of bbm
 *
 * @author SANGGRA HANDI
 */
class pegawai_su extends MY_Controller {

    public function __construct() {
    	$this->formConfig = array('full-width' => 1);
        parent::__construct();
        $this->load->model(array("user_model","pegawai_model"));
    }

    public function index() {
        $data['title'] = 'Employee Data';

        /* INSERT LOG */
        //$this->access_right->activity_logs('view',$data['title']);
        /* END OF INSERT LOG */

        $data['button_group'] = $this->getAvailableButtons();
        //$data['button_right'] = $this->getTools();
        //$data['button_right'] = $this->getPrintTools();
        
        $data['table']['main']  = $this->table_main(array('wrapper', 'filter'));
        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'base/page_content.tpl';
    
        $this->twig->display('index.tpl', $data);
    }

    public function table_main($option = array()){
        $this->load->model('hadiah_model');
        $this->load->library('Datatable');
        $table = $this->datatable;
        
        if (in_array('filter', $option)) {
            $table->dataFilter = array(
                                array('nama_cabang', 'Nama Cabang', 'text'),
                                array('nama_pegawai','Nama Pegawai','text'),
                                array('nik','EMPCODE','text'),
                                array('kd_level','KD Level','text'),
                                array('nama_level','Nama Level','text'),
                                array('grup_nama','Posisi','text'),
                                array('nama_divisi','Nama Divisi','text'),
                                
                            );
        }

        if (in_array('wrapper', $option)) {
            $table->numbering  = true;
            $table->isScrollable= false;
            $table->id         = 'table-hadiah';
            $table->header     = array('ID','NIK','EMPNAME', 'DIVISI','POSITION','KD LEVEL', 'IN DATE','CABANG','SALARY','USER ID','ACTION');
            $table->source     = site_url($this->class_name.'/table_main');
            return $table->generateWrapper();
        } else {
            $table
                ->setModel($this->pegawai_model)
                ->setNumbering()
                ->with(array('grup','level','user','cabang','divisi'))
                ->select('nik,this.id_karyawan, nama_pegawai,nama_divisi, grup_nama,kd_level,tgl_masuk,nama_cabang, salary,user_name,id_pegawai')
                ->edit_column('salary','$1','hgenerator::number(salary)')
                ->order_by('id_pegawai','desc')
                ->edit_column('id_pegawai', '$1', 'view::btn_group_edit_delete_full_width(id_pegawai)')
/*                ->order_by('this.id_hadiah')
                ->edit_column('id_hadiah', '$1', 'view::btn_group_edit_delete(id_hadiah)')
                ->edit_column('price_pax', '$1', 'hgenerator::number(price_pax)')
                ->edit_column('harga', '$1', 'hgenerator::number(harga)')*/;
            echo $table->generate();
        }

    }

    
	public function __index() {
        $data['title'] = 'Employee Data';
        $data['data_source'] = base_url($this->class_name . '/load');

        /* INSERT LOG */
        $this->access_right->activity_logs('view',$data['title']);
        /* END OF INSERT LOG */

        $data['button_group'] = $this->getAvailableButtons();
        $data['button_right'] = $this->getTools();

        $data['assets_url'] = $this->config->item('assets_url');
        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'base/page_content.tpl';
        $data['nama_pegawai'] = $this->currentUsername;
    
        $this->twig->display('index.tpl', $data);
    }

    public function load($page = 0) {
        $this->load->library("custom_table");

        $table = new stdClass();
        $header[0] = array(
                            "EMPCODE",1,1,
        					"EMPNAME",1,1,
                            "PHONE NUMBER",1, 1,
        					"ADDRESS",1,1,
                            "POSITION",1,1,
                            "KD LEVEL",1,1,
                            "LEVEL",1,1,
                            "IN DATE",1,1,
                            "SALARY",1,1,
                            "USER ID",1,1
                        );

        if ($this->access_right->otoritas('edit') || $this->access_right->otoritas('delete')) {
            $header[0] = hgenerator::merge_array_raw(
            			$header[0],
            			array("ACTION",1,1)
            		);
        }

        $table->header = $header;
        $table->id     = 't_bbm';
        $table->style = "table table-striped table-bordered table-hover datatable dataTable";
        $table->model = "pegawai_model->data_table";
        $table->limit = $this->limit;
        $table->page = $page;
        $data = $this->custom_table->generate_ajax($table);

        echo json_encode($data);
    }

	public function add($id='',$status_delete = 0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->load->model('pegawai_model');
            $this->load->model('store_model');
            $this->load->model('level_model');
            $this->load->model('jabatan_model');
            $this->load->model('user_model');
            $this->load->model('divisi_model');


            $title = 'Add Employee';
            $data['form_action'] = $this->class_name . '/proses';

            $data['id'] = $id;
            if ($id != '') {
                $title = 'Edit Data '.$id;
                $row = $this->pegawai_model->get_by_id($id)->row();
                $visibility = array('display' => ' style=display:none ');
                $data['data'] = $row;
                $data['additional_info'] = "<span>Keep it blank if you don't want to change password<span>";
            }
            $data['title']      = $title;
            $data['assets_url'] = $this->config->item('assets_url');
            $data['sidebar']    = $this->access_right->menu();
            $data['form']       = $this->class_name.'/form.tpl';

            if($myUser)
            	$users[$myUser->user_id] = $myUser->user_name;
            // dropdown island, province, city, area
            $htmlOptions =  'class="form-control select2me"';
            $data['roles']  = form_dropdown('data[grup_id]', $this->jabatan_model->options_empty(), $row->grup_id, $htmlOptions.' data-placeholder="Pilih Posisi"');
            $data['levels']  = form_dropdown('data[id_level]', $this->level_model->options_empty(), $row->id_level, $htmlOptions.' data-placeholder="Pilih Level"');
            $data['divisi']  = form_dropdown('data[id_divisi]', $this->divisi_model->options_empty(), $row->id_divisi, $htmlOptions.' data-placeholder="Pilih Divisi"');
            $data['branches_old']  = form_dropdown('data[current_location]', $this->store_model->options_empty(), $row->current_location, $htmlOptions.' data-placeholder="Pilih Lokasi Sekarang"');
            $data['branches']  = form_dropdown('data[default_store_id]', $this->store_model->options_empty(), $row->default_store_id, $htmlOptions.' data-placeholder="Pilih Lokasi Asal"');
            $data['branches_gaji']  = form_dropdown('data[lokasi_gaji]', $this->store_model->options_empty(), $row->lokasi_gaji, $htmlOptions.' data-placeholder="Pilih Lokasi Gaji"');

            $button_group   = array();
            $button_group[] = view::button_back();

            if($status_delete == 0){
                $data['form_action']    = view::form_input($id);
                $button_group[]         = view::button_save();
            }else{
                $data['title']          = 'Delete Employee';
                $data['form_action']    = view::form_delete($id);;
                $data['readonly']       = 'readonly=""';
                $button_group[]         = view::button_delete_confirm();
            }

            $data['button_group'] = view::render_button_group($button_group, array('class'=>'pull-right'));            
            $this->twig->display('base/page_form.tpl', $data);

        } else {
            $this->access_right->redirect();
        }
    }
    
    public function delete($id_enc) {
    	$id = url_base64_decode($id_enc);
    	$this->access_right->otoritas('delete', true);
    	$this->add($id,1);
    }
    
    public function proses_delete() {
    	$id = $this->input->post('id');
    	$this->access_right->otoritas('delete', true);
    	if ($this->access_right->otoritas('delete', true)) {
    		$message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');
    		if ($this->pegawai_model->delete($id)) {
    			$message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', 'refresh_filter()');
    			/* INSERT LOG */
    			$this->access_right->activity_logs('delete','Delete BBM');
    			/* END OF INSERT LOG */
    		}
    		echo json_encode($message);
    	}
    }

    public function proses() {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $id = $this->input->post('id');
            $data = $this->input->post('data');
            $_POST['pass'] = $data['user_password'];
            $_POST['confirm'] = $data['user_confirm_password'];
            
            if ($id == ''){
                /*$this->form_validation->set_rules('data[user_name]', 'Username', 'required|trim|is_unique[m_user.user_name]');
                $this->form_validation->set_rules('data[user_password]', 'Password', 'required|trim');
                $this->form_validation->set_rules('data[user_confirm_password]', 'Confirm Password', 'required|trim');*/
                if(strlen($data['user_password']) > 0){
                    $this->form_validation->set_rules('pass', 'Password', 'trim|matches[confirm]');
                    $this->form_validation->set_rules('confirm', 'Confirm Password', 'trim');
                }
            }else{
                $this->form_validation->set_rules('data[user_name]', 'Username', 'edit_unique[m_user.user_name.'.$data['user_id'].'.user_id]');
                if(strlen($data['user_password']) > 0){
                    $this->form_validation->set_rules('pass', 'Password', 'trim|matches[confirm]');
                    $this->form_validation->set_rules('confirm', 'Confirm Password', 'trim');
                }
            }
            
            $this->form_validation->set_rules('data[nik]', 'Employee Code', 'required|trim');
            $this->form_validation->set_rules('data[nama_pegawai]', 'Employee Name', 'required|trim');
            $this->form_validation->set_rules('data[grup_id]', 'Position', 'required|trim');
            $this->form_validation->set_rules('data[id_level]', 'Level', 'required|trim');
            $this->form_validation->set_rules('data[tgl_masuk]', 'In Date', 'required|trim');
            $this->form_validation->set_rules('data[salary]', 'Salary', 'required|numeric');
    
            if ($this->form_validation->run()) {
                $message = array(false, 'Process failed', 'Failed to save', '');
    
                if ($id == '') {
                    if ($this->pegawai_model->create($data)) {
                        $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh_filter()');
                        /* INSERT LOG */
                        $this->access_right->activity_logs('add','Add Employee');
                        /* END OF INSERT LOG */
                    }
                } else {
                    if ($this->pegawai_model->update($data, $id)) {
                        $message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', 'refresh_filter()');
                        /* INSERT LOG */
                        $this->access_right->activity_logs('edit','Edit Employee');
                        /* END OF INSERT LOG */
                    }
                }
            } else {
                $message = array(false, 'Validation Error', validation_errors(), '');
            }
            echo json_encode($message);
        } else {
            $this->access_right->redirect();
        }
    }
    
    public function __proses() {
    	if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
    		$id = $this->input->post('id');
    		$data = $this->input->post('data');
    		$_POST['pass'] = $data['user_password'];
    		$_POST['confirm'] = $data['user_confirm_password'];
    		
    		if ($id == ''){
    			$this->form_validation->set_rules('data[user_name]', 'Username', 'required|trim|is_unique[m_user.user_name]');
                $this->form_validation->set_rules('data[user_password]', 'Password', 'required|trim');
    			$this->form_validation->set_rules('data[user_confirm_password]', 'Confirm Password', 'required|trim');
    			if(strlen($data['user_password']) > 0){
    				$this->form_validation->set_rules('pass', 'Password', 'trim|matches[confirm]');
    				$this->form_validation->set_rules('confirm', 'Confirm Password', 'trim');
    			}
    		}else{
                $this->form_validation->set_rules('data[user_name]', 'Username', 'edit_unique[m_user.user_name.'.$data['user_id'].'.user_id]');
            }
    		
    		$this->form_validation->set_rules('data[nik]', 'Employee Code', 'required|trim');
    		$this->form_validation->set_rules('data[nama_pegawai]', 'Employee Name', 'required|trim');
    		$this->form_validation->set_rules('data[grup_id]', 'Position', 'required|trim');
    		$this->form_validation->set_rules('data[id_level]', 'Level', 'required|trim');
    		$this->form_validation->set_rules('data[tgl_masuk]', 'In Date', 'required|trim');
    		$this->form_validation->set_rules('data[salary]', 'Salary', 'required|numeric');
    
    		if ($this->form_validation->run()) {
    			$message = array(false, 'Process failed', 'Failed to save', '');
    
    			if ($id == '') {
    				if ($this->pegawai_model->create($data)) {
    					$message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', 'refresh_filter()');
    					/* INSERT LOG */
    					$this->access_right->activity_logs('add','Add Employee');
    					/* END OF INSERT LOG */
    				}
    			} else {
    				if ($this->pegawai_model->update($data, $id)) {
    					$message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', 'refresh_filter()');
    					/* INSERT LOG */
    					$this->access_right->activity_logs('edit','Edit Employee');
    					/* END OF INSERT LOG */
    				}
    			}
    		} else {
    			$message = array(false, 'Validation Error', validation_errors(), '');
    		}
    		echo json_encode($message);
    	} else {
    		$this->access_right->redirect();
    	}
    }

    public function edit($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('edit', true);
        $this->add($id);
    }

    //INI DIGUNAKAN UNTUK MENAMBAHKAN DI LIST USER
    public function getAllUsers(){
        
        $tmp = $this->user_model
                ->with(array("pegawai", "grup"))
                ->select("pegawai.user_id, nama_pegawai, grup_nama")
                ->get()
                ->result_array();

        //print_r();
        $arr = array();
        foreach ($tmp as $k => $v) {
            if($k != '' && $tmp[$k]["user_id"] != NULL && $tmp[$k]["user_id"] != $this->session->userdata("user_id")){
                $obj = new StdClass();
                $obj->id        = $tmp[$k]["user_id"];
                $obj->text      = $tmp[$k]["nama_pegawai"]."--".$tmp[$k]["grup_nama"];
                array_push($arr, $obj);
            }
        }
        echo json_encode($arr);
    }

    
}

/* End of file bbm.php */
/* Location: ./application/controllers/bbm.php */

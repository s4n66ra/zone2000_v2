<?php

class inventory_summary extends My_Controller {

    private $inventorytype = array();

    public function __construct() {
        parent::__construct();
        $this->limit = 5;
    }


    
    public function index() {
        $this->load->library("csvreader");

        $data['title'] = 'Inventory Summary';
        $data['data_source'] = base_url($this->class_name . '/load');
        $this->load->model('inventory_summary_model');

        /* INSERT LOG */
        $this->access_right->activity_logs('view',$data['title']);
        /* END OF INSERT LOG */


        $form_input_group = array("action" => "");
        $data['form_inventory_summary'] = view::form_inventory_summary('form_inventory_summary_id', $form_input_group);

        $htmlOptions            =  'id="list-typename" target="#list-province" data-source="'.site_url($this->class_name.'/getlist/inventory_summary').'" onchange="inventorytype_name_selected()" class="form-control select2me" data-placeholder="Select Inventory Type"';
        $options = $this->inventory_summary_model->getInventoryTypes();
        $data['typename_list']    = form_dropdown('typename', $options, '', $htmlOptions);


        $data['assets_url'] = $this->config->item('assets_url');
        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'base/page_content.tpl';
        $data['nama_pegawai'] = $this->currentUsername;
    
        $this->twig->display('index.tpl', $data);
    }


    public function load($page = 0, $keyword = '', $inventorytype_name = '') {

        $this->load->library("custom_table");

        $table = new stdClass();

        $header[0] = array(
                            "INVENTORY KEY", 1, 1,
                            "INVENTORY TYPE NAME", 1, 1,
                            "INVENTORY ID", 1, 1,
                            "STORE", 1, 1,
                            "NUM STOK", 1, 1

                        );


        $table->header = $header;
        $table->id     = 't_bbm';
        $table->align = array('tanggal_bbm' => 'center', 'kd_bbm' => 'center','kd_purchase_order'=>'center','supplier'=>'center', 'aksi' => 'left');
        $table->style = "table table-striped table-bordered table-hover datatable dataTable";
        $table->model = "inventory_summary_model->data_table";
        $table->limit = $this->limit;
        $table->page = $page;
        $table->keyword = $keyword;
        $data = $this->custom_table->generate_ajax($table);

        echo json_encode($data);

    }

    function readExcel(){
        $this->load->library('csvreader');
        $result =   $this->csvreader->parse_file('Test.csv');

        $data['csvData'] =  $result;
        $this->load->view('view_csv', $data);  
    }

    function loadforview(){

    }

}

/* End of file bbm.php */
/* Location: ./application/controllers/bbm.php */

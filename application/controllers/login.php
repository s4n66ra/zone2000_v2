<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class login extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("session");
    }

    function index() {
        hprotection::login(false);
        $data = array();
        $this->config->load('twig');
        $data['assets_url'] = $this->config->item('assets_url');
        $data['base_url'] = base_url();
        $this->twig->display('login.tpl', $data);
    }

    function logout() {
        $this->session->sess_destroy();
        redirect('login');
    }

    function cek_auth() {
        hprotection::login(false);
        $data = array();
        $this->config->load('twig');
        $data['assets_url'] = $this->config->item('assets_url');
        $data['base_url'] = base_url();
        $data[] = "";
        $this->load->model('login_model');
        $usernameExist = $this->login_model->isUsernameExist();
        $userExist = $this->login_model->isUserExist();
        $cekSupplier = $this->login_model->cekSupplier(hprotection::getSupllierGroupId());
        
        // var_dump($userExist);die;
        if (!$usernameExist) {
            $data['message'] = "User Tidak Tersedia / Tidak Boleh Kosong";
            $this->twig->display('login.tpl', $data);
        } else if (!$userExist) {
            $data['message'] = "Password User Salah / Tidak Boleh Kosong";
            $this->twig->display('login.tpl', $data);
        } else if($cekSupplier){
            $userExist = $this->login_model->getDataSupplier();
            $this->updateLastLogin($userExist);
            $this->setSessionSupplier($userExist); 
            redirect('web_supplier');
        } else {
        	$this->updateLastLogin($userExist);
        	$this->setSession($userExist); 
        	redirect('machine_transaction/redeem/');
        }
    }
    
    function setSession($user){
        // masih hard code menentukan yang login adalah head office atau bukan
        if($user->default_store_id==hprotection::getHO())
            $is_head_office = true;
        else
            $is_head_office = false;

        //HITUNG JUMLAH NOTIFIKASI
        $jml_notif = 0;

        $this->load->model(array('notification_model', 'notification_store_model'));

        if($user->grup_id == 15){
            $arr_data = $this->notification_model
                    ->select('*')
                    ->with(array('notif', 'notif_user', 'notif_pr', 'purchase_request'))
                    ->where(array('this.status'=>0, 'notif_user.user_id'=>$user->user_id))
                    ->get()
                    ->result_array();
            $jml_notif = count($arr_data);
        }else{
            $arr_data = $this->notification_store_model
                ->select('*')
                ->with(array('notification', 'store', 'pegawai'))
                ->where(array('this.status'=>0, 'this.store_id'=> $user->default_store_id))
                ->group_by('this.id')
                ->get()
                ->result_array();
            $jml_notif = count($arr_data);
        }


    	$sessionData = array(
    		'user_id' => $user->user_id,
    		'user_name' => $user->nama_pegawai,
    		'store_id'=> $user->default_store_id,
    		'kd_roles' => $user->grup_id,
    		'status_login'=>true,
            'is_head_office'=>$is_head_office,
            'jml_notif' => $jml_notif,
    	);


    	$this->session->set_userdata($sessionData);
    }

    function setSessionSupplier($user){
        // masih hard code menentukan yang login adalah head office atau bukan
        if($user->default_store_id==hprotection::getHO())
            $is_head_office = true;
        else
            $is_head_office = false;

        //HITUNG JUMLAH NOTIFIKASI
        $jml_notif = 0;

        $this->load->model(array('notification_model', 'notification_store_model'));

        if($user->grup_id == 15){
            $arr_data = $this->notification_model
                    ->select('*')
                    ->with(array('notif', 'notif_user', 'notif_pr', 'purchase_request'))
                    ->where(array('this.status'=>0, 'notif_user.user_id'=>$user->user_id))
                    ->get()
                    ->result_array();
            $jml_notif = count($arr_data);
        }else{
            $arr_data = $this->notification_store_model
                ->select('*')
                ->with(array('notification', 'store', 'pegawai'))
                ->where(array('this.status'=>0, 'this.store_id'=> $user->default_store_id))
                ->group_by('this.id')
                ->get()
                ->result_array();
            $jml_notif = count($arr_data);
        }


        $sessionData = array(
            'user_id' => $user->user_id,
            'user_name' => $user->nama_supplier,
            'store_id'=> $user->default_store_id,
            'kd_roles' => $user->grup_id,
            'status_login'=>true,
            'is_head_office'=>$is_head_office,
            'jml_notif' => $jml_notif,
            'supplier_id' => $user->id_supplier,
        );


        $this->session->set_userdata($sessionData);
    }
    
    function updateLastLogin($user){
    	$updateData['last_login'] = date("Y-m-d");
    	$this->login_model->update($updateData, $user->user_id);
    }

}

/* End of file login.php */
/* Location: ./application/controllers/login.php */
<?php

class laporan_servis extends MY_Controller {

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->load->model('service_model');
    }

    public function index() {
        $data['title'] = 'Laporan Service';        

        $data['button_group'] = '';// $this->getAvailableButtons();
        $data['button_right'] = $this->getPrintTools();

        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'base/page_content.tpl'; 
        $data['nama_pegawai'] = $this->currentUsername;    
        
        $data['table']['main'] = $this->table_main(array('wrapper', 'filter'));
        $this->twig->display('index.tpl', $data);

    }

    public function table_main($option = array(),$st_print = FALSE, $arrall = array()){
        $this->load->library('Datatable');
        $table = $this->datatable;
       
            //->edit_column('id_supplier', '$1', 'view::btn_group_edit_delete_page(id_supplier)');

        if (in_array('filter', $option)) {
            $table->dataFilter = array(
                array('serial_number', 'Kode Machine', 'text'),
                array('doc_number', 'Service No', 'text'),
                array('nama_cabang', 'Store', 'text'),
                array('kd_cabang', 'KD Store', 'text'),
                array('nama_jenis_mesin', 'Nama Mesin', 'text'),
                array('nama', 'Status Service', 'text'),
                array('issue_description', 'Issue', 'text'),
                array('loc_name','Location','text'),

                //array('this.hadiahtype_id', 'Type', 'list', $this->hadiah_type_model->options()),
            );
        }


        if (in_array('wrapper', $option)) {
            $table->id         = 'table-supplier';
            $table->numbering  = true;
            $table->header     = array('REG MACHINE','SERVICE NO','MACHINE', 'STORE','KD STORE', 'REQUEST DATE','DUE DATE','LOCATION', 'STATUS', 'PROBLEM');
            $table->source     = site_url($this->class_name.'/table_main');
            return $table->generateWrapper();
        } else { 
            $test = "like('nama_cabang', $arrdata[0], 'both')";
            $table  
                ->setModel($this->service_model)
                ->setNumbering()
                ->with(array('machine', 'store', 'transaction', 'repair','machine_type','document','v_detail','status_service'))
                ->select('serial_number,doc_number,nama_jenis_mesin,nama_cabang,kd_cabang ,rec_created,due_date,loc_name,nama,issue_description')
                ->or_like_in('nama_cabang', $arrall["arr_nama_cabang"], 'both')
                ->or_like_in('doc_number', $arrall["arr_doc_number"], 'both')
                ->or_like_in('issue_description', $arrall["arr_issue_description"], 'both')
                ->or_like_in('loc_name', $arrall["arr_loc_name"], 'both')
                ->or_like_in('nama_jenis_mesin', $arrall["arr_nama_jenis_mesin"], 'both')
                ->or_like_in('nama', $arrall["arr_nama"], 'both')
                ->edit_column('rec_created','$1','date::longDate(rec_created)')
                ->edit_column('due_date','$1','date::longDate(due_date)');

            if($st_print == TRUE)
                return $table->getData();
            else
                echo $table->generate();
        }
    }

  
   

    public function getCode($machine_id = NULL){
        if (!$machine_id)
            return;
        $storeId = $this->service_model->getStoreId($machine_id);
        echo code::getCodeAndRaise('sr', $storeId);
    }

    public function getPrintTools(){
        $button_group = array();
        if($this->access_right->otoritas('print')){
            $button_group[] = view::button_export2($this->class_name.'/excel');
        }
        
        if(true){//export
            //$button_group[] = view::button_export();
        }
        return view::render_button_group_laporan_payout($button_group, array(), true);

    }

    public function excel($store_id=4){
        $arr_nama_cabang = array();
        $arr_doc_number = array();
        $arr_nama_jenis_mesin = array();
        $arr_nama = array();
        $arr_issue_description = array();
        $arr_loc_name = array();

        $arrall = array();


        foreach ($_POST as $key => $value) {
            switch ($key) {
                case 'arr_nama_cabang':
                    foreach ($_POST['arr_nama_cabang'] as $key => $value) {
                        array_push($arr_nama_cabang, $_POST['arr_nama_cabang'][$key]);
                    }
                    break;

                case 'arr_doc_number':
                    foreach ($_POST['arr_doc_number'] as $key => $value) {
                        array_push($arr_doc_number, $_POST['arr_doc_number'][$key]);
                    }
                    break;

                case 'arr_nama_jenis_mesin':
                    foreach ($_POST['arr_nama_jenis_mesin'] as $key => $value) {
                        array_push($arr_nama_jenis_mesin, $_POST['arr_nama_jenis_mesin'][$key]);
                    }
                    break;

                case 'arr_nama':
                    foreach ($_POST['arr_nama'] as $key => $value) {
                        array_push($arr_nama, $_POST['arr_nama'][$key]);
                    }
                    break;

                case 'arr_issue_description':
                    foreach ($_POST['arr_issue_description'] as $key => $value) {
                        array_push($arr_issue_description, $_POST['arr_issue_description'][$key]);
                    }
                    break;

                case 'arr_loc_name':
                    foreach ($_POST['arr_loc_name'] as $key => $value) {
                        array_push($arr_loc_name, $_POST['arr_loc_name'][$key]);
                    }
                    break;

                
                default:
                    # code...
                    break;
            }
        }
        $arrall = array("arr_nama_cabang" => $arr_nama_cabang, "arr_doc_number" => $arr_doc_number, "arr_nama_jenis_mesin" => $arr_nama_jenis_mesin, "arr_nama" => $arr_nama, "arr_issue_description" => $arr_issue_description, "arr_loc_name" => $arr_loc_name );


        $this->load->model(array('store_model','laporan_payout_model'));        
        $header = array('REG MACHINE','SERVICE NO','MACHINE', 'STORE', 'REQUEST DATE', 'STATUS', 'PROBLEM');

        $data['header'] = $header;
        //----------------------------------------------------
        $date = date('Ymd His');
        $data['judul_kecil']    = 'Laporan Service';
        $data['filename']       = 'Laporan Service('.$date.')';
        $data['content']        = $this->table_main(array(),TRUE, $arrall);//$this->laporan_payout_model->data_detail(array("id_cabang" => $store_id));
        $data['selected_store'] = $this->store_model
                                 ->select('nama_cabang')
                                 ->where(array("id_cabang"=>hprotection::getSC()))
                                 ->get()
                                 ->row();
        $this->load->view('cetak_excel',$data);
    
    }


}

/* End of file bbm.php */
/* Location: ./application/controllers/bbm.php */

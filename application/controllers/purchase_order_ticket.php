<?php

class purchase_order_ticket extends MY_Controller {
    
    //global attribute for pdf
    private $arr_po_data='';
    private $arr_po_code=array();
    private $attr_po='';
    private $date_po='';
    private $glob_nama_supplier='';
    private $glob_alamat_supplier='';


    public function __construct() {
        parent::__construct();
        $this->load->model(array('purchase_request_model', 'purchase_request_detail_model','purchase_order_model'));
        $this->load->library('Datatable');
        $this->load->helper('notification');
    }

    function test(){
        echo Date('Y-m-d H:i:s');
        $prdetail = $this->purchase_request_detail_model
                            ->select('prdetail_id')
                            ->with('pr')
                            ->where('item_id', 1)
                            ->where('transactionbundle_id', 1)
                            ->get()->result_array();
        // $this->db->trans_start();
        // $model = $this->purchase_request_model;
        // $model->with(array('pr','item'))->where('pr_status',3)->where_in('pr_id',array(1,2,3,4,5))->update();
        //echo $model->stringJoin;
        
        // $model = $this->purchase_request_model;
        // $model->where('transactionbundle_id',1)->update(array('pr_status'=>1), 1);
        // $this->db->trans_complete();
        
        //print_r($this->db->arr_where    );
    }

    public function index() {

        $data['title'] = 'PURCHASE ORDER TICKET';
        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'base/page_content.tpl';     
        $data['content_body'] = $this->class_name.'/page_main.tpl';
        $data['javascript'] = array('po.js');

        //MENAMPILKAN NOTIFIKASI
        /*
        SEMENTARA TIDAK DIPAKAI
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')){
	        $data['notification'] = notification::viewNotification($this->session->userdata('user_name'), $this->session->userdata('kd_roles'));
        }
        */

        //$data['data_source_page_2'] = site_url($this->class_name.'/draft');
        
        $data['table']['table_review']  = $this->table_review(array('wrapper'));
        //$data['table']['table_bundle']  = $this->table_bundle(array('wrapper'));        
        //$data['table']['table_po']      = $this->table_po(array('wrapper'));
        
        $data['st_extend2'] = TRUE;
        $this->twig->display('index.tpl', $data);
    }

    public function review(){
        $data = $this->table_review(array('wrapper'));
        echo $data;
    }

    public function bundle(){
        $data = $this->table_bundle(array('wrapper'));
        echo $data;        
    }

    public function po(){
        $data = $this->table_po(array('wrapper'));
        echo $data;                
    }

    public function table_review($options = array()){
        $this->load->model(array('transaction_bundle_model','purchase_request_model'));
        $table = new Datatable();
        $table->reset();

        if (in_array('wrapper', $options)) {
            $table->isScrollable= false;
            $table->checklist   = true;
            $table->id          = 'table-review';
            $table->caption_btn_group = 'Create Draft PO';
            if($this->access_right->otoritas('add')){
                $table->url_action_group = site_url($this->class_name.'/proses_draft');
            }

            $filter = array(
                array('pr_code', 'NO.PP', 'text'),
                array('nama_cabang', 'Store', 'text'),
                array('pr_type', 'PR Type', 'text'),
                array('pr_status', 'Status', 'text'),
            );


            $table->dataFilter = $filter;


            $table->header     = array('NO.PP', 'STORE', 'TYPE', 'STATUS', 'DATE', 'NOTE');
            $table->source     = site_url($this->class_name.'/table_review');
            $table->callback_group_submit = 
                    "var param = {
                        pr_ids : grid.getSelectedRows(),
                    };
                    var thisPage = '#page-1';
                    blockUI(thisPage);
                    $.post(
                        '".site_url($this->class_name.'/proses_draft')."',
                        param, 
                        function(data){
                            if(data.success){
                                toastr['success'](data.message, 'Success');        
                                if(data.callback)
                                    eval(data.callback);
                            }
                            else
                                toastr['warning'](data.message, 'Error');

                        },'json'
                    ).error(function(xhr, textStatus, error){
                        toastr['error'](error.message, 'Server Error');
                    }).always(function(){            
                        unblockUI(thisPage);
                    });"
                ;
            return $table->generateWrapper();
        } else {
            $table
                ->setModel($this->purchase_request_model)
                ->select("pr_id, pr_code, nama_cabang, pr_type, pr_status, date_created, pr_note")
                ->with(array('document', 'store'))
                ->where('pr_status', prstatus::WAITING_REVIEW) // waiting for review
                ->where('pr_type',itemtype::ITEM_TICKET)
                ->edit_column('pr_id', '<input type="checkbox" name="id[]" value="$1">','pr_id')
                ->edit_column('pr_status', '$1', "transaksi::getPrStatus(pr_status)")
                ->edit_column('pr_type', '$1', "transaksi::getItemType(pr_type)");
            echo $table->generate();
        }

        
    }


    public function table_bundle($options = array()){
        $this->load->model('transaction_bundle_model');
        $table = new Datatable();
        $table->reset();
        if (in_array('wrapper', $options)) {
            $table->numbering   = true;
            $table->isScrollable= false;
            $table->id          = 'table-bundle';
            $table->header      = array('LIST PR', 'DATE', 'STATUS', 'ACTION');
            

            $filter = array(
                array('rec_created', 'PR Date', 'date'),
                array('bundle_status', 'Status', 'text'),
            );


            $table->dataFilter = $filter;

            $table->source      = site_url($this->class_name.'/table_bundle');
            return $table->generateWrapper();
        } else {
            $table = $this->datatable
                    ->setNumbering()
                    ->selectFromIndex()
                    ->setModel($this->transaction_bundle_model)
                    ->select("this.transactionbundle_id, rec_created, bundle_status, this.transactionbundle_id as view")
                    ->with(array('transaction', 'document'))
                    ->group_by('this.transactionbundle_id')
                    ->where('po_type','ticket')
                    //->where('pr_type',4) //----------- type Ticket only
                    ->order_by('this.transaction_id','DESC')
                    ->edit_column('rec_created', '$1', "date::shortDate(rec_created)")
                    ->edit_column('view', '$1', "view::button_view_bundle_sc(view)")
                    ->edit_column('bundle_status', '$1', "transaksi::getBundleStatus(bundle_status)");
            $data = $table->getData();

            foreach ($data as $key => $value) {
                $temp = $this->purchase_request_model
                    ->with('document')
                    ->select('pr_code')
                    ->where('transactionbundle_id', $value['transactionbundle_id'])
                    ->get()->result_array();
                $docs = '';
                foreach ($temp as $tmp) {
                    $docs.= ' <span class="badge badge-success badge-roundless">'.$tmp['pr_code'].'</span> ';
                }
                $data[$key]['transactionbundle_id'] = $docs;
            }
            $table->setData($data);

            echo $table->generate();
        }
    }  

    public function table_po($options = array()){
        //$this->load->model('transaction_bundle_model');
        $this->load->model(array('status_expired_model', 'status_model','purchase_order_model'));
        $getExp = $this->status_expired_model->select('expired')->where('status_id',9)->where('itemtype_id',2)->get()->row();
        $table = new Datatable();
        $table->reset();

        $filter = array(
                array('po_code', 'No PO', 'text'),
                array('nama_supplier', 'Supplier', 'text'),
                array('po_status', 'Status', 'text'),
                array('total', 'Total', 'text'),
            );


        $table->dataFilter = $filter;

        if (in_array('wrapper', $options)) {
            $table->numbering   = true;
            $table->isScrollable= false;
            $table->id          = 'table-po';
            $table->showfilter_id      = 'showfilter_po';
            $table->header      = array('NO.PO','DATE', 'SUPPLIER', 'STATUS','EXP RECEIVE', 'TOTAL','ACTION');
            $table->source      = site_url($this->class_name.'/table_po');
            return $table->generateWrapper();
        } else {
            $table = $this->datatable
                    ->setNumbering()
                    ->selectFromIndex()
                    ->setModel($this->purchase_order_model)
                    ->select("po_code,date_created, nama_supplier, po_status,
                        if(po_status = 1 , DATEDIFF(date_created,now()) + $getExp->expired,this.date_received ) as diff, sum(price*qty_approved) as total, this.po_id as view")
                    ->with(array('detail','transaction','document','supplier'))
                    ->group_by('this.po_id')
                    ->order_by('date_created','DESC')
                    ->edit_column('view', '$1', "view::btn_group_po(view,po_status)")
                    ->edit_column('diff','$1',"view::format_day(diff)")
                    ->edit_column('total', '$1', 'view::format_number(total)')
                    ->edit_column('po_status', '$1', "transaksi::getPoStatus(po_status)");
            echo $table->generate();
        }
    } 

    public function __table_po($options = array()){
        $table = new Datatable();
        $table->reset();

        if (in_array('wrapper', $options)) {
            $table->numbering   = true;
            $table->isScrollable= false;
            $table->id          = 'table-po';
            $table->header      = array('NO.PO', 'SUPPLIER', 'STATUS', 'ITEMS', 'TOTAL');
            $table->source      = site_url($this->class_name.'/table_po');
            return $table->generateWrapper();
        } else {
            $table
                ->setNumbering()
                ->selectFromIndex()
                ->setModel($this->purchase_order_model)
                ->select("po_code, nama_supplier, po_status, sum(qty_approved) as qty, sum(price*qty_approved) as total, this.po_id as view")
                ->with(array('detail','transaction','document','supplier'))
                ->group_by('this.po_id')
                //->edit_column('view', '$1', "view::btn_group_po(view)")
                ->edit_column('total', '$1', 'view::format_number(total)')
                ->edit_column('po_status', '$1', "transaksi::getPoStatus(po_status)");
            echo $table->generate();
        }
    }

    public function proses_confirm($bundle_id = NULL){
        $data   = $this->input->post('data');
        $act    = $this->input->post('act');

        if(!$bundle_id) return; 
        if(!$data) return;

        $this->load->model('purchase_request_detail_model');
        $model  = $this->purchase_request_detail_model;

        $this->db->trans_start();
        foreach ($data as $key => $value) {
            $temp = array();
            $temp['qty_approved']   = $value['qty_approved'];
            $model->update($temp, $value['prdetail_id']);            
        }

        // update purchase request based on act
        $now = date::now_sql();
        switch ($act) {
            case 'revisi':
                $this->purchase_request_model
                    ->set('pr_status', prstatus::REVISI)
                    ->set('date_revisi', $now)
                    ->set('count_revisi','count_revisi+1', FALSE)
                    ->where("transactionbundle_id", $bundle_id)->update();
                break;
            case 'approve':
                $this->purchase_request_model
                    ->set('pr_status', prstatus::APPROVE)
                    ->set('date_confirmed', $now)
                    ->where('transactionbundle_id', $bundle_id)->update();
                // UPDATE status di purchase_request detail
                break;
            
            default:
                # code...
                break;
        }

        if($this->db->trans_status()){
            $message = array(
                        'success' => true,     
                        'message' => 'Save Success',                   
                        'act' => $act,
                    );
            if($act=='approve')
                $message['callback'] = "loadNextPage(2,'".site_url($this->class_name."/supplier/".$bundle_id)."')";
        }else{
            $message = array(
                        'success' => false,                        
                        'message' => 'Database Error Occured',
                    );
        }

        $this->db->trans_complete();

        echo json_encode($message);
    }

    public function draft($bundle_id = NULL){
        
        if(!$bundle_id) return;

        $this->load->model('transaction_bundle_model');
        $bundle = $this->transaction_bundle_model->getById($bundle_id)->row();

        // get all purchase request plus detailnya
        $prdetail = $this->purchase_request_model
            ->with(array('detail','item','store'))
            ->where('this.transactionbundle_id', $bundle_id)
            ->order_by('pr_type','asc')
            ->order_by('detail.item_id', 'asc')
            ->get()->result_array();
        
        $rows = array();
        $rows['machine'] = array();
        $rows['merchandise'] = array();
        $rows['sparepart'] = array();
        $rows['coin'] = array();
        $rows['ticket'] = array();
        $rows['card'] = array();

        foreach ($prdetail as $key => $value) {
            $temp = array();
            $temp['primary_key'] = $value['prdetail_id'];
            $temp['nama_cabang'] = $value['nama_cabang'];
            $temp['item_key'] = $value['item_key'];
            $temp['item_name'] = $value['item_name'];
            $temp['request'] = $value['qty_request'];
            $temp['approve'] = ($bundle->bundle_status == bundlestatus::OPEN ? 
                                    '<input type="text" size="5" name="" prdetail-id='.$value['prdetail_id'].' class="form-control qty-approved" value="'.$value['qty_approved'].'"/>' :
                                    $value['qty_approved']
                                );

            switch ($value['itemtype_id']) {
                case itemtype::ITEM_MACHINETYPE:
                    $rows['machine'][] = $temp;
                    break;

                case itemtype::ITEM_MERCHANDISE:
                    $rows['merchandise'][] = $temp;
                    break;

                case itemtype::ITEM_SPAREPART:
                    $rows['sparepart'][] = $temp;
                    break;
                
                case itemtype::ITEM_COIN:
                    $rows['coin'][] = $temp;
                    break;
                
                case itemtype::ITEM_TICKET:
                    $rows['ticket'][] = $temp;
                    break;
                
                case itemtype::ITEM_CARD:
                    $rows['card'][] = $temp;
                    break;
                
                default:
                    # code...
                    break;
            }
        }

        // INISIALISASI TABLE
        $this->load->library ("custom_table" );
        $table = new custom_table();
        $header [0] = array (
                "STORE",1,1,
                "SKU",1,1,
                "ITEM",1,1,
                "REQUEST",1,1,
                "APPROVE",1,1,
        );
        $table->header = $header;
        $table->style = "table table-advance table-hover";

        // GENERATE TABLE (machine, merchandise, sparepart, dll)
        foreach ($rows as $key => $row) {
            if($row){
                $table->id = 'table-draft-'.$key;
                $table->data = $row;
                $data['list_pr'][$key] = $table->generate();   
            }else{
                unset($rows[$key]);
            }
        }

        $this->load->model(array('transaction_bundle_model'));
        $data['data'] = $this->transaction_bundle_model->getById($bundle_id)->row();
        $data['source']['supplier'] = site_url($this->class_name.'/supplier/'.$bundle_id);
        $data['source']['detail'] = site_url($this->class_name.'/detail/'.$bundle_id);
        $data['url']['action_save'] = site_url($this->class_name.'/proses_confirm/'.$bundle_id);
        $data['bundle'] = $this->transaction_bundle_model->getById($bundle_id)->row();

        $this->twig->display($this->class_name.'/page_draft.tpl',$data);
    }

    public function proses_draft(){
        $id = $this->input->post('id');
        if(!$id){
            /*------------------
             CREATE BUNDLE
            ------------------*/

            // inisialisasi variable
            $post = $this->input->post();
            $pr_ids = $post['pr_ids'];

            // validasi jika tidak ada pr-id yang dikirim
            if(!$pr_ids){
                $message = array(
                        'success' => false,
                        'title' => 'Error',
                        'message' => 'No Purchase Request Choosed',
                    );
                echo json_encode($message);
                return;
            }

            $this->load->model(array('transaction_model','transaction_document_model',
                    'transaction_store_model','transaction_bundle_model'
                ));

            $this->db->trans_start();
            // CREATE TRANSACTION BUNDLE
            $user_id = $this->session->userdata('user_id');
            $store_id= $this->session->userdata('store_id');
            $doc_number = $this->input->post('doc_number');

            // insert to transaction table
            $trans['transactiontype_id'] = transactiontype::BUNDLE;
            $trans['rec_created'] = date::now_sql();
            $trans['rec_user'] = $user_id;
            $this->transaction_model->create($trans);
            $transaction_id = $this->db->insert_id();

            // insert to transaction_store table
            $store['store_id'] = $store_id;
            $store['transaction_id'] = $transaction_id;
            $this->transaction_store_model->create($store);

            // insert to transaction_document table
            $doc['doc_number'] = $doc_number;
            $doc['transaction_id'] = $transaction_id;
            $this->transaction_document_model->create($doc);

            // insert to transaction_bundle table
            $bundle['transaction_id'] = $transaction_id;
            $bundle['bundle_type'] = 1; // bundle purchase Request
            $bundle['po_type'] = 'ticket';
            $this->transaction_bundle_model->create($bundle);
            $bundle_id = $this->db->insert_id();

            $data['data'] = $this->transaction_bundle_model->getById($bundle_id);

            // update all purchase request diset bundle id nya dan status = waiting confirmation
            // semua purchase_request_detail akan otomatis terupdate status = waiting confirmation
            $pr = array();
            $pr['transactionbundle_id'] = $bundle_id;
            $pr['pr_status'] = prstatus::WAITING_CONFIRM;
            $this->purchase_request_model
                ->where_in('this.pr_id', $pr_ids)
                ->update($pr);
            

            /*------------------
                CREATE PURCHASE ORDER
                po yang digabung per supplier
            --------------------*/
            $this->createPurchaseOrder($bundle_id);

            /*------------------
                CREATE PO_STORE
                po yang digabung per supplier per store
            --------------------*/
            $this->createPoStore($bundle_id);

            if($this->db->trans_status() == true){
                $message = array(
                        'success' => true,
                        'title' => 'Proses Berhasil',
                        'message' => 'Penyimpanan Berhasil',
                        'data' => array(
                                'bundle_id' => $bundle_id,
                        ),
                        'callback' => "loadNextPage(1,'".site_url($this->class_name."/detail/".$bundle_id)."')",
                    );
            } else {
                $message = array(
                        'success' => false,
                        'title' => 'Error',
                        'message' => $this->db->_error_message(),
                    );
            }

            $this->db->trans_complete();
            
            echo json_encode($message);
        }
    }

    public function createPurchaseOrder($bundle_id){
        // membuat purchase_order. purchase order yg dikelompokkan berdasarkan supplier
        $this->load->model(array(
                'transaction_model','transaction_store_model','transaction_document_model',
                'purchase_order_detail_model', 'transaction_bundle_model',
                'po_store_model', 'po_store_detail_model',
                'purchase_request_detail_model',
            ));

        $modelPr        = $this->purchase_request_model;
        $modelPrDetail  = $this->purchase_request_detail_model;
        $modelPo        = $this->purchase_order_model;
        $modelPoDetail  = $this->purchase_order_detail_model;
        $modelPoStore   = $this->po_store_model;
        $modelPoStoreDetail = $this->po_store_detail_model;

        $temp = $modelPr->with(array('detail'))
                        ->where('transactionbundle_id', $bundle_id)
                        ->get()->result_array();

        // data purchase request detail dikelompokan berdasarkan supplier_id
        $dataPerSupplier = array();
        foreach ($temp as $value) {
            $dataPerSupplier[$value['supplier_id']][] =  $value;
        }

        // membuat purchase_order dan purchase_order_detail per supplier
        foreach ($dataPerSupplier as $supplier_id => $prdetails) {

            // insert to table purchase order
            $po['bundle_id']        = $bundle_id;
            $po['supplier_id']      = $supplier_id;
            $modelPo->create($po);

            $transaction_id = $modelPo->transaction_id;
            $po_id          = $modelPo->insert_id;

            // mengelompokkan prdetails berdasarkan jenis item
            $podetails = array();
            $prdetailIds = array();
            foreach ($prdetails as $prdetail) {
                $tempId = $prdetail['prdetail_id'];
                $podetails[$tempId]['prdetail_id']  = $prdetail['prdetail_id'];
                $podetails[$tempId]['item_id']      = $prdetail['item_id'];
                $podetails[$tempId]['price']        = $prdetail['price'];
                $podetails[$tempId]['qty_approved'] = $prdetail['qty_approved']; //karena baru dibuat. jadi qty approved = 0
                $podetails[$tempId]['qty_request']  = $prdetail['qty_request'];
                $podetails[$tempId]['po_id']        = $po_id;
                $podetails[$tempId]['transaction_id'] = $transaction_id;
                $podetails[$tempId]['store_id']     = $prdetail['store_id'];

                // menambahkan prdetail_id untuk keperluan update
                $prdetailIds[] = $prdetail['prdetail_id'];
                
            }

            // update purchase request detail . po_id = po_id
            $this->purchase_request_detail_model
                ->where_in('prdetail_id', $prdetailIds)
                ->set('po_id', $po_id)
                ->update();
                
            // insert batch purchase order detail
            $modelPoDetail->insert_batch($podetails);
        
        }

    }

    public function createPoStore($bundle_id){
        // membuat po_store. purchase order dikelompokkan berdasarkan supplier dan masing-masing store

        // inisialisasi
        $this->load->model(array(
                'transaction_model','transaction_store_model','transaction_document_model',
                'purchase_order_detail_model', 'transaction_bundle_model',
                'po_store_model', 'po_store_detail_model',
                'purchase_request_detail_model',
                'history_prbundle_model'
            ));
        $now = date::now_sql();

        $modelPr        = $this->purchase_request_model;
        $modelPrDetail  = $this->purchase_request_detail_model;
        $modelPo        = $this->purchase_order_model;
        $modelPoDetail  = $this->purchase_order_detail_model;
        $modelPoStore   = $this->po_store_model;
        $modelPoStoreDetail = $this->po_store_detail_model;
        $modelBundle    = $this->transaction_bundle_model;



        // update pr_status dan date_confirmed
        $modelPr
            //->set('pr_status', prstatus::APPROVE)
            ->set('date_confirmed', $now)
            ->where('transactionbundle_id', $bundle_id)->update();

        // get semua po - po_detail - item_type dengan bundle id
        $tmp = $modelPo
            ->where('bundle_id', $bundle_id)
            ->with(array('detail','item'))
            ->get()->result_array();

        // pengelompokan berdasarkan supplier. store. itemtype
        $data = array();
        foreach ($tmp as $key => $value) {
            $sup = $value['supplier_id'];
            $typ = $value['itemtype_id'];
            $sto = $value['store_id'];
            $data[$sup]['po_id']      = $value['po_id'];
            $data[$sup]['detail'][$sto][$typ][] = $value;
        }

        foreach ($data as $supplier_id => $tmp1) {
            foreach ((array)$tmp1['detail'] as $store_id => $tmp2) {

                foreach ($tmp2 as $itemtype => $tmp3) {
                    $prdetailIds = array();
                    foreach ($tmp3 as $k => $postore) {
                        $tmp = array();
                        $tmp['supplier_id'] = $supplier_id;   
                        $tmp['store_id']    = $store_id;
                        $tmp['po_type']     = $postore['itemtype_id'];
                        $tmp['bundle_id']   = $postore['bundle_id'];
                        $tmp['po_id']       = $postore['po_id'];
                        $tmp['podetail_id']     = $poStore['podetail_id'];

                        $prdetailIds[] = $postore['prdetail_id'];


                        // [POINT] insert to table po_store
                        $modelPoStore->create($tmp);
                        $tmpId = $modelPoStore->insert_id;

                        // [POINT]  edit postore_id di purchase_order_detail
                        $modelPoDetail
                            /*->where('po_id', $postore['po_id'])
                            ->where('store_id', $store_id)*/
                            ->where('podetail_id',$postore['podetail_id'])
                            ->set('postore_id', $tmpId)->update();

                        // [POINT]  edit postore_id di purchase_request_detail
                        //$modelPrDetail->where_in('prdetail_id', $prdetailIds)
                        $modelPrDetail->where('prdetail_id', $postore['prdetail_id'])
                            ->set('postore_id', $tmpId)->update();
                    }
                }
            }
        }

    }

    /*--------------------------------------
        Menampilkan semua purchase store dengan bundle id
        fungsi : mengatur document id nya masing masing po global
        action : proses_detail
    ----------------------------------------*/
    public function detail($bundle_id = NULL){
        if(!$bundle_id) return;
        $this->load->model(array(
            'purchase_order_model','purchase_order_detail_model',
            'transaction_bundle_model', 'item_model',
            'item_supplier_model', 'item_supplier_default_model','transaction_document_model',
            'po_store_model', 'po_store_detail_model',
            'purchase_request_model', 'purchase_request_detail_model',
        ));

        $modelPo            = $this->purchase_order_model;
        $modelPoDetail      = $this->purchase_order_detail_model;
        $modelPrDetail      = $this->purchase_request_detail_model;
        $modelPoStoreDetail = $this->po_store_detail_model;
        $modelPoStore       = $this->po_store_model;
        $modelBundle        = $this->transaction_bundle_model;


        //get status bundle
        $param['st_bundle'] = $modelBundle
            ->where('transactionbundle_id',$bundle_id)->get()->row()->bundle_status;

        // mendapatkan semua pr_detail, postore, postore_detail dengan parameter bundle_id
        $rows = $modelPoDetail
                    ->select('*, this.price as harga')
                    ->with(array('po', 'store', 'item', 'supplier','item_supplier','item_stok'))
                    ->where('bundle_id', $bundle_id)
                    ->order_by('nama_supplier', 'asc')
                    ->order_by('item_name', 'asc')
                    ->group_by('prdetail_id')
                    ->get()->result_array();


        // pengelompokan berdasarkan supplier
        $listSupplier = array();

        foreach ($rows as $key => $detail) {
            // mendapatkan list supplier untuk tiap masing-masing item
            $item_id = $detail['item_id'];
            if (!$listSupplier[$item_id]){
                $listSupplier[$item_id] = $this->item_supplier_model->options($item_id);
            }
            $htmlOptions = 'podetail-id="'.$detail['podetail_id'].'" ';
            $htmlOptions.= 'id="list-supplier-'.$detail['supplier_id'].'-'.$item_id.'" ';
            $htmlOptions.= 'data-source="'.site_url($this->class_name.'/proses_supplier_change').'" ';
            $htmlOptions.= 'class="form-control list-supplier bs-select col-md-12"';
            $list_supplier      = form_dropdown('data[supplier_id]', $listSupplier[$item_id] , $detail['supplier_id'], $htmlOptions);

            $temp = array();
            $temp['store_name']     = $detail['kd_cabang'].'.'.$detail['nama_cabang'];
            $temp['item_name']      = $detail['item_name'];
            $temp['supplier_name']  = $list_supplier;
            $temp['price']          = '<span class="format-number po-price">'.$detail['price'].'<span>';
            $temp['reg_diskon']     = hgenerator::diskon($detail['reg_diskon']);
            $temp['qty_request']    = hgenerator::number($detail['qty_request']);
            $temp['qty_approved']   = '<input type="text" size="5" name="" podetail-id="'.$detail['podetail_id'].'" prdetail-id='.$detail['prdetail_id'].' pp-item ="'.$detail['item_id'].'" class="form-control format-number qty-approved-sc " value="'.$detail['qty_approved'].'"/>';
            $temp['qty_for_po']   = '<input type="text" size="5" name="" podetail-id="'.$detail['podetail_id'].'" prdetail-id='.$detail['prdetail_id'].' pp-item ="'.$detail['item_id'].'" class="form-control format-number qty-for-po " value="'.$detail['qty_for_po'].'"/>';
            $temp['additional_diskon']   = '<input type="hidden" name"" podetail-id="'.$detail['podetail_id'].'" prdetail-id='.$detail['prdetail_id'].' class="sup-price-pax" value="'.$detail['sup_price_pax'].'"><input type="hidden" name"" podetail-id="'.$detail['podetail_id'].'" prdetail-id='.$detail['prdetail_id'].' class="reg-diskon" value="'.$detail['reg_diskon'].'"><input type="text" size="5" name="" podetail-id="'.$detail['podetail_id'].'" prdetail-id='.$detail['prdetail_id'].' class="form-control format-number additional-diskon-sc " value="'.$detail['additional_diskon'].'"/>';
            $temp['sub_total'] = '<span class="format-number sub-total">'.$detail['sub_total'].'<span>';

            $temp_item = array();
            $temp_item['item_name']     = $detail['item_name'];
            $temp_item['stok_gudang']   = $detail['stok_gudang'];
        //    $temp_item['stok_mutasi']   = $detail['stok_gudang_mutasi'];
        //    $temp_item['stok_total']    = $detail['stok_gudang'] + $detail['stok_gudang_mutasi'];
            $temp_item['total_pp']      = '<div id="total_pp_'.$detail['item_id'].'" class="format-number"></div>';
            $temp_item['total_po']      = '';
            $temp_item['kekurangan_po'] = '';

            /*$temp = array();
            $temp['item_name'] ['store_name']     = $detail['kd_cabang'].'.'.$detail['nama_cabang'];
            $temp['item_name'] ['item_name']      = $detail['item_name'];
            $temp['item_name'] ['supplier_name']  = $list_supplier;
            $temp['item_name'] ['price']          = '<span class="format-number po-price">'.$detail['price'].'<span>';
            $temp['item_name'] ['qty_request']    = $detail['qty_request'];
            $temp['item_name'] ['qty_approved']   = '<input type="text" size="5" name="" podetail-id="'.$detail['podetail_id'].'" prdetail-id='.$detail['prdetail_id'].' class="form-control format-number qty-approved " value="'.$detail['qty_approved'].'"/>';*/
           

            $data[$detail['supplier_id']]['po_id']          = $detail['po_id'];
            $data[$detail['supplier_id']]['po_code']        = $detail['po_code'];
            $data[$detail['supplier_id']]['name']           = $detail['nama_supplier'];
            $data[$detail['supplier_id']]['po_status']      = transaksi::getPoStatus($detail['po_status']);
            $data[$detail['supplier_id']]['detail'][]       = $temp;
            $data[$detail['supplier_id']]['detail_item'][$detail['item_id']]       = $temp_item;

            $this->glob_po_code = $detail['po_code'];
            // menghitung total
            $data[$detail['supplier_id']]['total']+= intval($temp['price']*$temp['qty_approved']);
            //vdebug($data[$detail['supplier_id']]['total']);
        }


        // inisialisasi table
        $this->load->library ("custom_table" );
        

        
        // generate table untuk masing - masing supplier
        $HOStoreId = $this->session->userdata('store_id');
        $count = 0;
        if ($data)
            foreach ($data as $key => $row) {
                if($row['detail']){
                    $table = new custom_table();
                    $table->columnWidth = array(30,20,20,15,15);
                    $table->align = array('left', 'right', 'right', 'right', 'right');
                    $table->headerAlign = $table->align;

                    $table->header[0] = array (
                            "ITEM NAME",1,1,
                            "STOK GUDANG",1,1,
                            "TOTAL PP",1,1,
                            "SUDAH PO",1,1,
                            "KEKURANGAN",1,1,
                    );

                    $table->id           = 'table-po-detail-item'.$key.'_a';
                    $table->data         = $row['detail_item'];
                    $table->wrapperClass = ' table-po-detail-item ';
                    //$table->footer       = $this->twig->render('base/table/footer_total.tpl', array('total'=>view::format_number($row['total'])));
                    
                    /*if (!$row['po_code']) {
                       $data[$key.'_a']['po_code']      = code::getCode('po', $HOStoreId ,$key, $count);
                        $count++;
                    }*/
                    $data[$key]['table_item']        = $table->generateWithWrapper();  

                    //==================================================================
                    $table = new custom_table();
                    $table->columnWidth = array(15,15,15,10,10,10,10,15);
                    $table->align = array('left', 'left', 'left', 'right', 'right', 'right','right', 'right');
                    $table->headerAlign = $table->align;

                    $table->header[0] = array (
                            "STORE",1,1,
                            "ITEM NAME",1,1,
                            "SUPPLIER",1,1,
                            "PRICE",1,1,
                            "DISKON",1,1,
                            "REQUEST",1,1,
                            "APPROVE",1,1,
                            "PO",1,1,
                            "ADDITIONAL DISKON",1,1,
                            "TOTAL",1,1,
                    );

                    $table->id           = 'table-po-detail-'.$key;
                    $table->data         = $row['detail'];
                    //$table->data         = is_numeric($row['detail'])?hgenerator::number($row['detail']):$row['detail'];
                    $table->wrapperClass = ' table-po-detail ';
                    $table->footer       = $this->twig->render('base/table/footer_total.tpl', array('total'=>view::format_number($row['total'])));
                    
                    if (!$row['po_code']) {
                        $data[$key]['po_code']      = code::getCode('po', $HOStoreId ,$key, $count);

                        $count++;
                    }
                    $data[$key]['table']        = $table->generateWithWrapper();  

                    unset($data[$key]['detail']);
                }else{
                    unset($data[$key]);
                }
            }

        $param['page']['history']       = $this->getForm('history', $bundle_id, array('return'));
        $param['list']['postore']       = $data;
        $param['url']['action_save']    = site_url($this->class_name.'/proses_detail/'.$bundle_id.'/revisi');
        $param['url']['generate_po']    = site_url($this->class_name.'/generate_po/'.$bundle_id);

        if($this->input->is_ajax_request()){
            $this->twig->display($this->class_name.'/page_detail.tpl', $param);
        }else{
            $data = $param;
            $data['sidebar'] = $this->access_right->menu();
            $data['content'] = $this->class_name.'/page_detail.tpl';
            $this->twig->display('index.tpl', $data);    
        }

    }




    /*--------------------------------------
        Menampilkan semua purchase store dengan bundle id
        fungsi : mengatur document id nya masing masing po global
        action : proses_detail
    ----------------------------------------*/
    public function view($po_id = NULL){
        //if(!$bundle_id) return;
        $this->load->model(array(
            'purchase_order_model','purchase_order_detail_model',
            'transaction_bundle_model', 'item_model',
            'item_supplier_model', 'item_supplier_default_model','transaction_document_model',
            'po_store_model', 'po_store_detail_model',
            'purchase_request_model', 'purchase_request_detail_model',
        ));

        $modelPo            = $this->purchase_order_model;
        $modelPoDetail      = $this->purchase_order_detail_model;
        $modelPrDetail      = $this->purchase_request_detail_model;
        $modelPoStoreDetail = $this->po_store_detail_model;
        $modelPoStore       = $this->po_store_model;
        $modelBundle        = $this->transaction_bundle_model;


        //get status bundle
        $param['st_bundle'] = $modelBundle
            ->where('transactionbundle_id',$bundle_id)->get()->row()->bundle_status;

        // mendapatkan semua pr_detail, postore, postore_detail dengan parameter bundle_id
        $rows = $modelPoDetail
                    ->select('*, this.price as harga')
                    ->with(array('po', 'store', 'item', 'supplier'))
                    //->where('bundle_id', $bundle_id)
                    ->where('po.po_id', $po_id)
                    ->order_by('nama_supplier', 'asc')
                    ->order_by('item_name', 'asc')
                    ->get()->result_array();


        // pengelompokan berdasarkan supplier
        $listSupplier = array();

        foreach ($rows as $key => $detail) {
            // mendapatkan list supplier untuk tiap masing-masing item
            $item_id = $detail['item_id'];
            if (!$listSupplier[$item_id]){
                $listSupplier[$item_id] = $this->item_supplier_model->options($item_id);
            }
            $htmlOptions = 'podetail-id="'.$detail['podetail_id'].'" ';
            $htmlOptions.= 'id="list-supplier-'.$detail['supplier_id'].'-'.$item_id.'" ';
            $htmlOptions.= 'data-source="'.site_url($this->class_name.'/proses_supplier_change').'" ';
            $htmlOptions.= 'class="form-control list-supplier bs-select col-md-12 readonly" ';
            $list_supplier      = $detail['nama_supplier'];//form_dropdown('data[supplier_id]', $listSupplier[$item_id] , $detail['supplier_id'], $htmlOptions);

            $temp = array();
            $temp['store_name']     = $detail['kd_cabang'].'.'.$detail['nama_cabang'];
            $temp['item_name']      = $detail['item_name'];
            $temp['supplier_name']  = $list_supplier;
            $temp['price']          = '<span class="format-number po-price">'.$detail['price'].'<span>';
            $temp['qty_request']    = $detail['qty_request'];
            $temp['qty_approved']   = '<input type="text" size="5" name="" podetail-id="'.$detail['podetail_id'].'" prdetail-id='.$detail['prdetail_id'].' class="form-control format-number qty-approved po-approved" value="'.$detail['qty_approved'].'" readonly/>';

            $data[$detail['supplier_id']]['po_id']          = $detail['po_id'];
            $data[$detail['supplier_id']]['po_code']        = $detail['po_code'];
            $data[$detail['supplier_id']]['name']           = $detail['nama_supplier'];
            $data[$detail['supplier_id']]['po_status']      = transaksi::getPoStatus($detail['po_status']);
            $data[$detail['supplier_id']]['detail'][]       = $temp;

            $this->glob_po_code = $detail['po_code'];
            // menghitung total
            $data[$detail['supplier_id']]['total']+= intval($temp['price']*$temp['qty_approved']);
            //vdebug($data[$detail['supplier_id']]['total']);
        }


        // inisialisasi table
        $this->load->library ("custom_table" );
        $table = new custom_table();
        $table->columnWidth = array(20,20,20,15,10,15);
        $table->align = array('left', 'left', 'left', 'right', 'right', 'right');
        $table->headerAlign = $table->align;

        $table->header[0] = array (
                "STORE",1,1,
                "ITEM NAME",1,1,
                "SUPPLIER",1,1,
                "PRICE",1,1,
                "REQUEST",1,1,
                "APPROVE",1,1,
        );
        
        // generate table untuk masing - masing supplier
        $HOStoreId = $this->session->userdata('store_id');
        $count = 0;
        if ($data)
            foreach ($data as $key => $row) {
                if($row['detail']){

                    $table->id           = 'table-po-detail-'.$key;
                    $table->data         = $row['detail'];
                    $table->wrapperClass = ' table-po-detail ';
                    $table->footer       = $this->twig->render('base/table/footer_total.tpl', array('total'=>view::format_number($row['total'])));
                    
                    if (!$row['po_code']) {
                        $data[$key]['po_code']      = code::getCode('po', $HOStoreId ,$key, $count);
                        $count++;
                    }
                    $data[$key]['table']        = $table->generateWithWrapper();  

                    unset($data[$key]['detail']);
                }else{
                    unset($data[$key]);
                }
            }

        $param['page']['history']       = $this->getForm('history', $bundle_id, array('return'));
        $param['list']['postore']       = $data;
        $param['url']['action_save']    = site_url($this->class_name.'/proses_detail/'.$bundle_id.'/revisi');
        $param['url']['generate_po']    = site_url($this->class_name.'/generate_po/'.$bundle_id);

        if($this->input->is_ajax_request()){
            $this->twig->display($this->class_name.'/page_view.tpl', $param);
        }else{
            $data = $param;
            $data['sidebar'] = $this->access_right->menu();
            $data['content'] = $this->class_name.'/page_view.tpl';
            $this->twig->display('index.tpl', $data);    
        }

    }


    public function cancel_purchase_order($po_id=NULL){
        $this->db->trans_start();
        $this->load->model(array('purchase_order_model'));
        $this->purchase_order_model
            ->where('po_id', $po_id)
            ->set('po_status', postatus::CANCEL)
            ->update();

        if($this->db->trans_status()){
            $message = array(
                            'success' => true,
                            'message' => 'Cancel PO Success',
                            'callback'=> "loadNewCustomTable('#table-po')",
                        );
        }else{
            $message = array(
                            'success' => false,
                            'message' => 'Database Error',
                            //'callback'=> 
                        );
        }

        $this->db->trans_complete();

        echo json_encode($message);

    }


    public function confirm($act, $id){
        $button_group   = array();
        $button_group[] = view::button_back();
        $button_group[] = view::button_confirm_cancel_po();

        switch ($act) {
            

            case 'send':
                $data['title'] = 'SEND REQUEST';
                $data['status'] = 1;
                break;
            
            case 'revisi':
                $data['title'] = 'REVISI REQUEST';
                $data['status'] = 2;
                break;

            case 'reject':
                $data['title'] = 'REJECT REQUEST';
                $data['status'] = 4;
                break;

            case 'approve':
                $data['title'] = 'APPROVE REQUEST';
                $data['status'] = 5;
                break;

            case 'cancel':
                $data['title'] = 'CANCEL PURCHASE ORDER';
                $data['status'] = 9;
                break;

            default:
                # code...
                break;
        }
        $data['hide_kd']        = true;
        $data['button_group']   = view::render_button_group($button_group);
        $data['form_action']    = view::form($id,$this->class_name.'/confirm_proses');
        $data['form'] = $this->class_name.'/form.tpl';
        $data['po_id'] = $id;
        $this->twig->display('base/page_form.tpl', $data);

    }

    public function copy($po_id = NULL){
        //if(!$bundle_id) return;
        $this->load->model(array(
            'purchase_order_model','purchase_order_detail_model',
            'transaction_bundle_model', 'item_model',
            'item_supplier_model', 'item_supplier_default_model','transaction_document_model',
            'po_store_model', 'po_store_detail_model',
            'purchase_request_model', 'purchase_request_detail_model',
        ));

        $modelPo            = $this->purchase_order_model;
        $modelPoDetail      = $this->purchase_order_detail_model;
        $modelPrDetail      = $this->purchase_request_detail_model;
        $modelPoStoreDetail = $this->po_store_detail_model;
        $modelPoStore       = $this->po_store_model;
        $modelBundle        = $this->transaction_bundle_model;


        //get status bundle
        $param['st_bundle'] = $modelBundle
            ->where('transactionbundle_id',$bundle_id)->get()->row()->bundle_status;

        // mendapatkan semua pr_detail, postore, postore_detail dengan parameter bundle_id
        $rows = $modelPoDetail
                    ->select('*, this.price as harga')
                    ->with(array('po', 'store', 'item', 'supplier'))
                    //->where('bundle_id', $bundle_id)
                    ->where('po.po_id', $po_id)
                    ->order_by('nama_supplier', 'asc')
                    ->order_by('item_name', 'asc')
                    ->get()->result_array();


        // pengelompokan berdasarkan supplier
        $listSupplier = array();

        foreach ($rows as $key => $detail) {
            // mendapatkan list supplier untuk tiap masing-masing item
            $item_id = $detail['item_id'];
            if (!$listSupplier[$item_id]){
                $listSupplier[$item_id] = $this->item_supplier_model->options($item_id);
            }
            $htmlOptions = 'podetail-id="'.$detail['podetail_id'].'" ';
            $htmlOptions.= 'id="list-supplier-'.$detail['supplier_id'].'-'.$item_id.'" ';
            $htmlOptions.= 'data-source="'.site_url($this->class_name.'/proses_supplier_change').'" ';
            $htmlOptions.= 'class="form-control list-supplier bs-select col-md-12 readonly" ';
            $list_supplier      = $detail['nama_supplier'];//form_dropdown('data[supplier_id]', $listSupplier[$item_id] , $detail['supplier_id'], $htmlOptions);

            $temp = array();
            $temp['store_name']     = $detail['kd_cabang'].'.'.$detail['nama_cabang'];
            $temp['item_name']      = $detail['item_name'];
            $temp['supplier_name']  = $list_supplier;
            $temp['price']          = '<span class="format-number po-price">'.$detail['price'].'<span>';
            $temp['qty_request']    = $detail['qty_request'];
            $temp['qty_approved']   = '<input type="text" size="5" name="" podetail-id="'.$detail['podetail_id'].'" prdetail-id='.$detail['prdetail_id'].' class="form-control format-number qty-approved po-approved" value="'.$detail['qty_approved'].'" readonly/>';

            $data[$detail['supplier_id']]['po_id']          = $detail['po_id'];
            $data[$detail['supplier_id']]['po_code']        = $detail['po_code'];
            $data[$detail['supplier_id']]['name']           = $detail['nama_supplier'];
            $data[$detail['supplier_id']]['po_status']      = transaksi::getPoStatus($detail['po_status']);
            $data[$detail['supplier_id']]['detail'][]       = $temp;

            $this->glob_po_code = $detail['po_code'];
            // menghitung total
            $data[$detail['supplier_id']]['total']+= intval($temp['price']*$temp['qty_approved']);
            //vdebug($data[$detail['supplier_id']]['total']);
        }


        // inisialisasi table
        $this->load->library ("custom_table" );
        $table = new custom_table();
        $table->columnWidth = array(20,20,20,15,10,15);
        $table->align = array('left', 'left', 'left', 'right', 'right', 'right');
        $table->headerAlign = $table->align;

        $table->header[0] = array (
                "STORE",1,1,
                "ITEM NAME",1,1,
                "SUPPLIER",1,1,
                "PRICE",1,1,
                "REQUEST",1,1,
                "APPROVE",1,1,
        );
        
        // generate table untuk masing - masing supplier
        $HOStoreId = $this->session->userdata('store_id');
        $count = 0;
        if ($data)
            foreach ($data as $key => $row) {
                if($row['detail']){

                    $table->id           = 'table-po-detail-'.$key;
                    $table->data         = $row['detail'];
                    $table->wrapperClass = ' table-po-detail ';
                    $table->footer       = $this->twig->render('base/table/footer_total.tpl', array('total'=>view::format_number($row['total'])));
                    
                    if (!$row['po_code']) {
                        $data[$key]['po_code']      = code::getCode('po', $HOStoreId ,$key, $count);
                        $count++;
                    }
                    $data[$key]['table']        = $table->generateWithWrapper();  

                    unset($data[$key]['detail']);
                }else{
                    unset($data[$key]);
                }
            }

        $param['page']['history']       = $this->getForm('history', $bundle_id, array('return'));
        $param['list']['postore']       = $data;
        $param['url']['action_save']    = site_url($this->class_name.'/proses_detail/'.$bundle_id.'revisi');
        $param['url']['generate_po']    = site_url($this->class_name.'/generate_po/'.$bundle_id);

        if($this->input->is_ajax_request()){
            $this->twig->display($this->class_name.'/page_copy.tpl', $param);
        }else{
            $data = $param;
            $data['sidebar'] = $this->access_right->menu();
            $data['content'] = $this->class_name.'/page_copy.tpl';
            $this->twig->display('index.tpl', $data);    
        }

    }

    public function proses_supplier_change(){
        // inisialisasi
        $this->db->trans_start();
        $podetail_id        = $this->input->post('podetail_id');
        $supplier_id        = $this->input->post('supplier_id');

        // load model
        $this->load->model(array('po_store_model', 'po_store_detail_model', 
                    'purchase_order_model','purchase_order_detail_model',
                    'purchase_request_detail_model', 'item_supplier_model'));

        $modelPo       = $this->purchase_order_model;
        $modelPoDetail = $this->purchase_order_detail_model;
        $modelPoStore       = $this->po_store_model;
        $modelPoStoreDetail = $this->po_store_detail_model;
        $modelPrDetail      = $this->purchase_request_detail_model;
        $modelItemSupplier  = $this->item_supplier_model;

        // get data dari podetail_id itu
        $poDetail = $modelPoDetail->with(array('po', 'prdetail'))
                            //->select('postore.store_id, postore.po_type, bundle_id')
                            ->getById($podetail_id)->row();

        
                            // print_r($poDetail);
        // berpindah postore atau membuat postore baru jika supplier belum ada
        $po     = $modelPo
                    ->where('bundle_id', $poDetail->bundle_id)
                    ->where('supplier_id', $supplier_id)
                    ->get()->row();

        if(!$po){
            // jika tidak ditemukan. maka create purchase_order baru
            $tmp['bundle_id']   = $poDetail->bundle_id;
            $tmp['po_type']     = $poDetail->po_type;
            $tmp['supplier_id'] = $supplier_id;
            $tmp['user_id']     = $this->session->userdata('user_id');
            $tmp['date_created']= date::now_sql();
            $modelPo->create($tmp);

            // mendapatkan postore yang baru saja dibuat
            $po = $modelPo->getLastInserted()->row();
        }

        if($po){

            // mendapatkan harga item dari supplier
            $itemSupplier = $modelItemSupplier
                                ->select('price')
                                ->where('supplier_id', $supplier_id)
                                ->where('item_id', $poDetail->item_id)
                                ->get()->row();
           
            // update purchase_order_detail
            $modelPoDetail
                ->set('po_id', $po->po_id)
                ->findByPk($poDetail->podetail_id)
                ->update();

            // update pr_detail
            $modelPrDetail
                ->set('supplier_id', $supplier_id)
                ->set('po_id', $po->po_id)
                ->set('price', $itemSupplier->price)
                ->findByPk($poDetail->prdetail_id)
                ->update();
        }

        if($this->db->trans_status()){
            $message = array(
                        'success' => true,     
                        'message' => 'Save Success',
                        'callback'=> "reloadPageLoading()",        
                    );
        }else{
            $message = array(
                        'success' => false,                        
                        'message' => 'Database Error Occured',
                    );
        }
        $this->db->trans_complete();
        echo json_encode($message);

    }

    public function proses_detail($bundle_id = NULL, $type = NULL){

        // data : array of ( postore_id, doc_number)
        $dataPoCode = $this->input->post('po_code');
        $dataQty    = $this->input->post('qty');

        // $bundle_id = 1;
        // $type='revisi';
        // $dataPoCode = array(
        //             array('po_id'=>1, 'po_code'=>'00000000001'),
        //             array('po_id'=>2, 'po_code'=>'00000000002'),
        //         );

        // $dataQty = array(
        //             array('prdetail_id'=>1, 'podetail_id'=>1, 'qty_approved'=>1),
        //             array('prdetail_id'=>2, 'podetail_id'=>2, 'qty_approved'=>2),
        //         );

        if(!($dataPoCode && $dataQty))
            return;

        $this->load->model(array('transaction_document_model', 'po_store_model', 'sequence_model',
                                'purchase_request_detail_model','purchase_order_model', 'purchase_order_detail_model'
                        ));
        $modelPo      = $this->purchase_order_model;
        $modelPoDetail= $this->purchase_order_detail_model;
        $modelPoStore = $this->po_store_model;
        $modelPrDetail= $this->purchase_request_detail_model;

        // inisialisasi
        $this->db->trans_start();
        
        foreach ($dataPoCode as $key => $poCode) {
            // update po_code di postore
            $po = $modelPo->getById($poCode['po_id'])->row();
            if(!$po->po_code){
                $tempCode = code::getCode('po', $this->session->userdata('store_id'), $po->supplier_id);
                $this->sequence_model->raiseSequence('po');
                $modelPo
                    ->set('po_code', $poCode['po_code'])
                    ->findByPk($poCode['po_id'])->update();
            }
        }

        foreach ($dataQty as $key => $prDetail) {
            // update qty approved di purchase_order_detail
            if(!empty($prDetail['qty_approved'])){
                $modelPrDetail
                ->set('qty_approved', hgenerator::switch_number($prDetail['qty_approved']))
                ->findByPk($prDetail['prdetail_id'])->update();
                $modelPoDetail
                ->set('qty_approved', hgenerator::switch_number($prDetail['qty_approved']))
                ->findByPk($prDetail['podetail_id'])->update();
            }
            if(!empty($prDetail['qty_for_po'])){
                $modelPrDetail
                ->set('qty_for_po', hgenerator::switch_number($prDetail['qty_for_po']))
                ->findByPk($prDetail['prdetail_id'])->update();
                $modelPoDetail
                ->set('qty_for_po', hgenerator::switch_number($prDetail['qty_for_po']))
                ->findByPk($prDetail['podetail_id'])->update();
            }
            if(!empty($prDetail['qty_approved_pax'])){
                $modelPrDetail
                ->set('qty_approved_pax', hgenerator::switch_number($prDetail['qty_approved_pax']))
                ->findByPk($prDetail['prdetail_id'])->update();
                $modelPoDetail
                ->set('qty_approved_pax', hgenerator::switch_number($prDetail['qty_approved_pax']))
                ->findByPk($prDetail['podetail_id'])->update();
            }
            if(!empty($prDetail['additional_diskon'])){
                $modelPrDetail
                ->set('additional_diskon', hgenerator::switch_number($prDetail['additional_diskon']))
                ->findByPk($prDetail['prdetail_id'])->update();
                $modelPoDetail
                ->set('additional_diskon', hgenerator::switch_number($prDetail['additional_diskon']))
                ->findByPk($prDetail['podetail_id'])->update();
            }
            if(!empty($prDetail['sup_price_pax'])){
                $modelPrDetail
                ->set('sup_price_pax', ($prDetail['sup_price_pax']))
                ->findByPk($prDetail['prdetail_id'])->update();
                $modelPoDetail
                ->set('sup_price_pax', ($prDetail['sup_price_pax']))
                ->findByPk($prDetail['podetail_id'])->update();
            }
            if(!empty($prDetail['reg_diskon'])){
                $modelPrDetail
                ->set('reg_diskon', ($prDetail['reg_diskon']))
                ->findByPk($prDetail['prdetail_id'])->update();
                $modelPoDetail
                ->set('reg_diskon', ($prDetail['reg_diskon']))
                ->findByPk($prDetail['podetail_id'])->update();
            }

/*            $modelPrDetail
                ->set('qty_approved', hgenerator::switch_number($prDetail['qty_approved']))
                ->set('qty_approved_pax', hgenerator::switch_number($prDetail['qty_approved_pax']))
                ->set('additional_diskon', hgenerator::switch_number($prDetail['additional_diskon']))
                ->findByPk($prDetail['prdetail_id'])->update();

            // update qty approved di purchase_order_detail
            $modelPoDetail
                ->set('qty_approved', hgenerator::switch_number($prDetail['qty_approved']))
                ->set('qty_approved_pax', hgenerator::switch_number($prDetail['qty_approved_pax']))
                ->set('additional_diskon', hgenerator::switch_number($prDetail['additional_diskon']))
                ->findByPk($prDetail['podetail_id'])->update();*/


        }


        // SAVE REVISI
        switch ($type) {
            case 'revisi':
                /*------------------
                    CREATE HISTORY_PRBUNDLE
                    history revisi purchase request
                --------------------*/
                $this->load->model(array('history_model','history_prbundle_model', 'history_prbundle_detail_model', 'notification_model', 'notification_pr_model'));

                //HILANGKAN NOTIFIKASI / UPDATE STATUS NOTIFIKASI dari 0 jadi 1
                $notif_prbundle = $this->purchase_request_model
                        ->select("transactionbundle_id, pr_id")
                        ->where("transactionbundle_id", $bundle_id)
                        ->get()
                        ->result_array();

                foreach ($notif_prbundle as $key => $value) {
                    $this->notification_model
                            ->with(array("notif_pr"))
                            ->where("notif_pr.pr_id", $notif_prbundle[$key]["pr_id"])
                            ->set("this.status", 1)
                            ->update();
                }

                //END HILANGKAN NOTIFIKASI
        
                
                // create history
                $his['rec_user']    = $this->session->userdata('user_id');
                $his['store_id']    = $this->session->userdata('store_id');
                $his['rec_created'] = date::now_sql();
                $his['historytype_id'] = transactiontype::BUNDLE;
                $this->history_model->create($his);
                $history_id = $this->history_model->insert_id;

                // create history prbundle
                $bun['history_id'] = $history_id;
                $bun['bundle_id']  = $bundle_id;
                $this->history_prbundle_model->create($bun);
                $historyprbundle_id = $this->db->insert_id();

                // create history prbundle detail
                $prdetailids = array_values_index($dataQty, 'prdetail_id');
                $prdetails   = $this->purchase_request_detail_model
                                    ->select('item_id, supplier_id, store_id, price, qty_request, qty_for_po, qty_approved, qty_approved_pax, sup_price_pax,reg_diskon,additional_diskon')
                                    ->where_in('prdetail_id', $prdetailids)
                                    ->with('pr')
                                    ->get()->result_array();
                foreach ($prdetails as $key => $value) {
                    $prdetails[$key]['history_id'] = $history_id;
                    $prdetails[$key]['historyprbundle_id'] = $historyprbundle_id;
                    //$prdetails[$key]['total_price'] = $prdetails[$key]['qty_approved_pax']*($prdetails[$key]['sup_price_pax']-$prdetails[$key]['reg_diskon']-$prdetails[$key]['additional_diskon']);

                    $prdetails[$key]['total_price'] = ($prdetails[$key]['qty_approved']*($prdetails[$key]['price'] - ($prdetails[$key]['price'] * ($prdetails[$key]['reg_diskon'] +$prdetails[$key]['additional_diskon'])/100)));
                }
                $this->history_prbundle_detail_model->insert_batch($prdetails);

                break;
            
            default:
                # code...
                break;
        }

        if($this->db->trans_status()){
            $message = array(
                        'success' => true,     
                        'message' => 'Save Success',
                        'callback'=> "loadNewCustomTable('#div-table-history')",        
                    );
        }else{
            $message = array(
                        'success' => false,                        
                        'message' => 'Database Error Occured',
                    );
        }

        $this->db->trans_complete();
        echo json_encode($message);
    }

    public function update_pr_from_historybundle($historyprbundle_id=''){
        $this->db->trans_start();
        $this->load->model(array('history_prbundle_model','history_prbundle_detail_model','purchase_request_model','purchase_request_detail_model','purchase_order_detail_model'));

        if($historyprbundle_id != ''){
            //UPDATE BUNDLE STATUS
            $this->history_prbundle_model
                ->where('historyprbundle_id', $historyprbundle_id)
                ->set('bundle_status',1)
                ->update();


            $hprbundle = $this->history_prbundle_model
                    ->with('detail')
                    ->where('this.historyprbundle_id', $historyprbundle_id)
                    ->get()
                    ->result_array();

            $pr = $this->purchase_request_model
                    ->with('detail')
                    ->where('transactionbundle_id', $hprbundle[0]['bundle_id'])
                    ->get()
                    ->result_array();

            foreach ($pr as $key => $value) {
                $this->purchase_request_detail_model
                    ->where('item_id', $hprbundle[$key]['item_id'])
                    ->set(array('supplier_id' => $hprbundle[$key]['supplier_id'], 'qty_request' => $hprbundle[$key]['qty_request'], 'qty_approved' => $hprbundle[$key]['qty_approved']))
                    ->update();

                $this->purchase_order_model
                    ->with('detail')
                    ->where('item_id', $hprbundle[$key]['item_id'])
                    ->set(array('supplier_id' => $hprbundle[$key]['supplier_id'], 'qty_request' => $hprbundle[$key]['qty_request'], 'qty_approved' => $hprbundle[$key]['qty_approved']))
                    ->update();
            }
            
            $this->generate_po($hprbundle[0]['bundle_id']);
        }


        if($this->db->trans_status()){
            $message = array(
                            'success' => true,
                            'message' => 'Save Success',
                            //'callback'=> 
                        );
        }else{
            $message = array(
                            'success' => false,
                            'message' => 'Database Error',
                            //'callback'=> 
                        );
        }


        $this->db->trans_complete();
        echo json_encode($message);
    }

    public function generate_po($bundle_id){
        $this->db->trans_start();
        //$this->createPurchaseOrder($bundle_id);



        $this->load->model(array('transaction_bundle_model','po_store_model','purchase_request_model','purchase_request_detail_model'));
        // edit status receive di : po, postore, pr, prdetail
        $this->purchase_order_model->where('bundle_id', $bundle_id)->set('po_status', postatus::WAITING_RECEIVE)->update();
        $this->po_store_model->where('bundle_id', $bundle_id)->set('postore_status', postatus::WAITING_RECEIVE)->update();
        $this->purchase_request_model->where('transactionbundle_id', $bundle_id)->set('pr_status', prstatus::APPROVE)->update();
        $this->purchase_request_detail_model
            ->with('pr')
            ->where('transactionbundle_id', $bundle_id)
            ->set('detail_status', prstatus::APPROVE)->update();
        $this->transaction_bundle_model->where('transactionbundle_id',$bundle_id)->set('bundle_status',1)->update();
        if($this->db->trans_status()){
            $message = array(
                            'success' => true,
                            'message' => 'Save Success',
                            //'callback'=> 
                        );
        }else{
            $message = array(
                            'success' => false,
                            'message' => 'Database Error',
                            //'callback'=> 
                        );
        }

        $this->db->trans_complete();

        //echo json_encode($message);
    }

    public function table_history_detail($id){
        echo $this->getForm('table-history-detail', $id);
    }

    public function getForm($type, $id = NULL, $param=array()){
        switch ($type) {
            case 'historydua':
                $this->load->library('Datatable');
                $table = new Datatable();

                if (in_array('wrapper', $param)) {
                    $this->load->model('itemtype_model');
                    $data = $this->itemtype_model->options_filter();
                    $table->innerFilter = array(
                        array('itemtype_id', 'Item Type', 'list', $data),
                        array('item_name', 'Item Name', 'text'),
                    );
                    $table->dataFilter = $table->innerFilter;

                    $table->id          = 'table-stok';
                    $table->isScrollable= false;
                    $table->numbering   = true;
                    $table->header      = array('ITEM', 'TYPE', 'STORE', 'STOK');
                    $table->source      = site_url($this->class_name.'/getform/table-main/');
                    return $table->generateWrapper();
                } else {
                    $table
                        ->setModel($this->item_stok_model)
                        ->setNumbering()
                        ->with(array('store','item'))
                        ->select('item_name, itemtype_id, nama_cabang, stok_total')
                        ->order_by('nama_cabang')
                        ->order_by('itemtype_id')
                        ->order_by('item_name')
                        ->edit_column('itemtype_id','$1','transaksi::getItemType(itemtype_id)');
                    echo $table->generate();
                }
                break;

            case 'history':
                $this->load->model(array('history_prbundle_model'));
                $this->load->library("custom_table");
                $this->load->model('transaction_bundle_model');
                $modelBundle        = $this->transaction_bundle_model;

                //get status bundle
                $st_bundle = $modelBundle
                    ->where('transactionbundle_id',$id)->get()->row()->bundle_status;
                $table = new custom_table();

                $data = $this->history_prbundle_model
                        ->select('historyprbundle_id as primary_key, rec_created,status_name,this.bundle_status')
                        ->with(array('history','status_bundle', 'status'))
                        ->where('bundle_id', $id)
                        ->order_by('rec_created')->get()->result_array();
                foreach ($data as $key => $value) {
                    $data2[$key]['primary_key']  = $value['primary_key']; 
                    $data2[$key]['rec_created']  = date::longDate($value['rec_created']);  
                    $data2[$key]['status_name']  = $value['status_name'];  
                    //$data[$key]['bundle_status'] = ($data[$key]['bundle_status'] == '' ? '' : 'GENERATED');
                    //$data[$key]['status_name'] = $data[$key]['status_name'];
                    if($st_bundle == 0) {
                        $data2[$key]['action']       = view::btn_group_po_history_sc($value['primary_key']);
                    }else{
                        if($data[$key]['bundle_status']==0){
                            $data2[$key]['action']       = view::btn_group_close_po_history($value['primary_key']);
                        }else{
                            $data2[$key]['action']       = view::btn_group_close_po_history_generated($value['primary_key']);
                        }
                    }
                }

                $table->columns = array('DATE', 'STATUS','ACTION');
                $table->id      = 'table-history';
                $table->data_source = site_url($this->class_name.'/getform/history/'.$id);
                $table->data    = $data2;

                if(in_array('return', $param))
                    return $table->generateWithWrapper();
                else
                    echo $table->generateWithWrapper();

                break;
            
            case 'table-history-detail':
                $this->load->model(array('history_prbundle_model', 'history_prbundle_detail_model'));

                $this->load->library("custom_table_history_po");
                $table = new custom_table_history_po();

                $tmp = $this->history_prbundle_detail_model
                    ->select('nama_cabang, item_name, nama_supplier, price,reg_diskon, qty_request, qty_approved,qty_for_po,additional_diskon,total_price, supplier_id')
                    ->where('historyprbundle_id', $id)
                    ->order_by('supplier_id')
                    ->with(array('item','store','supplier'))
                    ->get()->result_array();



                // breakdown per supplier
                $detail = array();
                foreach ($tmp as $key => $value) {
                    $supplier_id = $value['supplier_id'];
                    $detail[$value['supplier_id']]['name'] = $value['nama_supplier'];
                    unset($value['supplier_id']);
                    if(is_int($value))
                        $detail[$supplier_id]['data'][] = hgenerator::number($value);
                    else
                        $detail[$supplier_id]['data'][] = $value;

                    
                }

                // generate parameter
                foreach ($detail as $key => $value) {
                    $table->id      = 'table-history-detail-'.$id.'-'.$key;
                    $table->columns = array('STORE','ITEM NAME', 'SUPPLIER','PRICE','DISKON','QTY REQ','QTY APRV','PO','ADDITIONAL DISKON','TOTAL');
                    $table->data    = $value['data'];
                    $table->align   = array('left','left','left','right','right','right','right','right','right');
                    $table->headerAlign   = array('left','left','left','right','right','right','right','right','right');
                    $detail[$key]['table'] = $table->generateWithWrapperHistoryPO();
                }

                $param['details'] = $detail;
                //$table->columns = array('DATE', 'STATUS', 'ACTION');

                //SET PO DATA
                $this->arr_po_data = $tmp;
                $tmp3 = $this->history_prbundle_model
                        ->select('bundle_id')
                        ->where('historyprbundle_id', $id)
                        ->get()
                        ->result_array();
                //SET BUNDLE ID
                $this->setPoCode($tmp3[0]['bundle_id']);

                
                return $this->twig->render($this->class_name.'/page_history_detail.tpl', $param);

                break;

            case 'table-history-detail-perpo':
                $this->load->model(array('history_prbundle_model', 'history_prbundle_detail_model'));

                $this->load->library("custom_table");
                $table = new custom_table();

                $tmp = $this->history_prbundle_detail_model
                    ->select('nama_cabang, item_name, nama_supplier, price, qty_request, qty_approved, supplier_id')
                    //->where('historyprbundle_id', $id)
                    ->where('po_id',$id)
                    ->order_by('supplier_id')
                    ->with(array('item','store','supplier'))
                    ->get()->result_array();



                // breakdown per supplier
                $detail = array();
                foreach ($tmp as $key => $value) {
                    $supplier_id = $value['supplier_id'];
                    $detail[$value['supplier_id']]['name'] = $value['nama_supplier'];
                    unset($value['supplier_id']);
                    $detail[$supplier_id]['data'][] = $value;

                    
                }

                // generate parameter
                foreach ($detail as $key => $value) {
                    $table->id      = 'table-history-detail-'.$id.'-'.$key;
                    $table->data    = $value['data'];
                    $detail[$key]['table'] = $table->generateWithWrapper();
                }

                $param['details'] = $detail;
                //$table->columns = array('DATE', 'STATUS', 'ACTION');

                //SET PO DATA
                $this->arr_po_data = $tmp;
                $tmp3 = $this->history_prbundle_model
                        ->select('bundle_id')
                        ->where('historyprbundle_id', $id)
                        ->get()
                        ->result_array();
                //SET BUNDLE ID
                $this->setPoCode($tmp3[0]['bundle_id']);

                
                return $this->twig->render($this->class_name.'/page_history_detail.tpl', $param);

                break;

            default:
                # code...
                break;
        }
    }



/*-----------------------------------------------------------------------------
-----------------------------------------------------------------------------*/

    public function proses_detail_backup($bundle_id = NULL, $type = NULL){

        // data : array of ( postore_id, doc_number)
        $dataDoc    = $this->input->post('doc_number');
        $dataQty    = $this->input->post('qty');

        if(!($dataDoc && $dataQty))
            return;

        $this->load->model(array('transaction_document_model', 'po_store_model', 'purchase_request_detail_model'));
        $modelPoStore = $this->po_store_model;
        $modelPrDetail= $this->purchase_request_detail_model;

        // inisialisasi
        $this->db->trans_start();
        
        foreach ($dataDoc as $key => $poStore) {
            // update doc. number di postore
            $modelPoStore
                ->set('doc_number', $poStore['doc_number'])
                ->findByPk($poStore['postore_id'])->update();
        }

        foreach ($dataQty as $key => $prDetail) {
            // update qty approved
            $modelPrDetail
                ->set('qty_approved', $prDetail['qty_approved'])
                ->findByPk($prDetail['prdetail_id'])->update();
        }


        // SAVE REVISI
        switch ($type) {
            case 'revisi':
                /*------------------
                    CREATE HISTORY_PRBUNDLE
                    history revisi purchase request
                --------------------*/
                $this->load->model(array('history_model','history_prbundle_model', 'history_prbundle_detail_model'));
                
                // create history
                $his['rec_user']    = $this->session->userdata('user_id');
                $his['store_id']    = $this->session->userdata('store_id');
                $his['rec_created'] = date::now_sql();
                $his['historytype_id'] = transactiontype::BUNDLE;
                $this->history_model->create($his);
                $history_id = $this->history_model->insert_id;

                // create history prbundle
                $bun['history_id'] = $history_id;
                $bun['bundle_id']  = $bundle_id;
                $this->history_prbundle_model->create($bun);
                $historyprbundle_id = $this->db->insert_id();

                // create history prbundle detail
                $prdetailids = array_values_index($dataQty, 'prdetail_id');
                $prdetails   = $this->purchase_request_detail_model
                                    ->select('item_id, supplier_id, store_id, price, qty_request, qty_approved')
                                    ->where_in('prdetail_id', $prdetailids)
                                    ->with('pr')
                                    ->get()->result_array();
                foreach ($prdetails as $key => $value) {
                    $prdetails[$key]['history_id'] = $history_id;
                    $prdetails[$key]['historyprbundle_id'] = $historyprbundle_id;
                }
                $this->history_prbundle_detail_model->insert_batch($prdetails);

                break;
            
            default:
                # code...
                break;
        }

        if($this->db->trans_status()){
            $message = array(
                        'success' => true,     
                        'message' => 'Save Success',
                        'callback'=> "loadNewCustomTable('#div-table-history')",        
                    );
        }else{
            $message = array(
                        'success' => false,                        
                        'message' => 'Database Error Occured',
                    );
        }

        $this->db->trans_complete();
        echo json_encode($message);
    }
    public function detail_backup($bundle_id = NULL){
        if(!$bundle_id) return;
        $this->load->model(array(
            'transaction_bundle_model', 'item_model',
            'item_supplier_model', 'item_supplier_default_model','transaction_document_model',
            'po_store_model', 'po_store_detail_model',
            'purchase_request_model', 'purchase_request_detail_model',
        ));

        $modelPrDetail = $this->purchase_request_detail_model;
        $modelPoStoreDetail = $this->po_store_detail_model;
        $modelPoStore   = $this->po_store_model;

        // mendapatkan semua pr_detail, postore, postore_detail dengan parameter bundle_id
        $rows = $modelPoStoreDetail
                    ->select('*, prdetail.price as harga')
                    ->with(array('postore', 'prdetail', 'item', 'store', 'supplier'))
                    ->where('bundle_id', $bundle_id)
                    ->order_by('nama_supplier', 'asc')
                    ->order_by('item_name', 'asc')
                    ->get()->result_array();

        // pengelompokan berdasarkan supplier
        $listSupplier = array();
        foreach ($rows as $key => $detail) {
            // mendapatkan list supplier untuk tiap masing-masing item
            $item_id = $detail['item_id'];
            if (!$listSupplier[$item_id]){
                $listSupplier[$item_id] = $this->item_supplier_model->options($item_id);
            }
            $htmlOptions = 'postoredetail-id="'.$detail['postoredetail_id'].'" ';
            $htmlOptions.= 'id="list-supplier-'.$detail['supplier_id'].'-'.$item_id.'" ';
            $htmlOptions.= 'data-source="'.site_url($this->class_name.'/proses_supplier_change').'" ';
            $htmlOptions.= 'class="form-control list-supplier bs-select"';
            $list_supplier      = form_dropdown('data[supplier_id]', $listSupplier[$item_id] , $detail['supplier_id'], $htmlOptions);

            $temp = array();
            $temp['store_name']     = $detail['kd_cabang'].'.'.$detail['nama_cabang'];
            $temp['item_name']      = $detail['item_name'];
            $temp['supplier_name']  = $list_supplier;
            $temp['price']          = '<span class="format-number po-price">'.$detail['harga'].'<span>';
            $temp['qty_request']    = $detail['qty_request'];
            $temp['qty_approved']   = '<input type="text" size="5" name="" prdetail-id='.$detail['prdetail_id'].' class="form-control format-number qty-approved po-approved" value="'.$detail['qty_approved'].'"/>';

            $data[$detail['supplier_id']]['postore_id']     = $detail['postore_id'];
            $data[$detail['supplier_id']]['name']           = $detail['nama_supplier'];
            $data[$detail['supplier_id']]['postore_status'] = transaksi::getPrStatus($detail['postore_status']);
            $data[$detail['supplier_id']]['detail'][]       = $temp;

            // menghitung total
            $data[$detail['supplier_id']]['total']+= intval($temp['price']*$temp['qty_approved']);
            //vdebug($data[$detail['supplier_id']]['total']);
        }

        // inisialisasi table
        $this->load->library ("custom_table" );
        $table = new custom_table();
        
        $table->align = array('left', 'left', 'left', 'right', 'right', 'right');
        $table->headerAlign = $table->align;

        $table->header[0] = array (
                "STORE",1,1,
                "ITEM NAME",1,1,
                "SUPPLIER",1,1,
                "PRICE",1,1,
                "REQUEST",1,1,
                "APPROVE",1,1,
        );
        
        // generate table untuk masing - masing supplier
        $HOStoreId = $this->session->userdata('store_id');
        $count = 0;
        if ($data)
            foreach ($data as $key => $row) {
                if($row['detail']){

                    $table->id           = 'table-po-detail-'.$key;
                    $table->data         = $row['detail'];
                    $table->wrapperClass = ' table-po-detail ';
                    $table->footer       = $this->twig->render('base/table/footer_total.tpl', array('total'=>view::format_number($row['total'])));
                    
                    $data[$key]['po_code']      = code::getCode('po', $HOStoreId ,$key, $count++);
                    $data[$key]['table']        = $table->generateWithWrapper();  

                    unset($data[$key]['detail']);
                }else{
                    unset($data[$key]);
                }
            }

        $param['page']['history']       = $this->getForm('history', $bundle_id, array('return'));
        $param['list']['postore']       = $data;
        $param['url']['action_save']    = site_url($this->class_name.'/proses_detail/'.$bundle_id.'/revisi');
        $param['url']['generate_po']    = site_url($this->class_name.'/generate_po/'.$bundle_id);


        if($this->input->is_ajax_request()){
            $this->twig->display($this->class_name.'/page_detail.tpl', $param);
        }else{
            $data = $param;
            $data['sidebar'] = $this->access_right->menu();
            $data['content'] = $this->class_name.'/page_detail.tpl';
            $this->twig->display('index.tpl', $data);    
        }

    }

    public function proses_supplier_change_backup(){
        // inisialisasi
        $this->db->trans_start();
        $postoredetail_id   = $this->input->post('postoredetail_id');
        $supplier_id        = $this->input->post('supplier_id');

        // load model
        $this->load->model(array('po_store_model', 'po_store_detail_model', 'purchase_request_detail_model', 'item_supplier_model'));
        $modelPoStore       = $this->po_store_model;
        $modelPoStoreDetail = $this->po_store_detail_model;
        $modelPrDetail      = $this->purchase_request_detail_model;
        $modelItemSupplier  = $this->item_supplier_model;

        // get data dari postoredetail_id itu
        $poStoreDetail = $modelPoStoreDetail->with(array('postore', 'prdetail'))
                            //->select('postore.store_id, postore.po_type, bundle_id')
                            ->getById($postoredetail_id)->row();

        
                            // print_r($poStoreDetail);
        // berpindah postore atau membuat postore baru jika supplier belum ada
        $poStore = $modelPoStore
                    ->where('bundle_id', $poStoreDetail->bundle_id)
                    ->where('supplier_id', $supplier_id)
                    ->where('store_id', $poStoreDetail->store_id)
                    ->where('po_type', $poStoreDetail->po_type)
                    ->get()->row();

        if(!$poStore){
            // jika tidak ditemukan. maka create postore dulu
            $tmp['store_id']    = $poStoreDetail->store_id;
            $tmp['bundle_id']   = $poStoreDetail->bundle_id;
            $tmp['po_type']     = $poStoreDetail->po_type;
            $tmp['supplier_id'] = $supplier_id;
            $tmp['postore_status'] = 0;
            $modelPoStore->create($tmp);

            // mendapatkan postore yang baru saja dibuat
            $poStore = $modelPoStore->getLastInserted()->row();
        }

        if($poStore){

            // mendapatkan harga item dari supplier
            $itemSupplier = $modelItemSupplier
                                ->select('price')
                                ->where('supplier_id', $supplier_id)
                                ->where('item_id', $poStoreDetail->item_id)
                                ->get()->row();
           
            // update postore_detail
            $modelPoStoreDetail
                ->set('postore_id', $poStore->postore_id)
                ->findByPk($postoredetail_id)
                ->update();

            // update prdetail
            $modelPrDetail
                ->set('supplier_id', $supplier_id)
                ->set('postore_id', $poStore->postore_id)
                ->set('price', $itemSupplier->price)
                ->findByPk($poStoreDetail->prdetail_id)
                ->update();
        }

        if($this->db->trans_status()){
            $message = array(
                        'success' => true,     
                        'message' => 'Save Success',
                        'callback'=> "reloadPageLoading()",        
                    );
        }else{
            $message = array(
                        'success' => false,                        
                        'message' => 'Database Error Occured',
                    );
        }
        $this->db->trans_complete();
        echo json_encode($message);

    }

    public function createPoStoreDetail($bundle_id){
        // membuat po_store. purchase order dikelompokkan berdasarkan supplier dan masing-masing store

        // inisialisasi
        $this->load->model(array(
                'transaction_model','transaction_store_model','transaction_document_model',
                'purchase_order_detail_model', 'transaction_bundle_model',
                'po_store_model', 'po_store_detail_model',
                'purchase_request_detail_model',
                'history_prbundle_model'
            ));
        $now = date::now_sql();

        $modelPr        = $this->purchase_request_model;
        $modelPrDetail  = $this->purchase_request_detail_model;
        $modelPo        = $this->purchase_order_model;
        $modelPoDetail  = $this->purchase_order_detail_model;
        $modelPoStore   = $this->po_store_model;
        $modelPoStoreDetail = $this->po_store_detail_model;

        // update pr_status dan date_confirmed
        $modelPr
            ->set('pr_status', prstatus::APPROVE)
            ->set('date_confirmed', $now)
            ->where('transactionbundle_id', $bundle_id)->update();

        // mendapatkan purchase request detail dengan parameter bundle_id
        $prDetails = $modelPrDetail->with(array('supplier_id', 'bundle', 'pr'))
                    ->where('transactionbundle_id', $bundle_id)
                    ->get()->result_array();

        // mengelompokkan purchase request pdetail berdasarkan supplier_id -- per store -- per item_type
        foreach ($prDetails as $prdetail) {
            $prDetailPo[$prdetail['supplier_id']][$prdetail['store_id']][$prdetail['pr_type']][] = $prdetail;
            //$supplierIds[$prdetail['supplier_id']] = $prdetail['supplier_id'];
        }
        
        // insert to postore table per supplier -- store -- item_type
        foreach ($prDetailPo as $supplier_id => $prDetailStore) {
            foreach ($prDetailStore as $store_id => $prDetailType) {
                foreach ($prDetailType as $prtype => $items) {

                    // insert to table po_store
                    $postore = array();
                    $postore['store_id']    = $store_id;
                    $postore['po_type']     = $prtype;
                    $postore['supplier_id'] = $supplier_id;
                    $postore['postore_status'] = 0;
                    $postore['bundle_id']   = $bundle_id;
                    $modelPoStore->create($postore);
                    $postore_id = $modelPoStore->insert_id;

                    // insert to table po_store_detail
                    $postoredetail = array();
                    $prdetailIds = array();
                    foreach ($items as $item) {
                        $temp = array();
                        $temp['postore_id'] = $postore_id;
                        $temp['prdetail_id'] = $item['prdetail_id'];
                        $postoredetail[] = $temp;

                        // menambahkan prdetail_id untuk keperluan update postore_Id
                        $prdetailIds[] = $item['prdetail_id'];
                    }
                    $modelPoStoreDetail->insert_batch($postoredetail);
                    // update postore_id di purchase request detail
                    $modelPrDetail->set('postore_id', $postore_id)->where_in('prdetail_id', $prdetailIds)->update();
                    // update postore_id di purchase_order_detail 
                    $modelPoDetail->set('postore_id', $postore_id)->where_in('prdetail_id', $prdetailIds)->update();

                }
            } 
        }
    }

    public function createTempFile($id){
        $this->load->model(array('history_prbundle_model','history_prbundle_detail_model', 'item_model', 'store_model', 'supplier_model'));

        $tmp = $this->history_prbundle_detail_model
            ->select('nama_cabang, item_name, nama_supplier, price, qty_request, qty_approved')
            ->where('historyprbundle_id', $id)
            ->order_by('supplier_id')
            ->with(array('item','store','supplier'))
            ->get()->result_array();    

        $myfile = fopen("newfile.txt", "w") or die("Unable to open file!");

        foreach ($tmp as $key => $value) {
            $txt = $tmp[$key]['name_cabang'].";".$tmp[$key]['item_name'].";".$tmp[$key]['nama_supplier'].";".$tmp[$key]['price'].";".$tmp[$key]['qty_request'].";".$tmp[$key]['qty_approved']."\n";
            fwrite($myfile, $txt);
        }

/*        while($rows=mysql_fetch_array($eksekusiquery)){
            $txt = $rows["id_transaksi"].";".$rows["merkbaju"].";".$rows["nomor_resi"].";".$rows["status"].";".$rows["alamat_tujuan"].";".$rows["jumlah"].";".$rows["harga"].";".$rows["total"]."\n";
            fwrite($myfile, $txt);                
        }
*/        
        fclose($myfile);    

    }

    public function cetakHistory(){
        $this->load->library('Datapdf');

        $pdf = new Datapdf();
        $pdf->title = "Purchase Order";
        $pdf->Header("Purchase Order");
        $pdf->AddPage();
        $pdf->setKop(array('JENIS TRANSAKSI'=>'','TANGGAL'=>'','ACCESSOR'=>'','NOMORHP'=>'','ALAMAT'=>''));

        // Column headings
        $header = array('Store', 'Item', "Supplier", 'Price','Qty Req','Qty Appoved','Supplier');

        $id_user='aaa';
        $bulanke = '1';
        $tahunke = '2015';
        // Data loading
        //$pdf->createFile($id_user,$bulanke,$tahunke);
        $this->createTempFile(126);
        $data = $pdf->getData();
        //$data = $pdf->LoadData('newfile.txt');
        //$pdf->SetFont('Arial','B',16);

        //$pdf->ImprovedTable($header,$data, $id_user, $bulanke, $tahunke);

        $pdf->generatePDF($header, $data, $id_user, $bulanke, $tahunke);
        //$pdf->AddPage();
        $pdf->Output();
    }


    function print_pdf($id='',$po=''){
        $this->load->helper('mpdf');
        $html='';
        
        $res_arr_po = $this->getArrPoByBundleId($id);
        $length = count($res_arr_po['arr_po_code']);
        $count = 0;
        foreach ($res_arr_po['arr_po_code'] as $key => $value) {

            $header = array('STORE','ITEM NAME','QTY','TOTAL');
            $data = $this->getDataByBundleAndPo($res_arr_po['bundle_id'],$res_arr_po['arr_po_code'][$key]);
            
            $html .= $this->createHeaderReport($res_arr_po['arr_po_code'][$key]);
            $html .= $this->createMainTable($header, $data);
            $html .= $this->createFooterReport();
            
            if($count<$length-1)
                $html .= '<pagebreak />';
            
            $count++;
        }
        //echo $html;
        //print_r($html);
        //print_r($this->arr_po_data);
        to_pdf($html,'zone 2000');
    }

    function print_pdf_po($id=''){
        $this->load->helper('mpdf');
        $html='';
        
        $this->load->model('purchase_order_model');
        $PoCode = $this->purchase_order_model->select('po_code')->where('po_id',$id)->get()->row()->po_code;
        $res_arr_po = $this->getArrPoByBundleId($id);
        $length = count($res_arr_po['arr_po_code']);
        $count = 0;
        //foreach ($res_arr_po['arr_po_code'] as $key => $value) {

            $header = array('Store Name','Item Name','Price', 'Qty','Total');
            $data = $this->getDataByPo($PoCode);
            
            $html .= $this->createHeaderReport($PoCode);
            $html .= $this->createMainTable($header, $data);
            $html .= $this->createFooterReport();
            
            /*if($count<$length-1)
                $html .= '<pagebreak />';
            
            $count++;*/
        //}
        //echo $html;
        //print_r($html);
        //print_r($this->arr_po_data);
        to_pdf($html,'zone 2000');
    }

    function createHeaderReport($po_code=''){
        $hleft='<table border="0" width = "100%"><tr><td colspan="2"><b>Zone2000</b></td></tr>
                <tr><td colspan="2">Jl. KH Wahid Hasyim no 220 kav A-B <br>Jakarta, Indonesia <br> Telp (021) 3924061</td></tr>
                
                <tr><td colspan="2"><b>Supplier<b></td></tr>
                <tr><td>Nama </td><td>'.$this->glob_nama_supplier.'</td></tr>
                <tr><td>Alamat </td><td>'.$this->glob_alamat_supplier.'</td></tr>
                </table>';

        $hright='<table border="0" width = "100%"><tr><td colspan="2"><b>Purchase Order<b></td></tr>
                <tr><td>Date </td><td>'.$this->date_po.'</td></tr>
                <tr><td>No. PO</td><td>'.$po_code.'</td></tr>
                </table>';

        $tab ='<table width = "100%"><tr><td width = "50%">'.$hleft.'</td><td width = "50%">'.$hright.'</td></tr></table>';
        $result_tab = $tab;

        return $result_tab;
    }

    function createMainTable($header=array(),$data=array()){

        $br = '<br><br><br><br><br><br><br><br><br>';
        $headerlength = count($header);
        $count = 0;

        $res_header .='<tr>';
        
        foreach ($header as $key => $value) {
            $res_header .= '<td style="font-weight:bold;">'.$header[$key].'</td>';
        }
        $res_header .='</tr>';


        $trbuka ='<tr>';
        $trtutup ='</tr>';

        $totalharga = 0;

        $tab_content;
        foreach ($data as $key => $value) {
            $count=0;
            $tab_content .= '<tr>';
            foreach ($data[$key] as $k => $v) {
                if(($k !='supplier_id')&&($k !='nama_supplier')&&($k!='qty_request')&&($k!='alamat_supplier')&&($k!='price')){
                    if($k=='price'||$k=='sup_price_pax'||$k=='reg_diskon'||$k=='additional_diskon'||$k=='totalharga'){
                        $tab_content .= '<td>'.'Rp '.hgenerator::rupiah($data[$key][$k]).',-</td>';
                        //$price = $data[$key][$k];
                    }else{
                        $tab_content .= '<td>'.$data[$key][$k].'</td>';
                    }

                    if($k=='qty_for_po'){
                        $qty_for_po = $data[$key][$k];
                    }
                    if($k=='price'){
                        $price = $data[$key][$k];
                    }
                    /*if($k=='reg_diskon'){
                        $reg_diskon = $data[$key][$k];
                    }
                    if($k=='additional_diskon'){
                        $additional_diskon = $data[$key][$k];
                    }*/

                    /*if($count==$headerlength-1){
                        $tab_content .= '</tr>';
                    }*/

                    /*if($k=='price'){
                        $totalharga = $totalharga + $data[$key][$k];
                    }*/
                    $count++;                        
                    
                }

            }
            $total = $qty_for_po * $price;
            $totalharga += $total;
            //$tab_content .= '<td>Rp '.hgenerator::rupiah($total).'</td>';
            $tab_content .= '</tr>';
        }

        $trjumlah ='<tr><td colspan=2><b>Total</b></td><td colspan=2><b>Rp '.hgenerator::rupiah($totalharga).',-</b></td></tr>';


        $all_table .= '<table width ="100%">'.$res_header.$tab_content.$trjumlah;
        $all_table .= '</table>';
        return $all_table;
    }

    function createFooterReport(){
        $hleft='<table border=0><tr><td><b>Prepared By</b></td></tr>
                </table>';

        $hright='<table><tr><td><b>Approved By<b></td></tr></table>';
        $br = '<br><br><br><br><br><br><br><br><br>';
        $hr = '<hr>';
        $tab ='<table style="width:900px; position:absolute; margin-bottom:0px;"><tr><td style="width:400px;">'.$hleft.$br.$hr.'</td><td style="width:400px;">'.$hright.$br.$hr.'</td></tr></table>';
        $result_tab = $tab;

        return $result_tab;        
    }

    public function setPoCode($bundle_id=''){
        $this->setDatePO($bundle_id);
        $temp=$this->purchase_order_model
                ->where('bundle_id',$bundle_id)
                ->with('supplier')
                ->get()
                ->result_array();
        foreach ($temp as $key => $value){
            foreach($temp[$key] as $k => $v){
                if($k=='po_code'){
                    $this->attr_po .= $temp[$key][$k].'<br>';
                    array_push($this->arr_po_code, $temp[$key][$k]);
                }else if($k=='nama_supplier'){
                    $this->glob_nama_supplier = $temp[$key][$k];
                }else if($k=='alamat_supplier'){
                    $this->glob_alamat_supplier = $temp[$key][$k];
                }                
            }
        }
    }    

    public function setDatePO($bundle_id=''){
        $this->load->model(array('history_prbundle_model'));
        $this->load->model('transaction_bundle_model');
        $modelBundle        = $this->transaction_bundle_model;

        //get status bundle
        $st_bundle = $modelBundle
            ->where('transactionbundle_id',$bundle_id)->get()->row()->bundle_status;
        
        $data = $this->history_prbundle_model
                ->select('historyprbundle_id as primary_key, rec_created,status_name,this.bundle_status')
                ->with(array('history','status_bundle', 'status'))
                ->where('bundle_id', $bundle_id)
                ->order_by('rec_created')->get()->result_array();
        foreach ($data as $key => $value) {
            $data2[$key]['primary_key']  = $value['primary_key']; 
            $data2[$key]['rec_created']  = date::longDate($value['rec_created']);
            if($st_bundle == 0) {
                $data2[$key]['action']       = view::btn_group_po_history($value['primary_key']);
            }else{
                if($data[$key]['bundle_status']==0){
                    $data2[$key]['action']       = view::btn_group_close_po_history($value['primary_key']);
                }else{
                    $data2[$key]['action']       = view::btn_group_close_po_history_generated($value['primary_key']);
                    $this->date_po = $data2[$key]['rec_created'];
                }
            }
        }  
   }
   
   public function getArrPoByBundleId($id=''){
        $this->load->model(array('history_prbundle_model', 'history_prbundle_detail_model'));

        $this->load->library("custom_table");
        $table = new custom_table();

        $tmp = $this->history_prbundle_detail_model
            ->select('nama_cabang, item_name, nama_supplier, price, qty_request, qty_approved, supplier_id')
            ->where('historyprbundle_id', $id)
            ->order_by('supplier_id')
            ->with(array('item','store','supplier'))
            ->get()->result_array();


        //SET PO DATA
        $this->arr_po_data = $tmp;
        $tmp3 = $this->history_prbundle_model
                ->select('bundle_id')
                ->where('historyprbundle_id', $id)
                ->get()
                ->result_array();

        //SET BUNDLE ID set arr_po_code
        $this->setPoCode($tmp3[0]['bundle_id']);

        $result['arr_po_code']=$this->arr_po_code;
        $result['bundle_id']=$id;
        return $result;
   }

   function getDataByBundleAndPo($id='', $po_code=''){

        $this->load->model(array('history_prbundle_model', 'history_prbundle_detail_model')); 
        $tmp = $this->history_prbundle_model
            ->select('nama_cabang, item_name, nama_supplier, po_detail.price, po_detail.qty_for_po,po_detail.qty_for_po * price as totalharga')
            //->select('*')
            ->where(array('this.historyprbundle_id'=> $id, 'po.po_code' => $po_code))
            ->where('po_detail.qty_for_po > 0',NULL)
            //->group_by('item_name')
            ->with(array('po','supplier','po_detail','item_detail','cabang'))
            ->get()->result_array();

        return $tmp;
   }

   function getDataByPo($po_code=''){

        $this->load->model(array('history_prbundle_model', 'history_prbundle_detail_model')); 
        $tmp = $this->history_prbundle_model
            ->select('nama_cabang, item_name, nama_supplier, po_detail.price, po_detail.qty_approved, alamat_supplier')
            //->select('*')
            ->where(array('po.po_code' => $po_code))
            //->group_by('item_name')
            ->with(array('po','supplier','po_detail','item_detail','cabang'))
            ->get()->result_array();

        return $tmp;
   }


   function collectPoFromPoCode($po_code = NULL){
        $this->load->model(array('purchase_order_model', 'purchase_order_detail_model'));
        $this->db->trans_start();
        $result = $this->purchase_order_model->select('*')
                ->with(array('detail'))
                ->where('po_code',$po_code)
                ->get()
                ->result_array();
        

        $HOStoreId = $this->session->userdata('store_id');

        $count=0;

        $po_codedua = '';

        foreach ($result as $key => $value) {
            if(count($result)==1){
                $key=1;
            }
            $po_codedua = code::getCodeAndRaise('po', $HOStoreId ,$key, $count);
            $count++;
        }

        $result2 = $this->purchase_order_model->select('*')
                ->where('po_code',$po_code)
                ->get()
                ->result_array();

        $data['transaction_id'] = $result2[0]['transaction_id'];
        $temp_trans_id = $result2[0]['transaction_id'];

        $data['bundle_id'] = $result2[0]['bundle_id'];        
        $data['supplier_id'] = $result2[0]['supplier_id'];
        $data['po_status'] = 1;
        $data['po_type'] = $result2[0]['po_type'];
        $data['user_id'] = $result2[0]['user_id'];
        $data['date_created'] = date::now_sql();
        $data['po_code'] = $po_codedua;


        //print_r($data);
        $this->purchase_order_model->create($data);

        $data_detail = array();
        foreach ($result as $k => $v) {
            $data2['po_id'] = $this->db->insert_id();
            $data2['postore_id'] = $result[$k]['postore_id'];
            $data2['transaction_id'] = $result[$k]['transaction_id'];
            $data2['prdetail_id'] = $result[$k]['prdetail_id'];
            $data2['item_id'] = $result[$k]['item_id'];
            $data2['price'] = $result[$k]['price'];
            $data2['qty_request'] = $result[$k]['qty_request'];
            $data2['qty_approved'] = $result[$k]['qty_approved'];
            $data2['date_received'] = $result[$k]['date_received'];
            $data2['store_id'] = $result[$k]['store_id'];
            $data2['qty_received'] = $result[$k]['qty_received'];
            $data2['dc_qty_received'] = $result[$k]['dc_qty_received'];
            $data2['dc_date_received'] = $result[$k]['dc_date_received'];
            array_push($data_detail, $data2);
        }

        //print_r($data_detail);
        foreach ($data_detail as $k => $v) {
            $this->purchase_order_detail_model
                ->create($data_detail[$k]);
        }

        if($this->db->trans_status()){
            $message = array(
                            'success' => true,
                            'message' => 'Save Success',
                            //'callback'=> 
                        );
        }else{
            $message = array(
                            'success' => false,
                            'message' => 'Database Error',
                            //'callback'=> 
                        );
        }

        $this->db->trans_complete();


   }

}
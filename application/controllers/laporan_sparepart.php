<?php

class laporan_sparepart extends MY_Controller {

    public function __construct() {
        // Declaration
        parent::__construct();
        $this->load->model('laporan_sparepart_model');
    }

    public function index() {

        $data['title'] = 'Laporan Penggunaan Sparepart';        

        $data['button_group'] = '';// $this->getAvailableButtons();
        $data['button_right'] = $this->getPrintTools();

        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'base/page_content.tpl'; 
        $data['nama_pegawai'] = $this->currentUsername;    
        
        $data['table']['main'] = $this->table_main(array('wrapper', 'filter'));
        $this->twig->display('index.tpl', $data);

    }

    public function table_main($option = array(),$st_print = FALSE){
        $this->load->library('Datatable');
        $table = $this->datatable;
       
            //->edit_column('id_supplier', '$1', 'view::btn_group_edit_delete_page(id_supplier)');

        if (in_array('filter', $option)) {
            $table->dataFilter = array(
                array('serial_number', 'Kode Machine', 'text'),
                array('doc_number', 'Service No', 'text'),
                array('nama_cabang', 'Store', 'text'),
                array('nama_jenis_mesin', 'Nama Mesin', 'text'),
                array('nama', 'Status Service', 'text'),
                array('issue_description', 'Issue', 'text'),
                array('loc_name','Location','text'),

                //array('this.hadiahtype_id', 'Type', 'list', $this->hadiah_type_model->options()),
            );
        }



        if (in_array('wrapper', $option)) {
            $table->id         = 'table-supplier';
            $table->numbering  = true;
            $table->header     = array('ITEM','GUDANG','GUDANG MUTASI');
            $table->source     = site_url($this->class_name.'/table_main');
            return $table->generateWrapper();
        } else 
            $table  
                ->setModel($this->laporan_sparepart_model)
                ->setNumbering()
                ->with(array('item'))
                ->select('item_name,sum(qty),sum(qty_mutasi)')
                ->group_by('this.item_id');
            if($st_print == TRUE)
                return $table->getData();
            else
                echo $table->generate();

    }

  
   

    public function getCode($machine_id = NULL){
        if (!$machine_id)
            return;
        $storeId = $this->laporan_sparepart_model->getStoreId($machine_id);
        echo code::getCodeAndRaise('sr', $storeId);
    }

    public function getPrintTools(){
        $button_group = array();
        if($this->access_right->otoritas('print')){
            $button_group[] = view::button_export2($this->class_name.'/excel');
        }
        
        if(true){//export
            //$button_group[] = view::button_export();
        }
        return view::render_button_group_laporan_payout($button_group, array(), true);

    }

    public function excel($store_id=4){

        $this->load->model(array('store_model','laporan_payout_model'));        
        $header = array('ITEM','GUDANG','GUDANG MUTASI');

        $data['header'] = $header;
        //----------------------------------------------------
        $date = date('Ymd His');
        $data['judul_kecil']    = 'Laporan Penggunaan Sparepart';
        $data['filename']       = 'Laporan Penggunaan Sparepart('.$date.')';
        $data['content']        = $this->table_main(array(),TRUE);//$this->laporan_payout_model->data_detail(array("id_cabang" => $store_id));
        $data['selected_store'] = $this->store_model
                                 ->select('nama_cabang')
                                 ->where(array("id_cabang"=>hprotection::getSC()))
                                 ->get()
                                 ->row();
        //print_r($data['selected_store']);
        $this->load->view('cetak_excel',$data);
    
    }


}

/* End of file bbm.php */
/* Location: ./application/controllers/bbm.php */

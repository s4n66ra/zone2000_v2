<?php

/**
 *
 *
 * @author ROFID RAHMADI
 */
class status extends MY_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model(array('status_service_model', 'status_repair_model', 'status_pr_model', 'status_po_model'));
    }

    public function index() {

        $data['button_group'] = $this->getAvailableButtons();
        $data['button_right'] = $this->getTools();
        $data['title'] = 'STATUS';
        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'base/page_content.tpl'; 
        
        $this->load->library('Datatable');
        $table = $this->datatable;
        $data['table']['main'] = $this->table_main(array('wrapper','filter'));

        $this->twig->display('index.tpl', $data);

    }

    public function table_main($option = array()){
        $this->load->library('Datatable');
        $table = $this->datatable;
        if (in_array('filter', $option)) {
            $this->load->model('transactiontype_model');
            $listOptions = $this->transactiontype_model->options_empty_name();
            $table->dataFilter = array(
                                    array('transactiontype_name', 'Transaction Type', 'list', $listOptions),
                                );
        }

        if (in_array('wrapper', $option)) {
            $table->numbering  = true;
            $table->id         = 'table-status';
            $table->header     = array('TYPE', 'STATUS CODE', 'STATUS NAME', 'ACTION');
            $table->source     = site_url($this->class_name.'/table_main');
            return $table->generateWrapper();
        } else {
            $table
                ->setModel($this->status_model)
                ->setNumbering()
                ->with(array('type'))
                ->order_by('status_type', 'asc')
                //->order_by('status_id', 'asc')
                ->select('transactiontype_name, status_code, status_name, status_id')
                ->edit_column('status_id', '$1', "view::btn_group_edit_delete(status_id)");
            echo $table->generate();
        }
    }

    public function form($type='add', $id=NULL) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            
            if ($id) {
                $data['title'] = 'Edit Status';
                $row = $this->status_model
                            ->withAll()
                            ->getById($id)->row();
                $data['data'] = $row;
                
                switch ($row->status_type) {
                    case 15:
                        // PR
                        $data['status_number'] = $row->statuspr_id;
                        break;
                    case 17:
                        // PO
                        $data['status_number'] = $row->statuspo_id;
                        break;
                    case 10:
                        // SERVICE REQUEST
                        $data['status_number'] = $row->statusservice_id;
                        break;
                    case 11:
                        // REPAIR JOB
                        $data['status_number'] = $row->statusrepair_id;
                        break;
                    default:
                        # code...
                        break;
                }
            }

            
            $button_group   = array();
            $button_group[] = view::button_back();

            switch ($type) {

                case 'add' :
                    $data['title'] = 'New Status';
                    $data['form_action']    = view::form_input($id);
                    $button_group[]         = view::button_save();
                    break;

                case 'edit' :
                    $data['title'] = 'Edit Status';
                    $data['form_action']    = view::form_input($id);
                    $button_group[]         = view::button_save();
                    break;

                case 'delete':
                    $data['title']          = 'Delete Status';
                    $data['form_action']    = view::form_delete($id);
                    $data['readonly']       = 'readonly=""';
                    $button_group[]         = view::button_delete_confirm();
                    break;
                
                default:
                    # code...
                    break;
            }

            $this->load->model('transactiontype_model');
            $data['list']['status_type'] = form_dropdown(
                                            'data[status_type]', 
                                            $this->transactiontype_model->options_empty(), 
                                            $row->status_type, 
                                            'class="form-control select2" data-placeholder="Select Type"'
                                        );
            $data['form']       = $this->class_name.'/form.tpl';
            $data['button_group'] = view::render_button_group($button_group);
            $this->twig->display('base/page_form.tpl', $data);

        } else {
            $this->access_right->redirect();
        }
    }

    public function add(){
        $this->form('add');
    }

    public function edit($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('edit', true);
        $this->form('edit', $id);
    }

    public function delete($id_enc) {        
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('delete', true);
        $this->form('delete', $id);
    }

    public function proses() {        
        
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('data[status_code]', 'Supplier Code', 'required|trim');
            $this->form_validation->set_rules('data[status_name]', 'Supplier Name', 'required|trim');
            $this->form_validation->set_rules('data[status_type]', 'Supplier Address', 'required|trim');

            if ($this->form_validation->run()) {

                $this->db->trans_start();

                $id = $this->input->post('id'); 
                $data = $this->input->post('data');
                // status number adalah statuspr_id / statuspo_id / statusservice_id / dll
                $status_number = $this->input->post('status_number');

                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */
                switch ($data['status_type']) {
                    case 15:
                        // PR
                        $model = $this->status_pr_model;
                        break;
                    case 17:
                        // PO
                        $model = $this->status_po_model;
                        break;
                    case 10:
                        // SERVICE REQUEST
                        $model = $this->status_service_model;
                        break;
                    case 11:
                        // REPAIR JOB
                        $model = $this->status_repair_model;
                        break;
                    default:
                        # code...
                        break;
                }

                $specific = $model->getById($status_number)->row_array();
                if($specific)
                    $id = $specific['status_id'];

                if ($id == '') {
                    // insert to status_model
                    $this->status_model->create($data);
                    $status_id = $this->status_model->insert_id;
                    // insert to status specific . ex : status_pr / status_po / status_service
                    $temp['status_id'] = $status_id;
                    $temp[$model->primary] = $status_number;
                    $model->create($temp);
                    
                } else {
                    $this->status_model->update($data, $id);
                    $model->where('status_id', $id)->set($model->primary, $status_number)->update();
                }

                if($this->db->trans_status()){
                    $message = array(true, 'Success', 'Save Success', 'refreshTable()');
                }else{
                    $message = array(false, 'Error', 'Database Error', 'refreshTable()');
                }

                $this->db->trans_complete();

            } else {
                $message = array(false, 'Terjadi Kesalahan', validation_errors(), '');
            }
            echo json_encode($message);
        } else {
            $this->access_right->redirect();
        }
    }

    public function proses_delete() {
        $id = $this->input->post('id');
		$this->access_right->otoritas('delete', true);
        if ($this->access_right->otoritas('delete', true)) {
            $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');
            if ($this->status_model->delete($id)) {
                $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', 'refreshTable()');
            }
            echo json_encode($message);
        }
    }

    
}


<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class shipping_mutasi extends MY_Controller {
    private $dataheader = array();
    private $totkoli = 0;
    private $arr_koli = array();

    public function __construct() {
        parent::__construct();
        $this->load->model(array(
        	'shipping_mutasi_model','mutasi_model'
        ));
        //$this->button['add'] = view::button_add(array('onclick'=>"btnLoadNextPage(this)"));
    }

    public function test(){
        $this->load->model(array('po_store_model','purchase_order_detail_model'));
        $model = $this->purchase_order_detail_model;
        $this->db->trans_start();

        $this->koli_model
            ->findByPk(1)
            ->set('koli_status', 90)
            ->update();
        vdebug('asd');
        $this->db->trans_complete();
    }

    public function main(){
        $data['content']    = 'base/page_content.tpl';     
        $data['table']['main']  = $this->table_main(array('wrapper'));
        return $this->twig->display('index.tpl', $data);
    }

    public function index() {

        $data['title'] 		= 'SHIPPING MUTASI';
        $data['sidebar'] 	= $this->access_right->menu();
        $data['content'] 	= 'base/page_content.tpl';     

        $data['javascript'] = array('machine-transaction.js');
        //TADINYA page-index diganti dengan main
        $data['table']['main'] = $this->getForm('page-index', $id);
        //$data['table']['main'] = $this->getForm('page-index', $id);       
        $this->twig->display('index.tpl', $data);
    }

    public function form_shipping($id = 0){
        $ids = $this->input->post('ids');
        echo $this->getForm('form-shipping', $id, array('ids'=>$ids));
    }

    public function proses_shipping($id = 0){
        $this->load->model('purchase_order_model');
        $ids = $this->input->post('ids');
        $data = $this->input->post('data');

        $modelShippingMutasi = $this->shipping_mutasi_model;
        $modelMutasi = $this->mutasi_model;
/*        $modelShipKoli = $this->shipping_koli_model;
        $modelPO = $this->purchase_order_model;
*/
        //------------------------------------------
        $this->db->trans_start();
        $dataDestination = $modelMutasi 
            ->select('store_destination_id,store_id')
            ->where_in('mutasi_id',$ids)
            ->group_by('store_destination_id')
            ->get();
        foreach ($dataDestination->result() as $row) {
            $do_code = $this->getCode($row->store_destination_id);
            //echo '1212'.$do_code;
            $dataInsertShipping = array(
                'shipping_code' => $do_code,
                'type'=>storetype::WAREHOUSE,
                'shipping_status' => transactionstatus::SHIP,
                'truck_number' => $data['truck_number'],
                'store_id' => $row->store_destination_id,
                'fstore_id' => $row->store_id,
                'date_delivered' => $data['date_delivered'],
                'driver_name' => $data['driver_name']);

            $modelShippingMutasi->create($dataInsertShipping);
            $idShipping = $this->shipping_mutasi_model->insert_id;
            $this->mutasi_model
            ->where_in('mutasi_id', $ids)
            ->where('store_destination_id',$row->store_destination_id)
            ->set('status',mutasistatus::SHIP)
            ->set('shipping_mutasi_id',$idShipping)
            ->update(); //-----------update status koli
        }

        
        if($this->db->trans_status()){
            $message = array(
                'success'=>true, 
                //'message'=>'Save Success',
                'message' => $do_code ,
                'callback'=> 'refreshDataTable(true);'
            );
        }else{
            $message = array('success'=>false, 'message'=>$this->db->_error_message());
        }
        $this->db->trans_complete();
        echo json_encode($message);
        
       

    }

    public function table_main($option = array()){

        $this->load->library('Datatable');
        $table = $this->datatable;

        if (in_array('wrapper', $option)) {
            $table->numbering  = true;
            $table->id         = 'table-shipping';
            $table->isScrollable= false;
            $table->header     = array('DESTINATION', 'USER', 'DATE', 'ACTION');
            $table->source     = site_url($this->class_name.'/table_main');
            return $table->generateWrapper();
        } else {
        	$table
	            ->setModel($this->shipping_model)
	            ->setNumbering()
	            ->with(array('store','employee'))
	            ->where('type', storetype::WAREHOUSE)
	            ->select('nama_cabang, nama_pegawai , date_created, shipping_id')
	            ->edit_column('date_created', '$1', 'date::longDate(date_created)')
	            ->edit_column('shipping_id', '$1', 'view::btn_group_receiving_warehouse(shipping_id)');
            echo $table->generate();
        }

    }


    public function add() {
        $this->detail();
    
    }

    public function detail($id = '') {
    	echo $this->getForm('page-detail', $id);
    }

    

    public function getForm($type = '', $id = NULL, $param = array()){
    	$this->load->library(array('custom_form','custom_table'));
    	$this->load->model(array('sequence_model','po_store_model'));
    	$form = $this->custom_form;

    	switch ($type) {
            case 'page-index':
                $this->load->library('custom_tab');
                $tab = new Custom_tab();

                $tab->tab = array(
                    'list_koli' => array(
                                    'title' => 'List Koli',
                                    'body'  => $this->getForm('table-koli', $id, array('wrapper'))
                                ),
                    'list_shipping' => array(
                                    'title' => 'List Shipping',
                                    'body'  => $this->getForm('table-shipping', $id, array('wrapper'))
                                ),
                );
                
                return $tab->generate();
                break;

            case 'table-koli':
                $this->load->library('Datatable');
                $table = new Datatable();

                if (in_array('wrapper', $param)) {
                    $table->id          = 'table-koli';
                    $table->isScrollable= false;
                    $table->checklist   = true;
                    $table->pagination  = false;
                    $table->header      = array('SKU','NAME','QTY','STORE','DESTINATION');
                    $table->source      = site_url($this->class_name.'/getform/table-koli/'.$id);
                    $table->caption_btn_group   = 'Add to Shipping';
                    $table->url_action_group    = site_url($this->class_name.'/form_shipping');
                    $table->callback_group_submit = '
                        var param = {
                            ids : grid.getSelectedRows()
                        };
                        console.log(param);
                        var url = "'.site_url($this->class_name.'/form_shipping/'.$id).'";
                        loadModal(url, param);

                    ';
                    return $table->generateWrapper();
                } else {
                    $table
                        ->setModel($this->mutasi_model)
                        ->with(array('item','outlet','outlet_destinasi'))
                        ->select('mutasi_id,item_key, item_name,qty ,outlet.nama_cabang,outlet_destinasi.nama_cabang as nama_cabang_destinasi')
                        ->edit_column('mutasi_id', '<input type="checkbox" name="id[]" value="$1">','mutasi_id')
                        ->where('status', mutasistatus::APPROVE);
                    echo $table->generate();
                }
                break;

            case 'table-shipping':
                $this->load->model('mesin_model');
                $this->load->library('datatable');
                $table = new Datatable();

                $data = $this->mesin_model->options_filter();
                $filter = array(
                    array('koli_code', 'Coli Code', 'text'),
                    array('nama_cabang', 'Destination', 'text'),
                    array('koli_status', 'Coli Status', 'date'),
                    array('truck_number', 'Truck Number', 'text')
                );


                $table->dataFilter = $filter;


                if (in_array('wrapper', $param)) {
                    $table->id          = 'table-shipping-koli';
                    $table->isScrollable= false;
                    $table->numbering   = true;
                    $table->pagination  = true;
                    $table->header      = array('SHIPPING KODE','SKU','NAME','QTY','STORE','DESTINATION','STATUS',' TRUCK NUMBER','ACTION');
                    //array('SKU','NAME','QTY','STORE','DESTINATION');
                    $table->source      = site_url($this->class_name.'/getform/table-shipping/'.$id);
                    return $table->generateWrapper();
                } else {
                    $model = $this->shipping_mutasi_model;
                    $id = !empty($id) ? $id : 0;
                    $table
                        ->setModel($model)
                        ->setNumbering()
                        ->with(array('mutasi','item','outlet','outlet_destinasi'))
                        //->where('shipping_id', $id)
                        ->select('shipping_code,item_key, item_name,qty ,outlet.nama_cabang,outlet_destinasi.nama_cabang as nama_cabang_destinasi,status,truck_number,shipping_id')
                        ->edit_column('status', '$1', 'mutasistatus::getStatus(status)')
                        ->edit_column('shipping_id', '$1', "view::btn_group_shipping(shipping_code,shipping_id)");
                    echo $table->generate();
                }
                break;

            case 'form-shipping' :

                $param['form_action']   = form_open(site_url($this->class_name.'/proses_shipping/'.$id));
                $data = $this->input->post('ids');
                $hidden_ids = '';
                foreach ($data as $key => $value) {
                    $hidden_ids .= form_hidden('ids[]',$value);
                    # code...
                }
                $param['hidden_ids'] = $hidden_ids;
                $param['date_now'] = date('Y-m-d');

                $form->portlet          = true;
                $form->button_save      = true;                
                $form->id               = 'form-shipping';
                $form->param            = $param;
                $form->url_form         = 'shipping/form_mutasi_store.tpl';
                return $form->generate();
                break;

    		case 'page-detail':
    			$param['title'] 	= 'SHIPPING DETAIL';
    			$param['portlet']	= true;
                $param['button']['portlet'] = view::render_button_group_portlet(array(
                            //view::button_save_form(),
                            view::button_shipping($id),
                            view::button_print_pdf($id)
                    ));
    			$param['button']['back'] = true;
    			$param['form']['info'] 	= $this->getForm('form-info', $id);
                //$param['form']['info_pdf']  = $this->getForm('form-info-pdf', $id);
    			$param['table']['main']	= $this->getForm('page-shipping', $id);
    			return $this->twig->render('base/page_content.tpl', $param);
    			break;

    		

            case 'form-info-pdf' :
                $param['form_action']   = form_open(site_url($this->class_name.'/print_pdf/'.$id));
                $param['data']          = $this->shipping_model->with('store')->getById($id)->row();
                $is_head_office = hprotection::isHeadOffice();
                $param['is_head_office'] = $is_head_office;
                if($is_head_office){
                    $this->load->model('store_model');
                    $htmlOptions            =  'onchange="refreshList(this)" target="#shipping_code" target-type="text" 
                                                data-source="'.site_url($this->class_name.'/getcode').'"
                                                class="form-control bs-select" data-live-search="true" data-placeholder="Select Store"';
                    $param['list_store']     = form_dropdown('data[store_id]', $this->store_model->options_empty(), 13, $htmlOptions);
                }


                $form->button_save      = false;                
                $form->id               = 'form-pdf';
                $form->param            = $param;
                $form->url_form         = 'shipping/form_info.tpl';
                return $form->generate();
                break;

    		case 'form-koli' :
    			$this->load->model('store_model');
    			if($id){

    			}else{
    				
    			}

    			$poStores = $this->po_store_model->with(array('po','store'))->where_in('postore_id',$param['ids'])->get()->result();
    			$postore_id = $param['ids'][0];    			
    			$postore = $poStores[0];

    			$param['postores'] 			= $poStores;
    			$param['data']['koli_code'] = code::getCode('koli',$postore_id);

    			$param['title']			= 'Create Koli';
    			$param['form_action'] 	= form_open(
                                                site_url($this->class_name.'/proses_koli'),
                                                '', array(
                                                	'id' => $id,
                                                	'postore_ids'=>json_encode($param['ids'])
                                                )
                                            );
    			$option = array($postore->store_id => $postore->nama_cabang);
    			$param['list']['store'] = form_dropdown('data[store_id]', $option, $postore->store_id, 'class="bs-select" disabled');

    			$form->portlet          = true;
	            $form->button_back      = true;
	            $form->id               = 'form-koli';
	            $form->param            = $param;
	            $form->url_form         = 'receiving/warehouse/form_koli.tpl';
	            return $form->generate();
    			break;
    		default:
    			# code...
    			break;
    	}
    }

    public function getCode($storeId = NULL){
        if (!$storeId)
            return;
        return code::getCodeAndRaise('do', $storeId);
    }

    public function __proses_shipping($id = NULL){
        
        $id = !empty($id) ?$id:0;
        $this->db->trans_start();
        $data = $this->input->post('data');
        $data['type'] = storetype::WAREHOUSE;
        $data['shipping_status'] = transactionstatus::SHIP;
        $this->shipping_model->create($data);
        $id_new = $this->shipping_model->insert_id;


        // [POINT] update table shipping . truck number
        
        /*$data['shipping_status'] = transactionstatus::SHIP;
        $this->shipping_model->update($data, $id);*/

        // [POINT] update status di koli. postore. podetail.prdetail
        $this->db->set('shipping_id',$id_new)
            ->where('shipping_id',$id)
            ->update('zn_shipping_koli');
        $id = $id_new;
        $koliIds = $this->koli_model->select('this.koli_id')->with('shipping')->where('shipping_id', $id)->get()->result_array();
        $ids = array_values_index($koliIds, 'koli_id');
        $this->koli_model
            ->where_in('koli_id', $ids)
            ->set('koli_status',transactionstatus::SHIP)
            ->update();

        if($this->db->trans_status()){
            $message = array(
                        'success' => true,     
                        'message' => 'Save Success',
                        //'callback'=> "reloadPageLoading();",        
                    );
        }else{
            $message = array(
                        'success' => false,                        
                        'message' => 'Database Error Occured',
                    );
        }
        $this->db->trans_complete();
        echo json_encode($message);
    }


    public function proses($postore_id){
        

        if(!$postore_id) return;

        $this->load->model(array('purchase_request_detail_model','po_store_model','item_in_model','item_stok_model'));
        $prdetailModel = $this->purchase_request_detail_model;
            $iteminModel = $this->item_in_model;
            $itemStokModel = $this->item_stok_model;

        $this->db->trans_start();
        /*
            data = array of (prdetail_id, qty_received)
        */
        $postore = $this->po_store_model->getById($postore_id)->row_array();
        
        if(!$postore) return;

        $data = $this->input->post('data');
        if(!$data) return;

        foreach ($data as $value) {

            $prdetail = $prdetailModel->getById($value['prdetail_id'])->row_array();

            // UPDATE PURCHASE_REQUEST_DETAIL
            $prdetail_data = array();
            $prdetail_data['qty_received']   = $value['qty_received'];
            $prdetail_data['qty_temp']       = $value['qty_received'];
            $prdetail_data['detail_status']  = prstatus::RECEIVE;
            $prdetailModel->update($prdetail_data, $value['prdetail_id']);

            // INSERT TO ITEM_IN
            $itemin = array();
            $itemin['store_id'] = $postore['store_id'];
            $itemin['item_id']  = $prdetail['item_id'];
            $itemin['price']    = $prdetail['price'];
            $itemin['qty']      = $value['qty_received'];
            $itemin['postore_id'] = $postore_id;
            $this->item_in_model->create($itemin);

            // UPDATE TO ITEM_STOK : cek item_stok ada atau tidak
            $itemstok = $itemStokModel
                        ->where('store_id', $postore['store_id'])
                        ->where('item_id', $prdetail['item_id'])
                        ->get()->row();
            if($itemstok) {
                // menghitung harga item rata-rata. update harga rata-rata per item per store
                $rata = 0;
                if($value['qty_received'] > 0)
                $rata = (($itemstok->stok_store *  $itemstok->price_avg) +
                        ($prdetail['price'] * $value['qty_received'])) / 
                        ($itemstok->stok_store + $value['qty_received']);

                $itemStokModel->set('stok_total','stok_total+'.$value['qty_received'], FALSE);
                $itemStokModel->set('stok_gudang','stok_gudang+'.$value['qty_received'], FALSE);
                $itemStokModel->set('stok_store','stok_store+'.$value['qty_received'], FALSE);
                $itemStokModel->set('price_avg', $rata);
                $itemStokModel->where('store_id', $postore['store_id']);
                $itemStokModel->where('item_id', $prdetail['item_id']);
                $itemStokModel->update();
            }else{
                $itemStok_data['stok_total'] = $value['qty_received'];
                $itemStok_data['stok_gudang'] = $value['qty_received'];
                $itemStok_data['stok_store'] = $value['qty_received'];
                $itemStok_data['store_id'] = $postore['store_id'];
                $itemStok_data['item_id'] = $prdetail['item_id'];
                $itemStok_data['price_avg'] = $prdetail['price'];
                $itemStokModel->create($itemStok_data);
            }

        }

        // UPDATE PO_STORE
        $postore_data['postore_status'] = 1; //CLOSED
        $postore_data['date_received']  = date::now_sql();
        $this->po_store_model->update($postore_data, $postore_id);

        if($this->db->trans_status()){
            $message = array(
                        'success' => true,     
                        'message' => 'Save Success',
                        'callback'=> "reloadPageLoading()",        
                    );
        }else{
            $message = array(
                        'success' => false,                        
                        'message' => 'Database Error Occured',
                    );
        }

        
        $this->db->trans_complete();

        echo json_encode($message);

    }


    function getJmlKoliFromKoliCode($koli_code = ''){
        $tmp = $this->koli_model
                ->select('qty')
                ->where('koli_code',$koli_code)
                ->get()
                ->result_array();
        return $tmp[0]['qty'];
    }


    function createHeaderReport($data=array()){
        $hleft='<table border="0" style="width:300px;">
                <tr><td>DO# </td><td>'.$data['shipping_code']
                .' / '.$this->session->all_userdata('user_name').'</td></tr>
                <tr><td>Trx Code</td><td>'.$this->attr_po.'</td></tr>
                <tr><td>Supplier</td><td>'.$this->attr_po.'</td></tr>
                <tr><td>Remark</td><td>Zone2000</td></tr>
                </table>';
        
        $hcenter = '<table border="0"><tr><td colspan="2"><b><h2>SURAT JALAN</h2></b></td></tr>
                </table>';

        $hright='<table border="0" style="width:300px;">
                <tr><td>Tujuan </td><td>'.$data['tujuan'].'</td></tr>
                <tr><td>Truck Number</td><td>'.$data['truck_number'].'</td></tr>
                <tr><td>Date</td><td>'.date("Y-m-d H:i:s").'</td></tr>
                </table>';

        $tab ='<table style="width:900px;"><tr><td style="width:300px;">'.$hleft.'</td>
                <td style="width:300px;"><center>'.$hcenter.'</center></td></tr>
                <td style="width:300px;">'.$hright.'</td></tr></table>';
        
        $result_tab = $tab;

        return $result_tab;
    }

    function createHeaderReportSP($data=array()){
        $hleft='<table border="0" style="width:300px;">
                <tr><td>No SJ</td><td>'.$data['sj'].'</td></tr>
                <tr><td>No SP</td><td>'.$data['sp'].'</td></tr>
                <tr><td>Kode Koli</td><td>'.$data['koli_code'].'</td></tr>
                <tr><td>Jml Koli</td><td>'.$data['jml_koli'].'</td></tr>
                <tr><td>Keterangan</td><td></td></tr>
                <tr><td>Faktur</td><td>Zone2000</td></tr>
                </table>';
        
        $hcenter = '<table border="0"><tr><td colspan="2"><b><h2>SURAT PENGANTAR</h2></b></td></tr>
                </table>';

        $hright='<table border="0" style="width:300px;">
                <tr><td>Tujuan </td><td>'.$data['tujuan'].'</td></tr>
                </table>';

        $tab ='<table border="0" style="width:900px;"><tr><td style="width:300px;">'.$hleft.'</td>
                <td style="width:300px;"><center>'.$hcenter.'</center></td></tr>
                <td style="width:300px;">'.$hright.'</td></tr></table>';
        
        $result_tab = $tab;

        return $result_tab;
    }


    function createMainTable($header=array(),$data=array()){

        $br = '<br><br><br><br><br><br><br><br><br>';
        $headerlength = count($header);
        $count = 0;

        $res_header .='<tr>';
        
        foreach ($header as $key => $value) {
            $res_header .= '<td style="font-weight:bold;">'.$header[$key].'</td>';
        }
        $res_header .='</tr>';


        $trbuka ='<tr>';
        $trtutup ='</tr>';


        $totalkoli = 0;
        $number = 1;

        $tab_content;
        $count=0;
        foreach ($data as $key => $value) {
            $tab_content .= '<tr>';
            $tab_content .= '<td>'.$number.'</td>';
            foreach ($data[$key] as $k => $v) {
                if($k != 'truck_number' && $k != 'shipping_code' && $k != 'nama_cabang'){
                    $tab_content .= '<td>'.$data[$key][$k].'</td>';
                }

                if($count==$headerlength-1){
                    $tab_content .= '</tr>';
                }

                if($k=='qty'){
                    $totalkoli = $totalkoli+$data[$key][$k];
                }
            }
            $number++;
            $count++;
        }

        $trtotal ='<table border="0"><tr><td><b>Total Koli : </b></td><td><b>'.$totalkoli.'</b></td><td><b>Total SP : </b></td><td><b>'.$count.'</b></td></tr></table>';

        $all_table .= '<table border="0" style="width:900px;">'.$res_header.$tab_content.'</table>';
        $all_table .= $trtotal;
        return $all_table;
    }

    function createMainTableSP($header=array(),$data=array()){

        $br = '<br><br><br><br><br><br><br><br><br>';
        $headerlength = count($header);
        $count = 0;

        $res_header .='<tr>';
        
        foreach ($header as $key => $value) {
            $res_header .= '<td style="font-weight:bold;">'.$header[$key].'</td>';
        }
        $res_header .='</tr>';


        $trbuka ='<tr>';
        $trtutup ='</tr>';
        $number = 1;
        $totharga = 0;

        $tab_content;
        foreach ($data as $key => $value) {
            $count=0;
            $tab_content .= '<tr>';
            $tab_content .= '<td>'.$number.'</td>';
            foreach ($data[$key] as $k => $v) {
                
                if($k != 'nama_cabang'){
                    $dataheader['tujuan']=$data[$key][$k];
                }

                if($k != 'truck_number' && $k != 'shipping_code' && $k != 'nama_cabang'){

                    if($k == 'sub_total'){
                        $totharga = $totharga + $data[$key][$k];
                        $data[$key][$k] = 'Rp '.hgenerator::rupiah($data[$key][$k]);
                    }

                    if($k == 'price'){
                        $data[$key][$k] = 'Rp '.hgenerator::rupiah($data[$key][$k]);
                    }

                    $tab_content .= '<td>'.$data[$key][$k].'</td>';

                }


                if($count==$headerlength-1){
                    $tab_content .= '</tr>';
                }
                $count++;
            }
            $number++;
        }

        $tr_total = '<tr><td colspan="5"><b>Total</b></td><td><b>'.'Rp '.hgenerator::rupiah($totharga).'</b></td></tr>';
        $all_table .= '<table border="0" style="width:900px;">'.$res_header.$tab_content.$tr_total;
        $all_table .= '</table>';
        return $all_table;
    }

    function createFooterReport(){
        $hleft='<table border=0><tr><td><b>Tanda Terima,</b></td></tr>
                </table>';

        $hright='<table border="0"><tr><td><b>Hormat Kami,<b></td></tr></table>';
        $br = '<br><br><br><br><br><br><br><br><br>';
        $hr = '<hr>';
        $tab ='<table border="0" style="width:900px; position:absolute; margin-bottom:0px;"><tr><td style="width:400px;">'.$hleft.$br.$hr.'</td><td style="width:400px;">'.$hright.$br.$hr.'</td></tr></table>';
        $result_tab = $tab;

        return $result_tab;        
    }

    function createFooterReportSP(){
        $hleft='<table border=0><tr><td><b>Pengirim,</b></td></tr>
                </table>';

        $hmiddle = '<table border="0"><tr><td><b>Ekspedisi,<b></td></tr></table>';

        $hright='<table border="0"><tr><td><b>Penerima,<b></td></tr></table>';

        $br = '<br><br><br><br><br><br><br><br><br>';
        $hr = '<hr>';

        $tab ='<table border="0" style="width:900px; position:absolute; margin-bottom:0px;"><tr><td style="width:300px;">'.$hleft.$br.$hr.'</td><td style="width:300px;">'.$hmiddle.$br.$hr.'</td><td style="width:300px;">'.$hright.$br.$hr.'</td></tr></table>';
        $result_tab = $tab;

        return $result_tab;        
    }


    public function getSJData($code=''){
        $this->load->model('shipping_koli_model');
        $model = $this->shipping_koli_model;
        $code = !empty($code) ? $code : 0;
        
        $result = $model->with(array('koli','storekoli','shipping','supplier'))
            //nama_cabang = destination
            //shipping_code dijadikan global
            ->select('qty, note, koli_code, nama_cabang, shipping_code, koli_code as kc, truck_number')
            ->where('shipping_code', $code)
            ->get()
            ->result_array();
        foreach ($result as $key => $value) {
            foreach ($result[$key] as $k => $v) {
                if($k=='status'){
                    $result[$key][$k] = transaksi::getTransStatus($result[$key][$k]);
                }

                if($k=='nama_cabang')
                    $this->dataheader['tujuan']=$result[$key][$k];                

                if($k=='truck_number')
                    $this->dataheader['truck_number']=$result[$key][$k];                                    

                if($k=='shipping_code')                                    
                    $this->dataheader['shipping_code']=$result[$key][$k];

            }
        }

        $this->totkoli = $totalkoli;
        return $result;
    }


    public function getSPData($shipping_code='', $koli_code=''){
        $this->load->model('receiving_postore_model');
        $model = $this->receiving_postore_model;
        $code = !empty($code) ? $code : 0;
        
        $result = $model->with(array('receiving_po_koli','po_detail','item','koli','shipping_koli','shipping','supplier','store'))
            //nama_cabang = destination
            //shipping_code dijadikan global
            ->select('koli_code, item_name, po_detail.qty_approved as qty, price, (po_detail.qty_approved*price) as sub_total, nama_cabang')
            //->select('*')
            ->where(array('shipping_code' => $shipping_code, 'koli_code' => $koli_code))
            ->get()
            ->result_array();

        $this->dataheader['sj'] = $shipping_code;
        $this->dataheader['sp'] = $koli_code;
        $this->dataheader['jml_koli'] = $this->getJmlKoliFromKoliCode($koli_code);
        foreach ($result as $key => $value) {
            foreach ($result[$key] as $k => $v) {
                if($k=='nama_cabang'){
                    $this->dataheader['tujuan']=$result[$key][$k];
                }

                if($k=='koli_code'){
                    $this->dataheader['koli_code']=$result[$key][$k];
                    //set global coli code
                    array_push($this->arr_koli, $result[$key][$k]);

                }

            }
        }

        return $result;
        //print_r($result);
    }


    function print_pdf_suratjalan($shipping_code){
        $this->load->helper('mpdf');
        

        //$html = '1212';//  $this->getForm('table-shipping', $id);
        
        $header = array('No','Jml Koli','Keterangan', 'No SP', 'No Colie');
        
        $sjdata = $this->getSJData($shipping_code);
        $html = $this->createHeaderReport($this->dataheader);
        $html .= $this->createMainTable($header, $sjdata);
        $html .= $this->createFooterReport();
    
        to_pdf($html,'zone 2000');
        //echo $html;
    }

    function print_pdf_pengantar($shipping_code='', $koli_code=''){
        $this->load->helper('mpdf');
        

        //$html = '1212';//  $this->getForm('table-shipping', $id);
        
        $header = array('No','Faktur','Item Name', 'Qty', 'Price', 'Sub Total');
        
        $spdata = $this->getSPData($shipping_code,$koli_code);

        $html = $this->createHeaderReportSP($this->dataheader);
        $html .= $this->createMainTableSP($header, $spdata);

        $html .= $this->createFooterReportSP();
    
        //print_r($spdata);
        to_pdf($html,'zone 2000');
        //echo $html;    
    }


    function print_pdf_all($shipping_code=''){
        $this->load->helper('mpdf');
        $break = '<pagebreak />';

        //$html = '1212';//  $this->getForm('table-shipping', $id);
        
        $header = array('No','Jml Koli','Keterangan', 'No SP', 'No Colie');
        
        $sjdata = $this->getSJData($shipping_code);
        $html = $this->createHeaderReport($this->dataheader);
        $html .= $this->createMainTable($header, $sjdata);
        $html .= $this->createFooterReport();
        $html .= $break;

        $sjdata = $this->getSJData($shipping_code);

        $arr_koli_code = array();
        foreach ($sjdata as $key => $value) {
            foreach ($sjdata[$key] as $k => $v) {
                if($k=='kc'){
                    array_push($arr_koli_code, $sjdata[$key][$k]);                
                }
            }
        }

        $sum_koli = count($arr_koli_code);
        foreach ($arr_koli_code as $k2 => $v2) {
            $headersp = array('No','Faktur','Item Name', 'Qty', 'Price', 'Sub Total');
            
            $spdata = $this->getSPData($shipping_code,$arr_koli_code[$k2]);

            $html .= $this->createHeaderReportSP($this->dataheader);
            $html .= $this->createMainTableSP($headersp, $spdata);

            $html .= $this->createFooterReportSP();
            if($k2<$sum_koli-1){
                $html .= $break;                
            }
        }
        to_pdf($html,'zone 2000');
    }
}

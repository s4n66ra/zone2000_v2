<?php

/**
 * Description of bbm
 *
 * @author SANGGRA HANDI
 */
class purchase_request extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function testing(){
        echo code::getCode('pr',11);
    }

    public function index() {

        $data['title'] = 'PERMINTAAN PEMBELIAN';
        //  $data['data_source'] = base_url($this->class_name . '/load');        

        /* BUTTON GROUP */
        $button_group    = array(
                                view::button_add()
                            );

        $data['button_group'] = view::render_button_group($button_group);

        /* INSERT LOG */
        $this->access_right->activity_logs('view','PR Hadiah');

        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'base/page_content.tpl'; 
        
        
        $data['table']['main'] = $this->table_main(array('wrapper'));
       // $data['st_extend2'] = TRUE;
        $this->twig->display('index.tpl', $data);
    }

    public function table_main($options = array()){
        $this->load->model('transaction_bundle_model');
        $this->load->library('Datatable');
        $table = new Datatable();
        $table->reset();

        if (in_array('wrapper', $options)){
            $table->numbering  = true;
            $table->isScrollable=false;
            $table->id         = 'table-purchase-request';

            $filter = array(
                array('pr_code', 'NO.PP', 'text'),
                array('nama_cabang', 'Store', 'text'),
                array('date_created', 'Date Creted', 'date'),
                array('pr_type', 'PR Type', 'text'),
                array('pr_status', 'Status', 'text'),
                array('nama_divisi_md','Division Name','text'),
            );


            $table->dataFilter = $filter;


            $table->header     = array('NO.PP', 'KD STORE', 'STORE','DATE', 'TYPE','DIVISI', 'STATUS', 'TOTAL','CREATED-BY', 'ACTION');
            $table->source     = site_url($this->class_name.'/table_main');
            return $table->generateWrapper();
        }else{

            $table
                ->setNumbering()
                ->setModel($this->purchase_request_model)
                ->select("pr_code,kd_cabang,  nama_cabang, date_created, pr_type,nama_divisi_md, pr_status, sum(qty_request*price) as total, nama_pegawai, this.pr_id as det")
                ->with(array('store','detail','employee','divisi'))
                ->group_by('this.pr_id')
                ->order_by('date_created','DESC')
                ->edit_column('date_created', '$1', 'date::shortDate(date_created)')
                ->edit_column('total', '$1', 'view::format_number(total)')
                ->edit_column('pr_status', '$1', "transaksi::getPrStatus(pr_status)")
                ->edit_column('pr_type', '$1', "transaksi::getItemType(pr_type)")
                ->edit_column('det', '$1', "view::btn_group_pr(det)");

            echo $table->generate();
        }
    }

    public function add($id='',$status_delete = 0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->config->load('twig');
            $this->load->model('divisi_md_model');

            $title = 'NEW PURCHASE REQUEST';
            $data['form_action'] = $this->class_name . '/proses';

            $data['id'] = $id;
            if ($id != '') {
                $title = 'Edit Data';
                $row = $this->purchase_request_model->with('document')->getById($id)->row();
                $data['data'] = $row;
            }

            $is_head_office = hprotection::isHeadOffice();

            $data['title']      = $title;
            $data['assets_url'] = $this->config->item('assets_url');
            $data['sidebar']    = $this->access_right->menu();
            $data['form']       = 'purchase_request/form.tpl';
            $data['is_head_office'] = $is_head_office;
            
            if($is_head_office){
                $this->load->model('store_model');
                $htmlOptions            =  'onchange="refreshList(this)" target="#pr_code" target-type="text" 
                                            data-source="'.site_url($this->class_name.'/getcode').'"
                                            class="form-control bs-select" data-live-search="true" data-placeholder="Select Store"';
                $data['list_store']     = form_dropdown('data[store_id]', $this->store_model->options_empty(), $row->store_id, $htmlOptions);
            }else{
                $data['data']['pr_code'] = $this->getCodeReturn($this->session->userdata('store_id'));
            }

            $button_group   = array();
            $button_group[] = view::button_back();

            if($status_delete == 0){
                $data['form_action']    = view::form_input($id);
                $button_group[]         = view::button_save();
            }else{
                $data['title']          = 'Delete PR Hadiah';
                $data['form_action']    = view::form_delete($id);;
                $data['readonly']       = 'readonly=""';
                $button_group[]         = view::button_delete_confirm();
            }
            $htmlOptions =  'class="form-control select2me"';
            $data['divisi']  = form_dropdown('data[id_divisi_md]', $this->divisi_md_model->options_empty(), $row->id_divisi_md, $htmlOptions.' data-placeholder="Pilih Divisi"');

            $data['button_group'] = view::render_button_group($button_group);
            $this->twig->display('base/page_form.tpl', $data);
        } else {
            $this->access_right->redirect();
        }
    }

    public function edit($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('edit', true);
        $this->add($id);
    }

    public function delete($id_enc) {        
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('delete', true);
        $this->add($id,1);
    }

    public function proses() {        
        /*
            $_POST :
                id : purchase request id
                doc_number : 
                data : array (
                            pr_type : 1,2,3,4,5
                            pr_note
                        )


        */

        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('data[pr_type]', 'Purchase Request Type', 'required|trim');
            //$this->form_validation->set_rules('data[store_id]', 'Store', 'required|trim');
            $this->form_validation->set_rules('data[pr_code]', 'Purchase Request Code', 'required|trim');
            $this->form_validation->set_rules('data[id_divisi_md]', 'Divisi', 'required|trim');

            if ($this->form_validation->run()) {
                $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');
                
                $id = $this->input->post('id');
                $data = $this->input->post('data');

                if ($id == '') {
                    $this->db->trans_start();
                    $user_id = $this->session->userdata('user_id');
                    $store_id= $this->session->userdata('store_id');
                    $now = date::now_sql();

                    $this->load->model(array(
                        'transaction_model','transaction_store_model','transaction_document_model','sequence_model'
                    ));
                    // insert to transaction
                    $trans['transactiontype_id'] = transactiontype::PR;
                    $trans['rec_user'] = $user_id;
                    $trans['rec_created'] = $now;
                    $this->transaction_model->create($trans);
                    $transaction_id = $this->db->insert_id();

                    // insert to transaction store
                    $trans_store['transaction_id'] = $transaction_id;
                    $trans_store['store_id'] = $store_id;
                    $this->transaction_store_model->create($trans_store);

                    // insert to transaction document
                    // $trans_doc['transaction_id'] = $transaction_id;
                    // $trans_doc['doc_number'] = $this->input->post('doc_number');
                    // $this->transaction_document_model->create($trans_doc);

                    // insert to purchase_request
                    
                    // cek jika dibuat oleh HO
                    if(hprotection::isHeadOffice()){
                        $data['create_type']    = 1;
                        $data['store_id']       = $data['store_id'];
                        $data['created_by']     = $store_id;
                    }else{
                        $data['store_id']       = $store_id;
                        $data['created_by']     = $store_id;
                    }

                    $data['transaction_id'] = $transaction_id;
                    $data['user_id'] = $user_id;

                    // set doc.number / pr_code
                    $data['pr_code'] = $data['pr_code'];
                    $this->sequence_model->raiseSequence('pr');

                    $this->purchase_request_model->create($data);
                    $pr_id = $this->db->insert_id();

                    if($this->db->trans_status() === TRUE){
                        $message = array(
                            true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.',"loadNextPage(1,'".site_url($this->class_name."/detail/".$pr_id)."')"  
                        );
                    }else{
                        // Database Error
                        $message = array(false, 'Database Error Occured', $this->db->_error_message(), '');
                    }
                    $this->db->trans_complete();

                } else {
                    // EDIT
                    $this->load->model(array(
                        'transaction_model','transaction_store_model','transaction_document_model'
                    ));

                    $pr = $this->purchase_request_model->getById($id)->row();
                    if($pr){
                        $this->db->trans_start();
                        // Edit purchase Request
                        $this->purchase_request_model->update($data, $id);

                        // Edit Transaction Document Number
                        $trans_doc['doc_number'] = $this->input->post('doc_number');
                        $this->transaction_document_model->where('transaction_id', $pr->transaction_id)->update($trans_doc);
                        file_put_contents('parameter.txt', $this->db->last_query());

                        if($this->db->trans_status() === TRUE){
                            $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', 'refresh_filter()');
                        }else{
                            // Database Error
                            $message = array(false, 'Database Error Occured', $this->db->_error_message(), '');
                        }                            
                        $this->db->trans_complete();
                    }
                }
            } else {
                $message = array(false, 'Terjadi Kesalahan', validation_errors(), '');
            }
            echo json_encode($message);
        } else {
            $this->access_right->redirect();
        }
    }

    public function proses_delete() {
        $id = $this->input->post('id');
        $this->access_right->otoritas('delete', true);
        if ($this->access_right->otoritas('delete', true)) {
            $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '','id : '.$id);
            if ($this->purchase_request_model->delete($id)) {
                $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', 'refresh_filter()');
                /* INSERT LOG */
                $this->access_right->activity_logs('delete','Delete pr_hadiah');
                /* END OF INSERT LOG */
            }
            echo json_encode($message);
        }
    }

    public function confirm($act, $id){
        $button_group   = array();
        $button_group[] = view::button_back();
        $button_group[] = view::button_confirm();

        switch ($act) {
            

            case 'send':
                $data['title'] = 'SEND REQUEST';
                $data['status'] = 1;
                break;
            
            case 'revisi':
                $data['title'] = 'REVISI REQUEST';
                $data['status'] = 2;
                break;

            case 'reject':
                $data['title'] = 'REJECT REQUEST';
                $data['status'] = 4;
                break;

            case 'approve':
                $data['title'] = 'APPROVE REQUEST';
                $data['status'] = 5;
                break;

            case 'cancel':
                $data['title'] = 'CANCEL REQUEST';
                $data['status'] = 9;
                break;

            default:
                # code...
                break;
        }
        $data['hide_kd']        = true;
        $data['button_group']   = view::render_button_group($button_group);
        $data['form_action']    = view::form($id,$this->class_name.'/confirm_proses');
        $data['form'] = $this->class_name.'/form.tpl';
        $this->twig->display('base/page_form.tpl', $data);

    }

    public function proses_request($id){
        $message = array('success'=>false, 'message'=>'No Purchase Request Selected');

        if (!$id) {
            echo json_encode($message);return;
        }

        $this->db->trans_start();
        $this->purchase_request_model
            ->set('pr_status', transactionstatus::WAITING_REVIEW)
            ->set('date_request', date::now_sql())
            ->where('this.pr_id',$id)
            ->update();

        //MENAMBAHKAN NOTIFIKASI BARU
        $this->load->model(array('notification_model','notification_grup_model','user_model','notification_user_model','notification_pr_model'));
        $this->notification_model->create(array('kd_notif'=>'001','status'=>0, 'rec_created' => date::now_sql()));
        
        $notif_id = $this->db->insert_id();
        $this->notification_grup_model->create(array('notif_id'=>$notif_id, 'grup_id' => 15));
        $usr = $this->user_model
                ->select('user_id')
                ->where('grup_id', 15)
                ->get()
                ->result_array();

        foreach ($usr as $key => $value) {
            $this->notification_user_model->create(array('user_id'=>$usr[$key]['user_id'], 'notif_id'=>$notif_id));            
        }

        //INSERT KE ZN_NOTIFICATION_PR
        $this->notification_pr_model->create(array("notif_id" => $notif_id, "pr_id" => $id));



        //END NOTIFIKASI---

        if($this->db->trans_status()){
            $message = array(
                'success'=>true, 'message'=>'Save Success',
                'callback'=>'reloadPageLoading()',
            );
        }else{
            $message['message'] = $this->db->_error_message();
        }
        $this->db->trans_complete();
        echo json_encode($message);
    }

    public function confirm_proses($prid = NULL){
        $id     = $this->input->post('id');
        $status = $this->input->post('status');
        $data   = $this->input->post('data');

        if(!$id) $id = $prid;

        //$this->purchase_request_model->update($data, $id);
        $result = $this->purchase_request_model->proceed($id, $status);
        
        // if($result['success']){        
        //     // refresh page content berguna untuk me-refresh page content dengan parameter url data-source, id_element, 
        //     // id-element default = page-content
        //     // data-source default= diambil dari attr data-source pada id element
        //     $message = array(true, 'Proses Berhasil', 'Proses Berhasil.', 'reloadPageLoading()');
        // }else{
        //     $message = array(false, 'Proses gagal', $result['message'], '','id : '.$id);    
        // }
        echo json_encode($result);
    }

    

    public function detail($id){
        // id : id_pr_detail

        $data['title'] = 'PURCHASE REQEUST';
        $data['subtitle']= transaksi::getItemType($pr->pr_type);
        
        $data['data_source_page']   = base_url($this->class_name . '/detail/'.$id);
        $pr                         = $this->purchase_request_model
                                        ->with(array('document','store'))
                                        ->getById($id)->row();
        $data['data']               = $pr;
        

        // button bagian atas
        $btnGroup = array();
        if($pr->pr_status==prstatus::DRAFT) 
            $btnGroup[] = view::button_add_detail(array('id'=>$id));
        $data['button_group'] = view::render_button_group($btnGroup); 

        // button bagian bawah
        if ($pr->pr_status==prstatus::REVISI || $pr->pr_status==prstatus::DRAFT){
            $btngroup2[] = view::button_cancel_request($id);
            $btngroup2[] = view::button_send_request($id);
        }
        $data['button_group_2'] =view::render_button_group($btngroup2);

        // view keterangan status pr dan revisi pr
        $data['status_pr']  = view::render_status_pr($data['data']->pr_status);
        $data['revisi_pr']  = view::render_revisi_pr($data['data']->count_revisi);
        $data['page']['id'] = 'pr-detail';

        $data['table']['main']  = $this->table_detail($id, array('wrapper'));
        $data['readonly']       = "readonly";
        $data['isBtnPageBack']  = true;
        $data['form_info']      = 'purchase_request/form_info.tpl';

        if($this->input->is_ajax_request()){
            $this->twig->display('base/page_content.tpl',$data);
        }else{
            $data['sidebar'] = $this->access_right->menu();
            $data['content'] = 'base/page_content.tpl'; 
            $this->twig->display('index.tpl', $data);    
        }
        
    }

    public function table_detail($id_pr = '', $options = array()){
        $pr = $this->purchase_request_model->getById($id_pr)->row();
        $this->load->library("custom_table");
        $table = new custom_table();
        $table->headerAlign = array('left','left','left', 'right','right','right','right', 'center');
        $table->columns = array('SKU', 'ITEM', 'SUPPLIER', 'REQUEST', 'APPROVED', 'PRICE', 'TOTAL');
        //$table->columns = array('SKU', 'ITEM', 'SUPPLIER', 'LAST STOK', 'REQUEST', 'APPROVED', 'PRICE', 'TOTAL');

        if ($pr->pr_status==prstatus::DRAFT)
            $table->columns[] = 'ACTION';

        $table->id      = 'table-pr-detail';
        $table->data_source = site_url($this->class_name.'/table_detail/'.$id_pr);
        $table->data    = $this->purchase_request_detail_model->getDataCustomTable($id_pr);
        $param['total'] = $this->purchase_request_detail_model->total;
        $table->footer  = $this->twig->render('base/table/footer_total.tpl', $param);
        
        if(in_array('wrapper', $options))
            return $table->generateWithWrapper();
        else{
            echo $table->generate();
        }

    }

    public function add_detail($pr_id ='', $id='', $status_delete = 0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            
            // load custom form
            $this->load->library('custom_form');
            $form = new custom_form();

            // load model
            $this->load->model('purchase_request_detail_model');
            $this->load->model(array('item_model', 'supplier_model', 'item_supplier_model'));
            $data['id'] = $id;

            if ($id) {
                
                $row    = $this->purchase_request_detail_model->with('supplier')->getById($id)->row();
                $pr_id  = $row->pr_id;
                $param['title'] = 'EDIT ITEM';
                $param['data']  = $row;
            } else{
                // input new . set pr_id
                $param['title']         = 'NEW ITEM';
                $param['data']['pr_id'] = $pr_id;
            }

            if($pr_id){
                $pr             = $this->purchase_request_model->getById($pr_id)->row();
                $itemtype_id    = $pr->pr_type;

            }

            // DROPDOWN ITEM BERDASARKAN ITEMTYPE ID (mesin, merchandise, sparepart, dll)
            //$htmlOptions        = 'onChange="refresh_dropdown_options(this, \'#supplier_select\', \''.site_url("purchase_request/getSupplier").'\')" class="form-control select2" data-placeholder="Select '.transaksi::getPrType($itemtype_id).'" '. ($status_delete==1 ? 'disabled' : '');

/*            $htmlOptions        = 'onChange="refreshList(this)" target="#supplier_select" data-source="'.
                                site_url("purchase_request/getSupplier").'" id="list_item_id" class="form-control select2" data-placeholder="Select '.transaksi::getPrType($itemtype_id).'" '. ($status_delete==1 ? 'disabled' : '');

            $param['list_item']  = form_dropdown('data[item_id]', $this->item_model->options_empty($itemtype_id), $row->item_id, $htmlOptions);
*/

            $param['list_item'] = '<input type="text" id="pr_list_item_id" name="data[item_id]" value="'.$row->item_id.'" target="#supplier_select" class="form-control" onChange="refreshList(this)" data-source="'.
                                site_url("purchase_request/getSupplier").'"/>';

            $htmlOptions        = 'id="supplier_select" class="form-control select2" data-placeholder="Select Supplier" '. ($status_delete==1 ? 'disabled' : '');
            $param['list_supplier']  = form_dropdown('data[supplier_id]', $this->item_supplier_model->options_empty($row->item_id), $row->supplier_id, $htmlOptions);

            if($status_delete == 0){
                $param['form_action']    = view::form_input_detail($id);
            }else{
                $param['title']          = 'DELETE ITEM';
                $param['form_action']    = view::form_delete_detail($id);
                $form->type = 'delete';
            }

            $form->portlet          = true;
            $form->button_back      = true;
            $form->id               = 'form-pr-detail-add';
            $form->param            = $param;
            $form->data_source      = site_url($this->class_name.'/table_detail/'.$pr_id);
            $form->url_form         = $this->class_name.'/form_detail.tpl';
            
            echo $form->generate();
        } else {
            $this->access_right->redirect();
        }
    }
    
    public function getSupplier($itemId){
    	$this->load->model("item_supplier_model");
    	$options = $this->item_supplier_model->options_empty($itemId);
    	
    	$result = "";
    	foreach ($options as $key => $value) {
    		$result .= "<option value='".$key."'>".$value."</option>";
    	}
    	echo $result;
    }

    public function edit_detail($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('edit', true);
        $this->add_detail('', $id);
    }

    public function delete_detail($pr_id, $id_enc) {        
        $id = url_base64_decode($id_enc);
        $this->access_right->otoritas('delete', true);
        $this->add_detail($pr_id, $id, 1);
    }

    public function proses_detail() {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {

            $this->form_validation->set_rules('data[pr_id]', 'Purchase Request Doc', 'required|trim');
            $this->form_validation->set_rules('data[item_id]', 'Item', 'required|trim');
            $this->form_validation->set_rules('data[qty_temp]', 'Qty', 'required|trim');
            $this->form_validation->set_rules('data[supplier_id]', 'Supplier', 'required|trim');

            if ($this->form_validation->run()) {
                $message = array('success'=>false, 'message'=>'Proses penyimpanan data gagal.');

                $id     = $this->input->post('id');
                $data   = $this->input->post('data');
                $this->db->trans_start();
                $this->load->model(array('item_supplier_model', 'purchase_request_detail_model'));

                if (!$id) {
                    $item_id    = $data['item_id'];

                    $pr     = $this->purchase_request_model->getById($pr_id)->row();
                    $item   = $this->item_supplier_model
                            	->where('item_id', $item_id)
                            	->where('supplier_id', $data['supplier_id'])
                            	->get()->row();
                   
                    // insert to table transaction_detail
                    $detail['pr_id']            = $data['pr_id'];
                    $detail['transaction_id']   = $pr->transaction_id;
                    $detail['price']            = $item->price;
                    $detail['qty_request']      = $data['qty_temp'];
                    $detail['qty_temp']         = $data['qty_temp'];
                    $detail['item_id']          = $data['item_id'];
                    $detail['supplier_id']      = $data['supplier_id'];

                    $this->purchase_request_detail_model->create($detail);
                    
                    
                } else {
                    $detail['qty_request']  = $data['qty_temp'];
                    $detail['qty_temp']     = $data['qty_temp'];
                    $this->purchase_request_detail_model->update($detail, $id);
                }
                
                if($this->db->trans_status()){
                    $message = array('success'=>true, 'message'=>'Save Success', 'callback'=>'loadNewCustomTable("#div-table-pr-detail")');
                }else{
                    $message = array('success'=>false, 'message'=>'Database Error');
                }
                $this->db->trans_complete();
            } else {
                $message = array('success'=>false, 'message'=>validation_errors());
            }
            echo json_encode($message);
        } else {
            $this->access_right->redirect();
        }
    }

    public function proses_delete_detail() {
        $id = $this->input->post('id');

        if(!$id) {
            $message = array('success'=>false, 'message'=>'Id Required');
            echo json_encode($message);
            return;
        }

        $this->access_right->otoritas('delete', true);
        if ($this->access_right->otoritas('delete', true)) {

            $this->db->trans_start();
            $this->purchase_request_detail_model->delete($id);
            if($this->db->trans_status()){
                $message = array('success'=>true, 'message'=>'Save Success', 'callback'=>'loadNewCustomTable("#div-table-pr-detail")');
            }else{
                $message = array('success'=>false, 'message'=>'Database Error');
            }
            $this->db->trans_complete();
        }
        echo json_encode($message);
    }

    public function proceed($id = 0, $status = 0){
        /*
            memproses PR.
            dari draft - pengajuan
            dari revisi - pengajuan

        */
        //print_r($this->session->userdata('user'));
        //$this->output->enable_profiler(TRUE);

        // should be form validation

        // id_pr yang akan dirubah statusnya
        $return = $this->purchase_request_model->proceed($id, $status);
        return $return;
    }

    public function getCode($storeId = NULL){
        if (!$storeId)
            return;
        echo code::getCode('pr', $storeId);
    }

    public function getCodeReturn($storeId = NULL){
        if (!$storeId)
            return;
        return code::getCode('pr', $storeId);
    }

    public function getItemsByIdAndKeyword($pr_id='', $keyword = ''){
        $this->load->model(array('item_model','purchase_request_model'));

        $pr             = $this->purchase_request_model->getById($pr_id)->row();
        $itemtype_id    = $pr->pr_type;
        
        $tmp = $this->item_model->getItemsByIdAndKeyword($itemtype_id,$keyword);
        //$html_result = '<ul class="select2-results" role="listbox" id="select2-results-1">';
        $html_result = '';
        foreach ($tmp as $key => $value) {
             if($key!=''){
                $html_result .= '<li class="select2-results-dept-0 select2-result select2-result-selectable" role="presentation">
                                <div class="select2-result-label" id="select2-result-label-'.$key.'" role="option">'.$value.'</div></li>';
             }
        }
        echo $html_result;
    }

    public function getItemsByIdAndKeywordJSON($pr_id=''){
        $this->load->model(array('item_model','purchase_request_model'));

        $pr             = $this->purchase_request_model->getById($pr_id)->row();
        $itemtype_id    = $pr->pr_type;
        
        $tmp = $this->item_model->getItemsByIdAndKeyword($itemtype_id);
        
        //$tmp = $this->item_model->getItemsByIdAndKeyword(2,'');
        $arr = array();
        foreach ($tmp as $k => $v) {
            if($k != ''){
                $obj = new StdClass();
                $obj->id        = $k;
                $obj->text      = $tmp[$k];
                array_push($arr, $obj);
            }
        }
        echo json_encode($arr);
    }



}
/* End of file bbm.php */
/* Location: ./application/controllers/bbm.php */

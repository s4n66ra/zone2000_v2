<?php

/**
 * Description of roles
 *
 * @author Warman Suganda
 */
class roles extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    function index() {
        $data['title'] = 'Manajemen Role';
        $data['data_source'] = base_url($this->class_name . '/load');

        /* INSERT LOG */
        $this->access_right->activity_logs('view','Management Role');
        /* END OF INSERT LOG */
        
        
        $data['assets_url'] = $this->config->item('assets_url');
        $data['content'] = 'base/page_content.tpl';
        $data['nama_pegawai'] = $this->currentUsername;  
        $button_group    = array(
                                view::button_add(array('onclick'=>'btnLoadNextPage(this)'))
                            );
        $data['button_group'] = view::render_button_group($button_group);

        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'base/page_content.tpl';
        $this->twig->display('index.tpl', $data);
    }
    
    public function load($page = 0) {
        $this->load->library("custom_table");

        $table = new stdClass();
        $header[0] = array("Nama Role", 1, 1, "Deskripsi", 1, 1, "Aksi", 1, 1);
        $table->header = $header;
        $table->align = array('nik' => 'center', 'tanggal' => 'center', 'menu_urutan' => 'center', 'action' => 'center');
        $table->style = "table table-striped table-bordered table-hover datatable dataTable";
        $table->numbering = true;

        $table->model = $this->class_name . '_model->data_table';
        $table->limit = $this->limit;
        $table->page = $page;
        $data = $this->custom_table->generate_ajax($table);

        echo json_encode($data);
    }

    public function add($id = '',$status_delete = 0) {
        if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->load->model('menu_group_model');
            $this->load->model('menu_model');

            $title = 'Tambah Data';
            $data['form_action'] = $this->class_name . '/proses';

            $menu_group = $this->menu_group_model->get_data();
            $menu = $this->menu_model->parsing_data();

            $data['id'] = $id;
            $roles = array(
                'is_view' => array(), 'is_add' => array(), 'is_edit' => array(), 'is_delete' => array(), 'is_approve' => array(), 'is_import' => array(), 'is_print' => array()
            );
            if ($id != '') {
                $title = 'Edit Data';

                $db = $this->roles_model->get_by_id($id);
                $roles = $this->roles_model->parsing_roles(array('grup_id' => $id));
                if ($db->num_rows() > 0) {
                    $row = $db->row();
                    $default = $row;
                    $data['def_grup_nama'] = $row->grup_nama;
                    $data['def_grup_deskripsi'] = $row->grup_deskripsi;
                    $roles = $roles;
                }
            }
            $table = '';

            $table .= '<table id="table_list_pegawai" class="table table-bordered table-hover datatable dataTable">
            <thead>
                <tr>
                    <th>'.form_checkbox('cb_select_menu', '', false, 'id="cb_select_menu" target-selected="cb_select" onchange="select_all(this.id)"').'</th>
                    <th>Nama Menu</th>
                    <th>View</th>
                    <th>Add</th>
                    <th>Edit</th>
                    <th>Delete</th>
                    <th>Approve</th>
                    <th>Import</th>
                    <th>Export</th>
                </tr>
            </thead> ';
            foreach ($menu_group->result() as $group) :
            $table .= '<tr id="default_row_select">
                    <td align="center"><img style="cursor: pointer;" src="'.base_url() . 'images/bawah.png'.'" onclick="qslide('.$group->kd_grup_menu.');" /></td>
                    <td colspan="8" style="font-weight: bold;font-size: 12px;">'.$group->nama_grup_menu.'</td>
                </tr>';
                $table .= '<tbody id="menu_group_'.$group->kd_grup_menu.'">';
                    if (isset($menu[$group->kd_grup_menu])) :
                        if (isset($menu[$group->kd_grup_menu][0])) :
                            foreach ($menu[$group->kd_grup_menu][0] as $value) :
                                $table .= '<tr>
                                    <td align="center"><i class="icon-menu"></i></td>
                                    <td style="color: blue;font-weight: bold;">'.$value['nama_menu'].'</td>
                                    <td align="center">'.form_checkbox('otoritas_menu_view[]', $value['kd_menu'], in_array($value['kd_menu'], $roles['is_view']) ? true : false, 'class="cb_select" target-selected="cb_select_baris_' . $value['kd_menu'] . '" id="cb_select_menu_' . $value['kd_menu'] . '" onchange="select_all(this.id)" ').'</td>
                                    <td align="center">'.form_checkbox('otoritas_menu_add[]', $value['kd_menu'], in_array($value['kd_menu'], $roles['is_add']) ? true : false, 'class="cb_select cb_select_baris_' . $value['kd_menu'] . '"').'</td>
                                    <td align="center">'.form_checkbox('otoritas_menu_edit[]', $value['kd_menu'], in_array($value['kd_menu'], $roles['is_edit']) ? true : false, 'class="cb_select cb_select_baris_' . $value['kd_menu'] . '"').'</td>
                                    <td align="center">'.form_checkbox('otoritas_menu_delete[]', $value['kd_menu'], in_array($value['kd_menu'], $roles['is_delete']) ? true : false, 'class="cb_select cb_select_baris_' . $value['kd_menu'] . '"').'</td>
                                    <td align="center">'.form_checkbox('otoritas_menu_approve[]', $value['kd_menu'], in_array($value['kd_menu'], $roles['is_approve']) ? true : false, 'class="cb_select cb_select_baris_' . $value['kd_menu'] . '"').'</td>
                                    <td align="center">'.form_checkbox('otoritas_menu_export[]', $value['kd_menu'], in_array($value['kd_menu'], $roles['is_import']) ? true : false, 'class="cb_select cb_select_baris_' . $value['kd_menu'] . '"').'</td>
                                    <td align="center">'.form_checkbox('otoritas_menu_print[]', $value['kd_menu'], in_array($value['kd_menu'], $roles['is_print']) ? true : false, 'class="cb_select cb_select_baris_' . $value['kd_menu'] . '"').'</td>
                                </tr>';

                                if (isset($menu[$group->kd_grup_menu][$value['kd_menu']])) :
                                    foreach ($menu[$group->kd_grup_menu][$value['kd_menu']] as $value2) :
                                        $table .= '<tr>
                                            <td style="padding-right: 16px;" align="right"><i class="icon-menu"></i></td>
                                            <td style="font-weight: bold;">'.$value2['nama_menu'].'</td>
                                            <td align="center">'.form_checkbox('otoritas_menu_view[]', $value2['kd_menu'], in_array($value2['kd_menu'], $roles['is_view']) ? true : false, 'class="cb_select" target-selected="cb_select_baris_' . $value2['kd_menu'] . '" id="cb_select_menu_' . $value2['kd_menu'] . '" onchange="select_all(this.id)" ').'</td>
                                            <td align="center">'.form_checkbox('otoritas_menu_add[]', $value2['kd_menu'], in_array($value2['kd_menu'], $roles['is_add']) ? true : false, 'class="cb_select cb_select_baris_' . $value2['kd_menu'] . '"').'</td>
                                            <td align="center">'.form_checkbox('otoritas_menu_edit[]', $value2['kd_menu'], in_array($value2['kd_menu'], $roles['is_edit']) ? true : false, 'class="cb_select cb_select_baris_' . $value2['kd_menu'] . '"').'</td>
                                            <td align="center">'.form_checkbox('otoritas_menu_delete[]', $value2['kd_menu'], in_array($value2['kd_menu'], $roles['is_delete']) ? true : false, 'class="cb_select cb_select_baris_' . $value2['kd_menu'] . '"').'</td>
                                            <td align="center">'.form_checkbox('otoritas_menu_approve[]', $value2['kd_menu'], in_array($value2['kd_menu'], $roles['is_approve']) ? true : false, 'class="cb_select cb_select_baris_' . $value2['kd_menu'] . '"').'</td>
                                            <td align="center">'.form_checkbox('otoritas_menu_export[]', $value2['kd_menu'], in_array($value2['kd_menu'], $roles['is_import']) ? true : false, 'class="cb_select cb_select_baris_' . $value2['kd_menu'] . '"').'</td>
                                            <td align="center">'.form_checkbox('otoritas_menu_print[]', $value2['kd_menu'], in_array($value2['kd_menu'], $roles['is_print']) ? true : false, 'class="cb_select cb_select_baris_' . $value2['kd_menu'] . '"').'</td>
                                        </tr>';
                                        if (isset($menu[$group->kd_grup_menu][$value2['kd_menu']])) :
                                            $total_menu_3 = count($menu[$group->kd_grup_menu][$value2['kd_menu']]);
                                            $idx_menu_3 = 0;
                                            foreach ($menu[$group->kd_grup_menu][$value2['kd_menu']] as $value3) :
                                                $idx_menu_3++;
                                                $id3 = $value3['kd_menu'];
                                                $group_menu_3_up = '';
                                                $group_menu_3_down = '';
                                                if ($idx_menu_3 == 1) {
                                                    $group_menu_3_up = 'disabled="disabled"';
                                                }
                                                if ($idx_menu_3 == $total_menu_3) {
                                                    $group_menu_3_down = 'disabled="disabled"';
                                                }
                                                $table .= '<tr>
                                                    <td align="right"><i class="icon-menu"></i></td>
                                                    <td>'.$value3['nama_menu'].'</td>
                                                    <td align="center">'.form_checkbox('otoritas_menu_view[]', $value3['kd_menu'], in_array($value3['kd_menu'], $roles['is_view']) ? true : false, 'class="cb_select" target-selected="cb_select_baris_' . $value3['kd_menu'] . '" id="cb_select_menu_' . $value3['kd_menu'] . '" onchange="select_all(this.id)" ').'</td>
                                                    <td align="center">'.form_checkbox('otoritas_menu_add[]', $value3['kd_menu'], in_array($value3['kd_menu'], $roles['is_add']) ? true : false, 'class="cb_select cb_select_baris_' . $value3['kd_menu'] . '"').'</td>
                                                    <td align="center">'.form_checkbox('otoritas_menu_edit[]', $value3['kd_menu'], in_array($value3['kd_menu'], $roles['is_edit']) ? true : false, 'class="cb_select cb_select_baris_' . $value3['kd_menu'] . '"').'</td>
                                                    <td align="center">'.form_checkbox('otoritas_menu_delete[]', $value3['kd_menu'], in_array($value3['kd_menu'], $roles['is_delete']) ? true : false, 'class="cb_select cb_select_baris_' . $value3['kd_menu'] . '"').'</td>
                                                    <td align="center">'.form_checkbox('otoritas_menu_approve[]', $value3['kd_menu'], in_array($value3['kd_menu'], $roles['is_approve']) ? true : false, 'class="cb_select cb_select_baris_' . $value3['kd_menu'] . '"').'</td>
                                                    <td align="center">'.form_checkbox('otoritas_menu_export[]', $value3['kd_menu'], in_array($value3['kd_menu'], $roles['is_import']) ? true : false, 'class="cb_select cb_select_baris_' . $value3['kd_menu'] . '"').'</td>
                                                    <td align="center">'.form_checkbox('otoritas_menu_print[]', $value3['kd_menu'], in_array($value3['kd_menu'], $roles['is_print']) ? true : false, 'class="cb_select cb_select_baris_' . $value3['kd_menu'] . '"').'</td>
                                                </tr>';
                                            endforeach;
                                        endif;
                                    endforeach;
                                endif;
                            endforeach;
                        endif;
                    endif;
                $table .= '</tbody>';
            endforeach; 
        $table .= '</table>';


            $button_group = array();
            $data['title'] = $title;
            if($status_delete == 0){
                $data['form_action'] = '<form action="'.base_url($this->class_name.'/proses').'" method="post" id="finput" class="form-horizontal" novalidate="novalidate"><input name = "id" type="hidden" value="'.$id.'">';
                $button_group[]         = view::button_save();
            }else{
                $data['title'] = 'Delete Data Pulau';
                /*$data['form_action'] = '<form action="'.base_url($this->class_name.'/proses_delete').'" method="post" id="form_sample_3" class="form-horizontal" novalidate="novalidate"><input name = "id" type="hidden" value="'.$id.'">';
                $button_group[]         = view::button_delete_confirm();*/

                //==============
                $data['form_action']    = view::form_delete($id);;
                $data['readonly']       = 'readonly=""';
                $button_group[]         = view::button_delete_confirm();
            }
            //$data['button_submit'] .= '<a href = "'.base_url($this->class_name).'" class="btn default">Cancel</a>';
            $data['button_group'] = view::render_button_group($button_group);
            $data['table'] = $table;
            $data['isBtnPageBack'] = true;
            $data['form'] = $this->class_name.'/form.tpl';
            $this->twig->display('base/page_form.tpl', $data);
        } else {
            $this->access_right->redirect();
        }
    }
    
    public function edit($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->add($id);
    }

    public function delete($id_enc) {
        $id = url_base64_decode($id_enc);
        $this->add($id,1);
    }

    public function proses() {
        //if ($this->access_right->otoritas('add') || $this->access_right->otoritas('edit')) {
            $this->form_validation->set_rules('nama_role', 'Nama Role', 'trim|required');

            if ($this->form_validation->run()) {
                $message = array(false, 'Proses gagal', 'Proses penyimpanan data gagal.', '');

                $id = $this->input->post('id');

                $data = array(
                    'grup_nama' => $this->input->post('nama_role'),
                    'grup_deskripsi' => $this->input->post('deskripsi'),
                    'otoritas_data' => $this->input->post('otoritas_data'),
                    'otoritas_menu_view' => $this->input->post('otoritas_menu_view'),
                    'otoritas_menu_add' => $this->input->post('otoritas_menu_add'),
                    'otoritas_menu_edit' => $this->input->post('otoritas_menu_edit'),
                    'otoritas_menu_delete' => $this->input->post('otoritas_menu_delete'),
                    'otoritas_menu_approve' => $this->input->post('otoritas_menu_approve'),
                    'otoritas_menu_export' => $this->input->post('otoritas_menu_export'),
                    'otoritas_menu_print' => $this->input->post('otoritas_menu_print')
                );

                /*
                 * If : $id == '', lakukan proses create data
                 * Else : lakukan proses update
                 */

                if ($id == '') {
                    if ($this->roles_model->create($data)) {
                        $insert_id = $this->session->userdata('insert_id');
						$this->access_right->activity_logs('add','Manajemen Role');
                        $message = array(true, 'Proses Berhasil', 'Proses penyimpanan data berhasil.', '');
                    }
                } else {
                    if ($this->roles_model->update($data, $id)) {
                        $insert_id = $id;
						$this->access_right->activity_logs('edit','Manajemen Role');
                        $message = array(true, 'Proses Berhasil', 'Proses update data berhasil.', '');
                    }
                }
            } else {
                $message = array(false, 'Terjadi Kesalahan', validation_errors(), '');
            }
            
            echo json_encode($message);
    }

    public function proses_delete() {
        $id = $this->input->post('id');
        if ($this->access_right->otoritas('delete', true)) {
            $message = array(false, 'Proses gagal', 'Proses hapus data gagal.', '');
            if ($this->roles_model->delete($id)) {
				/*$this->access_right->activity_logs('delete','Manajemen Role');
                $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', '#content_table');*/

                //=============
                $message = array(true, 'Proses Berhasil', 'Proses hapus data berhasil.', 'refresh_filter()');
                /* INSERT LOG */
                $this->access_right->activity_logs('delete','Delete BBM');
            }
            echo json_encode($message);
        }
    }

}

/* End of file roles.php */
/* Location: ./application/controllers/roles.php */
<?php

/**
 * Description of Divisi
 *
 * @author SANGGRA HANDI
 */
class lap_receiving_dc extends My_Controller {


    public function __construct() {
        parent::__construct();
        $this->load->model('lap_pegawai_model');
    }

    
    public function index() {
        $data['title'] = 'Laporan Penerimaan Barang DC ';

        /* INSERT LOG */
        //$this->access_right->activity_logs('view',$data['title']);
        /* END OF INSERT LOG */

        //$data['button_group'] = $this->getAvailableButtons();
        $data['button_right'] = $this->getPrintTools();

        $data['table']['main']  = $this->table_main(array('wrapper', 'filter'));
        $data['sidebar'] = $this->access_right->menu();
        $data['content'] = 'base/page_content.tpl';
    
        $this->twig->display('index.tpl', $data);
    }

    public function table_main($option = array(),$st_print = FALSE){
        $this->load->model('koli_model');
        $this->load->library('DatatablelapSDM');
        $table = $this->datatablelapsdm;

        /*$this->load->library('Datatable_lapSDM');
        $table = $this->datatable_lapSDM;*/

        if (in_array('filter', $option)) {
            $table->dataFilter = array(
                                array('koli_code','KOLI CODE','text'),
                                array('nama_cabang', 'STORE', 'text'),
                                array('kd_cabang', 'KD STORE', 'text'),
                                array('note', 'NOTE', 'text'),
                                array('item_name', 'ITEM', 'text'),
                                array('nama_supplier', 'SUPPLIER', 'text'),
                            );
        }

        if (in_array('wrapper', $option)) {
            $table->id          = 'table-stok-warehouse';
            $table->isScrollable= false;
            $table->numbering   = true;
            $table->header      = array('KOLI CODE','STORE', 'QTY KOLI', 'NOTE','ITEM','QTY','SUPPLIER', 'STATUS', 'ACTION');
            $table->source      = site_url($this->class_name.'/table_main');
            return $table->generateWrapper();
        } else {
           $table
                ->setModel($this->koli_model)
                ->setNumbering()
                ->with(array('store','supplier','postore_koli','po_detail','item'))
                ->select('koli_code, nama_cabang, qty, note,item_name,dc_qty_received,nama_supplier,koli_status, this.koli_id as ki')
                ->order_by('koli_code')
                ->where_in('itemtype_id',hprotection::getDcItem())
                ->edit_column('koli_status','$1','prstatus::getStatus(koli_status)')
                ->edit_column('ki', '$1', "view::btn_print_barcode_koli(ki)");
            if($st_print == TRUE)
                return $table->getData();
            else
                echo $table->generate();
        }

    }

    

    public function getPrintTools(){
        $button_group = array();
        if($this->access_right->otoritas('print')){
            $button_group[] = view::button_export_laporan();
        }
        
        if(true){//export
            //$button_group[] = view::button_export();
        }
        return view::render_button_group_laporan_penjualan($button_group, array(), true);

    }

    public function excel($store_id=''){

        $this->load->model(array('store_model','laporan_payout_model'));  
        /*$this->load->library('DatatablelapSDM');
        $table = $this->datatablelapsdm;
        $table  ->setModel($this->lap_pegawai_model)
                ->with(array('divisi','level'))
                ->setNumbering()
               // ->order_by('no_urut','ASC')
                ->order_by('this.id_divisi','DESC')
                ->select('nama_pegawai,nik,id_karyawan,kd_level,nama_divisi');
        $dataTable = $table->getData();    */  
        $filter = array();

        $data['filter'] = $filter;
        //----------------------------------------------------
        $date = date('Ymd His');
        $header =array('NAME','ID','NIK','LEVEL');

        $data['header'] = $header;
        $data['judul_kecil']    = 'Laporan Pegawai Pusat';
        $data['filename']       = 'Laporan Pegawai Pusat('.$date.')';
        $data['content']        = $this->table_main(array(),TRUE);// $this->laporan_payout_model->data_detail(array("id_cabang" => $store_id));
        $data['selected_store'] = $this->store_model
                                 ->select('nama_cabang')
                                 ->where(array("id_cabang"=>$store_id))
                                 ->get()
                                 ->row();
        //print_r($data['selected_store']);
        $this->load->view('cetak_excel_lap_pegawai',$data);
    
    }  

/*    public function getItemsByIdAndKeywordJSON($itemtype_id=''){
        $this->load->model(array('item_model'));

        $tmp = $this->item_model->getItemsByIdAndKeyword($itemtype_id);
        
        //$tmp = $this->item_model->getItemsByIdAndKeyword(2,'');
        $arr = array();
        foreach ($tmp as $k => $v) {
            if($k != ''){
                $obj = new StdClass();
                $obj->id        = $k;
                $obj->text      = $tmp[$k];
                array_push($arr, $obj);
            }
        }
        echo json_encode($arr);
    }*/





}

/* End of file divisi.php */
/* Location: ./application/controllers/bbm.php */

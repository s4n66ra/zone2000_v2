<div class="form-body" style="margin-bottom: 10px">
	<h3 class="form-section"></h3>
	<div class="alert alert-danger display-hide">
		<button class="close" data-close="alert"></button>
		You have some form errors. Please check below.
	</div>
	<!-- BEGIN FORM-->
	{{ form_action|raw }}
	<div class="form-body">
		<h3 class="form-section"></h3>
		<div class="alert alert-danger display-hide">
			<button class="close" data-close="alert"></button>
			You have some form errors. Please check below.
		</div>
		<div class="alert alert-success display-hide">
			<button class="close" data-close="alert"></button>
			Your form validation is successful!
		</div>
		<div class="form-group row">
			<label class="control-label col-md-4">Level Code<span class="required"> * </span>
			</label>
			<div class="col-md-6">
				<input type="text" name="kd_level" data-required="1" class="form-control" value="{{def_kd_level}}" />
			</div>
		</div>
		<div class="form-group row">
			<label class="control-label col-md-4">Level Name<span class="required"> * </span>
			</label>
			<div class="col-md-6">
				<input type="text" name="nama_level" value="{{def_nama_level}}" data-required="1" class="form-control" />
			</div>
		</div>
		<div class="form-group row">
			<label class="control-label col-md-4">Status Head SDM Only<span class="required"> * </span>
			</label>
			<div class="col-md-6">
				{{head_sdm_only|raw}}
			</div>
		</div>


	</div>
	<div class="form-actions">
		<div class="row">
			<div class="col-md-offset-3 col-md-9">{{button_submit|raw}}</div>
		</div>
	</div>
	</form>
	<!-- END FORM-->
</div>
<!-- END PAGE CONTENT-->
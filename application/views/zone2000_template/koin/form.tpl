{{ form_action|raw }}

    <div class="form-body">
        <h3 class="form-section"></h3>
        <div class="alert alert-danger display-hide">
            <button class="close" data-close="alert"></button>
            You have some form errors. Please check below.
        </div>
        <div class="alert alert-success display-hide">
            <button class="close" data-close="alert"></button>
            Your form validation is successful!
        </div>
        <div class="row">
                <div class="form-group">
                    <label class="control-label col-md-3">Coin Code<span class="required">
                    * </span>
                    </label>
                    <div class="col-md-8">
                        <input type="text" name="kd_koin" data-required="1" class="form-control" value="{{data.kd_koin}}"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3">Coin Name<span class="required">
                    * </span>
                    </label>
                    <div class="col-md-8">
                        <input type="text" name="nama_koin" value = "{{data.nama_koin}}" data-required="1" class="form-control"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3">Coin Price<span class="required">
                    * </span>
                    </label>
                    <div class="col-md-8">
                        <input type="text" name="price" value = "{{data.price}}" data-required="1" class="form-control"/>
                    </div>
                </div>

            
        </div>
    </div>
</form>
<script>
    jQuery(document).ready(function(){ 
        if ($().select2) {
            $('.select2me').select2({
                placeholder: "Select",
                allowClear: true
            });
        }

        if ($().datepicker) {
            $('.date-picker').datepicker({
                rtl: Metronic.isRTL(),
                orientation: "left",
                autoclose: true
            });
            //$('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
        }
    });
</script>
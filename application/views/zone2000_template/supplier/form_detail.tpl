<!-- <table class="table table-advance table-hover" id="table-kalkulator">
	<colgroup><col width="30%"><col width="30%"><col width="30%"><col width="10%">
	</colgroup>
	<thead>
	<tr>
	<th class="cell-first">INPUT JUMLAH TIKET</th><th class="">SISA</th><th class="">TOTAL</th>
	</tr>

	</thead>
	<tbody>
		<tr>
			<td>
				{{list.item|raw}}	
			</td>	
			<td>
				<span class="input-group-addon">
				Rp
				</span>
				<input type="text" name="data[price]" data-required="1" class="form-control format-number" value="{{ data.price }}" {{readonly}}/>
			</td>
			<td>
				{{ button_group_inform|raw }}							
			</td>
		</tr>
	</tbody>
</table> -->
<div class="form-group row">
	<div class="col-md-3">
		Item Name
	</div>
	<div class="col-md-3">
		Harga / Unit
	</div>
	<div class="col-md-3">
		Harga / Pax
	</div>
	<div class="col-md-2">
		Diskount
	</div>
<!-- 	<div class="col-md-2">
		Total Harga
	</div> -->
	<div class="col-md-1">
		
	</div>
</div>

<div class="form-group row">
	<div class="col-md-3">
		{{list.item|raw}}
	</div>
	<div class="col-md-3">
		<div class="input-group">
			<span class="input-group-addon">
				Rp
			</span>
			<input type="text" name="data[price]" data-required="1" class="form-control format-number" value="{{ data.price }}" {{readonly}}/>
		</div>
	</div>
	<div class="col-md-3">
		<div class="input-group">
			<span class="input-group-addon">
				Rp
			</span>
			<input type="text" name="data[sup_price_pax]" data-required="1" class="form-control format-number" value="{{ data.sup_price_pax }}" {{readonly}}/>
		</div>
	</div>
	<div class="col-md-2">
		<div class="input-group">
			<span class="input-group-addon">
				%
			</span>
			<input type="text" name="data[reg_diskon]" data-required="1" class="form-control format-number" value="{{ data.reg_diskon }}" {{readonly}}/>
		</div>
	</div>
<!-- 	<div class="col-md-2">
		<div class="input-group">
			<span class="input-group-addon">
				Rp
			</span>
			<input type="text" name="data[final_price]" data-required="1" class="form-control format-number" value="{{ data.final_price }}" {{readonly}}/>
		</div>
	</div> -->
	<div class="col-md-1">
		{{ button_group_inform|raw }}
	</div>
</div>

<script type="text/javascript">
	
</script>
{{ form_action|raw }}
	<div class="form-body">
		<h3 class="form-section"></h3>
		<div class="alert alert-danger display-hide">
			<button class="close" data-close="alert"></button>
			You have some form errors. Please check below.
		</div>
		<div class="alert alert-success display-hide">
			<button class="close" data-close="alert"></button>
			Your form validation is successful!
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group row">
					<label class="control-label col-md-4">Supplier Code <span class="required">
					* </span>
					</label>
					<div class="col-md-8">
						<div class="input-icon right">
							<i class="fa"></i>
							<input type="text" name="data[kd_supplier]" data-required="1" class="form-control" value="{{data.kd_supplier}}" {{readonly}}/>
						</div>
					</div>
				</div>
				<div class="form-group row">
					<label class="control-label col-md-4">Supplier Name <span class="required">
					* </span>
					</label>
					<div class="col-md-8">
						<div class="input-icon right">
							<i class="fa"></i>
							<input type="text" name="data[nama_supplier]" value = "{{data.nama_supplier}}" data-required="1" class="form-control" {{readonly}}/>
						</div>
					</div>
				</div>
				<div class="form-group row">
					<label class="control-label col-md-4">Unit Business <span class="required">
					* </span>
					</label>
					<div class="col-md-8">
						<div class="input-icon right">
							<i class="fa"></i>
							{{options_unitBusiness|raw}}
						</div>
					</div>
				</div>
				<div class="form-group row">
					<label class="control-label col-md-4">Divisi <span class="required">
					* </span>
					</label>
					<div class="col-md-8">
						<div class="input-icon right">
							<i class="fa"></i>
							{{list_divisi|raw}}
						</div>
					</div>
				</div>
				<div class="form-group row">
					<label class="control-label col-md-4">Owner <span class="required">
					* </span>
					</label>
					<div class="col-md-8">
						<div class="input-icon right">
							<i class="fa"></i>
							<input type="text" name="data[pemilik]" value = "{{data.pemilik}}" data-required="1" class="form-control" {{readonly}}/>
						</div>
					</div>
				</div>
				<div class="form-group row">
					<label class="control-label col-md-4">Telp <span class="required">
					* </span>
					</label>
					<div class="col-md-8">
						<div class="input-icon right">
							<i class="fa"></i>
							<input type="text" name="data[telp]" value = "{{data.telp}}" data-required="1" class="form-control" {{readonly}}/>
						</div>
					</div>
				</div>
				<div class="form-group row">
					<label class="control-label col-md-4">Fax <span class="required">
					* </span>
					</label>
					<div class="col-md-8">
						<div class="input-icon right">
							<i class="fa"></i>
							<input type="text" name="data[fax]" value = "{{data.fax}}" data-required="1" class="form-control" {{readonly}}/>
						</div>
					</div>
				</div>
				<div class="form-group row">
					<label class="control-label col-md-4">Supplier Address <span class="required">
					* </span>
					</label>
					<div class="col-md-8">
						<div class="input-icon right">
							<i class="fa"></i>
							<input type="text" name="data[alamat_supplier]" value = "{{data.alamat_supplier}}" data-required="1" class="form-control" {{readonly}}/>
						</div>
					</div>
				</div>
				<div class="form-group row">
					<label class="control-label col-md-4">NPWP <span class="required">
					* </span>
					</label>
					<div class="col-md-8">
						<div class="input-icon right">
							<i class="fa"></i>
							<input type="text" name="data[npwp]" value = "{{data.npwp}}" data-required="1" class="form-control" {{readonly}}/>
						</div>
					</div>
				</div>
				<div class="form-group row">
					<label class="control-label col-md-4">Note <span class="required">
					* </span>
					</label>
					<div class="col-md-8">
						<div class="input-icon right">
							<i class="fa"></i>
							<textarea name="data[description]" class="form-control">{{data.description}}</textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group row">
					<label class="control-label col-md-4">User Name </label>
					<div class="col-md-8">
						<input type="text" name="data[user_name]" value="{{data.user_name}}" data-required="1" class="form-control" />
						<input type="hidden" name="data[user_id]" value="{{data.user_id}}" />
					</div>
				</div>
				<div class="form-group row">
					<label class="control-label col-md-4">Password </label>
					<div class="col-md-8">
						<input name="data[user_password]" data-required="1" class="form-control" type="password" /> {{additional_info|raw}}
					</div>
				</div>
				<div class="form-group row">
					<label class="control-label col-md-4">Confirm Password </label>
					<div class="col-md-8">
						<input name="data[user_confirm_password]" data-required="1" class="form-control" type="password" /> {{additional_info|raw}}
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
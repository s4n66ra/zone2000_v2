<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-wrench"></i>
			<span class="caption-subject font-blue-hoki bold uppercase">Machine Reparation</span>
		</div>
		<div class="tools">
			<a href="" class="collapse">
			</a>
		</div>
	</div>
	<div class="portlet-body form">

		<div class="form-group row">
			<label class="control-label col-md-4">Machine
			</label>
			<div class="col-md-8">
				<input type="text" name="mesin[kd_mesin]" value="{{mesin.kd_mesin}}" readonly="" class="form-control">
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-md-4">Repair Date
			</label>
			<div class="col-md-8">
				<div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd">
					<input type="text" name="mesin[tgl_mulai]" class="form-control" value="{{mesin.tgl_mulai}}" readonly>
					<span class="input-group-btn">
						<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
					</span>
				</div>
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-md-4">Target Finish
			</label>
			<div class="col-md-8">
				<div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd">
					<input type="text" name="mesin[tgl_target]" class="form-control" value="{{mesin.tgl_target}}" readonly>
					<span class="input-group-btn">
						<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
					</span>
				</div>
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-md-4">Problem<span class="required">
			* </span>
			</label>
			<div class="col-md-8">
				<textarea type="text" name="mesin[deskripsi]" data-required="1" rows="3" class="form-control" readonly>{{ mesin.deskripsi }}</textarea>
			</div>
		</div>

	</div>
</div>
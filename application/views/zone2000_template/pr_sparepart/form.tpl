{{ form_action|raw }}
	<div class="form-body col-md-12">
		<h3 class="form-section"></h3>
		<div class="alert alert-danger display-hide">
			<button class="close" data-close="alert"></button>
			You have some form errors. Please check below.
		</div>
		<div class="alert alert-success display-hide">
			<button class="close" data-close="alert"></button>
			Your form validation is successful!
		</div>

		<input type="hidden" name="status" value="{{status}}" />

		<div class="row">
			<div class="col-md-5">
				<div class="form-group row">
					<label class="control-label col-md-4">Code PR <span class="required">
					* </span>
					</label>
					<div class="col-md-8">
						<input type="text" name="data[kd_pr]" data-required="1" class="form-control" value="{{ data.kd_pr }}" {{readonly}}/>
					</div>
				</div>

				<div class="form-group row">
					<label class="control-label col-md-4">Note<span class="required">
					* </span>
					</label>
					<div class="col-md-8">
						<textarea type="text" name="data[keterangan]" data-required="1" rows="3" class="form-control" {{readonly}}>{{ data.keterangan }}</textarea>
					</div>
				</div>

				<div class="form-group row">
					<label class="control-label col-md-4">Reparation				
					</label>
					<div class="col-md-8">
						{{ list_mesin_rusak|raw }}
					</div>
				</div>

			</div>

			<div class="col-md-7" >
				
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-wrench"></i>
							<span class="caption-subject font-blue-hoki bold uppercase">Machine Reparation</span>
						</div>
						<div class="tools">
							<a href="" class="collapse">
							</a>
						</div>
					</div>
					<div class="portlet-body" id="div-kerusakan" data-source="{{ data_source_kerusakan }}">
						{{ form_kerusakan | raw }}
					</div>
				</div>

			</div>

		</div>



	</div>
</form>



<script>
	jQuery(document).ready(function(){ 
		if ($().select2) {

            $('#list-mesin-rusak').select2({
                placeholder: "Select",
                allowClear: true
            }).on('change',function(){
            	var url = $('#div-kerusakan').attr('data-source');
            	load_page('#div-kerusakan',url+'/'+$(this).val());
            });
        }

        if ($().datepicker) {
            $('.date-picker').datepicker({
                rtl: Metronic.isRTL(),
                orientation: "left",
                autoclose: true
            });
            //$('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
        }
	});
</script>
{{ form_action|raw }}
	<div class="form-body">
		
		<h3 class="form-section"></h3>
		<div class="alert alert-danger display-hide">
			<button class="close" data-close="alert"></button>
			You have some form errors. Please check below.
		</div>
		<div class="alert alert-success display-hide">
			<button class="close" data-close="alert"></button>
			Your form validation is successful!
		</div>
		
		<input type="hidden" name="data[id_pr]" value="{{ data.id_pr }}"/>

		<div class="form-group row">
			<label class="control-label col-md-4">Merchandise <span class="required">
			* </span>
			</label>
			<div class="col-md-8">
				{{ list_item|raw }}
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-md-4">Qty <span class="required">
			* </span>
			</label>
			<div class="col-md-8">
				<input type="text" name="data[jumlah]" data-required="1" class="form-control" value="{{ data.jumlah }}" {{readonly}}/>
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-md-4">Note<span class="required">
			* </span>
			</label>
			<div class="col-md-8">
				<textarea type="text" name="data[keterangan]" value = "{{data.keterangan}}" data-required="1" class="form-control" {{readonly}}>{{data.keterangan}}</textarea>
			</div>
		</div>
	</div>
</form>


<script>
	jQuery(document).ready(function(){ 
		if ($().select2) {
            $('.select2me').select2({
                placeholder: "Select",
                allowClear: true
            });
        }

        if ($().datepicker) {
            $('.date-picker').datepicker({
                rtl: Metronic.isRTL(),
                orientation: "left",
                autoclose: true
            });
        }
	});
</script>
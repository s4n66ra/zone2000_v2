{{ form_action|raw }}
	<div class="row">
		<div class="form-body col-md-12">
			<div class="form-group row">
				<label class="control-label col-md-4">DATE
				</label>
				<div class="col-md-8">
					
					<div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd">
					<input type="text" name="data[date]" class="form-control" value="{{data.date}}" > <span class="input-group-btn">
						<button class="btn default" type="button">
							<i class="fa fa-calendar"></i>
						</button>
					</span>
				</div>
				</div>
			</div>
			{% if is_head_office %}
			<div class="form-group row">
				<label class="control-label col-md-4">Outlet <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					{{list_store|raw}}
				</div>
			</div>
			{% else %}
				{{list_store|raw}}
			{% endif %}
			<div class="form-group row">
				<label class="control-label col-md-4">Item <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					{{list_item|raw}}
				</div>
			</div>
			<div class="form-group row">
				<label class="control-label col-md-4">Qty<span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="data[qty]" value = "{{data.qty}}" data-required="1" class="form-control format-number"/>
				</div>
			</div>

			<div class="form-group row">
				<label class="control-label col-md-4">Note <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<textarea type="text" name="data[note]" data-required="1" rows="3" class="form-control" >{{data.note}}</textarea>
				</div>
			</div>
		</div>
	</div>
</form>
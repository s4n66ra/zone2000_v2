	{{ form_action|raw }}
	<div class="form-body">
		<h3 class="form-section"></h3>
		<div class="alert alert-danger display-hide">
			<button class="close" data-close="alert"></button>
			You have some form errors. Please check below.
		</div>
		<div class="alert alert-success display-hide">
			<button class="close" data-close="alert"></button>
			Your form validation is successful!
		</div>

		<div class="form-group row">
			<label class="control-label col-md-4">Nama Menu <span class="required">
			* </span>
			</label>
			<div class="col-md-8">
				<input type="text" name="nama_menu" value = "{{def_nama_menu}}" data-required="1" class="form-control"/>
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-md-4">URL <span class="required">
			* </span>
			</label>
			<div class="col-md-8">
				<span class="add-on" style="font-size: 11px;font-weight: bold;"><i class="icon-globe"></i> {{base_url}}</span>
				<input type="text" name="url" value = "{{def_url}}" data-required="1" class="form-control"/>
			</div>
		</div>
		<div class="form-group row">
			<label class="control-label col-md-4">Deskripsi <span class="required">
			* </span>
			</label>
			<div class="col-md-8">
				<textarea name="deskripsi" rows = '4' data-required="1" class="form-control"/>{{data.deskripsi}}</textarea>
			</div>
		</div>
		<div class="form-group row">
			<label class="control-label col-md-4">Group Menu <span class="required">
			* </span>
			</label>
			<div class="col-md-8">
				{{options_group_menu|raw}}
			</div>
		</div>
		<div class="form-group row">
			<label class="control-label col-md-4">Parent <span class="required">
			* </span>
			</label>
			<div class="col-md-8">
				{{options_parent|raw}}
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-md-4">Icon <span class="required">
			* </span>
			</label>
			<div class="col-md-8">
				<input type="hidden" name="icon" value = "{{data.icon}}" data-required="1" class="form-control form-icon"/>
				{{button.icon|raw}}
			</div>
		</div>

	</div>

</form>

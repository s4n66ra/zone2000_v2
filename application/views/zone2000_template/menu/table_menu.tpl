<style type="text/css">
	.menu-container {
		padding: 10px;
	}

	.row-menu {
		border-bottom: 1px solid rgb(223, 223, 223);
		padding: 10px;
	}
</style>

<div class="menu-container">
	{% for i in range(1, 10) %}
	<div class="row-menu">
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-3">Menu {{i}}</div>
			<div class="col-md-3"></div>
			<div class="col-md-2"></div>
			<div class="col-md-3"></div>
		</div>
		<div class="row child">
			Anak nya
		</div>
		<div class="child" style="display:none;">
		</div>
	</div>
	{% endfor %}
</div>

<table id="table_list_pegawai" class="table table-bordered table-hover datatable dataTable">
    <thead>
        <tr>
            <th style="width: 50px;"><i class="icon-sitemap"></i></th>
            <th>Nama Menu</th>
            <th>Dekripsi</th>
            <th>URL</th>
            <th>Urutan</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <?php
    $total_group = $menu_group->num_rows();
    $idx_group = 0;
    foreach ($menu_group->result() as $group) :
        $idx_group++;
        $group_up = '';
        $group_down = '';
        if ($idx_group == 1) {
            $group_up = 'disabled="disabled"';
        } else if ($idx_group == $total_group) {
            $group_down = 'disabled="disabled"';
        }
        ?>
        <tr id="default_row_select">
            <td align="center"><img style="cursor: pointer;" src="<?php echo base_url() . 'images/bawah.png' ?>" onclick="qslide('<?php echo $group->kd_grup_menu; ?>');" /></td>
            <td colspan="5" style="font-weight: bold;font-size: 12px;"><i class="<?php echo $group->icon; ?>"></i> &nbsp; <?php echo $group->nama_grup_menu; ?></td>
        </tr>
        <tbody id="menu_group_<?php echo $group->kd_grup_menu ?>">
            <?php
            if (isset($menu[$group->kd_grup_menu])) :
                if (isset($menu[$group->kd_grup_menu][0])) :
                    $total_menu_1 = count($menu[$group->kd_grup_menu][0]);
                    $idx_menu_1 = 0;
                    foreach ($menu[$group->kd_grup_menu][0] as $value) :
                        $idx_menu_1++;
                        $id = $value['kd_menu'];
                        $group_menu_1_up = '';
                        $group_menu_1_down = '';
                        if ($idx_menu_1 == 1) {
                            $group_menu_1_up = 'disabled="disabled"';
                        }
                        if ($idx_menu_1 == $total_menu_1) {
                            $group_menu_1_down = 'disabled="disabled"';
                        }
                        ?>
                        <tr>
                            <td align="center"><i class="icon-menu"></i></td>
                            <td style="color: blue;font-weight: bold;"><?php echo $value['nama_menu']; ?></td>
                            <td><?php echo $value['deskripsi']; ?></td>
                            <td><?php echo $value['url']; ?></td>
                            <td align="center">
                                <?php
                                if ($this->access_right->otoritas('edit')) {
                                    echo anchor(null, '<i class="icon-arrow-up"></i>', 'class="btn transparant" ' . $group_menu_1_up);
                                    echo anchor(null, '<i class="icon-arrow-down"></i>', 'class="btn transparant" style="margin-left:5px;" ' . $group_menu_1_down);
                                } else {
                                    echo '<i class="icon-lock denied-color" title="Acces Denied"></i>';
                                }
                                ?>
                            </td>
                            <td align="center">
                                <?php
                                $action1 = '';
                                if ($this->access_right->otoritas('edit')) {
                                    $action1 .= anchor(null, '<i class="icon-edit"></i>', array('id' => 'button-edit-' . $id, 'class' => 'btn', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('menu/edit/' . $id)));
                                }
                                if ($this->access_right->otoritas('delete')) {
                                    $action1 .= anchor(null, '<i class="icon-trash"></i>', array('id' => 'button-delete-' . $id, 'class' => 'btn', 'onclick' => 'delete_row(this.id)', 'data-source' => base_url('menu/delete/' . $id), 'style' => 'margin-left:5px;'));
                                }
                                echo!empty($action1) ? $action1 : '<i class="icon-lock denied-color" title="Acces Denied"></i>';
                                ?>
                            </td>
                        </tr>

                        <?php
                        if (isset($menu[$group->kd_grup_menu][$value['kd_menu']])) :
                            $total_menu_2 = count($menu[$group->kd_grup_menu][$value['kd_menu']]);
                            $idx_menu_2 = 0;
                            foreach ($menu[$group->kd_grup_menu][$value['kd_menu']] as $value2) :
                                $idx_menu_2++;
                                $id2 = $value2['kd_menu'];
                                $group_menu_2_up = '';
                                $group_menu_2_down = '';
                                if ($idx_menu_2 == 1) {
                                    $group_menu_2_up = 'disabled="disabled"';
                                }
                                if ($idx_menu_2 == $total_menu_2) {
                                    $group_menu_2_down = 'disabled="disabled"';
                                }
                                ?>
                                <tr>
                                    <td style="padding-right: 16px;" align="right"><i class="icon-menu"></i></td>
                                    <td style="font-weight: bold;"><?php echo $value2['nama_menu']; ?></td>
                                    <td><?php echo $value2['deskripsi']; ?></td>
                                    <td><?php echo $value2['url']; ?></td>
                                    <td align="center">
                                        <?php
                                        if ($this->access_right->otoritas('edit')) {
                                            echo anchor(null, '<i class="icon-arrow-up"></i>', 'class="btn transparant" ' . $group_menu_2_up);
                                            echo anchor(null, '<i class="icon-arrow-down"></i>', 'class="btn transparant" style="margin-left:5px;" ' . $group_menu_2_down);
                                        } else {
                                            echo '<i class="icon-lock denied-color" title="Acces Denied"></i>';
                                        }
                                        ?>
                                    </td>
                                    <td align="center">
                                        <?php
                                        $action2 = '';
                                        if ($this->access_right->otoritas('edit')) {
                                            $action2 .= anchor(null, '<i class="icon-edit"></i>', array('id' => 'button-edit-' . $id2, 'class' => 'btn', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('menu/edit/' . $id2)));
                                        }
                                        if ($this->access_right->otoritas('delete')) {
                                            $action2 .= anchor(null, '<i class="icon-trash"></i>', array('id' => 'button-delete-' . $id2, 'class' => 'btn', 'onclick' => 'delete_row(this.id)', 'data-source' => base_url('menu/delete/' . $id2), 'style' => 'margin-left:5px;'));
                                        }
                                        echo!empty($action2) ? $action2 : '<i class="icon-lock denied-color" title="Acces Denied"></i>';
                                        ?>
                                    </td>
                                </tr>
                                <?php
                                if (isset($menu[$group->kd_grup_menu][$value2['kd_menu']])) :
                                    $total_menu_3 = count($menu[$group->kd_grup_menu][$value2['kd_menu']]);
                                    $idx_menu_3 = 0;
                                    foreach ($menu[$group->kd_grup_menu][$value2['kd_menu']] as $value3) :
                                        $idx_menu_3++;
                                        $id3 = $value3['kd_menu'];
                                        $group_menu_3_up = '';
                                        $group_menu_3_down = '';
                                        if ($idx_menu_3 == 1) {
                                            $group_menu_3_up = 'disabled="disabled"';
                                        }
                                        if ($idx_menu_3 == $total_menu_3) {
                                            $group_menu_3_down = 'disabled="disabled"';
                                        }
                                        ?>
                                        <tr>
                                            <td align="right"><i class="icon-menu"></i></td>
                                            <td><?php echo $value3['nama_menu']; ?></td>
                                            <td><?php echo $value3['deskripsi']; ?></td>
                                            <td><?php echo $value3['url']; ?></td>
                                            <td align="center">
                                                <?php
                                                if ($this->access_right->otoritas('edit')) {
                                                    echo anchor(null, '<i class="icon-arrow-up"></i>', 'class="btn transparant" ' . $group_menu_3_up);
                                                    echo anchor(null, '<i class="icon-arrow-down"></i>', 'class="btn transparant" style="margin-left:5px;" ' . $group_menu_3_down);
                                                } else {
                                                    echo '<i class="icon-lock denied-color" title="Acces Denied"></i>';
                                                }
                                                ?>
                                            </td>
                                            <td align="center">
                                                <?php
                                                $action3 = '';
                                                if ($this->access_right->otoritas('edit')) {
                                                    $action3 .= anchor(null, '<i class="icon-edit"></i>', array('id' => 'button-edit-' . $id3, 'class' => 'btn', 'onclick' => 'load_form_modal(this.id)', 'data-source' => base_url('menu/edit/' . $id3)));
                                                }
                                                if ($this->access_right->otoritas('delete')) {
                                                    $action3 .= anchor(null, '<i class="icon-trash"></i>', array('id' => 'button-delete-' . $id3, 'class' => 'btn', 'onclick' => 'delete_row(this.id)', 'data-source' => base_url('menu/delete/' . $id3), 'style' => 'margin-left:5px;'));
                                                }
                                                echo!empty($action3) ? $action3 : '<i class="icon-lock denied-color" title="Acces Denied"></i>';
                                                ?>
                                            </td>
                                        </tr>
                                        <?php
                                    endforeach;
                                endif;
                            endforeach;
                        endif;
                    endforeach;
                endif;
            endif;
            ?>
        </tbody>
    <?php endforeach; ?>
</table>
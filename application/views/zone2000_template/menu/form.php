<div class="row-fluid">
    <div class="box-title">
        <?php echo (isset($title)) ? $title : 'Untitle'; ?>
    </div>
    <div class="box-content">
        <?php
        $hidden_form = array('id' => !empty($id) ? $id : '');
        echo form_open_multipart($form_action, array('id' => 'finput', 'class' => 'form-horizontal'), $hidden_form);
        ?>
        <div class="control-group">
            <label for="password" class="control-label">Nama Menu</label>
            <div class="controls">
                <?php echo form_input('nama_menu', !empty($default->nama_menu) ? $default->nama_menu : '', 'class="span6" rows="4"'); ?>
            </div>
        </div>
        <div class="control-group">
            <label for="password" class="control-label">URL</label>
            <div class="controls">
                <div class="span12 input-prepend">
                    <span class="add-on" style="font-size: 11px;font-weight: bold;"><i class="icon-globe"></i> <?php echo base_url(); ?></span>
                    <?php echo form_input('url', !empty($default->url) ? $default->url : '', 'class="span7" rows="4"'); ?>
                </div>
            </div>
        </div>
        <div class="control-group">
            <label for="password" class="control-label">Deskripsi</label>
            <div class="controls">
                <?php echo form_textarea('deskripsi', !empty($default->deskripsi) ? $default->deskripsi : '', 'class="span8" rows="4"'); ?>
            </div>
        </div>
        <div class="control-group">
            <label for="select" class="control-label">Group Menu</label>
            <div class="controls">
                <?php echo form_dropdown('group_menu', $menu_group_options, !empty($default->kd_grup_menu) ? $default->kd_grup_menu : '', 'id="group_menu" class="chosen"') ?>
            </div>
        </div>
        <div class="control-group">
            <label for="select" class="control-label">Parent</label>
            <div class="controls">
                <?php echo form_dropdown('parent', array('' => 'NULL'), !empty($default->kd_parent) ? $default->kd_parent : '', 'id="parent" class="chosen" data-source="' . $parent_source . '"') ?>
            </div>
        </div>
        <div class="form-actions">
            <?php echo anchor(null, '<i class="icon-save"></i> Simpan', array('id' => 'button-save', 'class' => 'blue btn', 'onclick' => "simpan_data(this.id, '#finput', '#button-back')")); ?>
            <?php echo anchor(null, '<i class="icon-circle-arrow-left"></i> Kembali', array('id' => 'button-back', 'class' => 'btn', 'onclick' => 'close_form_modal(this.id)')); ?>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('.chosen').chosen();
        get_options('#parent', {kode: '<?php echo!empty($default->kd_grup_menu) ? $default->kd_grup_menu : ''; ?>', selected: '<?php echo!empty($default->kd_parent) ? $default->kd_parent : ''; ?>'}, true);
        $('#group_menu').change(function() {
            var id = $(this).val();
            var selected = '';
            get_options('#parent', {kode: id, selected: selected}, true);
        });
    });
</script>
<div class="inner_content">
    <div class="user_bar">
        <div class="row-fluid">
            <div class="float_left">
                <h3><span><?php echo (isset($title)) ? $title : 'Untitle'; ?></span></h3>
            </div>
        </div>
    </div>
    <div class="widgets_area">
        <div class="row-fluid">
            <div class="span6">
                <div class="well-content no-search">

                    <?php if ($this->access_right->otoritas('add')) : ?>
                        <div class="well">
                            <div class="pull-left">
                                <?php echo hgenerator::render_button_group($button_group); ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    
                    <div class="row-fluid" style="display: none;">
                        <div class="span12">
                            <div class="well">
                                <div class="well-content clearfix">

                                    <?php echo form_open_multipart('', array('id' => 'ffilter')); ?>
                                    <div class="form_row">
                                        <div class="pull-left span4">
                                            <div>
                                                <label>Keterangan :</label>
                                            </div>
                                            <div>
                                                <?php echo form_input('keterangan', '', 'class="span11 onchange"') ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php echo form_close(); ?>

                                </div>
                            </div> 
                        </div>
                    </div>

                    <div id="content_table" data-source="<?php echo $data_source; ?>" data-filter="#ffilter"></div>
                    <div>&nbsp;</div>
                </div>

                <div id="form-content" class="modal fade modal-xlarge"></div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(function() {
        laod_table_menu();
    });
</script>

<script type="text/javascript">
    function qslide(id) {
        $('#menu_group_' + id).slideToggle();
    }

    function laod_table_menu() {
        $('#content_table').html('&nbsp;').addClass('loading-progress');
        $('#content_table').load($('#content_table').attr('data-source'), function() {
            $('#content_table').removeClass('loading-progress');
        });
    }
</script>
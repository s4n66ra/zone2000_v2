<div class="row-fluid">
    <div class="box-title">
        <?php echo (isset($title)) ? $title : 'Untitle'; ?>
    </div>
    <div class="box-content">
        <?php
        $hidden_form = array('id' => !empty($id) ? $id : '');
        echo form_open_multipart($form_action, array('id' => 'finput', 'class' => 'form-horizontal'), $hidden_form);
        ?>

        <div class="control-group">
            <label for="select" class="control-label">Nama Cabang<span class="required">*</span></label>
            <div class="controls">
                <?php echo form_dropdown('id_lokasi',$options_cabang, !empty($default->id_cabang) ? $default->id_cabang : '', 'class="span6 onchange chosen"') ?>
            </div>
        </div> 

        <div class="control-group">
            <label for="select" class="control-label">Hadiah<span class="required">*</span></label>
            <div class="controls">
                <?php echo form_dropdown('id_hadiah',$options_hadiah, !empty($default->id_hadiah) ? $default->id_hadiah : '', 'class="span6 onchange chosen"') ?>  
            </div>
        </div> 


        <div class="control-group">
            <label for="select" class="control-label">Jumlah Tiket<span class="required">*</span></label>
            <div class="controls">
                <?php echo form_input('jumlah_tiket', !empty($default->jumlah_tiket) ? $default->jumlah_tiket : '', 'class="span6"') ?>
                
            </div>
        </div> 


        <div class="form-actions">
            <?php echo anchor(null, '<i class="icon-trash"></i> Hapus', array('id' => 'button-delete', 'class' => 'red btn', 'onclick' => "simpan_data(this.id, '#finput', '#button-back')")); ?>
            <?php echo anchor(null, '<i class="icon-circle-arrow-left"></i> Kembali', array('id' => 'button-back', 'class' => 'btn', 'onclick' => 'close_form_modal(this.id)')); ?>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('.date').datepicker({
            format: 'dd-mm-yyyy',
        });
        $('.chosen').chosen();
        $('.numeric').numeric();
    });

    function refresh_filter(){ 
        load_table('#content_table', 1,function(){

        });
    }
</script>

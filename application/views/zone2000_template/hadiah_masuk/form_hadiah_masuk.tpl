{{ form_action|raw }}
<div class="form-body">
	<h3 class="form-section"></h3>
	<div class="alert alert-danger display-hide">
		<button class="close" data-close="alert"></button>
		You have some form errors. Please check below.
	</div>
	<div class="alert alert-success display-hide">
		<button class="close" data-close="alert"></button>
		Your form validation is successful!
	</div>

		<div class="form-group">
			<label class="control-label col-md-3">Doorprize<span class="required">
			* </span>
			</label>
			<div class="col-md-4">
				{{options_hadiah|raw}}
			</div>

		</div>


		<div class="form-group">
			<label class="control-label col-md-3">Branch Name <span class="required">
			* </span>
			</label>
			<div class="col-md-4">
				{{options_cabang|raw}}
			</div>
		</div>


		<div class="form-group">
			<label class="control-label col-md-3">Amount <span class="required">
			* </span>
			</label>
			<div class="col-md-4">
				<input type="text" name="jumlah" value = "{{def_jumlah}}" data-required="1" class="form-control"/>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-md-3"> In Date<span class="required">
			* </span>
			</label>
			<div class="col-md-3">
				<div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd" data-date-viewmode="years" data-date-minviewmode="months">
					<input type="text" name="tanggal" class="form-control" readonly="" value="{{def_tanggal}}" todayHighlight="true">
					<span class="input-group-btn">
					<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
					</span>
				</div>
				<!-- /input-group -->
				<span class="help-block">
				Select month only </span>
			</div>							
		</div>
</div>
{{ form_action|raw }}
	<div class="row">
		<div class="form-body col-md-12">
			<div class="form-group row">
				<label class="control-label col-md-4">DATE BEGIN
				</label>
				<div class="col-md-8">
					
					<div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd">
					<input type="text" name="data[date_begin]" class="form-control" value="{{data.date_begin}}" > <span class="input-group-btn">
						<button class="btn default" type="button">
							<i class="fa fa-calendar"></i>
						</button>
					</span>
				</div>
				</div>
			</div>
			<div class="form-group row">
				<label class="control-label col-md-4">DATE END
				</label>
				<div class="col-md-8">
					
					<div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd">
					<input type="text" name="data[date_end]" class="form-control" value="{{data.date_end}}" > <span class="input-group-btn">
						<button class="btn default" type="button">
							<i class="fa fa-calendar"></i>
						</button>
					</span>
				</div>
				</div>
			</div>
			{% if is_head_office %}
			<div class="form-group row">
				<label class="control-label col-md-4">Outlet <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					{{list_store|raw}}
				</div>
			</div>
			{% else %}
				{{list_store|raw}}
			{% endif %}
			<div class="form-group row">
				<label class="control-label col-md-4">Item <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					{{list_item|raw}}
				</div>
			</div>
			<div class="form-group row">
				<label class="control-label col-md-4">Regular Tiket <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="reg_tiket" value = "{{reg_tiket}}" data-required="1" id="reg_tiket" class="form-control" target-type="text" readonly/>
				</div>
			</div>
			<div class="form-group row">
				<label class="control-label col-md-4">Qty Tiket <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="data[qty_tiket]" value = "{{data.qty_tiket}}" data-required="1" class="form-control format-number"/>
				</div>
			</div>

			<div class="form-group row">
				<label class="control-label col-md-4">Note <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<textarea type="text" name="data[note]" data-required="1" rows="3" class="form-control" >{{data.note}}</textarea>
				</div>
			</div>
		</div>
	</div>
</form>
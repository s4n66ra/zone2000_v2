{{ form_action|raw }}
	<div class="form-body">
		<h3 class="form-section"></h3>
		<div class="alert alert-danger display-hide">
			<button class="close" data-close="alert"></button>
			You have some form errors. Please check below.
		</div>
		<div class="alert alert-success display-hide">
			<button class="close" data-close="alert"></button>
			Your form validation is successful!
		</div>
		<div class="form-group row">
			<label class="control-label col-md-4">Class Code <span class="required">
			* </span>
			</label>
			<div class="col-md-8">
				<input type="text" name="data[kd_kelas]" data-required="1" class="form-control" value="{{ data.kd_kelas }}" {{readonly}}/>
			</div>
		</div>
		<div class="form-group row">
			<label class="control-label col-md-4">Class Name <span class="required">
			* </span>
			</label>
			<div class="col-md-8">
				<input type="text" name="data[nama_kelas]" value = "{{data.nama_kelas}}" data-required="1" class="form-control" {{readonly}}/>
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-md-4">Min Value<span class="required">
			* </span>
			</label>
			<div class="col-md-8">
				<input type="text" name="data[min_value]" value = "{{data.min_value}}" data-required="1" class="form-control" {{readonly}}/>
			</div>
		</div>
		<div class="form-group row">
			<label class="control-label col-md-4">Max Value<span class="required">
			* </span>
			</label>
			<div class="col-md-8">
				<input type="text" name="data[max_value]" value = "{{data.max_value}}" data-required="1" class="form-control" {{readonly}}/>
			</div>
		</div>
	</div>
</form>
<div class="row-fluid">
    <div class="box-title">
        <?php echo (isset($title)) ? $title : 'Untitle'; ?>
    </div>
    <div class="box-content">
        <?php
        $hidden_form = array('id' => !empty($id) ? $id : '');
        echo form_open_multipart($form_action, array('id' => 'finput', 'class' => 'form-horizontal'), $hidden_form);
        ?>

        <div class="control-group">
            <label for="select" class="control-label">Kode Kelas<span class="required">*</span></label>
            <div class="controls">
                <?php echo form_input('kd_kelas', !empty($default->kd_kelas) ? $default->kd_kelas : '', 'class="span6"') ?>
                
            </div>
        </div> 
        <div class="control-group">
            <label for="select" class="control-label">Nama Kelas<span class="required">*</span></label>
            <div class="controls">
                <?php echo form_input('nama_kelas', !empty($default->nama_kelas) ? $default->nama_kelas : '', 'class="span6"') ?>
                
            </div>
        </div>

        <div class="control-group">
            <label for="select" class="control-label">Min Value<span class="required">*</span></label>
            <div class="controls">
                <?php echo form_input('min_value', !empty($default->min_value) ? $default->min_value : '', 'class="span6"') ?>
                
            </div>
        </div>

        <div class="control-group">
            <label for="select" class="control-label">Max Value<span class="required">*</span></label>
            <div class="controls">
                <?php echo form_input('max_value', !empty($default->max_value) ? $default->max_value : '', 'class="span6"') ?>                
            </div>
        </div>


        <div class="form-actions">
            <?php echo anchor(null, '<i class="icon-trash"></i> Hapus', array('id' => 'button-delete', 'class' => 'red btn', 'onclick' => "simpan_data(this.id, '#finput', '#button-back')")); ?>
            <?php echo anchor(null, '<i class="icon-circle-arrow-left"></i> Kembali', array('id' => 'button-back', 'class' => 'btn', 'onclick' => 'close_form_modal(this.id)')); ?>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('.date').datepicker({
            format: 'dd-mm-yyyy',
        });
        $('.chosen').chosen();
        $('.numeric').numeric();
    });

    function refresh_filter(){ 
        load_table('#content_table', 1,function(){

        });
    }
</script>

<div class="row">
	<div class="col-md-4">
		<div class="form-group row">
			<label class="control-label col-md-4">STORE NAME
			</label>
			<div class="col-md-8">
				<input type="text" name="data[nama_cabang]" value = "{{data.nama_cabang}}" data-required="1" class="form-control" disabled="" />
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-md-4">DATE
			</label>
			<div class="col-md-8">
				
				<div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd">
				<input type="text" name="data[date_created]" class="form-control" value="{{data.date_created}}" readonly> <span class="input-group-btn">
					<button class="btn default" type="button">
						<i class="fa fa-calendar"></i>
					</button>
				</span>
			</div>
			</div>
		</div>

	</div>
</div>
<div class="tabbable-custom show-overflow">
	<ul class="nav nav-tabs ">
		{% if(page.create_koli!=null) %}
			<li class="active">
				<a href="#tab_15_1" data-toggle="tab">
				LIST PO</a>
			</li>
			<li>
				<a href="#tab_15_2" data-toggle="tab">
				LIST RECEIVING</a>
			</li>
		{% else %}
			<li class="active">
				<a href="#tab_15_2" data-toggle="tab">
				LIST RECEIVING</a>
			</li>
		{% endif %}
	</ul>

	<div class="tab-content">
		{% if(page.create_koli!=null) %}
			<div class="tab-pane active" id="tab_15_1">
				{{ page.create_koli | raw }}
			</div>
			<div class="tab-pane" id="tab_15_2">
				{{ page.list_koli | raw }}
			</div>
		{% else %}
			<div class="tab-pane active" id="tab_15_2">
				{{ page.list_koli | raw }}
			</div>
		{% endif %}
	</div>

</div>


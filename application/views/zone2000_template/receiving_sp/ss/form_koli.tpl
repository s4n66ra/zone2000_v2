{{ form_action|raw }}
	<div class="form-body">

		<div class="form-group row">
			<label class="control-label col-md-4">PO
			</label>
			<div class="col-md-8">
				{% for po in postores %}
				{{po.po_code}}<br>
				{% endfor %}
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-md-4">Store
			</label>
			<div class="col-md-8">
				{{list.store|raw}}
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-md-4">Code Receiving
			</label>
			<div class="col-md-8">
				<input type="text" name="data[koli_code]" data-required="1" class="form-control " value="{{ data.koli_code }}" disabled/>
				<input type="hidden" name="data[koli_id]"  value="{{ data.koli_id }}" />
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-md-4">DATE
			</label>
			<div class="col-md-8">
				
				<div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd">
				<input type="text" name="data[date_created]" class="form-control" value="{{data.date_created}}" readonly> <span class="input-group-btn">
					<button class="btn default" type="button">
						<i class="fa fa-calendar"></i>
					</button>
				</span>
			</div>
			</div>
		</div>

<!-- 		<div class="form-group row">
			<label class="control-label col-md-4">Qty Koli
			</label>
			<div class="col-md-8">
				<input type="text" name="data[qty]" data-required="1" class="form-control format-number" value="{{ data.qty }}"/>
			</div>
		</div> -->

		<input type="hidden" name="data[qty]" value="1">

		<div class="form-group row">
			<label class="control-label col-md-4">Note<span class="required">
			* </span>
			</label>
			<div class="col-md-8">
				<textarea type="text" name="data[note]" data-required="1" rows="3" class="form-control" >{{ data.note }}</textarea>
			</div>
		</div>

	</div>
	<div class="scroll-modal" >
		<div class="form-group-min-mg row" style="border-bottom:1px solid #eee" >

			<label class="control-label col-md-8">
				ITEM
			</label>
			<label class="control-label col-md-2">
				JUMLAH
			</label>
			<label class="control-label col-md-2">
				RECEIVED
			</label>
		</div>
		{% for data in table %}
			<div class="form-group-min-mg row">
				<label class="control-label col-md-8">
					{{ data.item_name|raw }}
				</label>
				<label class="control-label col-md-2">
					{{ data.qty_for_po|raw }}
				</label>
				<div class="col-md-2">
					<input type = "hidden" name="dc_item[{{data.podetail_id}}]" value = "{{data.item_id}}">
					<input type = "teks" size = "6" name="dc_qty[{{data.podetail_id}}]" value="{{data.dc_qty_received}}">
				</div>
			</div>
		{% endfor %} 
	</div>
{% if form_action %}
</form>
{% endif %}
{{ form_action|raw }}
	<div class="row">
		<div class="form-body col-md-12">
			<div class="form-group row" hidden>
				<label class="control-label col-md-4">ID <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="data[koli_id]" id="koli_id" data-required="1" class="form-control" value="{{data.koli_id}}"/>
				</div>
			</div>

			<div class="form-group row" hidden>
				<label class="control-label col-md-4">Code <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="data[koli_code]" id="koli_code" data-required="1" class="form-control" value="{{data.koli_code}}"/>
				</div>
			</div>
			<div class="form-group row" hidden>
				<label class="control-label col-md-4">Qty <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="data[qty]" id="qty" value = "{{data.qty}}" data-required="1" class="form-control"/>
				</div>
			</div>
			<div class="form-group row" hidden>
				<label class="control-label col-md-4">Note <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="data[note]" id="note" value = "{{data.note}}" data-required="1" class="form-control"/>
				</div>
			</div>

			<div class="form-group row" hidden>
				<label class="control-label col-md-4">Koli Status <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="data[koli_status]" id="koli_status" value = "{{data.koli_status}}" data-required="1" class="form-control"/>
				</div>
			</div>

			<div class="form-group row" hidden>
				<label class="control-label col-md-4">Store Id <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="data[store_id]" id="store_id" value = "{{data.store_id}}" data-required="1" class="form-control"/>
				</div>
			</div>


			<div class="form-group row" hidden>
				<label class="control-label col-md-4">Supplier ID <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="data[supplier_id]" id="supplier_id" value = "{{data.supplier_id}}" data-required="1" class="form-control"/>
				</div>
			</div>

			<div class="form-group row" hidden>
				<label class="control-label col-md-4">Sp Code <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="data[sp_code]" id="sp_code" value = "{{data.sp_code}}" data-required="1" class="form-control"/>
				</div>
			</div>


			<div class="form-group row" hidden>
				<label class="control-label col-md-4">dc_qty_received<span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="data[dc_qty_received]" id="dc_qty_received" value = "{{data.dc_qty_received}}" data-required="1" class="form-control"/>
				</div>
			</div>

			<div class="form-group row" hidden>
				<label class="control-label col-md-4">kd_cabang<span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="data[kd_cabang]" id="kd_cabang" value = "{{data.kd_cabang}}" data-required="1" class="form-control"/>
				</div>
			</div>
			
			<div class="form-group row">
				<div class="col-md-12" align="center" style="margin-left:10px;">
					<!-- OPSI CANVAS -->
					<!-- <canvas id="barcode_modal" width="200px" height="70px"> </canvas>-->

					<!-- OPSI HTML -->
					<div id="barcode_modal"></div>
 				</div>
			</div>

			<div class="form-group row">
				<div class="col-md-12" align="center">
					<!-- OPSI CANVAS -->
					<!-- <canvas id="barcode_modal" width="200px" height="70px"> </canvas>-->

					<!-- OPSI HTML -->
					<div>{{data.item_name}}</div>
 				</div>
			</div>

			<div class="form-group row">
				<div class="col-md-6">
					<label class="control-label" style="float:right; margin-right:0px;">Number of Copies
					</label>					
				</div>

				<div class="col-md-6">
					<div class="col-md-7">
						<select class="form-control" style="float:left;margin-left:0px;" id="numb_copies">
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="10">10</option>
							<option value="20">20</option>
							<option value="50">50</option>
							<option value="100">100</option>
						</select>
						
					</div>

				</div>
					
			</div>

			<div class="form-group row">
				<div class="col-md-12" align="center">
					<!-- OPSI CANVAS -->
					<!-- <canvas id="barcode_modal" width="200px" height="70px"> </canvas>-->

					<!-- OPSI HTML -->
					<div>
						<span id="list_printer">
							
						</span>
					</div>
 				</div>
			</div>
		</div>
	</div>
</form>
<div class="tabbable-custom show-overflow">
	<ul class="nav nav-tabs ">
		{% if page.mc_refill is not empty %}
		<li class="active">
			<a href="#rep-mc-refill" data-toggle="tab">
			REPLENISH</a>
		</li>
		{% endif %}

		{% if page.hist_mc_refill is not empty %}
		<li class="">
			<a href="#hist-rep-mc-refill" data-toggle="tab">
			HISTORY REPLENISH</a>
		</li>
		{% endif %}
	</ul>

	<div class="tab-content">
		<div class="tab-pane active" id="rep-mc-refill">
			{{ page.mc_refill | raw }}
		</div>

		<div class="tab-pane" id="hist-rep-mc-refill">
			{{ page.hist_mc_refill | raw }}
		</div>
	</div>

</div>


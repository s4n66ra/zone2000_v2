
<div class="form-body">
	<h3 class="form-section"></h3>
	<div class="alert alert-danger display-hide">
		<button class="close" data-close="alert"></button>
		You have some form errors. Please check below.
	</div>
	<div class="alert alert-success display-hide">
		<button class="close" data-close="alert"></button>
		Your form validation is successful!
	</div>

	<div class="row">
		{{ form_action|raw }}
			<div class="col-md-6">
				<div class="form-group row">
					<label class="control-label col-md-4">Type Code
					
					</label>
					<div class="col-md-8">
						<input type="text" name="data[kd_jenis_mesin]" data-required="1" id="machine-id" class="form-control" value="{{ data.kd_jenis_mesin }}" {{readonly}} readonly/>
					</div>
				</div>

				<div class="form-group row">
					<label class="control-label col-md-4">Barcode
					
					</label>
					<div class="col-md-8">
						<input type="text" name="data[item_key]" data-required="1" class="form-control" value="{{ data.item_key }}" {{readonly}}/>
					</div>
				</div>
				
				<div class="form-group row">
					<label class="control-label col-md-4">Type Name
					</label>
					<div class="col-md-8">
						<input type="text" name="data[nama_jenis_mesin]" data-required="1" class="form-control" value="{{ data.nama_jenis_mesin }}" {{readonly}}/>
					</div>
				</div>

				<div class="form-group row">
					<label class="control-label col-md-4">Category
					
					</label>
					<div class="col-md-8">
						{{ list_kategori|raw }}
					</div>
				</div>

				<!-- <div class="form-group row">
					<label class="control-label col-md-4">Coin Type
					</label>
					<div class="col-md-8">
						{{ list_coin|raw }}
					</div>
				</div> -->

				<!-- <div class="form-group row">
					<label class="control-label col-md-4">Class
					</label>
					<div class="col-md-8">
						{{ list_kelas|raw }}
					</div>
				</div> -->

				

			</div>

			<div class="col-md-6">
				<div class="form-group row">
					<label class="control-label col-md-4">Input Type
					</label>
					<div class="col-md-8">
						{{ list_typein|raw }}
					</div>
				</div>

				<div class="form-group row">
					<label class="control-label col-md-4">Output Type
					</label>
					<div class="col-md-8">
						{{ list_typeout|raw }}
					</div>
				</div>

				<div class="form-group row">
					<label class="control-label col-md-4">Description
					</label>
					<div class="col-md-8">
						<textarea name="data[deskripsi]" class="form-control" rows="5">{{data.deskripsi}}</textarea>
					</div>
				</div>

				<!-- <div class="form-group row">
					<label class="control-label col-md-4">Price
					
					</label>
					<div class="col-md-8">
						<input type="text" name="data[price]" data-required="1" class="form-control format-number" value="{{ data.price }}" {{readonly}}/>
					</div>
				</div> -->
				<!-- <div class="form-group row">
					<label class="control-label col-md-4">Image</label>
					<div class="col-md-8">
						<input type="text" name="data[foto]" data-required="1" class="form-control" value="{{ data.foto }}" {{readonly}}/>
					</div>
				</div> -->

			</div>
		</form>
	</div>

</div>

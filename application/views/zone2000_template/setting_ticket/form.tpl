{{ form_action|raw }}
	<div class="form-group row">
		<label class="control-label col-md-4">Kategori
		</label>
		<div class="col-md-8">
			<div class="input-icon right">
				<i class="fa"></i>
				<input type="text" name="data[setting_name]" data-required="1" class="form-control" value="{{data.setting_name}}" {{readonly}}/>
			</div>
		</div>
	</div>

	<div class="form-group row">
		<label class="control-label col-md-4">Value <span class="required">
		* </span>
		</label>
		<div class="col-md-8">
			<div class="input-icon right">
				<i class="fa"></i>
				<input type="text" name="data[setting_qty]" value = "{{data.setting_qty}}" data-required="1" class="form-control format-number" {{readonly}}/>
			</div>
		</div>
	</div>
</form>
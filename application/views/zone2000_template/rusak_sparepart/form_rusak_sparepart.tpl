{{ form_action|raw }}
	<div class="form-body col-md-12">
		<h3 class="form-section"></h3>
		<div class="alert alert-danger display-hide">
			<button class="close" data-close="alert"></button>
			You have some form errors. Please check below.
		</div>
		<div class="alert alert-success display-hide">
			<button class="close" data-close="alert"></button>
			Your form validation is successful!
		</div>


		<div class="row">
			<div class="col-md-12">
				<div class="form-group row">
					<label class="control-label col-md-4">BROKEN SPAREPART <span class="required">
					* </span>
					</label>
					<div class="col-md-8">
						<input type="text" name="kd_rusak_sparepart" data-required="1" class="form-control" value="{{ data.kd_rusak_sparepart }}" {{readonly}}/>
					</div>
				</div>

				<div class="form-group row">
					<label class="control-label col-md-4">MACHINE NAME				
					</label>
					<div class="col-md-8">
						{{ list_id_mesin_kerusakan|raw }}
					</div>
				</div>

				<div class="form-group row">
					<label class="control-label col-md-4">SPAREPART NAME				
					</label>
					<div class="col-md-8">
						{{ list_sparepart|raw }}
					</div>
				</div>

			</div>

		</div>



	</div>
</form>



<script>
	jQuery(document).ready(function(){ 
		if ($().select2) {
            $('.select2me').select2({
                placeholder: "Select",
                allowClear: true
            });
        }

        if ($().datepicker) {
            $('.date-picker').datepicker({
                rtl: Metronic.isRTL(),
                orientation: "left",
                autoclose: true
            });
            //$('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
        }
	});
</script>
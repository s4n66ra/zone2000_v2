{{ form_action|raw }}
	<div class="row">
		<div class="form-body col-md-12">
<!-- 			<div class="form-group row">
				<label class="control-label col-md-4">Code <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="data[kd_hadiah]" data-required="1" class="form-control" value="{{data.kd_hadiah}}"/>
				</div>
			</div> -->
			
			<div class="form-group row">
				<label class="control-label col-md-4">SKU Code <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="data[item_key]" data-required="1" class="form-control" value="{{data.item_key}}" readonly/>
				</div>
			</div>
			<div class="form-group row">
				<label class="control-label col-md-4">Code Exist 
				</label>
				<div class="col-md-8">
					<input type="text" name="data[code_exist]" data-required="1" class="form-control" value="{{data.code_exist}}" />
				</div>
			</div>
			<div class="form-group row">
				<label class="control-label col-md-4">Name <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="data[nama_hadiah]" value = "{{data.nama_hadiah}}" data-required="1" class="form-control"/>
				</div>
			</div>

			<div class="form-group row">
				<label class="control-label col-md-4">Price/Unit <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="data[harga]" value = "{{data.harga}}" data-required="1" class="form-control format-number"/>
				</div>
			</div>
			<div class="form-group row">
				<label class="control-label col-md-4">Satuan <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					{{options_satuan|raw}}
				</div>
			</div>

			<div class="form-group row">
				<label class="control-label col-md-4">Price/Pax <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="data[price_pax]" value = "{{data.price_pax}}" data-required="1" class="form-control format-number"/>
				</div>
			</div>

			<div class="form-group row">
				<label class="control-label col-md-4">Type <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					{{options_hadiah|raw}}
				</div>
			</div>
			<div class="form-group row">
				<label class="control-label col-md-4">Divisi <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					{{options_divisi|raw}}
				</div>
			</div>

			<div class="form-group row">
				<label class="control-label col-md-4">Barcode Eksisting <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="data[item_barcode]" data-required="1" class="form-control" value="{{data.item_barcode}}"/>
				</div>
			</div>
		</div>
	</div>
</form>
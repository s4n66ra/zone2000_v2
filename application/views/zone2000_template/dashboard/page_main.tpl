
			<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<!-- END PAGE TITLE -->
 
				<!-- BEGIN PAGE TOOLBAR -->
				<div class="page-toolbar">
					<!-- BEGIN THEME PANEL -->
					<!-- END THEME PANEL -->
				</div>
				<!-- END PAGE TOOLBAR -->
			</div>
			<!-- END PAGE HEAD -->
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb hide">
				<li>
					<a href="#">Home</a><i class="fa fa-circle"></i>
				</li>
				<li class="active">
					 Dashboard
				</li>
			</ul>
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->

			<!--BEGIN TEST ROW DIAGRAM -->
			<!-- BEGIN ROW -->
			<div class="row">
				<div class="col-md-6">
					<!-- BEGIN CHART PORTLET-->
					<div class="portlet light">
						<div class="portlet-title">

							<div class="caption">
								<i class="icon-bar-chart "></i>
								<span class="caption-subject bold uppercase "> TOP 10 Merchandise
								</span>
								

							</div>

						</div>
						<div class="portlet-body">
							<div id="horbar" class="chart" style="height: 400px; width:100%;">
							</div>
						</div>
					</div>
					<!-- END CHART PORTLET-->
				</div>
				<!-- END BAR -->

				<div class="col-md-6">
					<!-- BEGIN CHART PORTLET-->
					<div class="portlet light">
						<div class="portlet-title">

							<div class="caption">
								<i class="icon-bar-chart "></i>
								<span class="caption-subject bold uppercase "><a href="lap_store"> TOP 10 Store </a>
								</span>
								

							</div>

						</div>
						<div class="portlet-body">
							<div id="dsbtoptoko" class="chart" style="height: 400px; width:100%;">
							</div>
						</div>
					</div>
					<!-- END CHART PORTLET-->
				</div>
				<!-- END BAR -->

				<div class="col-md-6">
					<!-- BEGIN CHART PORTLET-->
					<div class="portlet light">
						<div class="portlet-title">

							<div class="caption">
								<i class="icon-bar-chart "></i>
								<span class="caption-subject bold uppercase "><a href="lap_store"> Worse 10 Store </a>
								</span>
								

							</div>

						</div>
						<div class="portlet-body">
							<div id="dsbworsetoko" class="chart" style="height: 400px; width:100%;">
							</div>
						</div>
					</div>
					<!-- END CHART PORTLET-->
				</div>
				<!-- END BAR -->

				<div class="col-md-6">
					<!-- BEGIN CHART PORTLET-->
					<div class="portlet light">
						<div class="portlet-title">

							<div class="caption">
								<i class="icon-bar-chart "></i>
								<span class="caption-subject bold uppercase "><a href="lap_mesin"> TOP 5 Machine</a>
								</span>
								

							</div>

						</div>
						<div class="portlet-body">
							<div id="top5machine" class="chart" style="height: 400px; width:100%;">
							</div>
						</div>
					</div>
					<!-- END CHART PORTLET-->
				</div>
				<!-- END BAR -->

				<div class="col-md-6">
					<!-- BEGIN CHART PORTLET-->
					<div class="portlet light">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-bar-chart "></i>
								<span class="caption-subject bold uppercase "> WORSE 5 Machine
								</span>
							</div>
						</div>
						<div class="portlet-body">
							<div id="worse5machine" class="chart" style="height: 400px; width:100%;">
							</div>
						</div>
					</div>
					<!-- END CHART PORTLET-->
				</div>
				<!-- END BAR -->

				<div class="col-md-6">
					<!-- BEGIN CHART PORTLET-->
					<div class="portlet light">
						<div class="portlet-title">

							<div class="caption">
								<i class="icon-bar-chart "></i>
								<span class="caption-subject bold uppercase "> Status Service
								</span>
								

							</div>

						</div>
						<div class="portlet-body">
							<div id="statusservice" class="chart" style="height: 400px; width:100%;">
							</div>
						</div>
					</div>
					<!-- END CHART PORTLET-->
				</div>


				<div class="col-md-6">
					<!-- BEGIN CHART PORTLET-->
					<div class="portlet light">
						<div class="portlet-title">

							<div class="caption">
								<i class="icon-bar-chart "></i>
								<span class="caption-subject bold uppercase "> TOP 10 Sparepart
								</span>
								

							</div>

						</div>
						<div class="portlet-body">
							<div id="top10sparepart" class="chart" style="height: 400px; width:100%;">
							</div>
						</div>
					</div>
					<!-- END CHART PORTLET-->
				</div>
				<!-- END BAR -->

				<div class="col-md-6">
					<!-- BEGIN CHART PORTLET-->
					<div class="portlet light">
						<div class="portlet-title">

							<div class="caption">
								<i class="icon-bar-chart "></i>
								<span class="caption-subject bold uppercase "> Trend Service
								</span>
								

							</div>

						</div>
						<div class="portlet-body">
							<div id="trendservice" class="chart" style="height: 400px; width:100%;">
							</div>
						</div>
					</div>
					<!-- END CHART PORTLET-->
				</div>
				<!-- END BAR -->



				<div class="col-md-6" hidden>
					<!-- BEGIN CHART PORTLET-->
					<div class="portlet light">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-bar-chart "></i>
								<span class="caption-subject bold uppercase "> Stock Charts</span>
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="fullscreen">
								</a>
								<a href="javascript:;" class="remove">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<div id="pie_laku" class="chart" style="height: 400px; width:100%;">
							</div>
						</div>
					</div>
					<!-- END CHART PORTLET-->
				</div>
				<!-- END CHART -->




			</div>
			<!-- END ROW -->
			<!-- END TEST -->


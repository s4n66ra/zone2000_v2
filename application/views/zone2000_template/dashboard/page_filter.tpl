<div class="component-page">


<div class="portlet light">	
	<div class="portlet-title">
		<div class="caption font-purple-plum">
			<span class="caption-subject bold uppercase"> Dashboard Data Filter</span>
			<span class="caption-helper"></span>
		</div>
		<div class="tools">
			<a href="javascript:;" class="expand" data-original-title="" title="">
			</a>
		</div>
		<div class="actions">
			<div style="display:inline;margin-right:10px;">
			</div>

			
		</div>
	</div>

	<div class="portlet-body" style="display:none;">
	<div class="table-toolbar">
		<div class="row" class="col-md-12">
			<div class="col-md-3">
				<label>Kategory</label>
				<select class="form-control select2" id="kategory_id">
					<option value="store">
					Store
					</option>					
					<option value="area">
					Area
					</option>					
					<option value="province">
					Province
					</option>					
				</select>

			</div>

			<div class="col-md-3">
				<label></label>
					<input type="text" id="det_kategory_id" class="form-control" style="margin-top:1.8%" />
			</div>

			<div class="col-md-3">
				<label>Period</label>
				<div class="input-group date-picker input-daterange input-daterange" data-date="10/11/2012" data-date-format="yyyy-mm-dd">
					<input type="text" class="form-control" name="rent_date_start" id="rent_date_start" url-action="rent_date_start" />
					<span class="input-group-addon">
					to </span>
					<input type="text" class="form-control" name="rent_date_end" id="rent_date_end" url-action="rent_date_end" />
				</div>
			</div>

			<div class="col-md-3">
				<label>Mode</label>
				<select class="form-control select2" id="mode">
					<option value="daily">
					Daily
					</option>					
					<option value="monthly">
					Monthly
					</option>					
					<option value="yearly">
					Yearly
					</option>					
				</select>
			</div>




		</div>
		<div class="row" class="col-md-12" style="margin-top:1%">
			<div class="col-md-11">
				<div class="note h4">
					<span class="font-black-cascade">
						No Filter 
					</span>
				</div>				
			</div>
			<div class="col-md-1">
				<button class="btn green-turquoise btn-form-modal" id="btn_dsb_filter_apply" style="float:right;margin-top:1%;">Apply</button>
			</div>			
		</div>

	</div>

	</div>
</div>

</div>
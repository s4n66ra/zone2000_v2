{{ form_action|raw }}
	<div class="row">
		<div class="form-body col-md-12">
			
			<div class="form-group row">
				<label class="control-label col-md-4">Kode<span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="data[kd_satuan]" value = "{{data.kd_satuan}}" data-required="1" class="form-control"/>
				</div>
			</div>
			<div class="form-group row">
				<label class="control-label col-md-4">Satuan Name<span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="data[nama_satuan]" value = "{{data.nama_satuan}}" data-required="1" class="form-control"/>
				</div>
			</div>
		</div>
	</div>
</form>
{{ form_action|raw }}
	<div class="row">
		<div class="form-body col-md-12">
			<div class="form-group row">
				<label class="control-label col-md-4">DATE
				</label>
				<div class="col-md-8">
					
					<div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd">
					<input type="text" name="data[rec_created]" class="form-control" value="{{data.rec_created}}" > <span class="input-group-btn">
						<button class="btn default" type="button">
							<i class="fa fa-calendar"></i>
						</button>
					</span>
				</div>
				</div>
			</div>
			<div class="form-group row">
				<label class="control-label col-md-4">Outlet <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					{{list_store|raw}}
				</div>
			</div>
			<div class="form-group row">
				<label class="control-label col-md-4">Item <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					{{list_item|raw}}
				</div>
			</div>
			<div class="form-group row">
				<label class="control-label col-md-4">Item Name <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="data[item_name]" value = "{{data.item_name}}" data-required="1" class="form-control"/>
				</div>
			</div>
			<div class="form-group row">
				<label class="control-label col-md-4">Harga <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="data[harga]" value = "{{data.harga}}" data-required="1" class="form-control format-number"/>
				</div>
			</div>
			<div class="form-group row">
				<label class="control-label col-md-4">Qty <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="data[item_qty]" value = "{{data.item_qty}}" data-required="1" class="form-control format-number"/>
				</div>
			</div>
			<div class="form-group row">
				<label class="control-label col-md-4">Note <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<textarea name="data[keterangan]" class="form-control" >{{data.keterangan}}</textarea>
				</div>
			</div>
			{{list_jenis|raw}}
				
		</div>
	</div>
</form>
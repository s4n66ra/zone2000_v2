{{ form_action|raw }}
<div class="row">
	<div class="col-md-12">
		{{ hidden_ids | raw }}
		<div class="form-group row">
			<label class="control-label col-md-4">DATE
			</label>
			<div class="col-md-8">
				
				<div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd">
				<input type="text" name="data[date_delivered]" class="form-control" value="{{ date_now }}" > <span class="input-group-btn">
					<button class="btn default" type="button">
						<i class="fa fa-calendar"></i>
					</button>
				</span>
			</div>
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-md-4">DRIVER NAME
			</label>
			<div class="col-md-8">
				<input type="text" name="data[driver_name]" value = "{{data.nama_cabang}}" data-required="1" class="form-control"  />
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-md-4">TRUCK NUMBER
			</label>
			<div class="col-md-8">
				<input type="text" name="data[truck_number]" value = "{{data.truck_number}}" data-required="1" class="form-control" />
			</div>
		</div>

	</div>
{% if form_action %}
</div>
{% endif %}
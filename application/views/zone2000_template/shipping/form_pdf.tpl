{{ form_action|raw }}
<div class="row">
	<div class="col-md-6">
		<div class="form-group row">
			<label class="control-label col-md-4">NO. SURAT JALAN / DO <span class="required">
			* </span>
			</label>
			<div class="col-md-8">
				<input type="hidden" name="data[shipping_code]"  id="shipping_code" class="form-control" value="{{ data.shipping_code }}" />
			</div>
		</div>
	</div>
	{{ group_hidden | raw}}
{% if form_action %}
</div>
{% endif %}
{{form_action | raw}}
<div class="form-body">
	<h3 class="form-section"></h3>
	<div class="alert alert-danger display-hide">
		<button class="close" data-close="alert"></button>
		You have some form errors. Please check below.
	</div>
	<div class="alert alert-success display-hide">
		<button class="close" data-close="alert"></button>
		Your form validation is successful!
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="form-group row">
				<label class="control-label col-md-4">Code SKU <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="kd_sparepart" data-required="1" class="form-control" value="{{data.kd_sparepart}}"/>
				</div>
			</div>
			<div class="form-group row">
				<label class="control-label col-md-4">Code Exist 
				</label>
				<div class="col-md-8">
					<input type="text" name="code_exist" data-required="1" class="form-control" value="{{data.code_exist}}"/>
				</div>
			</div>
<!-- 			<div class="form-group row">
				<label class="control-label col-md-4">Code SKU<span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="kd_sparepart" data-required="1" class="form-control" value="{{data.kd_sparepart}}"/>
				</div>
			</div> -->
			<div class="form-group row">
				<label class="control-label col-md-4">Sparepart Name<span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="nama_sparepart" value = "{{data.nama_sparepart}}" data-required="1" class="form-control"/>
				</div>
			</div>
			<div class="form-group row">
				<label class="control-label col-md-4">Price<span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="harga" value = "{{data.harga}}" data-required="1" class="form-control"/>
				</div>
			</div>
		</div>
	</div>

</div>
</form>
<div class="form-body">
	<h3 class="form-section"></h3>
	<div class="alert alert-danger display-hide">
		<button class="close" data-close="alert"></button>
		You have some form errors. Please check below.
	</div>
	<div class="alert alert-success display-hide">
		<button class="close" data-close="alert"></button>
		Your form validation is successful!
	</div>
	<div class="row">
		{{ form_action|raw }}
			<div class="form-group">
				<label class="control-label col-md-3">Position Code<span class="required">
				* </span>
				</label>
				<div class="col-md-4">
					<input type="text" name="kd_jabatan" data-required="1" class="form-control" value="{{data}}"/>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3">Position Name<span class="required">
				* </span>
				</label>
				<div class="col-md-4">
					<input type="text" name="nama_jabatan" value = "{{data}}" data-required="1" class="form-control"/>
				</div>
			</div>
		</form>
	</div>
</div>

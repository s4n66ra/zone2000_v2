<div class="form-body" style="padding: 10px">
    <h3 class="form-section"></h3>
    <div class="alert alert-danger display-hide">
        <button class="close" data-close="alert"></button>
        You have some form errors. Please check below.
    </div>
    <div class="alert alert-success display-hide">
        <button class="close" data-close="alert"></button>
        Your form validation is successful!
    </div>
    <div class="row">
        {{ form_action|raw }}
            <div class="form-group row">
                <label class="control-label col-md-4">Position Code<span class="required">
                * </span>
                </label>
                <div class="col-md-4">
                    <input type="text" name="data[grup_nama]" data-required="1" class="form-control" value="{{data.grup_nama}}"/>
                </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-md-4">Position Name<span class="required">
                * </span>
                </label>
                <div class="col-md-4">
                    <input type="text" name="data[grup_deskripsi]" value = "{{data.grup_deskripsi}}" data-required="1" class="form-control"/>
                </div>
            </div>
        </form>
    </div>
</div>
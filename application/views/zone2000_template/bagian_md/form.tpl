{{ form_action|raw }}
	<div class="row">
		<div class="form-body col-md-12">
			
			<div class="form-group row">
				<label class="control-label col-md-4">Divisi MD Code<span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="data[kd_bagian_md]" value = "{{data.kd_bagian_md}}" data-required="1" class="form-control"/>
				</div>
			</div>
			<div class="form-group row">
				<label class="control-label col-md-4">Divisi MD Name<span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="data[nama_bagian_md]" value = "{{data.nama_bagian_md}}" data-required="1" class="form-control"/>
				</div>
			</div>
			<div class="form-group row">
				<label class="control-label col-md-4">Keterangan
				</label>
				<div class="col-md-8">
					<input type="text" name="data[keterangan]" value = "{{data.keterangan}}" data-required="1" class="form-control"/>
				</div>
			</div>
			<div class="form-group row">
				<label class="control-label col-md-4">No Urut
				</label>
				<div class="col-md-8">
					<input type="text" name="data[no_urut]" value = "{{data.no_urut}}" data-required="1" class="form-control"/>
				</div>
			</div>
		</div>
	</div>
</form>
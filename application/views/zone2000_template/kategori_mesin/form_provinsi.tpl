{{ form_action|raw }}
	<div class="form-body">
		<h3 class="form-section"></h3>
		<div class="alert alert-danger display-hide">
			<button class="close" data-close="alert"></button>
			You have some form errors. Please check below.
		</div>
		<div class="alert alert-success display-hide">
			<button class="close" data-close="alert"></button>
			Your form validation is successful!
		</div>
		<div class="form-group row">
			<label class="control-label col-md-3">Kode Provinsi <span class="required">
			* </span>
			</label>
			<div class="col-md-4">
				<input type="text" name="kd_provinsi" data-required="1" class="form-control" value="{{def_kd_provinsi}}"/>
			</div>
		</div>
		<div class="form-group row">
			<label class="control-label col-md-3">Nama Provinsi <span class="required">
			* </span>
			</label>
			<div class="col-md-4">
				<input type="text" name="nama_provinsi" value = "{{def_nama_provinsi}}" data-required="1" class="form-control"/>
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-md-3">Nama Pulau <span class="required">
			* </span>
			</label>
			<div class="col-md-4">
				{{options_pulau|raw}}
			</div>
		</div>
	</div>
</form>
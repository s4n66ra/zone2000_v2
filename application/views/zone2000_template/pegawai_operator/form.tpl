<div class="form-body" style="margin-bottom: 10px">
	<h3 class="form-section"></h3>
	<div class="alert alert-danger display-hide">
		<button class="close" data-close="alert"></button>
		You have some form errors. Please check below.
	</div>
	<div class="alert alert-success display-hide">
		<button class="close" data-close="alert"></button>
		Your form validation is successful!
	</div>

	<ul class="nav nav-tabs">
		<li class="active"><a href="#form-pegawai" data-toggle="tab"> Employee </a></li>
		<li><a href="#form-user" data-toggle="tab"> User </a></li>
	</ul>
	{{ form_action|raw }}
	<div class="tab-content">
		<div class="tab-pane fade active in" id="form-pegawai">
			<div class="row">

				<div class="col-md-6">
					<div class="form-group row">
						<label class="control-label col-md-4">Employee Code <span class="required"> * </span>
						</label>
						<div class="col-md-8">
							<input type="text" name="data[nik]" data-required="1" class="form-control" value="{{data.nik}}" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Employee Name<span class="required"> * </span>
						</label>
						<div class="col-md-8">
							<input type="text" name="data[nama_pegawai]" value="{{data.nama_pegawai}}" data-required="1" class="form-control" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Phone Number
						</label>
						<div class="col-md-8">
							<input type="text" name="data[no_hp]" value="{{data.no_hp}}" data-required="1" class="form-control" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Address </label>
						<div class="col-md-8">
							<input type="text" name="data[alamat]" value="{{data.alamat}}" data-required="1" class="form-control" />
						</div>
					</div>
<!-- 					<div class="form-group row">
						<label class="control-label col-md-4">Position / Roles<span class="required"> * </span>
						</label>
						<div class="col-md-8"></div>
					</div> -->
					{{roles|raw}}
		<!-- 			<div class="form-group row">
						<label class="control-label col-md-4">Level<span class="required"> * </span>
						</label>
						<div class="col-md-8">{{levels|raw}}</div>
					</div> -->
					<div class="form-group row">
						<label class="control-label col-md-4">In Date<span class="required"> * </span>
						</label>
						<div class="col-md-8">
							<input type="text" name="data[tgl_masuk]" value="{{data.tgl_masuk}}" data-required="1" class="form-control date-picker" data-date-format="yyyy-mm-dd" />
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group row">
						<label class="control-label col-md-4">Out Date </label>
						<div class="col-md-8">
							<input type="text" name="data[tgl_keluar]" value="{{data.tgl_keluar}}" data-required="1" class="form-control date-picker" data-date-format="yyyy-mm-dd" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Salary </label>
						<div class="col-md-8">
							<input type="text" name="data[salary]" value="{{data.salary}}" data-required="1" class="form-control" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Status </label>
						<div class="col-md-8">
							<input type="text" name="data[status_pegawai]" value="{{data.status}}" data-required="1" class="form-control" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Info </label>
						<div class="col-md-8">
							<input type="text" name="data[keterangan]" value="{{data.keterangan}}" data-required="1" class="form-control" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Branch </label>
						<div class="col-md-8">{{branches|raw}}</div>
					</div>
				</div>

			</div>
		</div>
		<div class="tab-pane fade" id="form-user" style="position: relative; top: 0px">
			<div class="row">
				<div class="col-md-4">
					<div class="form-group row">
						<label class="control-label col-md-4">User Name </label>
						<div class="col-md-8">
							<input type="text" name="data[user_name]" value="{{data.user_name}}" data-required="1" class="form-control" />
							<input type="hidden" name="data[user_id]" value="{{data.user_id}}" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Password </label>
						<div class="col-md-8">
							<input name="data[user_password]" data-required="1" class="form-control" type="password" /> {{additional_info|raw}}
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Confirm Password </label>
						<div class="col-md-8">
							<input name="data[user_confirm_password]" data-required="1" class="form-control" type="password" /> {{additional_info|raw}}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</form>
</div>

<script>
	jQuery(document).ready(function(){ 
		if ($().select2) {
            $('.select2me').select2({
                placeholder: "Select",
                allowClear: true
            });
        }

        if ($().datepicker) {
            $('.date-picker').datepicker({
                rtl: Metronic.isRTL(),
                orientation: "left",
                autoclose: true
            });
            //$('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
        }
	});
</script>
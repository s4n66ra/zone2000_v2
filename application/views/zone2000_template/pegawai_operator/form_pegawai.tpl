<!-- BEGIN PAGE HEADER-->
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<!-- BEGIN PAGE TITLE -->
		<div class="page-title">
			<h1>{{title}}</h1>
		</div>
		<!-- END PAGE TITLE -->
		
	</div>
	<!-- END PAGE HEAD -->

	<!-- END PAGE HEADER-->
	<!-- BEGIN PAGE CONTENT-->					
	<div class="row">
		<div class="col-md-12">
			<!-- BEGIN VALIDATION STATES-->
			<div class="portlet box green">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-gift"></i>
					</div>
				</div>
				<div class="portlet-body form">
					<!-- BEGIN FORM-->
					{{ form_action|raw }}
						<div class="form-body">
							<h3 class="form-section"></h3>
							<div class="alert alert-danger display-hide">
								<button class="close" data-close="alert"></button>
								You have some form errors. Please check below.
							</div>
							<div class="alert alert-success display-hide">
								<button class="close" data-close="alert"></button>
								Your form validation is successful!
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Employee Unique Number<span class="required">
								* </span>
								</label>
								<div class="col-md-4">
									<input type="text" name="nik" data-required="1" class="form-control" value="{{def_nik}}"/>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Empl. Name<span class="required">
								* </span>
								</label>
								<div class="col-md-4">
									<input type="text" name="nama_pegawai" value = "{{def_nama_pegawai}}" data-required="1" class="form-control"/>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">No HP<span class="required">
								* </span>
								</label>
								<div class="col-md-4">
									<input type="text" name="no_hp" value = "{{def_no_hp}}" data-required="1" class="form-control"/>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">Address<span class="required">
								* </span>
								</label>
								<div class="col-md-4">
									<input type="text" name="alamat" value = "{{def_alamat}}" data-required="1" class="form-control"/>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">Position<span class="required">
								* </span>
								</label>
								<div class="col-md-4">
									{{options_jabatan|raw}}
								</div>
							</div>


							<div class="form-group">
								<label class="control-label col-md-3">Level Name<span class="required">
								* </span>
								</label>
								<div class="col-md-4">
									{{options_level|raw}}
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label col-md-3"> In Date<span class="required">
								* </span>
								</label>
								<div class="col-md-3">
									<div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd" data-date-viewmode="years" data-date-minviewmode="months">
										<input type="text" name="tgl_masuk" class="form-control" readonly="" value="{{def_tgl_masuk}}" todayHighlight="true">
										<span class="input-group-btn">
										<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
										</span>
									</div>
									<!-- /input-group -->
									<span class="help-block">
									Select month only </span>
								</div>							
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">Out Date<span class="required">
								* </span>
								</label>
								<div class="col-md-3">
									<div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd" data-date-viewmode="years" data-date-minviewmode="months">
										<input type="text" name="tgl_keluar" class="form-control" value="{{def_tgl_keluar}}" readonly="">
										<span class="input-group-btn">
										<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
										</span>
									</div>
									<!-- /input-group -->
									<span class="help-block">
									Select month only </span>
								</div>							
				
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">Salary<span class="required">
								* </span>
								</label>
								<div class="col-md-4">
									<input type="text" name="salary" value = "{{def_salary}}" data-required="1" class="form-control"/>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Emp. Status<span class="required">
								* </span>
								</label>
								<div class="col-md-4">
									<input type="text" name="status_pegawai" value = "{{def_status_pegawai}}" data-required="1" class="form-control"/>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Annotation<span class="required">
								* </span>
								</label>
								<div class="col-md-4">
									<input type="text" name="keterangan" value = "{{def_keterangan}}" data-required="1" class="form-control"/>
								</div>
							</div>


						</div>
						<div class="form-actions">
							<div class="row">
								<div class="col-md-offset-3 col-md-9">
									{{button_submit|raw}}
								</div>
							</div>
						</div>
					</form>
					<!-- END FORM-->
				</div>
				<!-- END VALIDATION STATES-->
			</div>
		</div>
	</div>
	<!-- END PAGE CONTENT-->
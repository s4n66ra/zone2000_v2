	
	<div class="form-group row">
		<div class="col-md-4">
			Item Name
		</div>
		<div class="col-md-3">
			Gudang 
		</div>
		<div class="col-md-3">
			Gudang Mutasi
		</div>
		<div class="col-md-2">
			
		</div>
	</div>	
	<div class="form-group row">
		<div class="col-md-4">
			{{list.sparepart|raw}}
		</div>
		<div class="col-md-3">
			<input type="text" name="data[qty]" data-required="1" class="form-control format-number" value="{{ data.qty }}" {{readonly}}/>
		</div>
		<div class="col-md-3">
			<input type="text" name="data[qty_mutasi]" data-required="1" class="form-control format-number" value="{{ data.qty_mutasi }}" {{readonly}}/>
		</div>
		<div class="col-md-2">
			{{ button_group_inform|raw }}
		</div>
	</div>	

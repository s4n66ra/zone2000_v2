{{ form_action|raw }}
	
	<div class="row">
		<div class="form-body col-md-12">
			<div class="form-group row">
				<label class="control-label col-md-4"> Machine
				</label>
				<div class="col-md-8">
					<input type="text" data-required="1" class="form-control" value="{{ data.serial_number }} - {{ data.nama_jenis_mesin }}" readonly/>
				</div>
			</div>

			<div class="form-group row">
				<label class="control-label col-md-4"> Store
				</label>
				<div class="col-md-8">
					<input type="text" data-required="1" class="form-control" value="{{ data.nama_cabang }}" readonly/>
				</div>
			</div>

			<div class="form-group row">
				<label class="control-label col-md-4">Problem<span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<textarea type="text" data-required="1" rows="3" class="form-control" readonly>{{ data.issue_description }}</textarea>
				</div>
			</div>

			<div class="form-group row">
				<label class="control-label col-md-4"> Status
				</label>
				<div class="col-md-8">
					{{ list.repair_status|raw }}
				</div>
			</div>

			<div class="form-group row">
				<label class="control-label col-md-4">Notes
				</label>
				<div class="col-md-8">
					<textarea type="text" name="data[repair_description]" data-required="1" rows="3" class="form-control" {{readonly}}>{{ data.repair_description }}</textarea>
				</div>
			</div>

		</div>
	</div>
</form>
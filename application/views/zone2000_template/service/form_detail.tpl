{{ form_action|raw }}
	<div class="row">
		<div class="form-body col-md-12">

			<div class="form-group row">
				<label class="control-label col-md-4">Status<span class="required"> * </span>				
				</label>
				<div class="col-md-8">
					{{ list.status|raw }}
				</div>
			</div>
			<div class="form-group row">
				<label class="control-label col-md-4">Location<span class="required"> * </span>				
				</label>
				<div class="col-md-8">
					{{ list.location|raw }}
				</div>
			</div>

			<div class="form-group row">
				<label class="control-label col-md-4">Keterangan
				</label>
				<div class="col-md-8">
					<textarea type="text" name="data[note]" data-required="1" rows="3" class="form-control" {{readonly}}>{{ data.note }}</textarea>
				</div>
			</div>

		</div>
	</div>
</form>
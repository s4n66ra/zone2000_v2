{{ form_action|raw }}
<div class="row">
	<div class="form-body col-md-12">
		
		<input type="hidden" name="status" value="{{status}}" />

		<div class="row" style="margin-bottom:10px;">
			<div class="col-md-12" style="text-align:right;">
				{% if data.technician_id %}
				<span class="badge badge-success badge-roundless">Assigned</span> 
				{% else %}
				<span class="badge badge-danger badge-roundless">Not Assigned</span> 
				{% endif %}
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">

			{% if act!='new' %}
				{% if act!='confirmation' %}
				<input type="hidden" id="data-status-request" name="data[service_status]" value="{{data.service_status}}">
				<!-- <div class="form-group row">
					<label class="control-label col-md-4">REF <span class="required">
					* </span>
					</label>
					<div class="col-md-8">
						<input type="text" name="temp[kd_service]" data-required="1" class="form-control" value="{{ data.kd_service }}" readonly/>
					</div>
				</div> -->

				<div class="form-group row">
					<label class="control-label col-md-4">Machine <span class="required">
					* </span>
					</label>
					<div class="col-md-8">
						<input type="text" name="temp[machine]" data-required="1" class="form-control" value="{{ data.kd_mesin ~ ' - ' ~ data.nama_jenis_mesin }}" readonly/>
					</div>
				</div>

				<div class="form-group row">
					<label class="control-label col-md-4">Store <span class="required">
					* </span>
					</label>
					<div class="col-md-8">
						<input type="text" name="temp[nama_cabang]" data-required="1" class="form-control" value="{{ data.nama_cabang }}" readonly/>
					</div>
				</div>

				<div class="form-group row">
					<label class="control-label col-md-4">Requested At
					</label>
					<div class="col-md-8">
						<input type="text" name="temp[rec_created]" data-required="1" class="form-control" value="{{ data.rec_created }}" readonly/>						
					</div>
				</div>

				{% if data.date_confirmation %}
				<div class="form-group row">
					<label class="control-label col-md-4">Confirmed At
					</label>
					<div class="col-md-8">
						<input type="text" name="temp[date_confirmation]" data-required="1" class="form-control" value="{{ data.date_confirmation }}" readonly/>						
					</div>
				</div>
				{% endif %}

				<!-- <div class="form-group row">
					<label class="control-label col-md-4">Accepted At
					</label>
					<div class="col-md-8">
						<input type="text" name="temp[tgl_diterima]" data-required="1" class="form-control" value="{{ data.tgl_diterima }}" readonly/>						
					</div>
				</div> -->

				<div class="form-group row">
					<label class="control-label col-md-4">Problem<span class="required">
					* </span>
					</label>
					<div class="col-md-8">
						<textarea type="text" name="temp[issue_description]" data-required="1" rows="3" class="form-control" readonly>{{ data.issue_description }}</textarea>
					</div>
				</div>
				
				<hr>

				<div class="form-group row">
					<label class="control-label col-md-4">Technician<span class="required">
					* </span>
					</label>
					<div class="col-md-8">
						{{ list.teknisi | raw }}
					</div>
				</div>

				{% endif %}

				

				<div class="form-group row">
					<label class="control-label col-md-4">Note<span class="required">
					* </span>
					</label>
					<div class="col-md-8">
						<textarea type="text" name="data[service_note]" data-required="1" rows="3" class="form-control" {{readonly}}>{{ data.service_note }}</textarea>
					</div>
				</div>

			{% else %}

				<input type="hidden" name="data[id_mesin_rusak]" value="{{data.id_mesin_rusak}}">
				<div class="form-group row">
					<label class="control-label col-md-4">Note<span class="required">
					* </span>
					</label>
					<div class="col-md-8">
						<textarea type="text" name="data[service_note]" data-required="1" rows="3" class="form-control" {{readonly}}>{{ data.service_note }}</textarea>
					</div>
				</div>

			{% endif %}

			</div>

		</div>

	</div>
</div>
</form>



<script>
	jQuery(document).ready(function(){ 
		
        $('#button-approve').click(function(){
        	$('#data-status-request').val(1); 
        	// form nya udah cari otomatis form terdekat
        	simpan_data($(this).attr('id'));
        });

        $('#button-reject').click(function(){
        	$('#data-status-request').val(2);        	
        	// form nya udah cari otomatis form terdekat
        	simpan_data($(this).attr('id'));
        });

	});
</script>
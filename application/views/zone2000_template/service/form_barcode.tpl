{{ form_action|raw }}
	<div class="row">
		<div class="form-body col-md-12">
			<div class="form-group row" hidden>
				<label class="control-label col-md-4">Code <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="data[doc_number]" id="doc_number" data-required="1" class="form-control" value="{{data.doc_number}}"/>
				</div>
			</div>
			<div class="form-group row" hidden>
				<label class="control-label col-md-4">Name <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="data[nama_jenis_mesin]" id="nama_jenis_mesin" value = "{{data.nama_jenis_mesin}}" data-required="1" class="form-control"/>
				</div>
			</div>
			
			<div class="form-group row">
				<div class="col-md-12" align="center" style="margin-left:10px;">
					<!-- OPSI CANVAS -->
					<!-- <canvas id="barcode_modal" width="200px" height="70px"> </canvas>-->

					<!-- OPSI HTML -->
					<div id="barcode_modal"></div>
 				</div>
			</div>

			<div class="form-group row">
				<div class="col-md-12" align="center">
					<!-- OPSI CANVAS -->
					<!-- <canvas id="barcode_modal" width="200px" height="70px"> </canvas>-->

					<!-- OPSI HTML -->
					<div>{{data.nama_jenis_mesin}}</div>
 				</div>
			</div>

			<div class="form-group row">
				<div class="col-md-6">
					<label class="control-label" style="float:right; margin-right:0px;">Number of Copies
					</label>					
				</div>

				<div class="col-md-6">
					<div class="col-md-7">
						<select class="form-control" style="float:left;margin-left:0px;" id="numb_copies">
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="10">10</option>
							<option value="20">20</option>
							<option value="50">50</option>
							<option value="100">100</option>
						</select>
						
					</div>

				</div>
					
			</div>

			<div class="form-group row">
				<div class="col-md-12" align="center">
					<!-- OPSI CANVAS -->
					<!-- <canvas id="barcode_modal" width="200px" height="70px"> </canvas>-->

					<!-- OPSI HTML -->
					<div>
						<span id="list_printer">
							
						</span>
					</div>
 				</div>
			</div>
		</div>
	</div>
</form>
{{ form_action|raw }}
	<input type="hidden" name="status" value="{{status}}" />
	<input type="hidden" name="data[service_type]" value="{{data.service_type}}" />
	<div class="row">
		<div class="form-body col-md-12">

			<div class="form-group row">
				<label class="control-label col-md-4">DN <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="doc_number" id="pr_code" data-required="1" class="form-control" value="{{ data.doc_number }}" {{readonly}}/>
				</div>
			</div>

			<div class="form-group row">
				<label class="control-label col-md-4">MACHINE				
				</label>
				<div class="col-md-8">
					{{ list.machine|raw }}
				</div>
			</div>

			<div class="form-group row">
				<label class="control-label col-md-4">Problem<span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<textarea type="text" name="data[issue_description]" data-required="1" rows="3" class="form-control" {{readonly}}>{{ data.issue_description }}</textarea>
				</div>
			</div>

			<div class="form-group row">
				<label class="control-label col-md-4"> Jobs
				</label>
				<div class="col-md-8">
					{{ list.service_type|raw }}
				</div>
			</div>

		</div>
	</div>
</form>
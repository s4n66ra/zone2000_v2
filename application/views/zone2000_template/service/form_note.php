{{ form_action|raw }}
<div class="row">
	<div class="form-body col-md-12">
		<h3 class="form-section"></h3>
		<div class="alert alert-danger display-hide">
			<button class="close" data-close="alert"></button>
			You have some form errors. Please check below.
		</div>
		<div class="alert alert-success display-hide">
			<button class="close" data-close="alert"></button>
			Your form validation is successful!
		</div>

		<input type="hidden" name="status" value="{{status}}" />

		{% if hide_kd!=true %}
		<div class="form-group row">
			<label class="control-label col-md-4">Code PR <span class="required">
			* </span>
			</label>
			<div class="col-md-8">
				<input type="text" name="data[kd_pr]" data-required="1" class="form-control" value="{{ data.kd_pr }}" {{readonly}}/>
			</div>
		</div>
		{% endif %}

		<div class="form-group row">
			<label class="control-label col-md-4">Note<span class="required">
			* </span>
			</label>
			<div class="col-md-8">
				<textarea type="text" name="data[keterangan]" data-required="1" rows="3" class="form-control" {{readonly}}>{{ data.keterangan }}</textarea>
			</div>
		</div>
	</div>
</div>
</form>
<!-- FORM INFO-->
{% include form_info %}
{{ form.info | raw }}
{{ form.info_pdf | raw }}


<!-- BUTTTON GROUP ATAS -->
{% if button_group or button_right or button %}
<div class="table-toolbar">
	<div class="row">
		<div class="col-md-6">
			<div class="btn-group">
				{{ button_group|raw }}
				{{ form_inventory_summary|raw }}
				{{ typename_list|raw }}
			</div>
		</div>
		<div class="col-md-6">
			<div class="btn-group pull-right">
				{{ button_right|raw }}
				{{ button.right|raw }}
			</div>
		</div>
	</div>
</div>
{% endif %}

<!-- FORM MAIN . EX : UNTUK INPUT PR_DETAIL -->
{% if form.main %}
<div class="form-main">
	{{form.main|raw}}
</div>
{% endif %}

<!-- MAIN CONTENT -->
<div id="content_table{{page.id}}" class="grid-table" data-source="{{data_source}}" data-filter="#ffilter">

	{% include content_body %}
  	{{ table.main|raw }}
 	{{ content.body|raw }}
</div>

<!-- BUTTON GROUP BAWAH -->
{% if button_group_2 %}
<div class="table-toolbar">
	<div class="row">
		<div class="col-md-12">
			<div class="btn-group pull-right">
				{{ button_group_2|raw }}
			</div>
		</div>
	</div>
</div>
{% endif %}
	<div class="portlet light modal-body" tabindex="-1">				
		<div class="portlet-title">
			<div class="caption font-purple-plum">
				<i class="icon-speech font-purple-plum"></i>
				<span class="caption-subject bold uppercase"> {{title}}</span>
				{% if subtitle %}
				<span class="caption-helper">right click inside the box</span>
				{% endif %}
			</div>
			<div class="actions">
				
				<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="#" data-original-title="" title="Fullscreen"></a>
				<a class="btn btn-circle btn-icon-only btn-default remove" data-dismiss="modal" title="Fullscreen" href="#">
					<i class="icon-close"></i>
				</a>
				<!-- <a href="javascript:;" class="fullscreen"></a>
				<a href="" class="remove"></a> -->
			</div>
		</div>

		<div class="portlet-body">
			<div class="row">
				<div class="col-md-12">
				{{ form | raw}}
				</div>
			</div>
			
		</div>

	</div>
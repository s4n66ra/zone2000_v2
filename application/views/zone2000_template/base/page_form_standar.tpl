{{ form_action|raw }}
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-title">{{ title }}</h4>
	</div>
	<div class="modal-body">
		<div class="row">
		<div class="col-md-12">
	
						{% include form %}

						<div class="form-actions">
							<div class="row">
								<div class="col-md-offset-3 col-md-9">
									
								</div>
							</div>
						</div>
					


		</div>
	</div>

	</div>
	<div class="modal-footer">
		<div class="row">
			<div class="col-md-12">
				{{button_group|raw}}
			</div>
		</div>
	</div>
</form>
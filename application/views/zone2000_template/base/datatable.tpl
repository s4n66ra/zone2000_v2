<div class="portlet-body">
	<div class="table-container">
		
		<table class="table table-hover" id="datatable_ajax">
			<thead>		
				<tr role="row" class="heading yellow">
					<th width="2%">
						<input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
					</th>
					<th width="5%">
						 DN&nbsp;#
					</th>
					<th width="15%">
						 Store
					</th>
					<th width="15%">
						 Type
					</th>
					<th width="10%">
						 Status
					</th>
					<th width="10%">
						 Date
					</th>
					<th width="10%">
						 Note
					</th>
				</tr>		
			</thead>

			<tbody>

			</tbody>

		</table>
	</div>
</div>
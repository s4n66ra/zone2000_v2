<div class="component-form" id="div-{{id}}" close-modal="{{ close_modal }}">
	{% if only_form %}
		{% if form_open %} {{ form_open|raw }} {% endif %}
		{% include form %}
		{% if form_open %} </form> {% endif %}

	{% else %}
		
		{% if portlet==true or no_portlet==false %}

			<div class="portlet light modal-body" tabindex="-1">				
				<div class="portlet-title">
					{% if isBtnPageBack %}
					{{ btn_page_back() | raw }}
					{% endif %}
					<div class="caption font-purple-plum">
						<i class="icon-speech font-purple-plum"></i>
						<span class="caption-subject bold uppercase"> {{title}}</span>
						{% if subtitle %}
						<span class="caption-helper">{{subtitle}}</span>
						{% endif %}
					</div>
					<div class="actions">
						{% if action_fullscreen %}
						<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="#" data-original-title="" title="Fullscreen"></a>
						{% endif %}
						<a class="btn btn-circle btn-icon-only btn-default remove" data-dismiss="modal" title="Fullscreen" href="#">
							<i class="icon-close"></i>
						</a>
					</div>
				</div>

				<div class="portlet-body">
					<div class="row">
						<div class="col-md-12">
							{% if form_open %} {{ form_open|raw }} {% endif %}
							{% include form %}
							{% if form_open %} </form> {% endif %}
						</div>
					</div>

					<div class="row" style="margin-top:20px;">
						<div class="col-md-12" style="text-align:center;">
							{{button_group|raw}}
						</div>
					</div>

				</div>

			</div>

		{% else %}
			<div class="row">
				<div class="col-md-12" id="wrapper-{{id}}">
					{% if form_open %} {{ form_open|raw }} {% endif %}
					{% include form %}
					{% if form_open %} </form> {% endif %}
				</div>
			</div>

			<div class="row" style="margin-top:20px;">
				<div class="col-md-12" style="text-align:center;">
					{{button_group|raw}}
				</div>
			</div>

		{% endif %}	

	{% endif %}	

</div>
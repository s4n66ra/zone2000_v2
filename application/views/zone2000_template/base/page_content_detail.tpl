
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet light">
						
						<div class="portlet-title">
							{% if isBtnPageBack %}
							{{ btn_page_back() | raw }}
							{% endif %}
							<div class="caption font-purple-plum">
								<i class="icon-speech font-purple-plum"></i>
								<span class="caption-subject bold uppercase"> {{title}}</span>
								{% if subtitle %}
								<span class="caption-helper">{{subtitle}}</span>
								{% endif %}
							</div>
							<div class="actions">
								<!--
								<a class="btn btn-circle btn-icon-only btn-default" href="#">
								<i class="icon-cloud-upload"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="#">
								<i class="icon-wrench"></i>
								</a>
								<a class="btn btn-circle btn-icon-only btn-default" href="#">
								<i class="icon-trash"></i>
								</a>
								-->
								<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="#" data-original-title="" title="">
								</a>
								
							</div>
						</div>

						<div class="portlet-body">

							<!-- FORM INFO-->
							{% include form_info %}
							{{ form.info | raw }}

							<!-- TOOLBAR ATAS -->
							<div class="table-toolbar">
								<div class="row">
									<div class="col-md-6">
										
									</div>
									<div class="col-md-6">
										<div class="btn-group pull-right">
											{{ button_group|raw }}
										</div>
									</div>
								</div>
							</div>

							<!-- TABLE DATAGRID -->
							<div id="content_table_detail" class="grid-table" data-source="{{data_source}}" data-filter="#ffilter">	
								{{ table.main|raw }}
							</div>

							<!-- TOOLBAR BAWAH-->
							<div class="table-toolbar">
								<div class="row">
									
									<div class="col-md-12">
										<div class="btn-group pull-right">
											{{ button_group_2|raw }}
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT -->

<div class="component-page">

{% if portlet is null %}
    {% set portlet = true %}
{% endif %}

{% if portlet %}
<div class="portlet light">	
	<div class="portlet-title">
		{% if isBtnPageBack or button.back %}
		{{ btn_page_back() | raw }}
		{% endif %}
		<div class="caption font-purple-plum">
			<i class="icon-speech font-purple-plum"></i>
			<span class="caption-subject bold uppercase"> {{title}}</span>
			<!-- INI DATEPICKER GLOBAL -->
			

			{% if subtitle %}
			<span class="caption-helper">{{ subtitle }}}</span>
			{% endif %}
		</div>
		<div class="actions">
			{% if btn_close_service or button.close_service %}

					<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="#" data-original-title="" title="">
					</a>
					<a class="btn btn-circle btn-icon-only btn-default remove" data-dismiss="modal" title="Fullscreen" href="#">
						<i class="icon-close"></i>
					</a>
			{% endif %}
			{% if st_date == 1 %}
			<div class="form-group row">
				<label class="control-label col-md-3" id="glob_date_outlet" {{ style }} >Sistem Date </label>
				<div class="col-md-7">
					<div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd">
						<input type="text" name="data[tgl_beli]" class="form-control" id="sistem_date" target={{target}} onChange="changeGlobeDate(this)" value="{{hid_date}}" readonly> <span class="input-group-btn">
							<button class="btn default" type="button">
								<i class="fa fa-calendar"></i>
							</button>
						</span>
					</div>
				</div>
				
				<div class="col-md-1">
					<a class="btn btn-circle btn-icon-only btn-default fullscreen" href="#" data-original-title="" title="">
					</a>
					{% if btn_close or button.close %}
					<a class="btn btn-circle btn-icon-only btn-default remove" data-dismiss="modal" title="Fullscreen" href="#">
						<i class="icon-close"></i>
					</a>
					{% endif %}
				</div>
			</div>


			<!-- <span>System Date &nbsp;<input name="sistem_date" type="text" class="date-picker" data-date-format="yyyy-mm-dd" id="glob_date_outlet" style="border-radius: 10px;font-weight:bold;" onChange="changeGlobeDate(this)" values="{{sistem_date}}" /></span>
			<input type="hidden" name="hid_date" id="hid_date" values="{{hid_date}}"> -->
			{% endif %}
			<div style="display:inline;margin-right:10px;">
			{{ button.portlet |raw }}
			</div>
			<!-- <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="#" data-original-title="" title="">
			</a>
			{% if btn_close or button.close %}
			<a class="btn btn-circle btn-icon-only btn-default remove" data-dismiss="modal" title="Fullscreen" href="#">
				<i class="icon-close"></i>
			</a>
			{% endif %} -->
			
		</div>
	</div>

	<div class="portlet-body">
		{% include 'base/page_content_body.tpl' %}
	</div>
</div>
{% else %}
	{% include 'base/page_content_body.tpl' %}
{% endif %}

</div>
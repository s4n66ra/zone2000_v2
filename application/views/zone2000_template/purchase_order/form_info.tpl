		<div class="row">
			<div class="col-md-4">
				<div class="form-group row">
					<label class="control-label col-md-4">PR 
					
					</label>
					<div class="col-md-8">
						<input type="hidden" name="id" value="{{data.id_pr}}">
						<input type="text" name="data[doc_number]" data-required="1" class="form-control" value="{{ data.doc_number }}" {{readonly}}/>
					</div>
				</div>
				<div class="form-group row">
					<label class="control-label col-md-4">STORE ID 
					
					</label>
					<div class="col-md-8">
						<input type="text" name="data[kd_cabang]" value = "{{data.kd_cabang}}" data-required="1" class="form-control green" {{readonly}}/>
					</div>
				</div>

				<div class="form-group row">
					<label class="control-label col-md-4">STORE NAME
					</label>
					<div class="col-md-8">
						<input type="text" name="data[nama_cabang]" value = "{{data.nama_cabang}}" data-required="1" class="form-control" {{readonly}}/>
					</div>
				</div>

				<div class="form-group row">
					<label class="control-label col-md-4">DATE
					</label>
					<div class="col-md-8">
						<input type="text" name="data[date_created]" value = "{{data.date_created}}" data-required="1" class="form-control" {{readonly}}/>
					</div>
				</div>

			</div>

			<div class="col-md-4">
				<div class="form-group row">
					<label class="control-label col-md-4">NOTE 
					
					</label>
					<div class="col-md-8">
						<textarea name="data[pr_note]" class="form-control" rows="5" {{readonly}} >{{data.pr_note}}</textarea>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				{{ status_pr|raw }}
				{{ revisi_pr|raw }}
			</div>
		</div>
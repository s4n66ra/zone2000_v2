{{ form_action|raw }}
<div class="form-body">
	<h3 class="form-section"></h3>
	<div class="alert alert-danger display-hide">
		<button class="close" data-close="alert"></button>
		You have some form errors. Please check below.
	</div>
	<div class="alert alert-success display-hide">
		<button class="close" data-close="alert"></button>
		Your form validation is successful!
	</div>
	<div class="form-group row">
		<label class="control-label col-md-2">Machine ID</label><span class="required"> * </span>
		<div class="col-md-4">
			<input type="text" name="data[kd_mesin]" data-required="1" class="form-control" value="{{ data.kd_mesin }}" {{readonly}}/>
		</div>
		
		<label class="control-label col-md-2">Supplier<span class="required"> * </span>
		</label>
		<div class="col-md-4">{{list_supplier|raw}}</div>
	</div>
	<div class="form-group row">
		<label class="control-label col-md-2">Serial Number </label><span class="required"> * </span>
		<div class="col-md-4">
			<input type="text" name="data[serial_number]" value="{{data.serial_number}}" data-required="1" class="form-control green" {{readonly}}/>
		</div>
		
		<label class="control-label col-md-2">Machine Type<span class="required"> * </span>
		</label>
		<div class="col-md-4">{{list_jenis_mesin|raw}}</div>
	</div>

	<div class="form-group row">
		<label class="control-label col-md-2">Price </label><span class="required"> * </span>
		<div class="col-md-4">
			<input type="text" name="data[harga]" class="form-control" rows="5" value="{{data.harga}}" />
		</div>
		
		<label class="control-label col-md-2">Branch<span class="required"> * </span>
		</label>
		<div class="col-md-4">{{list_cabang|raw}}</div>
	</div>

	<div class="form-group row">
		<label class="control-label col-md-2">Buy Date </label><span class="required"> * </span>
		<div class="col-md-4">
			<div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd">
				<input type="text" name="data[tgl_beli]" class="form-control" value="{{data.tgl_beli}}" readonly> <span class="input-group-btn">
					<button class="btn default" type="button">
						<i class="fa fa-calendar"></i>
					</button>
				</span>
			</div>
		</div>
		
		<label class="control-label col-md-2">Input Type<span class="required"> * </span>
		</label>
		<div class="col-md-4">{{list_koin|raw}}</div>
	</div>

	<div class="form-group row">
		<label class="control-label col-md-2">Guarantee </label><span class="required"> * </span>
		<div class="col-md-4">
			<div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd">
				<input type="text" name="data[garansi_toko]" class="form-control" value="{{data.garansi_toko}}" readonly> <span class="input-group-btn">
					<button class="btn default" type="button">
						<i class="fa fa-calendar"></i>
					</button>
				</span>
			</div>
		</div>
		
		<label class="control-label col-md-2">Owner<span class="required"> * </span>
		</label>
		<div class="col-md-4">{{list_owner|raw}}</div>
	</div>

	<div class="form-group row">
		<label class="control-label col-md-2">Status </label>
		<div class="col-md-4">
			{{status|raw}}
		</div>
	</div>
</div>
</form>

<script>
	jQuery(document).ready(function(){ 
		if ($().select2) {
            $('.select2me').select2({
                placeholder: "Select",
                allowClear: true
            });
        }

        if ($().datepicker) {
            $('.date-picker').datepicker({
                rtl: Metronic.isRTL(),
                orientation: "left",
                autoclose: true
            });
            //$('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
        }
	});
</script>
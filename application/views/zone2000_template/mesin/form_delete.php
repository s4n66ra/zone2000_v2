<div class="row-fluid">
    <div class="box-title">
        <?php echo (isset($title)) ? $title : 'Untitle'; ?>
    </div>
    <div class="box-content">
        <?php
        $hidden_form = array('id' => !empty($id) ? $id : '');
        echo form_open_multipart($form_action, array('id' => 'finput', 'class' => 'form-horizontal'), $hidden_form);
        ?>

        <div class="control-group">
            <label for="select" class="control-label">Tanggal<span class="required">*</span></label>
            <div class="controls">
                <div class="input-append date" id="dp3" style="margin-right: 30px;" data-date="<?php echo!empty($default->tanggal_bbm) ? date('d-m-Y', strtotime($default->tanggal_bbm)) : date('d-m-Y'); ?>" data-date-format="dd-mm-yyyy">
                    <?php echo form_input('tgl_beli', !empty($default->tgl_beli) ? date('d-m-Y', strtotime($default->tgl_beli)) : date('d-m-Y'), 'class="span12 onchange date" readonly=""'); ?>
                    <span class="add-on"><i class="icon-calendar"></i></span>
                </div>
            </div>
        </div>

        <div class="control-group">
            <label for="select" class="control-label">Kode mesin<span class="required">*</span></label>
            <div class="controls">
                <?php echo form_input('kd_mesin', !empty($default->kd_mesin) ? $default->kd_mesin : '', 'class="span6"') ?>
                
            </div>
        </div> 

        <div class="control-group">
            <label for="select" class="control-label">Nama mesin<span class="required">*</span></label>
            <div class="controls">
                <?php echo form_input('nama_mesin', !empty($default->nama_mesin) ? $default->nama_mesin : '', 'class="span6"') ?>
                
            </div>
        </div> 

        <div class="control-group">
            <label for="select" class="control-label">Harga<span class="required">*</span></label>
            <div class="controls">
                <?php echo form_input('harga', !empty($default->harga) ? $default->harga : '', 'class="span6"') ?>
                
            </div>
        </div> 


        <div class="control-group">
            <label for="select" class="control-label">Kelas<span class="required">*</span></label>
            <div class="controls">
                <?php echo form_dropdown('id_kelas',$options_kelas, !empty($default->id_kelas) ? $default->id_kelas : '', 'class="span6 onchange chosen"') ?>  
            </div>
        </div> 

        <div class="control-group">
            <label for="select" class="control-label">Kategori Mesin<span class="required">*</span></label>
            <div class="controls">
                <?php echo form_dropdown('id_kategori_mesin',$options_kategori_mesin, !empty($default->id_kategori_mesin) ? $default->id_kategori_mesin : '', 'class="span6 onchange chosen"') ?>  
            </div>
        </div> 

        <div class="control-group">
            <label for="select" class="control-label">Supplier<span class="required">*</span></label>
            <div class="controls">
                <?php echo form_dropdown('id_supplier',$options_supplier, !empty($default->id_supplier) ? $default->id_supplier : '', 'class="span6 onchange chosen"') ?>  
            </div>
        </div> 

        <div class="control-group">
            <label for="select" class="control-label">PO<span class="required">*</span></label>
            <div class="controls">
                <?php echo form_dropdown('id_po',$options_po, !empty($default->id_po) ? $default->id_po : '', 'class="span6 onchange chosen"') ?>  
            </div>
        </div> 

        <div class="control-group">
            <label for="select" class="control-label">Garansi Toko<span class="required">*</span></label>
            <div class="controls">
                <?php echo form_input('garansi_toko', !empty($default->garansi_toko) ? $default->garansi_toko : '', 'class="span6"') ?>                
            </div>
        </div>

        <div class="form-actions">
            <?php echo anchor(null, '<i class="icon-trash"></i> Hapus', array('id' => 'button-delete', 'class' => 'red btn', 'onclick' => "simpan_data(this.id, '#finput', '#button-back')")); ?>
            <?php echo anchor(null, '<i class="icon-circle-arrow-left"></i> Kembali', array('id' => 'button-back', 'class' => 'btn', 'onclick' => 'close_form_modal(this.id)')); ?>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('.date').datepicker({
            format: 'dd-mm-yyyy',
        });
        $('.chosen').chosen();
        $('.numeric').numeric();
    });

    function refresh_filter(){ 
        load_table('#content_table', 1,function(){

        });
    }
</script>

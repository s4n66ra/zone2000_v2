{{ form_action|raw }}
	<div class="form-body col-md-12">
		<h3 class="form-section"></h3>
		<div class="alert alert-danger display-hide">
			<button class="close" data-close="alert"></button>
			You have some form errors. Please check below.
		</div>
		<div class="alert alert-success display-hide">
			<button class="close" data-close="alert"></button>
			Your form validation is successful!
		</div>

		<input type="hidden" name="status" value="{{status}}" />

		{% if is_head_office %}
		<div class="form-group row">
			<label class="control-label col-md-4">Store <span class="required">
			* </span>
			</label>
			<div class="col-md-8">
				{{ list_store|raw }}
			</div>
		</div>
		{% endif %}

		{% if hide_kd!=true %}
		<div class="form-group row">
			<label class="control-label col-md-4">NO. PP <span class="required">
			* </span>
			</label>
			<div class="col-md-8">
				<input type="text" name="data[pr_code]" data-required="1" id="pr_code" class="form-control" value="{{ data.pr_code }}" readonly/>
			</div>
		</div>
		

		<div class="form-group row">
			<label class="control-label col-md-4">Request For<span class="required">
			* </span>
			</label>
			<div class="col-md-8">
				<select class="bs-select form-control" name="data[pr_type]" value="{{data.pr_type}}">
					<option value="7">Machine</option>
					<option value="2">Merchandise</option>
					<option value="3">Sparepart</option>
					<option value="4">Ticket</option>
				</select>
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-md-4">Divisi<span class="required">
			* </span>
			</label>
			<div class="col-md-8">
				{{divisi|raw}}
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-md-4">Note
			
			</label>
			<div class="col-md-8">
				<textarea type="text" name="data[pr_note]" data-required="1" rows="3" class="form-control" {{readonly}}>{{ data.pr_note }}</textarea>
			</div>
		</div>
		{% else %}
		<div class="row text-center">
			<div class="col-md-12">
				<h4>Are you sure Raise Purchase Request?</h4>
			</div>
		</div>
		{% endif %}

		

	</div>
</form>
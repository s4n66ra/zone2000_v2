{{ form_action|raw }}
	<div class="form-body" id="form-body-id">
		
		<h3 class="form-section"></h3>
		<div class="alert alert-danger display-hide">
			<button class="close" data-close="alert"></button>
			You have some form errors. Please check below.
		</div>
		<div class="alert alert-success display-hide">
			<button class="close" data-close="alert"></button>
			Your form validation is successful!
		</div>
		
		<input type="hidden" name="data[pr_id]" value="{{ data.pr_id }}" id="pr_id"/>

		<div class="form-group row">
			<label class="control-label col-md-4">Item <span class="required">
			* </span>
			</label>
			<div class="col-md-8">
				{{ list_item|raw }}
			</div>
		</div>
		
		<div class="form-group row">
			<label class="control-label col-md-4">Supplier <span class="required">
			* </span>
			</label>
			<div class="col-md-8">
				{{ list_supplier|raw }}
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-md-4">Qty (In Units) <span class="required">
			* </span>
			</label>
			<div class="col-md-8">
				<input type="text" id="modal_qty_item" name="data[qty_temp]" data-required="1" class="form-control format-number" value="{{ data.qty_temp }}" {{readonly}}/>
			</div>
		</div>

		<!-- <div class="form-group row">
			<label class="control-label col-md-4">Note<span class="required">
			* </span>
			</label>
			<div class="col-md-8">
				<textarea type="text" name="data[keterangan]" value = "{{data.keterangan}}" data-required="1" class="form-control" {{readonly}}>{{data.keterangan}}</textarea>
			</div>
		</div> -->
	</div>
</form>
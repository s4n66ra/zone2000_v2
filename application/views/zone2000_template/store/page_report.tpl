<div class="row">
		
	<div class="col-md-12">
		<div class="tabbable-custom">
			<ul class="nav nav-tabs ">
				<li class="active">
					<a href="#tab_store_report_1" data-toggle="tab">INCOME</a>
				</li>
				<li>
					<a href="#tab_store_report_2" data-toggle="tab">CARD & MEMBERSHIP</a>
				</li>

				<li>
					<a href="#tab_store_report_3" data-toggle="tab">EMPLOYEE</a>
				</li>
			</ul>
			
			<div class="tab-content">
				
				<div class="tab-pane active" id="#tab_store_report_1">
					{{ report.income|raw }}
				</div>

				<div class="tab-pane" id="#tab_store_report_2">
					{{ report.membership|raw }}
				</div>

				<div class="tab-pane" id="#tab_store_report_3">
					{{ report.employee|raw }}
				</div>

				<div class="tab-pane" id="#tab_store_report_4">
					
				</div>
				
			</div>
		</div>
	</div>
	

</div>
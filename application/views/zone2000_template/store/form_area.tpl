{{ form_action|raw }}
		<div class="form-group row">
			<label class="control-label col-md-4">Store Type
			
			</label>
			<div class="col-md-8">
				{{ list.storetype|raw }}
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-md-4"> Store Area 
			
			</label>
			<div class="col-md-8">
				<div class="input-group">
					<input type="text" name="data[store_area]" data-required="1" url-action="data[store_area]" class="form-control format-number" value="{{data.store_area}}" {{readonly}}/>
					<span class="input-group-addon">
						m<sup>2</sup>
					</span>
				</div>
				
			</div>
		</div>
		{% if data.status_ps == 0 %}
		<div class="form-group row">
			<label class="control-label col-md-4">Service Charge 
			
			</label>
			<div class="col-md-8">
				<div class="input-group">
					<input type="text" name="data[service_charge]" url-action="data[service_charge]" value = "{{data.service_charge}}" data-required="1" class="form-control format-number" {{readonly}}/>
					<span class="input-group-addon">
						/m<sup>2</sup>
					</span>
				</div>
				
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-md-4">Rent Charge 
			
			</label>
			<div class="col-md-8">
				<div class="input-group">
					<input type="text" name="data[rent_charge]" url-action="data[rent_charge]" data-required="1" class="form-control format-number" value="{{data.rent_charge}}" {{readonly}}/>
					<span class="input-group-addon">
						/m<sup>2</sup>
					</span>
				</div>
				
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-md-4">Rent Period
			</label>
			<div class="col-md-8">
				<div class="input-group input-large date-picker input-daterange" data-date="10/11/2012" data-date-format="yyyy-mm-dd">
					<input type="text" class="form-control" name="data[rent_date_start]" url-action="data[rent_date_start]" value="{{data.rent_date_start}}">
					<span class="input-group-addon">
					to </span>
					<input type="text" class="form-control" name="data[rent_date_end]" url-action="data[rent_date_end]" value="{{data.rent_date_end}}">
				</div>
			</div>
		</div>

		<div class="form-group row" hidden="true">
			<label class="control-label col-md-4">Prosentase Profit Sharing</label>
			<div class="col-md-8">
				<div class="input-group">
					<input type="text" name="" url-action="data[percentage_ps]" data-required="1" class="form-control format-number" value="{{data.percentage_ps}}"/><span class="input-group-addon">%<sup></sup></span>
				</div>
			</div>
		</div>
		{% endif %}
		{% if data.status_ps == 1 %}
		<div class="form-group row" hidden="true">
			<label class="control-label col-md-4">Service Charge 
			
			</label>
			<div class="col-md-8">
				<div class="input-group">
					<input type="text" name="data[service_charge]" url-action="data[service_charge]" value = "{{data.service_charge}}" data-required="1" class="form-control format-number" {{readonly}}/>
					<span class="input-group-addon">
						/m<sup>2</sup>
					</span>
				</div>
				
			</div>
		</div>

		<div class="form-group row" hidden="true">
			<label class="control-label col-md-4">Rent Charge 
			
			</label>
			<div class="col-md-8">
				<div class="input-group">
					<input type="text" name="data[rent_charge]" url-action="data[rent_charge]" data-required="1" class="form-control format-number" value="{{data.rent_charge}}" {{readonly}}/>
					<span class="input-group-addon">
						/m<sup>2</sup>
					</span>
				</div>
				
			</div>
		</div>

		<div class="form-group row" hidden="true">
			<label class="control-label col-md-4">Rent Period
			</label>
			<div class="col-md-8">
				<div class="input-group input-large date-picker input-daterange" data-date="10/11/2012" data-date-format="yyyy-mm-dd">
					<input type="text" class="form-control" name="data[rent_date_start]" url-action="data[rent_date_start]" value="{{data.rent_date_start}}">
					<span class="input-group-addon">
					to </span>
					<input type="text" class="form-control" name="data[rent_date_end]" url-action="data[rent_date_end]" value="{{data.rent_date_end}}">
				</div>
			</div>
		</div>

		<div class="form-group row" >
			<label class="control-label col-md-4">Prosentase Profit Sharing</label>
			<div class="col-md-8">
				<div class="input-group">
					<input type="text" name="" url-action="data[percentage_ps]" data-required="1" class="form-control format-number" value="{{data.percentage_ps}}"/><span class="input-group-addon">%<sup></sup></span>
				</div>
			</div>
		</div>
		{% endif %}

</form>
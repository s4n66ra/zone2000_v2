{{ form_action|raw }}
	<div class="row" style="margin:15px;">
		<div class="col-md-4 form-body">
			<div class="form-group row">
				<label class="control-label col-md-4"> Year
				</label>
				<div class="col-md-8">
					{{ list.year|raw }}
				</div>
			</div>
		</div>
	</div>

	<div class="row" style="margin:15px;" id="form-target-part">
		{{ form_part|raw }}
	</div>

</form>
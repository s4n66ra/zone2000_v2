
<div class="form-body">
	<h3 class="form-section"></h3>
	<div class="alert alert-danger display-hide">
		<button class="close" data-close="alert"></button>
		You have some form errors. Please check below.
	</div>
	<div class="alert alert-success display-hide">
		<button class="close" data-close="alert"></button>
		Your form validation is successful!
	</div>

	<div class="row">
		
			<div class="col-md-12">
				<div class="tabbable-custom">
					<ul class="nav nav-tabs ">
						<li class="active">
							<a href="#tab_15_1" data-toggle="tab">
							General</a>
						</li>
						<li>
							<a href="#tab_15_2" data-toggle="tab">
							Setting </a>
						</li>

						<li>
							<a href="#tab_15_3" data-toggle="tab">
							Additional Data </a>
						</li>

						<li>
							<a href="#tab_15_4" data-toggle="tab">
							Store Area </a>
						</li>
					</ul>
					
					<div class="tab-content">
						
						<div class="tab-pane active" id="tab_15_1">
							{{ form_action|raw }}
							<div class="row" style="margin:15px;">
								<div class="col-md-6 form">
									<div class="form-group row">
										<label class="control-label col-md-4">Store ID 
										
										</label>
										<div class="col-md-8">
											<input type="text" name="data[kd_cabang]" data-required="1" class="form-control" value="{{ data.kd_cabang }}" {{readonly}}/>
										</div>
									</div>
									<div class="form-group row">
										<label class="control-label col-md-4">Store Name 
										
										</label>
										<div class="col-md-8">
											<input type="text" name="data[nama_cabang]" value = "{{data.nama_cabang}}" data-required="1" class="form-control green" {{readonly}}/>
										</div>
									</div>

									<div class="form-group row">
										<label class="control-label col-md-4">Full Address 
										
										</label>
										<div class="col-md-8">
											<textarea name="data[alamat]" class="form-control" rows="5">{{data.alamat}}</textarea>
										</div>
									</div>

									<div class="form-group row">
										<label class="control-label col-md-4">Phone
										</label>
										<div class="col-md-8">
											<input type="text" name="data[telp]" value = "{{data.telp}}" data-required="1" class="form-control" {{readonly}}/>
										</div>
									</div>

									<div class="form-group row">
										<label class="control-label col-md-4">Fax
										</label>
										<div class="col-md-8">
											<input type="text" name="data[fax]" value = "{{data.fax}}" data-required="1" class="form-control" {{readonly}}/>
										</div>
									</div>

									<div class="form-group row">
										<label class="control-label col-md-4">Started At
										</label>
										<div class="col-md-8">
											<div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd">
												<input type="text" name="data[date_start]" class="form-control" value="{{data.date_start}}" readonly>
												<span class="input-group-btn">
													<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
										</div>
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group row">
										<label class="control-label col-md-4">Country
										
										</label>
										<div class="col-md-8">
											{{ list_country|raw }}
										</div>
									</div>
									<div class="form-group row">
										<label class="control-label col-md-4">Island
										
										</label>
										<div class="col-md-8">
											{{ list_island|raw }}
											<!-- <select class="form-control select2me" data-placeholder="Select...">
												<option value="AL">Alabama</option>
												<option value="WY">Wyoming</option>
											</select> -->
											<!-- <input type="text" name="nama_supplier" value = "{{def_nama_supplier}}" data-required="1" class="form-control" {{readonly}}/> -->
										</div>
									</div>
									<div class="form-group row">
										<label class="control-label col-md-4">Province 
										
										</label>
										<div class="col-md-8">
											{{ list_province|raw }}
										</div>
									</div>
									<div class="form-group row">
										<label class="control-label col-md-4">City
										
										</label>
										<div class="col-md-8">
											{{ list_city|raw }}
										</div>
									</div>

									<h3 class="form-section"></h3>

									<div class="form-group row">
										<label class="control-label col-md-4">Teritory Head 
										
										</label>
										<div class="col-md-8">
											{{ list_area|raw }}
										</div>
									</div>
								</div>
								</form>
							</div>
						</div>

						<div class="tab-pane" id="tab_15_2">
							<div class="row" style="margin:15px;">
								<div class="col-md-6">
									<div class="form-group row">
										<label class="control-label col-md-4">Store Owner 
										
										</label>
										<div class="col-md-8">
											{{ list_owner|raw }}
										</div>
									</div>
									<h3 class="form-section"></h3>
									<div class="form-group row">
										<label class="control-label col-md-4">Merchandise Ratio 
										
										</label>
										<div class="col-md-8">
											<div class="input-group margin-top-10">
												<input type="text" name="data[setting][merchandise_ratio]" data-required="1" class="form-control" value="{{data.merchandise_ratio}}" {{readonly}}/>
												<span class="input-group-addon">
													%
												</span>
											</div>
											
										</div>
									</div>
									

								</div>

								<div class="col-md-6">
									
									<div class="form-group row">
										<label class="control-label col-md-4">Coin Regular
										
										</label>
										<div class="col-md-8">
											<input type="text" name="data[setting][coin_regular]" value = "{{data.coin_regular}}" data-required="1" class="form-control" {{readonly}}/>
										</div>
									</div>

									<div class="form-group row">
										<label class="control-label col-md-4">Coin Acrylic 
										
										</label>
										<div class="col-md-8">
											<input type="text" name="data[setting][coin_acrylic]" data-required="1" class="form-control" value="{{data.coin_acrylic}}" {{readonly}}/>
										</div>
									</div>
									<div class="form-group row">
										<label class="control-label col-md-4">Wristband 
										
										</label>
										<div class="col-md-8">
											<input type="text" name="data[setting][wristband]" value = "{{data.wristband}}" data-required="1" class="form-control" {{readonly}}/>
										</div>
									</div>

									<!-- <div class="form-group row">
										<label class="control-label col-md-4">Budget 
										
										</label>
										<div class="col-md-8">
											<div class="input-group margin-top-10">
												<span class="input-group-addon">
													Rp
												</span>
												<input type="text" name="data[setting][budget]" value = "{{data.budget}}" data-required="1" class="form-control" {{readonly}}/>
												
											</div>
											
										</div>
									</div> -->

									
									
								</div>
							</div>
						</div>

						<div class="tab-pane" id="tab_15_3">
							<div class="row" style="margin:15px;">
								<div class="col-md-6 col-xs-12">
									{{ form_data|raw }}
								</div>

								<div class="col-md-6 col-xs-12">
									{{ table_data|raw }}
								</div>								

							</div>
						</div>

						<div class="tab-pane" id="tab_15_4">
							<div class="row" style="margin:15px;">
								<div class="col-md-6 col-xs-12">
									{{ form.store_area|raw }}
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
			
		
	</div>

</div>

<script>
	jQuery(document).ready(function(){ 
		if ($().select2) {
            $('.select2me').select2({
                placeholder: "Select",
                allowClear: true
            });
        }

        if ($().datepicker) {
            $('.date-picker').datepicker({
                rtl: Metronic.isRTL(),
                orientation: "left",
                autoclose: true
            });

            $(".date-picker-month").datepicker( {
            	rtl: Metronic.isRTL(),
                orientation: "left",
                autoclose: true,
			    format: "mm-yyyy",
			    viewMode: "months", 
			    minViewMode: "months"
			});

            //$('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
        }
	});
</script>
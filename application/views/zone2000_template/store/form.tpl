{{ form_action|raw }}
	<div class="row" style="margin:15px;">
		<div class="col-md-6 form-body">
			<div class="form-group row">
				<label class="control-label col-md-4">Store ID 
				
				</label>
				<div class="col-md-8">
					<input type="text" name="data[kd_cabang]" data-required="1" class="form-control" value="{{ data.kd_cabang }}" {{readonly}}/>
				</div>
			</div>
			<div class="form-group row">
				<label class="control-label col-md-4">Store Name 
				
				</label>
				<div class="col-md-8">
					<input type="text" name="data[nama_cabang]" value = "{{data.nama_cabang}}" data-required="1" class="form-control green" {{readonly}}/>
				</div>
			</div>

			<div class="form-group row">
				<label class="control-label col-md-4">Owner
				
				</label>
				<div class="col-md-8">
					{{ list.owner|raw }}
				</div>
			</div>

			<div class="form-group row">
				<label class="control-label col-md-4">Full Address 
				
				</label>
				<div class="col-md-8">
					<textarea name="data[alamat]" class="form-control" rows="5">{{data.alamat}}</textarea>
				</div>
			</div>

			<div class="form-group row">
				<label class="control-label col-md-4">Phone
				</label>
				<div class="col-md-8">
					<input type="text" name="data[telp]" value = "{{data.telp}}" data-required="1" class="form-control" {{readonly}}/>
				</div>
			</div>

			<div class="form-group row">
				<label class="control-label col-md-4">Fax
				</label>
				<div class="col-md-8">
					<input type="text" name="data[fax]" value = "{{data.fax}}" data-required="1" class="form-control" {{readonly}}/>
				</div>
			</div>

			<div class="form-group row">
				<label class="control-label col-md-4">Started At
				</label>
				<div class="col-md-8">
					<div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd">
						<input type="text" name="data[date_start]" class="form-control" value="{{data.date_start}}" readonly>
						<span class="input-group-btn">
							<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
						</span>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-6 form-body">
			<div class="form-group row">
				<label class="control-label col-md-4">Country
				
				</label>
				<div class="col-md-8">
					{{ list.country|raw }}
				</div>
			</div>
			<div class="form-group row">
				<label class="control-label col-md-4">Island
				
				</label>
				<div class="col-md-8">
					{{ list.island|raw }}
					<!-- <select class="form-control select2me" data-placeholder="Select...">
						<option value="AL">Alabama</option>
						<option value="WY">Wyoming</option>
					</select> -->
					<!-- <input type="text" name="nama_supplier" value = "{{def_nama_supplier}}" data-required="1" class="form-control" {{readonly}}/> -->
				</div>
			</div>
			<div class="form-group row">
				<label class="control-label col-md-4">Province 
				
				</label>
				<div class="col-md-8">
					{{ list.province|raw }}
				</div>
			</div>
			<div class="form-group row">
				<label class="control-label col-md-4">City
				
				</label>
				<div class="col-md-8">
					{{ list.city|raw }}
				</div>
			</div>
			<h3 class="form-section"></h3>

			<div class="form-group row">
				<label class="control-label col-md-4">Area 
				
				</label>
				<div class="col-md-8">
					{{ list.area|raw }}
				</div>
			</div>
			<div class="form-group row">
				<label class="control-label col-md-4">Kode Lokasi
				</label>
				<div class="col-md-8">
					<input type="text" name="data[kode_lokasi]" value = "{{data.kode_lokasi}}" data-required="1" class="form-control" {{readonly}}/>
				</div>
			</div>
			<div class="form-group row">
				<label class="control-label col-md-4">Kode Center Tekno
				</label>
				<div class="col-md-8">
					<input type="text" name="data[kode_center_tekno]" value = "{{data.kode_center_tekno}}" data-required="1" class="form-control" {{readonly}}/>
				</div>
			</div>
		</div>
		
	</div>

</form>

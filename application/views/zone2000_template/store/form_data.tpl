{{ form_action|raw }}
	<div class="form-group row">
		<label class="control-label col-md-4">Data For
		</label>
		<div class="col-md-8">
			<div class="row">
			<div class="col-md-6"> {{list_month|raw}} </div>
			<div class="col-md-6"> {{list_year|raw}} </div>
			</div>
		</div>
	</div>

	<div class="form-group row">
		<label class="control-label col-md-4"> Store Area 
		
		</label>
		<div class="col-md-8">
			<div class="input-group margin-top-10">
				<input type="text" name="data[store_area]" data-required="1" class="form-control format-number" value="{{data.store_area}}" {{readonly}}/>
				<span class="input-group-addon">
					m<sup>2</sup>
				</span>
			</div>
			
		</div>
	</div>

	<div class="form-group row">
		<label class="control-label col-md-4">Service Charge 
		
		</label>
		<div class="col-md-8">
			<div class="input-group margin-top-10">
				<input type="text" name="data[service_charge]" value = "{{data.service_charge}}" data-required="1" class="form-control format-number" {{readonly}}/>
				<span class="input-group-addon">
					/m<sup>2</sup>
				</span>
			</div>
			
		</div>
	</div>

	<div class="form-group row">
		<label class="control-label col-md-4">Target 
		
		</label>
		<div class="col-md-8">
			<input type="text" name="data[target]" value = "{{data.target}}" data-required="1" class="form-control format-number" {{readonly}}/>
		</div>
	</div>

	<div class="form-group row">
		<label class="control-label col-md-4">Rent Charge 
		
		</label>
		<div class="col-md-8">
			<div class="input-group margin-top-10">
				<input type="text" name="data[rent_charge]" data-required="1" class="form-control format-number" value="{{data.rent_charge}}" {{readonly}}/>
				<span class="input-group-addon">
					/m<sup>2</sup>
				</span>
			</div>
			
		</div>
	</div>

	<div class="form-group row">
		<label class="control-label col-md-4">Rent Period
		</label>
		<div class="col-md-8">
			<div class="input-group input-large date-picker input-daterange" data-date="10/11/2012" data-date-format="yyyy-mm-dd">
				<input type="text" class="form-control" name="data[rent_date_start]" value="{{data.rent_date_start}}">
				<span class="input-group-addon">
				to </span>
				<input type="text" class="form-control" name="data[rent_date_end]" value="{{data.rent_date_end}}">
			</div>
		</div>
	</div>

</form>
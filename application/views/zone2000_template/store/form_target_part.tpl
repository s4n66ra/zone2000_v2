<div class="col-md-6 form-body">
	{% for i in range(1, 6) %}
	<div class="form-group row">
		<label class="control-label col-md-4"> {{ getMonthName(i) }}
		</label>
		<div class="col-md-8">
			<div class="input-group">
				<span class="input-group-addon">
					Rp
				</span>
				<input type="text" name="data[month][{{i}}]" value = "{{data[i]}}" data-required="1" class="form-control format-number" {% if i<month %} readonly {% endif %}/>
				
			</div>
		</div>
	</div>
	{% endfor %}
</div>

<div class="col-md-6 form-body">
	{% for i in range(7, 12) %}
	<div class="form-group row">
		<label class="control-label col-md-4"> {{ getMonthName(i) }}
		</label>
		<div class="col-md-8">
			<div class="input-group">
				<span class="input-group-addon">
					Rp
				</span>
				<input type="text" name="data[month][{{i}}]" value = "{{data[i]}}" data-required="1" class="form-control format-number" {% if i<month %} readonly {% endif %}/>
				
			</div>
		</div>
	</div>
	{% endfor %}
</div>

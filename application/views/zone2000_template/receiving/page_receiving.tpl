<div class="tabbable-custom show-overflow">
	<ul class="nav nav-tabs ">
		<li class="active">
			<a href="#tab_15_1" data-toggle="tab">
			IN TRANSIT</a>
		</li>
		<li>
			<a href="#tab_15_2" data-toggle="tab">
			RECEIVING</a>
		</li>
	</ul>


	<div class="tab-content">
		<div class="tab-pane active" id="tab_15_1">
			{{ page.create_koli | raw }}
		</div>
		<div class="tab-pane" id="tab_15_2">
			{{ page.list_koli | raw }}
		</div>

	</div>

</div>


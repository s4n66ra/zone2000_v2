{{ form_action|raw }}
	<div class="form-body">

		<div class="form-group row">
			<label class="control-label col-md-4">PO
			</label>
			<div class="col-md-8">
				{% for po in postores %}
				{{po.po_code}}<br>
				{% endfor %}
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-md-4">Store
			</label>
			<div class="col-md-8">
				{{list.store|raw}}
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-md-4">Receiving Code
			</label>
			<div class="col-md-8">
				<input type="text" name="data[koli_code]" data-required="1" class="form-control " value="{{ data.koli_code }}" disabled/>
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-md-4">DATE
			</label>
			<div class="col-md-8">
				
				<div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd">
				<input type="text" name="data[date_created]" class="form-control" value="{{data.date_created}}" readonly> <span class="input-group-btn">
					<button class="btn default" type="button">
						<i class="fa fa-calendar"></i>
					</button>
				</span>
			</div>
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-md-4">Qty
			</label>
			<div class="col-md-8">
				<input type="text" name="data[qty]" data-required="1" class="form-control format-number" value="{{ data.qty }}"/>
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-md-4">Note<span class="required">
			* </span>
			</label>
			<div class="col-md-8">
				<textarea type="text" name="data[note]" data-required="1" rows="3" class="form-control" >{{ data.note }}</textarea>
			</div>
		</div>

	</div>
{% if form_action %}
</form>
{% endif %}
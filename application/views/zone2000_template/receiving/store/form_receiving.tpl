{{ form_action|raw }}
	<div class="form-body">

		<div class="form-group-min-mg row">
			<label class="control-label col-md-4">SHIPPING CODE
			</label>
			<label class="control-label col-md-8">
				{{data.shipping_code}}<br>
			</label>
		</div>

		<div class="form-group-min-mg row">
			<label class="control-label col-md-4">STORE
			</label>
			<label class="col-md-8">
				{{data.nama_cabang|raw}}
			</label>
		</div>

		<div class="form-group-min-mg row">
			<label class="control-label col-md-4">DELIVERY DATE
			</label>
			<label class="col-md-8">
				<!-- <input type="text" name="data[koli_code]" data-required="1" class="form-control " value="{{ data.koli_code }}" disabled/> -->
				{{data.date_created|raw}}
			</label>
		</div>

		<div class="form-group-min-mg row">
			<label class="control-label col-md-4">TRUCK NUMBER
			</label>
			<label class="col-md-8">
				<!-- <input type="text" name="data[qty]" data-required="1" class="form-control format-number" value="{{ data.qty }}"/> -->
				{{data.truck_number|raw}}
			</label>
		</div>

		<div class="form-group-min-mg row"  >
			<label class="control-label col-md-4">RECEIVING DATE 
			</label>
			<div class="col-md-8">
				
				<div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd">
				<input type="text" name="date_arrived" class="form-control" value="{{date_arrived}}" readonly> <span class="input-group-btn">
					<button class="btn default" type="button">
						<i class="fa fa-calendar"></i>
					</button>
				</span>
			</div>
			</div>
		</div>
		<div class="scroll-modal" >
			<div class="form-group-min-mg row" style="border-bottom:1px solid #eee" >
				<label class="control-label col-md-4">KOLI
				</label>
				<label class="control-label col-md-4">
					ITEM
				</label>
				<label class="control-label col-md-2">
					JUMLAH
				</label>
				<label class="control-label col-md-2">
					RECEIVED
				</label>
			</div>
			{% for data in table %}
			<div class="form-group-min-mg row">
				<label class="control-label col-md-4">{{ data.koli_code|raw }}
				</label>
				<label class="control-label col-md-4">
					{{ data.item_name|raw }}
				</label>
				<label class="control-label col-md-2">
					{{ data.dc_qty_received|raw }}
				</label>
				<div class="col-md-2">
					<input type = "teks" size = "6" name="data[{{data.podetail_id}}]">
				</div>
			</div>
			{% endfor %} 
		</div>
		

	</div>
{% if form_action %}
</form>
{% endif %}
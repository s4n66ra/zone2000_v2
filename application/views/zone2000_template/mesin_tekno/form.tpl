{{ form_action|raw }}
<div class="form-body">
	<h3 class="form-section"></h3>
	<div class="alert alert-danger display-hide">
		<button class="close" data-close="alert"></button>
		You have some form errors. Please check below.
	</div>
	<div class="alert alert-success display-hide">
		<button class="close" data-close="alert"></button>
		Your form validation is successful!
	</div>
	<div class="form-group row">
		<label class="control-label col-md-4">Teknogame Name <span class="required"> * </span></label>
		<div class="col-md-8">
			<input type="text" name="data[nama_mesin]" data-required="1" class="form-control" id="machine-id" value="{{ data.nama_mesin }}" {{readonly}}/>
		</div>
	</div>
	<div class="form-group row">
		<label class="control-label col-md-4">Teknogame Kode <span class="required"> * </span> </label>
		<div class="col-md-8">
			<input type="text" name="data[kd_mesin]" value="{{data.kd_mesin}}" data-required="1" class="form-control green" {{readonly}}/>
		</div>
	</div>
	<div class="form-group row">
		
		<label class="control-label col-md-4">Reg. Number
		</label>
		<div class="col-md-8">{{list_reg_number|raw}}</div>
	</div>
	
	<div class="form-group row">	
		<label class="control-label col-md-4">Machine Type
		</label>
		<div class="col-md-8">{{list_jenis_mesin|raw}}</div>
	</div>

	<div class="form-group row">	
		<label class="control-label col-md-4">Kode Machine Type
		</label>
		<div class="col-md-8">{{kd_jenis_mesin|raw}}</div>
	</div>

	<div class="form-group row">
		<label class="control-label col-md-4">Store<span class="required"> * </span>
		</label>
		<div class="col-md-8">{{list_cabang|raw}}</div>
	</div>
</div>
</form>

<script>
	jQuery(document).ready(function(){ 
		if ($().select2) {
            $('.select2me').select2({
                placeholder: "Select",
                allowClear: true
            });
        }

        if ($().datepicker) {
            $('.date-picker').datepicker({
                rtl: Metronic.isRTL(),
                orientation: "left",
                autoclose: true
            });
            //$('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
        }
	});
</script>
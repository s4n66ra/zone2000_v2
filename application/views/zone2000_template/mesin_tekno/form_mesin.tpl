<!-- BEGIN PAGE HEADER-->
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<!-- BEGIN PAGE TITLE -->
		<div class="page-title">
			<h1>{{title}}</h1>
		</div>
		<!-- END PAGE TITLE -->
		
	</div>
	<!-- END PAGE HEAD -->

	<!-- END PAGE HEADER-->
	<!-- BEGIN PAGE CONTENT-->					
	<div class="row">
		<div class="col-md-12">
			<!-- BEGIN VALIDATION STATES-->
			<div class="portlet box green">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-gift"></i>
					</div>
				</div>
				<div class="portlet-body form">
					<!-- BEGIN FORM-->
					{{ form_action|raw }}
						<div class="form-body">
							<h3 class="form-section"></h3>
							<div class="alert alert-danger display-hide">
								<button class="close" data-close="alert"></button>
								You have some form errors. Please check below.
							</div>
							<div class="alert alert-success display-hide">
								<button class="close" data-close="alert"></button>
								Your form validation is successful!
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">Machine Code<span class="required">
								* </span>
								</label>
								<div class="col-md-4">
									<input type="text" name="kd_mesin" data-required="1" class="form-control" value="{{def_kd_mesin}}"/>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">Machine Name<span class="required">
								* </span>
								</label>
								<div class="col-md-4">
									<input type="text" name="nama_mesin" value = "{{def_nama_mesin}}" data-required="1" class="form-control"/>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">Harga<span class="required">
								* </span>
								</label>
								<div class="col-md-4">
									<input type="text" name="harga" value = "{{def_harga}}" data-required="1" class="form-control"/>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">Class Name<span class="required">
								* </span>
								</label>
								<div class="col-md-4">
									{{options_kelas|raw}}
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">Category Name<span class="required">
								* </span>
								</label>
								<div class="col-md-4">
									{{options_kategori_mesin|raw}}
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">Supplier<span class="required">
								* </span>
								</label>
								<div class="col-md-4">
									{{options_supplier|raw}}
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">PO<span class="required">
								* </span>
								</label>
								<div class="col-md-4">
									{{options_po|raw}}
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">Buy At<span class="required">
								* </span>
								</label>
								<div class="col-md-3">
									<div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd" data-date-viewmode="years" data-date-minviewmode="months">
										<input type="text" name="tgl_beli" class="form-control" readonly="" value="{{def_tgl_beli}}" todayHighlight="true">
										<span class="input-group-btn">
										<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
										</span>
									</div>
									<!-- /input-group -->
									<span class="help-block">
									Select month only </span>
								</div>							
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">Guarantee<span class="required">
								* </span>
								</label>
								<div class="col-md-4">
									<input type="text" name="garansi_toko" value = "{{def_garansi_toko}}" data-required="1" class="form-control"/>
								</div>
							</div>


						</div>
						<div class="form-actions">
							<div class="row">
								<div class="col-md-offset-3 col-md-9">
									{{button_submit|raw}}
								</div>
							</div>
						</div>
					</form>
					<!-- END FORM-->
				</div>
				<!-- END VALIDATION STATES-->
			</div>
		</div>
	</div>
	<!-- END PAGE CONTENT-->
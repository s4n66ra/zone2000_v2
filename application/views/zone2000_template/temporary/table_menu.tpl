
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE HEADER-->
			<!-- BEGIN PAGE HEAD -->
			<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1>{{title}} </h1>
				</div>
				<!-- END PAGE TITLE -->
			</div>
			<!-- END PAGE HEAD -->
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN SAMPLE TABLE PORTLET-->
					<div class="portlet">
						<div class="portlet-body">
							<div class="table-scrollable">
								<table class="table table-striped table-bordered table-advance table-hover">
								<thead>
								<tr>
									<th>
										<i class="fa fa-briefcase"></i> Company
									</th>
									<th class="hidden-xs">
										<i class="fa fa-user"></i> Contact
									</th>
									<th>
										<i class="fa fa-shopping-cart"></i> Total
									</th>
									<th>
									</th>
								</tr>
								</thead>
								<tbody>
								<tr>
									<td class="highlight">
										<div class="success">
										</div>
										<a href="#">
										RedBull </a>
									</td>
									<td class="hidden-xs">
										 Mike Nilson
									</td>
									<td>
										 2560.60$
									</td>
									<td>
										<a href="#" class="btn default btn-xs purple">
										<i class="fa fa-edit"></i> Edit </a>
									</td>
								</tr>
								<tr>
									<td class="highlight">
										<div class="info">
										</div>
										<a href="#">
										Google </a>
									</td>
									<td class="hidden-xs">
										 Adam Larson
									</td>
									<td>
										 560.60$
									</td>
									<td>
										<a href="#" class="btn default btn-xs black">
										<i class="fa fa-trash-o"></i> Delete </a>
									</td>
								</tr>
								<tr>
									<td class="highlight">
										<div class="success">
										</div>
										<a href="#">
										Apple </a>
									</td>
									<td class="hidden-xs">
										 Daniel Kim
									</td>
									<td>
										 3460.60$
									</td>
									<td>
										<a href="#" class="btn default btn-xs purple">
										<i class="fa fa-edit"></i> Edit </a>
									</td>
								</tr>
								<tr>
									<td class="highlight">
										<div class="warning">
										</div>
										<a href="#">
										Microsoft </a>
									</td>
									<td class="hidden-xs">
										 Nick
									</td>
									<td>
										 2560.60$
									</td>
									<td>
										<a href="#" class="btn default btn-xs blue">
										<i class="fa fa-share"></i> Share </a>
									</td>
								</tr>
								</tbody>
								</table>
							</div>
						</div>
					</div>
					<!-- END SAMPLE TABLE PORTLET-->
				</div>
				
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->

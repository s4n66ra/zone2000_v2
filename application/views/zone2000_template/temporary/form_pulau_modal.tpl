<div class="modal-content">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-title">Modal Title</h4>
	</div>
	<div class="modal-body">
		 <div class="row-fluid">
		    <div class="box-title">
		        <?php echo (isset($title)) ? $title : 'Untitle'; ?>
		    </div>
		    <div class="box-content">
		        {{form_add|raw}}
		        <div class="form-group">
		            <label class="col-md-3 control-label">Kode Pulau</label>
		            <div class="col-md-9">
		                {{input_kd_pulau|raw}}
		            </div>
		        </div> 
		        <div class="form-group">
		            <label class="col-md-3 control-label">Nama Pulau</label>
		            <div class="col-md-9">
			            {{input_nama_pulau|raw}}
			        </div>
		        </div> 

		        <span class="required">* harus diisi</span>
		        <div class="form-actions">
		        </div>
		        <div class="modal-footer">
					<button type="button" class="btn default" data-dismiss="modal">Close</button>
					{{anchor_save|raw}}
					<button type="submit" class="btn blue">Save changes</button>
				</div>
		        <?php echo form_close(); ?>
		    </div>
		</div>
	</div>
	
</div>
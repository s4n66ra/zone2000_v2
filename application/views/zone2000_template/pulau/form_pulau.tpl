{{ form_action|raw }}
	<div class="form-body">
		<h3 class="form-section"></h3>
		<div class="alert alert-danger display-hide">
			<button class="close" data-close="alert"></button>
			You have some form errors. Please check below.
		</div>
		<div class="alert alert-success display-hide">
			<button class="close" data-close="alert"></button>
			Your form validation is successful!
		</div>
		<div class="form-group row">
			<label class="control-label col-md-4">Kode Island <span class="required">
			* </span>
			</label>
			<div class="col-md-8">
				<input type="text" name="kd_pulau" data-required="1" class="form-control" value="{{def_kd_pulau}}"/>
			</div>
		</div>
		<div class="form-group row">
			<label class="control-label col-md-4">Nama Island <span class="required">
			* </span>
			</label>
			<div class="col-md-8">
				<input type="text" name="nama_pulau" value = "{{def_nama_pulau}}" data-required="1" class="form-control"/>
			</div>
		</div>
		<div class="form-group row">
			<label class="control-label col-md-4">Nama Negara <span class="required">
			* </span>
			</label>
			<div class="col-md-8">
				{{options_negara|raw}}
			</div>
		</div>
	</div>
</form>
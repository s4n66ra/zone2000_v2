<div class="page-header">
	<!-- END HEADER TOP -->
	<!-- BEGIN HEADER MENU -->
	<div class="page-header-menu">
		<div class="container">
			<!-- BEGIN HEADER SEARCH BOX -->
			<ul class="nav navbar-nav pull-right" style="margin-left: 10px;">
					<!-- BEGIN NOTIFICATION -->
					{{notification | raw}}
					<!-- END NOTIFICATION -->
					<!-- BEGIN USER LOGIN DROPDOWN -->
					<li class="dropdown dropdown-extended dropdown-light dropdown-inbox" id="header_inbox_bar">
						<a href="{{base_url}}notification">
						<span class="circle" id="jml_notif1">{{session.jml_notif}}</span>
						<span class="corner"></span>
						</a>
					</li>
					<li class="dropdown dropdown-user dropdown-dark">
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						<i class="icon-user"></i> 
						<span class="username username-hide-mobile hidden-sm hidden-xs" style="margin-left: 5px;">{{ session.user_name | upper }}</span>
						</a>
						<ul class="dropdown-menu dropdown-menu-default">
							<li>
								<a href="extra_profile.html">
								<i class="icon-user"></i> My Profile </a>
							</li>
							<li class="divider">
							</li>

							<li>
								<a href="{{base_url}}notification_sender" id="jml_notif2">
								<i class="icon-bell"></i> Notification ({{session.jml_notif}})</a>
							</li>

							<li>
								<a href="extra_lock.html">
								<i class="icon-lock"></i> Lock Screen </a>
							</li>
							<li>
								<a href="{{site_url('login/logout')}}">
								<i class="fa fa-power-off"></i> Log Out </a>
							</li>
						</ul>
					</li>
					<!-- END USER LOGIN DROPDOWN -->
				</ul>

			<!-- <form class="search-form" action="extra_search.html" method="GET">
				<div class="input-group">
					<input type="text" class="form-control" placeholder="Search" name="query">
					<span class="input-group-btn">
					<a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
					</span>
				</div>
			</form> -->
			
			{{ sidebar|raw }}



		</div>
	</div>
	<!-- END HEADER MENU -->
</div>
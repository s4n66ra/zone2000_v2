{{ form_action|raw }}
		<div class="form-group row">
			<label class="control-label col-md-4">Code
			</label>
			<div class="col-md-8">
				<div class="input-icon right">
					<i class="fa"></i>
					<input type="text" name="data[status_code]" data-required="1" class="form-control" value="{{data.status_code}}" {{readonly}}/>
				</div>
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-md-4">Name <span class="required">
			* </span>
			</label>
			<div class="col-md-8">
				<div class="input-icon right">
					<i class="fa"></i>
					<input type="text" name="data[status_name]" value = "{{data.status_name}}" data-required="1" class="form-control" {{readonly}}/>
				</div>
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-md-4">Status Type <span class="required">
			* </span>
			</label>
			<div class="col-md-8">
				{{ list.status_type | raw }}
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-md-4">Status Number <span class="required">
			* </span>
			</label>
			<div class="col-md-8">
				<div class="input-icon right">
					<i class="fa"></i>
					<input type="text" name="status_number" value = "{{status_number}}" data-required="1" class="form-control" {{readonly}}/>
				</div>
			</div>
		</div>


</form>
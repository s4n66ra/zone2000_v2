
	<div class="form-body col-md-12">
		<h3 class="form-section"></h3>
		<div class="alert alert-danger display-hide">
			<button class="close" data-close="alert"></button>
			You have some form errors. Please check below.
		</div>
		<div class="alert alert-success display-hide">
			<button class="close" data-close="alert"></button>
			Your form validation is successful!
		</div>

		<input type="hidden" name="status" value="{{status}}" />

		<div class="row">
			<div class="col-md-5">
				{{ form | raw }}
			</div>

			<div class="col-md-7" >
				
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-wrench"></i>
							<span class="caption-subject font-blue-hoki bold uppercase">Machine Service</span>
						</div>
						<div class="tools">
							<a href="" class="collapse">
							</a>
						</div>
					</div>
					<div class="portlet-body" id="div-service" data-source="{{ data_source_service }}">
						{{ form_service | raw }}
					</div>
				</div>

			</div>

		</div>



	</div>



<script>
	jQuery(document).ready(function(){ 
		if ($().select2) {

            $('#list-mesin-rusak').select2({
                placeholder: "Select",
                allowClear: true
            }).on('change',function(){
            	var url = $('#div-kerusakan').attr('data-source');
            	load_page('#div-kerusakan',url+'/'+$(this).val());
            });
        }

        if ($().datepicker) {
            $('.date-picker').datepicker({
                rtl: Metronic.isRTL(),
                orientation: "left",
                autoclose: true
            });
            //$('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
        }
	});
</script>
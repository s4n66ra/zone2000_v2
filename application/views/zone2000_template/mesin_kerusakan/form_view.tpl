{{ form_action|raw }}
	<div class="form-body col-md-12">
		<h3 class="form-section"></h3>
		<div class="alert alert-danger display-hide">
			<button class="close" data-close="alert"></button>
			You have some form errors. Please check below.
		</div>
		<div class="alert alert-success display-hide">
			<button class="close" data-close="alert"></button>
			Your form validation is successful!
		</div>

		<input type="hidden" name="status" value="{{status}}" />

		<div class="row">
			<div class="col-md-4">
				<div class="form-group row">
					<label class="control-label col-md-4">REF <span class="required">
					* </span>
					</label>
					<div class="col-md-8">
						<input type="text" name="data[kd_mesin_rusak]" data-required="1" class="form-control" value="{{ data.kd_mesin_rusak }}" {{readonly}}/>
					</div>
				</div>

				<div class="form-group row">
					<label class="control-label col-md-4">MACHINE				
					</label>
					<div class="col-md-8">
						{{ list_mesin|raw }}
					</div>
				</div>

				<div class="form-group row">
					<label class="control-label col-md-4">Repair Date
					</label>
					<div class="col-md-8">
						<div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd">
							<input type="text" name="data[tgl_mulai]" class="form-control" value="{{data.tgl_mulai}}" {{readonly}}>
							<span class="input-group-btn">`
								<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
							</span>
						</div>
					</div>
				</div>

				<div class="form-group row">
					<label class="control-label col-md-4">Target Finish
					</label>
					<div class="col-md-8">
						<div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd">
							<input type="text" name="data[tgl_target]" class="form-control" value="{{data.tgl_target}}" {{readonly}}>
							<span class="input-group-btn">
								<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
							</span>
						</div>
					</div>
				</div>

				<div class="form-group row">
					<label class="control-label col-md-4">Problem<span class="required">
					* </span>
					</label>
					<div class="col-md-8">
						<textarea type="text" name="data[deskripsi]" data-required="1" rows="3" class="form-control" {{readonly}}>{{ data.deskripsi }}</textarea>
					</div>
				</div>

			</div>

			<div class="col-md-4">
				{{ form_service|raw }}
			</div>

			<div class="col-md-4">
			</div>

		</div>



	</div>
</form>



<script>
	jQuery(document).ready(function(){ 
		if ($().select2) {
            $('.select2me').select2({
                placeholder: "Select",
                allowClear: true
            });
        }

        if ($().datepicker) {
            $('.date-picker').datepicker({
                rtl: Metronic.isRTL(),
                orientation: "left",
                autoclose: true
            });
            //$('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
        }
	});
</script>
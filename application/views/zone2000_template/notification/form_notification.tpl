{{ form_action|raw }}
	<div class="row">
		<div class="form-body col-md-12">
			<div class="form-group row" hidden>
				<label class="control-label col-md-4">Code <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="data[kd_hadiah]" data-required="1" class="form-control" value="{{data.kd_hadiah}}"/>
				</div>
			</div>

			<div class="form-group row" hidden>
				<label class="control-label col-md-4">Created <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="data[rec_created]" data-required="1" class="form-control" value="{{data.rec_created}}"/>
				</div>
			</div>
			
			<div class="form-group row">
				<label class="control-label col-md-4">Message <span class="required">
				* </span>
				</label>
<!-- 				<div class="col-md-8">
					<input type="text" name="data[custom_message]" data-required="1" id="custom_message" class="form-control" value="{{data.custom_message}}" />
				</div>
 -->		
 			<div class="col-md-8">
 				<textarea class="form-control" rows="3" z-index="200000" name="data[custom_message]"id="custom_message"></textarea>
 			</div>	
 			</div>

			<div class="form-group row">
				<label class="control-label col-md-4">User
				
				</label>
				<div class="col-md-6">
					<input type="text" data-required="1" class="form-control" id="list_user"/>
					<!-- {{ list.user|raw }} -->
				</div>

				<div class="btn btn-default col-md-1 filter-add" id="tambah_user">
					<i class="icon-plus">
						
					</i>
				</div>
			</div>



			<div class="form-group row" hidden>
				<label class="control-label col-md-4">Name <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="data[nama_hadiah]" value = "{{data.nama_hadiah}}" data-required="1" class="form-control"/>
				</div>
			</div>

			<div class="form-group row" hidden>
				<label class="control-label col-md-4">Price <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="data[harga]" value = "{{data.harga}}" data-required="1" class="form-control format-number"/>
				</div>
			</div>

			<div class="form-group row" hidden>
				<label class="control-label col-md-4">Type <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					{{options_hadiah|raw}}
				</div>
			</div>

			<div class="form-group row" hidden>
				<label class="control-label col-md-4">Barcode Eksisting <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="data[item_barcode]" data-required="1" class="form-control" value="{{data.item_barcode}}"/>
				</div>
			</div>
		</div>
	</div>
</form>
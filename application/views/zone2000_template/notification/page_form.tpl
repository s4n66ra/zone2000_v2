<div class="row">
		
	<div class="col-md-12">
		<div class="tabbable-custom">
			<ul class="nav nav-tabs ">
				<li class="active">
					<a href="#tab_15_1" data-toggle="tab">
					Store Notification</a>
				</li>
				{% if is_head_office %}
				<li>
					<a href="#tab_15_2" data-toggle="tab">
					PP Notification </a>
				</li>

				<li>
					<a href="#tab_15_4" data-toggle="tab">
					 </a>
				</li>
				{% endif %}
			</ul>
			
			<div class="tab-content">
				
				<div class="tab-pane active" id="tab_15_1">
					{{ form.user_status |raw}}
				</div>

				<div class="tab-pane" id="tab_15_2">
					{{ form.pr_status|raw }}
				</div>


				<div class="tab-pane" id="tab_15_4">
					
				</div>
				
			</div>
		</div>
	</div>
	

</div>
<!-- BEGIN PAGE HEADER-->
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<!-- BEGIN PAGE TITLE -->
		<div class="page-title">
			<h1>{{title}}</h1>
		</div>
		<!-- END PAGE TITLE -->
		
	</div>
	<!-- END PAGE HEAD -->

	<!-- END PAGE HEADER-->
	<!-- BEGIN PAGE CONTENT-->					
	<div class="row">
		<div class="col-md-12">
			<!-- BEGIN VALIDATION STATES-->
			<div class="portlet box green">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-gift"></i>
					</div>
				</div>
				<div class="portlet-body form">
					<!-- BEGIN FORM-->
					{{ form_action|raw }}
						<div class="form-body">
							<h3 class="form-section"></h3>
							<div class="alert alert-danger display-hide">
								<button class="close" data-close="alert"></button>
								You have some form errors. Please check below.
							</div>
							<div class="alert alert-success display-hide">
								<button class="close" data-close="alert"></button>
								Your form validation is successful!
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">PO Code <span class="required">
								* </span>
								</label>
								<div class="col-md-4">
									<input type="text" name="kd_po" data-required="1" class="form-control" value="{{def_kd_po}}"/>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">PO Name<span class="required">
								* </span>
								</label>
								<div class="col-md-4">
									<input type="text" name="nama_po" value = "{{def_nama_po}}" data-required="1" class="form-control"/>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">Supplier Name<span class="required">
								* </span>
								</label>
								<div class="col-md-4">
									{{options_supplier|raw}}
								</div>
							</div>


							<div class="form-group">
								<label class="control-label col-md-3">DESTINATION<span class="required">
								* </span>
								</label>
								<div class="col-md-4">
									{{options_cabang|raw}}
								</div>
							</div>

						</div>
						<div class="form-actions">
							<div class="row">
								<div class="col-md-offset-3 col-md-9">
									{{button_submit|raw}}
								</div>
							</div>
						</div>
					</form>
					<!-- END FORM-->
				</div>
				<!-- END VALIDATION STATES-->
			</div>
		</div>
	</div>
	<!-- END PAGE CONTENT-->
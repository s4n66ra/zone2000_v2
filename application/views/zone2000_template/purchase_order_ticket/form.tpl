{{ form_action|raw }}
	<div class="form-body col-md-12">
		<h3 class="form-section"></h3>
		<div class="alert alert-danger display-hide">
			<button class="close" data-close="alert"></button>
			You have some form errors. Please check below.
		</div>
		<div class="alert alert-success display-hide">
			<button class="close" data-close="alert"></button>
			Your form validation is successful!
		</div>

		<input type="hidden" name="status" value="{{status}}" />

		{% if hide_kd!=true %}
		<div class="form-group row">
			<label class="control-label col-md-4">Doc. Number <span class="required">
			* </span>
			</label>
			<div class="col-md-8">
				<input type="text" name="doc_number" data-required="1" class="form-control" value="{{ data.doc_number }}" {{readonly}}/>
			</div>
		</div>
		

		<div class="form-group row">
			<label class="control-label col-md-4">Request For<span class="required">
			* </span>
			</label>
			<div class="col-md-8">
				<select class="form-control" name="data[pr_type]" value="{{data.pr_type}}">
					<option value="7">Machine</option>
					<option value="2">Merchandise</option>
					<option value="3">Sparepart</option>
				</select>
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-md-4">Note<span class="required">
			* </span>
			</label>
			<div class="col-md-8">
				<textarea type="text" name="data[pr_note]" data-required="1" rows="3" class="form-control" {{readonly}}>{{ data.pr_note }}</textarea>
			</div>
		</div>
		{% else %}
		<div class="row text-center">
			<div class="col-md-12">
				<input value="{{po_id}}" name="po_id" id="po_id" type="hidden"/>
				<h4>Are you sure to Cancel Purchase Order?</h4>
			</div>
		</div>
		{% endif %}

	</div>
</form>
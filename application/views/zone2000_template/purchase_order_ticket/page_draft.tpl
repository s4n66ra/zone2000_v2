<script type="text/javascript">
	$(document).ready(function(){
		
	});
</script>

<div class="portlet light">
	<div class="portlet-title">
		{{ btn_page_back()|raw }}
		<div class="caption">
			<i class="glyphicon glyphicon-list-alt font-blue-madison"></i>
			<span class="caption-subject bold font-blue-madison uppercase">
			DRAFT PURCHASE ORDER </span>
			<span class="caption-helper"> Qty Confirmation</span>
		</div>
		<div class="actions">

			<a href="#" class="btn-draft-supplier" onclick="btnLoadNextPage(this)" data-source="{{source.supplier}}">
				
			</a>
			
			{% if bundle.bundle_status==0 %}
			<div class="btn-group">
				<a class="btn btn-circle green-turquoise " href="#" data-toggle="dropdown" aria-expanded="false">
				<i class="fa fa-check"></i> Confirm <i class="fa fa-angle-down"></i>
				</a>
				<ul class="dropdown-menu pull-right">
					<li>
						<a href="#" class="btn-draft-save" url-action="{{url.action_save}}" act="approve">
						<i class="fa fa-check"></i> Approve </a>
					</li>
					<li>
						<a href="#" class="btn-draft-save" url-action="{{url.action_save}}" act="revisi">
						<i class="fa fa-edit"></i> Revisi </a>
					</li>
					
					<li class="divider">
					
					<li>
						<a href="#">
						<i class="fa fa-close"></i> Cancel </a>
					</li>
					
				</ul>
			</div>
			{% endif %}

			{% if bundle.bundle_status==1 %}
			<a href="#" class="btn btn-circle blue btn-draft-detail" onclick="btnLoadNextPage(this)" data-source="{{source.detail}}"><i class="fa fa-edit"></i> Detail </a>
			{% endif %}
			
			<a href="" class="collapse">
			<a href="#" class="btn btn-circle btn-default btn-icon-only fullscreen" data-original-title="" title=""></a>
		</div>
		
	</div>
	<div class="portlet-body">
		<div class="row">
			<div class="col-md-6">
				<input type="hidden" id="transactionbundle-id" value="{{data.transactionbundle_id}}" />
			</div>
		</div>

		{% for key, draft in list_pr %}
		<div class="row">
			<div class="col-md-12">
				
				<div class="portlet light">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-puzzle font-red-flamingo"></i>
							<span class="caption-subject bold font-red-flamingo uppercase">
							{{ key }} </span>
							<span class="caption-helper"></span>
						</div>
						<div class="tools">
							<a href="" class="collapse">
							</a>
						</div>
					</div>
					<div class="portlet-body">
						
						<div class="table-scrollable">
							 {{ draft | raw}}
						</div>

					</div>
				</div>
			</div>
		</div>
		{% endfor %}

	</div>
</div>

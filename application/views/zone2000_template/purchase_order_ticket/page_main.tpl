<div class="tabbable-custom show-overflow">
	<ul class="nav nav-tabs ">
		<li class="active">
			<a href="#tab_po_tk_1" data-toggle="tab">
			CREATE DRAFT PO</a>
		</li>
		<li>
			<a href="#tab_po_tk_2" data-toggle="tab">
			LIST DRAFT PO</a>
		</li>
		<li>
			<a href="#tab_po_tk_3" data-toggle="tab">
			PURCHASE ORDER</a>
		</li>
	</ul>

	<div class="tab-content">
		<div class="tab-pane active" id="tab_po_tk_1">
			{{ table.table_review | raw }}
		</div>

		<div class="tab-pane" id="tab_po_tk_2">
			{{ table.table_bundle | raw }}
		</div>

		<div class="tab-pane" id="tab_po_tk_3">
			{{ table.table_po | raw }}
		</div>

	</div>

</div>


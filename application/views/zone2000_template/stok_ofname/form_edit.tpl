{{ form_action|raw }}
	<div class="row" style="margin:15px;">
		<div class="col-md-12 form-body">
			<div class="form-group row">
				<label class="control-label col-md-4">Item Name 
				
				</label>
				<div class="col-md-8">
					<input type="text" name="item_name" data-required="1" class="form-control" value="{{ data.item_name }}" readonly/>
					<input type="hidden" id="data['item_id']" name="data[item_id]" class="form-control" value="{{ data.item_id }}"/>
	            <!-- <input type="hidden" id="sell_list_item_id" name="data[item_id]" class="form-control"/> -->

				</div>
			</div>
			<div class="form-group row">
				<label class="control-label col-md-4">Store Name 
				
				</label>
				<div class="col-md-8">
					{{ list.store|raw }}
				</div>
			</div>
			<input type="hidden" name="data[store_id]" id="store_id" value="{{id_cabang}}" />
			<div class="form-group row">
				<div class="col-md-4">
				</div>
				<div class="col-md-4">
					LAMA
				</div>
				<div class="col-md-4">
					BARU
				</div>

			</div>


			<div class="form-group row">
				<label class="control-label col-md-4">Stok Total
				
				</label>
				<div class="col-md-4">
					<input type="text" name="data[old_stok_total]" value = "{{data.old_stok_total}}" data-required="1" class="form-control green " id="old_stok_total" readonly />
				</div>

				<div class="col-md-4">
					<input type="text" name="data[stok_total]" value = "{{data.stok_total}}" data-required="1" class="form-control green " id="stok_total" readonly />
				</div>

			</div>

			<div class="form-group row">
				<label class="control-label col-md-4">Stok Gudang 
				
				</label>
				<div class="col-md-4">
					<input type="text" name="data[old_stok_gudang]" value = "{{data.old_stok_gudang}}" data-required="1" class="form-control green " id="old_stok_gudang" readonly />
				</div>

				<div class="col-md-4">
					<input type="text" name="data[stok_gudang]" value = "{{data.stok_gudang}}" data-required="1" class="form-control green " id="stok_gudang"/>
				</div>

			</div>

			<div class="form-group row">
				<label class="control-label col-md-4">Stok Display
				</label>
				<div class="col-md-4">
					<input type="text" name="data[old_stok_store]" value = "{{data.old_stok_store}}" data-required="1" class="form-control green " id="old_stok_store" readonly />
				</div>

				<div class="col-md-4">
					<input type="text" name="data[stok_store]" value = "{{data.stok_store}}" data-required="1" class="form-control green " id="stok_store"/>
				</div>

			</div>

			<div class="form-group row">
				<label class="control-label col-md-4">Stok Vending
				</label>
				<div class="col-md-4">
					<input type="text" name="data[old_stok_mesin]" value = "{{data.old_stok_mesin}}" data-required="1" class="form-control green " id="old_stok_mesin" readonly />
				</div>

				<div class="col-md-4">
					<input type="text" name="data[stok_mesin]" value = "{{data.stok_mesin}}" data-required="1" class="form-control green " id="stok_mesin"/>
				</div>

			</div>


		</div>

<!--		<div class="col-md-6 form-body">
			<div class="form-group row">
				<label class="control-label col-md-4"> 
				
				</label>
				<div class="col-md-8">
				<label>
					&nbsp
				</label>
				</div>
			</div>
			<div class="form-group row">
				<label class="control-label col-md-4"> 
				
				</label>
				<div class="col-md-8">
				<label>
					&nbsp
				</label>
				</div>
			</div>

 			<div class="form-group row">
				<div class="col-md-8">
					<input type="text" name="data[stok_total]" value = "{{data.stok_total}}" data-required="1" class="form-control green " />
				</div>

			</div>

			<div class="form-group row">
				<div class="col-md-8">
					<input type="text" name="data[stok_gudang]" value = "{{data.stok_gudang}}" data-required="1" class="form-control green " />
				</div>
			</div>

			<div class="form-group row">
				<div class="col-md-8">
					<input type="text" name="data[stok_store]" value = "{{data.stok_store}}" data-required="1" class="form-control green " />
				</div>
			</div>
 -->
		</div>
		
	</div>

</form>

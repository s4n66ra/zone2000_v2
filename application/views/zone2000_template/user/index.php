<div class="inner_content">
    <div class="user_bar">
        <div class="row-fluid">
            <div class="float_left">
                <h3><span><?php echo (isset($title)) ? $title : 'Untitle'; ?></span></h3>
            </div>
        </div>
    </div>
    <div class="widgets_area">     
        <div class="row-fluid">
            <div class="span6">
                <div id="index-content" class="well-content">
                    <?php if ($this->access_right->otoritas('add') || $this->access_right->otoritas('print')) : ?>
                        <div class="well">
                            <div class="pull-left">
                                <?php echo hgenerator::render_button_group($button_group); ?>
                            </div>
                            <div class="pull-right">
                                <?php echo hgenerator::render_button_group($print_group); ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <div class="row-fluid">
                        <div class="span12">
                            <div class="well">
                                <div class="well-content clearfix">

                                    <?php echo form_open_multipart('', array('id' => 'ffilter')); ?>
                                    <div class="form_row">
                                        <div class="pull-left span4">
                                            <label class="control-label">NPP / Nama :</label>
                                            <div class="controls">
                                                <?php echo form_input('kata_kunci', '', 'class="span12 onchange"') ?>
                                            </div>
                                        </div>
                                    <?php echo form_close(); ?>

                                </div>
                            </div> 
                        </div>
                    </div>

                    <div id="content_table" data-source="<?php echo $data_source; ?>" data-filter="#ffilter"></div>
                    <div>&nbsp;</div>
                </div>

                <div id="form-content" class="well-content"></div>
            </div>
        </div>
    </div>
</div>
</div>

<script type="text/javascript">
    $(function() {

        load_table('#content_table', 1);

        $('.onchange').change(function() {
            load_table('#content_table', 1);
        });

    });

</script>
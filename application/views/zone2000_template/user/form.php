<div class="row-fluid">
    <div class="box-title">
        <?php echo (isset($title)) ? $title : 'Untitle'; ?>
    </div>
    <div class="box-content">
        <?php
        $hidden_form = array('id' => !empty($id) ? $id : '');
        echo form_open($form_action, array('id' => 'finput', 'class' => 'form-horizontal'), $hidden_form);
        ?>
        <div class="control-group" style="display: none;">
            <label for="select" class="control-label">NPP <span class="required">*</span></label>
            <div class="controls">
                <?php echo form_input('nik', !empty($default->nik) ? $default->nik : '', 'id="nik" class="span8"'); ?>
                <?php echo form_input('temp_nama', !empty($default->nama) && !empty($default->nik) ? $default->nik . ' - ' . $default->nama : '', 'id="temp_nama" class=""'); ?>
            </div>
        </div>
        <div class="control-group">
            <label for="select" class="control-label">NPP & Nama <span class="required">*</span></label>
            <div class="controls">
                <?php echo form_input('nama', !empty($default->nama) && !empty($default->nik) ? $default->nik . ' - ' . $default->nama : '', 'id="nama" class="span8"'); ?>
            </div>
        </div>
        <div class="control-group">
            <label for="select" class="control-label">Username <span class="required">*</span></label>
            <div class="controls">
                <?php echo form_input('user_name', !empty($default->user_name) ? $default->user_name : '', 'class="span4"'); ?>
            </div>
        </div>
        <div class="control-group">
            <label for="select" class="control-label">Role <span class="required">*</span></label>
            <div class="controls">
                <?php echo form_dropdown('roles', $role_options, !empty($default->grup_id) ? $default->grup_id : '', 'id="roles" class="span5 chosen"'); ?>
            </div>
        </div>
        <div class="control-group">
            <label for="select" class="control-label">Status <span class="required">*</span></label>
            <div class="controls">
                <?php
                echo form_radio('status', 't', !empty($default->is_active) && $default->is_active == 't' ? true : false) . ' Aktif &nbsp;&nbsp; ';
                echo form_radio('status', 'f', !empty($default->is_active) && $default->is_active == 'f' ? true : false) . ' Tidak Aktif';
                ?>
            </div>
        </div>

        <div class="widgets_area" id="roles_table" style="display: none;">

        </div>
		<span class="required">* harus diisi</span>
        <div class="form-actions">
            <?php echo anchor(null, '<i class="icon-save"></i> Simpan', array('id' => 'button-save', 'class' => 'blue btn', 'onclick' => "simpan_data(this.id, '#finput', '#button-back')")); ?>
            <?php echo anchor(null, '<i class="icon-circle-arrow-left"></i> Kembali', array('id' => 'button-back', 'class' => 'btn', 'onclick' => 'close_form(this.id)')); ?>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>

<script type="text/javascript">

    function roles_table(id) {
        if (id === '') {
            $('#roles_table').html('<div class="alert" style="font-weight:bold;">Silahkan pilih Role!</div>');
        } else {
            var link = '<?php echo $role_source; ?>';
            var user_id = $('input[name=id]').val();
            var data = {id: id, user_id: user_id};
            $('#roles_table').html('<div class="loading-progress">&nbsp;</div>');
            $.post(link, data, function(out) {
                $('#roles_table').html(out);
            }, 'json');
        }
    }

    function qslide(id) {
        $('#menu_group_' + id).slideToggle();
    }

    $(function() {
        //var detault_role = $('#roles').val();
        //roles_table(detault_role); // default

        $('.date').datepicker({
            format: 'dd-mm-yyyy'
        });

        $('.chosen').chosen();
        $('.uniform').uniform();

        var cache = {};
        var cache_map = {};
        $("#nama").typeahead({
            source: function(query, process) {

                states = [];
                map = {};

                if (query in cache) {
                    map = cache_map[query];
                    process(cache[query]);
                    $('.typeahead').addClass('span7').css('margin-left', 0);
                } else {
                    $.get('<?php echo $pegawai_source; ?>', {query: query}, function(data) {
                        $.each(data, function(i, state) {
                            map[state.nama] = state;
                            states.push(state.nama);
                        });
                        cache[query] = states;
                        cache_map[query] = map;
                        process(states);
                        $('.typeahead').addClass('span7').css('margin-left', 0);
                    }, 'json');
                }
            },
            minLength: 3,
            matcher: function(item) {
                if (item.toLowerCase().indexOf(this.query.trim().toLowerCase()) !== -1) {
                    return true;
                }
            },
            updater: function(item) {
                selectedState = map[item].nik;
                $('#nik').val(selectedState);
                $('#temp_nama').val(item);
                return item;
            }
        });

    });
</script>
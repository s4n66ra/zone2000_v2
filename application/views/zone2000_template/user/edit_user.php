<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    </head>
    <body>
    
        <form id="form1" name="form1" method="post" action="<?php echo base_url() . 'dashboard/update_user' ?>">
          <?php
                    if (!empty($data_user)) {
                        $no = 1;
                        $d = base_url();
                        foreach ($data_user as $data):
                            ?>
<div class="row-fluid" style="margin-left:20px">
    <div class="box-title">
        <?php echo (isset($title)) ? $title : 'Untitle'; ?>
    </div>
   <?php if(!empty($message)) { ?>
            	<div class="alert alert-<?php echo $status; ?>">
                      <?php echo $message; ?>
                </div>
    <?php } ?>
    <div class="box-content">
     <table>
       <tr>
         <td><label for="select" class="control-label">NIP</label></td>
         <td>:</td>
         <td> <input readonly name="txtnik" type="text" id="txtnik" value="<?php echo $data->nik ?>"/></td>
       </tr>
       <tr>
         <td><label for="select" class="control-label">Username Lama</label></td>
         <td>:</td>
         <td><input readonly name="txtulama" type="text" id="txtulama" value="<?php echo $data->user_name ?>" style="width: 250px" /></td>
       </tr>
       <tr>
         <td><label for="select" class="control-label">Password Lama</label></td>
         <td>:</td>
         <td><input name="txtplama" type="password" id="txtplama" style="width: 300px" /></td>
       </tr>
       <tr>
         <td><label for="select" class="control-label">Username Baru</label></td>
         <td>:</td>
         <td><input name="txtubaru" type="text" id="txtubaru" style="width: 250px" /></td>
       </tr>
       <tr>
         <td><label for="select" class="control-label">Password Baru</label></td>
         <td>:</td>
         <td><input name="txtpbaru" type="password" id="txtpbaru" style="width: 300px" /> <input name="status" type="hidden" id="status"/></td>
       </tr>
     </table>
 		 <div class="form-actions">
        	<input type="submit" class="blue btn" value="Simpan">
            <input type="reset" class="btn">
            <!-- <?php echo anchor(null, '<i class="icon-save"></i> Simpan', array('id' => 'button-save', 'class' => 'blue btn', 'onclick' => "simpan_data(this.id, '#finput', '#button-back')")); ?>
            <?php echo anchor(null, '<i class="icon-circle-arrow-left"></i> Kembali', array('id' => 'button-back', 'class' => 'btn', 'onclick' => 'close_form_modal(this.id)')); ?> -->
        </div>
      
    </div>
</div>
 <?php
                            $no++;
                        endforeach;
                    }
                    ?>
            <div id="table_grid_tampung" style="border-radius: 5px; width:700px">
         
        </form>
        <br>
        <br>
        </body>
</html>
<table id="table_list_pegawai" class="table table-bordered table-hover datatable dataTable">
    <thead>
        <tr>
            <th><?php echo form_checkbox('cb_select_menu', '', false, 'id="cb_select_menu" target-selected="cb_select" onchange="select_all(this.id)" disabled="disabled"'); ?></th>
            <th>Nama Menu</th>
            <th>View</th>
            <th>Add</th>
            <th>Edit</th>
            <th>Delete</th>
            <th>Approve</th>
            <th>Export</th>
            <th>Print</th>
        </tr>
    </thead>
    <?php foreach ($menu_group->result() as $group) : ?>
        <tr id="default_row_select">
            <td align="center"><img style="cursor: pointer;" src="<?php echo base_url() . 'images/bawah.png' ?>" onclick="qslide('<?php echo $group->kd_grup_menu; ?>');" /></td>
            <td colspan="8" style="font-weight: bold;font-size: 12px;"><?php echo $group->nama_grup_menu; ?></td>
        </tr>
        <tbody id="menu_group_<?php echo $group->kd_grup_menu ?>">
            <?php
            if (isset($menu[$group->kd_grup_menu])) :
                if (isset($menu[$group->kd_grup_menu][0])) :
                    foreach ($menu[$group->kd_grup_menu][0] as $value) :
                        $is_view = $is_add = $is_edit = $is_delete = $is_approve = $is_import = $is_print = false;
                        $is_view_disabled = $is_add_disabled = $is_edit_disabled = $is_delete_disabled = $is_approve_disabled = $is_import_disabled = $is_print_disabled = ' disabled="disabled" ';
                        if (in_array($value['kd_menu'], $roles['is_view'])) {
                            $is_view = in_array($value['kd_menu'], $user_akses['is_view']) ? true : false;
                            $is_view_disabled = '';
                        }
                        if (in_array($value['kd_menu'], $roles['is_add'])) {
                            $is_add = in_array($value['kd_menu'], $user_akses['is_add']) ? true : false;
                            $is_add_disabled = '';
                        }
                        if (in_array($value['kd_menu'], $roles['is_edit'])) {
                            $is_edit = in_array($value['kd_menu'], $user_akses['is_edit']) ? true : false;
                            $is_edit_disabled = '';
                        }
                        if (in_array($value['kd_menu'], $roles['is_delete'])) {
                            $is_delete = in_array($value['kd_menu'], $user_akses['is_delete']) ? true : false;
                            $is_delete_disabled = '';
                        }
                        if (in_array($value['kd_menu'], $roles['is_approve'])) {
                            $is_approve = in_array($value['kd_menu'], $user_akses['is_approve']) ? true : false;
                            $is_approve_disabled = '';
                        }
                        if (in_array($value['kd_menu'], $roles['is_import'])) {
                            $is_import = in_array($value['kd_menu'], $user_akses['is_import']) ? true : false;
                            $is_import_disabled = '';
                        }
                        if (in_array($value['kd_menu'], $roles['is_import'])) {
                            $is_import = in_array($value['kd_menu'], $user_akses['is_import']) ? true : false;
                            $is_import_disabled = '';
                        }
                        if (in_array($value['kd_menu'], $roles['is_print'])) {
                            $is_print = in_array($value['kd_menu'], $user_akses['is_print']) ? true : false;
                            $is_print_disabled = '';
                        }
                        ?>
                        <tr>
                            <td align="center"><img src="<?php echo base_url() . 'images/submenu-sub.gif' ?>"/></td>
                            <td style="color: blue;font-weight: bold;"><?php echo $value['nama_menu']; ?></td>
                            <td align="center"><?php echo form_checkbox('otoritas_menu_view[]', $value['kd_menu'], $is_view, $is_view_disabled . 'class="cb_select" target-selected="cb_select_baris_' . $value['kd_menu'] . '" id="cb_select_menu_' . $value['kd_menu'] . '" onchange="select_all(this.id)" '); ?></td>
                            <td align="center"><?php echo form_checkbox('otoritas_menu_add[]', $value['kd_menu'], $is_add, $is_add_disabled . 'class="cb_select cb_select_baris_' . $value['kd_menu'] . '"'); ?></td>
                            <td align="center"><?php echo form_checkbox('otoritas_menu_edit[]', $value['kd_menu'], $is_edit, $is_edit_disabled . 'class="cb_select cb_select_baris_' . $value['kd_menu'] . '"'); ?></td>
                            <td align="center"><?php echo form_checkbox('otoritas_menu_delete[]', $value['kd_menu'], $is_delete, $is_delete_disabled . 'class="cb_select cb_select_baris_' . $value['kd_menu'] . '"'); ?></td>
                            <td align="center"><?php echo form_checkbox('otoritas_menu_approve[]', $value['kd_menu'], $is_approve, $is_approve_disabled . 'class="cb_select cb_select_baris_' . $value['kd_menu'] . '"'); ?></td>
                            <td align="center"><?php echo form_checkbox('otoritas_menu_export[]', $value['kd_menu'], $is_import, $is_import_disabled . 'class="cb_select cb_select_baris_' . $value['kd_menu'] . '"'); ?></td>
                            <td align="center"><?php echo form_checkbox('otoritas_menu_print[]', $value['kd_menu'], $is_print, $is_print_disabled . 'class="cb_select cb_select_baris_' . $value['kd_menu'] . '"'); ?></td>
                        </tr>

                        <?php
                        if (isset($menu[$group->kd_grup_menu][$value['kd_menu']])) :
                            foreach ($menu[$group->kd_grup_menu][$value['kd_menu']] as $value2) :
                                $is_view2 = $is_add2 = $is_edit2 = $is_delete2 = $is_approve2 = $is_import2 = $is_print2 = false;
                                $is_view_disabled2 = $is_add_disabled2 = $is_edit_disabled2 = $is_delete_disabled2 = $is_approve_disabled2 = $is_import_disabled2 = $is_print_disabled2 = ' disabled="disabled" ';
                                if (in_array($value2['kd_menu'], $roles['is_view'])) {
                                    $is_view2 = in_array($value2['kd_menu'], $user_akses['is_view']) ? true : false;
                                    $is_view_disabled2 = '';
                                }
                                if (in_array($value2['kd_menu'], $roles['is_add'])) {
                                    $is_add2 = in_array($value2['kd_menu'], $user_akses['is_add']) ? true : false;
                                    $is_add_disabled2 = '';
                                }
                                if (in_array($value2['kd_menu'], $roles['is_edit'])) {
                                    $is_edit2 = in_array($value2['kd_menu'], $user_akses['is_edit']) ? true : false;
                                    $is_edit_disabled2 = '';
                                }
                                if (in_array($value2['kd_menu'], $roles['is_delete'])) {
                                    $is_delete2 = in_array($value2['kd_menu'], $user_akses['is_delete']) ? true : false;
                                    $is_delete_disabled2 = '';
                                }
                                if (in_array($value2['kd_menu'], $roles['is_approve'])) {
                                    $is_approve2 = in_array($value2['kd_menu'], $user_akses['is_approve']) ? true : false;
                                    $is_approve_disabled2 = '';
                                }
                                if (in_array($value2['kd_menu'], $roles['is_import'])) {
                                    $is_import2 = in_array($value2['kd_menu'], $user_akses['is_import']) ? true : false;
                                    $is_import_disabled2 = '';
                                }
                                if (in_array($value2['kd_menu'], $roles['is_import'])) {
                                    $is_import2 = in_array($value2['kd_menu'], $user_akses['is_import']) ? true : false;
                                    $is_import_disabled2 = '';
                                }
                                if (in_array($value2['kd_menu'], $roles['is_print'])) {
                                    $is_print2 = in_array($value2['kd_menu'], $user_akses['is_print']) ? true : false;
                                    $is_print_disabled2 = '';
                                }
                                ?>
                                <tr>
                                    <td align="right"><img src="<?php echo base_url() . 'images/submenu-sub.gif' ?>"/></td>
                                    <td><?php echo $value2['nama_menu']; ?></td>
                                    <td align="center"><?php echo form_checkbox('otoritas_menu_view[]', $value2['kd_menu'], $is_view2, $is_view_disabled2 . 'class="cb_select" target-selected="cb_select_baris_' . $value2['kd_menu'] . '" id="cb_select_menu_' . $value2['kd_menu'] . '" onchange="select_all(this.id)" '); ?></td>
                                    <td align="center"><?php echo form_checkbox('otoritas_menu_add[]', $value2['kd_menu'], $is_add2, $is_add_disabled2 . 'class="cb_select cb_select_baris_' . $value2['kd_menu'] . '"'); ?></td>
                                    <td align="center"><?php echo form_checkbox('otoritas_menu_edit[]', $value2['kd_menu'], $is_edit2, $is_edit_disabled2 . 'class="cb_select cb_select_baris_' . $value2['kd_menu'] . '"'); ?></td>
                                    <td align="center"><?php echo form_checkbox('otoritas_menu_delete[]', $value2['kd_menu'], $is_delete2, $is_delete_disabled2 . 'class="cb_select cb_select_baris_' . $value2['kd_menu'] . '"'); ?></td>
                                    <td align="center"><?php echo form_checkbox('otoritas_menu_approve[]', $value2['kd_menu'], $is_approve2, $is_approve_disabled2 . 'class="cb_select cb_select_baris_' . $value2['kd_menu'] . '"'); ?></td>
                                    <td align="center"><?php echo form_checkbox('otoritas_menu_export[]', $value2['kd_menu'], $is_import2, $is_import_disabled2 . 'class="cb_select cb_select_baris_' . $value2['kd_menu'] . '"'); ?></td>
                                    <td align="center"><?php echo form_checkbox('otoritas_menu_print[]', $value2['kd_menu'], $is_print2, $is_print_disabled2 . 'class="cb_select cb_select_baris_' . $value2['kd_menu'] . '"'); ?></td>
                                </tr>
                                <?php
                            endforeach;
                        endif;
                    endforeach;
                endif;
            endif;
            ?>
        </tbody>
    <?php endforeach; ?>
</table>
{{ form_action|raw }}
	<div class="form-body">
		<h3 class="form-section"></h3>
		<div class="alert alert-danger display-hide">
			<button class="close" data-close="alert"></button>
			You have some form errors. Please check below.
		</div>
		<div class="alert alert-success display-hide">
			<button class="close" data-close="alert"></button>
			Your form validation is successful!
		</div>

		<div class="form-group row">
			<label class="control-label col-md-3">Branch Name<span class="required">
			* </span>
			</label>
			<div class="col-md-8">
				{{list_cabang|raw}}
			</div>

		</div>

		<div class="form-group row">
			<label class="control-label col-md-3">Merchandise Name<span class="required">
			* </span>
			</label>
			<div class="col-md-8">
				{{list_hadiah|raw}}
			</div>

		</div>

		<div class="form-group row">
			<label class="control-label col-md-3">The Number of Ticket<span class="required">
			* </span>
			</label>
			<div class="col-md-8">
				<input type="text" name="jumlah_tiket" value = "{{data.jumlah_tiket}}" data-required="1" class="form-control"/>
			</div>
		</div>


	</div>
</form>

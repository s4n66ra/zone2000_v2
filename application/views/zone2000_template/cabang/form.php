<div class="row-fluid">
    <div class="box-title">
        <?php echo (isset($title)) ? $title : 'Untitle'; ?>
    </div>
    <div class="box-content">
        <?php
        $hidden_form = array('id' => !empty($id) ? $id : '');
        echo form_open_multipart($form_action, array('id' => 'finput', 'class' => 'form-horizontal'), $hidden_form);
        ?>

        <div class="control-group">
            <label for="select" class="control-label">Kode Cabang<span class="required">*</span></label>
            <div class="controls">
                <?php echo form_input('kd_cabang', !empty($default->kd_cabang) ? $default->kd_cabang : '', 'class="span6"') ?>
                
            </div>
        </div>

        <div class="control-group">
            <label for="select" class="control-label">Nama Cabang<span class="required">*</span></label>
            <div class="controls">
                <?php echo form_input('nama_cabang', !empty($default->nama_cabang) ? $default->nama_cabang : '', 'class="span6"') ?>
                
            </div>
        </div>

        <div class="control-group">
            <label for="select" class="control-label">Area<span class="required">*</span></label>
            <div class="controls">
                <?php echo form_dropdown('id_area',$options_area, !empty($default->id_area) ? $default->id_area : '', 'class="span6 onchange chosen"') ?>  
            </div>
        </div>

        <div class="control-group">
            <label for="select" class="control-label">Alamat<span class="required">*</span></label>
            <div class="controls">
                <?php echo form_input('alamat', !empty($default->alamat) ? $default->alamat : '', 'class="span6"') ?>
                
            </div>
        </div>

        <div class="control-group">
            <label for="select" class="control-label">Telpon<span class="required">*</span></label>
            <div class="controls">
                <?php echo form_input('telp', !empty($default->telp) ? $default->telp : '', 'class="span6"') ?>
                
            </div>
        </div>

        <div class="control-group">
            <label for="select" class="control-label">Supervisor<span class="required">*</span></label>
            <div class="controls">
                <?php echo form_dropdown('id_supervisor',$options_pegawai, !empty($default->status_pegawai) ? $default->status_pegawai : '', 'class="span6 onchange chosen"') ?>  
            </div>
        </div>

        <div class="control-group">
            <label for="select" class="control-label">Kepala Cabang<span class="required">*</span></label>
            <div class="controls">
                <?php echo form_dropdown('id_kepcabang',$options_pegawai, !empty($default->status_pegawai) ? $default->status_pegawai : '', 'class="span6 onchange chosen"') ?>  
            </div>
        </div>

        <div class="control-group">
            <label for="select" class="control-label">Luas<span class="required">*</span></label>
            <div class="controls">
                <?php echo form_input('luas', !empty($default->luas) ? $default->luas : '', 'class="span6"') ?>
                
            </div>
        </div>

        <div class="control-group">
            <label for="select" class="control-label">Pajak Toko<span class="required">*</span></label>
            <div class="controls">
                <?php echo form_input('pajak_toko', !empty($default->pajak_toko) ? $default->pajak_toko : '', 'class="span6"') ?>
                
            </div>
        </div>



        <span class="required">* harus diisi</span>
        <div class="form-actions">
            <?php echo anchor(null, '<i class="icon-save"></i> Simpan', array('id' => 'button-save', 'class' => 'blue btn', 'onclick' => "simpan_data(this.id, '#finput', '#button-back')")); ?>
            <?php echo anchor(null, '<i class="icon-circle-arrow-left"></i> Kembali', array('id' => 'button-back', 'class' => 'btn', 'onclick' => 'close_form_modal(this.id)')); ?>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('.date').datepicker({
            format: 'dd-mm-yyyy',
        });
        $('.chosen').chosen();
        $('.numeric').numeric();
    });
    function refresh_filter(){ 
        load_table('#content_table', 1,function(){

        });
    }
</script>

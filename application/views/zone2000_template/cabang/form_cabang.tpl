<!-- BEGIN PAGE HEADER-->
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<!-- BEGIN PAGE TITLE -->
		<div class="page-title">
			<h1>{{title}}</h1>
		</div>
		<!-- END PAGE TITLE -->
		
	</div>
	<!-- END PAGE HEAD -->

	<!-- END PAGE HEADER-->
	<!-- BEGIN PAGE CONTENT-->					
	<div class="row">
		<div class="col-md-12">
			<!-- BEGIN VALIDATION STATES-->
			<div class="portlet box green">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-gift"></i>
					</div>
				</div>
				<div class="portlet-body form">
					<!-- BEGIN FORM-->
					{{ form_action|raw }}
						<div class="form-body">
							<h3 class="form-section"></h3>
							<div class="alert alert-danger display-hide">
								<button class="close" data-close="alert"></button>
								You have some form errors. Please check below.
							</div>
							<div class="alert alert-success display-hide">
								<button class="close" data-close="alert"></button>
								Your form validation is successful!
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Branch Code<span class="required">
								* </span>
								</label>
								<div class="col-md-4">
									<input type="text" name="kd_cabang" data-required="1" class="form-control" value="{{def_kd_cabang}}"/>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">Branch Name<span class="required">
								* </span>
								</label>
								<div class="col-md-4">
									<input type="text" name="nama_cabang" value = "{{def_nama_cabang}}" data-required="1" class="form-control"/>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">Area<span class="required">
								* </span>
								</label>
								<div class="col-md-4">
									{{options_area|raw}}
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">Branch Address<span class="required">
								* </span>
								</label>
								<div class="col-md-4">
									<input type="text" name="alamat" value = "{{def_alamat}}" data-required="1" class="form-control"/>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">Telp<span class="required">
								* </span>
								</label>
								<div class="col-md-4">
									<input type="text" name="telp" value = "{{def_telp}}" data-required="1" class="form-control"/>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">Supervisor<span class="required">
								* </span>
								</label>
								<div class="col-md-4">
									{{options_supervisor|raw}}
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">Leader<span class="required">
								* </span>
								</label>
								<div class="col-md-4">
									{{options_kepcabang|raw}}
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">Wide<span class="required">
								* </span>
								</label>
								<div class="col-md-4">
									<input type="text" name="luas" value = "{{def_luas}}" data-required="1" class="form-control"/>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">Tax Of Branch<span class="required">
								* </span>
								</label>
								<div class="col-md-4">
									<input type="text" name="pajak_toko" value = "{{def_pajak_toko}}" data-required="1" class="form-control"/>
								</div>
							</div>


						</div>
						<div class="form-actions">
							<div class="row">
								<div class="col-md-offset-3 col-md-9">
									{{button_submit|raw}}
								</div>
							</div>
						</div>
					</form>
					<!-- END FORM-->
				</div>
				<!-- END VALIDATION STATES-->
			</div>
		</div>
	</div>
	<!-- END PAGE CONTENT-->
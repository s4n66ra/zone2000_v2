<div class="tabbable-custom show-overflow">
	<ul class="nav nav-tabs ">
		{% if page.cutter is not empty %}
		<li class="active">
			<a href="#cutter" data-toggle="tab">
			TICKET CUTTER </a>
		</li>
		{% endif %}

		{% if page.hist_cutter is not empty %}
		<li class="">
			<a href="#hist-cutter" data-toggle="tab">
			HISTORY TICKET CUTTER</a>
		</li>
		{% endif %}

		{% if page.ticket_refill is not empty %}
		<li class="active">
			<a href="#ticket-refill" data-toggle="tab">
			TICKET REFILL</a>
		</li>
		{% endif %}

		{% if page.hist_ticket_refill is not empty %}
		<li class="">
			<a href="#hist-ticket-refill" data-toggle="tab">
			HISTORY TICKET REFILL</a>
		</li>
		{% endif %}


		<!-- <li>
			<a href="#coin-count" data-toggle="tab">
			COIN COUNT</a>
		</li> -->
		{% if page.ticket_meter is not empty %}
		<li class="active">
			<a href="#ticket-meter" data-toggle="tab">
			TICKET METER</a>
		</li>
		{% endif %}

		{% if page.coin_kuras is not empty %}
		<li class="active">
			<a href="#coin-out" data-toggle="tab">
			KURAS KOIN</a>
		</li>
		{% endif %}

		{% if page.hist_coin_kuras is not empty %}
		<li class="">
			<a href="#hist-coin-out" data-toggle="tab">
			HISTORY KURAS KOIN</a>
		</li>
		{% endif %}

		

		{% if page.mc_refill is not empty %}
		<li class="active">
			<a href="#mc-refill" data-toggle="tab">
			VENDING</a>
		</li>
		{% endif %}

		{% if page.hist_mc_refill is not empty %}
		<li class="">
			<a href="#hist-mc-refill" data-toggle="tab">
			HISTORY VENDING</a>
		</li>
		{% endif %}


		{% if page.mc_redemp is not empty %}
		<li class="active">
			<a href="#mc-redemp" data-toggle="tab">
			REDEMP</a>
		</li>
		{% endif %}

		{% if page.hist_mc_redemp is not empty %}
		<li class="">
			<a href="#hist-mc-redemp" data-toggle="tab">
			HISTORY REDEMP</a>
		</li>
		{% endif %}

	</ul>

	<div class="tab-content">
		<div class="tab-pane active" id="cutter">
			{{ page.cutter | raw }}
		</div>

		<div class="tab-pane" id="hist-cutter">
			{{ page.hist_cutter | raw }}
		</div>

		<div class="tab-pane active" id="coin-count">
			{{ page.coin_meter | raw }}
		</div>

		<div class="tab-pane active" id="ticket-meter">
			{{ page.ticket_meter | raw }}
		</div>

		<div class="tab-pane active" id="coin-out">
			{{ page.coin_kuras | raw }}
		</div>

		<div class="tab-pane" id="hist-coin-out">
			{{ page.hist_coin_kuras | raw }}
		</div>

		<div class="tab-pane active" id="ticket-refill">
			{{ page.ticket_refill | raw }}
		</div>

		<div class="tab-pane" id="hist-ticket-refill">
			{{ page.hist_ticket_refill | raw }}
		</div>

		<div class="tab-pane active" id="mc-refill">
			{{ page.mc_refill | raw }}
		</div>

		<div class="tab-pane" id="hist-mc-refill">
			{{ page.hist_mc_refill | raw }}
		</div>

		<div class="tab-pane active" id="mc-redemp">
			{{ page.mc_redemp | raw }}
		</div>

		<div class="tab-pane" id="hist-mc-redemp">
			{{ page.hist_mc_redemp | raw }}
		</div>

	</div>

</div>


<div class="tabbable-custom show-overflow">
	<ul class="nav nav-tabs ">
		{% if page.cutter is not empty %}
		<li class="active">
			<a href="#cutter" data-toggle="tab">
			TICKET CUTTER </a>
		</li>
		{% endif %}

		{% if page.hist_cutter is not empty %}
		<li class="">
			<a href="#hist-cutter" data-toggle="tab">
			HISTORY TICKET CUTTER</a>
		</li>
		{% endif %}

		{% if page.ticket_refill is not empty %}
		<li class="active">
			<a href="#ticket-refill" data-toggle="tab">
			TICKET REFILL</a>
		</li>
		{% endif %}

		{% if page.hist_ticket_refill is not empty %}
		<li class="">
			<a href="#hist-ticket-refill" data-toggle="tab">
			HISTORY TICKET REFILL</a>
		</li>
		{% endif %}

		{% if page.ticket_payout is not empty %}
		<li class="active">
			<a href="#ticket-payout" data-toggle="tab">
			TICKET PAYOUT</a>
		</li>
		{% endif %}

		{% if page.hist_ticket_payout is not empty %}
		<li class="">
			<a href="#hist-ticket-payout" data-toggle="tab">
			HISTORY TICKET PAYOUT</a>
		</li>
		{% endif %}


		<!-- <li>
			<a href="#coin-count" data-toggle="tab">
			COIN COUNT</a>
		</li> -->
		{% if page.ticket_meter is not empty %}
		<li class="active">
			<a href="#ticket-meter" data-toggle="tab">
			TICKET METER</a>
		</li>
		{% endif %}

		{% if page.coin_kuras is not empty %}
		<li class="active">
			<a href="#coin-out" data-toggle="tab">
			KURAS KOIN</a>
		</li>
		{% endif %}

		{% if page.tambah_koin is not empty %}
		<li class="active">
			<a href="#coin-in" data-toggle="tab">
			TAMBAH KOIN</a>
		</li>
		{% endif %}

		{% if page.hist_coin_kuras is not empty %}
		<li class="">
			<a href="#hist-coin-out" data-toggle="tab">
			HISTORY KURAS KOIN</a>
		</li>
		{% endif %}

		{% if page.hist_tambah_koin is not empty %}
		<li class="">
			<a href="#hist-coin-in" data-toggle="tab">
			HISTORY TAMBAH KOIN</a>
		</li>
		{% endif %}

		{% if page.mc_refill is not empty %}
		<li class="active">
			<a href="#mc-refill" data-toggle="tab">
			VENDING</a>
		</li>
		{% endif %}

		{% if page.hist_mc_refill is not empty %}
		<li class="">
			<a href="#hist-mc-refill" data-toggle="tab">
			HISTORY VENDING</a>
		</li>
		{% endif %}


		{% if page.mc_redemp is not empty %}
		<li class="active">
			<a href="#mc-redemp" data-toggle="tab">
			REDEMP</a>
		</li>
		{% endif %}

		{% if page.hist_mc_redemp is not empty %}
		<li class="">
			<a href="#hist-mc-redemp" data-toggle="tab">
			HISTORY REDEMP</a>
		</li>
		{% endif %}

		{% if page.selling is not empty %}
		<li class="active">
			<a href="#selling" data-toggle="tab">
			PENJUALAN LANGSUNG</a>
		</li>
		{% endif %}

		{% if page.hist_selling is not empty %}
		<li class="">
			<a href="#hist-selling" data-toggle="tab">
			HISTORY PENJUALAN LANGSUNG</a>
		</li>
		{% endif %}

		{% if page.promosi is not empty %}
		<li class="active">
			<a href="#promosi" data-toggle="tab">
			PROMOSI</a>
		</li>
		{% endif %}

		{% if page.hist_promosi is not empty %}
		<li class="">
			<a href="#hist-promosi" data-toggle="tab">
			HISTORY PROMOSI</a>
		</li>
		{% endif %}

		{% if page.buying is not empty %}
		<li class="active">
			<a href="#buying" data-toggle="tab">
			PEMBELIAN LANGSUNG (STOK MD)</a>
		</li>
		{% endif %}

		{% if page.hist_buying is not empty %}
		<li class="">
			<a href="#hist-buying" data-toggle="tab">
			HISTORY PEMBELIAN LANGSUNG (STOK MD)</a>
		</li>
		{% endif %}

		{% if page.buying_sc is not empty %}
		<li class="active">
			<a href="#buying-sc" data-toggle="tab">
			PEMBELIAN LANGSUNG (STOK SPAREPART)</a>
		</li>
		{% endif %}

		{% if page.hist_buying_sc is not empty %}
		<li class="">
			<a href="#hist-buying-sc" data-toggle="tab">
			HISTORY PEMBELIAN LANGSUNG (STOK SPAREPART)</a>
		</li>
		{% endif %}

		{% if page.mutasi_sc_in is not empty %}
		<li class="active">
			<a href="#mutasi-sc-in" data-toggle="tab">
			MUTASI MASUK SC</a>
		</li>
		{% endif %}

		{% if page.hist_mutasi_sc_in is not empty %}
		<li class="">
			<a href="#hist-mutasi-sc-in" data-toggle="tab">
			HISTORY MUTASI MASUK SC</a>
		</li>
		{% endif %}

	</ul>

	<div class="tab-content">
		<div class="tab-pane active" id="cutter">
			{{ page.cutter | raw }}
		</div>

		<div class="tab-pane" id="hist-cutter">
			{{ page.hist_cutter | raw }}
		</div>

		<div class="tab-pane active" id="coin-count">
			{{ page.coin_meter | raw }}
		</div>

		<div class="tab-pane active" id="ticket-meter">
			{{ page.ticket_meter | raw }}
		</div>

		<div class="tab-pane active" id="coin-out">
			{{ page.coin_kuras | raw }}
		</div>

		<div class="tab-pane" id="hist-coin-out">
			{{ page.hist_coin_kuras | raw }}
		</div>

		<div class="tab-pane active" id="coin-in">
			{{ page.tambah_koin | raw }}
		</div>

		<div class="tab-pane" id="hist-coin-in">
			{{ page.hist_tambah_koin | raw }}
		</div>

		<div class="tab-pane active" id="ticket-refill">
			{{ page.ticket_refill | raw }}
		</div>

		<div class="tab-pane" id="hist-ticket-refill">
			{{ page.hist_ticket_refill | raw }}
		</div>

		<div class="tab-pane active" id="ticket-payout">
			{{ page.ticket_payout | raw }}
		</div>

		<div class="tab-pane" id="hist-ticket-payout">
			{{ page.hist_ticket_payout | raw }}
		</div>

		<div class="tab-pane active" id="mc-refill">
			{{ page.mc_refill | raw }}
		</div>

		<div class="tab-pane" id="hist-mc-refill">
			{{ page.hist_mc_refill | raw }}
		</div>

		<div class="tab-pane active" id="mc-redemp">
			{{ page.mc_redemp | raw }}
		</div>

		<div class="tab-pane" id="hist-mc-redemp">
			{{ page.hist_mc_redemp | raw }}
		</div>

		<div class="tab-pane active" id="selling">
			{{ page.selling | raw }}
		</div>

		<div class="tab-pane" id="hist-selling">
			{{ page.hist_selling | raw }}
		</div>

		<div class="tab-pane active" id="promosi">
			{{ page.promosi | raw }}
		</div>

		<div class="tab-pane" id="hist-promosi">
			{{ page.hist_promosi | raw }}
		</div>

		<div class="tab-pane active" id="buying">
			{{ page.buying | raw }}
		</div>

		<div class="tab-pane" id="hist-buying">
			{{ page.hist_buying | raw }}
		</div>

		<div class="tab-pane active" id="buying-sc">
			{{ page.buying_sc | raw }}
		</div>

		<div class="tab-pane" id="hist-buying-sc">
			{{ page.hist_buying_sc | raw }}
		</div>

		<div class="tab-pane active" id="mutasi-sc-in">
			{{ page.mutasi_sc_in | raw }}
		</div>

		<div class="tab-pane" id="hist-mutasi-sc-in">
			{{ page.hist_mutasi_sc_in | raw }}
		</div>

	</div>

</div>


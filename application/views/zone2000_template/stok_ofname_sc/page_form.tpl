<div class="row">
		
	<div class="col-md-12">
		<div class="tabbable-custom">
			<ul class="nav nav-tabs ">
				<li class="active">
					<a href="#tab_15_1" data-toggle="tab">
					General</a>
				</li>
				{% if act!='add' %}
				{% endif %}
			</ul>
			
			<div class="tab-content">
				
				<div class="tab-pane active" id="tab_15_1">
					{{ form.main|raw }}
				</div>

				<div class="tab-pane" id="tab_15_2">
					{{ form.area|raw }}
				</div>

				<div class="tab-pane" id="tab_15_3">
					{{ form.target|raw }}
				</div>

				<div class="tab-pane" id="tab_15_4">
					
				</div>
				
			</div>
		</div>
	</div>
	

</div>
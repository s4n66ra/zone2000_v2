{{ form_action|raw }}
	<div class="row">
		<div class="form-body col-md-12">
			<div class="form-group row">
				<label class="control-label col-md-4">Location Code <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="data[loc_code]" data-required="1" class="form-control" value="{{data.loc_code}}"/>
				</div>
			</div>
			
			<div class="form-group row">
				<label class="control-label col-md-4">Location Name <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="data[loc_name]" data-required="1" class="form-control" value="{{data.loc_name}}"/>
				</div>
			</div>
			<div class="form-group row">
				<label class="control-label col-md-4">Order <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="data[urutan]" value = "{{data.urutan}}" data-required="1" class="form-control"/>
				</div>
			</div>
		</div>
	</div>
</form>
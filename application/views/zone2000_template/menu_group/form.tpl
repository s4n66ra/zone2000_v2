
{{ form_action|raw }}
	<div class="form-body">
		<h3 class="form-section"></h3>
		<div class="alert alert-danger display-hide">
			<button class="close" data-close="alert"></button>
			You have some form errors. Please check below.
		</div>
		<div class="alert alert-success display-hide">
			<button class="close" data-close="alert"></button>
			Your form validation is successful!
		</div>

		<div class="form-group row">
			<label class="control-label col-md-4">Nama Grup Menu <span class="required">
			* </span>
			</label>
			<div class="col-md-8">
				<input type="text" name="nama" value = "{{data.nama_grup_menu}}" data-required="1" class="form-control"/>
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-md-4">Icon <span class="required">
			* </span>
			</label>
			<div class="col-md-8">
				<input type="hidden" name="icon" value = "{{data.icon}}" data-required="1" class="form-control form-icon"/>
				{{button.icon|raw}}
			</div>
		</div>

	</div>

</form>

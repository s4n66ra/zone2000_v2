{{ form_action|raw }}
	<div class="form-body">
		<h3 class="form-section"></h3>
		<div class="alert alert-danger display-hide">
			<button class="close" data-close="alert"></button>
			You have some form errors. Please check below.
		</div>
		<div class="alert alert-success display-hide">
			<button class="close" data-close="alert"></button>
			Your form validation is successful!
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Nama Roles <span class="required">
			* </span>
			</label>
			<div class="col-md-4">
				<input type="text" name="nama_role" data-required="1" class="form-control" value="{{def_grup_nama}}"/>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3">Deskripsi <span class="required">
			* </span>
			</label>
			<div class="col-md-4">
				<input type="text" name="deskripsi" value = "{{def_grup_deskripsi}}" data-required="1" class="form-control"/>
			</div>
		</div>
	</div>
	{{table|raw}}
	<div class="form-actions">
		<div class="row">
			<div class="col-md-offset-3 col-md-9">
				{{button_submit|raw}}
			</div>
		</div>
	</div>
</form>

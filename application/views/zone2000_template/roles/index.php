<div class="inner_content">
    <div class="user_bar">
        <div class="row-fluid">
            <div class="float_left">
                <h3><span><?php echo (isset($title)) ? $title : 'Untitle'; ?></span></h3>
            </div>
        </div>
    </div>
    <div class="widgets_area">     
        <div class="row-fluid">
            <div class="span6">
                <div id="index-content" class="well-content">

                    <div class="well">
                        <div class="pull-left">
                            <?php echo hgenerator::render_button_group($button_group); ?>
                        </div>
                    </div>

                    <div class="row-fluid">
                        <div class="span12">
                            <div class="well">
                                <div class="well-content clearfix">

                                    <?php echo form_open_multipart('', array('id' => 'ffilter')); ?>
                                    <div class="form_row">
                                        <div class="pull-left span4">
                                            <label class="control-label">Nama Role :</label>
                                            <div class="controls">
												<?php echo form_dropdown('kata_kunci', $options, '', 'class="span11 onchange chosen"'); ?>
                                                <?php //echo form_input('kata_kunci', '', 'class="span11 onchange"') ?>
                                            </div>
                                        </div>
                                        <div class="pull-left span4">
                                            <label class="control-label">Deskripsi :</label>
                                            <div class="controls">
                                                <?php echo form_input('deskripsi', '', 'class="span11 onchange"') ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php echo form_close(); ?>

                                </div>
                            </div> 
                        </div>
                    </div>

                    <div id="content_table" data-source="<?php echo $data_source; ?>" data-filter="#ffilter"></div>
                    <div>&nbsp;</div>
                </div>

                <div id="form-content" class="well-content"></div>
            </div>
        </div>
    </div>
</div>
</div>

<script type="text/javascript">
    $(function() {

        load_table('#content_table', 1);

        $('.onchange').change(function() {
            load_table('#content_table', 1);
        });

    });

</script>

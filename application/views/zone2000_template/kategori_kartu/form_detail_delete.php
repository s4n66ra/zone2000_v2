<div class="row-fluid">
    <div class="box-title">
        <?php echo (isset($title)) ? $title : 'Untitle'; ?>
    </div>
    <div class="box-content">
        <?php
        $hidden_form = array('id' => !empty($id) ? $id : '',
            'id_bbm' => !empty($id_bbm) ? $id_bbm : '');
        echo form_open($form_action, array('id' => 'finput-detail-'.$id_bbm, 'class' => 'form-horizontal'), $hidden_form);
        ?>


        <div class="control-group">
            <label for="select" class="control-label">Nama Barang<span class="required">*</span></label>
            <div class="controls">
                <?php echo form_input('kd_barang', !empty($default->nama_barang) ? $default->nama_barang : '', 'class="span6 " readonly') ?>
                
            </div>
        </div> 
        <div class="control-group">
            <label for="select" class="control-label">Jumlah<span class="required">*</span></label>
            <div class="controls">
                <?php echo form_input('jumlah', !empty($default->jumlah_barang) ? $default->jumlah_barang : '', 'class="span6" readonly') ?>
                
            </div>
        </div> 

        <div class="form-actions">
            <?php echo anchor(null, '<i class="icon-trash"></i> Hapus', array('id' => 'button-save', 'class' => 'red btn', 'onclick' => "simpan_data(this.id, '#finput-detail-$id_bbm', '#button-back')")); ?>
            <?php echo anchor(null, '<i class="icon-circle-arrow-left"></i> Kembali', array('id' => 'button-back', 'class' => 'btn', 'onclick' => 'close_form_modal(this.id)')); ?>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('.date').datepicker({
            format: 'dd-mm-yyyy',
        });
        $('.chosen').chosen();
        $('.numeric').numeric();
    });
    function refresh_filter(){ 
        load_table('#content_table_detail_<?php echo $id_bbm;?>', 1,function(){

        });
    }
</script>

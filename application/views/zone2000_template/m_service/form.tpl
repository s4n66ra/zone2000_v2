{{ form_action|raw }}
	<div class="row">
		<div class="form-body col-md-12">
			<div class="form-group row">
				<label class="control-label col-md-4">Nama <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="data[nama]" data-required="1" class="form-control" value="{{data.nama}}"/>
				</div>
			</div>
			
			<div class="form-group row">
				<label class="control-label col-md-4">Expire Time (In Days) <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="data[exp_time]" data-required="1" class="form-control" value="{{data.exp_time}}"/>
				</div>
			</div>
			<div class="form-group row">
				<label class="control-label col-md-4">Urutan <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="data[urutan]" value = "{{data.urutan}}" data-required="1" class="form-control"/>
				</div>
			</div>
		</div>
	</div>
</form>
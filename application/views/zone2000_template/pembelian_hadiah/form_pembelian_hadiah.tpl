{{ form_action|raw }}
	<div class="form-body col-md-12">
		<h3 class="form-section"></h3>
		<div class="alert alert-danger display-hide">
			<button class="close" data-close="alert"></button>
			You have some form errors. Please check below.
		</div>
		<div class="alert alert-success display-hide">
			<button class="close" data-close="alert"></button>
			Your form validation is successful!
		</div>


		<div class="row">
			<div class="col-md-12">
				<div class="form-group row">
					<label class="control-label col-md-4">Purchase Code <span class="required">
					* </span>
					</label>
					<div class="col-md-8">
						<input type="text" name="kd_pembelian" data-required="1" class="form-control" value="{{ data.kd_pembelian }}" {{readonly}}/>
					</div>
				</div>

				<div class="form-group row">
					<label class="control-label col-md-4">Branch Name				
					</label>
					<div class="col-md-8">
						{{ list_cabang|raw }}
					</div>
				</div>

				<div class="form-group row">
					<label class="control-label col-md-4">Supplier Name				
					</label>
					<div class="col-md-8">
						{{ list_supplier|raw }}
					</div>
				</div>

				<div class="form-group row">
					<label class="control-label col-md-4">Status<span class="required">
					* </span>
					</label>
					<div class="col-md-8">
						<input type="text" name="status" data-required="1" class="form-control" value="{{ data.status }}" {{readonly}}/>
					</div>
				</div>

				<div class="form-group row">
					<label class="control-label col-md-4">Pay Status<span class="required">
					* </span>
					</label>
					<div class="col-md-8">
						<input type="text" name="status_bayar" data-required="1" class="form-control" value="{{ data.status_bayar }}" {{readonly}}/>
					</div>
				</div>

				<div class="form-group row">
					<label class="control-label col-md-4">Created Date
					</label>
					<div class="col-md-8">
						<!-- <input class="form-control input-medium date-picker" size="16" type="text" value="" data-date-format="yyyy-mm-dd"/> -->
						<div class="input-group input-medium date date-picker-pembelian-hadiah" data-date-format="yyyy-mm-dd">
							<input type="text" name="tgl_dibuat" class="form-control" value="{{data.tgl_dibuat}}" {{readonly}}>
							<span class="input-group-btn">
							<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
							</span>
						</div>
					</div>
				</div>

				<div class="form-group row">
					<label class="control-label col-md-4">Arrival Date
					</label>
					<div class="col-md-8">
						<!-- <input class="form-control input-medium date-picker" size="16" type="text" value="" data-date-format="yyyy-mm-dd"/> -->
						<div class="input-group input-medium date date-picker-pembelian-hadiah" data-date-format="yyyy-mm-dd">
							<input type="text" name="tgl_sampai" class="form-control" value="{{data.tgl_sampai}}" {{readonly}}>
							<span class="input-group-btn">
							<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
							</span>
						</div>
					</div>
				</div>



			</div>

		</div>



	</div>
</form>



<script>
	jQuery(document).ready(function(){ 
		if ($().select2) {
            $('.select2me').select2({
                placeholder: "Select",
                allowClear: true
            });
        }

        if ($().datepicker) {
            $('.date-picker-pembelian-hadiah').datepicker({
                rtl: Metronic.isRTL(),
                orientation: "left",
                autoclose: true
            });
            //$('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
        }
	});
</script>
<div class="tab-pane fade active in" id="form-pegawai">
	<div class="row">
		{{ form_action|raw }}
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label col-md-4">Employee Code <span class="required"> * </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="data[nik]" data-required="1" class="form-control" value="{{data.nik}}" />
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-4">Employee Name<span class="required"> * </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="data[nama_pegawai]" value="{{data.nama_pegawai}}" data-required="1" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-4">Phone Number<span class="required"> * </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="data[no_hp]" value="{{data.no_hp}}" data-required="1" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-4">Address </label>
				<div class="col-md-8">
					<input type="text" name="data[alamat]" value="{{data.alamat}}" data-required="1" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-4">Position<span class="required"> * </span>
				</label>
				<div class="col-md-8">{{positions|raw}}</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-4">Level<span class="required"> * </span>
				</label>
				<div class="col-md-8">{{levels|raw}}</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-4">In Date<span class="required"> * </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="data[tgl_masuk]" value="{{data.tgl_masuk}}" data-required="1" class="form-control date-picker" />
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label col-md-4">Out Date </label>
				<div class="col-md-8">
					<input type="text" name="data[tgl_keluar]" value="{{data.tgl_keluar}}" data-required="1" class="form-control date-picker" />
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-4">Salary </label>
				<div class="col-md-8">
					<input type="text" name="data[salary]" value="{{data.salary}}" data-required="1" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-4">Status </label>
				<div class="col-md-8">
					<input type="text" name="data[status_pegawai]" value="{{data.status}}" data-required="1" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-4">Info </label>
				<div class="col-md-8">
					<input type="text" name="data[keterangan]" value="{{data.keterangan}}" data-required="1" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-4">User ID </label>
				<div class="col-md-8">
					<input type="text" name="data[user_id]" value="{{data.user_id}}" data-required="1" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-4">Branch </label>
				<div class="col-md-8">{{branches|raw}}</div>
			</div>
		</div>
		</form>
	</div>
</div>

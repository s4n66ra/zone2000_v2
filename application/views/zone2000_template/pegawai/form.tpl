<div class="form-body" style="margin-bottom: 10px">
	<h3 class="form-section"></h3>
	<div class="alert alert-danger display-hide">
		<button class="close" data-close="alert"></button>
		You have some form errors. Please check below.
	</div>
	<div class="alert alert-success display-hide">
		<button class="close" data-close="alert"></button>
		Your form validation is successful!
	</div>

	<ul class="nav nav-tabs">
		<li class="active"><a href="#form-pegawai" data-toggle="tab"> Employee </a></li>
		<li><a href="#form-user" data-toggle="tab"> User </a></li>
		<li><a href="#form-npwp" data-toggle="tab"> NPWP </a></li>
		<li><a href="#form-ahli-waris" data-toggle="tab"> Ahli Waris </a></li>
		<li><a href="#form-pendidikan" data-toggle="tab"> Pendidikan </a></li>
		<li><a href="#form-referensi" data-toggle="tab"> Referensi </a></li>
		<li><a href="#form-riwayat-kerja" data-toggle="tab"> Riwayat Kerja </a></li>
		<li><a href="#form-data-tanggungan" data-toggle="tab"> Data Tanggungan </a></li>
		<li><a href="#form-susunan-keluarga" data-toggle="tab"> Susunan Keluarga </a></li>
		<li><a href="#form-kontak-darurat" data-toggle="tab"> Kontak Darurat </a></li>
	</ul>
	{{ form_action|raw }}
	<div class="tab-content">
		<div class="tab-pane fade active in" id="form-pegawai">
			<div class="row">

				<div class="col-md-6">
					<div class="form-group row">
						<label class="control-label col-md-4">NIK <span class="required"> * </span>
						</label>
						<div class="col-md-8">
							<input type="text" name="data[id_karyawan]" data-required="1" class="form-control" value="{{data.id_karyawan}}" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">ID <span class="required"> * </span>
						</label>
						<div class="col-md-8">
							<input type="text" name="data[nik]" data-required="1" class="form-control" value="{{data.nik}}" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Nama Lengkap<span class="required"> * </span>
						</label>
						<div class="col-md-8">
							<input type="text" name="data[nama_pegawai]" value="{{data.nama_pegawai}}" data-required="1" class="form-control" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Nama Panggilan
						</label>
						<div class="col-md-8">
							<input type="text" name="data[nama_panggilan]" value="{{data.nama_panggilan}}" data-required="1" class="form-control" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Divisi <span class="required"> * </span>
						</label>
						<div class="col-md-8">
							{{divisi|raw}}
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Bagian
						</label>
						<div class="col-md-8">
							<input type="text" name="data[bagian]" value="{{data.bagian}}" data-required="1" class="form-control" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">No. KTP
						</label>
						<div class="col-md-8">
							<input type="text" name="data[ktp]" value="{{data.ktp}}" data-required="1" class="form-control" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">No. Paspor
						</label>
						<div class="col-md-8">
							<input type="text" name="data[paspor]" value="{{data.paspor}}" data-required="1" class="form-control" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Jenis Kelamin
						</label>
						<div class="col-md-8">
							<input type="text" name="data[jenis_kelamin]" value="{{data.jenis_kelamin}}" data-required="1" class="form-control" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Tempat Lahir
						</label>
						<div class="col-md-8">
							<input type="text" name="data[tempat_lahir]" value="{{data.tempat_lahir}}" data-required="1" class="form-control" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Tanggal Lahir
						</label>
						<div class="col-md-8">
							<input type="text" name="data[tanggal_lahir]" value="{{data.tanggal_lahir}}" data-required="1" class="form-control date-picker" data-date-format="yyyy-mm-dd" />
						</div>
					</div>
					
					<div class="form-group row">
						<label class="control-label col-md-4">Alamat Lengkap </label>
						<div class="col-md-8">
							<input type="text" name="data[alamat]" value="{{data.alamat}}" data-required="1" class="form-control" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Kelurahan </label>
						<div class="col-md-8">
							<input type="text" name="data[kelurahan]" value="{{data.kelurahan}}" data-required="1" class="form-control" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Kecamatan </label>
						<div class="col-md-8">
							<input type="text" name="data[kecamatan]" value="{{data.kecamatan}}" data-required="1" class="form-control" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Kota </label>
						<div class="col-md-8">
							<input type="text" name="data[kota]" value="{{data.kota}}" data-required="1" class="form-control" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Kode Pos </label>
						<div class="col-md-8">
							<input type="text" name="data[kode_pos]" value="{{data.kode_pos}}" data-required="1" class="form-control" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Provinsi </label>
						<div class="col-md-8">
							<input type="text" name="data[provinsi]" value="{{data.provinsi}}" data-required="1" class="form-control" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Negara </label>
						<div class="col-md-8">
							<input type="text" name="data[negara]" value="{{data.negara}}" data-required="1" class="form-control" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Telp Rumah </label>
						<div class="col-md-8">
							<input type="text" name="data[telp_rumah]" value="{{data.telp_rumah}}" data-required="1" class="form-control" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">No Handphone
						</label>
						<div class="col-md-8">
							<input type="text" name="data[no_hp]" value="{{data.no_hp}}" data-required="1" class="form-control" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">No Pager </label>
						<div class="col-md-8">
							<input type="text" name="data[no_pager]" value="{{data.no_pager}}" data-required="1" class="form-control" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Agama </label>
						<div class="col-md-8">
							<input type="text" name="data[agama]" value="{{data.agama}}" data-required="1" class="form-control" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Suku </label>
						<div class="col-md-8">
							<input type="text" name="data[suku]" value="{{data.suku}}" data-required="1" class="form-control" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Ukuran Baju </label>
						<div class="col-md-8">
							<input type="text" name="data[ukuran_baju]" value="{{data.ukuran_baju}}" data-required="1" class="form-control" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Ukuran Sepatu </label>
						<div class="col-md-8">
							<input type="text" name="data[ukuran_sepatu]" value="{{data.ukuran_sepatu}}" data-required="1" class="form-control" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Tinggi Badan </label>
						<div class="col-md-8">
							<input type="text" name="data[tinggi_badan]" value="{{data.tinggi_badan}}" data-required="1" class="form-control" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Berat Badan </label>
						<div class="col-md-8">
							<input type="text" name="data[berat_badan]" value="{{data.berat_badan}}" data-required="1" class="form-control" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Gol. Darah </label>
						<div class="col-md-8">
							<input type="text" name="data[gol_darah]" value="{{data.gol_darah}}" data-required="1" class="form-control" />
						</div>
					</div>
					
					
				</div>
				<div class="col-md-6">
					<div class="form-group row">
						<label class="control-label col-md-4">Position / Roles<span class="required"> * </span>
						</label>
						<div class="col-md-8">{{roles|raw}}</div>
					</div>
					
					<div class="form-group row">
						<label class="control-label col-md-4">Salary </label>
						<div class="col-md-8">
							<input type="text" name="data[salary]" value="{{data.salary}}" data-required="1" class="form-control" />
						</div>
					</div>
					<!-- <input type="hidden" name="data[salary]" value="{{data.salary}}"/> -->
					<div class="form-group row">
						<label class="control-label col-md-4">Status </label>
						<div class="col-md-8">
							<input type="text" name="data[status_pegawai]" value="{{data.status}}" data-required="1" class="form-control" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Keterangan </label>
						<div class="col-md-8">
							<input type="text" name="data[keterangan]" value="{{data.keterangan}}" data-required="1" class="form-control" />
						</div>
					</div>
					
				</div>

			</div>
		</div>
		<div class="tab-pane fade" id="form-user" style="position: relative; top: 0px">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group row">
						<label class="control-label col-md-4">User Name </label>
						<div class="col-md-8">
							<input type="text" name="data[user_name]" value="{{data.user_name}}" data-required="1" class="form-control" />
							<input type="hidden" name="data[user_id]" value="{{data.user_id}}" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Password </label>
						<div class="col-md-8">
							<input name="data[user_password]" data-required="1" class="form-control" type="password" /> {{additional_info|raw}}
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Confirm Password </label>
						<div class="col-md-8">
							<input name="data[user_confirm_password]" data-required="1" class="form-control" type="password" /> {{additional_info|raw}}
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="tab-pane fade" id="form-npwp" style="position: relative; top: 0px">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group row">
						<label class="control-label col-md-4">No NPWP 
						</label>
						<div class="col-md-8">
							<input type="text" name="data[npwp]" data-required="1" class="form-control" value="{{data.npwp}}" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Tanggal Masuk
						</label>
						<div class="col-md-8">
							<input type="text" name="data[tgl_masuk]" value="{{data.tgl_masuk}}" data-required="1" class="form-control date-picker" data-date-format="yyyy-mm-dd" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Tanggal Keluar </label>
						<div class="col-md-8">
							<input type="text" name="data[tgl_keluar]" value="{{data.tgl_keluar}}" data-required="1" class="form-control date-picker" data-date-format="yyyy-mm-dd" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Tanggal Pengangkatan
						</label>
						<div class="col-md-8">
							<input type="text" name="data[tgl_pengangkatan]" value="{{data.tgl_pengangkatan}}" data-required="1" class="form-control date-picker" data-date-format="yyyy-mm-dd" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Bank</label>
						<div class="col-md-8">
							<input type="text" name="data[bank]" value="{{data.bank}}" data-required="1" class="form-control" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">No. AIA</label>
						<div class="col-md-8">
							<input type="text" name="data[no_aia]" value="{{data.no_aia}}" data-required="1" class="form-control" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">No Rek Bank<span class="required"> * </span>
						</label>
						<div class="col-md-8">
							<input type="text" name="data[no_rek]" value="{{data.no_rek}}" data-required="1" class="form-control" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">No Astek<span class="required"> * </span>
						</label>
						<div class="col-md-8">
							<input type="text" name="data[no_astek]" value="{{data.no_astek}}" data-required="1" class="form-control" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Area<span class="required"> * </span>
						</label>
						<div class="col-md-8">
							<input type="text" name="data[area]" value="{{data.area}}" data-required="1" class="form-control" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Level<span class="required"> * </span>
						</label>
						<div class="col-md-8">{{levels|raw}}</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Surat Peringatan </label>
						<div class="col-md-8">
							<input type="text" name="data[level_status]" value="{{data.level_status}}" data-required="1" class="form-control" />
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group row">
						<label class="control-label col-md-4">Wilayah </label>
						<div class="col-md-8">
							<input type="text" name="data[wilayah]" value="{{data.wilayah}}" data-required="1" class="form-control" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Lokasi Sekarang </label>
						<div class="col-md-8">{{branches|raw}}</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Lokasi Asal </label>
						<div class="col-md-8">{{branches_old|raw}}</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Lokasi Gaji </label>
						<div class="col-md-8">{{branches_gaji|raw}}</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Jabatan </label>
						<div class="col-md-8">
							<input type="text" name="data[jabatan]" value="{{data.jabatan}}" data-required="1" class="form-control" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Divisi </label>
						<div class="col-md-8">
							<input type="text" name="data[level_divisi]" value="{{data.level_divisi}}" data-required="1" class="form-control" />
						</div>
					</div>
				</div>

			</div>
		</div>
		<div class="tab-pane fade" id="form-ahli-waris" style="position: relative; top: 0px">
			<div class="row">
				
				<div class="col-md-6">
					<div class="portlet light bordered">
						<h3 class="form-section">Ahli Waris 1 </h3>
						<div class="form-group row">
							<label class="control-label col-md-4">Nama
							</label>
							<div class="col-md-8">
								<input type="text" name="data[ahli_waris_1]" data-required="1" class="form-control" value="{{data.ahli_waris_1}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Hubungan 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[hubungan_waris_1]" data-required="1" class="form-control" value="{{data.hubungan_waris_1}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Alamat 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[alamat_1]" data-required="1" class="form-control" value="{{data.alamat_1}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Telepon 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[telp_1]" data-required="1" class="form-control" value="{{data.telp_1}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Pendidikan 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[pendidikan_1]" data-required="1" class="form-control" value="{{data.pendidikan_1}}" />
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="portlet light bordered">
						<h3 class="form-section">Ahli Waris 2 </h3>
						<div class="form-group row">
							<label class="control-label col-md-4">Nama
							</label>
							<div class="col-md-8">
								<input type="text" name="data[ahli_waris_2]" data-required="1" class="form-control" value="{{data.ahli_waris_2}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Hubungan 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[hubungan_waris_2]" data-required="1" class="form-control" value="{{data.hubungan_waris_2}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Alamat 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[alamat_2]" data-required="1" class="form-control" value="{{data.alamat_2}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Telepon 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[telp_2]" data-required="1" class="form-control" value="{{data.telp_2}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Pendidikan 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[pendidikan_2]" data-required="1" class="form-control" value="{{data.pendidikan_2}}" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	
		<div class="tab-pane fade" id="form-pendidikan" style="position: relative; top: 0px">
			<div class="row">
				<div class="col-md-8 ">
					<div class="col-md-12 portlet light bordered">
						<h3 class="form-section">Pendidikan </h3>
						<div class="form-group row">
							<label class="control-label col-md-2">SD
							</label>
							<div class="col-md-4">
								<input type="text" name="data[sd]" data-required="1" class="form-control" value="{{data.sd}}" />
							</div>
							<label class="control-label col-md-2">Lulus Th: 
							</label>
							<div class="col-md-4">
								<input type="text" name="data[lulus_sd]" data-required="1" class="form-control" value="{{data.lulus_sd}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-2">SMP 
							</label>
							<div class="col-md-4">
								<input type="text" name="data[smp]" data-required="1" class="form-control" value="{{data.smp}}" />
							</div>
							<label class="control-label col-md-2">Lulus Th: 
							</label>
							<div class="col-md-4">
								<input type="text" name="data[lulus_smp]" data-required="1" class="form-control" value="{{data.lulus_smp}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-2">SMU 
							</label>
							<div class="col-md-4">
								<input type="text" name="data[smu]" data-required="1" class="form-control" value="{{data.smu}}" />
							</div>
							<label class="control-label col-md-2">Lulus Th: 
							</label>
							<div class="col-md-4">
								<input type="text" name="data[lulus_smu]" data-required="1" class="form-control" value="{{data.lulus_smu}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-2">D3 
							</label>
							<div class="col-md-4">
								<input type="text" name="data[d3]" data-required="1" class="form-control" value="{{data.d3}}" />
							</div>
							<label class="control-label col-md-2">Lulus Th: 
							</label>
							<div class="col-md-4">
								<input type="text" name="data[lulus_d3]" data-required="1" class="form-control" value="{{data.lulus_d3}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-2">S1 
							</label>
							<div class="col-md-4">
								<input type="text" name="data[s1]" data-required="1" class="form-control" value="{{data.s1}}" />
							</div>
							<label class="control-label col-md-2">Lulus Th: 
							</label>
							<div class="col-md-4">
								<input type="text" name="data[lulus_s1]" data-required="1" class="form-control" value="{{data.lulus_s1}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-2">S2
							</label>
							<div class="col-md-4">
								<input type="text" name="data[s2]" data-required="1" class="form-control" value="{{data.s2}}" />
							</div>
							<label class="control-label col-md-2">Lulus Th: 
							</label>
							<div class="col-md-4">
								<input type="text" name="data[lulus_s2]" data-required="1" class="form-control" value="{{data.lulus_s2}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-2">S3
							</label>
							<div class="col-md-4">
								<input type="text" name="data[s3]" data-required="1" class="form-control" value="{{data.s3}}" />
							</div>
							<label class="control-label col-md-2">Lulus Th: 
							</label>
							<div class="col-md-4">
								<input type="text" name="data[lulus_s3]" data-required="1" class="form-control" value="{{data.lulus_s3}}" />
							</div>
						</div>
					</div>

				</div>
				<div class="col-md-4">
					<div class="portlet light bordered">
						<h3 class="form-section">Bahasa yang dikuasai </h3>
						<div class="form-group row">
							<label class="control-label col-md-2">1
							</label>
							<div class="col-md-10">
								<input type="text" name="data[bahasa_1]" data-required="1" class="form-control" value="{{data.bahasa_1}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-2">2 
							</label>
							<div class="col-md-10">
								<input type="text" name="data[bahasa_2]" data-required="1" class="form-control" value="{{data.bahasa_2}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-2">3 
							</label>
							<div class="col-md-10">
								<input type="text" name="data[bahasa_3]" data-required="1" class="form-control" value="{{data.bahasa_3}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-2">4 
							</label>
							<div class="col-md-10">
								<input type="text" name="data[bahasa_4]" data-required="1" class="form-control" value="{{data.bahasa_4}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-2">5 
							</label>
							<div class="col-md-10">
								<input type="text" name="data[bahasa_5]" data-required="1" class="form-control" value="{{data.bahasa_5}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-2">6
							</label>
							<div class="col-md-10">
								<input type="text" name="data[bahasa_6]" data-required="1" class="form-control" value="{{data.bahasa_6}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-2">7
							</label>
							<div class="col-md-10">
								<input type="text" name="data[bahasa_7]" data-required="1" class="form-control" value="{{data.bahasa_7}}" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="tab-pane fade" id="form-referensi" style="position: relative; top: 0px">
			<div class="row">
				<div class="col-md-8">
					<div class="form-group row">
						<label class="control-label col-md-4">Nama Referensi </label>
						<div class="col-md-8">
							<input type="text" name="data[nama_referensi]" value="{{data.nama_referensi}}" data-required="1" class="form-control" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Asal (Dalam/Luar Ramayana) </label>
						<div class="col-md-8">
							<input type="text" name="data[asal_referensi]" value="{{data.asal_referensi}}" data-required="1" class="form-control" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Alamat </label>
						<div class="col-md-8">
							<input type="text" name="data[alamat_referensi]" value="{{data.alamat_referensi}}" data-required="1" class="form-control" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Telepon </label>
						<div class="col-md-8">
							<input type="text" name="data[telp_referensi]" value="{{data.telp_referensi}}" data-required="1" class="form-control" />
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="tab-pane fade" id="form-riwayat-kerja" style="position: relative; top: 0px">
			<div class="row">
				<div class="col-md-6">
					<div class="col-md-12 portlet light bordered">
						<div class="form-group row">
							<label class="control-label col-md-4">Nama PT
							</label>
							<div class="col-md-8">
								<input type="text" name="data[nama_pt_1]" data-required="1" class="form-control" value="{{data.nama_pt_1}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Jenis Usaha 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[jenis_usaha_1]" data-required="1" class="form-control" value="{{data.jenis_usaha_1}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Alamat 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[alamat_pt_1]" data-required="1" class="form-control" value="{{data.alamat_pt_1}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Gaji Awal 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[gaji_awal_1]" data-required="1" class="form-control" value="{{data.gaji_awal_1}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Gaji Akhir 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[gaji_akhir_1]" data-required="1" class="form-control" value="{{data.gaji_akhir_1}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Tugas Utama 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[tugas_utama_1]" data-required="1" class="form-control" value="{{data.gaji_akhir_1}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Alasan Berhenti
							</label>
							<div class="col-md-8">
								<input type="text" name="data[alasan_berhenti_1]" data-required="1" class="form-control" value="{{data.alasan_berhenti_1}}" />
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="form-group row">
							<label class="control-label col-md-4">Nama PT
							</label>
							<div class="col-md-8">
								<input type="text" name="data[nama_pt_2]" data-required="1" class="form-control" value="{{data.nama_pt_2}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Jenis Usaha 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[jenis_usaha_2]" data-required="1" class="form-control" value="{{data.jenis_usaha_2}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Alamat 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[alamat_pt_2]" data-required="1" class="form-control" value="{{data.alamat_pt_2}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Gaji Awal 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[gaji_awal_2]" data-required="1" class="form-control" value="{{data.gaji_awal_2}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Gaji Akhir 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[gaji_akhir_2]" data-required="1" class="form-control" value="{{data.gaji_akhir_2}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Tugas Utama 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[tugas_utama_2]" data-required="1" class="form-control" value="{{data.gaji_akhir_2}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Alasan Berhenti
							</label>
							<div class="col-md-8">
								<input type="text" name="data[alasan_berhenti_2]" data-required="1" class="form-control" value="{{data.alasan_berhenti_2}}" />
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="form-group row">
							<label class="control-label col-md-4">Nama PT
							</label>
							<div class="col-md-8">
								<input type="text" name="data[nama_pt_3]" data-required="1" class="form-control" value="{{data.nama_pt_3}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Jenis Usaha 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[jenis_usaha_3]" data-required="1" class="form-control" value="{{data.jenis_usaha_3}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Alamat 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[alamat_pt_3]" data-required="1" class="form-control" value="{{data.alamat_pt_3}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Gaji Awal 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[gaji_awal_3]" data-required="1" class="form-control" value="{{data.gaji_awal_3}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Gaji Akhir 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[gaji_akhir_3]" data-required="1" class="form-control" value="{{data.gaji_akhir_3}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Tugas Utama 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[tugas_utama_3]" data-required="1" class="form-control" value="{{data.gaji_akhir_3}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Alasan Berhenti
							</label>
							<div class="col-md-8">
								<input type="text" name="data[alasan_berhenti_3]" data-required="1" class="form-control" value="{{data.alasan_berhenti_3}}" />
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="form-group row">
							<label class="control-label col-md-4">Nama PT
							</label>
							<div class="col-md-8">
								<input type="text" name="data[nama_pt_4]" data-required="1" class="form-control" value="{{data.nama_pt_4}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Jenis Usaha 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[jenis_usaha_4]" data-required="1" class="form-control" value="{{data.jenis_usaha_4}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Alamat 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[alamat_pt_4]" data-required="1" class="form-control" value="{{data.alamat_pt_4}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Gaji Awal 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[gaji_awal_4]" data-required="1" class="form-control" value="{{data.gaji_awal_4}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Gaji Akhir 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[gaji_akhir_4]" data-required="1" class="form-control" value="{{data.gaji_akhir_4}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Tugas Utama 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[tugas_utama_4]" data-required="1" class="form-control" value="{{data.gaji_akhir_4}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Alasan Berhenti
							</label>
							<div class="col-md-8">
								<input type="text" name="data[alasan_berhenti_3]" data-required="1" class="form-control" value="{{data.alasan_berhenti_4}}" />
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="portlet light bordered">
						<div class="form-group row">
							<label class="control-label col-md-4">Nama PT
							</label>
							<div class="col-md-8">
								<input type="text" name="data[nama_pt_5]" data-required="1" class="form-control" value="{{data.nama_pt_5}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Jenis Usaha 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[jenis_usaha_5]" data-required="1" class="form-control" value="{{data.jenis_usaha_5}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Alamat 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[alamat_pt_5]" data-required="1" class="form-control" value="{{data.alamat_pt_5}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Gaji Awal 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[gaji_awal_5]" data-required="1" class="form-control" value="{{data.gaji_awal_5}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Gaji Akhir 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[gaji_akhir_5]" data-required="1" class="form-control" value="{{data.gaji_akhir_5}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Tugas Utama 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[tugas_utama_5]" data-required="1" class="form-control" value="{{data.gaji_akhir_5}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Alasan Berhenti
							</label>
							<div class="col-md-8">
								<input type="text" name="data[alasan_berhenti_5]" data-required="1" class="form-control" value="{{data.alasan_berhenti_5}}" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="tab-pane fade" id="form-data-tanggungan" style="position: relative; top: 0px">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-6 ">
						<div class="form-group row">
							<label class="control-label col-md-4">Status Nikah
							</label>
							<div class="col-md-8">
								<input type="text" name="data[status_nikah]" data-required="1" class="form-control" value="{{data.status_nikah}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Tanggal Nikah 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[tgl_nikah]" value="{{data.tgl_nikah}}" data-required="1" class="form-control date-picker" data-date-format="yyyy-mm-dd" />
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="col-md-12 portlet light bordered">
						<div class="form-group row">
							<label class="control-label col-md-4">Nama Tanggungan
							</label>
							<div class="col-md-8">
								<input type="text" name="data[nama_tanggungan_1]" data-required="1" class="form-control" value="{{data.nama_tanggungan_1}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Hubungan 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[hubungan_1]" data-required="1" class="form-control" value="{{data.hubungan_1}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Alamat 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[alamat_tanggungan_1]" data-required="1" class="form-control" value="{{data.alamat_tanggungan_1}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Tanggal Lahir 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[tgl_lahir_tanggungan_1]" data-required="1" class="form-control date-picker" value="{{data.tgl_lahir_tanggungan_1}}" data-date-format="yyyy-mm-dd" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Pendidikan 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[pendidikan_tanggungan_1]" data-required="1" class="form-control" value="{{data.pendidikan_tanggungan_1}}" />
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="col-md-12 portlet light bordered">
						<div class="form-group row">
							<label class="control-label col-md-4">Nama Tanggungan
							</label>
							<div class="col-md-8">
								<input type="text" name="data[nama_tanggungan_2]" data-required="1" class="form-control" value="{{data.nama_tanggungan_2}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Hubungan 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[hubungan_2]" data-required="1" class="form-control" value="{{data.hubungan_2}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Alamat 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[alamat_tanggungan_2]" data-required="1" class="form-control" value="{{data.alamat_tanggungan_2}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Tanggal Lahir 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[tgl_lahir_tanggungan_2]" data-required="1" class="form-control date-picker" value="{{data.tgl_lahir_tanggungan_2}}" data-date-format="yyyy-mm-dd" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Pendidikan 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[pendidikan_tanggungan_2]" data-required="1" class="form-control" value="{{data.pendidikan_tanggungan_2}}" />
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="col-md-12 portlet light bordered">
						<div class="form-group row">
							<label class="control-label col-md-4">Nama Tanggungan
							</label>
							<div class="col-md-8">
								<input type="text" name="data[nama_tanggungan_3]" data-required="1" class="form-control" value="{{data.nama_tanggungan_3}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Hubungan 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[hubungan_3]" data-required="1" class="form-control" value="{{data.hubungan_3}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Alamat 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[alamat_tanggungan_3]" data-required="1" class="form-control" value="{{data.alamat_tanggungan_3}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Tanggal Lahir 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[tgl_lahir_tanggungan_3]" data-required="1" class="form-control date-picker" value="{{data.tgl_lahir_tanggungan_3}}" data-date-format="yyyy-mm-dd" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Pendidikan 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[pendidikan_tanggungan_3]" data-required="1" class="form-control" value="{{data.pendidikan_tanggungan_3}}" />
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="col-md-12 portlet light bordered">
						<div class="form-group row">
							<label class="control-label col-md-4">Nama Tanggungan
							</label>
							<div class="col-md-8">
								<input type="text" name="data[nama_tanggungan_4]" data-required="1" class="form-control" value="{{data.nama_tanggungan_4}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Hubungan 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[hubungan_4]" data-required="1" class="form-control" value="{{data.hubungan_4}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Alamat 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[alamat_tanggungan_4]" data-required="1" class="form-control" value="{{data.alamat_tanggungan_4}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Tanggal Lahir 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[tgl_lahir_tanggungan_4]" data-required="1" class="form-control date-picker" value="{{data.tgl_lahir_tanggungan_4}}" data-date-format="yyyy-mm-dd" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Pendidikan 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[pendidikan_tanggungan_4]" data-required="1" class="form-control" value="{{data.pendidikan_tanggungan_4}}" />
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="col-md-12 portlet light bordered">
						<div class="form-group row">
							<label class="control-label col-md-4">Nama Tanggungan
							</label>
							<div class="col-md-8">
								<input type="text" name="data[nama_tanggungan_5]" data-required="1" class="form-control" value="{{data.nama_tanggungan_5}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Hubungan 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[hubungan_5]" data-required="1" class="form-control" value="{{data.hubungan_5}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Alamat 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[alamat_tanggungan_5]" data-required="1" class="form-control" value="{{data.alamat_tanggungan_5}}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Tanggal Lahir 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[tgl_lahir_tanggungan_5]" data-required="1" class="form-control date-picker" value="{{data.tgl_lahir_tanggungan_5}}" data-date-format="yyyy-mm-dd" />
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-4">Pendidikan 
							</label>
							<div class="col-md-8">
								<input type="text" name="data[pendidikan_tanggungan_5]" data-required="1" class="form-control" value="{{data.pendidikan_tanggungan_5}}" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="tab-pane fade" id="form-susunan-keluarga" style="position: relative; top: 0px">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-1 ">
						<div class="form-group">
							<label class="control-label">No</label>
						</div>
					</div>
					<div class="col-md-3 ">
						<div class="form-group">
							<label class="control-label">Nama</label>
						</div>
					</div>
					<div class="col-md-2 ">
						<div class="form-group">
							<label class="control-label">L/P</label>
						</div>
					</div>
					<div class="col-md-2 ">
						<div class="form-group">
							<label class="control-label">Tgl Lahir</label>
						</div>
					</div>
					<div class="col-md-2 ">
						<div class="form-group">
							<label class="control-label">Hubungan</label>
						</div>
					</div>
					<div class="col-md-2 ">
						<div class="form-group">
							<label class="control-label">Pekerjaan</label>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-1 ">
						<div class="form-group">
							<label class="control-label">1</label>
						</div>
					</div>
					<div class="col-md-3 ">
						<div class="form-group">
							<input type="text" name="data[nama_kk_1]" data-required="1" class="form-control" value="{{data.nama_kk_1}}" />
						</div>
					</div>
					<div class="col-md-2 ">
						<div class="form-group">
							<input type="text" name="data[jk_kk_1]" data-required="1" class="form-control" value="{{data.jk_kk_1}}" />
						</div>
					</div>
					<div class="col-md-2 ">
						<div class="form-group">
							<input type="text" name="data[tgl_lahir_kk_1]" data-required="1" class="form-control date-picker" value="{{data.tgl_lahir_kk_1}}" data-date-format="yyyy-mm-dd"/>
						</div>
					</div>
					<div class="col-md-2 ">
						<div class="form-group">
							<input type="text" name="data[hubungan_kk_1]" data-required="1" class="form-control" value="{{data.hubungan_kk_1}}" />
						</div>
					</div>
					<div class="col-md-2 ">
						<div class="form-group">
							<input type="text" name="data[pekerjaan_kk_1]" data-required="1" class="form-control" value="{{data.pekerjaan_kk_1}}" />
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-1 ">
						<div class="form-group">
							<label class="control-label">2</label>
						</div>
					</div>
					<div class="col-md-3 ">
						<div class="form-group">
							<input type="text" name="data[nama_kk_2]" data-required="1" class="form-control" value="{{data.nama_kk_2}}" />
						</div>
					</div>
					<div class="col-md-2 ">
						<div class="form-group">
							<input type="text" name="data[jk_kk_2]" data-required="1" class="form-control" value="{{data.jk_kk_2}}" />
						</div>
					</div>
					<div class="col-md-2 ">
						<div class="form-group">
							<input type="text" name="data[tgl_lahir_kk_2]" data-required="1" class="form-control date-picker" value="{{data.tgl_lahir_kk_2}}" data-date-format="yyyy-mm-dd"/>
						</div>
					</div>
					<div class="col-md-2 ">
						<div class="form-group">
							<input type="text" name="data[hubungan_kk_2]" data-required="1" class="form-control" value="{{data.hubungan_kk_2}}" />
						</div>
					</div>
					<div class="col-md-2 ">
						<div class="form-group">
							<input type="text" name="data[pekerjaan_kk_2]" data-required="1" class="form-control" value="{{data.pekerjaan_kk_2}}" />
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-1 ">
						<div class="form-group">
							<label class="control-label">3</label>
						</div>
					</div>
					<div class="col-md-3 ">
						<div class="form-group">
							<input type="text" name="data[nama_kk_3]" data-required="1" class="form-control" value="{{data.nama_kk_3}}" />
						</div>
					</div>
					<div class="col-md-2 ">
						<div class="form-group">
							<input type="text" name="data[jk_kk_3]" data-required="1" class="form-control" value="{{data.jk_kk_3}}" />
						</div>
					</div>
					<div class="col-md-2 ">
						<div class="form-group">
							<input type="text" name="data[tgl_lahir_kk_3]" data-required="1" class="form-control date-picker" value="{{data.tgl_lahir_kk_3}}" data-date-format="yyyy-mm-dd"/>
						</div>
					</div>
					<div class="col-md-2 ">
						<div class="form-group">
							<input type="text" name="data[hubungan_kk_3]" data-required="1" class="form-control" value="{{data.hubungan_kk_3}}" />
						</div>
					</div>
					<div class="col-md-2 ">
						<div class="form-group">
							<input type="text" name="data[pekerjaan_kk_3]" data-required="1" class="form-control" value="{{data.pekerjaan_kk_3}}" />
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-1 ">
						<div class="form-group">
							<label class="control-label">4</label>
						</div>
					</div>
					<div class="col-md-3 ">
						<div class="form-group">
							<input type="text" name="data[nama_kk_4]" data-required="1" class="form-control" value="{{data.nama_kk_4}}" />
						</div>
					</div>
					<div class="col-md-2 ">
						<div class="form-group">
							<input type="text" name="data[jk_kk_4]" data-required="1" class="form-control" value="{{data.jk_kk_4}}" />
						</div>
					</div>
					<div class="col-md-2 ">
						<div class="form-group">
							<input type="text" name="data[tgl_lahir_kk_4]" data-required="1" class="form-control date-picker" value="{{data.tgl_lahir_kk_4}}" data-date-format="yyyy-mm-dd"/>
						</div>
					</div>
					<div class="col-md-2 ">
						<div class="form-group">
							<input type="text" name="data[hubungan_kk_4]" data-required="1" class="form-control" value="{{data.hubungan_kk_4}}" />
						</div>
					</div>
					<div class="col-md-2 ">
						<div class="form-group">
							<input type="text" name="data[pekerjaan_kk_4]" data-required="1" class="form-control" value="{{data.pekerjaan_kk_4}}" />
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-1 ">
						<div class="form-group">
							<label class="control-label">5</label>
						</div>
					</div>
					<div class="col-md-3 ">
						<div class="form-group">
							<input type="text" name="data[nama_kk_5]" data-required="1" class="form-control" value="{{data.nama_kk_5}}" />
						</div>
					</div>
					<div class="col-md-2 ">
						<div class="form-group">
							<input type="text" name="data[jk_kk_5]" data-required="1" class="form-control" value="{{data.jk_kk_5}}" />
						</div>
					</div>
					<div class="col-md-2 ">
						<div class="form-group">
							<input type="text" name="data[tgl_lahir_kk_5]" data-required="1" class="form-control date-picker" value="{{data.tgl_lahir_kk_5}}" data-date-format="yyyy-mm-dd"/>
						</div>
					</div>
					<div class="col-md-2 ">
						<div class="form-group">
							<input type="text" name="data[hubungan_kk_5]" data-required="1" class="form-control" value="{{data.hubungan_kk_5}}" />
						</div>
					</div>
					<div class="col-md-2 ">
						<div class="form-group">
							<input type="text" name="data[pekerjaan_kk_5]" data-required="1" class="form-control" value="{{data.pekerjaan_kk_5}}" />
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-1 ">
						<div class="form-group">
							<label class="control-label">6</label>
						</div>
					</div>
					<div class="col-md-3 ">
						<div class="form-group">
							<input type="text" name="data[nama_kk_6]" data-required="1" class="form-control" value="{{data.nama_kk_6}}" />
						</div>
					</div>
					<div class="col-md-2 ">
						<div class="form-group">
							<input type="text" name="data[jk_kk_6]" data-required="1" class="form-control" value="{{data.jk_kk_6}}" />
						</div>
					</div>
					<div class="col-md-2 ">
						<div class="form-group">
							<input type="text" name="data[tgl_lahir_kk_6]" data-required="1" class="form-control date-picker" value="{{data.tgl_lahir_kk_6}}" data-date-format="yyyy-mm-dd"/>
						</div>
					</div>
					<div class="col-md-2 ">
						<div class="form-group">
							<input type="text" name="data[hubungan_kk_6]" data-required="1" class="form-control" value="{{data.hubungan_kk_6}}" />
						</div>
					</div>
					<div class="col-md-2 ">
						<div class="form-group">
							<input type="text" name="data[pekerjaan_kk_6]" data-required="1" class="form-control" value="{{data.pekerjaan_kk_6}}" />
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-1 ">
						<div class="form-group">
							<label class="control-label">7</label>
						</div>
					</div>
					<div class="col-md-3 ">
						<div class="form-group">
							<input type="text" name="data[nama_kk_7]" data-required="1" class="form-control" value="{{data.nama_kk_7}}" />
						</div>
					</div>
					<div class="col-md-2 ">
						<div class="form-group">
							<input type="text" name="data[jk_kk_7]" data-required="1" class="form-control" value="{{data.jk_kk_7}}" />
						</div>
					</div>
					<div class="col-md-2 ">
						<div class="form-group">
							<input type="text" name="data[tgl_lahir_kk_7]" data-required="1" class="form-control date-picker" value="{{data.tgl_lahir_kk_7}}" data-date-format="yyyy-mm-dd"/>
						</div>
					</div>
					<div class="col-md-2 ">
						<div class="form-group">
							<input type="text" name="data[hubungan_kk_7]" data-required="1" class="form-control" value="{{data.hubungan_kk_7}}" />
						</div>
					</div>
					<div class="col-md-2 ">
						<div class="form-group">
							<input type="text" name="data[pekerjaan_kk_7]" data-required="1" class="form-control" value="{{data.pekerjaan_kk_7}}" />
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-1 ">
						<div class="form-group">
							<label class="control-label">8</label>
						</div>
					</div>
					<div class="col-md-3 ">
						<div class="form-group">
							<input type="text" name="data[nama_kk_8]" data-required="1" class="form-control" value="{{data.nama_kk_8}}" />
						</div>
					</div>
					<div class="col-md-2 ">
						<div class="form-group">
							<input type="text" name="data[jk_kk_8]" data-required="1" class="form-control" value="{{data.jk_kk_8}}" />
						</div>
					</div>
					<div class="col-md-2 ">
						<div class="form-group">
							<input type="text" name="data[tgl_lahir_kk_8]" data-required="1" class="form-control date-picker" value="{{data.tgl_lahir_kk_8}}" data-date-format="yyyy-mm-dd"/>
						</div>
					</div>
					<div class="col-md-2 ">
						<div class="form-group">
							<input type="text" name="data[hubungan_kk_8]" data-required="1" class="form-control" value="{{data.hubungan_kk_8}}" />
						</div>
					</div>
					<div class="col-md-2 ">
						<div class="form-group">
							<input type="text" name="data[pekerjaan_kk_8]" data-required="1" class="form-control" value="{{data.pekerjaan_kk_8}}" />
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="tab-pane fade" id="form-kontak-darurat" style="position: relative; top: 0px">
			<div class="row">
				<div class="col-md-8">
					<div class="form-group row">
						<label class="control-label col-md-4">Nama  </label>
						<div class="col-md-8">
							<input type="text" name="data[nama_kontak_darurat]" value="{{data.nama_kontak_darurat}}" data-required="1" class="form-control" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Hubungan </label>
						<div class="col-md-8">
							<input type="text" name="data[hubungan_kontak_darurat]" value="{{data.hubungan_kontak_darurat}}" data-required="1" class="form-control" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Alamat </label>
						<div class="col-md-8">
							<input type="text" name="data[alamat_kontak_darurat]" value="{{data.alamat_kontak_darurat}}" data-required="1" class="form-control" />
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-4">Telepon </label>
						<div class="col-md-8">
							<input type="text" name="data[telp_kontak_darurat]" value="{{data.telp_kontak_darurat}}" data-required="1" class="form-control" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</form>
</div>

<script>
	jQuery(document).ready(function(){ 
		if ($().select2) {
            $('.select2me').select2({
                placeholder: "Select",
                allowClear: true
            });
        }

        if ($().datepicker) {
            $('.date-picker').datepicker({
                rtl: Metronic.isRTL(),
                orientation: "left",
                autoclose: true
            });
            //$('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
        }
	});
</script>
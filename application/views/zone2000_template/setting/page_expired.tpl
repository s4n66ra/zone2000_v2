<div class="row">
	<div class="col-md-12">
		<div class="col-md-6">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<i class="icon-puzzle font-red-flamingo"></i>
						<span class="caption-subject bold font-red-flamingo uppercase">
						PURCHASE REQUEST
						</span>
						<span class="caption-helper"></span>
					</div>
					<div class="tools">
						<a href="" class="collapse">
						</a>
					</div>
				</div>
				<div class="portlet-body">
					{{ pr.form |raw }}
				</div>
			</div>
		</div>

		<div class="col-md-6">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<i class="icon-puzzle font-red-flamingo"></i>
						<span class="caption-subject bold font-red-flamingo uppercase">
						DELIVERY LEAD TIME
						</span>
						<span class="caption-helper"></span>
					</div>
					<div class="tools">
						<a href="" class="collapse">
						</a>
					</div>
				</div>
				<div class="portlet-body">
					{{ po.form |raw }}
				</div>
			</div>
		</div>
	</div>

	<div class="col-md-12">
		<div class="col-md-6">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<i class="icon-puzzle font-red-flamingo"></i>
						<span class="caption-subject bold font-red-flamingo uppercase">
						SERVICE REQUEST
						</span>
						<span class="caption-helper"></span>
					</div>
					<div class="tools">
						<a href="" class="collapse">
						</a>
					</div>
				</div>
				<div class="portlet-body">
					{{ service.form |raw }}
				</div>
			</div>
		</div>

		<div class="col-md-6">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<i class="icon-puzzle font-red-flamingo"></i>
						<span class="caption-subject bold font-red-flamingo uppercase">
						REPAIR JOB
						</span>
						<span class="caption-helper"></span>
					</div>
					<div class="tools">
						<a href="" class="collapse">
						</a>
					</div>
				</div>
				<div class="portlet-body">
					{{ repair.form |raw }}
				</div>
			</div>
		</div>
	</div>

</div>
{{ form_action | raw }}	
	<div class="form-group row">
		<label class="control-label col-md-4"> Draft
		
		</label>
		<div class="col-md-8">
			<div class="input-group margin-top-10">
				<input type="text" name="data[0]" data-required="1" class="form-control format-number" value="{{data[0]}}" {{readonly}}/>
				<span class="input-group-addon">
					days
				</span>
			</div>
			
		</div>
	</div>

	<div class="form-group row">
		<label class="control-label col-md-4"> Waiting For Review
		
		</label>
		<div class="col-md-8">
			<div class="input-group margin-top-10">
				<input type="text" name="data[1]" data-required="1" class="form-control format-number" value="{{data[1]}}" {{readonly}}/>
				<span class="input-group-addon">
					days
				</span>
			</div>
			
		</div>
	</div>

	<div class="form-group row">
		<label class="control-label col-md-4"> Waiting For Confirm
		
		</label>
		<div class="col-md-8">
			<div class="input-group margin-top-10">
				<input type="text" name="data[2]" data-required="1" class="form-control format-number" value="{{data[2]}}" {{readonly}}/>
				<span class="input-group-addon">
					days
				</span>
			</div>
			
		</div>
	</div>

	<div class="form-group row">
		<label class="control-label col-md-4"> Revision
		
		</label>
		<div class="col-md-8">
			<div class="input-group margin-top-10">
				<input type="text" name="data[3]" data-required="1" class="form-control format-number" value="{{data[3]}}" {{readonly}}/>
				<span class="input-group-addon">
					days
				</span>
			</div>
			
		</div>
	</div>

</form>
{{ form_action | raw }}	
	<div class="form-group row">
		<label class="control-label col-md-4"> Machine
		
		</label>
		<div class="col-md-8">
			<div class="input-group margin-top-10">
				<input type="text" name="data[7]" data-required="1" class="form-control format-number" value="{{data[7]}}" {{readonly}}/>
				<span class="input-group-addon">
					days
				</span>
			</div>
			
		</div>
	</div>

	<div class="form-group row">
		<label class="control-label col-md-4"> Merchandise
		
		</label>
		<div class="col-md-8">
			<div class="input-group margin-top-10">
				<input type="text" name="data[2]" data-required="1" class="form-control format-number" value="{{data[2]}}" {{readonly}}/>
				<span class="input-group-addon">
					days
				</span>
			</div>
			
		</div>
	</div>

	<div class="form-group row">
		<label class="control-label col-md-4"> Sparepart
		
		</label>
		<div class="col-md-8">
			<div class="input-group margin-top-10">
				<input type="text" name="data[3]" data-required="1" class="form-control format-number" value="{{data[3]}}" {{readonly}}/>
				<span class="input-group-addon">
					days
				</span>
			</div>
			
		</div>
	</div>

</form>
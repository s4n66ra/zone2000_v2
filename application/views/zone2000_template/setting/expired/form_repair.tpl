{{ form_action | raw }}	
	<div class="form-body">

	{% for key, detail in data %}
		<div class="form-group row">
			<label class="control-label col-md-4"> {{detail.status_name}}</label>
			<div class="col-md-8">
				<div class="input-group">
					<input type="text" name="data[{{detail.statusrepair_id}}]" data-required="1" class="form-control format-number" value="{{detail.expired}}" {{readonly}}/>
					<span class="input-group-addon">
						days
					</span>
				</div>
				
			</div>
		</div>

	{% endfor %}
	</div>
</form>
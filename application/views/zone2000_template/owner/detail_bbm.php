
<div class="inner_content">

    <div class="widgets_area">
        <div class="row-fluid">
            <div class="span6">
                <div id="index-content" class="well-content blue">
                    <div class="well">
                        <div class="pull-left">
                            <?php echo hgenerator::render_button_group($button_group); ?>
                        </div>
                        <div class="pull-right">
                            <?php
                            if ($this->access_right->otoritas('print')) 
                                echo hgenerator::render_button_group($print_group);
                            ?>
                        </div>
                    </div>
                    <?php echo form_open_multipart('', array('id' => 'ffilter_detail_'.$id_bbm,'tools-filter'=>FALSE)); ?>
                    <?php echo form_hidden('id_bbm',$id_bbm);?>
                    <?php echo form_close(); ?>
                    <div id="content_table_detail_<?php echo $id_bbm;?>" data-source="<?php echo $data_source; ?>" data-filter="#ffilter_detail_<?php echo $id_bbm;?>"></div>
                </div>
                
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        load_table('#content_table_detail_<?php echo $id_bbm;?>', 1);
        $('.onchange').change(function(){
            load_table('#content_table_detail_<?php echo $id_bbm;?>', 1);
        });
    });

    function refresh_filter(){ 
        load_table('#content_table_detail_<?php echo $id_bbm;?>', 1,function(){

        });
    }

</script>


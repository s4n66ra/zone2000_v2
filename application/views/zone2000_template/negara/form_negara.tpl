{{ form_action|raw }}
	<div class="form-body">
		<h3 class="form-section"></h3>
		<div class="alert alert-danger display-hide">
			<button class="close" data-close="alert"></button>
			You have some form errors. Please check below.
		</div>
		<div class="alert alert-success display-hide">
			<button class="close" data-close="alert"></button>
			Your form validation is successful!
		</div>
		<div class="form-group row">
			<label class="control-label col-md-3">Country Name<span class="required">
			* </span>
			</label>
			<div class="col-md-8">
				<input type="text" name="nama_negara" data-required="1" class="form-control" value="{{data.nama_negara}}"/>
			</div>
		</div>
	</div>
</form>
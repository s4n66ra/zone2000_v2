<div class="tabbable-custom show-overflow">
	<ul class="nav nav-tabs ">
		<li class="active">
			<a href="#cutter" data-toggle="tab">
			HISTORY TICKET CUTTER </a>
		</li>
		<!-- <li>
			<a href="#coin-count" data-toggle="tab">
			COIN COUNT</a>
		</li> -->
		<li>
			<a href="#ticket-meter" data-toggle="tab">
			HISTORY TICKET METER</a>
		</li>
		<li>
			<a href="#coin-out" data-toggle="tab">
			HISTORY KURAS KOIN</a>
		</li>
		<li>
			<a href="#ticket-refill" data-toggle="tab">
			HISTORY TICKET REFILL</a>
		</li>
		<li>
			<a href="#mc-refill" data-toggle="tab">
			HISTORY VENDING</a>
		</li>
		<li>
			<a href="#mc-redemp" data-toggle="tab">
			HISTORY REDEMP</a>
		</li>
	</ul>

	<div class="tab-content">
		<div class="tab-pane active" id="cutter">
			{{ page.cutter | raw }}
		</div>

		<div class="tab-pane" id="coin-count">
			{{ page.coin_meter | raw }}
		</div>

		<div class="tab-pane" id="ticket-meter">
			{{ page.ticket_meter | raw }}
		</div>

		<div class="tab-pane" id="coin-out">
			{{ page.coin_kuras | raw }}
		</div>

		<div class="tab-pane" id="ticket-refill">
			{{ page.ticket_refill | raw }}
		</div>

		<div class="tab-pane" id="mc-refill">
			{{ page.mc_refill | raw }}
		</div>

		<div class="tab-pane" id="mc-redemp">
			{{ page.mc_redemp | raw }}
		</div>

	</div>

</div>


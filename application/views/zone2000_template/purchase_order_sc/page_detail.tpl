<style type="text/css">
	.grand-total {
		color :rgb(231, 0, 0);
		padding-right: : 20px;
		font-size: 26px;
		line-height: 28px;
		text-align: right;
  		font-weight: bolder;
	}
</style>

<div class="portlet light">
	<div class="portlet-title">
		{{ btn_page_back()|raw }}
		<div class="caption">
			<i class="glyphicon glyphicon-list-alt font-blue-madison"></i>
			<span class="caption-subject bold font-blue-madison uppercase">
			PURCHASE ORDER DETAIL </span>
			<span class="caption-helper"> View</span>
		</div>
		{% if st_bundle==0 %}
		<div class="actions">


<!-- 			<a href="#" class="btn btn-circle yellow-saffron btn-supplier-save" url-action="{{url.generate_po}}"><i class="fa fa-check"></i> Generate Purchase Order </a>
 -->			
			<a href="" class="collapse">
			<a href="#" class="btn btn-circle btn-default btn-icon-only fullscreen" data-original-title="" title=""></a>
		</div>
		{% endif %}
	</div>
	<div class="portlet-body">

		<div class="tabbable-custom show-overflow">
			<ul class="nav nav-tabs">
				<li class="active">
					<a href="#tab_po_detail_1" data-toggle="tab">
					CURRENT</a>
				</li>


				<li>
					<a href="#tab_po_detail_2" data-toggle="tab">
					HISTORY</a>
				</li>
			</ul>

			<div class="tab-content">
				<div class="tab-pane active" id="tab_po_detail_1">

					{% if st_bundle==0 %}
						<div class="save-space">
							<a href="#" class="btn btn-circle green-jungle btn-detail-save" url-action="{{url.action_save}}" style="float:right;margin-right:20px;margin-bottom:10px;"><i class="fa fa-save"></i> Save </a>
						</div>
					{% endif %}

					<div class="row">
						<div class="col-md-6">
							<input type="hidden" id="transactionbundle-id" value="{{data.transactionbundle_id}}" />
						</div>
					</div>

					<div class="row" style="padding-right: 20px;">
						<div class="col-md-8">
						</div>
						<div class="col-md-4 format-number grand-total">
							0
						</div>
					</div>

					{% for key, po in list.postore %}
					<div class="row">
						<div class="col-md-12">
							
							<div class="portlet light">
								<div class="portlet-title">
									<div class="caption">
										<i class="icon-puzzle font-grey-gallery"></i>
										<span class="caption-subject bold font-grey-gallery uppercase">
										{{ po.name }}  </span>
										<span class="caption-helper"></span>
									</div>
									<div class="tools">
										<a href="" class="collapse">
										</a>
									</div>
								</div>
								<div class="portlet-body form-body">
									<div class="form-group row">
										<label class="control-label col-md-2">No. PO Sementara </label>
										<div class="col-md-4">
											<input type="text" name="temp[po_code]" data-required="1" po-id="{{ po.po_id }}" class="form-control po-code" value="{{ po.po_code }}" disabled="" />
										</div>
									</div>
									{{ po.table_item | raw}}
								 	{{ po.table | raw}}

								</div>
							</div>
						</div>
					</div>
					{% endfor %}
				</div>

				<div class="tab-pane" id="tab_po_detail_2">
					{{ page.history |raw }}
				</div>

			</div>

		</div>

	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){

	});
</script>

<div class="portlet light">
	<div class="portlet-title">
		{{ btn_page_back() | raw }}
		<div class="caption">
			<i class="glyphicon glyphicon-list-alt font-blue-madison"></i>
			<span class="caption-subject bold font-blue-madison uppercase">
			DRAFT PURCHASE ORDER </span>
			<span class="caption-helper">Choose Supplier</span>
		</div>
		<div class="actions">

			<a href="#" class="btn btn-circle green-turquoise btn-supplier-save" url-action="{{url.action_save}}"><i class="fa fa-check"></i> Process Purchase Order </a>
			
			<a href="" class="collapse">
			<a href="#" class="btn btn-circle btn-default btn-icon-only fullscreen" data-original-title="" title=""></a>
		</div>
		<!-- <div class="tools">
			<a href="" class="collapse">
			</a>
			</a>
			<a href="javascript:;" class="fullscreen">
			</a>
		</div> -->
	</div>
	<div class="portlet-body">
		<div class="row">
				<!-- <div class="col-md-6">
					<div class="btn-group">
						<a class="btn btn-default btn-page-back"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
						<a class="btn green-jungle btn-draft-save" url-action="{{url.action_save}}"><i class="fa fa-save"></i> Save</a>
					</div>
				</div> -->
			<div class="col-md-6">

				<input type="hidden" id="po-id" value="{{data.po_id}}" />
				
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				
				<div class="portlet light">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-puzzle font-red-flamingo"></i>
							<span class="caption-subject bold font-red-flamingo uppercase">
								List Item
							</span>
							<span class="caption-helper"></span>
						</div>
						<div class="tools">
							<a href="" class="collapse">
							</a>
						</div>
					</div>
					<div class="portlet-body">
						
						<div class="table-scrollable">
							 {{ table.list | raw}}
						</div>

					</div>
				</div>
			</div>
		</div>

	</div>
</div>

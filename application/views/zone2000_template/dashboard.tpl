<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.2
Version: 3.6.2
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<style type="text/css">
.capitalize {
  text-transform: capitalize;
}

.select2-no-results {
    display: none !important;
}

</style>
<meta charset="utf-8"/>
<title>Zone 2000</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>

<script src="{{assets_url}}global/plugins/pace/pace.min.js" type="text/javascript"></script>
<link href="{{assets_url}}global/plugins/pace/themes/pace-theme-flash.css" rel="stylesheet" type="text/css"/>

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="{{assets_url}}css/font-open-sans.css" rel="stylesheet" type="text/css">
<link href="{{assets_url}}global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="{{assets_url}}global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
<link href="{{assets_url}}global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="{{assets_url}}global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
<link href="{{assets_url}}global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- DATEPICKER -->
<link rel="stylesheet" type="text/css" href="{{assets_url}}global/plugins/clockface/css/clockface.css"/>
<link rel="stylesheet" type="text/css" href="{{assets_url}}global/plugins/bootstrap-datepicker/css/datepicker3.css"/>
<link rel="stylesheet" type="text/css" href="{{assets_url}}global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="{{assets_url}}global/plugins/bootstrap-colorpicker/css/colorpicker.css"/>
<link rel="stylesheet" type="text/css" href="{{assets_url}}global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<link rel="stylesheet" type="text/css" href="{{assets_url}}global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"/>

<!-- TOASTR -->
<link rel="stylesheet" type="text/css" href="{{assets_url}}global/plugins/bootstrap-toastr/toastr.min.css"/>

<!-- BEGIN PAGE MODAL STYLES -->
<link href="{{assets_url}}global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
<link href="{{assets_url}}global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>

<!-- BEGIN SELECT2 / COMBOBOX SEARCH PLUGINS -->
<link rel="stylesheet" type="text/css" href="{{assets_url}}global/plugins/bootstrap-select/bootstrap-select.min.css"/>
<link rel="stylesheet" type="text/css" href="{{assets_url}}global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="{{assets_url}}global/plugins/jquery-multi-select/css/multi-select.css"/>

<!-- BEGIN THEME STYLES -->
<link href="{{assets_url}}global/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css"/>
<link href="{{assets_url}}global/css/plugins.css" rel="stylesheet" type="text/css"/>

<link href="{{assets_url}}admin/layout3/css/layout.css" rel="stylesheet" type="text/css"/>
<!-- <link id="style_color" href="{{assets_url}}admin/layout4/css/themes/light.css" rel="stylesheet" type="text/css"/> -->
<link href="{{assets_url}}admin/layout3/css/custom.css" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->

<link rel="stylesheet" type="text/css" href="{{assets_url}}global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>

<!-- TAGS INPUT -->
<link rel="stylesheet" type="text/css" href="{{assets_url}}global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css"/>
<link rel="stylesheet" type="text/css" href="{{assets_url}}global/plugins/jquery-tags-input/jquery.tagsinput.css"/>
<link rel="stylesheet" type="text/css" href="{{assets_url}}global/plugins/typeahead/typeahead.css">

<!-- EDITABLE -->
<link rel="stylesheet" type="text/css" href="{{assets_url}}global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css"/>
<link rel="stylesheet" type="text/css" href="{{assets_url}}global/plugins/bootstrap-editable/inputs-ext/address/address.css"/>

<!-- CUSTOM CSS -->
<link href="{{assets_url}}css/custom.css" rel="stylesheet" type="text/css"/>
<!-- ROFID CUSTOM COMPONENT CSS -->
<link href="{{assets_url}}css/component/datatable.css" rel="stylesheet" type="text/css"/>

<script src="{{assets_url}}global/plugins/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript" src="{{assets_url}}global/plugins/datatables/plugins/bootstrap/dataTables.reordering.js"></script>

<link rel="shortcut icon" href="{{assets_url}}favicon/favicon.ico"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
<!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
<!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
<!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
<!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
<!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
<!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
<body class="page-header-fixed page-sidebar-closed-hide-logo ">
<!-- BEGIN HEADER -->

{% include 'topbar.tpl' %}

<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<div class="page-content-wrapper">
		<div class="page-content" id="page-content" data-source="{{data_source_page}}">
			{{ breadcumb |raw }}

			<div class="row">
				<div class="col-md-12 page" page="1" id="page-1" data-source="{{data_source_page_1}}">
					{% include dashboard_filter %}
				</div>
			</div>

			<div class="row">
				<div class="col-md-12 page" page="1" id="page-1" data-source="{{data_source_page_1}}">
					{% include content %}
				</div>
			</div>

			<div class="row">
				<div class="col-md-12 page" page="2" id="page-2" data-source="{{data_source_page_2}}" style="display:none;">
				</div>
			</div>

			<div class="row">
				<div class="col-md-12 page" page="3" id="page-3" data-source="{{data_source_page_3}}" style="display:none;">
				</div>
			</div>

		</div>
	</div>

	<div class="modal fade" id="form-content" tabindex="-1" aria-hidden="true"></div>
	<div class="modal fade" id="form-content-2" tabindex="-1" aria-hidden="true"></div>
	<div class="modal fade" id="modal-loading" tabindex="-1" aria-hidden="true"></div>
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
	<div class=" text-center font-white">
		 2015 &copy; Mimotech.
	</div>
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="{{assets_url}}global/plugins/respond.min.js"></script>
<script src="{{assets_url}}global/plugins/excanvas.min.js"></script> 
<![endif]-->

<!-- GLOBAL VARIABLES FOR ALL JS -->
<script type="text/javascript">
	{% for variable, value in javascript_var %}
	window.{{variable}} = '{{value}}';
	{% endfor %}
	window.base_url = '{{base_url()}}';
</script>

<script src="{{assets_url}}js/lib/stickUp.js"></script>
<script src="{{assets_url}}global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="{{assets_url}}global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="{{assets_url}}global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="{{assets_url}}global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="{{assets_url}}global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="{{assets_url}}global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="{{assets_url}}global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="{{assets_url}}global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="{{assets_url}}global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script type="text/javascript" src="{{assets_url}}global/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="{{assets_url}}global/plugins/jquery-validation/js/additional-methods.min.js"></script>
<script type="text/javascript" src="{{assets_url}}global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<!-- <script type="text/javascript" src="{{assets_url}}global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="{{assets_url}}global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="{{assets_url}}global/plugins/ckeditor/ckeditor.js"></script> -->
<script type="text/javascript" src="{{assets_url}}global/plugins/bootstrap-markdown/js/bootstrap-markdown.js"></script>
<script type="text/javascript" src="{{assets_url}}global/plugins/bootstrap-markdown/lib/markdown.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- BEGIN MODAL PLUGINS -->
<script src="{{assets_url}}global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="{{assets_url}}global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<!-- END MODAL PLUGINS -->

<!-- BEGIN JQUERY FORM STYLES -->
<script src="{{assets_url}}global/plugins/jquery.form.js" type="text/javascript"></script>

<!-- BEGIN BOOTBOX STYLES -->
<script src="{{assets_url}}global/plugins/bootbox/bootbox.min.js" type="text/javascript"></script>

<!-- BEGIN BOOTBOX STYLES -->
<link href="{{assets_url}}global/plugins/easyui/themes/default/pagination.css" rel="stylesheet">
<script src="{{assets_url}}global/plugins/jquery.easyui.pagination.js"></script>

<!-- TOASTR -->
<script src="{{assets_url}}global/plugins/bootstrap-toastr/toastr.min.js"></script>

<!-- TAGSINPUT -->
<script src="{{assets_url}}global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js" type="text/javascript"></script>
<script src="{{assets_url}}global/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript"></script>
<script src="{{assets_url}}global/plugins/typeahead/handlebars.min.js" type="text/javascript"></script>
<script src="{{assets_url}}global/plugins/typeahead/typeahead.bundle.min.js" type="text/javascript"></script>

<!-- BEGIN SELECT2 / COMBOBOX SEARCH PLUGINS -->
<!-- <script type="text/javascript" src="{{assets_url}}global/plugins/bootstrap-select/bootstrap-select-my-custom.js"></script>
 -->

<script type="text/javascript" src="{{assets_url}}global/plugins/bootstrap-select/bootstrap-select.js"></script>

<!-- <script type="text/javascript" src="{{assets_url}}global/plugins/select2/select2.min.js"></script> -->

<script type="text/javascript" src="{{assets_url}}global/plugins/select2/select2.js"></script>


<!-- <script type="text/javascript" src="{{assets_url}}global/plugins/select2/my_select2.js"></script> -->


<script type="text/javascript" src="{{assets_url}}global/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
<script src="{{assets_url}}admin/pages/scripts/components-dropdowns.js"></script>

<script src="{{assets_url}}global/scripts/metronic.js" type="text/javascript"></script>
<script src="{{assets_url}}admin/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="{{assets_url}}admin/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="{{assets_url}}admin/pages/scripts/form-validation.js"></script>

<script type="text/javascript" src="{{assets_url}}global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{assets_url}}global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>

<script type="text/javascript" src="{{assets_url}}global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js"></script>
<script type="text/javascript" src="{{assets_url}}global/plugins/bootstrap-editable/inputs-ext/address/address.js"></script>
<script type="text/javascript" src="{{assets_url}}global/plugins/moment.min.js"></script>
<script type="text/javascript" src="{{assets_url}}global/plugins/jquery.mockjax.js"></script>
<script src="{{assets_url}}admin/pages/scripts/form-editable.js"></script>
<!-- <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/lodash.js/0.10.0/lodash.min.js"></script>
 -->
<script src="{{assets_url}}js/price-format.min.js"></script>


<!-- FUNGSI FUNGSI COMMON JS -->
<script src="{{assets_url}}js/extend.js" type="text/javascript"></script>
<script src="{{assets_url}}js/form-validation.js"></script>
<script src="{{assets_url}}js/common.js" type="text/javascript"></script>
<script src="{{assets_url}}js/addition.js" type="text/javascript"></script>
<script src="{{assets_url}}js/component.min.js" type="text/javascript"></script>
<script src="{{assets_url}}js/filter.js" type="text/javascript"></script>
<script src="{{assets_url}}global/scripts/datatable.js"></script>
<script src="{{assets_url}}global/scripts/selecttigatab.js"></script>
<script src="{{assets_url}}global/scripts/jquery-barcode.js"></script>
<!-- <script src="{{assets_url}}global/scripts/ean13.min.js"></script>
 -->

<script src="{{assets_url}}global/scripts/jquery.printElement.js"></script>
<script src="{{assets_url}}admin/pages/scripts/table-ajax.js"></script>



<!-- BEGIN PAGE LEVEL PLUGINS AMCHARTS -->
<!-- <script src="{{assets_url}}admin/pages/scripts/charts-amcharts.js"></script>
 -->

<!-- END PAGE LEVEL PLUGINS AMCHARTS -->


<!-- BEGIN PAGE LEVEL PLUGINS AMCHARTS -->
<script src="{{assets_url}}global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
<script src="{{assets_url}}global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
<script src="{{assets_url}}global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
<script src="{{assets_url}}global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
<script src="{{assets_url}}global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
<script src="{{assets_url}}global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
<script src="{{assets_url}}global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
<script src="{{assets_url}}global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
<script src="{{assets_url}}global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
<script src="{{assets_url}}global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>

<!-- END PAGE LEVEL PLUGINS AMCHARTS -->

 <script src="{{assets_url}}admin/pages/myscripts/dsbhorbar.js"></script>
<script src="{{assets_url}}admin/pages/myscripts/dsbpiechart.js"></script>
<script src="{{assets_url}}admin/pages/myscripts/dsbpiemaktifchart.js"></script>
<!-- <script src="{{assets_url}}admin/pages/myscripts/dsblinechart.js"></script> dsbsummerchandise-->
<!-- <script src="{{assets_url}}admin/pages/myscripts/dsblineomsetchart.js"></script> dsbsumticket-->
<!-- <script src="{{assets_url}}admin/pages/myscripts/dsblinemervaluechart.js"></script> dsbsumvalue-->
<script src="{{assets_url}}admin/pages/myscripts/dsbmachine.js"></script> 
<!-- <script src="{{assets_url}}admin/pages/myscripts/dsbomzet.js"></script> -->

<script src="{{assets_url}}admin/pages/myscripts/dsbtopsparepart.js"></script>
<script src="{{assets_url}}admin/pages/myscripts/dsbstatusservice.js"></script>

<script src="{{assets_url}}admin/pages/myscripts/dsworsemachine.js"></script>
<script src="{{assets_url}}admin/pages/myscripts/dsbtoptoko.js"></script>
<script src="{{assets_url}}admin/pages/myscripts/dsbworsetoko.js"></script> 


<!-- LOAD SCRIPT BUATAN SENDIRI -->
{% for script in javascript %}
<script src="{{assets_url}}js/{{script}}" type="text/javascript"></script>
{% endfor %}

<script>
jQuery(document).ready(function() {  
	$('.page-header').stickUp();
   	// initiate layout and plugins
	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
	Demo.init(); // init demo features
});
</script>



</body>
<!-- END BODY -->
</html>


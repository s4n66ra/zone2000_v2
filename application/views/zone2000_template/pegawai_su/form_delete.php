<div class="row-fluid">
    <div class="box-title">
        <?php echo (isset($title)) ? $title : 'Untitle'; ?>
    </div>
    <div class="box-content">
        <?php
        $hidden_form = array('id' => !empty($id) ? $id : '');
        echo form_open_multipart($form_action, array('id' => 'finput', 'class' => 'form-horizontal'), $hidden_form);
        ?>

        <div class="control-group">
            <label for="select" class="control-label">NIK Karyawan<span class="required">*</span></label>
            <div class="controls">
                <?php echo form_input('nik', !empty($default->nik) ? $default->nik : '', 'class="span6"') ?>
                
            </div>
        </div> 

        <div class="control-group">
            <label for="select" class="control-label">Nama Karyawan<span class="required">*</span></label>
            <div class="controls">
                <?php echo form_input('nama_pegawai', !empty($default->nama_pegawai) ? $default->nama_pegawai : '', 'class="span6"') ?>
                
            </div>
        </div> 

        <div class="control-group">
            <label for="select" class="control-label">No HP<span class="required">*</span></label>
            <div class="controls">
                <?php echo form_input('no_hp', !empty($default->no_hp) ? $default->no_hp : '', 'class="span6"') ?>
                
            </div>
        </div> 

        <div class="control-group">
            <label for="select" class="control-label">Alamat<span class="required">*</span></label>
            <div class="controls">
                <?php echo form_input('alamat', !empty($default->alamat) ? $default->alamat : '', 'class="span6"') ?>
                
            </div>
        </div> 

        <div class="control-group">
            <label for="select" class="control-label">Jabatan<span class="required">*</span></label>
            <div class="controls">
                <?php echo form_dropdown('id_jabatan',$options_jabatan, !empty($default->id_jabatan) ? $default->id_jabatan : '', 'class="span6 onchange chosen"') ?>  
            </div>
        </div> 

        <div class="control-group">
            <label for="select" class="control-label">Level<span class="required">*</span></label>
            <div class="controls">
                <?php echo form_dropdown('id_level',$options_level, !empty($default->id_level) ? $default->id_level : '', 'class="span6 onchange chosen"') ?>  
            </div>
        </div> 

        <div class="control-group">
            <label for="select" class="control-label">Tgl Masuk<span class="required">*</span></label>
            <div class="controls">
                <div class="input-append date" id="dp3" style="margin-right: 30px;" data-date="<?php echo!empty($default->tgl_masuk) ? date('Y-m-d', strtotime($default->tgl_masuk)) : date('Y-m-d'); ?>" data-date-format="yyyy-mm-dd">
                    <?php echo form_input('tgl_masuk', !empty($default->tgl_masuk) ? date('Y-m-d', strtotime($default->tgl_masuk)) : date('Y-m-d'), 'class="span12 onchange date" readonly=""'); ?>
                    <span class="add-on"><i class="icon-calendar"></i></span>
                </div>
            </div>
        </div>

        <div class="control-group">
            <label for="select" class="control-label">Tgl Keluar<span class="required">*</span></label>
            <div class="controls">
                <div class="input-append date" id="dp3" style="margin-right: 30px;" data-date="<?php echo!empty($default->tgl_keluar) ? date('Y-m-d', strtotime($default->tgl_keluar)) : date('Y-m-d'); ?>" data-date-format="yyyy-mm-dd">
                    <?php echo form_input('tgl_keluar', !empty($default->tgl_keluar) ? date('Y-m-d', strtotime($default->tgl_keluar)) : date('Y-m-d'), 'class="span12 onchange date" readonly=""'); ?>
                    <span class="add-on"><i class="icon-calendar"></i></span>
                </div>
            </div>
        </div>

        <div class="control-group">
            <label for="select" class="control-label">Salary<span class="required">*</span></label>
            <div class="controls">
                <div class="input-prepend">
                    <span class="add-on">Rp.</span>
                    <?php echo form_input('salary', !empty($default->salary) ? $default->salary : '', 'id = "salary" class="span12" data-a-dec="," data-a-sep="."') ?>
                </div>
            </div>
        </div> 

        <div class="control-group">
            <label for="select" class="control-label">Status Karyawan<span class="required">*</span></label>
            <div class="controls">
                <?php echo form_input('status_pegawai', !empty($default->status_pegawai) ? $default->status_pegawai : '', 'class="span6"') ?>
                
            </div>
        </div> 
        <div class="control-group">
            <label for="select" class="control-label">Keterangan<span class="required">*</span></label>
            <div class="controls">
                <?php echo form_input('keterangan', !empty($default->keterangan) ? $default->keterangan : '', 'class="span6"') ?>
                
            </div>
        </div> 


        <div class="form-actions">
            <?php echo anchor(null, '<i class="icon-trash"></i> Hapus', array('id' => 'button-delete', 'class' => 'red btn', 'onclick' => "simpan_data(this.id, '#finput', '#button-back')")); ?>
            <?php echo anchor(null, '<i class="icon-circle-arrow-left"></i> Kembali', array('id' => 'button-back', 'class' => 'btn', 'onclick' => 'close_form_modal(this.id)')); ?>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('.date').datepicker({
            format: 'dd-mm-yyyy',
        });
        $('.chosen').chosen();
        $('.numeric').numeric();
    });

    function refresh_filter(){ 
        load_table('#content_table', 1,function(){

        });
    }
</script>

{{ form_action|raw }}
	<div class="row">
		<div class="form-body col-md-12">
			<div class="form-group row">
				<label class="control-label col-md-4">Min <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="data[min]" data-required="1" class="form-control format-number" value="{{data.min}}"/>
				</div>
			</div>
			
			<div class="form-group row">
				<label class="control-label col-md-4">Max <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="data[max]" data-required="1" class="form-control format-number" value="{{data.max}}"/>
				</div>
			</div>
			<div class="form-group row">
				<label class="control-label col-md-4">Kelipatan <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="data[kelipatan]" value = "{{data.kelipatan}}" data-required="1" class="form-control format-number"/>
				</div>
			</div>

			<!-- <div class="form-group row">
				<label class="control-label col-md-4">Ticket Redeemp <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					<input type="text" name="data[jumlah_tiket]" value = "{{data.jumlah_tiket}}" data-required="1" class="form-control format-number"/>
				</div>
			</div> -->

<!-- 			<div class="form-group row">
				<label class="control-label col-md-4">Type <span class="required">
				* </span>
				</label>
				<div class="col-md-8">
					{{options_hadiah|raw}}
				</div>
			</div> -->
		</div>
	</div>
</form>
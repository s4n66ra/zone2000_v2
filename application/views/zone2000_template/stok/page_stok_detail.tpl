<style>
	.page-history-detail {

	}
</style>

<div class="page-history-detail">
	<a class="btn btn-xs pull-right btn-hide-drilldown" onclick="hideDrilldown(this)"><i class="fa fa-angle-up"></i> hide</a>
{% for key, detail in details %}
<div class="row">
	<div class="col-md-12">
		
		<div class="caption">
			
			<h4 class="uppercase">{{ detail.name }}</h4>
		</div>

		<div class="form-body">
			<div class="form-group row">
				<label class="control-label col-md-2">No. PO </label>
				<div class="col-md-4">
					{{detail.doc_number}}
				</div>
			</div>

		 	{{ detail.table | raw}}
		</div>
		
	</div>
</div>
{% endfor %}
</div>
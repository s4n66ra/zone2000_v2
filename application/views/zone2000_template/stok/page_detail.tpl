<style type="text/css">
	.grand-total {
		color :rgb(231, 0, 0);
		padding-right: : 20px;
		font-size: 26px;
		line-height: 28px;
		text-align: right;
  		font-weight: bolder;
	}
</style>

<div class="portlet light">
	<div class="portlet-title">
		{{ btn_page_back()|raw }}
		<div class="caption">
			<i class="glyphicon glyphicon-list-alt font-blue-madison"></i>
			<span class="caption-subject bold font-blue-madison uppercase">
			STOK WAREHOUSE KOLI DETAIL </span>
			<span class="caption-helper"> View</span>
		</div>
		{% if st_bundle==0 %}
		<div class="actions">


<!-- 			<a href="#" class="btn btn-circle yellow-saffron btn-supplier-save" url-action="{{url.generate_po}}"><i class="fa fa-check"></i> Generate Purchase Order </a>
 -->			
			<a href="" class="collapse">
			<a href="#" class="btn btn-circle btn-default btn-icon-only fullscreen" data-original-title="" title=""></a>
		</div>
		{% endif %}
	</div>
	<div class="portlet-body">

		<div class="tabbable-custom show-overflow">
			<ul class="nav nav-tabs">
				<li class="active">
					<a href="#tab_po_detail_1" data-toggle="tab">
					KOLI DETAIL</a>
				</li>

			</ul>

			<div class="tab-content">
				<div class="tab-pane active" id="tab_po_detail_1">

					<div class="row">
						<div class="col-md-6">
							<input type="hidden" id="transactionbundle-id" value="{{data.transactionbundle_id}}" />
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							
							<div class="portlet light">
								<div class="portlet-title">
									<div class="caption">
										<i class="icon-puzzle font-grey-gallery"></i>
										<span class="caption-subject bold font-grey-gallery uppercase">
										{{ list.koli_code }}  </span>
										<span class="caption-helper"></span>
									</div>
<!-- 									<div class="tools">
										<a href="" class="collapse">
										</a>
									</div>
 -->							
 								</div>
								
								<div class="portlet-body form-body">
<!-- 								{% for key, po in list.postore %}
								 	{{ po.item_name | raw}}
								{% endfor %}
 -->							
 								{{list.content | raw}}	
 								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="tab-pane" id="tab_po_detail_2">
					{{ page.history |raw }}
				</div>

			</div>

		</div>

	</div>
</div>

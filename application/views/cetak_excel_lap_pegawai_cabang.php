<?php 
if(!empty($filename)){$filename=$filename.'.xls';} else {$filename="export.xls";}
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$filename");
header("Pragma: no-cache");
header("Expires: 0");
?> 
<?php echo "Tanggal : ".date('Y-m-d H:i:s')."<br>"; ?>
<?php echo $judul_kecil ?>

<hr>
<table border = 1>
<?php if(!empty($header)){
    
    
    $dataHeader = '';
    $dataHeader .=  '<tr>';
    $dataHeader .= '<td>NO</td>';
    foreach ($header as $key) {
        
        $dataHeader .=  '<td>'.$key.'</td>';
    }
    $dataHeader .=  '</tr>';

    
    $no = 1;
    $dataIsi = '';
    $headerDivisi = '';
    foreach ($content as $key => $value) {
        $dataIsi .= '<tr>';
        if($no<=count($content))
            $dataIsi .=  '<td>'.$no.'</td>';
        foreach ($content[$key] as $k => $v) {
            if($k!='no' && $k != 'kd_cabang' && $k != 'nama_cabang')
            $dataIsi .=  '<td> '.$v.'</td>';
            if($k == 'kd_cabang')
            $tempHeaderDivisi = $v;
            if($k == 'nama_cabang')
                $nama_cabang = $v;
        }
        $dataIsi .=  '</tr>';
        $no++;
        if($headerDivisi != $tempHeaderDivisi){
            if($no != 2)
                echo '<tr><td colspan = 6 align = "center">&nbsp;</td></tr>';
            $headerDivisi = $tempHeaderDivisi;
            echo '<tr><td colspan = 6 align = "center">'.$headerDivisi.' - '.$nama_cabang.'</td></tr>';
            echo $dataHeader;
            echo $dataIsi;
        }else{
            echo $dataIsi;
        }
        $dataIsi = '';
    }

}
?>
</table>
</br>
<?php echo $table;?>

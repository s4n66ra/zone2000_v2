	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<!-- general form elements disabled -->
				<div class="box box-warning">
					<div class="box-header">
						<h3 class="box-title">Form Product</h3>
					</div><!-- /.box-header -->
					<form id="form" role="form" action="" method="post">
						<input type="hidden" id="product-id" name="product_id" value="{{ product_id }}" />
						<div class="box-body">
							<!-- text input -->
							<div class="form-group group-category">
								{#<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> Input with error</label>#}
								<label>Category</label>
								<select id="category">
									{% for category in categories %}
										<option value="{{ category.category_id }}" {% if( category_id == category.category_id) %} selected {% endif %}>{{ category.category_name }}</option>
									{% endfor %}
								</select>
							</div>
							
							<div class="form-group group-name">
								<label>Name</label>
								<input id="name" type="text" class="form-control" placeholder="Product Name" value="{{ product_name }}">
							</div>
							
							<div class="form-group group-model">
								<label>Model</label>
								<input id="model" type="text" class="form-control" placeholder="Product Model" value="{{ product_model }}">
							</div>
							
							<div class="form-group group-detail">
								<label>Detail</label>
								<textarea id="detail" name="detail" rows="10" cols="80">
									{{ product_detail }}
								</textarea>
							</div>
							
							<div class="form-group group-price">
								<label>Price</label>
								<input type="number" class="form-control" id="price" placeholder="Product Price" value="{{ product_price }}" />
							</div>
							
							<div class="form-group group-file">
								<label for="exampleInputFile">Image</label>
								<input type="hidden" name="product_image_id" id="product-image-id" value="{{ product_image_id }}" />
								<input type="file" id="imageFile">
								<img id="preview" width="400" {% if(product_image_name) %} src="{{ base_url() }}{{ product_image_name }}" style="margin-top: 10px;" {% else %} style="display: none;margin-top: 10px;" {% endif %}/>
								<p class="help-block">Choose image. Format jpg | jpeg | png. Max Size 2MB</p>
							</div>
						</div><!-- /.box-body -->
						<div class="box-footer text-right">
							<button id="btn-cancel" type="button" class="btn btn-default">Cancel</button>
							<button id="btn-save" type="submit" class="btn btn-primary">Save</button>
						</div>
					</form>
				</div><!-- /.box -->
			</div><!--/.col (right) -->
		</div>
	</section>
	{% extends "admin/base/base.tpl" %}
	
	{% block css %}
        <link href="{{ base_url() }}public/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
	{% endblock %}
	
	{% block content %}
		<aside class="right-side">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Products
					<small>Manage Products</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="{{ base_url() }}admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
					<li class="active">Products</li>
				</ol>
			</section>

			<!-- Main content -->
			{% if action == "add" or action == "edit" %}
				{% include "admin/products/products.add.edit.tpl" %}
			{% else %}
				{% include "admin/products/products.view.tpl" %}
			{% endif %}
		</aside><!-- /.right-side -->
	{% endblock %}
	
	{% block js %}
		<!-- DATA TABES SCRIPT -->
        <script src="{{ base_url() }}public/admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="{{ base_url() }}public/admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
		<script src="{{ base_url() }}public/admin/js/products/add-edit-products.js" type="text/javascript"></script>
		<script src="{{ base_url() }}public/admin/js/products/products.js" type="text/javascript"></script>
		<!-- CK Editor -->
        <script src="//cdn.ckeditor.com/4.4.3/full/ckeditor.js"></script>
		<script>
			$(document).ready(function(){
				var products = Products.createIt({
					baseUrl: '{{ base_url() }}',
					action: '{{ action }}'
				})
				
				products.attach();
			});
		</script>
	{% endblock %}
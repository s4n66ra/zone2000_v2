	{% block sidemenu %}	
		<aside class="left-side sidebar-offcanvas">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->
				<div class="user-panel">
					<div class="pull-left image">
						<img src="{{ base_url() }}public/admin/img/photo.png" class="img-circle" alt="User Image" />
					</div>
					<div class="pull-left info">
						<p>Hello, {{ session.userdata.fullname }}</p>
						{#
						<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
						#}
					</div>
				</div>
				<!-- search form -->
				{#
				<form action="#" method="get" class="sidebar-form">
					<div class="input-group">
						<input type="text" name="q" class="form-control" placeholder="Search..."/>
						<span class="input-group-btn">
							<button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
						</span>
					</div>
				</form>
				#}
				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<ul class="sidebar-menu">
					<li class="active">
						<a href="{{ base_url }}dashboard">
							<i class="fa fa-dashboard"></i> <span>Dashboard</span>
						</a>
					</li>
					<li>
						<a href="{{ base_url() }}admin/base_categories">
							<i class="fa fa-link"></i> <span>Base Categories</span>
						</a>
					</li>
					<li>
						<a href="{{ base_url() }}admin/categories">
							<i class="fa fa-share-alt"></i> <span>Categories</span>
						</a>
					</li>
					<li>
						<a href="{{ base_url() }}admin/products">
							<i class="fa fa-suitcase"></i> <span>Products</span>
						</a>
					</li>
					<li>
						<a href="{{ base_url() }}admin/products_tag">
							<i class="fa fa-tags"></i> <span>Products Tag</span>
						</a>
					</li>
					<li>
						<a href="{{ base_url() }}admin/about_us">
							<i class="fa fa-tasks"></i> <span>About Us</span>
						</a>
					</li>
					<li>
						<a href="{{ base_url() }}admin/resi_pengiriman">
							<i class="fa fa-truck"></i> <span>Resi Pengiriman</span>
						</a>
					</li>
					<li class="treeview">
						<a href="#">
							<i class="fa fa-comment"></i> <span>Testimonial</span>
							<i class="fa fa-angle-left pull-right"></i>
						</a>
						<ul class="treeview-menu">
							<li><a href="{{ base_url() }}admin/watermark"><i class="fa fa-angle-double-right"></i> Watermark Image</a></li>
							<li><a href="{{ base_url() }}admin/testimonial"><i class="fa fa-angle-double-right"></i> Testimoni</a></li>
						</ul>
					</li>
					{#
					<li>
						<a href="{{ base_url() }}admin/testimonial">
							<i class="fa fa-comment"></i> <span>Testimonial</span>
						</a>
					</li>
					#}
					<li>
						<a href="{{ base_url() }}admin/contact_us">
							<i class="fa fa-phone"></i> <span>Contact Us</span>
						</a>
					</li>
					<li class="treeview">
						<a href="#">
							<i class="fa fa-folder"></i> <span>Utility</span>
							<i class="fa fa-angle-left pull-right"></i>
						</a>
						<ul class="treeview-menu">
							<li><a href="{{ base_url() }}admin/website_title"><i class="fa fa-angle-double-right"></i> Website Title</a></li>
							<li><a href="{{ base_url() }}admin/keyword"><i class="fa fa-angle-double-right"></i> Keyword</a></li>
							<li><a href="{{ base_url() }}admin/banner"><i class="fa fa-angle-double-right"></i> Banner</a></li>
							<li><a href="{{ base_url() }}admin/cara_pemesanan"><i class="fa fa-angle-double-right"></i> Cara Pemesanan</a></li>
							<li><a href="{{ base_url() }}admin/contact"><i class="fa fa-angle-double-right"></i> Contact</a></li>
						</ul>
					</li>
					<li>
						<a href="{{ base_url() }}admin/users">
							<i class="fa fa-users"></i> <span>Users</span>
						</a>
					</li>
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>
	{% endblock %}
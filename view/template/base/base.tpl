<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Admin | Dashboard</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		
		<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		<!-- Ionicons -->
		<link href="//code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css" rel="stylesheet" type="text/css" />
		<!-- Morris chart -->
		<link href="{{ base_url() }}public/admin/css/morris/morris.css" rel="stylesheet" type="text/css" />
		<!-- jvectormap -->
		<link href="{{ base_url() }}public/admin/css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
		<!-- Date Picker -->
		<link href="{{ base_url() }}public/admin/css/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
		<!-- Daterange picker -->
		<link href="{{ base_url() }}public/admin/css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
		<!-- bootstrap wysihtml5 - text editor -->
		<link href="{{ base_url() }}public/admin/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
		<!-- Theme style -->
		<link href="{{ base_url() }}public/admin/css/AdminLTE.css" rel="stylesheet" type="text/css" />
		<link href="{{ base_url() }}public/admin/css/select2/select2.css" rel="stylesheet" type="text/css" />
		<link href="{{ base_url() }}public/admin/css/custom.css" rel="stylesheet" type="text/css" />
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		{% block css %}
		{% endblock %}
    </head>
    <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="#" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                Admin
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            {% include "admin/base/base.menu.tpl" %}
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
			{% include "admin/base/base.sidemenu.tpl" %}

            <!-- Right side column. Contains the navbar and content of the page -->
            {% block content %}
			{% endblock %}
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="//code.jquery.com/ui/1.11.1/jquery-ui.min.js" type="text/javascript"></script>
		<!-- daterangepicker -->
		<script src="{{ base_url() }}public/admin/js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
		<!-- datepicker -->
		<script src="{{ base_url() }}public/admin/js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
		<!-- Bootstrap WYSIHTML5 -->
		<script src="{{ base_url() }}public/admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
		<!-- iCheck -->
		<script src="{{ base_url() }}public/admin/js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
		<!-- AdminLTE App -->
		<script src="{{ base_url() }}public/admin/js/AdminLTE/app.js" type="text/javascript"></script>
		<!-- AdminLTE for demo purposes -->
		<script src="{{ base_url() }}public/admin/js/AdminLTE/demo.js" type="text/javascript"></script>
		<script src="{{ base_url() }}public/admin/js/plugins/select2/select2.min.js" type="text/javascript"></script>
		<script src="{{ base_url() }}public/admin/js/global.js" type="text/javascript"></script>
		<script src="{{ base_url() }}public/admin/js/loader.js" type="text/javascript"></script>
		{% block js %}
		{% endblock %}
		
		<div class="mask"></div>
		<div class="loader">Loading...</div>
    </body>
</html>
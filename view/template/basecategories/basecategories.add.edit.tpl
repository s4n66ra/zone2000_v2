	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<!-- general form elements disabled -->
				<div class="box box-warning">
					<div class="box-header">
						<h3 class="box-title">Form Base Category</h3>
					</div><!-- /.box-header -->
					<form id="form" role="form" action="" method="post">
						<input type="hidden" id="basecategories-id" name="basecategories_id" value="{{ base_category_id }}" />
						<div class="box-body">
							<!-- text input -->
							<div class="form-group group-basecategories-name">
								{#<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> Input with error</label>#}
								<label>Base Category Name</label>
								<input type="text" class="form-control" id="basecategories-name" placeholder="Base Category Name" value="{{ base_category_name }}" />
							</div>
						</div><!-- /.box-body -->
						<div class="box-footer text-right">
							<button id="btn-cancel" type="button" class="btn btn-default">Cancel</button>
							<button id="btn-save" type="submit" class="btn btn-primary">Save</button>
						</div>
					</form>
				</div><!-- /.box -->
			</div><!--/.col (right) -->
		</div>
	</section>
	{% extends "admin/base/base.tpl" %}
	
	{% block css %}
        <link href="{{ base_url() }}public/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
	{% endblock %}
	
	{% block content %}
		<aside class="right-side">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Base Categories
					<small>Manage Base Categories</small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="{{ base_url() }}admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
					<li class="active">Base Categories</li>
				</ol>
			</section>

			<!-- Main content -->
			{% if action == "add" or action == "edit" %}
				{% include "admin/basecategories/basecategories.add.edit.tpl" %}
			{% else %}
				{% include "admin/basecategories/basecategories.view.tpl" %}
			{% endif %}
		</aside><!-- /.right-side -->
	{% endblock %}
	
	{% block js %}
		<!-- DATA TABES SCRIPT -->
        <script src="{{ base_url() }}public/admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="{{ base_url() }}public/admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
		<script src="{{ base_url() }}public/admin/js/basecategories/add-edit-basecategories.js" type="text/javascript"></script>
		<script src="{{ base_url() }}public/admin/js/basecategories/basecategories.js" type="text/javascript"></script>
		<script>
			$(document).ready(function(){
				var basecategories = BaseCategories.createIt({
					baseUrl: '{{ base_url() }}',
					action: '{{ action }}'
				})
				
				basecategories.attach();
			});
		</script>
	{% endblock %}
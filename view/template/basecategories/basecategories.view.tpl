	
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-warning">
					<div class="box-header">
						<h3 class="box-title">List Data</h3>
						<button id="add-basecategories" class="btn btn-success btn-flat pull-right btn-new">New Base Category</button>
					</div><!-- /.box-header -->
					<div class="box-body table-responsive">
						<table id="basecategories-list" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>#</th>
									<th>Base Category Name</th>
									<th class="text-center">Action</th>
								</tr>
							</thead>
							<tbody>
						</table>
					</div><!-- /.box-body -->
				</div><!-- /.box -->
			</div>
		</div>
	</section><!-- /.content -->
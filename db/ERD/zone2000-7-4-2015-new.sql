-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema zone2000
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema zone2000
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `zone2000` DEFAULT CHARACTER SET latin1 ;
-- -----------------------------------------------------
-- Schema zone2000
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema zone2000
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `zone2000` DEFAULT CHARACTER SET latin1 ;
USE `zone2000` ;

-- -----------------------------------------------------
-- Table `zone2000`.`m_kelas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`m_kelas` (
  `id_kelas` INT(3) NOT NULL AUTO_INCREMENT,
  `kd_kelas` VARCHAR(40) NOT NULL,
  `nama_kelas` VARCHAR(250) NOT NULL,
  `min_value` INT(20) NOT NULL,
  `max_value` INT(20) NOT NULL,
  PRIMARY KEY (`id_kelas`))
ENGINE = MyISAM
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `zone2000`.`m_kategori_mesin`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`m_kategori_mesin` (
  `id_kategori_mesin` INT(3) NOT NULL AUTO_INCREMENT,
  `kd_kategori_mesin` VARCHAR(20) NOT NULL,
  `nama_kategori_mesin` VARCHAR(250) NOT NULL,
  PRIMARY KEY (`id_kategori_mesin`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `zone2000`.`m_jenis_mesin`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`m_jenis_mesin` (
  `id_jenis_mesin` INT(4) NOT NULL,
  `kd_jenis_mesin` VARCHAR(40) NOT NULL,
  `nama_mesin` VARCHAR(50) NOT NULL,
  `deskripsi` VARCHAR(100) NULL,
  `thumbnail` VARCHAR(50) NULL DEFAULT NULL,
  `foto` VARCHAR(50) NULL DEFAULT NULL,
  `jenis_koin` INT(2) NULL,
  `id_kategori_mesin` INT(3) NOT NULL,
  `id_kelas` INT(3) NOT NULL,
  PRIMARY KEY (`id_jenis_mesin`),
  INDEX `fk_jenis_mesin_kelas_idx` (`id_kelas` ASC),
  INDEX `fk_jenis_mesin_kategori_idx` (`id_kategori_mesin` ASC),
  CONSTRAINT `fk_jenis_mesin_kelas`
    FOREIGN KEY (`id_kelas`)
    REFERENCES `zone2000`.`m_kelas` (`id_kelas`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_jenis_mesin_kategori`
    FOREIGN KEY (`id_kategori_mesin`)
    REFERENCES `zone2000`.`m_kategori_mesin` (`id_kategori_mesin`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `zone2000`.`m_supplier`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`m_supplier` (
  `id_supplier` INT(3) NOT NULL AUTO_INCREMENT,
  `nama_supplier` VARCHAR(250) NOT NULL,
  `kd_supplier` VARCHAR(20) NOT NULL,
  `urutan` INT(3) NOT NULL,
  `alamat_supplier` TEXT NOT NULL,
  PRIMARY KEY (`id_supplier`))
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `zone2000`.`m_negara`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`m_negara` (
  `id_negara` INT(11) NOT NULL AUTO_INCREMENT,
  `nama_negara` VARCHAR(30) NOT NULL,
  PRIMARY KEY (`id_negara`),
  UNIQUE INDEX `nama_negara` (`nama_negara` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `zone2000`.`m_pulau`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`m_pulau` (
  `id_pulau` INT(3) NOT NULL AUTO_INCREMENT,
  `nama_pulau` VARCHAR(250) NOT NULL,
  `kd_pulau` VARCHAR(20) NOT NULL,
  `id_negara` INT(11) NOT NULL DEFAULT '1',
  `urutan` INT(3) NOT NULL,
  PRIMARY KEY (`id_pulau`),
  INDEX `fk_pulau_negara_idx` (`id_negara` ASC),
  CONSTRAINT `fk_pulau_negara`
    FOREIGN KEY (`id_negara`)
    REFERENCES `zone2000`.`m_negara` (`id_negara`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `zone2000`.`m_provinsi`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`m_provinsi` (
  `id_provinsi` INT(3) NOT NULL AUTO_INCREMENT,
  `nama_provinsi` VARCHAR(250) NOT NULL,
  `kd_provinsi` VARCHAR(20) NOT NULL,
  `urutan` INT(3) NOT NULL,
  `id_pulau` INT(3) NOT NULL,
  PRIMARY KEY (`id_provinsi`),
  INDEX `fk_provinsi_pulau_idx` (`id_pulau` ASC),
  CONSTRAINT `fk_provinsi_pulau`
    FOREIGN KEY (`id_pulau`)
    REFERENCES `zone2000`.`m_pulau` (`id_pulau`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `zone2000`.`m_kota`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`m_kota` (
  `id_kota` INT(5) NOT NULL AUTO_INCREMENT,
  `nama_kota` VARCHAR(250) NULL DEFAULT NULL,
  `urutan` INT(5) NULL DEFAULT NULL,
  `id_provinsi` INT(3) NULL,
  `kd_kota` VARCHAR(50) NULL DEFAULT NULL,
  PRIMARY KEY (`id_kota`),
  INDEX `fk_kota_provinsi_idx` (`id_provinsi` ASC),
  CONSTRAINT `fk_kota_provinsi`
    FOREIGN KEY (`id_provinsi`)
    REFERENCES `zone2000`.`m_provinsi` (`id_provinsi`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `zone2000`.`m_grup`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`m_grup` (
  `grup_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `grup_nama` VARCHAR(25) NULL,
  `grup_deskripsi` VARCHAR(50) NULL DEFAULT NULL,
  `otoritas_data` CHAR(1) NULL DEFAULT NULL,
  PRIMARY KEY (`grup_id`),
  UNIQUE INDEX `grup_id` (`grup_id` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 20
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `zone2000`.`m_user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`m_user` (
  `user_id` INT UNSIGNED NOT NULL,
  `nik` VARCHAR(20) NULL,
  `grup_id` INT(11) UNSIGNED NULL,
  `user_name` VARCHAR(25) NULL DEFAULT NULL,
  `user_password` VARCHAR(50) NULL DEFAULT NULL,
  `last_update` DATE NULL DEFAULT NULL,
  `is_active` CHAR(1) NULL DEFAULT NULL,
  `last_login` DATE NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  INDEX `grup` (`grup_id` ASC),
  INDEX `pegawai` (`nik` ASC),
  UNIQUE INDEX `user_id_UNIQUE` (`user_id` ASC),
  CONSTRAINT `fk_user_group`
    FOREIGN KEY (`grup_id`)
    REFERENCES `zone2000`.`m_grup` (`grup_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `zone2000`.`m_jabatan`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`m_jabatan` (
  `id_jabatan` INT(4) NOT NULL AUTO_INCREMENT,
  `kd_jabatan` VARCHAR(50) NULL DEFAULT NULL,
  `nama_jabatan` VARCHAR(250) NULL DEFAULT NULL,
  `urutan` INT(3) NOT NULL,
  PRIMARY KEY (`id_jabatan`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `zone2000`.`m_pegawai`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`m_pegawai` (
  `id_pegawai` INT(6) NOT NULL AUTO_INCREMENT,
  `nik` VARCHAR(50) NULL DEFAULT NULL,
  `nama_pegawai` VARCHAR(250) NULL,
  `no_hp` VARCHAR(50) NULL DEFAULT NULL,
  `alamat` TEXT NULL,
  `id_jabatan` INT(4) NULL DEFAULT NULL,
  `id_level` INT(4) NULL DEFAULT NULL,
  `tgl_masuk` DATE NULL DEFAULT NULL,
  `tgl_keluar` DATE NULL DEFAULT NULL,
  `salary` INT(10) NULL DEFAULT NULL,
  `status_pegawai` INT(3) NULL DEFAULT NULL,
  `keterangan` TEXT NULL DEFAULT NULL,
  `user_id` INT UNSIGNED NULL,
  PRIMARY KEY (`id_pegawai`),
  INDEX `fk_pegawai_user_idx` (`user_id` ASC),
  INDEX `fk_pegawai_jabatan_idx` (`id_jabatan` ASC),
  CONSTRAINT `fk_pegawai_user`
    FOREIGN KEY (`user_id`)
    REFERENCES `zone2000`.`m_user` (`grup_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pegawai_jabatan`
    FOREIGN KEY (`id_jabatan`)
    REFERENCES `zone2000`.`m_jabatan` (`id_jabatan`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `zone2000`.`m_area`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`m_area` (
  `id_area` INT(6) NOT NULL,
  `kd_area` VARCHAR(20) NOT NULL,
  `nama_area` VARCHAR(250) NOT NULL,
  `urutan` INT(3) NULL,
  `id_kota` INT(5) NOT NULL DEFAULT '1',
  `id_ka_area` INT(6) NULL,
  PRIMARY KEY (`id_area`),
  INDEX `fk_area_kota_idx` (`id_kota` ASC),
  INDEX `fk_area_pegawai_idx` (`id_ka_area` ASC),
  CONSTRAINT `fk_area_kota`
    FOREIGN KEY (`id_kota`)
    REFERENCES `zone2000`.`m_kota` (`id_kota`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_area_pegawai`
    FOREIGN KEY (`id_ka_area`)
    REFERENCES `zone2000`.`m_pegawai` (`id_pegawai`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `zone2000`.`m_owner`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`m_owner` (
  `id_owner` INT(6) NOT NULL AUTO_INCREMENT,
  `kd_owner` VARCHAR(50) NULL,
  `nama_owner` VARCHAR(100) NULL,
  PRIMARY KEY (`id_owner`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zone2000`.`m_cabang`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`m_cabang` (
  `id_cabang` INT(3) NOT NULL AUTO_INCREMENT,
  `kd_cabang` VARCHAR(20) NOT NULL,
  `nama_cabang` VARCHAR(250) NOT NULL,
  `urutan` INT(3) NOT NULL,
  `alamat` TEXT NOT NULL,
  `telp` VARCHAR(250) NOT NULL,
  `fax` VARCHAR(20) NULL DEFAULT NULL,
  `date_start` DATE NOT NULL,
  `id_kepcabang` INT(6) NULL,
  `id_owner` INT(6) NOT NULL,
  `luas` INT(6) NOT NULL,
  `pajak_toko` INT(8) NOT NULL,
  `id_area` INT(6) NULL,
  PRIMARY KEY (`id_cabang`),
  INDEX `fk_cabang_area_idx` (`id_area` ASC),
  INDEX `fk_cabang_owner_idx` (`id_owner` ASC),
  CONSTRAINT `fk_cabang_area`
    FOREIGN KEY (`id_area`)
    REFERENCES `zone2000`.`m_area` (`id_area`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cabang_owner`
    FOREIGN KEY (`id_owner`)
    REFERENCES `zone2000`.`m_owner` (`id_owner`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 10
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `zone2000`.`m_koin`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`m_koin` (
  `id_koin` INT(2) NOT NULL AUTO_INCREMENT,
  `kd_koin` VARCHAR(50) NULL,
  `nama_koin` VARCHAR(50) NULL,
  PRIMARY KEY (`id_koin`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zone2000`.`m_mesin`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`m_mesin` (
  `id_mesin` INT(5) NOT NULL AUTO_INCREMENT,
  `kd_mesin` VARCHAR(250) NOT NULL,
  `serial_number` VARCHAR(50) NULL,
  `harga` INT(20) NOT NULL,
  `tgl_beli` DATE NOT NULL,
  `garansi_toko` TEXT NOT NULL,
  `status` VARCHAR(45) NULL COMMENT 'status\n0 : fine\n1 : rusak tapi masih bisa jalan\n2 : rusak dan tidak jalan/nyala',
  `id_supplier` INT(3) NOT NULL COMMENT 'id_supplier , harga , dan tgl_beli merupakan duplikasi data dari tabel pengadaan detail',
  `id_jenis_mesin` INT(4) NULL,
  `id_cabang` INT(3) NOT NULL,
  `id_koin` INT(2) NULL COMMENT 'jenis koin nya apa \n',
  `id_owner` INT(6) NULL,
  PRIMARY KEY (`id_mesin`),
  INDEX `fk_mesin_jenis_mesin_idx` (`id_jenis_mesin` ASC),
  INDEX `fk_mesin_supplier_idx` (`id_supplier` ASC),
  INDEX `fk_mesin_cabang_idx` (`id_cabang` ASC),
  INDEX `fk_mesin_koin_idx` (`id_koin` ASC),
  INDEX `fk_mesin_owner_idx` (`id_owner` ASC),
  CONSTRAINT `fk_mesin_jenis_mesin`
    FOREIGN KEY (`id_jenis_mesin`)
    REFERENCES `zone2000`.`m_jenis_mesin` (`id_jenis_mesin`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_mesin_supplier`
    FOREIGN KEY (`id_supplier`)
    REFERENCES `zone2000`.`m_supplier` (`id_supplier`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_mesin_cabang`
    FOREIGN KEY (`id_cabang`)
    REFERENCES `zone2000`.`m_cabang` (`id_cabang`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_mesin_koin`
    FOREIGN KEY (`id_koin`)
    REFERENCES `zone2000`.`m_koin` (`id_koin`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_mesin_owner`
    FOREIGN KEY (`id_owner`)
    REFERENCES `zone2000`.`m_owner` (`id_owner`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `zone2000`.`zn_mesin_history`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`zn_mesin_history` (
  `id_mesin_history` INT NOT NULL AUTO_INCREMENT,
  `kd_mesin_history` VARCHAR(50) NULL,
  `tgl_pengiriman` DATETIME NULL,
  `tgl_penerimaan` DATETIME NULL,
  `id_penerima` INT(3) NULL,
  `id_pengirim` INT(3) NULL,
  `tipe_mutasi` ENUM('purchase','transfer') NULL COMMENT '0 : purchase\n1 : transfer',
  `id_mesin` INT(5) NOT NULL,
  PRIMARY KEY (`id_mesin_history`),
  INDEX `fk_mesin_history_cabang_pengirim_idx` (`id_pengirim` ASC),
  INDEX `fk_mesin_history_cabang_penerima_idx` (`id_penerima` ASC),
  INDEX `fk_mesin_history_mesin_idx` (`id_mesin` ASC),
  CONSTRAINT `fk_mesin_history_mesin`
    FOREIGN KEY (`id_mesin`)
    REFERENCES `zone2000`.`m_mesin` (`id_mesin`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_mesin_history_cabang_pengirim`
    FOREIGN KEY (`id_pengirim`)
    REFERENCES `zone2000`.`m_cabang` (`id_cabang`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_mesin_history_cabang_penerima`
    FOREIGN KEY (`id_penerima`)
    REFERENCES `zone2000`.`m_cabang` (`id_cabang`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zone2000`.`zn_mesin_kerusakan`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`zn_mesin_kerusakan` (
  `id_mesin_rusak` INT NOT NULL AUTO_INCREMENT,
  `kd_mesin_rusak` VARCHAR(50) NULL,
  `id_mesin` INT(5) NULL,
  `tgl_mulai` DATETIME NULL COMMENT 'tanggal mulai perbaikan',
  `tgl_rusak` DATETIME NULL,
  `tgl_selesai` DATETIME NULL,
  `status_perbaikan` INT(1) NULL COMMENT '0 : perbaikan sendiri oleh teknisi di cabang\n1 : service oleh pusat\n',
  `status` INT(1) NULL COMMENT '0 : oke dah beres\n1 : rusak\n2 : inspection\n3 : waiting for spare part',
  `deskripsi` VARCHAR(255) NULL COMMENT 'keterangan kerusakan : \nmisal layar mati, tombol tidak bisa ditekan, dsb',
  PRIMARY KEY (`id_mesin_rusak`),
  INDEX `fk_mesin_kerusakan_mesin_idx` (`id_mesin` ASC),
  CONSTRAINT `fk_mesin_kerusakan_mesin`
    FOREIGN KEY (`id_mesin`)
    REFERENCES `zone2000`.`m_mesin` (`id_mesin`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'mencatat history kerusakan sparepart pada mesin';


-- -----------------------------------------------------
-- Table `zone2000`.`m_sparepart`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`m_sparepart` (
  `id_sparepart` INT(6) NOT NULL AUTO_INCREMENT,
  `nama_sparepart` VARCHAR(250) NULL DEFAULT NULL,
  `kd_sparepart` VARCHAR(50) NULL DEFAULT NULL,
  PRIMARY KEY (`id_sparepart`))
ENGINE = MyISAM
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `zone2000`.`zn_rusak_sparepart`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`zn_rusak_sparepart` (
  `id_rusak_sparepart` INT NOT NULL AUTO_INCREMENT,
  `kd_rusak_sparepart` VARCHAR(50) NULL,
  `id_mesin_rusak` INT NULL,
  `id_sparepart` INT(6) NULL,
  PRIMARY KEY (`id_rusak_sparepart`),
  INDEX `fk_rusak_mesin_rusak_idx` (`id_mesin_rusak` ASC),
  INDEX `fk_rusak_sparepart_sparepart_idx` (`id_sparepart` ASC),
  CONSTRAINT `fk_rusak_sparepart_mesin_rusak`
    FOREIGN KEY (`id_mesin_rusak`)
    REFERENCES `zone2000`.`zn_mesin_kerusakan` (`id_mesin_rusak`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rusak_sparepart_sparepart`
    FOREIGN KEY (`id_sparepart`)
    REFERENCES `zone2000`.`m_sparepart` (`id_sparepart`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zone2000`.`zn_mesin_kerusakan_history`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`zn_mesin_kerusakan_history` (
  `id_rusak_history` INT NOT NULL AUTO_INCREMENT,
  `kd_rusak_history` VARCHAR(50) NULL,
  `tanggal` DATE NULL,
  `status` VARCHAR(45) NULL COMMENT '0 : done\n1 : rusak\n2 : inspection\n3 : waiting for sparepart',
  `id_mesin_rusak` INT NULL,
  `id_pegawai` INT(6) NULL,
  PRIMARY KEY (`id_rusak_history`),
  INDEX `fk_mesin_kerusakan_history_mesin_kerusakan_idx` (`id_mesin_rusak` ASC),
  INDEX `fk_mesin_kerusakan_history_pegawai_idx` (`id_pegawai` ASC),
  CONSTRAINT `fk_mesin_kerusakan_history_mesin_kerusakan`
    FOREIGN KEY (`id_mesin_rusak`)
    REFERENCES `zone2000`.`zn_mesin_kerusakan` (`id_mesin_rusak`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_mesin_kerusakan_history_pegawai`
    FOREIGN KEY (`id_pegawai`)
    REFERENCES `zone2000`.`m_pegawai` (`id_pegawai`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zone2000`.`m_kategori_kartu`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`m_kategori_kartu` (
  `id_kategori_kartu` INT(3) NOT NULL AUTO_INCREMENT,
  `kd_kategori_kartu` VARCHAR(40) NOT NULL,
  `nama_kategori_kartu` VARCHAR(250) NOT NULL,
  `harga` INT(8) NOT NULL,
  PRIMARY KEY (`id_kategori_kartu`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `zone2000`.`m_kartu`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`m_kartu` (
  `id_kartu` INT(2) NOT NULL,
  `kd_kartu` VARCHAR(50) NULL,
  `nama_pemilik` VARCHAR(50) NULL,
  `id_cabang` INT(3) NULL,
  `id_kategori_kartu` INT(3) NULL,
  PRIMARY KEY (`id_kartu`),
  INDEX `fk_kartu_cabang_idx` (`id_cabang` ASC),
  INDEX `fk_kartu_kategori_katu_idx` (`id_kategori_kartu` ASC),
  CONSTRAINT `fk_kartu_cabang`
    FOREIGN KEY (`id_cabang`)
    REFERENCES `zone2000`.`m_cabang` (`id_cabang`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_kartu_kategori_katu`
    FOREIGN KEY (`id_kategori_kartu`)
    REFERENCES `zone2000`.`m_kategori_kartu` (`id_kategori_kartu`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zone2000`.`m_tiket`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`m_tiket` (
  `id_tiket` INT(2) NOT NULL AUTO_INCREMENT,
  `kd_tiket` VARCHAR(50) NULL,
  `nama_tiket` VARCHAR(50) NULL,
  PRIMARY KEY (`id_tiket`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zone2000`.`m_hadiah`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`m_hadiah` (
  `id_hadiah` INT(4) NOT NULL AUTO_INCREMENT,
  `kd_hadiah` VARCHAR(50) NULL,
  `nama_hadiah` VARCHAR(250) NULL DEFAULT NULL,
  `harga` INT(8) NULL DEFAULT NULL,
  PRIMARY KEY (`id_hadiah`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `zone2000`.`m_stok_hadiah`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`m_stok_hadiah` (
  `id_stok_hadiah` INT NOT NULL AUTO_INCREMENT,
  `kd_stok_hadiah` VARCHAR(50) NULL,
  `id_hadiah` INT(6) NULL,
  `id_cabang` INT(3) NULL,
  `stok_total` INT NULL DEFAULT 0,
  `stok_gudang` INT NULL DEFAULT 0,
  `stok_mesin` INT NULL DEFAULT 0,
  PRIMARY KEY (`id_stok_hadiah`),
  INDEX `fk_stok_hadiah_hadiah_idx` (`id_hadiah` ASC),
  INDEX `fk_stok_hadiah_cabang_idx` (`id_cabang` ASC),
  CONSTRAINT `fk_stok_hadiah_hadiah`
    FOREIGN KEY (`id_hadiah`)
    REFERENCES `zone2000`.`m_hadiah` (`id_hadiah`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_stok_hadiah_cabang`
    FOREIGN KEY (`id_cabang`)
    REFERENCES `zone2000`.`m_cabang` (`id_cabang`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zone2000`.`m_stok_koin`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`m_stok_koin` (
  `id_stok_hadiah` INT NOT NULL AUTO_INCREMENT,
  `kd_stok_hadiah` VARCHAR(50) NULL,
  `id_koin` INT(2) NULL,
  `id_cabang` INT(3) NULL,
  `stok_total` INT NULL DEFAULT 0,
  `stok_gudang` INT NULL DEFAULT 0,
  `stok_mesin` INT NULL DEFAULT 0,
  PRIMARY KEY (`id_stok_hadiah`),
  INDEX `fk_stok_hadiah_cabang_idx` (`id_cabang` ASC),
  INDEX `fk_stok_koin_koin_idx` (`id_koin` ASC),
  CONSTRAINT `fk_stok_koin_koin`
    FOREIGN KEY (`id_koin`)
    REFERENCES `zone2000`.`m_koin` (`id_koin`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_stok_koin_cabang`
    FOREIGN KEY (`id_cabang`)
    REFERENCES `zone2000`.`m_cabang` (`id_cabang`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zone2000`.`m_stok_tiket`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`m_stok_tiket` (
  `id_stok_tiket` INT NOT NULL AUTO_INCREMENT,
  `kd_stok_tiket` VARCHAR(50) NULL,
  `id_tiket` INT(2) NULL,
  `id_cabang` INT(3) NULL,
  `stok_total` INT NULL DEFAULT 0,
  `stok_gudang` INT NULL,
  `stok_mesin` INT NULL,
  PRIMARY KEY (`id_stok_tiket`),
  INDEX `fk_stok_hadiah_cabang_idx` (`id_cabang` ASC),
  INDEX `fk_stok_tiket_tiket_idx` (`id_tiket` ASC),
  CONSTRAINT `fk_stok_tiket_tiket`
    FOREIGN KEY (`id_tiket`)
    REFERENCES `zone2000`.`m_tiket` (`id_tiket`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_stok_tiket_cabang`
    FOREIGN KEY (`id_cabang`)
    REFERENCES `zone2000`.`m_cabang` (`id_cabang`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zone2000`.`m_stok_kartu`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`m_stok_kartu` (
  `id_stok_hadiah` INT NOT NULL AUTO_INCREMENT,
  `kd_stok_hadiah` VARCHAR(50) NULL,
  `id_kartu` INT(2) NULL,
  `id_cabang` INT(3) NULL,
  `stok_total` INT NULL DEFAULT 0,
  `stok_gudang` INT NULL DEFAULT 0,
  PRIMARY KEY (`id_stok_hadiah`),
  INDEX `fk_stok_kartu_cabang_idx` (`id_cabang` ASC),
  INDEX `fk_stok_kartu_kartu_idx` (`id_kartu` ASC),
  CONSTRAINT `fk_stok_kartu_cabang`
    FOREIGN KEY (`id_cabang`)
    REFERENCES `zone2000`.`m_cabang` (`id_cabang`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_stok_kartu_kartu`
    FOREIGN KEY (`id_kartu`)
    REFERENCES `zone2000`.`m_kartu` (`id_kartu`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zone2000`.`m_stok_sparepart`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`m_stok_sparepart` (
  `id_stok_sparepart` INT NOT NULL AUTO_INCREMENT,
  `kd_stok_sparepart` VARCHAR(50) NULL,
  `id_sparepart` INT(2) NULL,
  `id_cabang` INT(3) NULL,
  `stok_total` INT NULL DEFAULT 0,
  `stok_gudang` INT NULL,
  `stok_mesin` INT NULL,
  PRIMARY KEY (`id_stok_sparepart`),
  INDEX `fk_stok_hadiah_cabang_idx` (`id_cabang` ASC),
  INDEX `fk_stok_sparepart_sparepart_idx` (`id_sparepart` ASC),
  CONSTRAINT `fk_stok_sparepart_sparepart`
    FOREIGN KEY (`id_sparepart`)
    REFERENCES `zone2000`.`m_sparepart` (`id_sparepart`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_stok_sparepart_cabang`
    FOREIGN KEY (`id_cabang`)
    REFERENCES `zone2000`.`m_cabang` (`id_cabang`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zone2000`.`zn_kartu_penjualan`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`zn_kartu_penjualan` (
  `id_kartu_penjualan` INT NOT NULL AUTO_INCREMENT,
  `kd_kartu_penjualan` VARCHAR(50) NULL,
  `nama_pembeli` VARCHAR(50) NULL,
  `harga` INT NULL,
  `tanggal` DATE NULL,
  `id_pegawai` INT(6) NULL,
  `id_kartu` INT(2) NULL,
  `id_cabang` INT(3) NULL,
  PRIMARY KEY (`id_kartu_penjualan`),
  INDEX `fk_kartu_penjualan_cabang_idx` (`id_cabang` ASC),
  INDEX `fk_kartu_penjualan_kartu_idx` (`id_kartu` ASC),
  INDEX `fk_kartu_penjualan_pegawai_idx` (`id_pegawai` ASC),
  CONSTRAINT `fk_kartu_penjualan_kartu`
    FOREIGN KEY (`id_kartu`)
    REFERENCES `zone2000`.`m_kartu` (`id_kartu`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_kartu_penjualan_cabang`
    FOREIGN KEY (`id_cabang`)
    REFERENCES `zone2000`.`m_cabang` (`id_cabang`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_kartu_penjualan_pegawai`
    FOREIGN KEY (`id_pegawai`)
    REFERENCES `zone2000`.`m_pegawai` (`id_pegawai`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zone2000`.`zn_kartu_topup`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`zn_kartu_topup` (
  `id_kartu_topup` INT NOT NULL AUTO_INCREMENT,
  `kd_kartu_topup` VARCHAR(50) NULL,
  `id_kartu` INT(2) NULL,
  `id_cabang` INT(3) NULL,
  `tanggal` DATE NULL,
  `nominal` INT NULL,
  `id_pegawai` INT NULL,
  PRIMARY KEY (`id_kartu_topup`),
  INDEX `fk_kartu_toptup_kartu_idx` (`id_kartu` ASC),
  INDEX `fk_kartu_topup_cabang_idx` (`id_cabang` ASC),
  INDEX `fk_kartu_topup_pegawai_idx` (`id_pegawai` ASC),
  CONSTRAINT `fk_kartu_toptup_kartu`
    FOREIGN KEY (`id_kartu`)
    REFERENCES `zone2000`.`m_kartu` (`id_kartu`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_kartu_topup_cabang`
    FOREIGN KEY (`id_cabang`)
    REFERENCES `zone2000`.`m_cabang` (`id_cabang`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_kartu_topup_pegawai`
    FOREIGN KEY (`id_pegawai`)
    REFERENCES `zone2000`.`m_pegawai` (`id_pegawai`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zone2000`.`zn_pr_hadiah`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`zn_pr_hadiah` (
  `id_pr` INT NOT NULL AUTO_INCREMENT,
  `kd_pr` VARCHAR(50) NULL,
  `nama_pr` VARCHAR(250) NULL DEFAULT NULL,
  `status_pr` INT(2) NULL DEFAULT NULL COMMENT '0 : waiting aproval\n1 : disetujui\n2 : revisi\n3 : ditolak',
  `id_pengirim` INT(6) NULL DEFAULT NULL COMMENT 'cabang yang meminta PR',
  `id_tujuan` INT(6) NULL COMMENT 'id store pusat yang dituju',
  `keterangan` TEXT NULL DEFAULT NULL,
  `tgl_dibuat` DATETIME NULL DEFAULT NULL COMMENT 'tanggal PR ini dibuat',
  `tgl_disetujui` DATETIME NULL,
  `tgl_ditolak` DATETIME NULL,
  PRIMARY KEY (`id_pr`),
  INDEX `fk_pr_hadiah_pengirim_idx` (`id_pengirim` ASC),
  INDEX `fk_pr_hadiah_penerima_idx` (`id_tujuan` ASC),
  CONSTRAINT `fk_pr_hadiah_pengirim`
    FOREIGN KEY (`id_pengirim`)
    REFERENCES `zone2000`.`m_cabang` (`id_cabang`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pr_hadiah_tujuan`
    FOREIGN KEY (`id_tujuan`)
    REFERENCES `zone2000`.`m_cabang` (`id_cabang`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `zone2000`.`zn_pr_hadiah_history`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`zn_pr_hadiah_history` (
  `id_pr_history` INT NOT NULL AUTO_INCREMENT,
  `tanggal` VARCHAR(45) NULL,
  `status` INT(1) NULL COMMENT '0 : dibuat \n1 : diterima\n2 : revisi',
  `id_pegawai` INT NULL,
  `id_pr` INT NULL,
  PRIMARY KEY (`id_pr_history`),
  INDEX `fk_pr_hadiah_history_idx` (`id_pr` ASC),
  INDEX `fk_pr_hadiah_history_pegawai_idx` (`id_pegawai` ASC),
  CONSTRAINT `fk_pr_hadiah_history`
    FOREIGN KEY (`id_pr`)
    REFERENCES `zone2000`.`zn_pr_hadiah` (`id_pr`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pr_hadiah_history_pegawai`
    FOREIGN KEY (`id_pegawai`)
    REFERENCES `zone2000`.`m_pegawai` (`id_pegawai`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zone2000`.`zn_pr_hadiah_history_detail`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`zn_pr_hadiah_history_detail` (
  `id_pr_history_detail` INT NOT NULL AUTO_INCREMENT,
  `id_pr_history` INT NULL,
  `id_hadiah` INT(6) NULL,
  `jumlah` INT(5) NULL,
  `keterangan` VARCHAR(45) NULL,
  PRIMARY KEY (`id_pr_history_detail`),
  INDEX `fk_pr_hadiah_history_detail_idx` (`id_pr_history` ASC),
  INDEX `fk_pr_hadiah_history_detail_hadiah_idx` (`id_hadiah` ASC),
  CONSTRAINT `fk_pr_hadiah_history_detail`
    FOREIGN KEY (`id_pr_history`)
    REFERENCES `zone2000`.`zn_pr_hadiah_history` (`id_pr_history`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pr_hadiah_history_detail_hadiah`
    FOREIGN KEY (`id_hadiah`)
    REFERENCES `zone2000`.`m_hadiah` (`id_hadiah`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zone2000`.`zn_pr_mesin`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`zn_pr_mesin` (
  `id_pr` INT NOT NULL AUTO_INCREMENT,
  `kd_pr` VARCHAR(50) NULL,
  `nama_pr` VARCHAR(250) NULL DEFAULT NULL,
  `status_pr` INT(2) NULL DEFAULT NULL COMMENT '0 : waiting aproval\n1 : disetujui\n2 : revisi\n3 : ditolak',
  `id_pengirim` INT(6) NULL DEFAULT NULL COMMENT 'cabang yang meminta PR',
  `id_tujuan` INT(6) NULL COMMENT 'id store pusat yang dituju',
  `keterangan` TEXT NULL DEFAULT NULL,
  `tgl_dibuat` DATETIME NULL DEFAULT NULL COMMENT 'tanggal PR ini dibuat',
  `tgl_disetujui` DATETIME NULL,
  `tgl_ditolak` DATETIME NULL,
  PRIMARY KEY (`id_pr`),
  INDEX `fk_pr_hadiah_pengirim_idx` (`id_pengirim` ASC),
  INDEX `fk_pr_hadiah_penerima_idx` (`id_tujuan` ASC),
  CONSTRAINT `fk_pr_mesin_pengirim`
    FOREIGN KEY (`id_pengirim`)
    REFERENCES `zone2000`.`m_cabang` (`id_cabang`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pr_mesin_tujuan`
    FOREIGN KEY (`id_tujuan`)
    REFERENCES `zone2000`.`m_cabang` (`id_cabang`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `zone2000`.`zn_pr_mesin_history`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`zn_pr_mesin_history` (
  `id_pr_history` INT NOT NULL AUTO_INCREMENT,
  `tanggal` VARCHAR(45) NULL,
  `status` INT(1) NULL COMMENT '0 : dibuat \n1 : diterima\n2 : revisi',
  `id_pegawai` INT NULL,
  `id_pr` INT NULL,
  PRIMARY KEY (`id_pr_history`),
  INDEX `fk_pr_hadiah_history_pegawai_idx` (`id_pegawai` ASC),
  INDEX `fk_pr_mesin_history_idx` (`id_pr` ASC),
  CONSTRAINT `fk_pr_mesin_history`
    FOREIGN KEY (`id_pr`)
    REFERENCES `zone2000`.`zn_pr_mesin` (`id_pr`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pr_mesin_history_pegawai`
    FOREIGN KEY (`id_pegawai`)
    REFERENCES `zone2000`.`m_pegawai` (`id_pegawai`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zone2000`.`zn_pr_mesin_history_detail`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`zn_pr_mesin_history_detail` (
  `id_pr_history_detail` INT NOT NULL AUTO_INCREMENT,
  `id_pr_history` INT NULL,
  `id_jenis_mesin` INT(6) NULL,
  `jumlah` INT(5) NULL,
  `keterangan` VARCHAR(45) NULL,
  PRIMARY KEY (`id_pr_history_detail`),
  INDEX `fk_pr_hadiah_history_detail_idx` (`id_pr_history` ASC),
  INDEX `fk_pr_mesin_history_detail_jenis_mein_idx` (`id_jenis_mesin` ASC),
  CONSTRAINT `fk_pr_mesin_history_detail`
    FOREIGN KEY (`id_pr_history`)
    REFERENCES `zone2000`.`zn_pr_mesin_history` (`id_pr_history`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pr_mesin_history_detail_jenis_mein`
    FOREIGN KEY (`id_jenis_mesin`)
    REFERENCES `zone2000`.`m_jenis_mesin` (`id_jenis_mesin`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zone2000`.`zn_pr_sparepart`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`zn_pr_sparepart` (
  `id_pr` INT NOT NULL AUTO_INCREMENT,
  `kd_pr` VARCHAR(50) NULL,
  `nama_pr` VARCHAR(250) NULL DEFAULT NULL,
  `status_pr` INT(2) NULL DEFAULT NULL COMMENT '0 : waiting aproval\n1 : disetujui\n2 : revisi\n3 : ditolak',
  `id_pengirim` INT(6) NULL DEFAULT NULL COMMENT 'cabang yang meminta PR',
  `id_tujuan` INT(6) NULL COMMENT 'id store pusat yang dituju',
  `keterangan` TEXT NULL DEFAULT NULL,
  `tgl_dibuat` DATETIME NULL DEFAULT NULL COMMENT 'tanggal PR ini dibuat',
  `tgl_disetujui` DATETIME NULL,
  `tgl_ditolak` DATETIME NULL,
  `id_mesin_rusak` INT NULL,
  PRIMARY KEY (`id_pr`),
  INDEX `fk_pr_hadiah_pengirim_idx` (`id_pengirim` ASC),
  INDEX `fk_pr_hadiah_penerima_idx` (`id_tujuan` ASC),
  INDEX `fk_pr_sparepart_mesin_kerusakan_idx` (`id_mesin_rusak` ASC),
  CONSTRAINT `fk_pr_sparepart_pengirim`
    FOREIGN KEY (`id_pengirim`)
    REFERENCES `zone2000`.`m_cabang` (`id_cabang`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pr_sparepart_tujuan`
    FOREIGN KEY (`id_tujuan`)
    REFERENCES `zone2000`.`m_cabang` (`id_cabang`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pr_sparepart_mesin_kerusakan`
    FOREIGN KEY (`id_mesin_rusak`)
    REFERENCES `zone2000`.`zn_mesin_kerusakan` (`id_mesin_rusak`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `zone2000`.`zn_pr_sparepart_history`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`zn_pr_sparepart_history` (
  `id_pr_history` INT NOT NULL AUTO_INCREMENT,
  `tanggal` VARCHAR(45) NULL,
  `status` INT(1) NULL COMMENT '0 : dibuat \n1 : diterima\n2 : revisi',
  `id_pegawai` INT NULL,
  `id_pr` INT NULL,
  PRIMARY KEY (`id_pr_history`),
  INDEX `fk_pr_hadiah_history_idx` (`id_pr` ASC),
  INDEX `fk_pr_hadiah_history_pegawai_idx` (`id_pegawai` ASC),
  CONSTRAINT `fk_pr_sparepart_history`
    FOREIGN KEY (`id_pr`)
    REFERENCES `zone2000`.`zn_pr_sparepart` (`id_pr`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pr_sparepart_history_pegawai`
    FOREIGN KEY (`id_pegawai`)
    REFERENCES `zone2000`.`m_pegawai` (`id_pegawai`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zone2000`.`zn_pr_sparepart_history_detail`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`zn_pr_sparepart_history_detail` (
  `id_pr_history_detail` INT NOT NULL AUTO_INCREMENT,
  `id_pr_history` INT NULL,
  `id_sparepart` INT(6) NULL,
  `jumlah` INT(5) NULL,
  `keterangan` VARCHAR(45) NULL,
  PRIMARY KEY (`id_pr_history_detail`),
  INDEX `fk_pr_hadiah_history_detail_idx` (`id_pr_history` ASC),
  INDEX `fk_pr_sparepart_history_detail_sparepart_idx` (`id_sparepart` ASC),
  CONSTRAINT `fk_pr_sparepart_history_detail`
    FOREIGN KEY (`id_pr_history`)
    REFERENCES `zone2000`.`zn_pr_sparepart_history` (`id_pr_history`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pr_sparepart_history_detail_sparepart`
    FOREIGN KEY (`id_sparepart`)
    REFERENCES `zone2000`.`m_sparepart` (`id_sparepart`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zone2000`.`zn_service`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`zn_service` (
  `id_service` INT NOT NULL AUTO_INCREMENT,
  `id_mesin_rusak` INT NULL,
  `status` INT(1) NULL COMMENT '0 : waiting to confim\n1 : diterima',
  `tgl_dibuat` DATETIME NULL,
  `tgl_diterima` DATETIME NULL,
  `id_pegawai` INT(6) NULL COMMENT 'pegawai yang ditugaskan untuk melakukan service\njika sudah keluar order nya',
  PRIMARY KEY (`id_service`),
  INDEX `fk_service_mesin_rusak_idx` (`id_mesin_rusak` ASC),
  CONSTRAINT `fk_service_mesin_rusak`
    FOREIGN KEY (`id_mesin_rusak`)
    REFERENCES `zone2000`.`zn_mesin_kerusakan` (`id_mesin_rusak`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zone2000`.`zn_pembelian_mesin`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`zn_pembelian_mesin` (
  `id_pembelian` INT NOT NULL AUTO_INCREMENT,
  `kd_pembelian` VARCHAR(50) NULL,
  `id_cabang` INT(3) NULL,
  `id_supplier` INT(3) NULL,
  `status` INT(1) NULL COMMENT '0 : menunggu supplier\n1 : done\n',
  `status_bayar` INT(1) NULL COMMENT '0 : belum lunas\n1 : lunas',
  `tgl_dibuat` DATETIME NULL,
  `tgl_sampai` DATETIME NULL COMMENT 'tanggal barang sampai dikirim oleh supplier',
  PRIMARY KEY (`id_pembelian`),
  INDEX `fk_pembelian_mesin_cabang_idx` (`id_cabang` ASC),
  INDEX `fk_pembelian_mesin_supplier_idx` (`id_supplier` ASC),
  CONSTRAINT `fk_pembelian_mesin_cabang`
    FOREIGN KEY (`id_cabang`)
    REFERENCES `zone2000`.`m_cabang` (`id_cabang`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pembelian_mesin_supplier`
    FOREIGN KEY (`id_supplier`)
    REFERENCES `zone2000`.`m_supplier` (`id_supplier`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zone2000`.`zn_pembelian_mesin_detail`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`zn_pembelian_mesin_detail` (
  `id_pembelian_detail` INT NOT NULL AUTO_INCREMENT,
  `kd_pembelian_detail` VARCHAR(50) NULL,
  `id_pembelian` INT NULL,
  `id_mesin` INT(5) NULL,
  `jumlah` INT(3) NULL,
  `status` INT(1) NULL COMMENT 'status barang\n0 : rusak\n1 : ok \n2 : retur\ndsb',
  `harga` BIGINT NULL,
  PRIMARY KEY (`id_pembelian_detail`),
  INDEX `fk_pembelian_mesin_detail_idx` (`id_pembelian` ASC),
  INDEX `fk_pembelian_mesin_detail_mesin_idx` (`id_mesin` ASC),
  CONSTRAINT `fk_pembelian_mesin_detail`
    FOREIGN KEY (`id_pembelian`)
    REFERENCES `zone2000`.`zn_pembelian_mesin` (`id_pembelian`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pembelian_mesin_detail_mesin`
    FOREIGN KEY (`id_mesin`)
    REFERENCES `zone2000`.`m_mesin` (`id_mesin`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zone2000`.`zn_pembelian_hadiah`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`zn_pembelian_hadiah` (
  `id_pembelian` INT NOT NULL AUTO_INCREMENT,
  `kd_pembelian` VARCHAR(50) NULL,
  `id_cabang` INT(3) NULL,
  `id_supplier` INT(3) NULL,
  `status` INT(1) NULL COMMENT '0 : menunggu supplier\n1 : done\n',
  `status_bayar` INT(1) NULL COMMENT '0 : belum lunas\n1 : lunas',
  `tgl_dibuat` DATETIME NULL,
  `tgl_sampai` DATETIME NULL COMMENT 'tanggal barang sampai dikirim oleh supplier',
  PRIMARY KEY (`id_pembelian`),
  INDEX `fk_pembelian_mesin_cabang_idx` (`id_cabang` ASC),
  INDEX `fk_pembelian_mesin_supplier_idx` (`id_supplier` ASC),
  CONSTRAINT `fk_pembelian_hadiah_cabang`
    FOREIGN KEY (`id_cabang`)
    REFERENCES `zone2000`.`m_cabang` (`id_cabang`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pembelian_hadiah_supplier`
    FOREIGN KEY (`id_supplier`)
    REFERENCES `zone2000`.`m_supplier` (`id_supplier`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zone2000`.`zn_pembelian_hadiah_detail`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`zn_pembelian_hadiah_detail` (
  `id_pembelian_detail` INT NOT NULL AUTO_INCREMENT,
  `kd_pembelian_detail` VARCHAR(50) NULL,
  `id_pembelian` INT NULL,
  `id_hadiah` INT(5) NULL,
  `jumlah` INT(6) NULL,
  `status` INT(1) NULL COMMENT 'status barang\n0 : rusak\n1 : ok \n2 : retur\ndsb',
  `harga` BIGINT NULL,
  PRIMARY KEY (`id_pembelian_detail`),
  INDEX `fk_pembelian_mesin_detail_idx` (`id_pembelian` ASC),
  INDEX `fk_pembelian_hadiah_detail_hadiah_idx` (`id_hadiah` ASC),
  CONSTRAINT `fk_pembelian_hadiah_detail`
    FOREIGN KEY (`id_pembelian`)
    REFERENCES `zone2000`.`zn_pembelian_hadiah` (`id_pembelian`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pembelian_hadiah_detail_hadiah`
    FOREIGN KEY (`id_hadiah`)
    REFERENCES `zone2000`.`m_hadiah` (`id_hadiah`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zone2000`.`zn_pembelian_sparepart`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`zn_pembelian_sparepart` (
  `id_pembelian` INT NOT NULL AUTO_INCREMENT,
  `kd_pembelian` VARCHAR(50) NULL,
  `id_cabang` INT(3) NULL,
  `id_supplier` INT(3) NULL,
  `status` INT(1) NULL COMMENT '0 : menunggu supplier\n1 : done\n',
  `status_bayar` INT(1) NULL COMMENT '0 : belum lunas\n1 : lunas',
  `tgl_dibuat` DATETIME NULL,
  `tgl_sampai` DATETIME NULL COMMENT 'tanggal barang sampai dikirim oleh supplier',
  PRIMARY KEY (`id_pembelian`),
  INDEX `fk_pembelian_mesin_cabang_idx` (`id_cabang` ASC),
  INDEX `fk_pembelian_mesin_supplier_idx` (`id_supplier` ASC),
  CONSTRAINT `fk_pembelian_sparepart_cabang`
    FOREIGN KEY (`id_cabang`)
    REFERENCES `zone2000`.`m_cabang` (`id_cabang`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pembelian_sparepart_supplier`
    FOREIGN KEY (`id_supplier`)
    REFERENCES `zone2000`.`m_supplier` (`id_supplier`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zone2000`.`zn_pembelian_sparepart_detail`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`zn_pembelian_sparepart_detail` (
  `id_pembelian_detail` INT NOT NULL AUTO_INCREMENT,
  `kd_pembelian_detail` VARCHAR(50) NULL,
  `id_pembelian` INT NULL,
  `id_sparepart` INT(5) NULL,
  `jumlah` INT(6) NULL,
  `status` INT(1) NULL COMMENT 'status barang\n0 : rusak\n1 : ok \n2 : retur\ndsb',
  `harga` BIGINT NULL,
  PRIMARY KEY (`id_pembelian_detail`),
  INDEX `fk_pembelian_mesin_detail_idx` (`id_pembelian` ASC),
  INDEX `fk_pembelian_sparepart_detail_sparepart_idx` (`id_sparepart` ASC),
  CONSTRAINT `fk_pembelian_sparepart_detail`
    FOREIGN KEY (`id_pembelian`)
    REFERENCES `zone2000`.`zn_pembelian_sparepart` (`id_pembelian`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pembelian_sparepart_detail_sparepart`
    FOREIGN KEY (`id_sparepart`)
    REFERENCES `zone2000`.`m_sparepart` (`id_sparepart`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zone2000`.`zn_pembelian_kartu`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`zn_pembelian_kartu` (
  `id_pembelian` INT NOT NULL AUTO_INCREMENT,
  `kd_pembelian` VARCHAR(50) NULL,
  `id_cabang` INT(3) NULL,
  `id_supplier` INT(3) NULL,
  `status` INT(1) NULL COMMENT '0 : menunggu supplier\n1 : done\n',
  `status_bayar` INT(1) NULL COMMENT '0 : belum lunas\n1 : lunas',
  `tgl_dibuat` DATETIME NULL,
  `tgl_sampai` DATETIME NULL COMMENT 'tanggal barang sampai dikirim oleh supplier',
  PRIMARY KEY (`id_pembelian`),
  INDEX `fk_pembelian_mesin_cabang_idx` (`id_cabang` ASC),
  INDEX `fk_pembelian_mesin_supplier_idx` (`id_supplier` ASC),
  CONSTRAINT `fk_pembelian_kartu_cabang`
    FOREIGN KEY (`id_cabang`)
    REFERENCES `zone2000`.`m_cabang` (`id_cabang`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pembelian_kartu_supplier`
    FOREIGN KEY (`id_supplier`)
    REFERENCES `zone2000`.`m_supplier` (`id_supplier`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zone2000`.`zn_pembelian_kartu_detail`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`zn_pembelian_kartu_detail` (
  `id_pembelian_detail` INT NOT NULL AUTO_INCREMENT,
  `kd_pembelian_detail` VARCHAR(50) NULL,
  `id_pembelian` INT NULL,
  `id_kartu` INT(5) NULL,
  `jumlah` INT(6) NULL,
  `status` INT(1) NULL COMMENT 'status barang\n0 : rusak\n1 : ok \n2 : retur\ndsb',
  `harga` BIGINT NULL,
  PRIMARY KEY (`id_pembelian_detail`),
  INDEX `fk_pembelian_mesin_detail_idx` (`id_pembelian` ASC),
  INDEX `fk_pembelian_kartu_detail_kartu_idx` (`id_kartu` ASC),
  CONSTRAINT `fk_pembelian_kartu_detail`
    FOREIGN KEY (`id_pembelian`)
    REFERENCES `zone2000`.`zn_pembelian_kartu` (`id_pembelian`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pembelian_kartu_detail_kartu`
    FOREIGN KEY (`id_kartu`)
    REFERENCES `zone2000`.`m_kartu` (`id_kartu`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zone2000`.`zn_pembelian_koin`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`zn_pembelian_koin` (
  `id_pembelian` INT NOT NULL AUTO_INCREMENT,
  `kd_pembelian` VARCHAR(50) NULL,
  `id_cabang` INT(3) NULL,
  `id_supplier` INT(3) NULL,
  `status` INT(1) NULL COMMENT '0 : menunggu supplier\n1 : done\n',
  `status_bayar` INT(1) NULL COMMENT '0 : belum lunas\n1 : lunas',
  `tgl_dibuat` DATETIME NULL,
  `tgl_sampai` DATETIME NULL COMMENT 'tanggal barang sampai dikirim oleh supplier',
  PRIMARY KEY (`id_pembelian`),
  INDEX `fk_pembelian_mesin_cabang_idx` (`id_cabang` ASC),
  INDEX `fk_pembelian_mesin_supplier_idx` (`id_supplier` ASC),
  CONSTRAINT `fk_pembelian_koin_cabang`
    FOREIGN KEY (`id_cabang`)
    REFERENCES `zone2000`.`m_cabang` (`id_cabang`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pembelian_koin_supplier`
    FOREIGN KEY (`id_supplier`)
    REFERENCES `zone2000`.`m_supplier` (`id_supplier`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zone2000`.`zn_pembelian_koin_detail`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`zn_pembelian_koin_detail` (
  `id_pembelian_detail` INT NOT NULL AUTO_INCREMENT,
  `kd_pembelian_detail` VARCHAR(50) NULL,
  `id_pembelian` INT NULL,
  `id_koin` INT(5) NULL,
  `jumlah` INT(6) NULL,
  `status` INT(1) NULL COMMENT 'status barang\n0 : rusak\n1 : ok \n2 : retur\ndsb',
  `harga` BIGINT NULL,
  PRIMARY KEY (`id_pembelian_detail`),
  INDEX `fk_pembelian_mesin_detail_idx` (`id_pembelian` ASC),
  INDEX `fk_pembelian_koin_detail_koin_idx` (`id_koin` ASC),
  CONSTRAINT `fk_pembelian_koin_detail`
    FOREIGN KEY (`id_pembelian`)
    REFERENCES `zone2000`.`zn_pembelian_koin` (`id_pembelian`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pembelian_koin_detail_koin`
    FOREIGN KEY (`id_koin`)
    REFERENCES `zone2000`.`m_koin` (`id_koin`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zone2000`.`zn_pembelian_tiket`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`zn_pembelian_tiket` (
  `id_pembelian` INT NOT NULL AUTO_INCREMENT,
  `kd_pembelian` VARCHAR(50) NULL,
  `id_cabang` INT(3) NULL,
  `id_supplier` INT(3) NULL,
  `status` INT(1) NULL COMMENT '0 : menunggu supplier\n1 : done\n',
  `status_bayar` INT(1) NULL COMMENT '0 : belum lunas\n1 : lunas',
  `tgl_dibuat` DATETIME NULL,
  `tgl_sampai` DATETIME NULL COMMENT 'tanggal barang sampai dikirim oleh supplier',
  PRIMARY KEY (`id_pembelian`),
  INDEX `fk_pembelian_mesin_cabang_idx` (`id_cabang` ASC),
  INDEX `fk_pembelian_mesin_supplier_idx` (`id_supplier` ASC),
  CONSTRAINT `fk_pembelian_tiket_cabang`
    FOREIGN KEY (`id_cabang`)
    REFERENCES `zone2000`.`m_cabang` (`id_cabang`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pembelian_tiket_supplier`
    FOREIGN KEY (`id_supplier`)
    REFERENCES `zone2000`.`m_supplier` (`id_supplier`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zone2000`.`zn_pembelian_tiket_detail`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`zn_pembelian_tiket_detail` (
  `id_pembelian_detail` INT NOT NULL AUTO_INCREMENT,
  `kd_pembelian_detail` VARCHAR(50) NULL,
  `id_pembelian` INT NULL,
  `id_tiket` INT(2) NULL,
  `jumlah` INT(6) NULL,
  `status` INT(1) NULL COMMENT 'status barang\n0 : rusak\n1 : ok \n2 : retur\ndsb',
  `harga` BIGINT NULL,
  PRIMARY KEY (`id_pembelian_detail`),
  INDEX `fk_pembelian_mesin_detail_idx` (`id_pembelian` ASC),
  INDEX `fk_pembelian_tiket_detail_tiket_idx` (`id_tiket` ASC),
  CONSTRAINT `fk_pembelian_tiket_detail`
    FOREIGN KEY (`id_pembelian`)
    REFERENCES `zone2000`.`zn_pembelian_tiket` (`id_pembelian`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pembelian_tiket_detail_tiket`
    FOREIGN KEY (`id_tiket`)
    REFERENCES `zone2000`.`m_tiket` (`id_tiket`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

USE `zone2000` ;

-- -----------------------------------------------------
-- Table `zone2000`.`m_cabang_setting`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`m_cabang_setting` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `id_cabang` INT(3) NOT NULL,
  `merchandise_ratio` INT(5) NOT NULL,
  `store_area` INT(5) NOT NULL,
  `rent_charge` INT(11) NOT NULL,
  `service_charge` INT(11) NOT NULL,
  `store_owner` VARCHAR(50) NULL DEFAULT NULL,
  `coin_regular` INT(11) NOT NULL,
  `coin_acrylic` INT(11) NOT NULL,
  `wristband` INT(11) NOT NULL,
  `service_charge_total` INT(11) NOT NULL,
  `budget` INT(11) NOT NULL,
  `target` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_cabang_setting_cabang_idx` (`id_cabang` ASC),
  CONSTRAINT `fk_cabang_setting_cabang`
    FOREIGN KEY (`id_cabang`)
    REFERENCES `zone2000`.`m_cabang` (`id_cabang`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `zone2000`.`m_grup_menu`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`m_grup_menu` (
  `kd_grup_menu` INT UNSIGNED NOT NULL,
  `nama_grup_menu` VARCHAR(50) NULL DEFAULT NULL,
  `urutan` SMALLINT(6) NULL,
  `icon` VARCHAR(20) NULL DEFAULT NULL,
  PRIMARY KEY (`kd_grup_menu`),
  UNIQUE INDEX `kd_grup_menu` (`kd_grup_menu` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 11
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `zone2000`.`m_level`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`m_level` (
  `id_level` INT(3) NOT NULL AUTO_INCREMENT,
  `nama_level` VARCHAR(250) NULL DEFAULT NULL,
  `kd_level` VARCHAR(50) NULL DEFAULT NULL,
  PRIMARY KEY (`id_level`))
ENGINE = MyISAM
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `zone2000`.`m_menu`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`m_menu` (
  `kd_menu` INT(11) NOT NULL AUTO_INCREMENT,
  `kd_grup_menu` INT(11) UNSIGNED NULL DEFAULT NULL,
  `nama_menu` VARCHAR(50) NULL,
  `deskripsi` VARCHAR(50) NULL DEFAULT NULL,
  `menu_level` SMALLINT(6) NULL DEFAULT NULL,
  `menu_urutan` SMALLINT(6) NULL DEFAULT NULL,
  `url` VARCHAR(100) NULL DEFAULT NULL,
  `child_grup` SMALLINT(6) NULL DEFAULT NULL,
  `kd_parent` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`kd_menu`),
  INDEX `fk_menu_group_menu_idx` (`kd_grup_menu` ASC),
  CONSTRAINT `fk_menu_group_menu`
    FOREIGN KEY (`kd_grup_menu`)
    REFERENCES `zone2000`.`m_grup_menu` (`kd_grup_menu`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 67
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `zone2000`.`m_roles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`m_roles` (
  `roles_id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `menu_id` INT(11) NULL DEFAULT NULL,
  `grup_id` INT(11) UNSIGNED NULL DEFAULT NULL,
  `is_view` VARCHAR(1) NULL DEFAULT NULL,
  `is_add` VARCHAR(10) NULL DEFAULT NULL,
  `is_edit` VARCHAR(1) NULL DEFAULT NULL,
  `is_delete` VARCHAR(1) NULL DEFAULT NULL,
  `is_approve` VARCHAR(1) NULL DEFAULT NULL,
  `is_import` VARCHAR(1) NULL DEFAULT NULL,
  `is_print` VARCHAR(1) NULL DEFAULT NULL,
  `act` VARCHAR(1) NULL DEFAULT NULL,
  PRIMARY KEY (`roles_id`),
  UNIQUE INDEX `roles_id` (`roles_id` ASC),
  INDEX `fk_roles_group_idx` (`grup_id` ASC),
  CONSTRAINT `fk_roles_group`
    FOREIGN KEY (`grup_id`)
    REFERENCES `zone2000`.`m_grup` (`grup_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 716
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `zone2000`.`m_setting_hadiah`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`m_setting_hadiah` (
  `id_setting_hadiah` INT(4) NOT NULL AUTO_INCREMENT,
  `id_lokasi` INT(3) NULL DEFAULT NULL,
  `id_hadiah` INT(3) NULL DEFAULT NULL,
  `jumlah_tiket` INT(10) NULL DEFAULT NULL,
  PRIMARY KEY (`id_setting_hadiah`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `zone2000`.`zn_ci_sessions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`zn_ci_sessions` (
  `session_id` VARCHAR(40) NOT NULL,
  `ip_address` VARCHAR(50) NOT NULL,
  `last_activity` BIGINT(20) NOT NULL,
  `user_data` TEXT NOT NULL,
  `user_agent` VARCHAR(250) NOT NULL,
  PRIMARY KEY (`session_id`))
ENGINE = MyISAM
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `zone2000`.`zn_hadiah_keluar`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`zn_hadiah_keluar` (
  `id_hadiah_keluar` INT(8) NOT NULL AUTO_INCREMENT,
  `id_hadiah` INT(6) NULL,
  `id_lokasi` INT(6) NULL DEFAULT NULL,
  `jumlah` INT(4) NULL DEFAULT NULL,
  `tanggal` DATE NULL DEFAULT NULL,
  PRIMARY KEY (`id_hadiah_keluar`))
ENGINE = MyISAM
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `zone2000`.`zn_log`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`zn_log` (
  `nik` VARCHAR(15) NULL DEFAULT NULL,
  `username` VARCHAR(30) NULL DEFAULT NULL,
  `tgl_akses` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `activity` VARCHAR(150) NULL DEFAULT NULL,
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  UNIQUE INDEX `id` (`id` ASC))
ENGINE = MyISAM
AUTO_INCREMENT = 24198
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `zone2000`.`zn_mutasi_mesin`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`zn_mutasi_mesin` (
  `id_mutasi_mesin` INT(6) NULL DEFAULT NULL,
  `id_mesin` INT(6) NULL DEFAULT NULL,
  `id_lokasi_asal` INT(6) NULL DEFAULT NULL,
  `tanggal` DATE NULL DEFAULT NULL,
  `id_status_mutasi_mesin` INT(3) NULL DEFAULT NULL,
  `id_lokasi_tujuan` INT(6) NULL DEFAULT NULL)
ENGINE = MyISAM
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `zone2000`.`zn_po_hadiah`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`zn_po_hadiah` (
  `id_po` INT NOT NULL AUTO_INCREMENT,
  `kd_po` VARCHAR(250) NOT NULL,
  `nama_po` VARCHAR(250) NOT NULL,
  `id_pengirim` INT(6) NULL,
  `id_tujuan` INT(6) NULL,
  `status_po` INT(1) NULL COMMENT '0 : waiting dikirim\n1 : sudah dikirim\n2 : sudah diterima',
  `tgl_dibuat` DATETIME NULL COMMENT 'tanggal_dibuat',
  `tgl_dikirim` DATETIME NULL,
  `tgl_diterima` DATETIME NULL,
  `id_pr` INT NULL,
  PRIMARY KEY (`id_po`),
  INDEX `fk_po_hadiah_pr_idx` (`id_pr` ASC),
  INDEX `fk_po_hadiah_pengirim_idx` (`id_pengirim` ASC),
  INDEX `fk_po_hadiah_tujuan_idx` (`id_tujuan` ASC),
  CONSTRAINT `fk_po_hadiah_pr`
    FOREIGN KEY (`id_pr`)
    REFERENCES `zone2000`.`zn_pr_hadiah` (`id_pr`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_po_hadiah_pengirim`
    FOREIGN KEY (`id_pengirim`)
    REFERENCES `zone2000`.`m_cabang` (`id_cabang`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_po_hadiah_tujuan`
    FOREIGN KEY (`id_tujuan`)
    REFERENCES `zone2000`.`m_cabang` (`id_cabang`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `zone2000`.`zn_pr_hadiah_detail`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`zn_pr_hadiah_detail` (
  `id_pr_detail` INT NOT NULL AUTO_INCREMENT,
  `id_pr` INT NULL,
  `id_hadiah` INT(6) NULL,
  `jumlah` INT(6) NULL DEFAULT NULL,
  `keterangan` VARCHAR(45) NULL,
  PRIMARY KEY (`id_pr_detail`),
  INDEX `fk_fr_hadiah_detail_idx` (`id_pr` ASC),
  INDEX `fk_fr_hadiah_hadiah_idx` (`id_hadiah` ASC),
  CONSTRAINT `fk_fr_hadiah_detail`
    FOREIGN KEY (`id_pr`)
    REFERENCES `zone2000`.`zn_pr_hadiah` (`id_pr`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_fr_hadiah_hadiah`
    FOREIGN KEY (`id_hadiah`)
    REFERENCES `zone2000`.`m_hadiah` (`id_hadiah`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `zone2000`.`zn_po_hadiah_detail`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`zn_po_hadiah_detail` (
  `id_po_detail` INT NOT NULL AUTO_INCREMENT,
  `id_po` INT NULL,
  `id_hadiah` INT(6) NULL DEFAULT NULL,
  `jumlah` INT(6) NULL DEFAULT NULL,
  `id_pr_detail` INT NULL,
  `status` INT(1) NULL COMMENT '1 : diterima\n2 : retur\n3 : rusak',
  PRIMARY KEY (`id_po_detail`),
  INDEX `fk_po_hadiah_detail_idx` (`id_po` ASC),
  INDEX `fk_po_hadiah_detail_hadiah_idx` (`id_hadiah` ASC),
  INDEX `fk_po_hadiah_detail_pr_detail_idx` (`id_pr_detail` ASC),
  CONSTRAINT `fk_po_hadiah_detail`
    FOREIGN KEY (`id_po`)
    REFERENCES `zone2000`.`zn_po_hadiah` (`id_po`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_po_hadiah_detail_hadiah`
    FOREIGN KEY (`id_hadiah`)
    REFERENCES `zone2000`.`m_hadiah` (`id_hadiah`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_po_hadiah_detail_pr_detail`
    FOREIGN KEY (`id_pr_detail`)
    REFERENCES `zone2000`.`zn_pr_hadiah_detail` (`id_pr_detail`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `zone2000`.`zn_sparepart_keluar`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`zn_sparepart_keluar` (
  `id_sparepart_masuk` INT(9) NOT NULL AUTO_INCREMENT,
  `id_lokasi` INT(3) NULL,
  `id_sparepart` INT(3) NULL DEFAULT NULL,
  `jumlah` INT(10) NULL DEFAULT NULL,
  `tgl` DATE NULL DEFAULT NULL,
  `keterangan` TEXT NULL DEFAULT NULL,
  `id_service` INT(6) NULL DEFAULT NULL,
  PRIMARY KEY (`id_sparepart_masuk`))
ENGINE = MyISAM
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `zone2000`.`zn_transaksi_mesin`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`zn_transaksi_mesin` (
  `id_transaksi_mesin` INT(20) NOT NULL AUTO_INCREMENT,
  `id_mesin` INT(6) NULL DEFAULT NULL,
  `id_lokasi` INT(6) NULL DEFAULT NULL,
  `jumlah_swap` INT(4) NULL DEFAULT NULL,
  `jumlah_tiket` INT(8) NULL DEFAULT NULL,
  `tanggal` DATE NULL DEFAULT NULL,
  `nilai_swap` INT(8) NULL DEFAULT NULL,
  `jumlah_coin` INT(8) NULL DEFAULT NULL,
  PRIMARY KEY (`id_transaksi_mesin`))
ENGINE = MyISAM
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `zone2000`.`zn_pr_mesin_detail`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`zn_pr_mesin_detail` (
  `id_pr_detail` INT NOT NULL AUTO_INCREMENT,
  `id_pr` INT NULL,
  `id_jenis_mesin` INT(6) NULL,
  `jumlah` INT(6) NULL DEFAULT NULL,
  `keterangan` VARCHAR(45) NULL,
  PRIMARY KEY (`id_pr_detail`),
  INDEX `fk_fr_mesin_detail_idx` (`id_pr` ASC),
  INDEX `fk_fr_mesin_jenis_mesin_idx` (`id_jenis_mesin` ASC),
  CONSTRAINT `fk_fr_mesin_detail`
    FOREIGN KEY (`id_pr`)
    REFERENCES `zone2000`.`zn_pr_mesin` (`id_pr`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_fr_mesin_jenis_mesin`
    FOREIGN KEY (`id_jenis_mesin`)
    REFERENCES `zone2000`.`m_jenis_mesin` (`id_jenis_mesin`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `zone2000`.`zn_po_mesin`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`zn_po_mesin` (
  `id_po` INT NOT NULL AUTO_INCREMENT,
  `kd_po` VARCHAR(250) NOT NULL,
  `nama_po` VARCHAR(250) NOT NULL,
  `id_pengirim` INT(6) NULL,
  `id_tujuan` INT(6) NULL,
  `status_po` INT(1) NULL COMMENT '0 : waiting dikirim\n1 : sudah dikirim\n2 : sudah diterima',
  `tgl_dibuat` DATETIME NULL COMMENT 'tanggal_dibuat',
  `tgl_dikirim` DATETIME NULL,
  `tgl_diterima` DATETIME NULL,
  `id_pr` INT NULL,
  PRIMARY KEY (`id_po`),
  INDEX `fk_po_hadiah_pengirim_idx` (`id_pengirim` ASC),
  INDEX `fk_po_hadiah_tujuan_idx` (`id_tujuan` ASC),
  INDEX `fk_po_mesin_pr_idx` (`id_pr` ASC),
  CONSTRAINT `fk_po_mesin_pr`
    FOREIGN KEY (`id_pr`)
    REFERENCES `zone2000`.`zn_pr_mesin` (`id_pr`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_po_mesin_pengirim`
    FOREIGN KEY (`id_pengirim`)
    REFERENCES `zone2000`.`m_cabang` (`id_cabang`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_po_mesin_tujuan`
    FOREIGN KEY (`id_tujuan`)
    REFERENCES `zone2000`.`m_cabang` (`id_cabang`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `zone2000`.`zn_po_mesin_detail`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`zn_po_mesin_detail` (
  `id_po_detail` INT NOT NULL AUTO_INCREMENT,
  `id_po` INT NULL,
  `id_mesin` INT(6) NULL,
  `jumlah` INT(1) NULL DEFAULT 1,
  `id_pr_detail` INT NULL,
  `status` INT(1) NULL COMMENT '1 : diterima\n2 : retur\n3 : rusak',
  PRIMARY KEY (`id_po_detail`),
  INDEX `fk_po_hadiah_detail_idx` (`id_po` ASC),
  INDEX `fk_po_mesin_detail_pr_detail_idx` (`id_pr_detail` ASC),
  INDEX `fk_po_mesin_detail_mesin_idx` (`id_mesin` ASC),
  CONSTRAINT `fk_po_mesin_detail`
    FOREIGN KEY (`id_po`)
    REFERENCES `zone2000`.`zn_po_mesin` (`id_po`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_po_mesin_detail_mesin`
    FOREIGN KEY (`id_mesin`)
    REFERENCES `zone2000`.`m_mesin` (`id_mesin`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_po_mesin_detail_pr_detail`
    FOREIGN KEY (`id_pr_detail`)
    REFERENCES `zone2000`.`zn_pr_mesin_detail` (`id_pr_detail`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `zone2000`.`zn_po_sparepart`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`zn_po_sparepart` (
  `id_po` INT NOT NULL AUTO_INCREMENT,
  `kd_po` VARCHAR(250) NOT NULL,
  `nama_po` VARCHAR(250) NOT NULL,
  `id_pengirim` INT(6) NULL,
  `id_tujuan` INT(6) NULL,
  `status_po` INT(1) NULL COMMENT '0 : waiting dikirim\n1 : sudah dikirim\n2 : sudah diterima',
  `tgl_dibuat` DATETIME NULL COMMENT 'tanggal_dibuat',
  `tgl_dikirim` DATETIME NULL,
  `tgl_diterima` DATETIME NULL,
  `id_pr` INT NULL,
  PRIMARY KEY (`id_po`),
  INDEX `fk_po_hadiah_pr_idx` (`id_pr` ASC),
  INDEX `fk_po_hadiah_pengirim_idx` (`id_pengirim` ASC),
  INDEX `fk_po_hadiah_tujuan_idx` (`id_tujuan` ASC),
  CONSTRAINT `fk_po_sparepart_pr`
    FOREIGN KEY (`id_pr`)
    REFERENCES `zone2000`.`zn_pr_sparepart` (`id_pr`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_po_sparepart_pengirim`
    FOREIGN KEY (`id_pengirim`)
    REFERENCES `zone2000`.`m_cabang` (`id_cabang`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_po_sparepart_tujuan`
    FOREIGN KEY (`id_tujuan`)
    REFERENCES `zone2000`.`m_cabang` (`id_cabang`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `zone2000`.`zn_pr_sparepart_detail`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`zn_pr_sparepart_detail` (
  `id_pr_detail` INT NOT NULL AUTO_INCREMENT,
  `id_pr` INT NULL,
  `id_sparepart` INT(6) NULL,
  `jumlah` INT(6) NULL DEFAULT NULL,
  `keterangan` VARCHAR(45) NULL,
  PRIMARY KEY (`id_pr_detail`),
  INDEX `fk_fr_hadiah_detail_idx` (`id_pr` ASC),
  INDEX `fk_fr_sparepart_sparepart_idx` (`id_sparepart` ASC),
  CONSTRAINT `fk_fr_sparepart_detail`
    FOREIGN KEY (`id_pr`)
    REFERENCES `zone2000`.`zn_pr_sparepart` (`id_pr`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_fr_sparepart_sparepart`
    FOREIGN KEY (`id_sparepart`)
    REFERENCES `zone2000`.`m_sparepart` (`id_sparepart`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `zone2000`.`zn_po_sparepart_detail`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`zn_po_sparepart_detail` (
  `id_po_detail` INT NOT NULL AUTO_INCREMENT,
  `id_po` INT NULL,
  `id_sparepart` INT(6) NULL,
  `jumlah` INT(6) NULL DEFAULT NULL,
  `id_pr_detail` INT NULL,
  `status` INT(1) NULL COMMENT '1 : diterima\n2 : retur\n3 : rusak',
  PRIMARY KEY (`id_po_detail`),
  INDEX `fk_po_hadiah_detail_idx` (`id_po` ASC),
  INDEX `fk_po_hadiah_detail_pr_detail_idx` (`id_pr_detail` ASC),
  INDEX `fk_po_sparepart_detail_sparepart_idx` (`id_sparepart` ASC),
  CONSTRAINT `fk_po_sparepart_detail`
    FOREIGN KEY (`id_po`)
    REFERENCES `zone2000`.`zn_po_sparepart` (`id_po`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_po_sparepart_detail_sparepart`
    FOREIGN KEY (`id_sparepart`)
    REFERENCES `zone2000`.`m_sparepart` (`id_sparepart`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_po_sparepart_detail_pr_detail`
    FOREIGN KEY (`id_pr_detail`)
    REFERENCES `zone2000`.`zn_pr_sparepart_detail` (`id_pr_detail`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

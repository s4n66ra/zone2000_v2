ALTER TABLE `m_cabang` ADD `fax` VARCHAR(20) NULL AFTER `telp`;
ALTER TABLE `m_cabang` ADD `date_start` DATE NOT NULL AFTER `fax`;
ALTER TABLE `m_cabang` ADD `id_owner` INT NOT NULL AFTER `id_kepcabang`;

CREATE TABLE IF NOT EXISTS `m_cabang_setting` (
  `id_cabang` int(3) NOT NULL,
  `merchandise_ratio` int(5) NOT NULL,
  `store_area` int(5) NOT NULL,
  `rent_charge` int(11) NOT NULL,
  `service_charge` int(11) NOT NULL,
  `store_owner` varchar(50) NOT NULL,
  `coin_regular` int(11) NOT NULL,
  `coin_acrylic` int(11) NOT NULL,
  `wristband` int(11) NOT NULL,
  `service_charge_total` int(11) NOT NULL,
  `budget` int(11) NOT NULL,
  `target` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `m_cabang_setting` ADD `id` INT NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`id`) ;
ALTER TABLE `m_cabang_setting` CHANGE `store_owner` `store_owner` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL;



CREATE TABLE IF NOT EXISTS `m_negara` (
`id_negara` int(11) NOT NULL,
  `nama_negara` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `m_negara`
 ADD PRIMARY KEY (`id_negara`), ADD UNIQUE KEY `nama_negara` (`nama_negara`);

ALTER TABLE `m_negara`
MODIFY `id_negara` int(11) NOT NULL AUTO_INCREMENT;



ALTER TABLE `m_pulau` ADD `id_negara` INT NOT NULL DEFAULT '1' AFTER `kd_pulau`;
ALTER TABLE `m_area` ADD `id_kota` INT(3) NOT NULL DEFAULT '1';
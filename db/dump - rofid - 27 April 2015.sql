-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 27, 2015 at 09:41 AM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `zone2000`
--
CREATE DATABASE IF NOT EXISTS `zone2000` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `zone2000`;

-- --------------------------------------------------------

--
-- Table structure for table `m_area`
--

CREATE TABLE IF NOT EXISTS `m_area` (
`id_area` int(3) NOT NULL,
  `kd_area` varchar(20) NOT NULL,
  `nama_area` varchar(250) NOT NULL,
  `urutan` int(3) DEFAULT NULL,
  `id_kota` int(5) NOT NULL DEFAULT '1',
  `id_ka_area` int(6) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_area`
--

INSERT INTO `m_area` (`id_area`, `kd_area`, `nama_area`, `urutan`, `id_kota`, `id_ka_area`) VALUES
(1, 'Pas', 'Pasific Place', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_cabang`
--

CREATE TABLE IF NOT EXISTS `m_cabang` (
`id_cabang` int(3) NOT NULL,
  `kd_cabang` varchar(20) NOT NULL,
  `nama_cabang` varchar(250) NOT NULL,
  `urutan` int(3) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(250) NOT NULL,
  `fax` varchar(20) DEFAULT NULL,
  `date_start` date NOT NULL,
  `id_kepcabang` int(6) DEFAULT NULL,
  `id_owner` int(6) NOT NULL,
  `luas` int(6) NOT NULL,
  `pajak_toko` int(8) NOT NULL,
  `id_area` int(6) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_cabang`
--

INSERT INTO `m_cabang` (`id_cabang`, `kd_cabang`, `nama_cabang`, `urutan`, `alamat`, `telp`, `fax`, `date_start`, `id_kepcabang`, `id_owner`, `luas`, `pajak_toko`, `id_area`) VALUES
(1, 'ST111', 'Pluit', 0, 'Pluit', '0122', '123', '2015-04-08', NULL, 2, 0, 0, 1),
(10, 'ST001', 'Kantor Pusat', 0, 'Menteng', '021111', '021333', '2015-04-20', NULL, 2, 0, 0, 1),
(11, 'ST002', 'Gembira Playground', 0, 'Bandung Jalan Sarijadi blok 2 no.30', '+6285729067504', '', '0000-00-00', NULL, 1, 0, 0, 1),
(12, '003', 'Sarinah', 0, 'Sarinah', '021999', '0923', '2015-04-08', NULL, 2, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_cabang_setting`
--

CREATE TABLE IF NOT EXISTS `m_cabang_setting` (
`id` int(11) NOT NULL,
  `id_cabang` int(3) NOT NULL,
  `merchandise_ratio` int(5) NOT NULL,
  `store_area` int(5) NOT NULL,
  `rent_charge` int(11) NOT NULL,
  `service_charge` int(11) NOT NULL,
  `store_owner` varchar(50) DEFAULT NULL,
  `coin_regular` int(11) NOT NULL,
  `coin_acrylic` int(11) NOT NULL,
  `wristband` int(11) NOT NULL,
  `service_charge_total` int(11) NOT NULL,
  `budget` int(11) NOT NULL,
  `target` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_cabang_setting`
--

INSERT INTO `m_cabang_setting` (`id`, `id_cabang`, `merchandise_ratio`, `store_area`, `rent_charge`, `service_charge`, `store_owner`, `coin_regular`, `coin_acrylic`, `wristband`, `service_charge_total`, `budget`, `target`) VALUES
(9, 10, 30, 30, 30, 20, NULL, 20, 20, 20, 0, 200000, 200000),
(10, 11, 200, 200, 200, 100, NULL, 21, 21, 21, 0, 3000, 3000),
(11, 12, 30, 30, 30, 10, NULL, 10, 10, 10, 0, 0, 0),
(12, 1, 30, 30, 30, 30, NULL, 30, 30, 30, 0, 300000, 300000);

-- --------------------------------------------------------

--
-- Table structure for table `m_grup`
--

CREATE TABLE IF NOT EXISTS `m_grup` (
`grup_id` int(10) unsigned NOT NULL,
  `grup_nama` varchar(25) DEFAULT NULL,
  `grup_deskripsi` varchar(50) DEFAULT NULL,
  `otoritas_data` char(1) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_grup`
--

INSERT INTO `m_grup` (`grup_id`, `grup_nama`, `grup_deskripsi`, `otoritas_data`) VALUES
(15, 'Super Admin', 'Super Admin', 'D'),
(16, 'Produksi', 'produksi', '0'),
(17, 'Akuntansi', 'akuntansi', '0'),
(18, 'Direksi', 'Direksi', '0'),
(19, 'Logistik', 'Logistik', '0');

-- --------------------------------------------------------

--
-- Table structure for table `m_grup_menu`
--

CREATE TABLE IF NOT EXISTS `m_grup_menu` (
  `kd_grup_menu` int(10) unsigned NOT NULL,
  `nama_grup_menu` varchar(50) DEFAULT NULL,
  `urutan` smallint(6) DEFAULT NULL,
  `icon` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_grup_menu`
--

INSERT INTO `m_grup_menu` (`kd_grup_menu`, `nama_grup_menu`, `urutan`, `icon`) VALUES
(0, 'Purchase Request', NULL, '0'),
(1, 'Master Data', 11, 'icon-folder-open-alt'),
(2, 'Pengelolaan Pengguna', 12, 'icon-user'),
(10, 'Transaksi', NULL, 'icon-fighter-jet');

-- --------------------------------------------------------

--
-- Table structure for table `m_hadiah`
--

CREATE TABLE IF NOT EXISTS `m_hadiah` (
`id_hadiah` int(4) NOT NULL,
  `kd_hadiah` varchar(50) DEFAULT NULL,
  `nama_hadiah` varchar(250) DEFAULT NULL,
  `harga` int(8) DEFAULT NULL,
  `jumlah_tiket` int(5) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_key` varchar(200) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_hadiah`
--

INSERT INTO `m_hadiah` (`id_hadiah`, `kd_hadiah`, `nama_hadiah`, `harga`, `jumlah_tiket`, `item_id`, `item_key`) VALUES
(1, 'MDS001', 'Boneka Beruang Hitam', 100000, 100, 7, 'barmds001'),
(2, 'MSD002', 'Boneka James', 30000, 20, 8, '092103829'),
(3, 'MSD003', 'Gelang Lucu', 40000, 20, 9, '882898379'),
(4, 'MSD004', 'Topi Bundar', 70000, 23, 10, '9003949');

-- --------------------------------------------------------

--
-- Table structure for table `m_item`
--

CREATE TABLE IF NOT EXISTS `m_item` (
`item_id` int(11) NOT NULL,
  `item_key` varchar(200) DEFAULT NULL,
  `item_name` varchar(50) DEFAULT NULL,
  `itemtype_id` int(11) DEFAULT NULL COMMENT '1 : Mesin\n2 : Merchandise\n3 : Sparepart\n4 : Ticket\n5 : Coin\n6 : swipe card',
  `price` int(11) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_item`
--

INSERT INTO `m_item` (`item_id`, `item_key`, `item_name`, `itemtype_id`, `price`) VALUES
(1, 'ACR', 'Acrylic', 6, 9000),
(2, 'WRY', 'Wristband', 5, 0),
(3, 'REG', 'Regular', 5, 10),
(4, '001', 'Regular Ticket', 4, 0),
(5, '0987609876', 'M001', 1, 500000),
(6, '882763623', 'M002', 1, 0),
(7, 'AAA', 'TICKET REGULAR', 5, 90000),
(8, 'CCC', 'CCC 1', 5, 2000),
(9, '882898379', 'Gelang Lucu', 2, 40000),
(10, '9003949', 'Topi Bundar', 2, 70000),
(11, '8217321097', 'Virtual Cop 2', 7, 3000000),
(12, '22213123', 'SEGA RALLY', 7, 2000000),
(13, '123123123', 'Cruizer Z 2034', 7, 1400000),
(14, '32123213', 'SEGA RALLY OLD', 7, 400000),
(18, 'AAA', 'TICKET REGULAR', 5, 90000),
(22, 'III', 'III', 6, 70000),
(24, '1199281', 'Tombol Kaizer', 3, 90000),
(26, '8829381', 'Peer Tombol', 3, 5000),
(27, '0029930', 'Lampu LED Merah', 3, 90000);

-- --------------------------------------------------------

--
-- Table structure for table `m_item_redemption`
--

CREATE TABLE IF NOT EXISTS `m_item_redemption` (
  `itemredemption_id` int(11) NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL,
  `redemp_qty` int(4) DEFAULT NULL COMMENT 'jumlah yang ditukar',
  `redemp_type` int(1) DEFAULT NULL COMMENT 'redemp type : item ini ditukar dengan apa\n1 : ticket\n2 : ...'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_item_stok`
--

CREATE TABLE IF NOT EXISTS `m_item_stok` (
`itemstok_id` int(11) NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL,
  `stok_total` int(11) DEFAULT NULL COMMENT 'stok total dari awal sampe sekarang',
  `stok_gudang` int(11) NOT NULL DEFAULT '0' COMMENT 'stok gudang di store yang bersangkutan',
  `stok_mesin` int(11) NOT NULL DEFAULT '0' COMMENT 'stok item di semua mesin di store yang bersangkutan',
  `stok_store` int(11) NOT NULL DEFAULT '0' COMMENT 'stok item di store tersebut meliputi stok item yang ada di semua mesin dan stok mesin yang ada di gudang store tsb'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_item_stok`
--

INSERT INTO `m_item_stok` (`itemstok_id`, `item_id`, `store_id`, `stok_total`, `stok_gudang`, `stok_mesin`, `stok_store`) VALUES
(1, 1, 1, 100, 100, 0, 100),
(2, 2, 1, 100, 100, 0, 100),
(3, 3, 1, 100, 100, 0, 100),
(4, 4, 1, 1000, 1000, 0, 1000),
(5, 5, 1, 5, 5, 0, 5),
(6, 6, 1, 5, 5, 0, 5),
(7, 7, 1, 10, 10, 0, 10);

-- --------------------------------------------------------

--
-- Table structure for table `m_item_supplier`
--

CREATE TABLE IF NOT EXISTS `m_item_supplier` (
`itemsupplier_id` int(11) NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `default` int(1) DEFAULT '0' COMMENT '0 : bukan default\n1 : default'
) ENGINE=InnoDB AUTO_INCREMENT=153 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_item_supplier`
--

INSERT INTO `m_item_supplier` (`itemsupplier_id`, `item_id`, `supplier_id`, `default`) VALUES
(1, 1, 9, 0),
(2, 1, 10, 0),
(3, 1, 11, 0),
(4, 1, 12, 0),
(5, 2, 9, 0),
(6, 2, 10, 0),
(7, 2, 11, 0),
(8, 2, 12, 0),
(9, 3, 9, 0),
(10, 3, 10, 0),
(11, 3, 11, 0),
(12, 3, 12, 0),
(13, 4, 9, 0),
(14, 4, 10, 0),
(15, 4, 11, 0),
(16, 4, 12, 0),
(17, 5, 9, 0),
(18, 5, 10, 0),
(19, 5, 11, 0),
(20, 5, 12, 0),
(21, 6, 9, 0),
(22, 6, 10, 0),
(23, 6, 11, 0),
(24, 6, 12, 0),
(25, 7, 9, 0),
(26, 7, 10, 0),
(27, 7, 11, 0),
(28, 7, 12, 0),
(29, 8, 9, 0),
(30, 8, 10, 0),
(31, 8, 11, 0),
(32, 8, 12, 0),
(33, 9, 9, 0),
(34, 9, 10, 0),
(35, 9, 11, 0),
(36, 9, 12, 0),
(37, 10, 9, 0),
(38, 10, 10, 0),
(39, 10, 11, 0),
(40, 10, 12, 0),
(41, 11, 9, 0),
(42, 11, 10, 0),
(43, 11, 11, 0),
(44, 11, 12, 0),
(45, 12, 9, 0),
(46, 12, 10, 0),
(47, 12, 11, 0),
(48, 12, 12, 0),
(49, 13, 9, 0),
(50, 13, 10, 0),
(51, 13, 11, 0),
(52, 13, 12, 0),
(53, 14, 9, 0),
(54, 14, 10, 0),
(55, 14, 11, 0),
(56, 14, 12, 0),
(57, 18, 9, 0),
(58, 18, 10, 0),
(59, 18, 11, 0),
(60, 18, 12, 0),
(61, 22, 9, 0),
(62, 22, 10, 0),
(63, 22, 11, 0),
(64, 22, 12, 0),
(65, 24, 9, 0),
(66, 24, 10, 0),
(67, 24, 11, 0),
(68, 24, 12, 0),
(69, 26, 9, 0),
(70, 26, 10, 0),
(71, 26, 11, 0),
(72, 26, 12, 0),
(73, 27, 9, 0),
(74, 27, 10, 0),
(75, 27, 11, 0),
(76, 27, 12, 0),
(77, 1, 9, 0),
(78, 1, 10, 0),
(79, 1, 11, 0),
(80, 1, 12, 0),
(81, 2, 9, 0),
(82, 2, 10, 0),
(83, 2, 11, 0),
(84, 2, 12, 0),
(85, 3, 9, 0),
(86, 3, 10, 0),
(87, 3, 11, 0),
(88, 3, 12, 0),
(89, 4, 9, 0),
(90, 4, 10, 0),
(91, 4, 11, 0),
(92, 4, 12, 0),
(93, 5, 9, 0),
(94, 5, 10, 0),
(95, 5, 11, 0),
(96, 5, 12, 0),
(97, 6, 9, 0),
(98, 6, 10, 0),
(99, 6, 11, 0),
(100, 6, 12, 0),
(101, 7, 9, 0),
(102, 7, 10, 0),
(103, 7, 11, 0),
(104, 7, 12, 0),
(105, 8, 9, 0),
(106, 8, 10, 0),
(107, 8, 11, 0),
(108, 8, 12, 0),
(109, 9, 9, 0),
(110, 9, 10, 0),
(111, 9, 11, 0),
(112, 9, 12, 0),
(113, 10, 9, 0),
(114, 10, 10, 0),
(115, 10, 11, 0),
(116, 10, 12, 0),
(117, 11, 9, 0),
(118, 11, 10, 0),
(119, 11, 11, 0),
(120, 11, 12, 0),
(121, 12, 9, 0),
(122, 12, 10, 0),
(123, 12, 11, 0),
(124, 12, 12, 0),
(125, 13, 9, 0),
(126, 13, 10, 0),
(127, 13, 11, 0),
(128, 13, 12, 0),
(129, 14, 9, 0),
(130, 14, 10, 0),
(131, 14, 11, 0),
(132, 14, 12, 0),
(133, 18, 9, 0),
(134, 18, 10, 0),
(135, 18, 11, 0),
(136, 18, 12, 0),
(137, 22, 9, 0),
(138, 22, 10, 0),
(139, 22, 11, 0),
(140, 22, 12, 0),
(141, 24, 9, 0),
(142, 24, 10, 0),
(143, 24, 11, 0),
(144, 24, 12, 0),
(145, 26, 9, 0),
(146, 26, 10, 0),
(147, 26, 11, 0),
(148, 26, 12, 0),
(149, 27, 9, 0),
(150, 27, 10, 0),
(151, 27, 11, 0),
(152, 27, 12, 0);

-- --------------------------------------------------------

--
-- Table structure for table `m_item_supplier_default`
--

CREATE TABLE IF NOT EXISTS `m_item_supplier_default` (
`default_id` int(11) NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_item_supplier_default`
--

INSERT INTO `m_item_supplier_default` (`default_id`, `item_id`, `supplier_id`) VALUES
(1, 1, 10),
(2, 2, 11),
(3, 3, 12),
(4, 4, 9),
(5, 5, 10),
(6, 6, 11),
(7, 7, 12),
(8, 8, 9),
(9, 9, 10),
(10, 10, 11),
(11, 11, 12),
(12, 12, 9),
(13, 13, 10),
(14, 14, 11),
(15, 18, 11),
(16, 22, 11),
(17, 24, 9),
(18, 26, 11),
(19, 27, 12),
(20, 1, 2),
(21, 2, 3),
(22, 3, 4),
(23, 4, 1),
(24, 5, 2),
(25, 6, 3),
(26, 7, 4),
(27, 8, 1),
(28, 9, 2),
(29, 10, 3),
(30, 11, 4),
(31, 12, 1),
(32, 13, 2),
(33, 14, 3),
(34, 18, 3),
(35, 22, 3),
(36, 24, 1),
(37, 26, 3),
(38, 27, 4);

-- --------------------------------------------------------

--
-- Table structure for table `m_item_type`
--

CREATE TABLE IF NOT EXISTS `m_item_type` (
`itemtype_id` int(11) NOT NULL,
  `type_code` varchar(10) DEFAULT NULL,
  `type_name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_item_type`
--

INSERT INTO `m_item_type` (`itemtype_id`, `type_code`, `type_name`) VALUES
(1, '8888', 'Machine'),
(2, '8899', 'Marchendise'),
(3, '9998', 'Sparepart'),
(4, '7788', 'Ticket'),
(5, '9989', 'Coin'),
(6, '9980', 'Swipe Card'),
(7, '9883', 'Machine Type');

-- --------------------------------------------------------

--
-- Table structure for table `m_jabatan`
--

CREATE TABLE IF NOT EXISTS `m_jabatan` (
`id_jabatan` int(4) NOT NULL,
  `kd_jabatan` varchar(50) DEFAULT NULL,
  `nama_jabatan` varchar(250) DEFAULT NULL,
  `urutan` int(3) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_jabatan`
--

INSERT INTO `m_jabatan` (`id_jabatan`, `kd_jabatan`, `nama_jabatan`, `urutan`) VALUES
(1, '001', 'Manager', 0);

-- --------------------------------------------------------

--
-- Table structure for table `m_jenis_mesin`
--

CREATE TABLE IF NOT EXISTS `m_jenis_mesin` (
`id_jenis_mesin` int(4) NOT NULL,
  `kd_jenis_mesin` varchar(40) NOT NULL,
  `nama_jenis_mesin` varchar(50) NOT NULL,
  `deskripsi` varchar(100) DEFAULT NULL,
  `thumbnail` varchar(50) DEFAULT NULL,
  `foto` varchar(50) DEFAULT NULL,
  `jenis_koin` int(2) DEFAULT NULL,
  `id_kategori_mesin` int(3) NOT NULL,
  `id_kelas` int(3) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `item_key` varchar(200) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_jenis_mesin`
--

INSERT INTO `m_jenis_mesin` (`id_jenis_mesin`, `kd_jenis_mesin`, `nama_jenis_mesin`, `deskripsi`, `thumbnail`, `foto`, `jenis_koin`, `id_kategori_mesin`, `id_kelas`, `item_id`, `item_key`) VALUES
(1, '001', 'SEGA RALLY', 'game balapan', NULL, NULL, 1, 4, 2, 12, '22213123'),
(2, '002', 'Cruizer Z 2034', 'keren', NULL, NULL, 1, 4, 2, 13, '123123123'),
(3, '000', 'SEGA RALLY OLD', '', NULL, NULL, 1, 4, 2, 14, '32123213'),
(4, 'MCNTP0042', 'Virtual Cop 23', 'tembak2an', NULL, '', 2, 7, 4, 11, '8217321097');

-- --------------------------------------------------------

--
-- Table structure for table `m_kartu`
--

CREATE TABLE IF NOT EXISTS `m_kartu` (
  `id_kartu` int(2) NOT NULL,
  `kd_kartu` varchar(50) DEFAULT NULL,
  `nama_pemilik` varchar(50) DEFAULT NULL,
  `id_cabang` int(3) DEFAULT NULL,
  `id_kategori_kartu` int(3) DEFAULT NULL,
  `item_id` int(11) NOT NULL,
  `item_key` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_kategori_kartu`
--

CREATE TABLE IF NOT EXISTS `m_kategori_kartu` (
`id_kategori_kartu` int(3) NOT NULL,
  `kd_kategori_kartu` varchar(40) NOT NULL,
  `nama_kategori_kartu` varchar(250) NOT NULL,
  `harga` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_kategori_mesin`
--

CREATE TABLE IF NOT EXISTS `m_kategori_mesin` (
`id_kategori_mesin` int(3) NOT NULL,
  `kd_kategori_mesin` varchar(20) NOT NULL,
  `nama_kategori_mesin` varchar(250) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_kategori_mesin`
--

INSERT INTO `m_kategori_mesin` (`id_kategori_mesin`, `kd_kategori_mesin`, `nama_kategori_mesin`) VALUES
(4, '001', 'Mesin Pusher'),
(5, '002', 'Mesin Redemption'),
(6, '003', 'Mesin Vending'),
(7, '004', 'Fixed Game'),
(8, '005', 'Large Fixed Game'),
(9, '006', 'Large Game');

-- --------------------------------------------------------

--
-- Table structure for table `m_kelas`
--

CREATE TABLE IF NOT EXISTS `m_kelas` (
`id_kelas` int(3) NOT NULL,
  `kd_kelas` varchar(40) NOT NULL,
  `nama_kelas` varchar(250) NOT NULL,
  `min_value` int(20) NOT NULL,
  `max_value` int(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_kelas`
--

INSERT INTO `m_kelas` (`id_kelas`, `kd_kelas`, `nama_kelas`, `min_value`, `max_value`) VALUES
(2, 'A', 'Kelas A', 3000001, 999000000),
(3, 'B', 'Kelas B', 2000001, 3000000),
(4, 'C', 'Kelas C', 1000001, 2000000),
(5, 'D', 'Kelas D', 1, 1000000);

-- --------------------------------------------------------

--
-- Table structure for table `m_koin`
--

CREATE TABLE IF NOT EXISTS `m_koin` (
`id_koin` int(2) NOT NULL,
  `kd_koin` varchar(50) DEFAULT NULL,
  `nama_koin` varchar(50) DEFAULT NULL,
  `item_id` int(11) NOT NULL,
  `item_key` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_koin`
--

INSERT INTO `m_koin` (`id_koin`, `kd_koin`, `nama_koin`, `item_id`, `item_key`) VALUES
(1, 'ACR', 'Acrylic', 1, 'ACR'),
(2, 'WRY', 'Wristband', 1, '2222'),
(3, 'REG', 'Regular', 1, '2222'),
(4, 'MMM', 'MMM', 2, ''),
(5, 'DDDD', 'DDD', 1, 'ACR'),
(6, 'SSS', 'SSS', 3, 'REG'),
(8, 'III', 'III', 22, 'III');

-- --------------------------------------------------------

--
-- Table structure for table `m_kota`
--

CREATE TABLE IF NOT EXISTS `m_kota` (
`id_kota` int(5) NOT NULL,
  `nama_kota` varchar(250) DEFAULT NULL,
  `urutan` int(5) DEFAULT NULL,
  `id_provinsi` int(3) DEFAULT NULL,
  `kd_kota` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_kota`
--

INSERT INTO `m_kota` (`id_kota`, `nama_kota`, `urutan`, `id_provinsi`, `kd_kota`) VALUES
(1, 'Jakarta Pusat', 1, 1, 'JAK'),
(2, 'Jakarta Selatan', 2, 1, 'Jaksel');

-- --------------------------------------------------------

--
-- Table structure for table `m_level`
--

CREATE TABLE IF NOT EXISTS `m_level` (
`id_level` int(3) NOT NULL,
  `nama_level` varchar(250) DEFAULT NULL,
  `kd_level` varchar(50) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_level`
--

INSERT INTO `m_level` (`id_level`, `nama_level`, `kd_level`) VALUES
(2, 'Kepala Cabang', 'HO');

-- --------------------------------------------------------

--
-- Table structure for table `m_menu`
--

CREATE TABLE IF NOT EXISTS `m_menu` (
`kd_menu` int(11) NOT NULL,
  `kd_grup_menu` int(11) unsigned DEFAULT NULL,
  `nama_menu` varchar(50) DEFAULT NULL,
  `deskripsi` varchar(50) DEFAULT NULL,
  `menu_level` smallint(6) DEFAULT NULL,
  `menu_urutan` smallint(6) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `child_grup` smallint(6) DEFAULT NULL,
  `kd_parent` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_menu`
--

INSERT INTO `m_menu` (`kd_menu`, `kd_grup_menu`, `nama_menu`, `deskripsi`, `menu_level`, `menu_urutan`, `url`, `child_grup`, `kd_parent`) VALUES
(1, 1, 'Menu Aplikasi', '', NULL, 3, 'menu', NULL, 6),
(3, 1, 'Group Menu', '', NULL, 4, 'menu_group', NULL, 6),
(4, 2, 'Roles', '', NULL, 4, 'roles', NULL, NULL),
(5, 2, 'user', '', NULL, 3, 'user', NULL, NULL),
(6, 1, 'Menu', '', NULL, 5, '', NULL, NULL),
(48, 1, 'Pulau', '', NULL, 6, 'pulau', NULL, NULL),
(49, 1, 'Provinsi', '', NULL, 7, 'provinsi', NULL, NULL),
(50, 1, 'Area', '', NULL, 8, 'area', NULL, NULL),
(51, 1, 'Kategori Mesin', '', NULL, 9, 'kategori_mesin', NULL, NULL),
(52, 1, 'Supplier', '', NULL, 10, 'supplier', NULL, NULL),
(53, 1, 'Kelas', '', NULL, 11, 'kelas', NULL, NULL),
(55, 1, 'Mesin', '', NULL, 13, 'mesin', NULL, NULL),
(56, 1, 'Cabang', '', NULL, 14, 'cabang', NULL, NULL),
(57, 1, 'Jabatan', '', NULL, 15, 'jabatan', NULL, NULL),
(58, 1, 'Pegawai', '', NULL, 16, 'pegawai', NULL, NULL),
(59, 1, 'Level', '', NULL, 17, 'level', NULL, NULL),
(60, 10, 'Purchase Order', '', NULL, 1, 'po', NULL, NULL),
(61, 1, 'Setting Hadiah', '', NULL, 18, 'setting_hadiah', NULL, NULL),
(62, 1, 'Hadiah', '', NULL, 19, 'hadiah', NULL, NULL),
(63, 1, 'Sparepart', '', NULL, 20, 'sparepart', NULL, NULL),
(64, 1, 'Status Mutasi Mesin', '', NULL, 21, 'status_mutasi_mesin', NULL, NULL),
(65, 1, 'Strore', '', NULL, 22, 'store', NULL, NULL),
(66, 1, 'Machine Type', '', NULL, 23, 'jenis_mesin', NULL, NULL),
(67, 0, 'Merchandise', 'purchase request for hadiah', NULL, 2, 'pr_hadiah', NULL, NULL),
(68, 0, 'Sparepart', '', NULL, 3, 'pr_sparepart', NULL, NULL),
(69, 10, 'Machine Reparation', '', NULL, 2, 'mesin_kerusakan', NULL, NULL),
(70, 10, 'Machine Service', '', NULL, 3, 'service', NULL, NULL),
(71, 1, 'Ticket', '', NULL, 24, 'tiket', NULL, NULL),
(72, 10, 'Machine Transaction', '', NULL, 4, 'machine_transaction', NULL, NULL),
(73, 1, 'Employee & Store', '', NULL, 25, 'pegawai_cabang', NULL, NULL),
(74, 10, 'Inventory Summary', '', NULL, 5, 'inventory_summary', NULL, NULL),
(75, 1, 'Coin', '', NULL, 26, 'koin', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `m_mesin`
--

CREATE TABLE IF NOT EXISTS `m_mesin` (
`id_mesin` int(5) NOT NULL,
  `kd_mesin` varchar(250) NOT NULL,
  `serial_number` varchar(50) DEFAULT NULL,
  `harga` int(20) NOT NULL,
  `tgl_beli` date NOT NULL,
  `garansi_toko` text NOT NULL,
  `status` int(1) DEFAULT '1' COMMENT 'status\n1 : fine\n0 : rusak',
  `id_supplier` int(3) NOT NULL COMMENT 'id_supplier , harga , dan tgl_beli merupakan duplikasi data dari tabel pengadaan detail',
  `id_jenis_mesin` int(4) DEFAULT NULL,
  `id_cabang` int(3) NOT NULL,
  `id_koin` int(2) DEFAULT NULL COMMENT 'jenis koin nya apa \n',
  `id_owner` int(6) DEFAULT NULL,
  `input_type` enum('coin','swipe') DEFAULT 'coin',
  `output_type` enum('ticket','merchandise') DEFAULT 'ticket',
  `item_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_mesin`
--

INSERT INTO `m_mesin` (`id_mesin`, `kd_mesin`, `serial_number`, `harga`, `tgl_beli`, `garansi_toko`, `status`, `id_supplier`, `id_jenis_mesin`, `id_cabang`, `id_koin`, `id_owner`, `input_type`, `output_type`, `item_id`) VALUES
(1, 'M001', '0987609876', 500000, '2015-04-01', '2015-08-12', 0, 9, 1, 11, 2, 2, 'coin', 'ticket', 5),
(2, 'M002', '882763623', 5000000, '1900-12-08', '2015-04-24', 0, 9, 2, 11, 1, 2, 'coin', 'ticket', 6);

-- --------------------------------------------------------

--
-- Table structure for table `m_negara`
--

CREATE TABLE IF NOT EXISTS `m_negara` (
`id_negara` int(11) NOT NULL,
  `nama_negara` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_negara`
--

INSERT INTO `m_negara` (`id_negara`, `nama_negara`) VALUES
(1, 'Indonesia'),
(2, 'Malaysia');

-- --------------------------------------------------------

--
-- Table structure for table `m_owner`
--

CREATE TABLE IF NOT EXISTS `m_owner` (
`id_owner` int(6) NOT NULL,
  `kd_owner` varchar(50) DEFAULT NULL,
  `nama_owner` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_owner`
--

INSERT INTO `m_owner` (`id_owner`, `kd_owner`, `nama_owner`) VALUES
(1, 'RAM', 'RAMAYANA MAKMUR SENTOSA'),
(2, 'MUL', 'PT. MULKINDO PERKASA');

-- --------------------------------------------------------

--
-- Table structure for table `m_pegawai`
--

CREATE TABLE IF NOT EXISTS `m_pegawai` (
`id_pegawai` int(6) NOT NULL,
  `nik` varchar(50) DEFAULT NULL,
  `nama_pegawai` varchar(250) DEFAULT NULL,
  `no_hp` varchar(50) DEFAULT NULL,
  `alamat` text,
  `id_jabatan` int(4) DEFAULT NULL,
  `id_level` int(4) DEFAULT NULL,
  `tgl_masuk` date DEFAULT NULL,
  `tgl_keluar` date DEFAULT NULL,
  `salary` int(10) DEFAULT NULL,
  `status_pegawai` int(3) DEFAULT NULL,
  `keterangan` text,
  `user_id` int(10) unsigned DEFAULT NULL,
  `default_store_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_pegawai`
--

INSERT INTO `m_pegawai` (`id_pegawai`, `nik`, `nama_pegawai`, `no_hp`, `alamat`, `id_jabatan`, `id_level`, `tgl_masuk`, `tgl_keluar`, `salary`, `status_pegawai`, `keterangan`, `user_id`, `default_store_id`) VALUES
(4, 'RFD', 'Rofid Rahmadi', '08123', 'Sarijadi', 1, 2, '2015-01-01', '0000-00-00', 10000000, 0, 'Info', 1, 11),
(6, 'WHD', 'Wahid Hasan Ariyanto', '1234', 'Sarijadi', 1, 2, '2015-04-27', '0000-00-00', 10000000, 0, 'Info', 6, 12);

-- --------------------------------------------------------

--
-- Table structure for table `m_pegawai_cabang`
--

CREATE TABLE IF NOT EXISTS `m_pegawai_cabang` (
`id_pegawai_cabang` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  `id_cabang` int(11) NOT NULL,
  `is_active` int(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_pegawai_cabang`
--

INSERT INTO `m_pegawai_cabang` (`id_pegawai_cabang`, `id_pegawai`, `id_cabang`, `is_active`) VALUES
(36, 1, 11, 0),
(37, 3, 10, 0),
(38, 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `m_provinsi`
--

CREATE TABLE IF NOT EXISTS `m_provinsi` (
`id_provinsi` int(3) NOT NULL,
  `nama_provinsi` varchar(250) NOT NULL,
  `kd_provinsi` varchar(20) NOT NULL,
  `urutan` int(3) NOT NULL,
  `id_pulau` int(3) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_provinsi`
--

INSERT INTO `m_provinsi` (`id_provinsi`, `nama_provinsi`, `kd_provinsi`, `urutan`, `id_pulau`) VALUES
(1, 'Jakarta', '001', 0, 2),
(2, 'Jawa Barat', '002', 0, 3);

-- --------------------------------------------------------

--
-- Table structure for table `m_pulau`
--

CREATE TABLE IF NOT EXISTS `m_pulau` (
`id_pulau` int(3) NOT NULL,
  `nama_pulau` varchar(250) NOT NULL,
  `kd_pulau` varchar(20) NOT NULL,
  `id_negara` int(11) NOT NULL DEFAULT '1',
  `urutan` int(3) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_pulau`
--

INSERT INTO `m_pulau` (`id_pulau`, `nama_pulau`, `kd_pulau`, `id_negara`, `urutan`) VALUES
(1, 'Sumatra', '001', 1, 0),
(2, 'Jawa', '002', 1, 0),
(3, 'Kalimantan', '003', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `m_roles`
--

CREATE TABLE IF NOT EXISTS `m_roles` (
`roles_id` bigint(20) unsigned NOT NULL,
  `menu_id` int(11) DEFAULT NULL,
  `grup_id` int(11) unsigned DEFAULT NULL,
  `is_view` varchar(1) DEFAULT NULL,
  `is_add` varchar(10) DEFAULT NULL,
  `is_edit` varchar(1) DEFAULT NULL,
  `is_delete` varchar(1) DEFAULT NULL,
  `is_approve` varchar(1) DEFAULT NULL,
  `is_import` varchar(1) DEFAULT NULL,
  `is_print` varchar(1) DEFAULT NULL,
  `act` varchar(1) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1103 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_roles`
--

INSERT INTO `m_roles` (`roles_id`, `menu_id`, `grup_id`, `is_view`, `is_add`, `is_edit`, `is_delete`, `is_approve`, `is_import`, `is_print`, `act`) VALUES
(1071, 67, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(1072, 68, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(1073, 60, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(1074, 69, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(1075, 70, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(1076, 72, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(1077, 74, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(1078, 6, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(1079, 1, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(1080, 3, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(1081, 48, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(1082, 49, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(1083, 50, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(1084, 51, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(1085, 52, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(1086, 53, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(1087, 55, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(1088, 56, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(1089, 57, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(1090, 58, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(1091, 59, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(1092, 61, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(1093, 62, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(1094, 63, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(1095, 64, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(1096, 65, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(1097, 66, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(1098, 71, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(1099, 73, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(1100, 75, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(1101, 5, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(1102, 4, 15, 't', 't', 't', 't', 't', 't', 't', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `m_setting_hadiah`
--

CREATE TABLE IF NOT EXISTS `m_setting_hadiah` (
`id_setting_hadiah` int(4) NOT NULL,
  `id_lokasi` int(3) DEFAULT NULL,
  `id_hadiah` int(3) DEFAULT NULL,
  `jumlah_tiket` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_sparepart`
--

CREATE TABLE IF NOT EXISTS `m_sparepart` (
`id_sparepart` int(6) NOT NULL,
  `nama_sparepart` varchar(250) DEFAULT NULL,
  `kd_sparepart` varchar(50) DEFAULT NULL,
  `harga` int(11) DEFAULT '0',
  `item_id` int(11) DEFAULT NULL,
  `item_key` varchar(200) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_sparepart`
--

INSERT INTO `m_sparepart` (`id_sparepart`, `nama_sparepart`, `kd_sparepart`, `harga`, `item_id`, `item_key`) VALUES
(1, 'Tombol Kaizer', 'SP001', 90000, 24, '1199281'),
(2, 'Peer Tombol', 'SP002', 5000, 26, '8829381'),
(3, 'Lampu LED Merah', 'SP003', 90000, 27, '0029930');

-- --------------------------------------------------------

--
-- Table structure for table `m_stok_kartu`
--

CREATE TABLE IF NOT EXISTS `m_stok_kartu` (
`id_stok_hadiah` int(11) NOT NULL,
  `kd_stok_hadiah` varchar(50) DEFAULT NULL,
  `id_kartu` int(2) DEFAULT NULL,
  `id_cabang` int(3) DEFAULT NULL,
  `stok_total` int(11) DEFAULT '0',
  `stok_gudang` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_stok_koin`
--

CREATE TABLE IF NOT EXISTS `m_stok_koin` (
`id_stok_hadiah` int(11) NOT NULL,
  `kd_stok_hadiah` varchar(50) DEFAULT NULL,
  `id_koin` int(2) DEFAULT NULL,
  `id_cabang` int(3) DEFAULT NULL,
  `stok_total` int(11) DEFAULT '0',
  `stok_gudang` int(11) DEFAULT '0',
  `stok_mesin` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_stok_sparepart`
--

CREATE TABLE IF NOT EXISTS `m_stok_sparepart` (
`id_stok_sparepart` int(11) NOT NULL,
  `kd_stok_sparepart` varchar(50) DEFAULT NULL,
  `id_sparepart` int(2) DEFAULT NULL,
  `id_cabang` int(3) DEFAULT NULL,
  `stok_total` int(11) DEFAULT '0',
  `stok_gudang` int(11) DEFAULT '0',
  `stok_mesin` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_stok_tiket`
--

CREATE TABLE IF NOT EXISTS `m_stok_tiket` (
`id_stok_tiket` int(11) NOT NULL,
  `kd_stok_tiket` varchar(50) DEFAULT NULL,
  `id_tiket` int(2) DEFAULT NULL,
  `id_cabang` int(3) DEFAULT NULL,
  `stok_total` int(11) DEFAULT '0',
  `stok_gudang` int(11) DEFAULT NULL,
  `stok_mesin` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_supplier`
--

CREATE TABLE IF NOT EXISTS `m_supplier` (
`id_supplier` int(3) NOT NULL,
  `nama_supplier` varchar(250) NOT NULL,
  `kd_supplier` varchar(20) NOT NULL,
  `urutan` int(3) NOT NULL,
  `alamat_supplier` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_supplier`
--

INSERT INTO `m_supplier` (`id_supplier`, `nama_supplier`, `kd_supplier`, `urutan`, `alamat_supplier`) VALUES
(9, 'PT Indonesia Juara', 'S001', 0, 'Jakarta'),
(10, 'PT Maju Karya', 'S002', 0, 'Bandung'),
(11, 'PT Dinamo Indonesia', 'S003', 0, 'Jakarta'),
(12, 'PT Machine Jaya', 'S004', 0, 'Surabaya');

-- --------------------------------------------------------

--
-- Table structure for table `m_tiket`
--

CREATE TABLE IF NOT EXISTS `m_tiket` (
`id_tiket` int(2) NOT NULL,
  `kd_tiket` varchar(50) DEFAULT NULL,
  `nama_tiket` varchar(50) DEFAULT NULL,
  `item_id` int(11) NOT NULL,
  `item_key` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_tiket`
--

INSERT INTO `m_tiket` (`id_tiket`, `kd_tiket`, `nama_tiket`, `item_id`, `item_key`) VALUES
(7, 'AAA', 'TICKET REGULAR', 7, 'AAA');

-- --------------------------------------------------------

--
-- Table structure for table `m_user`
--

CREATE TABLE IF NOT EXISTS `m_user` (
`user_id` int(10) unsigned NOT NULL,
  `grup_id` int(11) unsigned DEFAULT NULL,
  `user_name` varchar(25) DEFAULT NULL,
  `user_password` varchar(50) DEFAULT NULL,
  `last_update` date DEFAULT NULL,
  `is_active` char(1) DEFAULT NULL,
  `last_login` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_user`
--

INSERT INTO `m_user` (`user_id`, `grup_id`, `user_name`, `user_password`, `last_update`, `is_active`, `last_login`) VALUES
(1, 15, 'admin', '21232f297a57a5a743894a0e4a801fc3', '2015-03-01', '1', '2015-04-27'),
(2, 16, 'produksi', 'edf3017a2946290b95c783bd1a7f0ba7', '2015-03-06', '1', '2015-03-11'),
(3, 17, 'akuntansi', '1139f90d50ba3bb7ff4b2602ad03aa26', '2015-03-01', '1', '2015-04-16'),
(4, 18, 'direksi', '18a5726d8227b237064ecef7d1f4e634', '2015-03-01', '1', '2015-04-16'),
(5, 19, 'logistik', 'cb1f02561c07f62717a4814c048a6239', '2015-01-21', '1', '2015-04-16'),
(6, 15, 'sarijadi', '21232f297a57a5a743894a0e4a801fc3', NULL, NULL, '2015-04-27');

-- --------------------------------------------------------

--
-- Table structure for table `zn_ci_sessions`
--

CREATE TABLE IF NOT EXISTS `zn_ci_sessions` (
  `session_id` varchar(40) NOT NULL,
  `ip_address` varchar(50) NOT NULL,
  `last_activity` bigint(20) NOT NULL,
  `user_data` text NOT NULL,
  `user_agent` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_hadiah_keluar`
--

CREATE TABLE IF NOT EXISTS `zn_hadiah_keluar` (
`id_hadiah_keluar` int(8) NOT NULL,
  `id_hadiah` int(6) DEFAULT NULL,
  `id_lokasi` int(6) DEFAULT NULL,
  `jumlah` int(4) DEFAULT NULL,
  `tanggal` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_kartu_penjualan`
--

CREATE TABLE IF NOT EXISTS `zn_kartu_penjualan` (
`id_kartu_penjualan` int(11) NOT NULL,
  `kd_kartu_penjualan` varchar(50) DEFAULT NULL,
  `nama_pembeli` varchar(50) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `id_pegawai` int(6) DEFAULT NULL,
  `id_kartu` int(2) DEFAULT NULL,
  `id_cabang` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_kartu_topup`
--

CREATE TABLE IF NOT EXISTS `zn_kartu_topup` (
`id_kartu_topup` int(11) NOT NULL,
  `kd_kartu_topup` varchar(50) DEFAULT NULL,
  `id_kartu` int(2) DEFAULT NULL,
  `id_cabang` int(3) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `nominal` int(11) DEFAULT NULL,
  `id_pegawai` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_log`
--

CREATE TABLE IF NOT EXISTS `zn_log` (
  `nik` varchar(15) DEFAULT NULL,
  `username` varchar(30) DEFAULT NULL,
  `tgl_akses` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `activity` varchar(150) DEFAULT NULL,
`id` bigint(20) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_mesin_history`
--

CREATE TABLE IF NOT EXISTS `zn_mesin_history` (
`id_mesin_history` int(11) NOT NULL,
  `kd_mesin_history` varchar(50) DEFAULT NULL,
  `tgl_pengiriman` datetime DEFAULT NULL,
  `tgl_penerimaan` datetime DEFAULT NULL,
  `id_penerima` int(3) DEFAULT NULL,
  `id_pengirim` int(3) DEFAULT NULL,
  `tipe_mutasi` enum('purchase','transfer') DEFAULT NULL COMMENT '0 : purchase\n1 : transfer',
  `id_mesin` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_mesin_kerusakan`
--

CREATE TABLE IF NOT EXISTS `zn_mesin_kerusakan` (
`id_mesin_rusak` int(11) NOT NULL,
  `kd_mesin_rusak` varchar(50) DEFAULT NULL,
  `id_mesin` int(5) DEFAULT NULL,
  `tgl_rusak` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `tgl_mulai` date DEFAULT NULL COMMENT 'tanggal mulai perbaikan',
  `tgl_target` date DEFAULT NULL,
  `tgl_selesai` datetime DEFAULT NULL,
  `request_service` int(1) DEFAULT '0' COMMENT 'apakah kerusakan ini meminta request service ke pusat\n0 : perbaikan sendiri oleh teknisi di cabang\n1 : service oleh pusat\n',
  `status_kerusakan` int(1) DEFAULT '0' COMMENT '0 : rusak\n1 : beres\n2 : inspection\n3 : waiting for sparepart\n4 : sparepart tiba',
  `problem` varchar(255) DEFAULT NULL COMMENT 'keterangan kerusakan : \nmisal layar mati, tombol tidak bisa ditekan, dsb',
  `id_cabang` int(3) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='mencatat history kerusakan sparepart pada mesin';

--
-- Dumping data for table `zn_mesin_kerusakan`
--

INSERT INTO `zn_mesin_kerusakan` (`id_mesin_rusak`, `kd_mesin_rusak`, `id_mesin`, `tgl_rusak`, `tgl_mulai`, `tgl_target`, `tgl_selesai`, `request_service`, `status_kerusakan`, `problem`, `id_cabang`) VALUES
(1, 'RS001', 1, '2015-04-11 00:53:43', '2015-04-23', '2015-05-01', '2015-05-01 00:00:00', 1, 0, 'lampu mati', 11),
(2, 'KER002', 2, '2015-04-13 12:14:42', '2015-03-29', '2015-04-30', NULL, 1, 0, 'Mengeluarkan bunyi-bunyi aneh', 11);

-- --------------------------------------------------------

--
-- Table structure for table `zn_mesin_kerusakan_history`
--

CREATE TABLE IF NOT EXISTS `zn_mesin_kerusakan_history` (
`id_rusak_history` int(11) NOT NULL,
  `kd_rusak_history` varchar(50) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL COMMENT '0 : done\n1 : rusak\n2 : inspection\n3 : waiting for sparepart',
  `id_mesin_rusak` int(11) DEFAULT NULL,
  `id_pegawai` int(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_mutasi_mesin`
--

CREATE TABLE IF NOT EXISTS `zn_mutasi_mesin` (
  `id_mutasi_mesin` int(6) DEFAULT NULL,
  `id_mesin` int(6) DEFAULT NULL,
  `id_lokasi_asal` int(6) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `id_status_mutasi_mesin` int(3) DEFAULT NULL,
  `id_lokasi_tujuan` int(6) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_pr_hadiah`
--

CREATE TABLE IF NOT EXISTS `zn_pr_hadiah` (
`id_pr` int(11) NOT NULL,
  `kd_pr` varchar(50) DEFAULT NULL,
  `kd_po` varchar(50) DEFAULT NULL,
  `nama_pr` varchar(250) DEFAULT NULL,
  `status_pr` int(2) DEFAULT '0' COMMENT '0 : draft\n1 : dibuat / waiting aproval\n2 : revisi\n3 : ditolak\n4 : diterima',
  `id_pengirim` int(3) DEFAULT NULL COMMENT 'cabang yang meminta PR',
  `id_tujuan` int(3) DEFAULT NULL COMMENT 'id store pusat yang dituju',
  `keterangan` text,
  `tgl_dibuat` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'tanggal PR ini dibuat',
  `tgl_diajukan` datetime DEFAULT NULL,
  `tgl_disetujui` datetime DEFAULT NULL,
  `tgl_ditolak` datetime DEFAULT NULL,
  `tgl_dikirim` datetime DEFAULT NULL,
  `tgl_diterima` datetime DEFAULT NULL,
  `tgl_revisi` datetime DEFAULT NULL,
  `count_revisi` int(2) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `zn_pr_hadiah`
--

INSERT INTO `zn_pr_hadiah` (`id_pr`, `kd_pr`, `kd_po`, `nama_pr`, `status_pr`, `id_pengirim`, `id_tujuan`, `keterangan`, `tgl_dibuat`, `tgl_diajukan`, `tgl_disetujui`, `tgl_ditolak`, `tgl_dikirim`, `tgl_diterima`, `tgl_revisi`, `count_revisi`) VALUES
(1, 'PRM001', NULL, NULL, 1, 11, 10, 'oke. please', '2015-04-10 11:02:44', '2015-04-10 18:04:52', NULL, NULL, NULL, NULL, NULL, 0),
(2, 'PRM2', NULL, NULL, 0, 11, 10, 'please', '2015-04-10 12:30:48', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(3, 'PRM003', NULL, NULL, 0, 11, 10, '', '2015-04-11 10:57:23', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(4, '1111', NULL, NULL, 0, 11, 10, 'sxxx', '2015-04-17 03:02:09', NULL, NULL, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `zn_pr_hadiah_history`
--

CREATE TABLE IF NOT EXISTS `zn_pr_hadiah_history` (
`id_pr_history` int(11) NOT NULL,
  `tanggal` varchar(45) DEFAULT NULL,
  `status` int(1) DEFAULT NULL COMMENT '1 : dibuat/diajukan\n2 : revisi\n3 : ditolak\n4 : diterima\n',
  `id_pegawai` int(11) DEFAULT NULL,
  `id_pr` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `zn_pr_hadiah_history`
--

INSERT INTO `zn_pr_hadiah_history` (`id_pr_history`, `tanggal`, `status`, `id_pegawai`, `id_pr`) VALUES
(1, '2015-04-10 18:04:52', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `zn_prpo`
--

CREATE TABLE IF NOT EXISTS `zn_prpo` (
`prpo_id` int(11) NOT NULL,
  `pr_id` int(11) DEFAULT NULL,
  `po_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_purchase_order`
--

CREATE TABLE IF NOT EXISTS `zn_purchase_order` (
`po_id` int(11) NOT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `bundle_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `po_status` int(1) DEFAULT '5' COMMENT '0 : draft\n1 : waiting approval\n2 : revisi\n3 : rejected\n4 : approved\n5 : shipped\n6 : received\n7 : canceled',
  `po_type` int(1) DEFAULT '0' COMMENT '0 : all\n1 : machine\n2 : merchandise\n3 : sparepart\n4 : ticket\n5 : coin\n6 : swipecard',
  `user_id` int(11) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `zn_purchase_order`
--

INSERT INTO `zn_purchase_order` (`po_id`, `transaction_id`, `bundle_id`, `supplier_id`, `po_status`, `po_type`, `user_id`, `date_created`) VALUES
(1, 56, 9, 11, 5, 0, 1, '2015-04-27 07:30:11'),
(2, 57, 8, 9, 5, 0, 1, '2015-04-27 07:31:30'),
(3, 58, 10, 11, 5, 0, 1, '2015-04-27 07:31:55'),
(4, 59, 7, 10, 5, 0, 1, '2015-04-27 07:32:42');

-- --------------------------------------------------------

--
-- Table structure for table `zn_purchase_order_detail`
--

CREATE TABLE IF NOT EXISTS `zn_purchase_order_detail` (
`podetail_id` int(11) NOT NULL,
  `po_id` int(11) DEFAULT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT '0' COMMENT '0 : draft\n1 : waiting approval\n2 : revisi\n3 : rejected\n4 : approved\n5 : shipped\n6 : received\n7 : canceled',
  `qty` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `zn_purchase_order_detail`
--

INSERT INTO `zn_purchase_order_detail` (`podetail_id`, `po_id`, `transaction_id`, `item_id`, `price`, `qty`) VALUES
(1, 1, 56, 9, 40000, 10),
(2, 1, 56, 10, 70000, 10),
(3, 2, 57, 12, 2000000, 2),
(4, 2, 57, 14, 400000, 4),
(5, 3, 58, 27, 90000, 4),
(6, 3, 58, 26, 5000, 4),
(7, 3, 58, 12, 2000000, 4),
(8, 3, 58, 14, 400000, 4),
(9, 4, 59, 9, 40000, 1),
(10, 4, 59, 10, 70000, 2),
(11, 4, 59, 27, 90000, 1),
(12, 4, 59, 26, 5000, 2),
(13, 4, 59, 24, 90000, 1),
(14, 4, 59, 8, 2000, 1),
(15, 4, 59, 7, 90000, 20),
(16, 4, 59, 13, 1400000, 2),
(17, 4, 59, 12, 2000000, 1),
(18, 4, 59, 11, 3000000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `zn_purchase_request`
--

CREATE TABLE IF NOT EXISTS `zn_purchase_request` (
`pr_id` int(11) NOT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_request` datetime DEFAULT NULL,
  `date_revisi` datetime DEFAULT NULL,
  `date_confirmed` datetime DEFAULT NULL,
  `count_revisi` int(2) DEFAULT '0',
  `pr_status` int(1) DEFAULT '0' COMMENT '0 : draft\n1 : waiting approval\n2 : revisi\n3 : rejected\n4 : approved\n5 : shipped\n6 : received\n7 : canceled',
  `store_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `pr_note` varchar(100) DEFAULT NULL,
  `pr_type` int(1) DEFAULT NULL COMMENT '0 : all\n1 : machine\n2 : merchandise\n3 : sparepart\n4 : ticket\n5 : coin\n6 : swipecard',
  `transactionbundle_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `zn_purchase_request`
--

INSERT INTO `zn_purchase_request` (`pr_id`, `transaction_id`, `date_created`, `date_request`, `date_revisi`, `date_confirmed`, `count_revisi`, `pr_status`, `store_id`, `user_id`, `pr_note`, `pr_type`, `transactionbundle_id`) VALUES
(1, 1, '2015-04-23 19:15:55', '2015-04-24 02:04:15', '2015-04-26 08:04:13', '2015-04-27 14:04:42', 3, 5, 11, 1, '', 7, 7),
(2, 2, '2015-04-23 19:16:24', '2015-04-24 02:04:51', '2015-04-26 08:04:13', '2015-04-27 14:04:42', 3, 5, 11, 1, '', 2, 7),
(3, 3, '2015-04-24 02:33:59', '2015-04-24 09:04:19', '2015-04-26 08:04:13', '2015-04-27 14:04:42', 3, 5, 11, 1, '', 2, 7),
(4, 4, '2015-04-24 09:31:56', '2015-04-24 16:04:21', '2015-04-26 08:04:13', '2015-04-27 14:04:42', 3, 5, 11, 1, 'horee', 7, 7),
(5, 17, '2015-04-26 00:26:07', '2015-04-26 08:04:53', '2015-04-26 08:04:05', '2015-04-27 14:04:30', 3, 5, 11, 1, '', 7, 8),
(6, 50, '2015-04-27 07:09:38', '2015-04-27 14:04:15', NULL, '2015-04-27 14:04:11', 0, 5, 11, 1, '', 2, 9),
(7, 53, '2015-04-27 07:24:11', '2015-04-27 14:04:23', NULL, '2015-04-27 14:04:55', 0, 5, 11, 1, '', 3, 10),
(8, 54, '2015-04-27 07:24:34', '2015-04-27 14:04:57', NULL, '2015-04-27 14:04:55', 0, 5, 11, 1, '', 7, 10);

-- --------------------------------------------------------

--
-- Table structure for table `zn_purchase_request_detail`
--

CREATE TABLE IF NOT EXISTS `zn_purchase_request_detail` (
`prdetail_id` int(11) NOT NULL,
  `pr_id` int(11) DEFAULT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `qty_request` int(3) DEFAULT NULL,
  `qty_approved` int(3) DEFAULT NULL,
  `qty_received` int(3) DEFAULT NULL,
  `qty_sent` int(3) DEFAULT NULL,
  `qty_temp` int(3) DEFAULT NULL,
  `note` varchar(50) DEFAULT NULL,
  `po_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `zn_purchase_request_detail`
--

INSERT INTO `zn_purchase_request_detail` (`prdetail_id`, `pr_id`, `transaction_id`, `item_id`, `price`, `qty_request`, `qty_approved`, `qty_received`, `qty_sent`, `qty_temp`, `note`, `po_id`) VALUES
(1, 1, 1, 11, 3000000, 3, 1, NULL, NULL, 1, NULL, 4),
(2, 1, 1, 12, 2000000, 2, 1, NULL, NULL, 1, NULL, 4),
(3, 1, 1, 13, 1400000, 5, 2, NULL, NULL, 2, NULL, 4),
(4, 2, 2, 9, 40000, 3, 1, NULL, NULL, 1, NULL, 4),
(5, 2, 2, 10, 70000, 20, 1, NULL, NULL, 1, NULL, 4),
(6, 2, 2, 7, 90000, 30, 20, NULL, NULL, 30, NULL, 4),
(7, 3, 3, 10, 70000, 10, 1, NULL, NULL, 1, NULL, 4),
(8, 3, 3, 8, 2000, 2, 1, NULL, NULL, 2, NULL, 4),
(9, 4, 4, 24, 90000, 12, 1, NULL, NULL, 1, NULL, 4),
(10, 4, 4, 26, 5000, 4, 2, NULL, NULL, 2, NULL, 4),
(11, 4, 4, 27, 90000, 2, 1, NULL, NULL, 1, NULL, 4),
(12, 5, 17, 12, 2000000, 1, 2, NULL, NULL, 1, NULL, 2),
(13, 5, 17, 14, 400000, 5, 4, NULL, NULL, 5, NULL, 2),
(14, 6, 50, 10, 70000, 23, 10, NULL, NULL, 23, NULL, 1),
(15, 6, 50, 9, 40000, 12, 10, NULL, NULL, 12, NULL, 1),
(16, 7, 53, 27, 90000, 21, 4, NULL, NULL, 21, NULL, 3),
(17, 7, 53, 26, 5000, 10, 4, NULL, NULL, 10, NULL, 3),
(18, 8, 54, 14, 400000, 12, 4, NULL, NULL, 12, NULL, 3),
(19, 8, 54, 12, 2000000, 4, 4, NULL, NULL, 4, NULL, 3);

-- --------------------------------------------------------

--
-- Table structure for table `zn_rusak_sparepart`
--

CREATE TABLE IF NOT EXISTS `zn_rusak_sparepart` (
`id_rusak_sparepart` int(11) NOT NULL,
  `kd_rusak_sparepart` varchar(50) DEFAULT NULL,
  `id_mesin_rusak` int(11) DEFAULT NULL,
  `id_sparepart` int(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_service`
--

CREATE TABLE IF NOT EXISTS `zn_service` (
`id_service` int(11) NOT NULL,
  `kd_service` varchar(50) DEFAULT NULL,
  `id_mesin_rusak` int(11) DEFAULT NULL,
  `status_request` int(1) DEFAULT '0' COMMENT '0 : waiting to confim\n1 : diterima\n2 : ditolak',
  `tgl_dibuat` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `tgl_diterima` datetime DEFAULT NULL,
  `id_pegawai` int(6) DEFAULT NULL COMMENT 'pegawai yang ditugaskan untuk melakukan service\njika sudah keluar order nya',
  `keterangan_service` varchar(45) DEFAULT NULL COMMENT 'catatan service'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `zn_service`
--

INSERT INTO `zn_service` (`id_service`, `kd_service`, `id_mesin_rusak`, `status_request`, `tgl_dibuat`, `tgl_diterima`, `id_pegawai`, `keterangan_service`) VALUES
(1, NULL, 1, 1, NULL, NULL, 1, 'tolong secepatnya diperbaiki'),
(2, NULL, 2, 2, '2015-04-13 13:27:51', NULL, NULL, 'ga bisa sory yah.. :)');

-- --------------------------------------------------------

--
-- Table structure for table `zn_sparepart_keluar`
--

CREATE TABLE IF NOT EXISTS `zn_sparepart_keluar` (
`id_sparepart_masuk` int(9) NOT NULL,
  `id_lokasi` int(3) DEFAULT NULL,
  `id_sparepart` int(3) DEFAULT NULL,
  `jumlah` int(10) DEFAULT NULL,
  `tgl` date DEFAULT NULL,
  `keterangan` text,
  `id_service` int(6) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_transaction`
--

CREATE TABLE IF NOT EXISTS `zn_transaction` (
`transaction_id` int(11) NOT NULL,
  `transactiontype_id` int(11) DEFAULT NULL,
  `rec_user` int(11) DEFAULT NULL,
  `rec_created` datetime DEFAULT NULL,
  `num_meter` int(11) DEFAULT NULL,
  `num_coin` int(11) DEFAULT NULL,
  `num_ticket` int(11) DEFAULT NULL,
  `num_start` int(11) DEFAULT NULL,
  `num_end` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `zn_transaction`
--

INSERT INTO `zn_transaction` (`transaction_id`, `transactiontype_id`, `rec_user`, `rec_created`, `num_meter`, `num_coin`, `num_ticket`, `num_start`, `num_end`) VALUES
(1, 15, 1, '2015-04-24 02:04:55', NULL, NULL, NULL, NULL, NULL),
(2, 15, 1, '2015-04-24 02:04:24', NULL, NULL, NULL, NULL, NULL),
(3, 15, 1, '2015-04-24 09:04:59', NULL, NULL, NULL, NULL, NULL),
(4, 15, 1, '2015-04-24 16:04:56', NULL, NULL, NULL, NULL, NULL),
(5, 16, 1, '2015-04-25 10:04:54', NULL, NULL, NULL, NULL, NULL),
(6, 16, 1, '2015-04-25 10:04:14', NULL, NULL, NULL, NULL, NULL),
(8, 16, 1, '2015-04-25 10:04:03', NULL, NULL, NULL, NULL, NULL),
(12, 16, 1, '2015-04-25 10:04:54', NULL, NULL, NULL, NULL, NULL),
(13, 16, 1, '2015-04-25 10:04:24', NULL, NULL, NULL, NULL, NULL),
(14, 16, 1, '2015-04-25 11:04:06', NULL, NULL, NULL, NULL, NULL),
(15, 16, 1, '2015-04-25 12:04:52', NULL, NULL, NULL, NULL, NULL),
(16, 16, 1, '2015-04-25 17:04:41', NULL, NULL, NULL, NULL, NULL),
(17, 15, 1, '2015-04-26 07:04:07', NULL, NULL, NULL, NULL, NULL),
(18, 16, 1, '2015-04-26 07:04:14', NULL, NULL, NULL, NULL, NULL),
(21, 17, 1, '2015-04-27 12:04:48', NULL, NULL, NULL, NULL, NULL),
(22, 17, 1, '2015-04-27 12:04:48', NULL, NULL, NULL, NULL, NULL),
(23, 17, 1, '2015-04-27 12:04:48', NULL, NULL, NULL, NULL, NULL),
(24, 17, 1, '2015-04-27 12:04:48', NULL, NULL, NULL, NULL, NULL),
(30, 17, 1, '2015-04-27 13:04:45', NULL, NULL, NULL, NULL, NULL),
(31, 17, 1, '2015-04-27 13:04:45', NULL, NULL, NULL, NULL, NULL),
(32, 17, 1, '2015-04-27 13:04:45', NULL, NULL, NULL, NULL, NULL),
(33, 17, 1, '2015-04-27 13:04:45', NULL, NULL, NULL, NULL, NULL),
(34, 17, 1, '2015-04-27 13:04:46', NULL, NULL, NULL, NULL, NULL),
(35, 17, 1, '2015-04-27 13:04:46', NULL, NULL, NULL, NULL, NULL),
(36, 17, 1, '2015-04-27 13:04:46', NULL, NULL, NULL, NULL, NULL),
(37, 17, 1, '2015-04-27 13:04:46', NULL, NULL, NULL, NULL, NULL),
(38, 17, 1, '2015-04-27 13:04:42', NULL, NULL, NULL, NULL, NULL),
(39, 17, 1, '2015-04-27 13:04:42', NULL, NULL, NULL, NULL, NULL),
(40, 17, 1, '2015-04-27 13:04:42', NULL, NULL, NULL, NULL, NULL),
(41, 17, 1, '2015-04-27 13:04:42', NULL, NULL, NULL, NULL, NULL),
(42, 17, 1, '2015-04-27 13:04:01', NULL, NULL, NULL, NULL, NULL),
(43, 17, 1, '2015-04-27 13:04:01', NULL, NULL, NULL, NULL, NULL),
(44, 17, 1, '2015-04-27 13:04:01', NULL, NULL, NULL, NULL, NULL),
(45, 17, 1, '2015-04-27 13:04:01', NULL, NULL, NULL, NULL, NULL),
(46, 17, 1, '2015-04-27 13:04:57', NULL, NULL, NULL, NULL, NULL),
(47, 17, 1, '2015-04-27 13:04:57', NULL, NULL, NULL, NULL, NULL),
(48, 17, 1, '2015-04-27 13:04:57', NULL, NULL, NULL, NULL, NULL),
(49, 17, 1, '2015-04-27 13:04:57', NULL, NULL, NULL, NULL, NULL),
(50, 15, 1, '2015-04-27 14:04:38', NULL, NULL, NULL, NULL, NULL),
(51, 16, 1, '2015-04-27 14:04:26', NULL, NULL, NULL, NULL, NULL),
(52, 17, 1, '2015-04-27 14:04:57', NULL, NULL, NULL, NULL, NULL),
(53, 15, 1, '2015-04-27 14:04:11', NULL, NULL, NULL, NULL, NULL),
(54, 15, 1, '2015-04-27 14:04:34', NULL, NULL, NULL, NULL, NULL),
(55, 16, 1, '2015-04-27 14:04:04', NULL, NULL, NULL, NULL, NULL),
(56, 17, 1, '2015-04-27 14:04:11', NULL, NULL, NULL, NULL, NULL),
(57, 17, 1, '2015-04-27 14:04:30', NULL, NULL, NULL, NULL, NULL),
(58, 17, 1, '2015-04-27 14:04:55', NULL, NULL, NULL, NULL, NULL),
(59, 17, 1, '2015-04-27 14:04:42', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `zn_transaction_bundle`
--

CREATE TABLE IF NOT EXISTS `zn_transaction_bundle` (
`transactionbundle_id` int(11) NOT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `bundle_status` int(1) DEFAULT '0' COMMENT '0 : open\n1 : closed',
  `bundle_type` int(1) DEFAULT NULL COMMENT 'tipe bundle : \n1 : purchase request\n'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `zn_transaction_bundle`
--

INSERT INTO `zn_transaction_bundle` (`transactionbundle_id`, `transaction_id`, `bundle_status`, `bundle_type`) VALUES
(7, 16, 1, 1),
(8, 18, 1, 1),
(9, 51, 1, 1),
(10, 55, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `zn_transaction_device`
--

CREATE TABLE IF NOT EXISTS `zn_transaction_device` (
`transactiondevice_id` int(11) NOT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `device_id` int(11) DEFAULT NULL,
  `device_transaction_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_transaction_document`
--

CREATE TABLE IF NOT EXISTS `zn_transaction_document` (
`transactiondocument_id` int(11) NOT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `doc_number` varchar(100) DEFAULT NULL,
  `doc_number2` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `zn_transaction_document`
--

INSERT INTO `zn_transaction_document` (`transactiondocument_id`, `transaction_id`, `doc_number`, `doc_number2`) VALUES
(1, 1, 'PR001', NULL),
(2, 2, 'PR002', NULL),
(3, 3, 'PR003', NULL),
(4, 4, 'PR004', NULL),
(5, 17, 'PR005', NULL),
(6, 50, 'PR006', NULL),
(7, 53, 'PR006', NULL),
(8, 54, 'PR006', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `zn_transaction_machine`
--

CREATE TABLE IF NOT EXISTS `zn_transaction_machine` (
`transactionmachine_id` int(11) NOT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `machine_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_transaction_meter`
--

CREATE TABLE IF NOT EXISTS `zn_transaction_meter` (
`transactionmeter_id` int(11) NOT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `num_initial` int(11) DEFAULT NULL,
  `num_current` int(11) DEFAULT NULL,
  `num_diff` int(11) DEFAULT NULL,
  `item_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_transaction_redemption`
--

CREATE TABLE IF NOT EXISTS `zn_transaction_redemption` (
`transactionredemption_id` int(11) NOT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `item_key` varchar(200) DEFAULT NULL,
  `item_qty` int(11) DEFAULT NULL,
  `rec_created` datetime DEFAULT NULL,
  `seq` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_transaction_refill`
--

CREATE TABLE IF NOT EXISTS `zn_transaction_refill` (
`transactionrefill_id` int(11) NOT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `item_key` varchar(200) DEFAULT NULL,
  `item_qty` int(11) DEFAULT NULL,
  `rec_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `seq` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_transaction_repair`
--

CREATE TABLE IF NOT EXISTS `zn_transaction_repair` (
`transactionrepair_id` int(11) NOT NULL,
  `repair_status` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_transaction_service`
--

CREATE TABLE IF NOT EXISTS `zn_transaction_service` (
`transactionservice_id` int(11) NOT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `issue_description` varchar(255) DEFAULT NULL,
  `service_status` int(1) DEFAULT '0' COMMENT '0 : waiting confim\n1 : accepted\n2 : rejeted',
  `service_note` varchar(255) DEFAULT NULL,
  `date_confirmation` datetime DEFAULT NULL,
  `technician_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_transaction_store`
--

CREATE TABLE IF NOT EXISTS `zn_transaction_store` (
`transactionstore_id` int(11) NOT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `zn_transaction_store`
--

INSERT INTO `zn_transaction_store` (`transactionstore_id`, `transaction_id`, `store_id`) VALUES
(1, 1, 1),
(2, 2, 11),
(3, 1, 11),
(4, 2, 11),
(5, 3, 11),
(6, 4, 11),
(7, 6, 11),
(9, 8, 11),
(13, 12, 11),
(14, 13, 11),
(15, 14, 11),
(16, 15, 11),
(17, 16, 11),
(18, 17, 11),
(19, 18, 11),
(22, 21, 11),
(23, 22, 11),
(24, 23, 11),
(25, 24, 11),
(31, 30, 11),
(32, 31, 11),
(33, 32, 11),
(34, 33, 11),
(35, 34, 11),
(36, 35, 11),
(37, 36, 11),
(38, 37, 11),
(39, 38, 11),
(40, 39, 11),
(41, 40, 11),
(42, 41, 11),
(43, 42, 11),
(44, 43, 11),
(45, 44, 11),
(46, 45, 11),
(47, 46, 11),
(48, 47, 11),
(49, 48, 11),
(50, 49, 11),
(51, 50, 11),
(52, 51, 11),
(53, 52, 11),
(54, 53, 11),
(55, 54, 11),
(56, 55, 11),
(57, 56, 11),
(58, 57, 11),
(59, 58, 11),
(60, 59, 11);

-- --------------------------------------------------------

--
-- Table structure for table `zn_transaction_type`
--

CREATE TABLE IF NOT EXISTS `zn_transaction_type` (
`transactiontype_id` int(11) NOT NULL,
  `transactiontype_key` varchar(20) DEFAULT NULL,
  `transactiontype_name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `zn_transaction_type`
--

INSERT INTO `zn_transaction_type` (`transactiontype_id`, `transactiontype_key`, `transactiontype_name`) VALUES
(1, 'COIN', 'COIN IN'),
(2, 'COUT', 'COIN OUT'),
(3, 'TOUT', 'TICKET OUT/TICKET METER'),
(4, 'MOUT', 'MERCHANDISE OUT'),
(5, 'TFIL', 'TICKET ISSUE/TICKET REFILL'),
(6, 'MFIL', 'MERCHANDISE REFILL'),
(7, 'TCUT', 'TICKET CUTTER METER'),
(8, 'TRDM', 'TICKET REDEMPTION IN'),
(9, 'MRDM', 'MERCHANDISE REDEMPTION'),
(10, 'SREQ', 'SERVICE REQUEST'),
(11, 'SREP', 'SERVICE REPAIR');

-- --------------------------------------------------------

--
-- Table structure for table `zn_transaksi_mesin`
--

CREATE TABLE IF NOT EXISTS `zn_transaksi_mesin` (
`id_transaksi_mesin` int(20) NOT NULL,
  `id_mesin` int(6) DEFAULT NULL,
  `id_lokasi` int(6) DEFAULT NULL,
  `jumlah_swap` int(4) DEFAULT NULL,
  `jumlah_tiket` int(8) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `nilai_swap` int(8) DEFAULT NULL,
  `jumlah_coin` int(8) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `m_area`
--
ALTER TABLE `m_area`
 ADD PRIMARY KEY (`id_area`), ADD KEY `fk_area_kota_idx` (`id_kota`), ADD KEY `fk_area_pegawai_idx` (`id_ka_area`);

--
-- Indexes for table `m_cabang`
--
ALTER TABLE `m_cabang`
 ADD PRIMARY KEY (`id_cabang`), ADD KEY `fk_cabang_area_idx` (`id_area`), ADD KEY `fk_cabang_owner_idx` (`id_owner`);

--
-- Indexes for table `m_cabang_setting`
--
ALTER TABLE `m_cabang_setting`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_cabang_setting_cabang_idx` (`id_cabang`);

--
-- Indexes for table `m_grup`
--
ALTER TABLE `m_grup`
 ADD PRIMARY KEY (`grup_id`), ADD UNIQUE KEY `grup_id` (`grup_id`);

--
-- Indexes for table `m_grup_menu`
--
ALTER TABLE `m_grup_menu`
 ADD PRIMARY KEY (`kd_grup_menu`), ADD UNIQUE KEY `kd_grup_menu` (`kd_grup_menu`);

--
-- Indexes for table `m_hadiah`
--
ALTER TABLE `m_hadiah`
 ADD PRIMARY KEY (`id_hadiah`), ADD KEY `item_key` (`item_key`);

--
-- Indexes for table `m_item`
--
ALTER TABLE `m_item`
 ADD PRIMARY KEY (`item_id`), ADD KEY `item_key` (`item_key`);

--
-- Indexes for table `m_item_redemption`
--
ALTER TABLE `m_item_redemption`
 ADD PRIMARY KEY (`itemredemption_id`);

--
-- Indexes for table `m_item_stok`
--
ALTER TABLE `m_item_stok`
 ADD PRIMARY KEY (`itemstok_id`);

--
-- Indexes for table `m_item_supplier`
--
ALTER TABLE `m_item_supplier`
 ADD PRIMARY KEY (`itemsupplier_id`);

--
-- Indexes for table `m_item_supplier_default`
--
ALTER TABLE `m_item_supplier_default`
 ADD PRIMARY KEY (`default_id`);

--
-- Indexes for table `m_item_type`
--
ALTER TABLE `m_item_type`
 ADD PRIMARY KEY (`itemtype_id`);

--
-- Indexes for table `m_jabatan`
--
ALTER TABLE `m_jabatan`
 ADD PRIMARY KEY (`id_jabatan`);

--
-- Indexes for table `m_jenis_mesin`
--
ALTER TABLE `m_jenis_mesin`
 ADD PRIMARY KEY (`id_jenis_mesin`), ADD KEY `fk_jenis_mesin_kelas_idx` (`id_kelas`), ADD KEY `fk_jenis_mesin_kategori_idx` (`id_kategori_mesin`);

--
-- Indexes for table `m_kartu`
--
ALTER TABLE `m_kartu`
 ADD PRIMARY KEY (`id_kartu`), ADD KEY `fk_kartu_cabang_idx` (`id_cabang`), ADD KEY `fk_kartu_kategori_katu_idx` (`id_kategori_kartu`);

--
-- Indexes for table `m_kategori_kartu`
--
ALTER TABLE `m_kategori_kartu`
 ADD PRIMARY KEY (`id_kategori_kartu`);

--
-- Indexes for table `m_kategori_mesin`
--
ALTER TABLE `m_kategori_mesin`
 ADD PRIMARY KEY (`id_kategori_mesin`);

--
-- Indexes for table `m_kelas`
--
ALTER TABLE `m_kelas`
 ADD PRIMARY KEY (`id_kelas`);

--
-- Indexes for table `m_koin`
--
ALTER TABLE `m_koin`
 ADD PRIMARY KEY (`id_koin`);

--
-- Indexes for table `m_kota`
--
ALTER TABLE `m_kota`
 ADD PRIMARY KEY (`id_kota`), ADD KEY `fk_kota_provinsi_idx` (`id_provinsi`);

--
-- Indexes for table `m_level`
--
ALTER TABLE `m_level`
 ADD PRIMARY KEY (`id_level`);

--
-- Indexes for table `m_menu`
--
ALTER TABLE `m_menu`
 ADD PRIMARY KEY (`kd_menu`), ADD KEY `fk_menu_group_menu_idx` (`kd_grup_menu`);

--
-- Indexes for table `m_mesin`
--
ALTER TABLE `m_mesin`
 ADD PRIMARY KEY (`id_mesin`), ADD KEY `fk_mesin_jenis_mesin_idx` (`id_jenis_mesin`), ADD KEY `fk_mesin_supplier_idx` (`id_supplier`), ADD KEY `fk_mesin_cabang_idx` (`id_cabang`), ADD KEY `fk_mesin_koin_idx` (`id_koin`), ADD KEY `fk_mesin_owner_idx` (`id_owner`);

--
-- Indexes for table `m_negara`
--
ALTER TABLE `m_negara`
 ADD PRIMARY KEY (`id_negara`), ADD UNIQUE KEY `nama_negara` (`nama_negara`);

--
-- Indexes for table `m_owner`
--
ALTER TABLE `m_owner`
 ADD PRIMARY KEY (`id_owner`);

--
-- Indexes for table `m_pegawai`
--
ALTER TABLE `m_pegawai`
 ADD PRIMARY KEY (`id_pegawai`), ADD KEY `fk_pegawai_user_idx` (`user_id`), ADD KEY `fk_pegawai_jabatan_idx` (`id_jabatan`);

--
-- Indexes for table `m_pegawai_cabang`
--
ALTER TABLE `m_pegawai_cabang`
 ADD PRIMARY KEY (`id_pegawai_cabang`);

--
-- Indexes for table `m_provinsi`
--
ALTER TABLE `m_provinsi`
 ADD PRIMARY KEY (`id_provinsi`), ADD KEY `fk_provinsi_pulau_idx` (`id_pulau`);

--
-- Indexes for table `m_pulau`
--
ALTER TABLE `m_pulau`
 ADD PRIMARY KEY (`id_pulau`), ADD KEY `fk_pulau_negara_idx` (`id_negara`);

--
-- Indexes for table `m_roles`
--
ALTER TABLE `m_roles`
 ADD PRIMARY KEY (`roles_id`), ADD UNIQUE KEY `roles_id` (`roles_id`), ADD KEY `fk_roles_group_idx` (`grup_id`);

--
-- Indexes for table `m_setting_hadiah`
--
ALTER TABLE `m_setting_hadiah`
 ADD PRIMARY KEY (`id_setting_hadiah`);

--
-- Indexes for table `m_sparepart`
--
ALTER TABLE `m_sparepart`
 ADD PRIMARY KEY (`id_sparepart`);

--
-- Indexes for table `m_stok_kartu`
--
ALTER TABLE `m_stok_kartu`
 ADD PRIMARY KEY (`id_stok_hadiah`), ADD KEY `fk_stok_kartu_cabang_idx` (`id_cabang`), ADD KEY `fk_stok_kartu_kartu_idx` (`id_kartu`);

--
-- Indexes for table `m_stok_koin`
--
ALTER TABLE `m_stok_koin`
 ADD PRIMARY KEY (`id_stok_hadiah`), ADD KEY `fk_stok_hadiah_cabang_idx` (`id_cabang`), ADD KEY `fk_stok_koin_koin_idx` (`id_koin`);

--
-- Indexes for table `m_stok_sparepart`
--
ALTER TABLE `m_stok_sparepart`
 ADD PRIMARY KEY (`id_stok_sparepart`), ADD KEY `fk_stok_hadiah_cabang_idx` (`id_cabang`), ADD KEY `fk_stok_sparepart_sparepart_idx` (`id_sparepart`);

--
-- Indexes for table `m_stok_tiket`
--
ALTER TABLE `m_stok_tiket`
 ADD PRIMARY KEY (`id_stok_tiket`), ADD KEY `fk_stok_hadiah_cabang_idx` (`id_cabang`), ADD KEY `fk_stok_tiket_tiket_idx` (`id_tiket`);

--
-- Indexes for table `m_supplier`
--
ALTER TABLE `m_supplier`
 ADD PRIMARY KEY (`id_supplier`);

--
-- Indexes for table `m_tiket`
--
ALTER TABLE `m_tiket`
 ADD PRIMARY KEY (`id_tiket`);

--
-- Indexes for table `m_user`
--
ALTER TABLE `m_user`
 ADD PRIMARY KEY (`user_id`), ADD KEY `grup` (`grup_id`);

--
-- Indexes for table `zn_ci_sessions`
--
ALTER TABLE `zn_ci_sessions`
 ADD PRIMARY KEY (`session_id`);

--
-- Indexes for table `zn_hadiah_keluar`
--
ALTER TABLE `zn_hadiah_keluar`
 ADD PRIMARY KEY (`id_hadiah_keluar`);

--
-- Indexes for table `zn_kartu_penjualan`
--
ALTER TABLE `zn_kartu_penjualan`
 ADD PRIMARY KEY (`id_kartu_penjualan`), ADD KEY `fk_kartu_penjualan_cabang_idx` (`id_cabang`), ADD KEY `fk_kartu_penjualan_kartu_idx` (`id_kartu`), ADD KEY `fk_kartu_penjualan_pegawai_idx` (`id_pegawai`);

--
-- Indexes for table `zn_kartu_topup`
--
ALTER TABLE `zn_kartu_topup`
 ADD PRIMARY KEY (`id_kartu_topup`), ADD KEY `fk_kartu_toptup_kartu_idx` (`id_kartu`), ADD KEY `fk_kartu_topup_cabang_idx` (`id_cabang`), ADD KEY `fk_kartu_topup_pegawai_idx` (`id_pegawai`);

--
-- Indexes for table `zn_log`
--
ALTER TABLE `zn_log`
 ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `zn_mesin_history`
--
ALTER TABLE `zn_mesin_history`
 ADD PRIMARY KEY (`id_mesin_history`), ADD KEY `fk_mesin_history_cabang_pengirim_idx` (`id_pengirim`), ADD KEY `fk_mesin_history_cabang_penerima_idx` (`id_penerima`), ADD KEY `fk_mesin_history_mesin_idx` (`id_mesin`);

--
-- Indexes for table `zn_mesin_kerusakan`
--
ALTER TABLE `zn_mesin_kerusakan`
 ADD PRIMARY KEY (`id_mesin_rusak`), ADD KEY `fk_mesin_kerusakan_mesin_idx` (`id_mesin`);

--
-- Indexes for table `zn_mesin_kerusakan_history`
--
ALTER TABLE `zn_mesin_kerusakan_history`
 ADD PRIMARY KEY (`id_rusak_history`), ADD KEY `fk_mesin_kerusakan_history_mesin_kerusakan_idx` (`id_mesin_rusak`), ADD KEY `fk_mesin_kerusakan_history_pegawai_idx` (`id_pegawai`);

--
-- Indexes for table `zn_pr_hadiah`
--
ALTER TABLE `zn_pr_hadiah`
 ADD PRIMARY KEY (`id_pr`), ADD KEY `fk_pr_hadiah_pengirim_idx` (`id_pengirim`), ADD KEY `fk_pr_hadiah_penerima_idx` (`id_tujuan`);

--
-- Indexes for table `zn_pr_hadiah_history`
--
ALTER TABLE `zn_pr_hadiah_history`
 ADD PRIMARY KEY (`id_pr_history`), ADD KEY `fk_pr_hadiah_history_idx` (`id_pr`), ADD KEY `fk_pr_hadiah_history_pegawai_idx` (`id_pegawai`);

--
-- Indexes for table `zn_prpo`
--
ALTER TABLE `zn_prpo`
 ADD PRIMARY KEY (`prpo_id`);

--
-- Indexes for table `zn_purchase_order`
--
ALTER TABLE `zn_purchase_order`
 ADD PRIMARY KEY (`po_id`);

--
-- Indexes for table `zn_purchase_order_detail`
--
ALTER TABLE `zn_purchase_order_detail`
 ADD PRIMARY KEY (`podetail_id`);

--
-- Indexes for table `zn_purchase_request`
--
ALTER TABLE `zn_purchase_request`
 ADD PRIMARY KEY (`pr_id`);

--
-- Indexes for table `zn_purchase_request_detail`
--
ALTER TABLE `zn_purchase_request_detail`
 ADD PRIMARY KEY (`prdetail_id`);

--
-- Indexes for table `zn_rusak_sparepart`
--
ALTER TABLE `zn_rusak_sparepart`
 ADD PRIMARY KEY (`id_rusak_sparepart`), ADD KEY `fk_rusak_mesin_rusak_idx` (`id_mesin_rusak`), ADD KEY `fk_rusak_sparepart_sparepart_idx` (`id_sparepart`);

--
-- Indexes for table `zn_service`
--
ALTER TABLE `zn_service`
 ADD PRIMARY KEY (`id_service`), ADD KEY `fk_service_mesin_rusak_idx` (`id_mesin_rusak`);

--
-- Indexes for table `zn_sparepart_keluar`
--
ALTER TABLE `zn_sparepart_keluar`
 ADD PRIMARY KEY (`id_sparepart_masuk`);

--
-- Indexes for table `zn_transaction`
--
ALTER TABLE `zn_transaction`
 ADD PRIMARY KEY (`transaction_id`), ADD KEY `transactiontype_id` (`transactiontype_id`), ADD KEY `rec_created` (`rec_created`), ADD KEY `rec_user` (`rec_user`);

--
-- Indexes for table `zn_transaction_bundle`
--
ALTER TABLE `zn_transaction_bundle`
 ADD PRIMARY KEY (`transactionbundle_id`);

--
-- Indexes for table `zn_transaction_device`
--
ALTER TABLE `zn_transaction_device`
 ADD PRIMARY KEY (`transactiondevice_id`);

--
-- Indexes for table `zn_transaction_document`
--
ALTER TABLE `zn_transaction_document`
 ADD PRIMARY KEY (`transactiondocument_id`);

--
-- Indexes for table `zn_transaction_machine`
--
ALTER TABLE `zn_transaction_machine`
 ADD PRIMARY KEY (`transactionmachine_id`);

--
-- Indexes for table `zn_transaction_meter`
--
ALTER TABLE `zn_transaction_meter`
 ADD PRIMARY KEY (`transactionmeter_id`);

--
-- Indexes for table `zn_transaction_redemption`
--
ALTER TABLE `zn_transaction_redemption`
 ADD PRIMARY KEY (`transactionredemption_id`);

--
-- Indexes for table `zn_transaction_refill`
--
ALTER TABLE `zn_transaction_refill`
 ADD PRIMARY KEY (`transactionrefill_id`);

--
-- Indexes for table `zn_transaction_repair`
--
ALTER TABLE `zn_transaction_repair`
 ADD PRIMARY KEY (`transactionrepair_id`);

--
-- Indexes for table `zn_transaction_service`
--
ALTER TABLE `zn_transaction_service`
 ADD PRIMARY KEY (`transactionservice_id`);

--
-- Indexes for table `zn_transaction_store`
--
ALTER TABLE `zn_transaction_store`
 ADD PRIMARY KEY (`transactionstore_id`);

--
-- Indexes for table `zn_transaction_type`
--
ALTER TABLE `zn_transaction_type`
 ADD PRIMARY KEY (`transactiontype_id`);

--
-- Indexes for table `zn_transaksi_mesin`
--
ALTER TABLE `zn_transaksi_mesin`
 ADD PRIMARY KEY (`id_transaksi_mesin`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `m_area`
--
ALTER TABLE `m_area`
MODIFY `id_area` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `m_cabang`
--
ALTER TABLE `m_cabang`
MODIFY `id_cabang` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `m_cabang_setting`
--
ALTER TABLE `m_cabang_setting`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `m_grup`
--
ALTER TABLE `m_grup`
MODIFY `grup_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `m_hadiah`
--
ALTER TABLE `m_hadiah`
MODIFY `id_hadiah` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `m_item`
--
ALTER TABLE `m_item`
MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `m_item_stok`
--
ALTER TABLE `m_item_stok`
MODIFY `itemstok_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `m_item_supplier`
--
ALTER TABLE `m_item_supplier`
MODIFY `itemsupplier_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=153;
--
-- AUTO_INCREMENT for table `m_item_supplier_default`
--
ALTER TABLE `m_item_supplier_default`
MODIFY `default_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `m_item_type`
--
ALTER TABLE `m_item_type`
MODIFY `itemtype_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `m_jabatan`
--
ALTER TABLE `m_jabatan`
MODIFY `id_jabatan` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `m_jenis_mesin`
--
ALTER TABLE `m_jenis_mesin`
MODIFY `id_jenis_mesin` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `m_kategori_kartu`
--
ALTER TABLE `m_kategori_kartu`
MODIFY `id_kategori_kartu` int(3) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_kategori_mesin`
--
ALTER TABLE `m_kategori_mesin`
MODIFY `id_kategori_mesin` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `m_kelas`
--
ALTER TABLE `m_kelas`
MODIFY `id_kelas` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `m_koin`
--
ALTER TABLE `m_koin`
MODIFY `id_koin` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `m_kota`
--
ALTER TABLE `m_kota`
MODIFY `id_kota` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `m_level`
--
ALTER TABLE `m_level`
MODIFY `id_level` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `m_menu`
--
ALTER TABLE `m_menu`
MODIFY `kd_menu` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=76;
--
-- AUTO_INCREMENT for table `m_mesin`
--
ALTER TABLE `m_mesin`
MODIFY `id_mesin` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `m_negara`
--
ALTER TABLE `m_negara`
MODIFY `id_negara` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `m_owner`
--
ALTER TABLE `m_owner`
MODIFY `id_owner` int(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `m_pegawai`
--
ALTER TABLE `m_pegawai`
MODIFY `id_pegawai` int(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `m_pegawai_cabang`
--
ALTER TABLE `m_pegawai_cabang`
MODIFY `id_pegawai_cabang` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `m_provinsi`
--
ALTER TABLE `m_provinsi`
MODIFY `id_provinsi` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `m_pulau`
--
ALTER TABLE `m_pulau`
MODIFY `id_pulau` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `m_roles`
--
ALTER TABLE `m_roles`
MODIFY `roles_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1103;
--
-- AUTO_INCREMENT for table `m_setting_hadiah`
--
ALTER TABLE `m_setting_hadiah`
MODIFY `id_setting_hadiah` int(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_sparepart`
--
ALTER TABLE `m_sparepart`
MODIFY `id_sparepart` int(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `m_stok_kartu`
--
ALTER TABLE `m_stok_kartu`
MODIFY `id_stok_hadiah` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_stok_koin`
--
ALTER TABLE `m_stok_koin`
MODIFY `id_stok_hadiah` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_stok_sparepart`
--
ALTER TABLE `m_stok_sparepart`
MODIFY `id_stok_sparepart` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_stok_tiket`
--
ALTER TABLE `m_stok_tiket`
MODIFY `id_stok_tiket` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_supplier`
--
ALTER TABLE `m_supplier`
MODIFY `id_supplier` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `m_tiket`
--
ALTER TABLE `m_tiket`
MODIFY `id_tiket` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `m_user`
--
ALTER TABLE `m_user`
MODIFY `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `zn_hadiah_keluar`
--
ALTER TABLE `zn_hadiah_keluar`
MODIFY `id_hadiah_keluar` int(8) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_kartu_penjualan`
--
ALTER TABLE `zn_kartu_penjualan`
MODIFY `id_kartu_penjualan` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_kartu_topup`
--
ALTER TABLE `zn_kartu_topup`
MODIFY `id_kartu_topup` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_log`
--
ALTER TABLE `zn_log`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_mesin_history`
--
ALTER TABLE `zn_mesin_history`
MODIFY `id_mesin_history` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_mesin_kerusakan`
--
ALTER TABLE `zn_mesin_kerusakan`
MODIFY `id_mesin_rusak` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `zn_mesin_kerusakan_history`
--
ALTER TABLE `zn_mesin_kerusakan_history`
MODIFY `id_rusak_history` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_pr_hadiah`
--
ALTER TABLE `zn_pr_hadiah`
MODIFY `id_pr` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `zn_pr_hadiah_history`
--
ALTER TABLE `zn_pr_hadiah_history`
MODIFY `id_pr_history` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `zn_prpo`
--
ALTER TABLE `zn_prpo`
MODIFY `prpo_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_purchase_order`
--
ALTER TABLE `zn_purchase_order`
MODIFY `po_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `zn_purchase_order_detail`
--
ALTER TABLE `zn_purchase_order_detail`
MODIFY `podetail_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `zn_purchase_request`
--
ALTER TABLE `zn_purchase_request`
MODIFY `pr_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `zn_purchase_request_detail`
--
ALTER TABLE `zn_purchase_request_detail`
MODIFY `prdetail_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `zn_rusak_sparepart`
--
ALTER TABLE `zn_rusak_sparepart`
MODIFY `id_rusak_sparepart` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_service`
--
ALTER TABLE `zn_service`
MODIFY `id_service` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `zn_sparepart_keluar`
--
ALTER TABLE `zn_sparepart_keluar`
MODIFY `id_sparepart_masuk` int(9) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_transaction`
--
ALTER TABLE `zn_transaction`
MODIFY `transaction_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=60;
--
-- AUTO_INCREMENT for table `zn_transaction_bundle`
--
ALTER TABLE `zn_transaction_bundle`
MODIFY `transactionbundle_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `zn_transaction_device`
--
ALTER TABLE `zn_transaction_device`
MODIFY `transactiondevice_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_transaction_document`
--
ALTER TABLE `zn_transaction_document`
MODIFY `transactiondocument_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `zn_transaction_machine`
--
ALTER TABLE `zn_transaction_machine`
MODIFY `transactionmachine_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_transaction_meter`
--
ALTER TABLE `zn_transaction_meter`
MODIFY `transactionmeter_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_transaction_redemption`
--
ALTER TABLE `zn_transaction_redemption`
MODIFY `transactionredemption_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_transaction_refill`
--
ALTER TABLE `zn_transaction_refill`
MODIFY `transactionrefill_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_transaction_repair`
--
ALTER TABLE `zn_transaction_repair`
MODIFY `transactionrepair_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_transaction_service`
--
ALTER TABLE `zn_transaction_service`
MODIFY `transactionservice_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_transaction_store`
--
ALTER TABLE `zn_transaction_store`
MODIFY `transactionstore_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT for table `zn_transaction_type`
--
ALTER TABLE `zn_transaction_type`
MODIFY `transactiontype_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `zn_transaksi_mesin`
--
ALTER TABLE `zn_transaksi_mesin`
MODIFY `id_transaksi_mesin` int(20) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `m_area`
--
ALTER TABLE `m_area`
ADD CONSTRAINT `zone2000_fk_area_kota` FOREIGN KEY (`id_kota`) REFERENCES `m_kota` (`id_kota`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `m_cabang`
--
ALTER TABLE `m_cabang`
ADD CONSTRAINT `zone2000_fk_cabang_area` FOREIGN KEY (`id_area`) REFERENCES `m_area` (`id_area`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_cabang_owner` FOREIGN KEY (`id_owner`) REFERENCES `m_owner` (`id_owner`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `m_cabang_setting`
--
ALTER TABLE `m_cabang_setting`
ADD CONSTRAINT `zone2000_fk_cabang_setting_cabang` FOREIGN KEY (`id_cabang`) REFERENCES `m_cabang` (`id_cabang`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `m_jenis_mesin`
--
ALTER TABLE `m_jenis_mesin`
ADD CONSTRAINT `zone2000_fk_jenis_mesin_kategori` FOREIGN KEY (`id_kategori_mesin`) REFERENCES `m_kategori_mesin` (`id_kategori_mesin`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_jenis_mesin_kelas` FOREIGN KEY (`id_kelas`) REFERENCES `m_kelas` (`id_kelas`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `m_kartu`
--
ALTER TABLE `m_kartu`
ADD CONSTRAINT `zone2000_fk_kartu_cabang` FOREIGN KEY (`id_cabang`) REFERENCES `m_cabang` (`id_cabang`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_kartu_kategori_katu` FOREIGN KEY (`id_kategori_kartu`) REFERENCES `m_kategori_kartu` (`id_kategori_kartu`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `m_kota`
--
ALTER TABLE `m_kota`
ADD CONSTRAINT `zone2000_fk_kota_provinsi` FOREIGN KEY (`id_provinsi`) REFERENCES `m_provinsi` (`id_provinsi`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `m_menu`
--
ALTER TABLE `m_menu`
ADD CONSTRAINT `zone2000_fk_menu_group_menu` FOREIGN KEY (`kd_grup_menu`) REFERENCES `m_grup_menu` (`kd_grup_menu`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `m_mesin`
--
ALTER TABLE `m_mesin`
ADD CONSTRAINT `zone2000_fk_mesin_cabang` FOREIGN KEY (`id_cabang`) REFERENCES `m_cabang` (`id_cabang`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_mesin_jenis_mesin` FOREIGN KEY (`id_jenis_mesin`) REFERENCES `m_jenis_mesin` (`id_jenis_mesin`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_mesin_koin` FOREIGN KEY (`id_koin`) REFERENCES `m_koin` (`id_koin`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_mesin_owner` FOREIGN KEY (`id_owner`) REFERENCES `m_owner` (`id_owner`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_mesin_supplier` FOREIGN KEY (`id_supplier`) REFERENCES `m_supplier` (`id_supplier`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `m_pegawai`
--
ALTER TABLE `m_pegawai`
ADD CONSTRAINT `zone2000_fk_pegawai_jabatan` FOREIGN KEY (`id_jabatan`) REFERENCES `m_jabatan` (`id_jabatan`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_pegawai_user` FOREIGN KEY (`user_id`) REFERENCES `m_user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `m_provinsi`
--
ALTER TABLE `m_provinsi`
ADD CONSTRAINT `zone2000_fk_provinsi_pulau` FOREIGN KEY (`id_pulau`) REFERENCES `m_pulau` (`id_pulau`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `m_pulau`
--
ALTER TABLE `m_pulau`
ADD CONSTRAINT `zone2000_fk_pulau_negara` FOREIGN KEY (`id_negara`) REFERENCES `m_negara` (`id_negara`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `m_roles`
--
ALTER TABLE `m_roles`
ADD CONSTRAINT `zone2000_fk_roles_group` FOREIGN KEY (`grup_id`) REFERENCES `m_grup` (`grup_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `m_stok_kartu`
--
ALTER TABLE `m_stok_kartu`
ADD CONSTRAINT `zone2000_fk_stok_kartu_cabang` FOREIGN KEY (`id_cabang`) REFERENCES `m_cabang` (`id_cabang`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_stok_kartu_kartu` FOREIGN KEY (`id_kartu`) REFERENCES `m_kartu` (`id_kartu`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `m_stok_koin`
--
ALTER TABLE `m_stok_koin`
ADD CONSTRAINT `zone2000_fk_stok_koin_cabang` FOREIGN KEY (`id_cabang`) REFERENCES `m_cabang` (`id_cabang`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_stok_koin_koin` FOREIGN KEY (`id_koin`) REFERENCES `m_koin` (`id_koin`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `m_stok_sparepart`
--
ALTER TABLE `m_stok_sparepart`
ADD CONSTRAINT `zone2000_fk_stok_sparepart_cabang` FOREIGN KEY (`id_cabang`) REFERENCES `m_cabang` (`id_cabang`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `m_stok_tiket`
--
ALTER TABLE `m_stok_tiket`
ADD CONSTRAINT `zone2000_fk_stok_tiket_cabang` FOREIGN KEY (`id_cabang`) REFERENCES `m_cabang` (`id_cabang`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_stok_tiket_tiket` FOREIGN KEY (`id_tiket`) REFERENCES `m_tiket` (`id_tiket`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `m_user`
--
ALTER TABLE `m_user`
ADD CONSTRAINT `zone2000_fk_user_group` FOREIGN KEY (`grup_id`) REFERENCES `m_grup` (`grup_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zn_kartu_penjualan`
--
ALTER TABLE `zn_kartu_penjualan`
ADD CONSTRAINT `zone2000_fk_kartu_penjualan_cabang` FOREIGN KEY (`id_cabang`) REFERENCES `m_cabang` (`id_cabang`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_kartu_penjualan_kartu` FOREIGN KEY (`id_kartu`) REFERENCES `m_kartu` (`id_kartu`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_kartu_penjualan_pegawai` FOREIGN KEY (`id_pegawai`) REFERENCES `m_pegawai` (`id_pegawai`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zn_kartu_topup`
--
ALTER TABLE `zn_kartu_topup`
ADD CONSTRAINT `zone2000_fk_kartu_toptup_kartu` FOREIGN KEY (`id_kartu`) REFERENCES `m_kartu` (`id_kartu`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_kartu_topup_cabang` FOREIGN KEY (`id_cabang`) REFERENCES `m_cabang` (`id_cabang`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_kartu_topup_pegawai` FOREIGN KEY (`id_pegawai`) REFERENCES `m_pegawai` (`id_pegawai`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zn_mesin_history`
--
ALTER TABLE `zn_mesin_history`
ADD CONSTRAINT `zone2000_fk_mesin_history_cabang_penerima` FOREIGN KEY (`id_penerima`) REFERENCES `m_cabang` (`id_cabang`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_mesin_history_cabang_pengirim` FOREIGN KEY (`id_pengirim`) REFERENCES `m_cabang` (`id_cabang`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_mesin_history_mesin` FOREIGN KEY (`id_mesin`) REFERENCES `m_mesin` (`id_mesin`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zn_mesin_kerusakan_history`
--
ALTER TABLE `zn_mesin_kerusakan_history`
ADD CONSTRAINT `zone2000_fk_mesin_kerusakan_history_mesin_kerusakan` FOREIGN KEY (`id_mesin_rusak`) REFERENCES `zn_mesin_kerusakan` (`id_mesin_rusak`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_mesin_kerusakan_history_pegawai` FOREIGN KEY (`id_pegawai`) REFERENCES `m_pegawai` (`id_pegawai`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zn_pr_hadiah`
--
ALTER TABLE `zn_pr_hadiah`
ADD CONSTRAINT `zone2000_fk_pr_hadiah_pengirim` FOREIGN KEY (`id_pengirim`) REFERENCES `m_cabang` (`id_cabang`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_pr_hadiah_tujuan` FOREIGN KEY (`id_tujuan`) REFERENCES `m_cabang` (`id_cabang`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zn_pr_hadiah_history`
--
ALTER TABLE `zn_pr_hadiah_history`
ADD CONSTRAINT `zone2000_fk_pr_hadiah_history` FOREIGN KEY (`id_pr`) REFERENCES `zn_pr_hadiah` (`id_pr`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zn_rusak_sparepart`
--
ALTER TABLE `zn_rusak_sparepart`
ADD CONSTRAINT `zone2000_fk_rusak_sparepart_mesin_rusak` FOREIGN KEY (`id_mesin_rusak`) REFERENCES `zn_mesin_kerusakan` (`id_mesin_rusak`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zn_service`
--
ALTER TABLE `zn_service`
ADD CONSTRAINT `zone2000_fk_service_mesin_rusak` FOREIGN KEY (`id_mesin_rusak`) REFERENCES `zn_mesin_kerusakan` (`id_mesin_rusak`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

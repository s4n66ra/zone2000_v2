-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 14, 2015 at 11:45 AM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `zone2000`
--

-- --------------------------------------------------------

--
-- Table structure for table `zn_transaction`
--

CREATE TABLE IF NOT EXISTS `zn_transaction` (
`transaction_id` int(11) NOT NULL,
  `transactiontype_id` int(11) DEFAULT NULL,
  `rec_username` varchar(50) DEFAULT NULL,
  `rec_created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_transaction_machine`
--

CREATE TABLE IF NOT EXISTS `zn_transaction_machine` (
`transactionmachine_id` int(11) NOT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `machine_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_transaction_meter`
--

CREATE TABLE IF NOT EXISTS `zn_transaction_meter` (
`transactionmeter_id` int(11) NOT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `num_initial` int(11) DEFAULT NULL,
  `num_current` int(11) DEFAULT NULL,
  `num_diff` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `zn_transaction`
--
ALTER TABLE `zn_transaction`
 ADD PRIMARY KEY (`transaction_id`);

--
-- Indexes for table `zn_transaction_machine`
--
ALTER TABLE `zn_transaction_machine`
 ADD PRIMARY KEY (`transactionmachine_id`);

--
-- Indexes for table `zn_transaction_meter`
--
ALTER TABLE `zn_transaction_meter`
 ADD PRIMARY KEY (`transactionmeter_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `zn_transaction`
--
ALTER TABLE `zn_transaction`
MODIFY `transaction_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_transaction_machine`
--
ALTER TABLE `zn_transaction_machine`
MODIFY `transactionmachine_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_transaction_meter`
--
ALTER TABLE `zn_transaction_meter`
MODIFY `transactionmeter_id` int(11) NOT NULL AUTO_INCREMENT;
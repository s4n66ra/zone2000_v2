-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 14, 2015 at 01:39 PM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `zone2000`
--

-- --------------------------------------------------------

--
-- Table structure for table `zn_transaction_store`
--

CREATE TABLE IF NOT EXISTS `zn_transaction_store` (
`transactionstore_id` int(11) NOT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `zn_transaction_store`
--
ALTER TABLE `zn_transaction_store`
 ADD PRIMARY KEY (`transactionstore_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `zn_transaction_store`
--
ALTER TABLE `zn_transaction_store`
MODIFY `transactionstore_id` int(11) NOT NULL AUTO_INCREMENT;
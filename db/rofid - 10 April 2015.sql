-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 10, 2015 at 06:42 AM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `zone2000`
--

-- --------------------------------------------------------

--
-- Table structure for table `zn_pr_hadiah`
--
DROP TABLE IF EXISTS `zn_po_hadiah_detail`;
DROP TABLE IF EXISTS `zn_po_hadiah`;
DROP TABLE IF EXISTS `zn_pr_hadiah_history_detail`;
DROP TABLE IF EXISTS `zn_pr_hadiah_history`;
DROP TABLE IF EXISTS `zn_pr_hadiah_detail`;
DROP TABLE IF EXISTS `zn_pr_hadiah`;

CREATE TABLE IF NOT EXISTS `zn_pr_hadiah` (
`id_pr` int(11) NOT NULL,
  `kd_pr` varchar(50) DEFAULT NULL,
  `kd_po` varchar(50) DEFAULT NULL,
  `nama_pr` varchar(250) DEFAULT NULL,
  `status_pr` int(2) DEFAULT '0' COMMENT '0 : draft\n1 : dibuat / waiting aproval\n2 : revisi\n3 : ditolak\n4 : diterima',
  `id_pengirim` int(3) DEFAULT NULL COMMENT 'cabang yang meminta PR',
  `id_tujuan` int(3) DEFAULT NULL COMMENT 'id store pusat yang dituju',
  `keterangan` text,
  `tgl_dibuat` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'tanggal PR ini dibuat',
  `tgl_diajukan` datetime DEFAULT NULL,
  `tgl_disetujui` datetime DEFAULT NULL,
  `tgl_ditolak` datetime DEFAULT NULL,
  `tgl_dikirim` datetime DEFAULT NULL,
  `tgl_diterima` datetime DEFAULT NULL,
  `tgl_revisi` datetime DEFAULT NULL,
  `count_revisi` int(2) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_pr_hadiah_detail`
--

CREATE TABLE IF NOT EXISTS `zn_pr_hadiah_detail` (
`id_pr_detail` int(11) NOT NULL,
  `id_pr` int(11) DEFAULT NULL,
  `id_hadiah` int(6) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `jml_diajukan` int(11) DEFAULT '0',
  `jml_disetujui` int(6) DEFAULT '0',
  `jml_dikirim` int(6) DEFAULT '0',
  `jml_diterima` int(6) DEFAULT '0',
  `keterangan` varchar(45) DEFAULT NULL,
  `jumlah` int(6) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_pr_hadiah_history`
--

CREATE TABLE IF NOT EXISTS `zn_pr_hadiah_history` (
`id_pr_history` int(11) NOT NULL,
  `tanggal` varchar(45) DEFAULT NULL,
  `status` int(1) DEFAULT NULL COMMENT '1 : dibuat/diajukan\n2 : revisi\n3 : ditolak\n4 : diterima\n',
  `id_pegawai` int(11) DEFAULT NULL,
  `id_pr` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_pr_hadiah_history_detail`
--


CREATE TABLE IF NOT EXISTS `zn_pr_hadiah_history_detail` (
`id_pr_history_detail` int(11) NOT NULL,
  `id_pr_history` int(11) DEFAULT NULL,
  `id_hadiah` int(6) DEFAULT NULL,
  `jumlah` int(5) DEFAULT NULL,
  `keterangan` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `zn_pr_hadiah`
--
ALTER TABLE `zn_pr_hadiah`
 ADD PRIMARY KEY (`id_pr`), ADD KEY `fk_pr_hadiah_pengirim_idx` (`id_pengirim`), ADD KEY `fk_pr_hadiah_penerima_idx` (`id_tujuan`);

--
-- Indexes for table `zn_pr_hadiah_detail`
--
ALTER TABLE `zn_pr_hadiah_detail`
 ADD PRIMARY KEY (`id_pr_detail`), ADD KEY `fk_fr_hadiah_detail_idx` (`id_pr`), ADD KEY `fk_fr_hadiah_hadiah_idx` (`id_hadiah`);

--
-- Indexes for table `zn_pr_hadiah_history`
--
ALTER TABLE `zn_pr_hadiah_history`
 ADD PRIMARY KEY (`id_pr_history`), ADD KEY `fk_pr_hadiah_history_idx` (`id_pr`), ADD KEY `fk_pr_hadiah_history_pegawai_idx` (`id_pegawai`);

--
-- Indexes for table `zn_pr_hadiah_history_detail`
--
ALTER TABLE `zn_pr_hadiah_history_detail`
 ADD PRIMARY KEY (`id_pr_history_detail`), ADD KEY `fk_pr_hadiah_history_detail_idx` (`id_pr_history`), ADD KEY `fk_pr_hadiah_history_detail_hadiah_idx` (`id_hadiah`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `zn_pr_hadiah`
--
ALTER TABLE `zn_pr_hadiah`
MODIFY `id_pr` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_pr_hadiah_detail`
--
ALTER TABLE `zn_pr_hadiah_detail`
MODIFY `id_pr_detail` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_pr_hadiah_history`
--
ALTER TABLE `zn_pr_hadiah_history`
MODIFY `id_pr_history` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_pr_hadiah_history_detail`
--
ALTER TABLE `zn_pr_hadiah_history_detail`
MODIFY `id_pr_history_detail` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `zn_pr_hadiah`
--
ALTER TABLE `zn_pr_hadiah`
ADD CONSTRAINT `zone2000_fk_pr_hadiah_pengirim` FOREIGN KEY (`id_pengirim`) REFERENCES `m_cabang` (`id_cabang`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_pr_hadiah_tujuan` FOREIGN KEY (`id_tujuan`) REFERENCES `m_cabang` (`id_cabang`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zn_pr_hadiah_detail`
--
ALTER TABLE `zn_pr_hadiah_detail`
ADD CONSTRAINT `zone2000_fk_fr_hadiah_detail` FOREIGN KEY (`id_pr`) REFERENCES `zn_pr_hadiah` (`id_pr`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_fr_hadiah_hadiah` FOREIGN KEY (`id_hadiah`) REFERENCES `m_hadiah` (`id_hadiah`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zn_pr_hadiah_history`
--
ALTER TABLE `zn_pr_hadiah_history`
ADD CONSTRAINT `zone2000_fk_pr_hadiah_history` FOREIGN KEY (`id_pr`) REFERENCES `zn_pr_hadiah` (`id_pr`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_pr_hadiah_history_pegawai` FOREIGN KEY (`id_pegawai`) REFERENCES `m_pegawai` (`id_pegawai`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zn_pr_hadiah_history_detail`
--
ALTER TABLE `zn_pr_hadiah_history_detail`
ADD CONSTRAINT `zone2000_fk_pr_hadiah_history_detail` FOREIGN KEY (`id_pr_history`) REFERENCES `zn_pr_hadiah_history` (`id_pr_history`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_pr_hadiah_history_detail_hadiah` FOREIGN KEY (`id_hadiah`) REFERENCES `m_hadiah` (`id_hadiah`) ON DELETE NO ACTION ON UPDATE NO ACTION;

-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 16, 2015 at 04:42 AM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `zone2000`
--

-- --------------------------------------------------------

--
-- Table structure for table `zn_transaction`
--

DROP TABLE IF EXISTS zn_transaction;
DROP TABLE IF EXISTS zn_transaction_device;
DROP TABLE IF EXISTS zn_transaction_machine;
DROP TABLE IF EXISTS zn_transaction_meter;
DROP TABLE IF EXISTS zn_transaction_store;

CREATE TABLE IF NOT EXISTS `zn_transaction` (
`transaction_id` int(11) NOT NULL,
  `transactiontype_id` int(11) DEFAULT NULL,
  `rec_user` int(11) DEFAULT NULL,
  `rec_created` datetime DEFAULT NULL,
  `num_meter` int(11) DEFAULT NULL,
  `num_coin` int(11) DEFAULT NULL,
  `num_ticket` int(11) DEFAULT NULL,
  `num_start` int(11) DEFAULT NULL,
  `num_end` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_transaction_device`
--

CREATE TABLE IF NOT EXISTS `zn_transaction_device` (
`transactiondevice_id` int(11) NOT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `device_id` int(11) DEFAULT NULL,
  `device_transaction_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_transaction_machine`
--

CREATE TABLE IF NOT EXISTS `zn_transaction_machine` (
`transactionmachine_id` int(11) NOT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `machine_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_transaction_meter`
--

CREATE TABLE IF NOT EXISTS `zn_transaction_meter` (
`transactionmeter_id` int(11) NOT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `num_initial` int(11) DEFAULT NULL,
  `num_current` int(11) DEFAULT NULL,
  `num_diff` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_transaction_store`
--

CREATE TABLE IF NOT EXISTS `zn_transaction_store` (
`transactionstore_id` int(11) NOT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `zn_transaction`
--
ALTER TABLE `zn_transaction`
 ADD PRIMARY KEY (`transaction_id`), ADD KEY `transactiontype_id` (`transactiontype_id`), ADD KEY `rec_created` (`rec_created`), ADD KEY `rec_user` (`rec_user`);

--
-- Indexes for table `zn_transaction_device`
--
ALTER TABLE `zn_transaction_device`
 ADD PRIMARY KEY (`transactiondevice_id`);

--
-- Indexes for table `zn_transaction_machine`
--
ALTER TABLE `zn_transaction_machine`
 ADD PRIMARY KEY (`transactionmachine_id`);

--
-- Indexes for table `zn_transaction_meter`
--
ALTER TABLE `zn_transaction_meter`
 ADD PRIMARY KEY (`transactionmeter_id`);

--
-- Indexes for table `zn_transaction_store`
--
ALTER TABLE `zn_transaction_store`
 ADD PRIMARY KEY (`transactionstore_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `zn_transaction`
--
ALTER TABLE `zn_transaction`
MODIFY `transaction_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_transaction_device`
--
ALTER TABLE `zn_transaction_device`
MODIFY `transactiondevice_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_transaction_machine`
--
ALTER TABLE `zn_transaction_machine`
MODIFY `transactionmachine_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_transaction_meter`
--
ALTER TABLE `zn_transaction_meter`
MODIFY `transactionmeter_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_transaction_store`
--
ALTER TABLE `zn_transaction_store`
MODIFY `transactionstore_id` int(11) NOT NULL AUTO_INCREMENT;
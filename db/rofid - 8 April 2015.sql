ALTER TABLE `zone2000`.`zn_pr_hadiah` 
CHANGE COLUMN `status_pr` `status_pr` INT(2) NULL DEFAULT 0 COMMENT '0 : draft\n1 : diajukan / waiting aproval\n2 : revisi\n3 : ditolak\n4 : diterima' ;

ALTER TABLE `zone2000`.`m_user` 
CHANGE COLUMN `user_id` `user_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ;

ALTER TABLE `zone2000`.`m_pegawai` 
ADD CONSTRAINT `zone2000_fk_pegawai_user`
  FOREIGN KEY (`user_id`)
  REFERENCES `zone2000`.`m_user` (`user_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

  ALTER TABLE `zone2000`.`zn_pr_hadiah` 
DROP COLUMN `tgl_diajukan`;

ALTER TABLE `zone2000`.`zn_pr_mesin_detail` 
ADD COLUMN `jumlah_disetujui` INT(6) NULL AFTER `jumlah`,
ADD COLUMN `jumlah_dikirim` INT(6) NULL AFTER `jumlah_disetujui`,
ADD COLUMN `jumlah_diterima` INT(6) NULL AFTER `jumlah_dikirim`;

ALTER TABLE `zone2000`.`zn_transaction` 
ADD COLUMN `device_id` INT NULL COMMENT 'device yang melakukan transaksi' AFTER `rec_created`,
ADD COLUMN `transaction_id_local` INT NULL COMMENT 'transaction id yang dilakukan di device di lokal store' AFTER `device_id`,
ADD COLUMN `num_meter` INT NULL AFTER `transaction_id_local`,
ADD COLUMN `num_coin` INT NULL AFTER `num_meter`,
ADD COLUMN `num_ticket` INT NULL AFTER `num_coin`,
ADD COLUMN `num_start` INT NULL AFTER `num_ticket`,
ADD COLUMN `num_end` INT NULL AFTER `num_start`;


ALTER TABLE `zone2000`.`zn_transaction_meter` 
CHANGE COLUMN `transactionmeter_id` `transactionchange_id` INT(11) NOT NULL AUTO_INCREMENT , RENAME TO  `zone2000`.`zn_transaction_change` ;

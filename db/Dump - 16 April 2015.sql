-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 16, 2015 at 05:02 PM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `zone2000`
--
CREATE DATABASE IF NOT EXISTS `zone2000` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `zone2000`;

-- --------------------------------------------------------

--
-- Table structure for table `m_area`
--

CREATE TABLE IF NOT EXISTS `m_area` (
`id_area` int(3) NOT NULL,
  `kd_area` varchar(20) NOT NULL,
  `nama_area` varchar(250) NOT NULL,
  `urutan` int(3) DEFAULT NULL,
  `id_kota` int(5) NOT NULL DEFAULT '1',
  `id_ka_area` int(6) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_area`
--

INSERT INTO `m_area` (`id_area`, `kd_area`, `nama_area`, `urutan`, `id_kota`, `id_ka_area`) VALUES
(1, 'Pas', 'Pasific Place', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_cabang`
--

CREATE TABLE IF NOT EXISTS `m_cabang` (
`id_cabang` int(3) NOT NULL,
  `kd_cabang` varchar(20) NOT NULL,
  `nama_cabang` varchar(250) NOT NULL,
  `urutan` int(3) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(250) NOT NULL,
  `fax` varchar(20) DEFAULT NULL,
  `date_start` date NOT NULL,
  `id_kepcabang` int(6) DEFAULT NULL,
  `id_owner` int(6) NOT NULL,
  `luas` int(6) NOT NULL,
  `pajak_toko` int(8) NOT NULL,
  `id_area` int(6) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_cabang`
--

INSERT INTO `m_cabang` (`id_cabang`, `kd_cabang`, `nama_cabang`, `urutan`, `alamat`, `telp`, `fax`, `date_start`, `id_kepcabang`, `id_owner`, `luas`, `pajak_toko`, `id_area`) VALUES
(10, 'ST001', 'Kantor Pusat', 0, 'Menteng', '021111', '021333', '2015-04-20', NULL, 2, 0, 0, 1),
(11, 'ST002', 'Gembira Playground', 0, 'Bandung Jalan Sarijadi blok 2 no.30', '+6285729067504', '', '0000-00-00', NULL, 1, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_cabang_setting`
--

CREATE TABLE IF NOT EXISTS `m_cabang_setting` (
`id` int(11) NOT NULL,
  `id_cabang` int(3) NOT NULL,
  `merchandise_ratio` int(5) NOT NULL,
  `store_area` int(5) NOT NULL,
  `rent_charge` int(11) NOT NULL,
  `service_charge` int(11) NOT NULL,
  `store_owner` varchar(50) DEFAULT NULL,
  `coin_regular` int(11) NOT NULL,
  `coin_acrylic` int(11) NOT NULL,
  `wristband` int(11) NOT NULL,
  `service_charge_total` int(11) NOT NULL,
  `budget` int(11) NOT NULL,
  `target` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_cabang_setting`
--

INSERT INTO `m_cabang_setting` (`id`, `id_cabang`, `merchandise_ratio`, `store_area`, `rent_charge`, `service_charge`, `store_owner`, `coin_regular`, `coin_acrylic`, `wristband`, `service_charge_total`, `budget`, `target`) VALUES
(9, 10, 30, 30, 30, 20, NULL, 20, 20, 20, 0, 200000, 200000),
(10, 11, 200, 200, 200, 100, NULL, 21, 21, 21, 0, 3000, 3000);

-- --------------------------------------------------------

--
-- Table structure for table `m_grup`
--

CREATE TABLE IF NOT EXISTS `m_grup` (
`grup_id` int(10) unsigned NOT NULL,
  `grup_nama` varchar(25) DEFAULT NULL,
  `grup_deskripsi` varchar(50) DEFAULT NULL,
  `otoritas_data` char(1) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_grup`
--

INSERT INTO `m_grup` (`grup_id`, `grup_nama`, `grup_deskripsi`, `otoritas_data`) VALUES
(15, 'Super Admin', 'Super Admin', 'D'),
(16, 'Produksi', 'produksi', '0'),
(17, 'Akuntansi', 'akuntansi', '0'),
(18, 'Direksi', 'Direksi', '0'),
(19, 'Logistik', 'Logistik', '0');

-- --------------------------------------------------------

--
-- Table structure for table `m_grup_menu`
--

CREATE TABLE IF NOT EXISTS `m_grup_menu` (
  `kd_grup_menu` int(10) unsigned NOT NULL,
  `nama_grup_menu` varchar(50) DEFAULT NULL,
  `urutan` smallint(6) DEFAULT NULL,
  `icon` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_grup_menu`
--

INSERT INTO `m_grup_menu` (`kd_grup_menu`, `nama_grup_menu`, `urutan`, `icon`) VALUES
(0, 'Purchase Request', NULL, '0'),
(1, 'Master Data', 11, 'icon-folder-open-alt'),
(2, 'Pengelolaan Pengguna', 12, 'icon-user'),
(10, 'Transaksi', NULL, 'icon-fighter-jet');

-- --------------------------------------------------------

--
-- Table structure for table `m_hadiah`
--

CREATE TABLE IF NOT EXISTS `m_hadiah` (
`id_hadiah` int(4) NOT NULL,
  `kd_hadiah` varchar(50) DEFAULT NULL,
  `nama_hadiah` varchar(250) DEFAULT NULL,
  `harga` int(8) DEFAULT NULL,
  `jumlah_tiket` int(5) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_key` varchar(200) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_hadiah`
--

INSERT INTO `m_hadiah` (`id_hadiah`, `kd_hadiah`, `nama_hadiah`, `harga`, `jumlah_tiket`, `item_id`, `item_key`) VALUES
(1, 'MC0001', 'Boneka Beruang Coklat Besar', 80000, 70, 0, '1111'),
(2, 'MC0002', 'Plush Toy', 40000, 90, 0, '2222'),
(3, 'MC0003', 'Boneka Cony Line', 94000, 100, 0, '3333'),
(4, 'MC001', 'Boneka Brown Besar', 90000, 90, 1, '4444');

-- --------------------------------------------------------

--
-- Table structure for table `m_item`
--

CREATE TABLE IF NOT EXISTS `m_item` (
`item_id` int(11) NOT NULL,
  `item_key` varchar(200) DEFAULT NULL,
  `item_name` varchar(50) DEFAULT NULL,
  `itemtype_id` int(11) DEFAULT NULL COMMENT '1 : Mesin\n2 : Merchandise\n3 : Sparepart\n4 : Ticket\n5 : Coin'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_item`
--

INSERT INTO `m_item` (`item_id`, `item_key`, `item_name`, `itemtype_id`) VALUES
(1, '4444', 'Boneka Brown Besar', 2),
(2, '1111', 'Boneka Beruang Coklat Besar', 2),
(3, '2222', 'Plush Toy', 2),
(4, '3333', 'Boneka Cony Line', 2),
(7, '1199283', 'Tombol Kaizer', 3),
(8, '1199281', NULL, 2);

-- --------------------------------------------------------

--
-- Table structure for table `m_item_stok`
--

CREATE TABLE IF NOT EXISTS `m_item_stok` (
`itemstok_id` int(11) NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL,
  `num_stok` int(11) DEFAULT NULL,
  `num_total` int(11) DEFAULT NULL COMMENT 'stok total dari awal sampe sekarang'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_item_type`
--

CREATE TABLE IF NOT EXISTS `m_item_type` (
`itemtype_id` int(11) NOT NULL,
  `type_code` varchar(10) DEFAULT NULL,
  `type_name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_jabatan`
--

CREATE TABLE IF NOT EXISTS `m_jabatan` (
`id_jabatan` int(4) NOT NULL,
  `kd_jabatan` varchar(50) DEFAULT NULL,
  `nama_jabatan` varchar(250) DEFAULT NULL,
  `urutan` int(3) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_jabatan`
--

INSERT INTO `m_jabatan` (`id_jabatan`, `kd_jabatan`, `nama_jabatan`, `urutan`) VALUES
(1, '001', 'Manager', 0);

-- --------------------------------------------------------

--
-- Table structure for table `m_jenis_mesin`
--

CREATE TABLE IF NOT EXISTS `m_jenis_mesin` (
`id_jenis_mesin` int(4) NOT NULL,
  `kd_jenis_mesin` varchar(40) NOT NULL,
  `nama_jenis_mesin` varchar(50) NOT NULL,
  `deskripsi` varchar(100) DEFAULT NULL,
  `thumbnail` varchar(50) DEFAULT NULL,
  `foto` varchar(50) DEFAULT NULL,
  `jenis_koin` int(2) DEFAULT NULL,
  `id_kategori_mesin` int(3) NOT NULL,
  `id_kelas` int(3) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_jenis_mesin`
--

INSERT INTO `m_jenis_mesin` (`id_jenis_mesin`, `kd_jenis_mesin`, `nama_jenis_mesin`, `deskripsi`, `thumbnail`, `foto`, `jenis_koin`, `id_kategori_mesin`, `id_kelas`) VALUES
(1, '001', 'SEGA RALLY', 'game balapan', NULL, NULL, 1, 4, 2),
(2, '002', 'Cruizer Z 2034', 'keren', NULL, NULL, 1, 4, 2),
(3, '000', 'SEGA RALLY OLD', NULL, NULL, NULL, 1, 4, 2);

-- --------------------------------------------------------

--
-- Table structure for table `m_kartu`
--

CREATE TABLE IF NOT EXISTS `m_kartu` (
  `id_kartu` int(2) NOT NULL,
  `kd_kartu` varchar(50) DEFAULT NULL,
  `nama_pemilik` varchar(50) DEFAULT NULL,
  `id_cabang` int(3) DEFAULT NULL,
  `id_kategori_kartu` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_kategori_kartu`
--

CREATE TABLE IF NOT EXISTS `m_kategori_kartu` (
`id_kategori_kartu` int(3) NOT NULL,
  `kd_kategori_kartu` varchar(40) NOT NULL,
  `nama_kategori_kartu` varchar(250) NOT NULL,
  `harga` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_kategori_mesin`
--

CREATE TABLE IF NOT EXISTS `m_kategori_mesin` (
`id_kategori_mesin` int(3) NOT NULL,
  `kd_kategori_mesin` varchar(20) NOT NULL,
  `nama_kategori_mesin` varchar(250) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_kategori_mesin`
--

INSERT INTO `m_kategori_mesin` (`id_kategori_mesin`, `kd_kategori_mesin`, `nama_kategori_mesin`) VALUES
(4, '001', 'Mesin Pusher'),
(5, '002', 'Mesin Redemption'),
(6, '003', 'Mesin Vending'),
(7, '004', 'Fixed Game'),
(8, '005', 'Large Fixed Game'),
(9, '006', 'Large Game');

-- --------------------------------------------------------

--
-- Table structure for table `m_kelas`
--

CREATE TABLE IF NOT EXISTS `m_kelas` (
`id_kelas` int(3) NOT NULL,
  `kd_kelas` varchar(40) NOT NULL,
  `nama_kelas` varchar(250) NOT NULL,
  `min_value` int(20) NOT NULL,
  `max_value` int(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_kelas`
--

INSERT INTO `m_kelas` (`id_kelas`, `kd_kelas`, `nama_kelas`, `min_value`, `max_value`) VALUES
(2, 'A', 'Kelas A', 3000001, 999000000),
(3, 'B', 'Kelas B', 2000001, 3000000),
(4, 'C', 'Kelas C', 1000001, 2000000),
(5, 'D', 'Kelas D', 1, 1000000);

-- --------------------------------------------------------

--
-- Table structure for table `m_koin`
--

CREATE TABLE IF NOT EXISTS `m_koin` (
`id_koin` int(2) NOT NULL,
  `kd_koin` varchar(50) DEFAULT NULL,
  `nama_koin` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_koin`
--

INSERT INTO `m_koin` (`id_koin`, `kd_koin`, `nama_koin`) VALUES
(1, 'ACR', 'Acrylic'),
(2, 'WRY', 'Wristband'),
(3, 'REG', 'Regular');

-- --------------------------------------------------------

--
-- Table structure for table `m_kota`
--

CREATE TABLE IF NOT EXISTS `m_kota` (
`id_kota` int(5) NOT NULL,
  `nama_kota` varchar(250) DEFAULT NULL,
  `urutan` int(5) DEFAULT NULL,
  `id_provinsi` int(3) DEFAULT NULL,
  `kd_kota` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_kota`
--

INSERT INTO `m_kota` (`id_kota`, `nama_kota`, `urutan`, `id_provinsi`, `kd_kota`) VALUES
(1, 'Jakarta Pusat', 1, 1, 'JAK'),
(2, 'Jakarta Selatan', 2, 1, 'Jaksel');

-- --------------------------------------------------------

--
-- Table structure for table `m_level`
--

CREATE TABLE IF NOT EXISTS `m_level` (
`id_level` int(3) NOT NULL,
  `nama_level` varchar(250) DEFAULT NULL,
  `kd_level` varchar(50) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_menu`
--

CREATE TABLE IF NOT EXISTS `m_menu` (
`kd_menu` int(11) NOT NULL,
  `kd_grup_menu` int(11) unsigned DEFAULT NULL,
  `nama_menu` varchar(50) DEFAULT NULL,
  `deskripsi` varchar(50) DEFAULT NULL,
  `menu_level` smallint(6) DEFAULT NULL,
  `menu_urutan` smallint(6) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `child_grup` smallint(6) DEFAULT NULL,
  `kd_parent` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_menu`
--

INSERT INTO `m_menu` (`kd_menu`, `kd_grup_menu`, `nama_menu`, `deskripsi`, `menu_level`, `menu_urutan`, `url`, `child_grup`, `kd_parent`) VALUES
(1, 1, 'Menu Aplikasi', '', NULL, 3, 'menu', NULL, 6),
(3, 1, 'Group Menu', '', NULL, 4, 'menu_group', NULL, 6),
(4, 2, 'Roles', '', NULL, 4, 'roles', NULL, NULL),
(5, 2, 'user', '', NULL, 3, 'user', NULL, NULL),
(6, 1, 'Menu', '', NULL, 5, '', NULL, NULL),
(48, 1, 'Pulau', '', NULL, 6, 'pulau', NULL, NULL),
(49, 1, 'Provinsi', '', NULL, 7, 'provinsi', NULL, NULL),
(50, 1, 'Area', '', NULL, 8, 'area', NULL, NULL),
(51, 1, 'Kategori Mesin', '', NULL, 9, 'kategori_mesin', NULL, NULL),
(52, 1, 'Supplier', '', NULL, 10, 'supplier', NULL, NULL),
(53, 1, 'Kelas', '', NULL, 11, 'kelas', NULL, NULL),
(55, 1, 'Mesin', '', NULL, 13, 'mesin', NULL, NULL),
(56, 1, 'Cabang', '', NULL, 14, 'cabang', NULL, NULL),
(57, 1, 'Jabatan', '', NULL, 15, 'jabatan', NULL, NULL),
(58, 1, 'Pegawai', '', NULL, 16, 'pegawai', NULL, NULL),
(59, 1, 'Level', '', NULL, 17, 'level', NULL, NULL),
(60, 10, 'Purchase Order', '', NULL, 1, 'po', NULL, NULL),
(61, 1, 'Setting Hadiah', '', NULL, 18, 'setting_hadiah', NULL, NULL),
(62, 1, 'Hadiah', '', NULL, 19, 'hadiah', NULL, NULL),
(63, 1, 'Sparepart', '', NULL, 20, 'sparepart', NULL, NULL),
(64, 1, 'Status Mutasi Mesin', '', NULL, 21, 'status_mutasi_mesin', NULL, NULL),
(65, 1, 'Strore', '', NULL, 22, 'store', NULL, NULL),
(66, 1, 'Machine Type', '', NULL, 23, 'jenis_mesin', NULL, NULL),
(67, 0, 'Merchandise', 'purchase request for hadiah', NULL, 2, 'pr_hadiah', NULL, NULL),
(68, 0, 'Sparepart', '', NULL, 3, 'pr_sparepart', NULL, NULL),
(69, 10, 'Machine Reparation', '', NULL, 2, 'mesin_kerusakan', NULL, NULL),
(70, 10, 'Machine Service', '', NULL, 3, 'service', NULL, NULL),
(71, 1, 'Ticket', '', NULL, 24, 'tiket', NULL, NULL),
(72, 10, 'Machine Transaction', '', NULL, 4, 'machine_transaction', NULL, NULL),
(73, 1, 'Employee & Store', '', NULL, 25, 'pegawai_cabang', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `m_mesin`
--

CREATE TABLE IF NOT EXISTS `m_mesin` (
`id_mesin` int(5) NOT NULL,
  `kd_mesin` varchar(250) NOT NULL,
  `serial_number` varchar(50) DEFAULT NULL,
  `harga` int(20) NOT NULL,
  `tgl_beli` date NOT NULL,
  `garansi_toko` text NOT NULL,
  `status` int(1) DEFAULT '1' COMMENT 'status\n1 : fine\n0 : rusak',
  `id_supplier` int(3) NOT NULL COMMENT 'id_supplier , harga , dan tgl_beli merupakan duplikasi data dari tabel pengadaan detail',
  `id_jenis_mesin` int(4) DEFAULT NULL,
  `id_cabang` int(3) NOT NULL,
  `id_koin` int(2) DEFAULT NULL COMMENT 'jenis koin nya apa \n',
  `id_owner` int(6) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_mesin`
--

INSERT INTO `m_mesin` (`id_mesin`, `kd_mesin`, `serial_number`, `harga`, `tgl_beli`, `garansi_toko`, `status`, `id_supplier`, `id_jenis_mesin`, `id_cabang`, `id_koin`, `id_owner`) VALUES
(1, 'M001', '0987609876', 500000, '2015-04-01', '2015-08-12', 0, 9, 1, 11, 2, 2),
(2, 'M002', '882763623', 5000000, '0000-00-00', '2015-04-24', 0, 9, 2, 11, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `m_negara`
--

CREATE TABLE IF NOT EXISTS `m_negara` (
`id_negara` int(11) NOT NULL,
  `nama_negara` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_negara`
--

INSERT INTO `m_negara` (`id_negara`, `nama_negara`) VALUES
(1, 'Indonesia'),
(2, 'Malaysia');

-- --------------------------------------------------------

--
-- Table structure for table `m_owner`
--

CREATE TABLE IF NOT EXISTS `m_owner` (
`id_owner` int(6) NOT NULL,
  `kd_owner` varchar(50) DEFAULT NULL,
  `nama_owner` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_owner`
--

INSERT INTO `m_owner` (`id_owner`, `kd_owner`, `nama_owner`) VALUES
(1, 'RAM', 'RAMAYANA MAKMUR SENTOSA'),
(2, 'MUL', 'PT. MULKINDO PERKASA');

-- --------------------------------------------------------

--
-- Table structure for table `m_pegawai`
--

CREATE TABLE IF NOT EXISTS `m_pegawai` (
`id_pegawai` int(6) NOT NULL,
  `nik` varchar(50) DEFAULT NULL,
  `nama_pegawai` varchar(250) DEFAULT NULL,
  `no_hp` varchar(50) DEFAULT NULL,
  `alamat` text,
  `id_jabatan` int(4) DEFAULT NULL,
  `id_level` int(4) DEFAULT NULL,
  `tgl_masuk` date DEFAULT NULL,
  `tgl_keluar` date DEFAULT NULL,
  `salary` int(10) DEFAULT NULL,
  `status_pegawai` int(3) DEFAULT NULL,
  `keterangan` text,
  `user_id` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_pegawai`
--

INSERT INTO `m_pegawai` (`id_pegawai`, `nik`, `nama_pegawai`, `no_hp`, `alamat`, `id_jabatan`, `id_level`, `tgl_masuk`, `tgl_keluar`, `salary`, `status_pegawai`, `keterangan`, `user_id`) VALUES
(1, '220091100841', 'sanggra', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, '3273212709830002', 'Joko', '081222', 'JKT', 1, 1, '2015-03-25', '2015-03-28', 9, 1, 'OKE', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `m_pegawai_cabang`
--

CREATE TABLE IF NOT EXISTS `m_pegawai_cabang` (
`id_pegawai_cabang` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  `id_cabang` int(11) NOT NULL,
  `is_active` int(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_pegawai_cabang`
--

INSERT INTO `m_pegawai_cabang` (`id_pegawai_cabang`, `id_pegawai`, `id_cabang`, `is_active`) VALUES
(36, 1, 11, 0),
(37, 3, 10, 0);

-- --------------------------------------------------------

--
-- Table structure for table `m_provinsi`
--

CREATE TABLE IF NOT EXISTS `m_provinsi` (
`id_provinsi` int(3) NOT NULL,
  `nama_provinsi` varchar(250) NOT NULL,
  `kd_provinsi` varchar(20) NOT NULL,
  `urutan` int(3) NOT NULL,
  `id_pulau` int(3) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_provinsi`
--

INSERT INTO `m_provinsi` (`id_provinsi`, `nama_provinsi`, `kd_provinsi`, `urutan`, `id_pulau`) VALUES
(1, 'Jakarta', '001', 0, 2),
(2, 'Jawa Barat', '002', 0, 3);

-- --------------------------------------------------------

--
-- Table structure for table `m_pulau`
--

CREATE TABLE IF NOT EXISTS `m_pulau` (
`id_pulau` int(3) NOT NULL,
  `nama_pulau` varchar(250) NOT NULL,
  `kd_pulau` varchar(20) NOT NULL,
  `id_negara` int(11) NOT NULL DEFAULT '1',
  `urutan` int(3) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_pulau`
--

INSERT INTO `m_pulau` (`id_pulau`, `nama_pulau`, `kd_pulau`, `id_negara`, `urutan`) VALUES
(1, 'Sumatra', '001', 1, 0),
(2, 'Jawa', '002', 1, 0),
(3, 'Kalimantan', '003', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `m_roles`
--

CREATE TABLE IF NOT EXISTS `m_roles` (
`roles_id` bigint(20) unsigned NOT NULL,
  `menu_id` int(11) DEFAULT NULL,
  `grup_id` int(11) unsigned DEFAULT NULL,
  `is_view` varchar(1) DEFAULT NULL,
  `is_add` varchar(10) DEFAULT NULL,
  `is_edit` varchar(1) DEFAULT NULL,
  `is_delete` varchar(1) DEFAULT NULL,
  `is_approve` varchar(1) DEFAULT NULL,
  `is_import` varchar(1) DEFAULT NULL,
  `is_print` varchar(1) DEFAULT NULL,
  `act` varchar(1) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=976 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_roles`
--

INSERT INTO `m_roles` (`roles_id`, `menu_id`, `grup_id`, `is_view`, `is_add`, `is_edit`, `is_delete`, `is_approve`, `is_import`, `is_print`, `act`) VALUES
(946, 67, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(947, 68, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(948, 60, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(949, 69, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(950, 70, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(951, 72, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(952, 6, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(953, 1, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(954, 3, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(955, 48, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(956, 49, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(957, 50, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(958, 51, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(959, 52, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(960, 53, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(961, 55, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(962, 56, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(963, 57, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(964, 58, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(965, 59, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(966, 61, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(967, 62, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(968, 63, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(969, 64, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(970, 65, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(971, 66, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(972, 71, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(973, 73, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(974, 5, 15, 't', 't', 't', 't', 't', 't', 't', NULL),
(975, 4, 15, 't', 't', 't', 't', 't', 't', 't', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `m_setting_hadiah`
--

CREATE TABLE IF NOT EXISTS `m_setting_hadiah` (
`id_setting_hadiah` int(4) NOT NULL,
  `id_lokasi` int(3) DEFAULT NULL,
  `id_hadiah` int(3) DEFAULT NULL,
  `jumlah_tiket` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_sparepart`
--

CREATE TABLE IF NOT EXISTS `m_sparepart` (
`id_sparepart` int(6) NOT NULL,
  `nama_sparepart` varchar(250) DEFAULT NULL,
  `kd_sparepart` varchar(50) DEFAULT NULL,
  `harga` int(11) DEFAULT '0',
  `item_id` int(11) DEFAULT NULL,
  `item_key` varchar(200) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_sparepart`
--

INSERT INTO `m_sparepart` (`id_sparepart`, `nama_sparepart`, `kd_sparepart`, `harga`, `item_id`, `item_key`) VALUES
(1, 'Tombol Kaizer', 'SP001', 90000, 7, '1199281');

-- --------------------------------------------------------

--
-- Table structure for table `m_stok_hadiah`
--

CREATE TABLE IF NOT EXISTS `m_stok_hadiah` (
`id_stok_hadiah` int(11) NOT NULL,
  `kd_stok_hadiah` varchar(50) DEFAULT NULL,
  `id_hadiah` int(6) DEFAULT NULL,
  `id_cabang` int(3) DEFAULT NULL,
  `stok_total` int(11) DEFAULT '0',
  `stok_gudang` int(11) DEFAULT '0',
  `stok_mesin` int(11) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_stok_hadiah`
--

INSERT INTO `m_stok_hadiah` (`id_stok_hadiah`, `kd_stok_hadiah`, `id_hadiah`, `id_cabang`, `stok_total`, `stok_gudang`, `stok_mesin`) VALUES
(1, NULL, 1, 11, 30, 30, 0),
(2, NULL, 2, 11, 5, 5, 0),
(3, NULL, 3, 11, 5, 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `m_stok_kartu`
--

CREATE TABLE IF NOT EXISTS `m_stok_kartu` (
`id_stok_hadiah` int(11) NOT NULL,
  `kd_stok_hadiah` varchar(50) DEFAULT NULL,
  `id_kartu` int(2) DEFAULT NULL,
  `id_cabang` int(3) DEFAULT NULL,
  `stok_total` int(11) DEFAULT '0',
  `stok_gudang` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_stok_koin`
--

CREATE TABLE IF NOT EXISTS `m_stok_koin` (
`id_stok_hadiah` int(11) NOT NULL,
  `kd_stok_hadiah` varchar(50) DEFAULT NULL,
  `id_koin` int(2) DEFAULT NULL,
  `id_cabang` int(3) DEFAULT NULL,
  `stok_total` int(11) DEFAULT '0',
  `stok_gudang` int(11) DEFAULT '0',
  `stok_mesin` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_stok_sparepart`
--

CREATE TABLE IF NOT EXISTS `m_stok_sparepart` (
`id_stok_sparepart` int(11) NOT NULL,
  `kd_stok_sparepart` varchar(50) DEFAULT NULL,
  `id_sparepart` int(2) DEFAULT NULL,
  `id_cabang` int(3) DEFAULT NULL,
  `stok_total` int(11) DEFAULT '0',
  `stok_gudang` int(11) DEFAULT '0',
  `stok_mesin` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_stok_tiket`
--

CREATE TABLE IF NOT EXISTS `m_stok_tiket` (
`id_stok_tiket` int(11) NOT NULL,
  `kd_stok_tiket` varchar(50) DEFAULT NULL,
  `id_tiket` int(2) DEFAULT NULL,
  `id_cabang` int(3) DEFAULT NULL,
  `stok_total` int(11) DEFAULT '0',
  `stok_gudang` int(11) DEFAULT NULL,
  `stok_mesin` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_supplier`
--

CREATE TABLE IF NOT EXISTS `m_supplier` (
`id_supplier` int(3) NOT NULL,
  `nama_supplier` varchar(250) NOT NULL,
  `kd_supplier` varchar(20) NOT NULL,
  `urutan` int(3) NOT NULL,
  `alamat_supplier` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_supplier`
--

INSERT INTO `m_supplier` (`id_supplier`, `nama_supplier`, `kd_supplier`, `urutan`, `alamat_supplier`) VALUES
(9, 'PT Indonesia Juara', 'S001', 0, 'Jakarta');

-- --------------------------------------------------------

--
-- Table structure for table `m_tiket`
--

CREATE TABLE IF NOT EXISTS `m_tiket` (
`id_tiket` int(2) NOT NULL,
  `kd_tiket` varchar(50) DEFAULT NULL,
  `nama_tiket` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_tiket`
--

INSERT INTO `m_tiket` (`id_tiket`, `kd_tiket`, `nama_tiket`) VALUES
(1, '001', 'Regular Ticket');

-- --------------------------------------------------------

--
-- Table structure for table `m_user`
--

CREATE TABLE IF NOT EXISTS `m_user` (
`user_id` int(10) unsigned NOT NULL,
  `nik` varchar(20) DEFAULT NULL,
  `grup_id` int(11) unsigned DEFAULT NULL,
  `user_name` varchar(25) DEFAULT NULL,
  `user_password` varchar(50) DEFAULT NULL,
  `last_update` date DEFAULT NULL,
  `is_active` char(1) DEFAULT NULL,
  `last_login` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_user`
--

INSERT INTO `m_user` (`user_id`, `nik`, `grup_id`, `user_name`, `user_password`, `last_update`, `is_active`, `last_login`) VALUES
(1, '220091100841', 15, 'admin', '21232f297a57a5a743894a0e4a801fc3', '2015-03-01', '1', '2015-04-16'),
(2, '1001', 16, 'produksi', 'edf3017a2946290b95c783bd1a7f0ba7', '2015-03-06', '1', '2015-03-11'),
(3, '1002', 17, 'akuntansi', '1139f90d50ba3bb7ff4b2602ad03aa26', '2015-03-01', '1', '2015-04-16'),
(4, '1003', 18, 'direksi', '18a5726d8227b237064ecef7d1f4e634', '2015-03-01', '1', '2015-04-16'),
(5, '1004', 19, 'logistik', 'cb1f02561c07f62717a4814c048a6239', '2015-01-21', '1', '2015-04-16');

-- --------------------------------------------------------

--
-- Table structure for table `testing`
--

CREATE TABLE IF NOT EXISTS `testing` (
`id` int(11) NOT NULL,
  `date` datetime DEFAULT CURRENT_TIMESTAMP,
  `value` text
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testing`
--

INSERT INTO `testing` (`id`, `date`, `value`) VALUES
(1, '2015-04-14 17:52:19', NULL),
(2, '2015-04-14 17:52:31', NULL),
(3, '2015-04-14 17:53:10', NULL),
(4, '2015-04-14 17:53:44', NULL),
(5, '2015-04-14 17:58:39', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `zn_ci_sessions`
--

CREATE TABLE IF NOT EXISTS `zn_ci_sessions` (
  `session_id` varchar(40) NOT NULL,
  `ip_address` varchar(50) NOT NULL,
  `last_activity` bigint(20) NOT NULL,
  `user_data` text NOT NULL,
  `user_agent` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_hadiah_keluar`
--

CREATE TABLE IF NOT EXISTS `zn_hadiah_keluar` (
`id_hadiah_keluar` int(8) NOT NULL,
  `id_hadiah` int(6) DEFAULT NULL,
  `id_lokasi` int(6) DEFAULT NULL,
  `jumlah` int(4) DEFAULT NULL,
  `tanggal` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_kartu_penjualan`
--

CREATE TABLE IF NOT EXISTS `zn_kartu_penjualan` (
`id_kartu_penjualan` int(11) NOT NULL,
  `kd_kartu_penjualan` varchar(50) DEFAULT NULL,
  `nama_pembeli` varchar(50) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `id_pegawai` int(6) DEFAULT NULL,
  `id_kartu` int(2) DEFAULT NULL,
  `id_cabang` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_kartu_topup`
--

CREATE TABLE IF NOT EXISTS `zn_kartu_topup` (
`id_kartu_topup` int(11) NOT NULL,
  `kd_kartu_topup` varchar(50) DEFAULT NULL,
  `id_kartu` int(2) DEFAULT NULL,
  `id_cabang` int(3) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `nominal` int(11) DEFAULT NULL,
  `id_pegawai` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_log`
--

CREATE TABLE IF NOT EXISTS `zn_log` (
  `nik` varchar(15) DEFAULT NULL,
  `username` varchar(30) DEFAULT NULL,
  `tgl_akses` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `activity` varchar(150) DEFAULT NULL,
`id` bigint(20) unsigned NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=435 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `zn_log`
--

INSERT INTO `zn_log` (`nik`, `username`, `tgl_akses`, `activity`, `id`) VALUES
('0', 'admin', '2015-04-12 13:57:09', 'Menampilkan halaman Machine Reparation', 1),
('0', 'admin', '2015-04-12 14:04:05', 'Menampilkan halaman Machine Reparation', 2),
('0', 'admin', '2015-04-12 14:09:27', 'Menampilkan halaman Machine Reparation', 3),
('0', 'admin', '2015-04-12 14:09:29', 'Menampilkan halaman Machine Reparation', 4),
('0', 'admin', '2015-04-12 14:11:54', 'Mengubah data Edit BBM', 5),
('0', 'admin', '2015-04-12 14:12:52', 'Mengubah data Edit BBM', 6),
('0', 'admin', '2015-04-12 14:13:41', 'Mengubah data Edit BBM', 7),
('0', 'admin', '2015-04-12 14:14:43', 'Menampilkan halaman Machine Reparation', 8),
('0', 'admin', '2015-04-12 14:15:59', 'Mengubah data Edit BBM', 9),
('0', 'admin', '2015-04-12 14:16:01', 'Menampilkan halaman Machine Reparation', 10),
('0', 'admin', '2015-04-12 14:17:25', 'Menampilkan halaman Machine Reparation', 11),
('0', 'admin', '2015-04-12 14:17:34', 'Mengubah data Edit BBM', 12),
('0', 'admin', '2015-04-12 14:18:00', 'Menampilkan halaman Machine Reparation', 13),
('0', 'admin', '2015-04-12 14:18:18', 'Menampilkan halaman Machine Reparation', 14),
('0', 'admin', '2015-04-12 14:18:27', 'Mengubah data Edit BBM', 15),
('0', 'admin', '2015-04-12 14:19:12', 'Mengubah data Edit BBM', 16),
('0', 'admin', '2015-04-12 14:20:34', 'Menampilkan halaman Machine Reparation', 17),
('0', 'admin', '2015-04-12 14:21:01', 'Mengubah data Edit BBM', 18),
('0', 'admin', '2015-04-12 14:21:03', 'Menampilkan halaman Machine Reparation', 19),
('0', 'admin', '2015-04-12 14:22:14', 'Menampilkan halaman Machine Reparation', 20),
('0', 'admin', '2015-04-12 14:22:23', 'Menampilkan halaman Machine Reparation', 21),
('0', 'admin', '2015-04-12 14:22:51', 'Menampilkan halaman Machine Reparation', 22),
('0', 'admin', '2015-04-12 14:23:04', 'Menampilkan halaman Machine Reparation', 23),
('0', 'admin', '2015-04-12 14:27:44', 'Menampilkan halaman Machine Reparation', 24),
('0', 'admin', '2015-04-12 14:39:11', 'Mengubah data Edit BBM', 25),
('0', 'admin', '2015-04-12 14:39:13', 'Menampilkan halaman Machine Reparation', 26),
('0', 'admin', '2015-04-12 14:49:29', 'Menampilkan halaman PR sparepart', 27),
('0', 'admin', '2015-04-12 14:59:16', 'Menampilkan halaman PR sparepart', 28),
('0', 'admin', '2015-04-12 15:04:53', 'Menampilkan halaman PR sparepart', 29),
('0', 'admin', '2015-04-12 15:06:35', 'Menampilkan halaman PR sparepart', 30),
('0', 'admin', '2015-04-12 15:07:53', 'Menampilkan halaman PR sparepart', 31),
('0', 'admin', '2015-04-12 15:08:13', 'Menampilkan halaman PR sparepart', 32),
('0', 'admin', '2015-04-12 15:13:22', 'Menampilkan halaman PR sparepart', 33),
('0', 'admin', '2015-04-12 15:56:56', 'Menampilkan halaman PR sparepart', 34),
('0', 'admin', '2015-04-12 16:02:23', 'Menampilkan halaman PR sparepart', 35),
('0', 'admin', '2015-04-12 16:30:22', 'Menampilkan halaman Machine Reparation', 36),
('0', 'admin', '2015-04-12 16:33:31', 'Menampilkan halaman Machine Reparation', 37),
('0', 'admin', '2015-04-12 17:03:40', 'Menampilkan halaman Machine Reparation', 38),
('0', 'admin', '2015-04-12 17:09:09', 'Menampilkan halaman Machine Reparation', 39),
('0', 'admin', '2015-04-12 17:13:40', 'Menampilkan halaman Machine Reparation', 40),
('0', 'admin', '2015-04-12 17:24:00', 'Menampilkan halaman Menu Aplikasi', 41),
('0', 'admin', '2015-04-12 17:24:00', 'Menampilkan halaman Master Pulau', 42),
('0', 'admin', '2015-04-12 17:24:35', 'Menampilkan halaman Menu Aplikasi', 43),
('0', 'admin', '2015-04-12 17:24:35', 'Menampilkan halaman Master Pulau', 44),
('0', 'admin', '2015-04-12 17:24:40', 'Menampilkan halaman Master Pulau', 45),
('0', 'admin', '2015-04-12 17:25:07', 'Mengubah data Manajemen Role', 46),
('0', 'admin', '2015-04-12 17:25:07', 'Menampilkan halaman Master Pulau', 47),
('0', 'admin', '2015-04-12 17:25:16', 'Menampilkan halaman Machine Maintenance', 48),
('0', 'admin', '2015-04-12 17:43:53', 'Menampilkan halaman PR sparepart', 49),
('0', 'admin', '2015-04-12 17:44:07', 'Menampilkan halaman Machine Maintenance', 50),
('0', 'admin', '2015-04-12 18:35:37', 'Menampilkan halaman Machine Master', 51),
('0', 'admin', '2015-04-12 18:36:13', 'Menampilkan halaman Store Master', 52),
('0', 'admin', '2015-04-12 18:44:21', 'Menampilkan halaman Machine Reparation', 53),
('0', 'admin', '2015-04-12 23:53:34', 'Menampilkan halaman Machine Reparation', 54),
('0', 'admin', '2015-04-12 23:54:01', 'Menampilkan halaman Machine Reparation', 55),
('0', 'admin', '2015-04-12 23:54:13', 'Menampilkan halaman Machine Reparation', 56),
('0', 'admin', '2015-04-12 23:55:45', 'Menampilkan halaman Machine Reparation', 57),
('0', 'admin', '2015-04-12 23:56:49', 'Menampilkan halaman Machine Reparation', 58),
('0', 'admin', '2015-04-13 00:13:13', 'Menampilkan halaman Machine Reparation', 59),
('0', 'admin', '2015-04-13 00:16:16', 'Menampilkan halaman Machine Reparation', 60),
('0', 'admin', '2015-04-13 00:23:27', 'Menampilkan halaman Machine Reparation', 61),
('0', 'admin', '2015-04-13 00:35:59', 'Menampilkan halaman Machine Reparation', 62),
('0', 'admin', '2015-04-13 00:36:41', 'Menampilkan halaman Machine Reparation', 63),
('0', 'admin', '2015-04-13 00:37:05', 'Menampilkan halaman Machine Reparation', 64),
('0', 'admin', '2015-04-13 00:38:02', 'Menampilkan halaman Machine Reparation', 65),
('0', 'admin', '2015-04-13 00:44:53', 'Menampilkan halaman Machine Reparation', 66),
('0', 'admin', '2015-04-13 00:47:43', 'Menampilkan halaman Machine Reparation', 67),
('0', 'admin', '2015-04-13 00:48:22', 'Menampilkan halaman Machine Reparation', 68),
('0', 'admin', '2015-04-13 00:48:22', 'Menampilkan halaman Machine Reparation', 69),
('0', 'admin', '2015-04-13 00:48:22', 'Menampilkan halaman Machine Reparation', 70),
('0', 'admin', '2015-04-13 00:49:38', 'Menampilkan halaman Machine Reparation', 71),
('0', 'admin', '2015-04-13 00:50:41', 'Menampilkan halaman Machine Reparation', 72),
('0', 'admin', '2015-04-13 00:51:34', 'Menampilkan halaman Machine Reparation', 73),
('0', 'admin', '2015-04-13 00:52:18', 'Menampilkan halaman Machine Reparation', 74),
('0', 'admin', '2015-04-13 00:52:55', 'Menampilkan halaman Machine Reparation', 75),
('0', 'admin', '2015-04-13 00:59:38', 'Menampilkan halaman Machine Reparation', 76),
('0', 'admin', '2015-04-13 00:59:59', 'Menampilkan halaman Machine Reparation', 77),
('0', 'admin', '2015-04-13 01:01:51', 'Menampilkan halaman Machine Reparation', 78),
('0', 'admin', '2015-04-13 01:03:36', 'Menampilkan halaman Machine Reparation', 79),
('0', 'admin', '2015-04-13 01:04:04', 'Menampilkan halaman Machine Reparation', 80),
('0', 'admin', '2015-04-13 01:04:28', 'Menampilkan halaman Machine Reparation', 81),
('0', 'admin', '2015-04-13 01:04:28', 'Menampilkan halaman Machine Reparation', 82),
('0', 'admin', '2015-04-13 01:04:28', 'Menampilkan halaman Machine Reparation', 83),
('0', 'admin', '2015-04-13 01:05:43', 'Menampilkan halaman Machine Reparation', 84),
('0', 'admin', '2015-04-13 01:23:50', 'Menampilkan halaman Machine Reparation', 85),
('0', 'admin', '2015-04-13 01:24:28', 'Menampilkan halaman Machine Reparation', 86),
('0', 'admin', '2015-04-13 01:26:26', 'Menampilkan halaman Machine Reparation', 87),
('0', 'admin', '2015-04-13 01:27:02', 'Menambahkan data Tambah Cabang', 88),
('0', 'admin', '2015-04-13 01:28:50', 'Menampilkan halaman Machine Reparation', 89),
('0', 'admin', '2015-04-13 01:30:15', 'Menampilkan halaman Machine Reparation', 90),
('0', 'admin', '2015-04-13 01:31:18', 'Menampilkan halaman Machine Reparation', 91),
('0', 'admin', '2015-04-13 01:33:46', 'Menampilkan halaman Machine Reparation', 92),
('0', 'admin', '2015-04-13 01:56:35', 'Menampilkan halaman Machine Reparation', 93),
('0', 'admin', '2015-04-13 01:57:36', 'Menampilkan halaman Machine Reparation', 94),
('0', 'admin', '2015-04-13 01:57:38', 'Menampilkan halaman Machine Reparation', 95),
('0', 'admin', '2015-04-13 02:02:13', 'Menampilkan halaman Machine Reparation', 96),
('0', 'admin', '2015-04-13 03:04:41', 'Menampilkan halaman Machine Maintenance', 97),
('0', 'admin', '2015-04-13 03:04:43', 'Menampilkan halaman Machine Maintenance', 98),
('0', 'admin', '2015-04-13 03:04:58', 'Menampilkan halaman Machine Reparation', 99),
('0', 'admin', '2015-04-13 03:05:01', 'Menampilkan halaman Machine Reparation', 100),
('0', 'admin', '2015-04-13 03:05:30', 'Menampilkan halaman PR sparepart', 101),
('0', 'admin', '2015-04-13 03:25:39', 'Menampilkan halaman Machine Reparation', 102),
('0', 'admin', '2015-04-13 03:37:21', 'Menampilkan halaman Machine Reparation', 103),
('0', 'admin', '2015-04-13 03:37:21', 'Menampilkan halaman Machine Reparation', 104),
('0', 'admin', '2015-04-13 03:37:58', 'Menampilkan halaman Machine Reparation', 105),
('0', 'admin', '2015-04-13 03:40:33', 'Menampilkan halaman Machine Reparation', 106),
('0', 'admin', '2015-04-13 03:40:54', 'Menampilkan halaman Machine Reparation', 107),
('0', 'admin', '2015-04-13 03:41:41', 'Menampilkan halaman Machine Reparation', 108),
('0', 'admin', '2015-04-13 03:43:50', 'Menampilkan halaman Machine Reparation', 109),
('0', 'admin', '2015-04-13 03:44:01', 'Menampilkan halaman Province Master', 110),
('0', 'admin', '2015-04-13 03:44:17', 'Menambahkan data Tambah BBM', 111),
('0', 'admin', '2015-04-13 03:44:41', 'Menampilkan halaman Province Master', 112),
('0', 'admin', '2015-04-13 03:44:48', 'Mengubah data Edit Supplier', 113),
('0', 'admin', '2015-04-13 03:45:12', 'Menampilkan halaman Province Master', 114),
('0', 'admin', '2015-04-13 03:45:30', 'Mengubah data Edit Supplier', 115),
('0', 'admin', '2015-04-13 03:46:15', 'Menampilkan halaman Province Master', 116),
('0', 'admin', '2015-04-13 03:46:21', 'Mengubah data Edit Supplier', 117),
('0', 'admin', '2015-04-13 03:47:05', 'Menampilkan halaman Province Master', 118),
('0', 'admin', '2015-04-13 03:47:10', 'Mengubah data Edit Supplier', 119),
('0', 'admin', '2015-04-13 03:48:46', 'Menampilkan halaman Province Master', 120),
('0', 'admin', '2015-04-13 03:48:51', 'Mengubah data Edit Supplier', 121),
('0', 'admin', '2015-04-13 03:49:06', 'Menampilkan halaman Machine Reparation', 122),
('0', 'admin', '2015-04-13 04:01:07', 'Menampilkan halaman Machine Maintenance', 123),
('0', 'admin', '2015-04-13 04:03:33', 'Menampilkan halaman Machine Maintenance', 124),
('0', 'admin', '2015-04-13 04:03:36', 'Menampilkan halaman Machine Reparation', 125),
('0', 'admin', '2015-04-13 04:04:59', 'Menampilkan halaman Machine Reparation', 126),
('0', 'admin', '2015-04-13 05:15:02', 'Menampilkan halaman Machine Maintenance', 127),
('0', 'admin', '2015-04-13 05:15:02', 'Menampilkan halaman Province Master', 128),
('0', 'admin', '2015-04-13 05:15:03', 'Menampilkan halaman Machine Reparation', 129),
('0', 'admin', '2015-04-13 05:32:04', 'Menampilkan halaman Machine Reparation', 130),
('0', 'admin', '2015-04-13 05:51:06', 'Menampilkan halaman Machine Reparation', 131),
('0', 'admin', '2015-04-13 05:51:23', 'Menambahkan data Tambah Cabang', 132),
('0', 'admin', '2015-04-13 06:01:34', 'Menampilkan halaman Machine Maintenance', 133),
('0', 'admin', '2015-04-13 06:01:34', 'Menampilkan halaman PR Hadiah', 134),
('0', 'admin', '2015-04-13 06:04:13', 'Menampilkan halaman Machine Reparation', 135),
('0', 'admin', '2015-04-13 06:04:42', 'Menampilkan halaman Machine Master', 136),
('0', 'admin', '2015-04-13 06:05:15', 'Menampilkan halaman Machine Type Master', 137),
('0', 'admin', '2015-04-13 10:53:35', 'Menampilkan halaman Machine Reparation', 138),
('0', 'admin', '2015-04-13 10:53:49', 'Menampilkan halaman Machine Type Master', 139),
('0', 'admin', '2015-04-13 12:13:08', 'Menampilkan halaman Machine Type Master', 140),
('0', 'admin', '2015-04-13 12:13:25', 'Menampilkan halaman Machine Master', 141),
('0', 'admin', '2015-04-13 12:14:05', 'Menambahkan data Tambah Mesin', 142),
('0', 'admin', '2015-04-13 12:14:42', 'Menambahkan data Tambah Cabang', 143),
('0', 'admin', '2015-04-13 12:14:58', 'Menampilkan halaman Machine Reparation', 144),
('0', 'admin', '2015-04-13 12:15:55', 'Menampilkan halaman Machine Reparation', 145),
('0', 'admin', '2015-04-13 12:25:48', 'Menampilkan halaman Machine Maintenance', 146),
('0', 'admin', '2015-04-13 12:30:36', 'Menampilkan halaman Machine Maintenance', 147),
('0', 'admin', '2015-04-13 12:35:48', 'Menampilkan halaman Machine Maintenance', 148),
('0', 'admin', '2015-04-13 12:37:39', 'Menampilkan halaman Machine Maintenance', 149),
('0', 'admin', '2015-04-13 12:37:48', 'Menampilkan halaman Machine Maintenance', 150),
('0', 'admin', '2015-04-13 12:38:01', 'Menampilkan halaman Machine Maintenance', 151),
('0', 'admin', '2015-04-13 12:42:02', 'Menampilkan halaman Machine Maintenance', 152),
('0', 'admin', '2015-04-13 12:42:18', 'Menampilkan halaman Machine Maintenance', 153),
('0', 'admin', '2015-04-13 12:45:35', 'Menampilkan halaman Machine Maintenance', 154),
('0', 'admin', '2015-04-13 13:26:33', 'Menampilkan halaman Machine Maintenance', 155),
('0', 'admin', '2015-04-13 13:27:51', 'Menambahkan data Tambah Cabang', 156),
('0', 'admin', '2015-04-13 13:31:07', 'Menampilkan halaman Machine Maintenance', 157),
('0', 'admin', '2015-04-13 13:38:21', 'Menampilkan halaman Machine Maintenance', 158),
('0', 'admin', '2015-04-13 13:38:41', 'Menampilkan halaman Machine Maintenance', 159),
('0', 'admin', '2015-04-13 13:45:20', 'Menampilkan halaman Machine Maintenance', 160),
('0', 'admin', '2015-04-13 14:41:25', 'Menampilkan halaman Machine Maintenance', 161),
('0', 'admin', '2015-04-13 14:42:00', 'Menampilkan halaman Machine Maintenance', 162),
('0', 'admin', '2015-04-13 14:47:47', 'Menampilkan halaman Machine Maintenance', 163),
('0', 'admin', '2015-04-13 14:50:13', 'Menampilkan halaman Machine Maintenance', 164),
('0', 'admin', '2015-04-13 15:03:54', 'Mengubah data Edit BBM', 165),
('0', 'admin', '2015-04-13 16:26:08', 'Menampilkan halaman Machine Reparation', 166),
('0', 'admin', '2015-04-14 01:40:35', 'Mengubah data Edit BBM', 167),
('0', 'admin', '2015-04-14 01:47:46', 'Menampilkan halaman Machine Reparation', 168),
('0', 'admin', '2015-04-14 02:38:51', 'Menampilkan halaman Machine Maintenance', 169),
('0', 'admin', '2015-04-14 02:38:51', 'Menampilkan halaman Machine Reparation', 170),
('0', 'admin', '2015-04-14 02:38:52', 'Menampilkan halaman Machine Maintenance', 171),
('0', 'admin', '2015-04-14 02:39:03', 'Menampilkan halaman Master Island', 172),
('0', 'admin', '2015-04-14 02:39:10', 'Menampilkan halaman Daftar User', 173),
('0', 'admin', '2015-04-14 02:39:16', 'Menampilkan halaman PO Master', 174),
('0', 'admin', '2015-04-14 02:39:34', 'Menampilkan halaman PR Hadiah', 175),
('0', 'admin', '2015-04-14 02:42:58', 'Menampilkan halaman PR Hadiah', 176),
('0', 'admin', '2015-04-14 02:43:39', 'Menampilkan halaman PR Hadiah', 177),
('0', 'admin', '2015-04-14 02:43:49', 'Menampilkan halaman PR Hadiah', 178),
('0', 'admin', '2015-04-14 02:43:57', 'Menampilkan halaman Machine Type Master', 179),
('0', 'admin', '2015-04-14 02:43:57', 'Menampilkan halaman Machine Master', 180),
('0', 'admin', '2015-04-14 02:47:40', 'Menampilkan halaman Menu Aplikasi', 181),
('0', 'admin', '2015-04-14 02:47:40', 'Menampilkan halaman Master Pulau', 182),
('0', 'admin', '2015-04-14 02:49:44', 'Menampilkan halaman Menu Aplikasi', 183),
('0', 'admin', '2015-04-14 02:49:44', 'Menampilkan halaman Master Pulau', 184),
('0', 'admin', '2015-04-14 02:49:51', 'Menampilkan halaman Master Pulau', 185),
('0', 'admin', '2015-04-14 02:50:06', 'Mengubah data Manajemen Role', 186),
('0', 'admin', '2015-04-14 02:50:07', 'Menampilkan halaman Master Pulau', 187),
('0', 'admin', '2015-04-14 02:50:28', 'Menampilkan halaman Province Master', 188),
('0', 'admin', '2015-04-14 02:50:42', 'Menambahkan data Tambah BBM', 189),
('0', 'admin', '2015-04-14 02:50:57', 'Menampilkan halaman Machine Reparation', 190),
('0', 'admin', '2015-04-14 05:52:21', 'Menampilkan halaman PO Master', 191),
('0', 'admin', '2015-04-14 05:52:35', 'Menampilkan halaman PR Hadiah', 192),
('0', 'admin', '2015-04-14 05:52:53', 'Menampilkan halaman PR Hadiah', 193),
('0', 'admin', '2015-04-14 05:59:42', 'Menampilkan halaman PR Hadiah', 194),
('0', 'admin', '2015-04-16 03:00:23', 'Menampilkan halaman PR Hadiah', 195),
('0', 'admin', '2015-04-16 03:00:35', 'Menampilkan halaman PR Hadiah', 196),
('0', 'admin', '2015-04-16 03:00:45', 'Menampilkan halaman Master Doorprize', 197),
('0', 'admin', '2015-04-16 03:33:14', 'Menampilkan halaman Master Doorprize', 198),
('0', 'admin', '2015-04-16 03:39:21', 'Menampilkan halaman Menu Aplikasi', 199),
('0', 'admin', '2015-04-16 03:39:21', 'Menampilkan halaman Master Pulau', 200),
('0', 'admin', '2015-04-16 03:39:53', 'Menampilkan halaman Menu Aplikasi', 201),
('0', 'admin', '2015-04-16 03:39:53', 'Menampilkan halaman Master Pulau', 202),
('0', 'admin', '2015-04-16 03:39:58', 'Menampilkan halaman Master Pulau', 203),
('0', 'admin', '2015-04-16 03:40:02', 'Menampilkan halaman Master Pulau', 204),
('0', 'admin', '2015-04-16 03:40:29', 'Mengubah data Manajemen Role', 205),
('0', 'admin', '2015-04-16 03:40:30', 'Menampilkan halaman Master Pulau', 206),
('0', 'admin', '2015-04-16 03:40:35', 'Menampilkan halaman Machine Transactions', 207),
('0', 'admin', '2015-04-16 03:42:36', 'Menampilkan halaman Machine Transactions', 208),
('0', 'admin', '2015-04-16 03:55:28', 'Menampilkan halaman Machine Transactions', 209),
('0', 'admin', '2015-04-16 04:09:28', 'Menampilkan halaman Master Supplier', 210),
('0', 'admin', '2015-04-16 04:20:41', 'Menampilkan halaman Machine Transactions', 211),
('0', 'admin', '2015-04-16 04:21:22', 'Menampilkan halaman Machine Transactions', 212),
('0', 'admin', '2015-04-16 04:22:28', 'Menampilkan halaman Machine Transactions', 213),
('0', 'admin', '2015-04-16 04:23:42', 'Menampilkan halaman Machine Transactions', 214),
('0', 'admin', '2015-04-16 04:24:42', 'Menampilkan halaman Master Supplier', 215),
('0', 'admin', '2015-04-16 04:24:47', 'Menampilkan halaman Machine Transactions', 216),
('0', 'admin', '2015-04-16 04:32:37', 'Menampilkan halaman Machine Transactions', 217),
('0', 'admin', '2015-04-16 04:33:53', 'Menampilkan halaman Machine Transactions', 218),
('0', 'admin', '2015-04-16 04:34:20', 'Menampilkan halaman Machine Transactions', 219),
('0', 'admin', '2015-04-16 04:40:12', 'Menampilkan halaman Machine Transactions', 220),
('0', 'admin', '2015-04-16 04:43:33', 'Menampilkan halaman Master Supplier', 221),
('0', 'admin', '2015-04-16 04:43:50', 'Menampilkan halaman Machine Transactions', 222),
('0', 'admin', '2015-04-16 04:44:08', 'Menampilkan halaman Master Supplier', 223),
('0', 'admin', '2015-04-16 04:45:03', 'Menampilkan halaman Master Supplier', 224),
('0', 'admin', '2015-04-16 04:45:08', 'Menampilkan halaman Machine Transactions', 225),
('0', 'admin', '2015-04-16 04:45:42', 'Menampilkan halaman Machine Transactions', 226),
('0', 'admin', '2015-04-16 04:46:05', 'Menampilkan halaman Master Supplier', 227),
('0', 'admin', '2015-04-16 04:47:05', 'Menampilkan halaman Master Supplier', 228),
('0', 'admin', '2015-04-16 04:47:16', 'Menampilkan halaman Machine Transactions', 229),
('0', 'admin', '2015-04-16 04:47:50', 'Menampilkan halaman Machine Transactions', 230),
('0', 'admin', '2015-04-16 04:48:22', 'Menampilkan halaman Machine Transactions', 231),
('0', 'admin', '2015-04-16 04:49:04', 'Menampilkan halaman Machine Transactions', 232),
('0', 'admin', '2015-04-16 04:51:17', 'Menampilkan halaman Machine Transactions', 233),
('0', 'admin', '2015-04-16 04:51:33', 'Menampilkan halaman Machine Transactions', 234),
('0', 'admin', '2015-04-16 04:53:26', 'Menampilkan halaman Machine Transactions', 235),
('0', 'admin', '2015-04-16 04:53:44', 'Menampilkan halaman Machine Transactions', 236),
('0', 'admin', '2015-04-16 04:54:37', 'Menampilkan halaman Machine Transactions', 237),
('0', 'admin', '2015-04-16 04:56:22', 'Menampilkan halaman Machine Transactions', 238),
('0', 'admin', '2015-04-16 05:00:20', 'Menampilkan halaman Machine Transactions', 239),
('0', 'admin', '2015-04-16 05:29:22', 'Menampilkan halaman Master Doorprize', 240),
('0', 'admin', '2015-04-16 05:32:02', 'Menampilkan halaman Master Doorprize', 241),
('0', 'admin', '2015-04-16 05:32:58', 'Menambahkan data Tambah BBM', 242),
('0', 'admin', '2015-04-16 05:33:45', 'Menampilkan halaman Store Master', 243),
('0', 'admin', '2015-04-16 05:33:52', 'Menampilkan halaman Branch Master', 244),
('0', 'admin', '2015-04-16 05:33:58', 'Menampilkan halaman Store Master', 245),
('0', 'admin', '2015-04-16 05:35:06', 'Menampilkan halaman Machine Transactions', 246),
('0', 'admin', '2015-04-16 06:57:13', 'Mengubah data Edit BBM', 247),
('0', 'admin', '2015-04-16 06:58:29', 'Mengubah data Edit BBM', 248),
('0', 'admin', '2015-04-16 07:12:00', 'Menampilkan halaman Machine Transactions', 249),
('0', 'admin', '2015-04-16 07:14:30', 'Menampilkan halaman Machine Transactions', 250),
('0', 'admin', '2015-04-16 07:17:44', 'Menampilkan halaman Master Doorprize', 251),
('0', 'admin', '2015-04-16 07:17:55', 'Mengubah data Edit BBM', 252),
('0', 'admin', '2015-04-16 07:18:08', 'Mengubah data Edit BBM', 253),
('0', 'admin', '2015-04-16 07:18:18', 'Mengubah data Edit BBM', 254),
('0', 'admin', '2015-04-16 07:18:39', 'Menampilkan halaman Machine Transactions', 255),
('0', 'admin', '2015-04-16 07:53:26', 'Menampilkan halaman Master Doorprize', 256),
('0', 'admin', '2015-04-16 07:53:40', 'Mengubah data Edit BBM', 257),
('0', 'admin', '2015-04-16 07:53:51', 'Mengubah data Edit BBM', 258),
('0', 'admin', '2015-04-16 07:54:42', 'Mengubah data Edit BBM', 259),
('0', 'admin', '2015-04-16 07:55:05', 'Mengubah data Edit BBM', 260),
('0', 'admin', '2015-04-16 07:55:09', 'Mengubah data Edit BBM', 261),
('0', 'admin', '2015-04-16 07:55:14', 'Mengubah data Edit BBM', 262),
('0', 'admin', '2015-04-16 07:55:19', 'Mengubah data Edit BBM', 263),
('0', 'admin', '2015-04-16 07:57:36', 'Menampilkan halaman Machine Maintenance', 264),
('0', 'admin', '2015-04-16 07:57:37', 'Menampilkan halaman Master owner', 265),
('0', 'admin', '2015-04-16 07:57:55', 'Menampilkan halaman Machine Maintenance', 266),
('0', 'admin', '2015-04-16 08:11:43', 'Menampilkan halaman Menu Aplikasi', 267),
('0', 'admin', '2015-04-16 08:11:43', 'Menampilkan halaman Master Pulau', 268),
('0', 'admin', '2015-04-16 08:13:11', 'Menampilkan halaman Menu Aplikasi', 269),
('0', 'admin', '2015-04-16 08:13:11', 'Menampilkan halaman Master Pulau', 270),
('0', 'admin', '2015-04-16 08:13:31', 'Menampilkan halaman Master Pulau', 271),
('0', 'admin', '2015-04-16 08:13:56', 'Mengubah data Manajemen Role', 272),
('0', 'admin', '2015-04-16 08:13:56', 'Menampilkan halaman Master Pulau', 273),
('0', 'admin', '2015-04-16 08:14:01', 'Menampilkan halaman Master Pulau', 274),
('0', 'admin', '2015-04-16 08:14:23', 'Menampilkan halaman Employee Data', 275),
('0', 'admin', '2015-04-16 08:19:26', 'Menampilkan halaman Machine Master', 276),
('0', 'admin', '2015-04-16 08:35:27', 'Menampilkan halaman Machine Master', 277),
('0', 'admin', '2015-04-16 08:36:30', 'Menampilkan halaman Machine Master', 278),
('0', 'admin', '2015-04-16 08:40:10', 'Menampilkan halaman Machine Master', 279),
('0', 'admin', '2015-04-16 08:42:06', 'Menampilkan halaman Machine Master', 280),
('0', 'admin', '2015-04-16 08:43:00', 'Menampilkan halaman Machine Master', 281),
('0', 'admin', '2015-04-16 08:44:56', 'Menampilkan halaman Machine Master', 282),
('0', 'admin', '2015-04-16 08:46:14', 'Menampilkan halaman Machine Master', 283),
('0', 'admin', '2015-04-16 08:48:07', 'Menampilkan halaman Machine Master', 284),
('0', 'admin', '2015-04-16 08:51:54', 'Menampilkan halaman Employee Data', 285),
('0', 'admin', '2015-04-16 08:52:07', 'Menampilkan halaman Employee Data', 286),
('0', 'admin', '2015-04-16 08:53:42', 'Menampilkan halaman Employee Data', 287),
('0', 'admin', '2015-04-16 08:54:05', 'Menampilkan halaman Machine Master', 288),
('0', 'admin', '2015-04-16 08:57:49', 'Menampilkan halaman Machine Master', 289),
('0', 'admin', '2015-04-16 08:58:10', 'Menampilkan halaman Machine Master', 290),
('0', 'admin', '2015-04-16 08:59:14', 'Menampilkan halaman Machine Master', 291),
('0', 'admin', '2015-04-16 08:59:25', 'Menampilkan halaman Machine Master', 292),
('0', 'admin', '2015-04-16 09:03:11', 'Menampilkan halaman Machine Master', 293),
('0', 'admin', '2015-04-16 09:07:54', 'Menampilkan halaman Machine Master', 294),
('0', 'admin', '2015-04-16 09:09:13', 'Menampilkan halaman Machine Master', 295),
('0', 'admin', '2015-04-16 09:15:29', 'Menampilkan halaman Machine Master', 296),
('0', 'admin', '2015-04-16 09:17:38', 'Menampilkan halaman Machine Master', 297),
('0', 'admin', '2015-04-16 09:17:48', 'Menambahkan data Tambah pegawai_cabang', 298),
('0', 'admin', '2015-04-16 09:18:51', 'Menampilkan halaman Machine Master', 299),
('0', 'admin', '2015-04-16 09:19:12', 'Menambahkan data Tambah pegawai_cabang', 300),
('0', 'admin', '2015-04-16 09:23:45', 'Menampilkan halaman Machine Master', 301),
('0', 'admin', '2015-04-16 09:24:09', 'Menambahkan data Tambah pegawai_cabang', 302),
('0', 'admin', '2015-04-16 09:29:29', 'Menampilkan halaman Machine Master', 303),
('0', 'admin', '2015-04-16 09:32:10', 'Mengubah data Edit pegawai_cabang', 304),
('0', 'admin', '2015-04-16 09:32:24', 'Mengubah data Edit pegawai_cabang', 305),
('0', 'admin', '2015-04-16 09:32:38', 'Mengubah data Edit pegawai_cabang', 306),
('0', 'admin', '2015-04-16 09:32:46', 'Mengubah data Edit pegawai_cabang', 307),
('0', 'admin', '2015-04-16 09:33:59', 'Menampilkan halaman Machine Master', 308),
('0', 'admin', '2015-04-16 09:34:14', 'Menghapus data Delete BBM', 309),
('0', 'admin', '2015-04-16 09:35:05', 'Menghapus data Delete BBM', 310),
('0', 'admin', '2015-04-16 09:35:13', 'Menghapus data Delete BBM', 311),
('0', 'admin', '2015-04-16 09:36:39', 'Menampilkan halaman Employees and Their Store', 312),
('0', 'admin', '2015-04-16 09:37:19', 'Menambahkan data Tambah pegawai_cabang', 313),
('0', 'admin', '2015-04-16 09:47:07', 'Menampilkan halaman Employees and Their Store', 314),
('0', 'admin', '2015-04-16 09:53:29', 'Menampilkan halaman Employees and Their Store', 315),
('0', 'admin', '2015-04-16 09:56:17', 'Menampilkan halaman Employees and Their Store', 316),
('0', 'admin', '2015-04-16 09:59:27', 'Menampilkan halaman Employees and Their Store', 317),
('0', 'admin', '2015-04-16 10:01:28', 'Menampilkan halaman Employees and Their Store', 318),
('0', 'admin', '2015-04-16 10:01:38', 'Mengubah data Edit pegawai_cabang', 319),
('0', 'admin', '2015-04-16 10:02:57', 'Mengubah data Edit BBM', 320),
('0', 'admin', '2015-04-16 10:03:00', 'Mengubah data Edit BBM', 321),
('0', 'admin', '2015-04-16 10:03:03', 'Mengubah data Edit BBM', 322),
('0', 'admin', '2015-04-16 10:07:47', 'Menampilkan halaman Employees and Their Store', 323),
('0', 'admin', '2015-04-16 10:08:07', 'Menampilkan halaman Employees and Their Store', 324),
('0', 'admin', '2015-04-16 10:10:32', 'Menampilkan halaman Employees and Their Store', 325),
('0', 'admin', '2015-04-16 10:10:43', 'Menambahkan data Tambah pegawai_cabang', 326),
('0', 'admin', '2015-04-16 10:11:02', 'Menghapus data Delete BBM', 327),
('0', 'admin', '2015-04-16 10:11:14', 'Menambahkan data Tambah pegawai_cabang', 328),
('0', 'admin', '2015-04-16 10:11:27', 'Menambahkan data Tambah pegawai_cabang', 329),
('0', 'admin', '2015-04-16 10:12:15', 'Menghapus data Delete BBM', 330),
('0', 'admin', '2015-04-16 10:12:21', 'Menghapus data Delete BBM', 331),
('0', 'admin', '2015-04-16 10:13:47', 'Menambahkan data Tambah pegawai_cabang', 332),
('0', 'admin', '2015-04-16 10:13:56', 'Menambahkan data Tambah pegawai_cabang', 333),
('0', 'admin', '2015-04-16 10:14:20', 'Mengubah data Edit pegawai_cabang', 334),
('0', 'admin', '2015-04-16 10:14:28', 'Menampilkan halaman Employees and Their Store', 335),
('0', 'admin', '2015-04-16 10:15:05', 'Mengubah data Edit pegawai_cabang', 336),
('0', 'admin', '2015-04-16 10:15:17', 'Mengubah data Edit pegawai_cabang', 337),
('0', 'admin', '2015-04-16 10:15:27', 'Mengubah data Edit pegawai_cabang', 338),
('0', 'admin', '2015-04-16 10:15:36', 'Mengubah data Edit pegawai_cabang', 339),
('0', 'admin', '2015-04-16 10:15:52', 'Menghapus data Delete BBM', 340),
('0', 'admin', '2015-04-16 10:16:02', 'Menambahkan data Tambah pegawai_cabang', 341),
('0', 'admin', '2015-04-16 10:16:09', 'Menghapus data Delete BBM', 342),
('0', 'admin', '2015-04-16 10:16:20', 'Menampilkan halaman Employees and Their Store', 343),
('0', 'admin', '2015-04-16 10:16:30', 'Menghapus data Delete BBM', 344),
('0', 'admin', '2015-04-16 10:20:10', 'Menampilkan halaman Employees and Their Store', 345),
('0', 'admin', '2015-04-16 10:20:23', 'Menambahkan data Tambah pegawai_cabang', 346),
('0', 'admin', '2015-04-16 10:20:32', 'Menambahkan data Tambah pegawai_cabang', 347),
('0', 'admin', '2015-04-16 10:20:44', 'Menghapus data Delete BBM', 348),
('0', 'admin', '2015-04-16 10:20:49', 'Menampilkan halaman Employees and Their Store', 349),
('0', 'admin', '2015-04-16 10:21:17', 'Menghapus data Delete BBM', 350),
('0', 'admin', '2015-04-16 10:21:20', 'Menampilkan halaman Employees and Their Store', 351),
('0', 'admin', '2015-04-16 10:21:30', 'Menghapus data Delete BBM', 352),
('0', 'admin', '2015-04-16 10:21:40', 'Menambahkan data Tambah pegawai_cabang', 353),
('0', 'admin', '2015-04-16 10:21:50', 'Menambahkan data Tambah pegawai_cabang', 354),
('0', 'admin', '2015-04-16 10:22:04', 'Menghapus data Delete BBM', 355),
('0', 'admin', '2015-04-16 10:22:13', 'Menghapus data Delete BBM', 356),
('0', 'admin', '2015-04-16 10:23:47', 'Menampilkan halaman Employees and Their Store', 357),
('0', 'admin', '2015-04-16 10:23:58', 'Menambahkan data Tambah pegawai_cabang', 358),
('0', 'admin', '2015-04-16 10:24:20', 'Menambahkan data Tambah pegawai_cabang', 359),
('0', 'admin', '2015-04-16 10:27:22', 'Menampilkan halaman Employees and Their Store', 360),
('0', 'admin', '2015-04-16 10:27:36', 'Menghapus data Delete BBM', 361),
('0', 'admin', '2015-04-16 10:31:14', 'Menampilkan halaman Employees and Their Store', 362),
('0', 'admin', '2015-04-16 10:32:02', 'Menampilkan halaman Employees and Their Store', 363),
('0', 'admin', '2015-04-16 10:32:10', 'Menghapus data Delete BBM', 364),
('0', 'admin', '2015-04-16 10:32:19', 'Menambahkan data Tambah pegawai_cabang', 365),
('0', 'admin', '2015-04-16 10:32:29', 'Menambahkan data Tambah pegawai_cabang', 366),
('0', 'admin', '2015-04-16 10:32:36', 'Menghapus data Delete BBM', 367),
('0', 'admin', '2015-04-16 10:32:42', 'Menghapus data Delete BBM', 368),
('0', 'admin', '2015-04-16 10:35:30', 'Menampilkan halaman Employees and Their Store', 369),
('0', 'admin', '2015-04-16 10:35:40', 'Menambahkan data Tambah pegawai_cabang', 370),
('0', 'admin', '2015-04-16 10:35:50', 'Menambahkan data Tambah pegawai_cabang', 371),
('0', 'admin', '2015-04-16 10:36:01', 'Menghapus data Delete BBM', 372),
('0', 'admin', '2015-04-16 10:36:34', 'Menampilkan halaman Employees and Their Store', 373),
('0', 'admin', '2015-04-16 10:36:45', 'Menambahkan data Tambah pegawai_cabang', 374),
('0', 'admin', '2015-04-16 10:36:50', 'Menampilkan halaman Employees and Their Store', 375),
('0', 'admin', '2015-04-16 10:37:18', 'Menampilkan halaman Employees and Their Store', 376),
('0', 'admin', '2015-04-16 10:40:44', 'Menampilkan halaman Machine Transactions', 377),
('0', 'admin', '2015-04-16 10:42:38', 'Menampilkan halaman Employee Data', 378),
('0', 'admin', '2015-04-16 10:42:38', 'Menampilkan halaman Employees and Their Store', 379),
('0', 'admin', '2015-04-16 10:55:36', 'Menampilkan halaman Employees and Their Store', 380),
('0', 'admin', '2015-04-16 10:55:47', 'Menghapus data Delete BBM', 381),
('0', 'admin', '2015-04-16 10:55:51', 'Menampilkan halaman Employees and Their Store', 382),
('0', 'admin', '2015-04-16 10:56:01', 'Menambahkan data Tambah pegawai_cabang', 383),
('0', 'admin', '2015-04-16 10:56:16', 'Menambahkan data Tambah pegawai_cabang', 384),
('0', 'admin', '2015-04-16 10:56:26', 'Menghapus data Delete BBM', 385),
('0', 'admin', '2015-04-16 10:56:42', 'Menambahkan data Tambah pegawai_cabang', 386),
('0', 'admin', '2015-04-16 10:56:54', 'Menambahkan data Tambah pegawai_cabang', 387),
('0', 'admin', '2015-04-16 10:57:05', 'Menghapus data Delete BBM', 388),
('0', 'admin', '2015-04-16 10:57:42', 'Menampilkan halaman Master Sparepart', 389),
('0', 'admin', '2015-04-16 11:04:37', 'Menampilkan halaman Master Sparepart', 390),
('0', 'admin', '2015-04-16 11:08:03', 'Menampilkan halaman Employees and Their Store', 391),
('0', 'admin', '2015-04-16 11:08:14', 'Menambahkan data Tambah pegawai_cabang', 392),
('0', 'admin', '2015-04-16 11:08:21', 'Menghapus data Delete BBM', 393),
('0', 'admin', '2015-04-16 11:08:30', 'Menambahkan data Tambah pegawai_cabang', 394),
('0', 'admin', '2015-04-16 11:08:38', 'Menambahkan data Tambah pegawai_cabang', 395),
('0', 'admin', '2015-04-16 11:08:45', 'Menghapus data Delete BBM', 396),
('0', 'admin', '2015-04-16 11:09:00', 'Menambahkan data Tambah pegawai_cabang', 397),
('0', 'admin', '2015-04-16 11:09:28', 'Menghapus data Delete BBM', 398),
('0', 'admin', '2015-04-16 11:09:40', 'Menambahkan data Tambah BBM', 399),
('0', 'admin', '2015-04-16 11:14:05', 'Menampilkan halaman Machine Transactions', 400),
('0', 'admin', '2015-04-16 11:14:50', 'Menampilkan halaman Master Sparepart', 401),
('0', 'admin', '2015-04-16 11:15:20', 'Mengubah data Edit BBM', 402),
('0', 'admin', '2015-04-16 11:15:42', 'Menampilkan halaman Master Sparepart', 403),
('0', 'admin', '2015-04-16 11:16:06', 'Menampilkan halaman Machine Master', 404),
('0', 'admin', '2015-04-16 11:17:48', 'Menampilkan halaman Employees and Their Store', 405),
('0', 'admin', '2015-04-16 11:17:59', 'Menambahkan data Tambah pegawai_cabang', 406),
('0', 'admin', '2015-04-16 11:18:08', 'Menambahkan data Tambah pegawai_cabang', 407),
('0', 'admin', '2015-04-16 11:18:30', 'Menampilkan halaman Employees and Their Store', 408),
('0', 'admin', '2015-04-16 11:19:07', 'Menampilkan halaman Employees and Their Store', 409),
('0', 'admin', '2015-04-16 11:21:26', 'Menampilkan halaman Employees and Their Store', 410),
('0', 'admin', '2015-04-16 11:28:19', 'Menampilkan halaman Employees and Their Store', 411),
('0', 'admin', '2015-04-16 11:28:39', 'Mengubah data Edit pegawai_cabang', 412),
('0', 'admin', '2015-04-16 11:29:24', 'Menghapus data Delete BBM', 413),
('0', 'admin', '2015-04-16 11:29:35', 'Menambahkan data Tambah pegawai_cabang', 414),
('0', 'admin', '2015-04-16 11:29:46', 'Menambahkan data Tambah pegawai_cabang', 415),
('0', 'admin', '2015-04-16 11:30:04', 'Menampilkan halaman Employees and Their Store', 416),
('0', 'admin', '2015-04-16 11:33:24', 'Menampilkan halaman Employees and Their Store', 417),
('0', 'admin', '2015-04-16 11:33:49', 'Menampilkan halaman Employees and Their Store', 418),
('0', 'admin', '2015-04-16 11:34:45', 'Menampilkan halaman Employees and Their Store', 419),
('0', 'admin', '2015-04-16 11:35:42', 'Menampilkan halaman Machine Maintenance', 420),
('0', 'admin', '2015-04-16 11:36:04', 'Menampilkan halaman Employees and Their Store', 421),
('0', 'admin', '2015-04-16 11:36:26', 'Menampilkan halaman Master Doorprize', 422),
('0', 'admin', '2015-04-16 11:37:08', 'Menampilkan halaman Master Doorprize', 423),
('0', 'admin', '2015-04-16 11:37:15', 'Menampilkan halaman Machine Transactions', 424),
('0', 'admin', '2015-04-16 11:37:33', 'Menampilkan halaman Employees and Their Store', 425),
('0', 'admin', '2015-04-16 11:44:42', 'Menampilkan halaman Employees and Their Store', 426),
('0', 'admin', '2015-04-16 11:44:50', 'Menghapus data Delete BBM', 427),
('0', 'admin', '2015-04-16 11:44:57', 'Menghapus data Delete BBM', 428),
('0', 'admin', '2015-04-16 11:45:08', 'Menambahkan data Tambah pegawai_cabang', 429),
('0', 'admin', '2015-04-16 11:45:23', 'Menambahkan data Tambah pegawai_cabang', 430),
('0', 'admin', '2015-04-16 11:45:31', 'Menghapus data Delete BBM', 431),
('0', 'admin', '2015-04-16 11:45:40', 'Menambahkan data Tambah pegawai_cabang', 432),
('0', 'admin', '2015-04-16 11:45:52', 'Menambahkan data Tambah pegawai_cabang', 433),
('0', 'admin', '2015-04-16 11:46:01', 'Menampilkan halaman Employees and Their Store', 434);

-- --------------------------------------------------------

--
-- Table structure for table `zn_mesin_history`
--

CREATE TABLE IF NOT EXISTS `zn_mesin_history` (
`id_mesin_history` int(11) NOT NULL,
  `kd_mesin_history` varchar(50) DEFAULT NULL,
  `tgl_pengiriman` datetime DEFAULT NULL,
  `tgl_penerimaan` datetime DEFAULT NULL,
  `id_penerima` int(3) DEFAULT NULL,
  `id_pengirim` int(3) DEFAULT NULL,
  `tipe_mutasi` enum('purchase','transfer') DEFAULT NULL COMMENT '0 : purchase\n1 : transfer',
  `id_mesin` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_mesin_kerusakan`
--

CREATE TABLE IF NOT EXISTS `zn_mesin_kerusakan` (
`id_mesin_rusak` int(11) NOT NULL,
  `kd_mesin_rusak` varchar(50) DEFAULT NULL,
  `id_mesin` int(5) DEFAULT NULL,
  `tgl_rusak` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `tgl_mulai` date DEFAULT NULL COMMENT 'tanggal mulai perbaikan',
  `tgl_target` date DEFAULT NULL,
  `tgl_selesai` datetime DEFAULT NULL,
  `request_service` int(1) DEFAULT '0' COMMENT 'apakah kerusakan ini meminta request service ke pusat\n0 : perbaikan sendiri oleh teknisi di cabang\n1 : service oleh pusat\n',
  `status_kerusakan` int(1) DEFAULT '0' COMMENT '0 : rusak\n1 : beres\n2 : inspection\n3 : waiting for sparepart\n4 : sparepart tiba',
  `problem` varchar(255) DEFAULT NULL COMMENT 'keterangan kerusakan : \nmisal layar mati, tombol tidak bisa ditekan, dsb',
  `id_cabang` int(3) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='mencatat history kerusakan sparepart pada mesin';

--
-- Dumping data for table `zn_mesin_kerusakan`
--

INSERT INTO `zn_mesin_kerusakan` (`id_mesin_rusak`, `kd_mesin_rusak`, `id_mesin`, `tgl_rusak`, `tgl_mulai`, `tgl_target`, `tgl_selesai`, `request_service`, `status_kerusakan`, `problem`, `id_cabang`) VALUES
(1, 'RS001', 1, '2015-04-11 00:53:43', '2015-04-23', '2015-05-01', '2015-05-01 00:00:00', 1, 0, 'lampu mati', 11),
(2, 'KER002', 2, '2015-04-13 12:14:42', '2015-03-29', '2015-04-30', NULL, 1, 0, 'Mengeluarkan bunyi-bunyi aneh', 11);

-- --------------------------------------------------------

--
-- Table structure for table `zn_mesin_kerusakan_history`
--

CREATE TABLE IF NOT EXISTS `zn_mesin_kerusakan_history` (
`id_rusak_history` int(11) NOT NULL,
  `kd_rusak_history` varchar(50) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL COMMENT '0 : done\n1 : rusak\n2 : inspection\n3 : waiting for sparepart',
  `id_mesin_rusak` int(11) DEFAULT NULL,
  `id_pegawai` int(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_mutasi_mesin`
--

CREATE TABLE IF NOT EXISTS `zn_mutasi_mesin` (
  `id_mutasi_mesin` int(6) DEFAULT NULL,
  `id_mesin` int(6) DEFAULT NULL,
  `id_lokasi_asal` int(6) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `id_status_mutasi_mesin` int(3) DEFAULT NULL,
  `id_lokasi_tujuan` int(6) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_pembelian_hadiah`
--

CREATE TABLE IF NOT EXISTS `zn_pembelian_hadiah` (
`id_pembelian` int(11) NOT NULL,
  `kd_pembelian` varchar(50) DEFAULT NULL,
  `id_cabang` int(3) DEFAULT NULL,
  `id_supplier` int(3) DEFAULT NULL,
  `status` int(1) DEFAULT NULL COMMENT '0 : menunggu supplier\n1 : done\n',
  `status_bayar` int(1) DEFAULT NULL COMMENT '0 : belum lunas\n1 : lunas',
  `tgl_dibuat` datetime DEFAULT NULL,
  `tgl_sampai` datetime DEFAULT NULL COMMENT 'tanggal barang sampai dikirim oleh supplier'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_pembelian_hadiah_detail`
--

CREATE TABLE IF NOT EXISTS `zn_pembelian_hadiah_detail` (
`id_pembelian_detail` int(11) NOT NULL,
  `kd_pembelian_detail` varchar(50) DEFAULT NULL,
  `id_pembelian` int(11) DEFAULT NULL,
  `id_hadiah` int(5) DEFAULT NULL,
  `jumlah` int(6) DEFAULT NULL,
  `status` int(1) DEFAULT NULL COMMENT 'status barang\n0 : rusak\n1 : ok \n2 : retur\ndsb',
  `harga` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_pembelian_kartu`
--

CREATE TABLE IF NOT EXISTS `zn_pembelian_kartu` (
`id_pembelian` int(11) NOT NULL,
  `kd_pembelian` varchar(50) DEFAULT NULL,
  `id_cabang` int(3) DEFAULT NULL,
  `id_supplier` int(3) DEFAULT NULL,
  `status` int(1) DEFAULT NULL COMMENT '0 : menunggu supplier\n1 : done\n',
  `status_bayar` int(1) DEFAULT NULL COMMENT '0 : belum lunas\n1 : lunas',
  `tgl_dibuat` datetime DEFAULT NULL,
  `tgl_sampai` datetime DEFAULT NULL COMMENT 'tanggal barang sampai dikirim oleh supplier'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_pembelian_kartu_detail`
--

CREATE TABLE IF NOT EXISTS `zn_pembelian_kartu_detail` (
`id_pembelian_detail` int(11) NOT NULL,
  `kd_pembelian_detail` varchar(50) DEFAULT NULL,
  `id_pembelian` int(11) DEFAULT NULL,
  `id_kartu` int(5) DEFAULT NULL,
  `jumlah` int(6) DEFAULT NULL,
  `status` int(1) DEFAULT NULL COMMENT 'status barang\n0 : rusak\n1 : ok \n2 : retur\ndsb',
  `harga` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_pembelian_koin`
--

CREATE TABLE IF NOT EXISTS `zn_pembelian_koin` (
`id_pembelian` int(11) NOT NULL,
  `kd_pembelian` varchar(50) DEFAULT NULL,
  `id_cabang` int(3) DEFAULT NULL,
  `id_supplier` int(3) DEFAULT NULL,
  `status` int(1) DEFAULT NULL COMMENT '0 : menunggu supplier\n1 : done\n',
  `status_bayar` int(1) DEFAULT NULL COMMENT '0 : belum lunas\n1 : lunas',
  `tgl_dibuat` datetime DEFAULT NULL,
  `tgl_sampai` datetime DEFAULT NULL COMMENT 'tanggal barang sampai dikirim oleh supplier'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_pembelian_koin_detail`
--

CREATE TABLE IF NOT EXISTS `zn_pembelian_koin_detail` (
`id_pembelian_detail` int(11) NOT NULL,
  `kd_pembelian_detail` varchar(50) DEFAULT NULL,
  `id_pembelian` int(11) DEFAULT NULL,
  `id_koin` int(5) DEFAULT NULL,
  `jumlah` int(6) DEFAULT NULL,
  `status` int(1) DEFAULT NULL COMMENT 'status barang\n0 : rusak\n1 : ok \n2 : retur\ndsb',
  `harga` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_pembelian_mesin`
--

CREATE TABLE IF NOT EXISTS `zn_pembelian_mesin` (
`id_pembelian` int(11) NOT NULL,
  `kd_pembelian` varchar(50) DEFAULT NULL,
  `id_cabang` int(3) DEFAULT NULL,
  `id_supplier` int(3) DEFAULT NULL,
  `status` int(1) DEFAULT NULL COMMENT '0 : menunggu supplier\n1 : done\n',
  `status_bayar` int(1) DEFAULT NULL COMMENT '0 : belum lunas\n1 : lunas',
  `tgl_dibuat` datetime DEFAULT NULL,
  `tgl_sampai` datetime DEFAULT NULL COMMENT 'tanggal barang sampai dikirim oleh supplier'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_pembelian_mesin_detail`
--

CREATE TABLE IF NOT EXISTS `zn_pembelian_mesin_detail` (
`id_pembelian_detail` int(11) NOT NULL,
  `kd_pembelian_detail` varchar(50) DEFAULT NULL,
  `id_pembelian` int(11) DEFAULT NULL,
  `id_mesin` int(5) DEFAULT NULL,
  `jumlah` int(3) DEFAULT NULL,
  `status` int(1) DEFAULT NULL COMMENT 'status barang\n0 : rusak\n1 : ok \n2 : retur\ndsb',
  `harga` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_pembelian_sparepart`
--

CREATE TABLE IF NOT EXISTS `zn_pembelian_sparepart` (
`id_pembelian` int(11) NOT NULL,
  `kd_pembelian` varchar(50) DEFAULT NULL,
  `id_cabang` int(3) DEFAULT NULL,
  `id_supplier` int(3) DEFAULT NULL,
  `status` int(1) DEFAULT NULL COMMENT '0 : menunggu supplier\n1 : done\n',
  `status_bayar` int(1) DEFAULT NULL COMMENT '0 : belum lunas\n1 : lunas',
  `tgl_dibuat` datetime DEFAULT NULL,
  `tgl_sampai` datetime DEFAULT NULL COMMENT 'tanggal barang sampai dikirim oleh supplier'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_pembelian_sparepart_detail`
--

CREATE TABLE IF NOT EXISTS `zn_pembelian_sparepart_detail` (
`id_pembelian_detail` int(11) NOT NULL,
  `kd_pembelian_detail` varchar(50) DEFAULT NULL,
  `id_pembelian` int(11) DEFAULT NULL,
  `id_sparepart` int(5) DEFAULT NULL,
  `jumlah` int(6) DEFAULT NULL,
  `status` int(1) DEFAULT NULL COMMENT 'status barang\n0 : rusak\n1 : ok \n2 : retur\ndsb',
  `harga` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_pembelian_tiket`
--

CREATE TABLE IF NOT EXISTS `zn_pembelian_tiket` (
`id_pembelian` int(11) NOT NULL,
  `kd_pembelian` varchar(50) DEFAULT NULL,
  `id_cabang` int(3) DEFAULT NULL,
  `id_supplier` int(3) DEFAULT NULL,
  `status` int(1) DEFAULT NULL COMMENT '0 : menunggu supplier\n1 : done\n',
  `status_bayar` int(1) DEFAULT NULL COMMENT '0 : belum lunas\n1 : lunas',
  `tgl_dibuat` datetime DEFAULT NULL,
  `tgl_sampai` datetime DEFAULT NULL COMMENT 'tanggal barang sampai dikirim oleh supplier'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_pembelian_tiket_detail`
--

CREATE TABLE IF NOT EXISTS `zn_pembelian_tiket_detail` (
`id_pembelian_detail` int(11) NOT NULL,
  `kd_pembelian_detail` varchar(50) DEFAULT NULL,
  `id_pembelian` int(11) DEFAULT NULL,
  `id_tiket` int(2) DEFAULT NULL,
  `jumlah` int(6) DEFAULT NULL,
  `status` int(1) DEFAULT NULL COMMENT 'status barang\n0 : rusak\n1 : ok \n2 : retur\ndsb',
  `harga` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_pr_hadiah`
--

CREATE TABLE IF NOT EXISTS `zn_pr_hadiah` (
`id_pr` int(11) NOT NULL,
  `kd_pr` varchar(50) DEFAULT NULL,
  `kd_po` varchar(50) DEFAULT NULL,
  `nama_pr` varchar(250) DEFAULT NULL,
  `status_pr` int(2) DEFAULT '0' COMMENT '0 : draft\n1 : dibuat / waiting aproval\n2 : revisi\n3 : ditolak\n4 : diterima',
  `id_pengirim` int(3) DEFAULT NULL COMMENT 'cabang yang meminta PR',
  `id_tujuan` int(3) DEFAULT NULL COMMENT 'id store pusat yang dituju',
  `keterangan` text,
  `tgl_dibuat` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'tanggal PR ini dibuat',
  `tgl_diajukan` datetime DEFAULT NULL,
  `tgl_disetujui` datetime DEFAULT NULL,
  `tgl_ditolak` datetime DEFAULT NULL,
  `tgl_dikirim` datetime DEFAULT NULL,
  `tgl_diterima` datetime DEFAULT NULL,
  `tgl_revisi` datetime DEFAULT NULL,
  `count_revisi` int(2) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `zn_pr_hadiah`
--

INSERT INTO `zn_pr_hadiah` (`id_pr`, `kd_pr`, `kd_po`, `nama_pr`, `status_pr`, `id_pengirim`, `id_tujuan`, `keterangan`, `tgl_dibuat`, `tgl_diajukan`, `tgl_disetujui`, `tgl_ditolak`, `tgl_dikirim`, `tgl_diterima`, `tgl_revisi`, `count_revisi`) VALUES
(1, 'PRM001', NULL, NULL, 1, 11, 10, 'please', '2015-04-10 11:02:44', '2015-04-10 18:04:52', NULL, NULL, NULL, NULL, NULL, 0),
(2, 'PRM2', NULL, NULL, 0, 11, 10, '', '2015-04-10 12:30:48', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(3, 'PRM003', NULL, NULL, 0, 11, 10, '', '2015-04-11 10:57:23', NULL, NULL, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `zn_pr_hadiah_detail`
--

CREATE TABLE IF NOT EXISTS `zn_pr_hadiah_detail` (
`id_pr_detail` int(11) NOT NULL,
  `id_pr` int(11) DEFAULT NULL,
  `id_hadiah` int(6) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `jml_diajukan` int(11) DEFAULT '0',
  `jml_disetujui` int(6) DEFAULT '0',
  `jml_dikirim` int(6) DEFAULT '0',
  `jml_diterima` int(6) DEFAULT '0',
  `keterangan` varchar(45) DEFAULT NULL,
  `jumlah` int(6) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `zn_pr_hadiah_detail`
--

INSERT INTO `zn_pr_hadiah_detail` (`id_pr_detail`, `id_pr`, `id_hadiah`, `harga`, `jml_diajukan`, `jml_disetujui`, `jml_dikirim`, `jml_diterima`, `keterangan`, `jumlah`) VALUES
(1, 1, 2, 40000, 20, 0, 0, 0, '', 20),
(3, 1, 1, 80000, 55, 0, 0, 0, '', 55),
(4, 3, 2, 40000, 15, 0, 0, 0, '', 15);

-- --------------------------------------------------------

--
-- Table structure for table `zn_pr_hadiah_history`
--

CREATE TABLE IF NOT EXISTS `zn_pr_hadiah_history` (
`id_pr_history` int(11) NOT NULL,
  `tanggal` varchar(45) DEFAULT NULL,
  `status` int(1) DEFAULT NULL COMMENT '1 : dibuat/diajukan\n2 : revisi\n3 : ditolak\n4 : diterima\n',
  `id_pegawai` int(11) DEFAULT NULL,
  `id_pr` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `zn_pr_hadiah_history`
--

INSERT INTO `zn_pr_hadiah_history` (`id_pr_history`, `tanggal`, `status`, `id_pegawai`, `id_pr`) VALUES
(1, '2015-04-10 18:04:52', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `zn_pr_hadiah_history_detail`
--

CREATE TABLE IF NOT EXISTS `zn_pr_hadiah_history_detail` (
`id_pr_history_detail` int(11) NOT NULL,
  `id_pr_history` int(11) DEFAULT NULL,
  `id_hadiah` int(6) DEFAULT NULL,
  `jumlah` int(5) DEFAULT NULL,
  `keterangan` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `zn_pr_hadiah_history_detail`
--

INSERT INTO `zn_pr_hadiah_history_detail` (`id_pr_history_detail`, `id_pr_history`, `id_hadiah`, `jumlah`, `keterangan`) VALUES
(1, 1, 2, 20, ''),
(2, 1, 1, 55, '');

-- --------------------------------------------------------

--
-- Table structure for table `zn_pr_mesin`
--

CREATE TABLE IF NOT EXISTS `zn_pr_mesin` (
`id_pr` int(11) NOT NULL,
  `kd_pr` varchar(50) DEFAULT NULL,
  `nama_pr` varchar(250) DEFAULT NULL,
  `status_pr` int(2) DEFAULT NULL COMMENT '0 : waiting aproval\n1 : disetujui\n2 : revisi\n3 : ditolak',
  `id_pengirim` int(6) DEFAULT NULL COMMENT 'cabang yang meminta PR',
  `id_tujuan` int(6) DEFAULT NULL COMMENT 'id store pusat yang dituju',
  `keterangan` text,
  `tgl_dibuat` datetime DEFAULT NULL COMMENT 'tanggal PR ini dibuat',
  `tgl_disetujui` datetime DEFAULT NULL,
  `tgl_ditolak` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_pr_sparepart`
--

CREATE TABLE IF NOT EXISTS `zn_pr_sparepart` (
`id_pr` int(11) NOT NULL,
  `kd_pr` varchar(50) DEFAULT NULL,
  `kd_po` varchar(50) DEFAULT NULL,
  `nama_pr` varchar(250) DEFAULT NULL,
  `status_pr` int(2) DEFAULT '0' COMMENT '0 : draft\n1 : dibuat / waiting aproval\n2 : revisi\n3 : ditolak\n4 : diterima',
  `id_pengirim` int(3) DEFAULT NULL COMMENT 'cabang yang meminta PR',
  `id_tujuan` int(3) DEFAULT NULL COMMENT 'id store pusat yang dituju',
  `keterangan` text,
  `tgl_dibuat` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'tanggal PR ini dibuat',
  `tgl_diajukan` datetime DEFAULT NULL,
  `tgl_disetujui` datetime DEFAULT NULL,
  `tgl_ditolak` datetime DEFAULT NULL,
  `tgl_dikirim` datetime DEFAULT NULL,
  `tgl_diterima` datetime DEFAULT NULL,
  `tgl_revisi` datetime DEFAULT NULL,
  `count_revisi` int(2) DEFAULT '0',
  `id_mesin_rusak` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_pr_sparepart_detail`
--

CREATE TABLE IF NOT EXISTS `zn_pr_sparepart_detail` (
`id_pr_detail` int(11) NOT NULL,
  `id_pr` int(11) DEFAULT NULL,
  `id_sparepart` int(6) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `jml_diajukan` int(11) DEFAULT '0',
  `jml_disetujui` int(6) DEFAULT '0',
  `jml_dikirim` int(6) DEFAULT '0',
  `jml_diterima` int(6) DEFAULT '0',
  `keterangan` varchar(45) DEFAULT NULL,
  `jumlah` int(6) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_pr_sparepart_history`
--

CREATE TABLE IF NOT EXISTS `zn_pr_sparepart_history` (
`id_pr_history` int(11) NOT NULL,
  `tanggal` varchar(45) DEFAULT NULL,
  `status` int(1) DEFAULT NULL COMMENT '0 : dibuat \n1 : diterima\n2 : revisi',
  `id_pegawai` int(11) DEFAULT NULL,
  `id_pr` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_pr_sparepart_history_detail`
--

CREATE TABLE IF NOT EXISTS `zn_pr_sparepart_history_detail` (
`id_pr_history_detail` int(11) NOT NULL,
  `id_pr_history` int(11) DEFAULT NULL,
  `id_sparepart` int(6) DEFAULT NULL,
  `jumlah` int(5) DEFAULT NULL,
  `keterangan` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_rusak_sparepart`
--

CREATE TABLE IF NOT EXISTS `zn_rusak_sparepart` (
`id_rusak_sparepart` int(11) NOT NULL,
  `kd_rusak_sparepart` varchar(50) DEFAULT NULL,
  `id_mesin_rusak` int(11) DEFAULT NULL,
  `id_sparepart` int(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_service`
--

CREATE TABLE IF NOT EXISTS `zn_service` (
`id_service` int(11) NOT NULL,
  `kd_service` varchar(50) DEFAULT NULL,
  `id_mesin_rusak` int(11) DEFAULT NULL,
  `status_request` int(1) DEFAULT '0' COMMENT '0 : waiting to confim\n1 : diterima\n2 : ditolak',
  `tgl_dibuat` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `tgl_diterima` datetime DEFAULT NULL,
  `id_pegawai` int(6) DEFAULT NULL COMMENT 'pegawai yang ditugaskan untuk melakukan service\njika sudah keluar order nya',
  `keterangan_service` varchar(45) DEFAULT NULL COMMENT 'catatan service'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `zn_service`
--

INSERT INTO `zn_service` (`id_service`, `kd_service`, `id_mesin_rusak`, `status_request`, `tgl_dibuat`, `tgl_diterima`, `id_pegawai`, `keterangan_service`) VALUES
(1, NULL, 1, 1, NULL, NULL, 1, 'tolong secepatnya diperbaiki'),
(2, NULL, 2, 2, '2015-04-13 13:27:51', NULL, NULL, 'ga bisa sory yah.. :)');

-- --------------------------------------------------------

--
-- Table structure for table `zn_sparepart_keluar`
--

CREATE TABLE IF NOT EXISTS `zn_sparepart_keluar` (
`id_sparepart_masuk` int(9) NOT NULL,
  `id_lokasi` int(3) DEFAULT NULL,
  `id_sparepart` int(3) DEFAULT NULL,
  `jumlah` int(10) DEFAULT NULL,
  `tgl` date DEFAULT NULL,
  `keterangan` text,
  `id_service` int(6) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_transaction`
--

CREATE TABLE IF NOT EXISTS `zn_transaction` (
`transaction_id` int(11) NOT NULL,
  `transactiontype_id` int(11) DEFAULT NULL,
  `rec_user` int(11) DEFAULT NULL,
  `rec_created` datetime DEFAULT NULL,
  `num_meter` int(11) DEFAULT NULL,
  `num_coin` int(11) DEFAULT NULL,
  `num_ticket` int(11) DEFAULT NULL,
  `num_start` int(11) DEFAULT NULL,
  `num_end` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `zn_transaction`
--

INSERT INTO `zn_transaction` (`transaction_id`, `transactiontype_id`, `rec_user`, `rec_created`, `num_meter`, `num_coin`, `num_ticket`, `num_start`, `num_end`) VALUES
(1, 1, 1, '0000-00-00 00:00:00', 900, 0, 0, 0, 0),
(2, 1, 1, '0000-00-00 00:00:00', 1000, 0, 0, 0, 0),
(3, 1, 1, '0000-00-00 00:00:00', 1500, 0, 0, 0, 0),
(4, 1, 1, '2015-04-15 07:17:21', 252, 0, 0, 0, 0),
(5, 1, 1, '2015-04-15 07:19:09', 253, 0, 0, 0, 0),
(6, 1, 1, '2015-04-15 07:20:51', 256, 0, 0, 0, 0),
(7, 2, 1, '2015-04-15 07:29:22', 270, 0, 0, 0, 0),
(8, 2, 1, '2015-04-15 08:07:29', 200, 0, 0, 0, 0),
(9, 1, 1, '2015-04-15 08:18:34', 257, 0, 0, 0, 0),
(10, 2, 1, '2015-04-15 08:18:53', 0, 250, 0, 0, 0),
(11, 2, 1, '2015-04-15 08:19:19', 0, 253, 0, 0, 0),
(12, 1, 1, '2015-04-15 08:22:42', 258, 0, 0, 0, 0),
(13, 2, 1, '2015-04-15 08:23:05', 0, 259, 0, 0, 0),
(14, 2, 1, '2015-04-15 08:23:33', 0, 369, 0, 0, 0),
(15, 3, 1, '2015-04-15 08:30:19', 288, 0, 0, 0, 0),
(16, 3, 1, '2015-04-15 08:32:57', 290, 0, 0, 0, 0),
(17, 3, 1, '2015-04-15 08:39:04', 305, 0, 0, 0, 0),
(18, 3, 1, '2015-04-15 08:41:32', 307, 0, 0, 0, 0),
(19, 4, 1, '2015-04-15 08:41:59', 566, 0, 0, 0, 0),
(20, 5, 1, '2015-04-15 09:43:56', 1000, 0, 0, 1001, 5000),
(21, 1, 1, '2015-04-15 09:51:39', 688, 0, 0, 0, 0),
(22, 1, 1, '2015-04-15 09:52:34', 699, 0, 0, 0, 0),
(23, 1, 1, '2015-04-15 09:57:14', 700, 0, 0, 0, 0),
(24, 2, 1, '2015-04-15 09:57:42', 0, 400, 0, 0, 0),
(25, 3, 1, '2015-04-15 09:58:15', 308, 0, 0, 0, 0),
(26, 4, 1, '2015-04-15 09:58:39', 567, 0, 0, 0, 0),
(27, 4, 1, '2015-04-15 09:59:07', 568, 0, 0, 0, 0),
(28, 1, 1, '0000-00-00 00:00:00', 1500, 0, 0, 100, 0),
(29, 1, 1, '0000-00-00 00:00:00', 1500, 0, 0, 100, 0),
(30, 1, 1, '0000-00-00 00:00:00', 1500, 0, 0, 100, 0),
(31, 1, 1, '0000-00-00 00:00:00', 1500, 0, 0, 100, 0),
(32, 1, 1, '2015-04-16 02:12:29', 1600, 0, 0, 0, 0),
(33, 1, 1, '2015-04-16 02:13:31', 1600, 0, 0, 0, 0),
(34, 1, 1, '0000-00-00 00:00:00', 1500, 0, 0, 100, 0),
(35, 1, 1, '2015-04-16 02:15:22', 1700, 0, 0, 0, 0),
(36, 1, 1, '2015-04-16 02:27:43', 1800, 0, 0, 0, 0),
(37, 2, 1, '2015-04-16 02:28:03', 0, 11632, 0, 0, 0),
(38, 3, 1, '2015-04-16 02:28:46', 1500000, 0, 0, 0, 0),
(39, 4, 1, '2015-04-16 02:29:03', 600, 0, 0, 0, 0),
(40, 5, 1, '2015-04-16 02:58:24', 5555, 0, 0, 5666, 6666),
(41, 6, 1, '0000-00-00 00:00:00', 0, 0, 0, 0, 0),
(44, 6, 1, '0000-00-00 00:00:00', 0, 0, 0, 0, 0),
(45, 6, 1, '0000-00-00 00:00:00', 0, 0, 0, 0, 0),
(47, 6, 1, '0000-00-00 00:00:00', 0, 0, 0, 0, 0),
(60, 6, 1, '0000-00-00 00:00:00', 0, 0, 0, 0, 0),
(61, 6, 1, '0000-00-00 00:00:00', 0, 0, 0, 0, 0),
(64, 6, 1, '0000-00-00 00:00:00', 0, 0, 0, 0, 0),
(65, 6, 1, '0000-00-00 00:00:00', 0, 0, 0, 0, 0),
(66, 6, 1, '0000-00-00 00:00:00', 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `zn_transaction_device`
--

CREATE TABLE IF NOT EXISTS `zn_transaction_device` (
`transactiondevice_id` int(11) NOT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `device_id` int(11) DEFAULT NULL,
  `device_transaction_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `zn_transaction_device`
--

INSERT INTO `zn_transaction_device` (`transactiondevice_id`, `transaction_id`, `device_id`, `device_transaction_id`) VALUES
(1, 1, 1, 1),
(2, 2, 1, 1),
(3, 3, 1, 1),
(4, 4, 70000, 1),
(5, 5, 70000, 1),
(6, 6, 70000, 1),
(7, 7, 70000, 1),
(8, 8, 70000, 1),
(9, 9, 70000, 1),
(10, 10, 70000, 2),
(11, 11, 70000, 3),
(12, 12, 70000, 1),
(13, 13, 70000, 2),
(14, 14, 70000, 3),
(15, 15, 70000, 4),
(16, 16, 70000, 5),
(17, 17, 70000, 9),
(18, 18, 70000, 10),
(19, 19, 70000, 11),
(20, 20, 70000, 5),
(21, 21, 70000, 1),
(22, 22, 70000, 2),
(23, 23, 70000, 1),
(24, 24, 70000, 2),
(25, 25, 70000, 3),
(26, 26, 70000, 4),
(27, 27, 70000, 5),
(28, 28, 1, 1),
(29, 29, 1, 1),
(30, 30, 1, 1),
(31, 31, 1, 1),
(32, 32, 70000, 4),
(33, 33, 70000, 5),
(34, 34, 1, 1),
(35, 35, 70000, 6),
(36, 36, 70000, 7),
(37, 37, 70000, 8),
(38, 38, 70000, 9),
(39, 39, 70000, 10),
(40, 40, 70000, 1),
(41, 41, 1, 1),
(44, 44, 1, 1),
(45, 45, 1, 1),
(47, 47, 1, 1),
(60, 60, 1, 1),
(61, 61, 1, 1),
(64, 64, 1, 1),
(65, 65, 1, 1),
(66, 66, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `zn_transaction_machine`
--

CREATE TABLE IF NOT EXISTS `zn_transaction_machine` (
`transactionmachine_id` int(11) NOT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `machine_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `zn_transaction_machine`
--

INSERT INTO `zn_transaction_machine` (`transactionmachine_id`, `transaction_id`, `machine_id`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 4, 1),
(5, 5, 1),
(6, 6, 1),
(7, 7, 1),
(8, 8, 1),
(9, 9, 1),
(10, 10, 1),
(11, 11, 1),
(12, 12, 1),
(13, 13, 1),
(14, 14, 1),
(15, 15, 1),
(16, 16, 1),
(17, 17, 1),
(18, 18, 1),
(19, 19, 1),
(20, 20, 1),
(21, 21, 1),
(22, 22, 1),
(23, 23, 1),
(24, 24, 1),
(25, 25, 1),
(26, 26, 1),
(27, 27, 1),
(28, 28, 1),
(29, 29, 1),
(30, 30, 1),
(31, 31, 1),
(32, 32, 1),
(33, 33, 1),
(34, 34, 1),
(35, 35, 1),
(36, 36, 1),
(37, 37, 1),
(38, 38, 1),
(39, 39, 1),
(40, 40, 1),
(41, 41, 1),
(44, 44, 1),
(45, 45, 1),
(47, 47, 1),
(60, 60, 1),
(61, 61, 1),
(64, 64, 1),
(65, 65, 1),
(66, 66, 1);

-- --------------------------------------------------------

--
-- Table structure for table `zn_transaction_meter`
--

CREATE TABLE IF NOT EXISTS `zn_transaction_meter` (
`transactionmeter_id` int(11) NOT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `num_initial` int(11) DEFAULT NULL,
  `num_current` int(11) DEFAULT NULL,
  `num_diff` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `zn_transaction_meter`
--

INSERT INTO `zn_transaction_meter` (`transactionmeter_id`, `transaction_id`, `num_initial`, `num_current`, `num_diff`) VALUES
(1, 1, 0, 900, 900),
(2, 2, 900, 1000, 100),
(3, 3, 1000, 1500, 500),
(4, 4, 1500, 252, 1248),
(5, 5, 252, 253, 1),
(6, 6, 253, 256, 3),
(7, 7, 256, 270, 14),
(8, 8, 270, 200, 70),
(9, 9, 256, 257, 1),
(10, 12, 257, 258, 1),
(11, 17, 0, 305, 305),
(12, 18, 305, 307, 2),
(13, 19, 0, 566, 566),
(14, 21, 258, 688, 430),
(15, 22, 688, 699, 11),
(16, 23, 699, 700, 1),
(17, 25, 307, 308, 1),
(18, 26, 566, 567, 1),
(19, 27, 567, 568, 1),
(20, 28, 700, 1500, 800),
(21, 29, 1500, 1500, 0),
(22, 30, 1500, 1500, 0),
(23, 31, 1500, 1500, 0),
(24, 32, 1500, 1600, 100),
(25, 33, 1600, 1600, 0),
(26, 34, 1600, 1500, 100),
(27, 35, 1500, 1700, 200),
(28, 36, 1700, 1800, 100),
(29, 38, 308, 1500000, 1499692),
(30, 39, 568, 600, 32),
(31, 40, 0, 5555, 5555);

-- --------------------------------------------------------

--
-- Table structure for table `zn_transaction_redemption`
--

CREATE TABLE IF NOT EXISTS `zn_transaction_redemption` (
`transactionredemption_id` int(11) NOT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `item_key` varchar(200) DEFAULT NULL,
  `item_qty` int(11) DEFAULT NULL,
  `rec_created` datetime DEFAULT NULL,
  `seq` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_transaction_refill`
--

CREATE TABLE IF NOT EXISTS `zn_transaction_refill` (
`transactionrefill_id` int(11) NOT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `item_key` varchar(200) DEFAULT NULL,
  `item_qty` int(11) DEFAULT NULL,
  `rec_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `seq` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `zn_transaction_refill`
--

INSERT INTO `zn_transaction_refill` (`transactionrefill_id`, `transaction_id`, `item_id`, `item_key`, `item_qty`, `rec_created`, `seq`) VALUES
(1, 44, 1, '1111', 5, '2015-04-16 08:23:36', 1),
(2, 44, 2, '2222', 10, '2015-04-16 08:23:36', 2),
(3, 44, 3, '3333', 4, '2015-04-16 08:23:36', 3),
(4, 44, 4, '4444', 2, '2015-04-16 08:23:36', 4),
(5, 45, 1, '1111', 5, '2015-04-16 08:25:04', 1),
(6, 45, 2, '2222', 0, '2015-04-16 08:25:04', 2),
(7, 45, 3, '3333', 4, '2015-04-16 08:25:04', 3),
(8, 45, 4, '4444', 7, '2015-04-16 08:25:04', 4),
(9, 47, 0, '1111', 5, '2015-04-16 08:26:36', 1),
(10, 47, 0, '2222', 0, '2015-04-16 08:26:36', 2),
(11, 47, 0, '3333', 4, '2015-04-16 08:26:36', 3),
(12, 47, 0, '4444', 7, '2015-04-16 08:26:36', 4);

-- --------------------------------------------------------

--
-- Table structure for table `zn_transaction_repair`
--

CREATE TABLE IF NOT EXISTS `zn_transaction_repair` (
`transactionrepair_id` int(11) NOT NULL,
  `repair_status` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_transaction_service`
--

CREATE TABLE IF NOT EXISTS `zn_transaction_service` (
`transactionservice_id` int(11) NOT NULL,
  `issue_description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_transaction_store`
--

CREATE TABLE IF NOT EXISTS `zn_transaction_store` (
`transactionstore_id` int(11) NOT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `zn_transaction_store`
--

INSERT INTO `zn_transaction_store` (`transactionstore_id`, `transaction_id`, `store_id`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 4, 1),
(5, 5, 1),
(6, 6, 1),
(7, 7, 1),
(8, 8, 1),
(9, 9, 1),
(10, 10, 1),
(11, 11, 1),
(12, 12, 1),
(13, 13, 1),
(14, 14, 1),
(15, 15, 1),
(16, 16, 1),
(17, 17, 1),
(18, 18, 1),
(19, 19, 1),
(20, 20, 1),
(21, 21, 1),
(22, 22, 1),
(23, 23, 1),
(24, 24, 1),
(25, 25, 1),
(26, 26, 1),
(27, 27, 1),
(28, 28, 1),
(29, 29, 1),
(30, 30, 1),
(31, 31, 1),
(32, 32, 1),
(33, 33, 1),
(34, 34, 1),
(35, 35, 1),
(36, 36, 1),
(37, 37, 1),
(38, 38, 1),
(39, 39, 1),
(40, 40, 1),
(41, 41, 1),
(44, 44, 1),
(45, 45, 1),
(47, 47, 1),
(60, 60, 1),
(61, 61, 1),
(64, 64, 1),
(65, 65, 1),
(66, 66, 1);

-- --------------------------------------------------------

--
-- Table structure for table `zn_transaksi_mesin`
--

CREATE TABLE IF NOT EXISTS `zn_transaksi_mesin` (
`id_transaksi_mesin` int(20) NOT NULL,
  `id_mesin` int(6) DEFAULT NULL,
  `id_lokasi` int(6) DEFAULT NULL,
  `jumlah_swap` int(4) DEFAULT NULL,
  `jumlah_tiket` int(8) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `nilai_swap` int(8) DEFAULT NULL,
  `jumlah_coin` int(8) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `m_area`
--
ALTER TABLE `m_area`
 ADD PRIMARY KEY (`id_area`), ADD KEY `fk_area_kota_idx` (`id_kota`), ADD KEY `fk_area_pegawai_idx` (`id_ka_area`);

--
-- Indexes for table `m_cabang`
--
ALTER TABLE `m_cabang`
 ADD PRIMARY KEY (`id_cabang`), ADD KEY `fk_cabang_area_idx` (`id_area`), ADD KEY `fk_cabang_owner_idx` (`id_owner`);

--
-- Indexes for table `m_cabang_setting`
--
ALTER TABLE `m_cabang_setting`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_cabang_setting_cabang_idx` (`id_cabang`);

--
-- Indexes for table `m_grup`
--
ALTER TABLE `m_grup`
 ADD PRIMARY KEY (`grup_id`), ADD UNIQUE KEY `grup_id` (`grup_id`);

--
-- Indexes for table `m_grup_menu`
--
ALTER TABLE `m_grup_menu`
 ADD PRIMARY KEY (`kd_grup_menu`), ADD UNIQUE KEY `kd_grup_menu` (`kd_grup_menu`);

--
-- Indexes for table `m_hadiah`
--
ALTER TABLE `m_hadiah`
 ADD PRIMARY KEY (`id_hadiah`), ADD KEY `item_key` (`item_key`);

--
-- Indexes for table `m_item`
--
ALTER TABLE `m_item`
 ADD PRIMARY KEY (`item_id`), ADD KEY `item_key` (`item_key`);

--
-- Indexes for table `m_item_stok`
--
ALTER TABLE `m_item_stok`
 ADD PRIMARY KEY (`itemstok_id`);

--
-- Indexes for table `m_item_type`
--
ALTER TABLE `m_item_type`
 ADD PRIMARY KEY (`itemtype_id`);

--
-- Indexes for table `m_jabatan`
--
ALTER TABLE `m_jabatan`
 ADD PRIMARY KEY (`id_jabatan`);

--
-- Indexes for table `m_jenis_mesin`
--
ALTER TABLE `m_jenis_mesin`
 ADD PRIMARY KEY (`id_jenis_mesin`), ADD KEY `fk_jenis_mesin_kelas_idx` (`id_kelas`), ADD KEY `fk_jenis_mesin_kategori_idx` (`id_kategori_mesin`);

--
-- Indexes for table `m_kartu`
--
ALTER TABLE `m_kartu`
 ADD PRIMARY KEY (`id_kartu`), ADD KEY `fk_kartu_cabang_idx` (`id_cabang`), ADD KEY `fk_kartu_kategori_katu_idx` (`id_kategori_kartu`);

--
-- Indexes for table `m_kategori_kartu`
--
ALTER TABLE `m_kategori_kartu`
 ADD PRIMARY KEY (`id_kategori_kartu`);

--
-- Indexes for table `m_kategori_mesin`
--
ALTER TABLE `m_kategori_mesin`
 ADD PRIMARY KEY (`id_kategori_mesin`);

--
-- Indexes for table `m_kelas`
--
ALTER TABLE `m_kelas`
 ADD PRIMARY KEY (`id_kelas`);

--
-- Indexes for table `m_koin`
--
ALTER TABLE `m_koin`
 ADD PRIMARY KEY (`id_koin`);

--
-- Indexes for table `m_kota`
--
ALTER TABLE `m_kota`
 ADD PRIMARY KEY (`id_kota`), ADD KEY `fk_kota_provinsi_idx` (`id_provinsi`);

--
-- Indexes for table `m_level`
--
ALTER TABLE `m_level`
 ADD PRIMARY KEY (`id_level`);

--
-- Indexes for table `m_menu`
--
ALTER TABLE `m_menu`
 ADD PRIMARY KEY (`kd_menu`), ADD KEY `fk_menu_group_menu_idx` (`kd_grup_menu`);

--
-- Indexes for table `m_mesin`
--
ALTER TABLE `m_mesin`
 ADD PRIMARY KEY (`id_mesin`), ADD KEY `fk_mesin_jenis_mesin_idx` (`id_jenis_mesin`), ADD KEY `fk_mesin_supplier_idx` (`id_supplier`), ADD KEY `fk_mesin_cabang_idx` (`id_cabang`), ADD KEY `fk_mesin_koin_idx` (`id_koin`), ADD KEY `fk_mesin_owner_idx` (`id_owner`);

--
-- Indexes for table `m_negara`
--
ALTER TABLE `m_negara`
 ADD PRIMARY KEY (`id_negara`), ADD UNIQUE KEY `nama_negara` (`nama_negara`);

--
-- Indexes for table `m_owner`
--
ALTER TABLE `m_owner`
 ADD PRIMARY KEY (`id_owner`);

--
-- Indexes for table `m_pegawai`
--
ALTER TABLE `m_pegawai`
 ADD PRIMARY KEY (`id_pegawai`), ADD KEY `fk_pegawai_user_idx` (`user_id`), ADD KEY `fk_pegawai_jabatan_idx` (`id_jabatan`);

--
-- Indexes for table `m_pegawai_cabang`
--
ALTER TABLE `m_pegawai_cabang`
 ADD PRIMARY KEY (`id_pegawai_cabang`);

--
-- Indexes for table `m_provinsi`
--
ALTER TABLE `m_provinsi`
 ADD PRIMARY KEY (`id_provinsi`), ADD KEY `fk_provinsi_pulau_idx` (`id_pulau`);

--
-- Indexes for table `m_pulau`
--
ALTER TABLE `m_pulau`
 ADD PRIMARY KEY (`id_pulau`), ADD KEY `fk_pulau_negara_idx` (`id_negara`);

--
-- Indexes for table `m_roles`
--
ALTER TABLE `m_roles`
 ADD PRIMARY KEY (`roles_id`), ADD UNIQUE KEY `roles_id` (`roles_id`), ADD KEY `fk_roles_group_idx` (`grup_id`);

--
-- Indexes for table `m_setting_hadiah`
--
ALTER TABLE `m_setting_hadiah`
 ADD PRIMARY KEY (`id_setting_hadiah`);

--
-- Indexes for table `m_sparepart`
--
ALTER TABLE `m_sparepart`
 ADD PRIMARY KEY (`id_sparepart`);

--
-- Indexes for table `m_stok_hadiah`
--
ALTER TABLE `m_stok_hadiah`
 ADD PRIMARY KEY (`id_stok_hadiah`), ADD KEY `fk_stok_hadiah_hadiah_idx` (`id_hadiah`), ADD KEY `fk_stok_hadiah_cabang_idx` (`id_cabang`);

--
-- Indexes for table `m_stok_kartu`
--
ALTER TABLE `m_stok_kartu`
 ADD PRIMARY KEY (`id_stok_hadiah`), ADD KEY `fk_stok_kartu_cabang_idx` (`id_cabang`), ADD KEY `fk_stok_kartu_kartu_idx` (`id_kartu`);

--
-- Indexes for table `m_stok_koin`
--
ALTER TABLE `m_stok_koin`
 ADD PRIMARY KEY (`id_stok_hadiah`), ADD KEY `fk_stok_hadiah_cabang_idx` (`id_cabang`), ADD KEY `fk_stok_koin_koin_idx` (`id_koin`);

--
-- Indexes for table `m_stok_sparepart`
--
ALTER TABLE `m_stok_sparepart`
 ADD PRIMARY KEY (`id_stok_sparepart`), ADD KEY `fk_stok_hadiah_cabang_idx` (`id_cabang`), ADD KEY `fk_stok_sparepart_sparepart_idx` (`id_sparepart`);

--
-- Indexes for table `m_stok_tiket`
--
ALTER TABLE `m_stok_tiket`
 ADD PRIMARY KEY (`id_stok_tiket`), ADD KEY `fk_stok_hadiah_cabang_idx` (`id_cabang`), ADD KEY `fk_stok_tiket_tiket_idx` (`id_tiket`);

--
-- Indexes for table `m_supplier`
--
ALTER TABLE `m_supplier`
 ADD PRIMARY KEY (`id_supplier`);

--
-- Indexes for table `m_tiket`
--
ALTER TABLE `m_tiket`
 ADD PRIMARY KEY (`id_tiket`);

--
-- Indexes for table `m_user`
--
ALTER TABLE `m_user`
 ADD PRIMARY KEY (`user_id`), ADD KEY `grup` (`grup_id`), ADD KEY `pegawai` (`nik`);

--
-- Indexes for table `testing`
--
ALTER TABLE `testing`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `zn_ci_sessions`
--
ALTER TABLE `zn_ci_sessions`
 ADD PRIMARY KEY (`session_id`);

--
-- Indexes for table `zn_hadiah_keluar`
--
ALTER TABLE `zn_hadiah_keluar`
 ADD PRIMARY KEY (`id_hadiah_keluar`);

--
-- Indexes for table `zn_kartu_penjualan`
--
ALTER TABLE `zn_kartu_penjualan`
 ADD PRIMARY KEY (`id_kartu_penjualan`), ADD KEY `fk_kartu_penjualan_cabang_idx` (`id_cabang`), ADD KEY `fk_kartu_penjualan_kartu_idx` (`id_kartu`), ADD KEY `fk_kartu_penjualan_pegawai_idx` (`id_pegawai`);

--
-- Indexes for table `zn_kartu_topup`
--
ALTER TABLE `zn_kartu_topup`
 ADD PRIMARY KEY (`id_kartu_topup`), ADD KEY `fk_kartu_toptup_kartu_idx` (`id_kartu`), ADD KEY `fk_kartu_topup_cabang_idx` (`id_cabang`), ADD KEY `fk_kartu_topup_pegawai_idx` (`id_pegawai`);

--
-- Indexes for table `zn_log`
--
ALTER TABLE `zn_log`
 ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `zn_mesin_history`
--
ALTER TABLE `zn_mesin_history`
 ADD PRIMARY KEY (`id_mesin_history`), ADD KEY `fk_mesin_history_cabang_pengirim_idx` (`id_pengirim`), ADD KEY `fk_mesin_history_cabang_penerima_idx` (`id_penerima`), ADD KEY `fk_mesin_history_mesin_idx` (`id_mesin`);

--
-- Indexes for table `zn_mesin_kerusakan`
--
ALTER TABLE `zn_mesin_kerusakan`
 ADD PRIMARY KEY (`id_mesin_rusak`), ADD KEY `fk_mesin_kerusakan_mesin_idx` (`id_mesin`);

--
-- Indexes for table `zn_mesin_kerusakan_history`
--
ALTER TABLE `zn_mesin_kerusakan_history`
 ADD PRIMARY KEY (`id_rusak_history`), ADD KEY `fk_mesin_kerusakan_history_mesin_kerusakan_idx` (`id_mesin_rusak`), ADD KEY `fk_mesin_kerusakan_history_pegawai_idx` (`id_pegawai`);

--
-- Indexes for table `zn_pembelian_hadiah`
--
ALTER TABLE `zn_pembelian_hadiah`
 ADD PRIMARY KEY (`id_pembelian`), ADD KEY `fk_pembelian_mesin_cabang_idx` (`id_cabang`), ADD KEY `fk_pembelian_mesin_supplier_idx` (`id_supplier`);

--
-- Indexes for table `zn_pembelian_hadiah_detail`
--
ALTER TABLE `zn_pembelian_hadiah_detail`
 ADD PRIMARY KEY (`id_pembelian_detail`), ADD KEY `fk_pembelian_mesin_detail_idx` (`id_pembelian`), ADD KEY `fk_pembelian_hadiah_detail_hadiah_idx` (`id_hadiah`);

--
-- Indexes for table `zn_pembelian_kartu`
--
ALTER TABLE `zn_pembelian_kartu`
 ADD PRIMARY KEY (`id_pembelian`), ADD KEY `fk_pembelian_mesin_cabang_idx` (`id_cabang`), ADD KEY `fk_pembelian_mesin_supplier_idx` (`id_supplier`);

--
-- Indexes for table `zn_pembelian_kartu_detail`
--
ALTER TABLE `zn_pembelian_kartu_detail`
 ADD PRIMARY KEY (`id_pembelian_detail`), ADD KEY `fk_pembelian_mesin_detail_idx` (`id_pembelian`), ADD KEY `fk_pembelian_kartu_detail_kartu_idx` (`id_kartu`);

--
-- Indexes for table `zn_pembelian_koin`
--
ALTER TABLE `zn_pembelian_koin`
 ADD PRIMARY KEY (`id_pembelian`), ADD KEY `fk_pembelian_mesin_cabang_idx` (`id_cabang`), ADD KEY `fk_pembelian_mesin_supplier_idx` (`id_supplier`);

--
-- Indexes for table `zn_pembelian_koin_detail`
--
ALTER TABLE `zn_pembelian_koin_detail`
 ADD PRIMARY KEY (`id_pembelian_detail`), ADD KEY `fk_pembelian_mesin_detail_idx` (`id_pembelian`), ADD KEY `fk_pembelian_koin_detail_koin_idx` (`id_koin`);

--
-- Indexes for table `zn_pembelian_mesin`
--
ALTER TABLE `zn_pembelian_mesin`
 ADD PRIMARY KEY (`id_pembelian`), ADD KEY `fk_pembelian_mesin_cabang_idx` (`id_cabang`), ADD KEY `fk_pembelian_mesin_supplier_idx` (`id_supplier`);

--
-- Indexes for table `zn_pembelian_mesin_detail`
--
ALTER TABLE `zn_pembelian_mesin_detail`
 ADD PRIMARY KEY (`id_pembelian_detail`), ADD KEY `fk_pembelian_mesin_detail_idx` (`id_pembelian`), ADD KEY `fk_pembelian_mesin_detail_mesin_idx` (`id_mesin`);

--
-- Indexes for table `zn_pembelian_sparepart`
--
ALTER TABLE `zn_pembelian_sparepart`
 ADD PRIMARY KEY (`id_pembelian`), ADD KEY `fk_pembelian_mesin_cabang_idx` (`id_cabang`), ADD KEY `fk_pembelian_mesin_supplier_idx` (`id_supplier`);

--
-- Indexes for table `zn_pembelian_sparepart_detail`
--
ALTER TABLE `zn_pembelian_sparepart_detail`
 ADD PRIMARY KEY (`id_pembelian_detail`), ADD KEY `fk_pembelian_mesin_detail_idx` (`id_pembelian`), ADD KEY `fk_pembelian_sparepart_detail_sparepart_idx` (`id_sparepart`);

--
-- Indexes for table `zn_pembelian_tiket`
--
ALTER TABLE `zn_pembelian_tiket`
 ADD PRIMARY KEY (`id_pembelian`), ADD KEY `fk_pembelian_mesin_cabang_idx` (`id_cabang`), ADD KEY `fk_pembelian_mesin_supplier_idx` (`id_supplier`);

--
-- Indexes for table `zn_pembelian_tiket_detail`
--
ALTER TABLE `zn_pembelian_tiket_detail`
 ADD PRIMARY KEY (`id_pembelian_detail`), ADD KEY `fk_pembelian_mesin_detail_idx` (`id_pembelian`), ADD KEY `fk_pembelian_tiket_detail_tiket_idx` (`id_tiket`);

--
-- Indexes for table `zn_pr_hadiah`
--
ALTER TABLE `zn_pr_hadiah`
 ADD PRIMARY KEY (`id_pr`), ADD KEY `fk_pr_hadiah_pengirim_idx` (`id_pengirim`), ADD KEY `fk_pr_hadiah_penerima_idx` (`id_tujuan`);

--
-- Indexes for table `zn_pr_hadiah_detail`
--
ALTER TABLE `zn_pr_hadiah_detail`
 ADD PRIMARY KEY (`id_pr_detail`), ADD KEY `fk_fr_hadiah_detail_idx` (`id_pr`), ADD KEY `fk_fr_hadiah_hadiah_idx` (`id_hadiah`);

--
-- Indexes for table `zn_pr_hadiah_history`
--
ALTER TABLE `zn_pr_hadiah_history`
 ADD PRIMARY KEY (`id_pr_history`), ADD KEY `fk_pr_hadiah_history_idx` (`id_pr`), ADD KEY `fk_pr_hadiah_history_pegawai_idx` (`id_pegawai`);

--
-- Indexes for table `zn_pr_hadiah_history_detail`
--
ALTER TABLE `zn_pr_hadiah_history_detail`
 ADD PRIMARY KEY (`id_pr_history_detail`), ADD KEY `fk_pr_hadiah_history_detail_idx` (`id_pr_history`), ADD KEY `fk_pr_hadiah_history_detail_hadiah_idx` (`id_hadiah`);

--
-- Indexes for table `zn_pr_mesin`
--
ALTER TABLE `zn_pr_mesin`
 ADD PRIMARY KEY (`id_pr`), ADD KEY `fk_pr_hadiah_pengirim_idx` (`id_pengirim`), ADD KEY `fk_pr_hadiah_penerima_idx` (`id_tujuan`);

--
-- Indexes for table `zn_pr_sparepart`
--
ALTER TABLE `zn_pr_sparepart`
 ADD PRIMARY KEY (`id_pr`), ADD KEY `fk_pr_hadiah_pengirim_idx` (`id_pengirim`), ADD KEY `fk_pr_hadiah_penerima_idx` (`id_tujuan`), ADD KEY `fk_pr_sparepart_mesin_kerusakan_idx` (`id_mesin_rusak`);

--
-- Indexes for table `zn_pr_sparepart_detail`
--
ALTER TABLE `zn_pr_sparepart_detail`
 ADD PRIMARY KEY (`id_pr_detail`), ADD KEY `fk_fr_sparepart_detail_idx` (`id_pr`);

--
-- Indexes for table `zn_pr_sparepart_history`
--
ALTER TABLE `zn_pr_sparepart_history`
 ADD PRIMARY KEY (`id_pr_history`), ADD KEY `fk_pr_hadiah_history_idx` (`id_pr`), ADD KEY `fk_pr_hadiah_history_pegawai_idx` (`id_pegawai`);

--
-- Indexes for table `zn_pr_sparepart_history_detail`
--
ALTER TABLE `zn_pr_sparepart_history_detail`
 ADD PRIMARY KEY (`id_pr_history_detail`), ADD KEY `fk_pr_sparepart_history_detail_idx` (`id_pr_history`), ADD KEY `fk_pr_sparepart_history_detail_sparepart_idx` (`id_sparepart`);

--
-- Indexes for table `zn_rusak_sparepart`
--
ALTER TABLE `zn_rusak_sparepart`
 ADD PRIMARY KEY (`id_rusak_sparepart`), ADD KEY `fk_rusak_mesin_rusak_idx` (`id_mesin_rusak`), ADD KEY `fk_rusak_sparepart_sparepart_idx` (`id_sparepart`);

--
-- Indexes for table `zn_service`
--
ALTER TABLE `zn_service`
 ADD PRIMARY KEY (`id_service`), ADD KEY `fk_service_mesin_rusak_idx` (`id_mesin_rusak`);

--
-- Indexes for table `zn_sparepart_keluar`
--
ALTER TABLE `zn_sparepart_keluar`
 ADD PRIMARY KEY (`id_sparepart_masuk`);

--
-- Indexes for table `zn_transaction`
--
ALTER TABLE `zn_transaction`
 ADD PRIMARY KEY (`transaction_id`), ADD KEY `transactiontype_id` (`transactiontype_id`), ADD KEY `rec_created` (`rec_created`), ADD KEY `rec_user` (`rec_user`);

--
-- Indexes for table `zn_transaction_device`
--
ALTER TABLE `zn_transaction_device`
 ADD PRIMARY KEY (`transactiondevice_id`);

--
-- Indexes for table `zn_transaction_machine`
--
ALTER TABLE `zn_transaction_machine`
 ADD PRIMARY KEY (`transactionmachine_id`);

--
-- Indexes for table `zn_transaction_meter`
--
ALTER TABLE `zn_transaction_meter`
 ADD PRIMARY KEY (`transactionmeter_id`);

--
-- Indexes for table `zn_transaction_redemption`
--
ALTER TABLE `zn_transaction_redemption`
 ADD PRIMARY KEY (`transactionredemption_id`);

--
-- Indexes for table `zn_transaction_refill`
--
ALTER TABLE `zn_transaction_refill`
 ADD PRIMARY KEY (`transactionrefill_id`);

--
-- Indexes for table `zn_transaction_repair`
--
ALTER TABLE `zn_transaction_repair`
 ADD PRIMARY KEY (`transactionrepair_id`);

--
-- Indexes for table `zn_transaction_service`
--
ALTER TABLE `zn_transaction_service`
 ADD PRIMARY KEY (`transactionservice_id`);

--
-- Indexes for table `zn_transaction_store`
--
ALTER TABLE `zn_transaction_store`
 ADD PRIMARY KEY (`transactionstore_id`);

--
-- Indexes for table `zn_transaksi_mesin`
--
ALTER TABLE `zn_transaksi_mesin`
 ADD PRIMARY KEY (`id_transaksi_mesin`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `m_area`
--
ALTER TABLE `m_area`
MODIFY `id_area` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `m_cabang`
--
ALTER TABLE `m_cabang`
MODIFY `id_cabang` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `m_cabang_setting`
--
ALTER TABLE `m_cabang_setting`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `m_grup`
--
ALTER TABLE `m_grup`
MODIFY `grup_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `m_hadiah`
--
ALTER TABLE `m_hadiah`
MODIFY `id_hadiah` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `m_item`
--
ALTER TABLE `m_item`
MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `m_item_stok`
--
ALTER TABLE `m_item_stok`
MODIFY `itemstok_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_item_type`
--
ALTER TABLE `m_item_type`
MODIFY `itemtype_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_jabatan`
--
ALTER TABLE `m_jabatan`
MODIFY `id_jabatan` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `m_jenis_mesin`
--
ALTER TABLE `m_jenis_mesin`
MODIFY `id_jenis_mesin` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `m_kategori_kartu`
--
ALTER TABLE `m_kategori_kartu`
MODIFY `id_kategori_kartu` int(3) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_kategori_mesin`
--
ALTER TABLE `m_kategori_mesin`
MODIFY `id_kategori_mesin` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `m_kelas`
--
ALTER TABLE `m_kelas`
MODIFY `id_kelas` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `m_koin`
--
ALTER TABLE `m_koin`
MODIFY `id_koin` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `m_kota`
--
ALTER TABLE `m_kota`
MODIFY `id_kota` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `m_level`
--
ALTER TABLE `m_level`
MODIFY `id_level` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `m_menu`
--
ALTER TABLE `m_menu`
MODIFY `kd_menu` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=74;
--
-- AUTO_INCREMENT for table `m_mesin`
--
ALTER TABLE `m_mesin`
MODIFY `id_mesin` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `m_negara`
--
ALTER TABLE `m_negara`
MODIFY `id_negara` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `m_owner`
--
ALTER TABLE `m_owner`
MODIFY `id_owner` int(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `m_pegawai`
--
ALTER TABLE `m_pegawai`
MODIFY `id_pegawai` int(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `m_pegawai_cabang`
--
ALTER TABLE `m_pegawai_cabang`
MODIFY `id_pegawai_cabang` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `m_provinsi`
--
ALTER TABLE `m_provinsi`
MODIFY `id_provinsi` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `m_pulau`
--
ALTER TABLE `m_pulau`
MODIFY `id_pulau` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `m_roles`
--
ALTER TABLE `m_roles`
MODIFY `roles_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=976;
--
-- AUTO_INCREMENT for table `m_setting_hadiah`
--
ALTER TABLE `m_setting_hadiah`
MODIFY `id_setting_hadiah` int(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_sparepart`
--
ALTER TABLE `m_sparepart`
MODIFY `id_sparepart` int(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `m_stok_hadiah`
--
ALTER TABLE `m_stok_hadiah`
MODIFY `id_stok_hadiah` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `m_stok_kartu`
--
ALTER TABLE `m_stok_kartu`
MODIFY `id_stok_hadiah` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_stok_koin`
--
ALTER TABLE `m_stok_koin`
MODIFY `id_stok_hadiah` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_stok_sparepart`
--
ALTER TABLE `m_stok_sparepart`
MODIFY `id_stok_sparepart` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_stok_tiket`
--
ALTER TABLE `m_stok_tiket`
MODIFY `id_stok_tiket` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_supplier`
--
ALTER TABLE `m_supplier`
MODIFY `id_supplier` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `m_tiket`
--
ALTER TABLE `m_tiket`
MODIFY `id_tiket` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `m_user`
--
ALTER TABLE `m_user`
MODIFY `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `testing`
--
ALTER TABLE `testing`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `zn_hadiah_keluar`
--
ALTER TABLE `zn_hadiah_keluar`
MODIFY `id_hadiah_keluar` int(8) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_kartu_penjualan`
--
ALTER TABLE `zn_kartu_penjualan`
MODIFY `id_kartu_penjualan` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_kartu_topup`
--
ALTER TABLE `zn_kartu_topup`
MODIFY `id_kartu_topup` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_log`
--
ALTER TABLE `zn_log`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=435;
--
-- AUTO_INCREMENT for table `zn_mesin_history`
--
ALTER TABLE `zn_mesin_history`
MODIFY `id_mesin_history` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_mesin_kerusakan`
--
ALTER TABLE `zn_mesin_kerusakan`
MODIFY `id_mesin_rusak` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `zn_mesin_kerusakan_history`
--
ALTER TABLE `zn_mesin_kerusakan_history`
MODIFY `id_rusak_history` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_pembelian_hadiah`
--
ALTER TABLE `zn_pembelian_hadiah`
MODIFY `id_pembelian` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_pembelian_hadiah_detail`
--
ALTER TABLE `zn_pembelian_hadiah_detail`
MODIFY `id_pembelian_detail` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_pembelian_kartu`
--
ALTER TABLE `zn_pembelian_kartu`
MODIFY `id_pembelian` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_pembelian_kartu_detail`
--
ALTER TABLE `zn_pembelian_kartu_detail`
MODIFY `id_pembelian_detail` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_pembelian_koin`
--
ALTER TABLE `zn_pembelian_koin`
MODIFY `id_pembelian` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_pembelian_koin_detail`
--
ALTER TABLE `zn_pembelian_koin_detail`
MODIFY `id_pembelian_detail` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_pembelian_mesin`
--
ALTER TABLE `zn_pembelian_mesin`
MODIFY `id_pembelian` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_pembelian_mesin_detail`
--
ALTER TABLE `zn_pembelian_mesin_detail`
MODIFY `id_pembelian_detail` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_pembelian_sparepart`
--
ALTER TABLE `zn_pembelian_sparepart`
MODIFY `id_pembelian` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_pembelian_sparepart_detail`
--
ALTER TABLE `zn_pembelian_sparepart_detail`
MODIFY `id_pembelian_detail` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_pembelian_tiket`
--
ALTER TABLE `zn_pembelian_tiket`
MODIFY `id_pembelian` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_pembelian_tiket_detail`
--
ALTER TABLE `zn_pembelian_tiket_detail`
MODIFY `id_pembelian_detail` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_pr_hadiah`
--
ALTER TABLE `zn_pr_hadiah`
MODIFY `id_pr` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `zn_pr_hadiah_detail`
--
ALTER TABLE `zn_pr_hadiah_detail`
MODIFY `id_pr_detail` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `zn_pr_hadiah_history`
--
ALTER TABLE `zn_pr_hadiah_history`
MODIFY `id_pr_history` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `zn_pr_hadiah_history_detail`
--
ALTER TABLE `zn_pr_hadiah_history_detail`
MODIFY `id_pr_history_detail` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `zn_pr_mesin`
--
ALTER TABLE `zn_pr_mesin`
MODIFY `id_pr` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_pr_sparepart`
--
ALTER TABLE `zn_pr_sparepart`
MODIFY `id_pr` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_pr_sparepart_detail`
--
ALTER TABLE `zn_pr_sparepart_detail`
MODIFY `id_pr_detail` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_pr_sparepart_history`
--
ALTER TABLE `zn_pr_sparepart_history`
MODIFY `id_pr_history` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_pr_sparepart_history_detail`
--
ALTER TABLE `zn_pr_sparepart_history_detail`
MODIFY `id_pr_history_detail` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_rusak_sparepart`
--
ALTER TABLE `zn_rusak_sparepart`
MODIFY `id_rusak_sparepart` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_service`
--
ALTER TABLE `zn_service`
MODIFY `id_service` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `zn_sparepart_keluar`
--
ALTER TABLE `zn_sparepart_keluar`
MODIFY `id_sparepart_masuk` int(9) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_transaction`
--
ALTER TABLE `zn_transaction`
MODIFY `transaction_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=75;
--
-- AUTO_INCREMENT for table `zn_transaction_device`
--
ALTER TABLE `zn_transaction_device`
MODIFY `transactiondevice_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=75;
--
-- AUTO_INCREMENT for table `zn_transaction_machine`
--
ALTER TABLE `zn_transaction_machine`
MODIFY `transactionmachine_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=75;
--
-- AUTO_INCREMENT for table `zn_transaction_meter`
--
ALTER TABLE `zn_transaction_meter`
MODIFY `transactionmeter_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `zn_transaction_redemption`
--
ALTER TABLE `zn_transaction_redemption`
MODIFY `transactionredemption_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_transaction_refill`
--
ALTER TABLE `zn_transaction_refill`
MODIFY `transactionrefill_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `zn_transaction_repair`
--
ALTER TABLE `zn_transaction_repair`
MODIFY `transactionrepair_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_transaction_service`
--
ALTER TABLE `zn_transaction_service`
MODIFY `transactionservice_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_transaction_store`
--
ALTER TABLE `zn_transaction_store`
MODIFY `transactionstore_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=75;
--
-- AUTO_INCREMENT for table `zn_transaksi_mesin`
--
ALTER TABLE `zn_transaksi_mesin`
MODIFY `id_transaksi_mesin` int(20) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `m_area`
--
ALTER TABLE `m_area`
ADD CONSTRAINT `zone2000_fk_area_kota` FOREIGN KEY (`id_kota`) REFERENCES `m_kota` (`id_kota`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_area_pegawai` FOREIGN KEY (`id_ka_area`) REFERENCES `m_pegawai` (`id_pegawai`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `m_cabang`
--
ALTER TABLE `m_cabang`
ADD CONSTRAINT `zone2000_fk_cabang_area` FOREIGN KEY (`id_area`) REFERENCES `m_area` (`id_area`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_cabang_owner` FOREIGN KEY (`id_owner`) REFERENCES `m_owner` (`id_owner`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `m_cabang_setting`
--
ALTER TABLE `m_cabang_setting`
ADD CONSTRAINT `zone2000_fk_cabang_setting_cabang` FOREIGN KEY (`id_cabang`) REFERENCES `m_cabang` (`id_cabang`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `m_jenis_mesin`
--
ALTER TABLE `m_jenis_mesin`
ADD CONSTRAINT `zone2000_fk_jenis_mesin_kategori` FOREIGN KEY (`id_kategori_mesin`) REFERENCES `m_kategori_mesin` (`id_kategori_mesin`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_jenis_mesin_kelas` FOREIGN KEY (`id_kelas`) REFERENCES `m_kelas` (`id_kelas`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `m_kartu`
--
ALTER TABLE `m_kartu`
ADD CONSTRAINT `zone2000_fk_kartu_cabang` FOREIGN KEY (`id_cabang`) REFERENCES `m_cabang` (`id_cabang`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_kartu_kategori_katu` FOREIGN KEY (`id_kategori_kartu`) REFERENCES `m_kategori_kartu` (`id_kategori_kartu`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `m_kota`
--
ALTER TABLE `m_kota`
ADD CONSTRAINT `zone2000_fk_kota_provinsi` FOREIGN KEY (`id_provinsi`) REFERENCES `m_provinsi` (`id_provinsi`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `m_menu`
--
ALTER TABLE `m_menu`
ADD CONSTRAINT `zone2000_fk_menu_group_menu` FOREIGN KEY (`kd_grup_menu`) REFERENCES `m_grup_menu` (`kd_grup_menu`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `m_mesin`
--
ALTER TABLE `m_mesin`
ADD CONSTRAINT `zone2000_fk_mesin_cabang` FOREIGN KEY (`id_cabang`) REFERENCES `m_cabang` (`id_cabang`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_mesin_jenis_mesin` FOREIGN KEY (`id_jenis_mesin`) REFERENCES `m_jenis_mesin` (`id_jenis_mesin`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_mesin_koin` FOREIGN KEY (`id_koin`) REFERENCES `m_koin` (`id_koin`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_mesin_owner` FOREIGN KEY (`id_owner`) REFERENCES `m_owner` (`id_owner`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_mesin_supplier` FOREIGN KEY (`id_supplier`) REFERENCES `m_supplier` (`id_supplier`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `m_pegawai`
--
ALTER TABLE `m_pegawai`
ADD CONSTRAINT `zone2000_fk_pegawai_jabatan` FOREIGN KEY (`id_jabatan`) REFERENCES `m_jabatan` (`id_jabatan`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_pegawai_user` FOREIGN KEY (`user_id`) REFERENCES `m_user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `m_provinsi`
--
ALTER TABLE `m_provinsi`
ADD CONSTRAINT `zone2000_fk_provinsi_pulau` FOREIGN KEY (`id_pulau`) REFERENCES `m_pulau` (`id_pulau`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `m_pulau`
--
ALTER TABLE `m_pulau`
ADD CONSTRAINT `zone2000_fk_pulau_negara` FOREIGN KEY (`id_negara`) REFERENCES `m_negara` (`id_negara`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `m_roles`
--
ALTER TABLE `m_roles`
ADD CONSTRAINT `zone2000_fk_roles_group` FOREIGN KEY (`grup_id`) REFERENCES `m_grup` (`grup_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `m_stok_hadiah`
--
ALTER TABLE `m_stok_hadiah`
ADD CONSTRAINT `zone2000_fk_stok_hadiah_cabang` FOREIGN KEY (`id_cabang`) REFERENCES `m_cabang` (`id_cabang`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_stok_hadiah_hadiah` FOREIGN KEY (`id_hadiah`) REFERENCES `m_hadiah` (`id_hadiah`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `m_stok_kartu`
--
ALTER TABLE `m_stok_kartu`
ADD CONSTRAINT `zone2000_fk_stok_kartu_cabang` FOREIGN KEY (`id_cabang`) REFERENCES `m_cabang` (`id_cabang`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_stok_kartu_kartu` FOREIGN KEY (`id_kartu`) REFERENCES `m_kartu` (`id_kartu`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `m_stok_koin`
--
ALTER TABLE `m_stok_koin`
ADD CONSTRAINT `zone2000_fk_stok_koin_cabang` FOREIGN KEY (`id_cabang`) REFERENCES `m_cabang` (`id_cabang`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_stok_koin_koin` FOREIGN KEY (`id_koin`) REFERENCES `m_koin` (`id_koin`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `m_stok_sparepart`
--
ALTER TABLE `m_stok_sparepart`
ADD CONSTRAINT `zone2000_fk_stok_sparepart_cabang` FOREIGN KEY (`id_cabang`) REFERENCES `m_cabang` (`id_cabang`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `m_stok_tiket`
--
ALTER TABLE `m_stok_tiket`
ADD CONSTRAINT `zone2000_fk_stok_tiket_cabang` FOREIGN KEY (`id_cabang`) REFERENCES `m_cabang` (`id_cabang`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_stok_tiket_tiket` FOREIGN KEY (`id_tiket`) REFERENCES `m_tiket` (`id_tiket`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `m_user`
--
ALTER TABLE `m_user`
ADD CONSTRAINT `zone2000_fk_user_group` FOREIGN KEY (`grup_id`) REFERENCES `m_grup` (`grup_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zn_kartu_penjualan`
--
ALTER TABLE `zn_kartu_penjualan`
ADD CONSTRAINT `zone2000_fk_kartu_penjualan_cabang` FOREIGN KEY (`id_cabang`) REFERENCES `m_cabang` (`id_cabang`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_kartu_penjualan_kartu` FOREIGN KEY (`id_kartu`) REFERENCES `m_kartu` (`id_kartu`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_kartu_penjualan_pegawai` FOREIGN KEY (`id_pegawai`) REFERENCES `m_pegawai` (`id_pegawai`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zn_kartu_topup`
--
ALTER TABLE `zn_kartu_topup`
ADD CONSTRAINT `zone2000_fk_kartu_toptup_kartu` FOREIGN KEY (`id_kartu`) REFERENCES `m_kartu` (`id_kartu`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_kartu_topup_cabang` FOREIGN KEY (`id_cabang`) REFERENCES `m_cabang` (`id_cabang`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_kartu_topup_pegawai` FOREIGN KEY (`id_pegawai`) REFERENCES `m_pegawai` (`id_pegawai`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zn_mesin_history`
--
ALTER TABLE `zn_mesin_history`
ADD CONSTRAINT `zone2000_fk_mesin_history_cabang_penerima` FOREIGN KEY (`id_penerima`) REFERENCES `m_cabang` (`id_cabang`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_mesin_history_cabang_pengirim` FOREIGN KEY (`id_pengirim`) REFERENCES `m_cabang` (`id_cabang`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_mesin_history_mesin` FOREIGN KEY (`id_mesin`) REFERENCES `m_mesin` (`id_mesin`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zn_mesin_kerusakan_history`
--
ALTER TABLE `zn_mesin_kerusakan_history`
ADD CONSTRAINT `zone2000_fk_mesin_kerusakan_history_mesin_kerusakan` FOREIGN KEY (`id_mesin_rusak`) REFERENCES `zn_mesin_kerusakan` (`id_mesin_rusak`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_mesin_kerusakan_history_pegawai` FOREIGN KEY (`id_pegawai`) REFERENCES `m_pegawai` (`id_pegawai`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zn_pembelian_hadiah`
--
ALTER TABLE `zn_pembelian_hadiah`
ADD CONSTRAINT `zone2000_fk_pembelian_hadiah_cabang` FOREIGN KEY (`id_cabang`) REFERENCES `m_cabang` (`id_cabang`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_pembelian_hadiah_supplier` FOREIGN KEY (`id_supplier`) REFERENCES `m_supplier` (`id_supplier`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zn_pembelian_hadiah_detail`
--
ALTER TABLE `zn_pembelian_hadiah_detail`
ADD CONSTRAINT `zone2000_fk_pembelian_hadiah_detail` FOREIGN KEY (`id_pembelian`) REFERENCES `zn_pembelian_hadiah` (`id_pembelian`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_pembelian_hadiah_detail_hadiah` FOREIGN KEY (`id_hadiah`) REFERENCES `m_hadiah` (`id_hadiah`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zn_pembelian_kartu`
--
ALTER TABLE `zn_pembelian_kartu`
ADD CONSTRAINT `zone2000_fk_pembelian_kartu_cabang` FOREIGN KEY (`id_cabang`) REFERENCES `m_cabang` (`id_cabang`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_pembelian_kartu_supplier` FOREIGN KEY (`id_supplier`) REFERENCES `m_supplier` (`id_supplier`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zn_pembelian_kartu_detail`
--
ALTER TABLE `zn_pembelian_kartu_detail`
ADD CONSTRAINT `zone2000_fk_pembelian_kartu_detail` FOREIGN KEY (`id_pembelian`) REFERENCES `zn_pembelian_kartu` (`id_pembelian`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_pembelian_kartu_detail_kartu` FOREIGN KEY (`id_kartu`) REFERENCES `m_kartu` (`id_kartu`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zn_pembelian_koin`
--
ALTER TABLE `zn_pembelian_koin`
ADD CONSTRAINT `zone2000_fk_pembelian_koin_cabang` FOREIGN KEY (`id_cabang`) REFERENCES `m_cabang` (`id_cabang`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_pembelian_koin_supplier` FOREIGN KEY (`id_supplier`) REFERENCES `m_supplier` (`id_supplier`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zn_pembelian_koin_detail`
--
ALTER TABLE `zn_pembelian_koin_detail`
ADD CONSTRAINT `zone2000_fk_pembelian_koin_detail` FOREIGN KEY (`id_pembelian`) REFERENCES `zn_pembelian_koin` (`id_pembelian`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_pembelian_koin_detail_koin` FOREIGN KEY (`id_koin`) REFERENCES `m_koin` (`id_koin`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zn_pembelian_mesin`
--
ALTER TABLE `zn_pembelian_mesin`
ADD CONSTRAINT `zone2000_fk_pembelian_mesin_cabang` FOREIGN KEY (`id_cabang`) REFERENCES `m_cabang` (`id_cabang`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_pembelian_mesin_supplier` FOREIGN KEY (`id_supplier`) REFERENCES `m_supplier` (`id_supplier`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zn_pembelian_mesin_detail`
--
ALTER TABLE `zn_pembelian_mesin_detail`
ADD CONSTRAINT `zone2000_fk_pembelian_mesin_detail` FOREIGN KEY (`id_pembelian`) REFERENCES `zn_pembelian_mesin` (`id_pembelian`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_pembelian_mesin_detail_mesin` FOREIGN KEY (`id_mesin`) REFERENCES `m_mesin` (`id_mesin`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zn_pembelian_sparepart`
--
ALTER TABLE `zn_pembelian_sparepart`
ADD CONSTRAINT `zone2000_fk_pembelian_sparepart_cabang` FOREIGN KEY (`id_cabang`) REFERENCES `m_cabang` (`id_cabang`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_pembelian_sparepart_supplier` FOREIGN KEY (`id_supplier`) REFERENCES `m_supplier` (`id_supplier`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zn_pembelian_sparepart_detail`
--
ALTER TABLE `zn_pembelian_sparepart_detail`
ADD CONSTRAINT `zone2000_fk_pembelian_sparepart_detail` FOREIGN KEY (`id_pembelian`) REFERENCES `zn_pembelian_sparepart` (`id_pembelian`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zn_pembelian_tiket`
--
ALTER TABLE `zn_pembelian_tiket`
ADD CONSTRAINT `zone2000_fk_pembelian_tiket_cabang` FOREIGN KEY (`id_cabang`) REFERENCES `m_cabang` (`id_cabang`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_pembelian_tiket_supplier` FOREIGN KEY (`id_supplier`) REFERENCES `m_supplier` (`id_supplier`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zn_pembelian_tiket_detail`
--
ALTER TABLE `zn_pembelian_tiket_detail`
ADD CONSTRAINT `zone2000_fk_pembelian_tiket_detail` FOREIGN KEY (`id_pembelian`) REFERENCES `zn_pembelian_tiket` (`id_pembelian`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_pembelian_tiket_detail_tiket` FOREIGN KEY (`id_tiket`) REFERENCES `m_tiket` (`id_tiket`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zn_pr_hadiah`
--
ALTER TABLE `zn_pr_hadiah`
ADD CONSTRAINT `zone2000_fk_pr_hadiah_pengirim` FOREIGN KEY (`id_pengirim`) REFERENCES `m_cabang` (`id_cabang`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_pr_hadiah_tujuan` FOREIGN KEY (`id_tujuan`) REFERENCES `m_cabang` (`id_cabang`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zn_pr_hadiah_detail`
--
ALTER TABLE `zn_pr_hadiah_detail`
ADD CONSTRAINT `zone2000_fk_fr_hadiah_detail` FOREIGN KEY (`id_pr`) REFERENCES `zn_pr_hadiah` (`id_pr`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_fr_hadiah_hadiah` FOREIGN KEY (`id_hadiah`) REFERENCES `m_hadiah` (`id_hadiah`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zn_pr_hadiah_history`
--
ALTER TABLE `zn_pr_hadiah_history`
ADD CONSTRAINT `zone2000_fk_pr_hadiah_history` FOREIGN KEY (`id_pr`) REFERENCES `zn_pr_hadiah` (`id_pr`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_pr_hadiah_history_pegawai` FOREIGN KEY (`id_pegawai`) REFERENCES `m_pegawai` (`id_pegawai`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zn_pr_hadiah_history_detail`
--
ALTER TABLE `zn_pr_hadiah_history_detail`
ADD CONSTRAINT `zone2000_fk_pr_hadiah_history_detail` FOREIGN KEY (`id_pr_history`) REFERENCES `zn_pr_hadiah_history` (`id_pr_history`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_pr_hadiah_history_detail_hadiah` FOREIGN KEY (`id_hadiah`) REFERENCES `m_hadiah` (`id_hadiah`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zn_pr_mesin`
--
ALTER TABLE `zn_pr_mesin`
ADD CONSTRAINT `zone2000_fk_pr_mesin_pengirim` FOREIGN KEY (`id_pengirim`) REFERENCES `m_cabang` (`id_cabang`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_pr_mesin_tujuan` FOREIGN KEY (`id_tujuan`) REFERENCES `m_cabang` (`id_cabang`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zn_pr_sparepart`
--
ALTER TABLE `zn_pr_sparepart`
ADD CONSTRAINT `zone2000_fk_pr_sparepart_mesin_kerusakan` FOREIGN KEY (`id_mesin_rusak`) REFERENCES `zn_mesin_kerusakan` (`id_mesin_rusak`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_pr_sparepart_pengirim` FOREIGN KEY (`id_pengirim`) REFERENCES `m_cabang` (`id_cabang`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_pr_sparepart_tujuan` FOREIGN KEY (`id_tujuan`) REFERENCES `m_cabang` (`id_cabang`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zn_pr_sparepart_detail`
--
ALTER TABLE `zn_pr_sparepart_detail`
ADD CONSTRAINT `zone2000_fk_fr_sparepart_detail` FOREIGN KEY (`id_pr`) REFERENCES `zn_pr_sparepart` (`id_pr`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zn_pr_sparepart_history`
--
ALTER TABLE `zn_pr_sparepart_history`
ADD CONSTRAINT `zone2000_fk_pr_sparepart_history` FOREIGN KEY (`id_pr`) REFERENCES `zn_pr_sparepart` (`id_pr`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_pr_sparepart_history_pegawai` FOREIGN KEY (`id_pegawai`) REFERENCES `m_pegawai` (`id_pegawai`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zn_pr_sparepart_history_detail`
--
ALTER TABLE `zn_pr_sparepart_history_detail`
ADD CONSTRAINT `zone2000_fk_pr_sparepart_history_detail` FOREIGN KEY (`id_pr_history`) REFERENCES `zn_pr_sparepart_history` (`id_pr_history`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zn_rusak_sparepart`
--
ALTER TABLE `zn_rusak_sparepart`
ADD CONSTRAINT `zone2000_fk_rusak_sparepart_mesin_rusak` FOREIGN KEY (`id_mesin_rusak`) REFERENCES `zn_mesin_kerusakan` (`id_mesin_rusak`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zn_service`
--
ALTER TABLE `zn_service`
ADD CONSTRAINT `zone2000_fk_service_mesin_rusak` FOREIGN KEY (`id_mesin_rusak`) REFERENCES `zn_mesin_kerusakan` (`id_mesin_rusak`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

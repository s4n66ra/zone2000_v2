ALTER TABLE `zone2000`.`m_mesin` 
CHANGE COLUMN `status` `status` INT(1) NULL COMMENT 'status\n0 : fine\n1 : rusak tapi masih bisa jalan\n2 : rusak dan tidak jalan/nyala' ;

ALTER TABLE `zone2000`.`zn_mesin_kerusakan` 
CHANGE COLUMN `status_perbaikan` `status_service` INT(1) NULL DEFAULT '0' COMMENT '0 : perbaikan sendiri oleh teknisi di cabang\n1 : service oleh pusat\n' ;

ALTER TABLE `zone2000`.`zn_service` 
ADD COLUMN `kd_service` VARCHAR(50) NULL AFTER `id_service`;

ALTER TABLE `zone2000`.`zn_service` 
CHANGE COLUMN `status` `status` INT(1) NULL DEFAULT 0 COMMENT '0 : waiting to confim\n1 : diterima\n2 : ditolak' ,
CHANGE COLUMN `tgl_dibuat` `tgl_dibuat` TIMESTAMP NULL DEFAULT NULL ,
ADD COLUMN `note_service` VARCHAR(45) NULL COMMENT 'catatan service' AFTER `id_pegawai`;

ALTER TABLE `zone2000`.`zn_service` 
CHANGE COLUMN `note_service` `keterangan_service` VARCHAR(45) NULL DEFAULT NULL COMMENT 'catatan service' ;

ALTER TABLE `zone2000`.`zn_mesin_kerusakan` 
CHANGE COLUMN `tgl_mulai` `tgl_mulai` DATE NULL DEFAULT NULL COMMENT 'tanggal mulai perbaikan' ,
CHANGE COLUMN `tgl_target` `tgl_target` DATE NULL DEFAULT NULL ;

ALTER TABLE `zone2000`.`zn_mesin_kerusakan` 
CHANGE COLUMN `status` `status_kerusakan` INT(1) NULL DEFAULT '0' COMMENT '0 : rusak\n1 : beres\n2 : inspection\n3 : waiting for sparepart\n4 : sparepart tiba' ;

ALTER TABLE `zone2000`.`m_mesin` 
DROP FOREIGN KEY `zone2000_fk_mesin_jenis_mesin`;

DROP TABLE IF EXISTS `zone2000`.`zn_po_mesin_detail`;
DROP TABLE IF EXISTS `zone2000`.`zn_po_mesin`;

DROP TABLE IF EXISTS `zone2000`.`zn_pr_mesin_history_detail`;
DROP TABLE IF EXISTS `zone2000`.`zn_pr_mesin_history`;
DROP TABLE IF EXISTS `zone2000`.`zn_pr_mesin_detail`;
DROP TABLE IF EXISTS `zone2000`.`zn_po_mesin`;

TRUNCATE `zone2000`.`m_jenis_mesin`;

ALTER TABLE `zone2000`.`m_jenis_mesin` 
CHANGE COLUMN `id_jenis_mesin` `id_jenis_mesin` INT(4) NOT NULL AUTO_INCREMENT ;


INSERT INTO `zone2000`.`m_jenis_mesin` (`id_jenis_mesin`, `kd_jenis_mesin`, `nama_jenis_mesin`, `deskripsi`, `jenis_koin`, `id_kategori_mesin`, `id_kelas`) VALUES ('1', '001', 'SEGA RALLY', 'game balapan', '1', '4', '2');
INSERT INTO `zone2000`.`m_jenis_mesin` (`id_jenis_mesin`, `kd_jenis_mesin`, `nama_jenis_mesin`, `deskripsi`, `jenis_koin`, `id_kategori_mesin`, `id_kelas`) VALUES ('2', '002', 'Cruizer Z 2034', 'keren', '1', '4', '2');
INSERT INTO `zone2000`.`m_jenis_mesin` (`id_jenis_mesin`, `kd_jenis_mesin`, `nama_jenis_mesin`, `jenis_koin`, `id_kategori_mesin`, `id_kelas`) VALUES ('0', '000', 'SEGA RALLY OLD', '1', '4', '2');

UPDATE `zone2000`.`m_mesin` SET `id_jenis_mesin`=NULL WHERE `id_mesin`='1';


ALTER TABLE `zone2000`.`m_mesin` 
ADD CONSTRAINT `zone2000_fk_mesin_jenis_mesin`
  FOREIGN KEY (`id_jenis_mesin`)
  REFERENCES `zone2000`.`m_jenis_mesin` (`id_jenis_mesin`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

UPDATE `zone2000`.`m_mesin` SET `id_jenis_mesin`='1' WHERE `id_mesin`='1';

ALTER TABLE `zone2000`.`m_mesin` 
CHANGE COLUMN `status` `status` INT(1) NULL DEFAULT 1 COMMENT 'status\n1 : fine\n0 : rusak' ;

ALTER TABLE `zone2000`.`zn_service` 
CHANGE COLUMN `tgl_dibuat` `tgl_dibuat` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ;


ALTER TABLE `zone2000`.`zn_mesin_kerusakan` 
CHANGE COLUMN `status_service` `request_service` INT(1) NULL DEFAULT '0' COMMENT 'apakah kerusakan ini meminta request service ke pusat\n0 : perbaikan sendiri oleh teknisi di cabang\n1 : service oleh pusat\n' ;


ALTER TABLE `zone2000`.`zn_service` 
CHANGE COLUMN `status` `status_request` INT(1) NULL DEFAULT '0' COMMENT '0 : waiting to confim\n1 : diterima\n2 : ditolak' ;

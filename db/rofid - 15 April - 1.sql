-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema zone2000
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema zone2000
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `zone2000` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `zone2000` ;

-- -----------------------------------------------------
-- Table `zone2000`.`m_item`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`m_item` (
  `item_id` INT NOT NULL AUTO_INCREMENT,
  `item_key` INT UNSIGNED NULL,
  `item_name` VARCHAR(50) NULL,
  `itemtype_id` INT NULL,
  PRIMARY KEY (`item_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zone2000`.`m_item_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`m_item_type` (
  `itemtype_id` INT NOT NULL AUTO_INCREMENT,
  `type_code` VARCHAR(10) NULL,
  `type_name` VARCHAR(50) NULL,
  PRIMARY KEY (`itemtype_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zone2000`.`m_item_stok`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zone2000`.`m_item_stok` (
  `itemstok_id` INT NOT NULL AUTO_INCREMENT,
  `item_id` INT NULL,
  `id_store` INT NULL,
  `num_stok` INT NULL,
  `num_total` INT NULL COMMENT 'stok total dari awal sampe sekarang',
  PRIMARY KEY (`itemstok_id`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

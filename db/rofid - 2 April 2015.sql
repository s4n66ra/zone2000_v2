
CREATE TABLE IF NOT EXISTS `m_machine` (
`id_mesin` int(3) NOT NULL AUTO_INCREMENT pro,
  `kd_mesin` varchar(40) NOT NULL,
  `nama_mesin` varchar(50) NOT NULL,
  `deskripsi` varchar(100) DEFAULT NULL,
  `thumbnail` varchar(50) DEFAULT NULL,
  `foto` varchar(50) DEFAULT NULL,
  `jenis_coin` enum('acrylic','regular','wristband','') NOT NULL DEFAULT 'acrylic',
  `id_kategori_mesin` int(3) NOT NULL,
  `id_kelas` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

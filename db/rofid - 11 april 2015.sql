-- --------------------------------------------------------

--
-- Table structure for table `zn_pr_sparepart`
--
DROP TABLE IF EXISTS `m_stok_sparepart`;
DROP TABLE IF EXISTS `zn_pembelian_sparepart_detail`;
DROP TABLE IF EXISTS `zn_rusak_sparepart`;
DROP TABLE IF EXISTS `zn_pr_sparepart_history_detail`;
DROP TABLE IF EXISTS `zn_pr_sparepart_history`;
DROP TABLE IF EXISTS `zn_pr_sparepart_detail`;
DROP TABLE IF EXISTS `zn_pr_sparepart`;
DROP TABLE IF EXISTS `m_sparepart`;

-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 11, 2015 at 01:24 PM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `zone2000`
--

-- --------------------------------------------------------

--
-- Table structure for table `m_sparepart`
--

DROP TABLE IF EXISTS `m_sparepart`;
CREATE TABLE IF NOT EXISTS `m_sparepart` (
`id_sparepart` int(6) NOT NULL,
  `nama_sparepart` varchar(250) DEFAULT NULL,
  `kd_sparepart` varchar(50) DEFAULT NULL,
  `harga` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_pembelian_sparepart_detail`
--

CREATE TABLE IF NOT EXISTS `zn_pr_sparepart` (
`id_pr` int(11) NOT NULL,
  `kd_pr` varchar(50) DEFAULT NULL,
  `kd_po` varchar(50) DEFAULT NULL,
  `nama_pr` varchar(250) DEFAULT NULL,
  `status_pr` int(2) DEFAULT '0' COMMENT '0 : draft\n1 : dibuat / waiting aproval\n2 : revisi\n3 : ditolak\n4 : diterima',
  `id_pengirim` int(3) DEFAULT NULL COMMENT 'cabang yang meminta PR',
  `id_tujuan` int(3) DEFAULT NULL COMMENT 'id store pusat yang dituju',
  `keterangan` text,
  `tgl_dibuat` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'tanggal PR ini dibuat',
  `tgl_diajukan` datetime DEFAULT NULL,
  `tgl_disetujui` datetime DEFAULT NULL,
  `tgl_ditolak` datetime DEFAULT NULL,
  `tgl_dikirim` datetime DEFAULT NULL,
  `tgl_diterima` datetime DEFAULT NULL,
  `tgl_revisi` datetime DEFAULT NULL,
  `count_revisi` int(2) DEFAULT '0',
  `id_mesin_rusak` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `zn_pembelian_sparepart_detail`;
CREATE TABLE IF NOT EXISTS `zn_pembelian_sparepart_detail` (
`id_pembelian_detail` int(11) NOT NULL,
  `kd_pembelian_detail` varchar(50) DEFAULT NULL,
  `id_pembelian` int(11) DEFAULT NULL,
  `id_sparepart` int(5) DEFAULT NULL,
  `jumlah` int(6) DEFAULT NULL,
  `status` int(1) DEFAULT NULL COMMENT 'status barang\n0 : rusak\n1 : ok \n2 : retur\ndsb',
  `harga` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_pr_sparepart_detail`
--

DROP TABLE IF EXISTS `zn_pr_sparepart_detail`;
CREATE TABLE IF NOT EXISTS `zn_pr_sparepart_detail` (
`id_pr_detail` int(11) NOT NULL,
  `id_pr` int(11) DEFAULT NULL,
  `id_sparepart` int(6) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `jml_diajukan` int(11) DEFAULT '0',
  `jml_disetujui` int(6) DEFAULT '0',
  `jml_dikirim` int(6) DEFAULT '0',
  `jml_diterima` int(6) DEFAULT '0',
  `keterangan` varchar(45) DEFAULT NULL,
  `jumlah` int(6) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

CREATE TABLE IF NOT EXISTS `zn_pr_sparepart_history` (
`id_pr_history` int(11) NOT NULL,
  `tanggal` varchar(45) DEFAULT NULL,
  `status` int(1) DEFAULT NULL COMMENT '0 : dibuat \n1 : diterima\n2 : revisi',
  `id_pegawai` int(11) DEFAULT NULL,
  `id_pr` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `zn_pr_sparepart_history_detail`
--

DROP TABLE IF EXISTS `zn_pr_sparepart_history_detail`;
CREATE TABLE IF NOT EXISTS `zn_pr_sparepart_history_detail` (
`id_pr_history_detail` int(11) NOT NULL,
  `id_pr_history` int(11) DEFAULT NULL,
  `id_sparepart` int(6) DEFAULT NULL,
  `jumlah` int(5) DEFAULT NULL,
  `keterangan` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zn_rusak_sparepart`
--

DROP TABLE IF EXISTS `zn_rusak_sparepart`;
CREATE TABLE IF NOT EXISTS `zn_rusak_sparepart` (
`id_rusak_sparepart` int(11) NOT NULL,
  `kd_rusak_sparepart` varchar(50) DEFAULT NULL,
  `id_mesin_rusak` int(11) DEFAULT NULL,
  `id_sparepart` int(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `m_sparepart`
--
ALTER TABLE `m_sparepart`
 ADD PRIMARY KEY (`id_sparepart`);

ALTER TABLE `zn_pr_sparepart`
 ADD PRIMARY KEY (`id_pr`), ADD KEY `fk_pr_hadiah_pengirim_idx` (`id_pengirim`), ADD KEY `fk_pr_hadiah_penerima_idx` (`id_tujuan`), ADD KEY `fk_pr_sparepart_mesin_kerusakan_idx` (`id_mesin_rusak`);


--
-- Indexes for table `zn_pembelian_sparepart_detail`
--
ALTER TABLE `zn_pembelian_sparepart_detail`
 ADD PRIMARY KEY (`id_pembelian_detail`), ADD KEY `fk_pembelian_mesin_detail_idx` (`id_pembelian`), ADD KEY `fk_pembelian_sparepart_detail_sparepart_idx` (`id_sparepart`);

--
-- Indexes for table `zn_pr_sparepart_detail`
--
ALTER TABLE `zn_pr_sparepart_detail`
 ADD PRIMARY KEY (`id_pr_detail`), ADD KEY `fk_fr_sparepart_detail_idx` (`id_pr`);

--
-- Indexes for table `zn_pr_sparepart_history_detail`
--
ALTER TABLE `zn_pr_sparepart_history`
 ADD PRIMARY KEY (`id_pr_history`), ADD KEY `fk_pr_hadiah_history_idx` (`id_pr`), ADD KEY `fk_pr_hadiah_history_pegawai_idx` (`id_pegawai`);


ALTER TABLE `zn_pr_sparepart_history_detail`
 ADD PRIMARY KEY (`id_pr_history_detail`), ADD KEY `fk_pr_sparepart_history_detail_idx` (`id_pr_history`), ADD KEY `fk_pr_sparepart_history_detail_sparepart_idx` (`id_sparepart`);

--
-- Indexes for table `zn_rusak_sparepart`
--
ALTER TABLE `zn_rusak_sparepart`
 ADD PRIMARY KEY (`id_rusak_sparepart`), ADD KEY `fk_rusak_mesin_rusak_idx` (`id_mesin_rusak`), ADD KEY `fk_rusak_sparepart_sparepart_idx` (`id_sparepart`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `m_sparepart`
--

ALTER TABLE `zn_pr_sparepart`
MODIFY `id_pr` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `m_sparepart`
MODIFY `id_sparepart` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_pembelian_sparepart_detail`
--
ALTER TABLE `zn_pembelian_sparepart_detail`
MODIFY `id_pembelian_detail` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_pr_sparepart_detail`
--
ALTER TABLE `zn_pr_sparepart_detail`
MODIFY `id_pr_detail` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `zn_pr_sparepart_history`
MODIFY `id_pr_history` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `zn_pr_sparepart_history_detail`
--
ALTER TABLE `zn_pr_sparepart_history_detail`
MODIFY `id_pr_history_detail` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zn_rusak_sparepart`
--
ALTER TABLE `zn_rusak_sparepart`
MODIFY `id_rusak_sparepart` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `zn_pembelian_sparepart_detail`
--


ALTER TABLE `zn_pr_sparepart`
ADD CONSTRAINT `zone2000_fk_pr_sparepart_mesin_kerusakan` FOREIGN KEY (`id_mesin_rusak`) REFERENCES `zn_mesin_kerusakan` (`id_mesin_rusak`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_pr_sparepart_pengirim` FOREIGN KEY (`id_pengirim`) REFERENCES `m_cabang` (`id_cabang`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_pr_sparepart_tujuan` FOREIGN KEY (`id_tujuan`) REFERENCES `m_cabang` (`id_cabang`) ON DELETE NO ACTION ON UPDATE NO ACTION;


ALTER TABLE `zn_pembelian_sparepart_detail`
ADD CONSTRAINT `zone2000_fk_pembelian_sparepart_detail` FOREIGN KEY (`id_pembelian`) REFERENCES `zn_pembelian_sparepart` (`id_pembelian`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zn_pr_sparepart_detail`
--
ALTER TABLE `zn_pr_sparepart_detail`
ADD CONSTRAINT `zone2000_fk_fr_sparepart_detail` FOREIGN KEY (`id_pr`) REFERENCES `zn_pr_sparepart` (`id_pr`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zn_pr_sparepart_history_detail`
--

ALTER TABLE `zn_pr_sparepart_history`
ADD CONSTRAINT `zone2000_fk_pr_sparepart_history` FOREIGN KEY (`id_pr`) REFERENCES `zn_pr_sparepart` (`id_pr`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `zone2000_fk_pr_sparepart_history_pegawai` FOREIGN KEY (`id_pegawai`) REFERENCES `m_pegawai` (`id_pegawai`) ON DELETE NO ACTION ON UPDATE NO ACTION;


ALTER TABLE `zn_pr_sparepart_history_detail`
ADD CONSTRAINT `zone2000_fk_pr_sparepart_history_detail` FOREIGN KEY (`id_pr_history`) REFERENCES `zn_pr_sparepart_history` (`id_pr_history`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zn_rusak_sparepart`
--
ALTER TABLE `zn_rusak_sparepart`
ADD CONSTRAINT `zone2000_fk_rusak_sparepart_mesin_rusak` FOREIGN KEY (`id_mesin_rusak`) REFERENCES `zn_mesin_kerusakan` (`id_mesin_rusak`) ON DELETE NO ACTION ON UPDATE NO ACTION;


CREATE TABLE IF NOT EXISTS `m_stok_sparepart` (
`id_stok_sparepart` int(11) NOT NULL,
  `kd_stok_sparepart` varchar(50) DEFAULT NULL,
  `id_sparepart` int(2) DEFAULT NULL,
  `id_cabang` int(3) DEFAULT NULL,
  `stok_total` int(11) DEFAULT '0',
  `stok_gudang` int(11) DEFAULT '0',
  `stok_mesin` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `m_stok_sparepart`
--
ALTER TABLE `m_stok_sparepart`
 ADD PRIMARY KEY (`id_stok_sparepart`), ADD KEY `fk_stok_hadiah_cabang_idx` (`id_cabang`), ADD KEY `fk_stok_sparepart_sparepart_idx` (`id_sparepart`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `m_stok_sparepart`
--
ALTER TABLE `m_stok_sparepart`
MODIFY `id_stok_sparepart` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `m_stok_sparepart`
--
ALTER TABLE `m_stok_sparepart`
ADD CONSTRAINT `zone2000_fk_stok_sparepart_cabang` FOREIGN KEY (`id_cabang`) REFERENCES `m_cabang` (`id_cabang`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE  `m_mesin` ADD  `item_id` INT( 11 ) NOT NULL

ALTER TABLE  `m_mesin` ADD INDEX (  `item_id` )

ALTER TABLE  `m_mesin` ADD FOREIGN KEY (  `item_id` ) REFERENCES  `zone2000`.`m_item` (
`item_id`
) ON DELETE NO ACTION ON UPDATE NO ACTION ;
